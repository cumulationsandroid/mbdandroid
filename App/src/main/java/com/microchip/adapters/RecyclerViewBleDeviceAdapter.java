/*
 *
 *
 *     RecyclerViewBleDeviceAdapter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.adapters;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.ParcelUuid;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.microchip.R;
import com.microchip.utils.SignalDrawable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class RecyclerViewBleDeviceAdapter extends RecyclerView.Adapter<RecyclerViewBleDeviceAdapter.DeviceViewHolder> {

    static private Activity mContext;
    private List<BluetoothDevice> mDeviceList;
    private List<Device> mCustomDeviceList = new ArrayList<>();
    private List<BluetoothDevice> mDeviceListSecond;
    private HashMap<String, Integer> hashMap;
    private OnDeviceConnectionListener listener;

    public RecyclerViewBleDeviceAdapter(List<BluetoothDevice> mList, Activity context) {
        this.mDeviceList = mList;
        mDeviceListSecond = new ArrayList<>();
        mDeviceListSecond.addAll(mList);
        mContext = context;
        hashMap = new HashMap<>();
    }

    public void removeCustomList(){
        if(mCustomDeviceList !=  null && mCustomDeviceList.size() > 0){
            mCustomDeviceList.clear();
        }
    }


    @Override
    public int getItemCount() {
        return mDeviceList.size();
        // return 10;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void add(BluetoothDevice device, int position) {
        mDeviceList.add(position, device);
        notifyItemInserted(position);
    }

    @Override
    public void onBindViewHolder(DeviceViewHolder contactViewHolder,final int pos) {


        /**
         * Setting the name and the RSSI of the BluetoothDevice. provided it
         * is a valid one
         */
        final BluetoothDevice device = mDeviceList.get(pos);


        final String deviceName = device.getName();
        if (deviceName != null && deviceName.length() > 0) {
            contactViewHolder.deviceName.setText(deviceName);
            contactViewHolder.signalStrengthValue.setText(hashMap.get(mDeviceList.get(pos).getAddress()) + "dBm");
            int drwawable = SignalDrawable.getStrength(hashMap.get(mDeviceList.get(pos).getAddress()));
            contactViewHolder.mSignalStrengthImageView.setImageDrawable(mContext.getResources().getDrawable(drwawable));
            contactViewHolder.deviceAddress.setText(mDeviceList.get(pos).getAddress());
//            contactViewHolder.deviceAddress.setText(device.getAddress());
//            byte rssival = (byte) mdevRssiValues.get(device.getAddress())
//                    .intValue();
//            if (rssival != 0) {
//                contactViewHolder.deviceRssi.setText(String.valueOf(rssival));
//            }

        } else {
            contactViewHolder.deviceName.setText("unknown");
            contactViewHolder.deviceName.setSelected(true);
            contactViewHolder.deviceAddress.setText(mDeviceList.get(pos).getAddress());
            // contactViewHolder.deviceAddress.setText(device.getAddress());
        }
   //     contactViewHolder.mParent.setVisibility(View.VISIBLE);

        contactViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ParcelUuid> uuid = null;
                try {
                    uuid = mCustomDeviceList.get(pos).uuid;
                }catch(Exception e){
                    e.printStackTrace();
                }


                listener.onDeviceConnected(device.getAddress(), deviceName, uuid);
//                BLEConnection.connect(device.getAddress(), deviceName, mContext);


            }
        });


    }


    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.device_list_row, viewGroup, false);
        return new DeviceViewHolder(itemView);
    }


    public void remove(int position) {
        mDeviceList.remove(position);
        notifyItemRemoved(position);
    }

    public void addDevices(BluetoothDevice mDevice, int rssi, List<ParcelUuid> uuidList) {
        if (!mDeviceList.contains(mDevice)) {
            mDeviceList.add(mDevice);
            mDeviceListSecond.add(mDevice);
            hashMap.put(mDevice.getAddress(), rssi);
            if(mCustomDeviceList != null){
               Device device = new Device();
               device.device = mDevice;
               device.rssi = rssi;
               device.uuid = uuidList;
               mCustomDeviceList.add(device);
            }

            notifyDataSetChanged();
        } else {
            hashMap.put(mDevice.getAddress(), rssi);
            notifyDataSetChanged();
        }

    }

    public void search(String value) {
        String s = value.toLowerCase(Locale.getDefault());
        mDeviceList.clear();
        if (s.length() == 0) {
            mDeviceList.addAll(mDeviceListSecond);

        } else {
            for (BluetoothDevice device : mDeviceListSecond) {

                if ((device.getName() == null ? "" : device.getName()).concat(device.getAddress()).toLowerCase(Locale.getDefault()).contains(s)) {
//                    if (!isDuplicate(mDeviceList, device)) {
                    mDeviceList.add(device);
//                    } else {
//                        mDeviceListSecond.remove(device);
//                    }
                }

            }
        }
        notifyDataSetChanged();
    }

    public void clearList() {
        mDeviceListSecond.clear();
    }

  /*  private boolean isDuplicate(List<BluetoothDevice> mDeviceList, BluetoothDevice device) {
        for (BluetoothDevice bluetoothDevice : mDeviceList) {
            if (bluetoothDevice.getAddress().equals(device)) {
                return true;
            }
        }
        return false;
    }*/

    public void setonDeviceConnectedLisetener(OnDeviceConnectionListener listener) {
        this.listener = listener;
    }

    public interface OnDeviceConnectionListener {
        void onDeviceConnected(String address, String name, List<ParcelUuid> uuidList);
    }

    public static class DeviceViewHolder extends RecyclerView.ViewHolder {

      //  protected RelativeLayout mParent, mLoading;
        protected TextView deviceName;
        protected TextView signalStrengthValue;
        protected TextView deviceAddress;
        protected CardView cardView;
        protected ImageView mSignalStrengthImageView;

        public DeviceViewHolder(View v) {
            super(v);
           // mParent = (RelativeLayout) v.findViewById(R.id.parent);
            deviceName = (TextView) v.findViewById(R.id.ble_device_name);
            deviceAddress = (TextView) v.findViewById(R.id.ble_device_address);
            cardView = (CardView) v.findViewById(R.id.card_view);
            mSignalStrengthImageView = (ImageView) v.findViewById(R.id.signalStrengthImageView);
            signalStrengthValue = (TextView) v.findViewById(R.id.signalStrengthValue);


        }
    }

    public static class Device{
        BluetoothDevice device;
        int rssi;
        List<ParcelUuid> uuid;
    }

}
