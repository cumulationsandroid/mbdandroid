/*
 *
 *
 *     SerialPortRecycleViewAdapter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.adapters.SerialPort;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.microchip.R;

import java.util.List;

/**
 * Created by
 */
public class SerialPortRecycleViewAdapter extends RecyclerView.Adapter<SerialPortRecycleViewAdapter.ViewHolder> {


    private Context context;
    private List<MessageModel> messageModelList;

    public SerialPortRecycleViewAdapter(List<MessageModel> listAdapter, Context context) {
        this.messageModelList = listAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.serial_port_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final MessageModel messageModel = messageModelList.get(position);
        if (messageModel != null) {
            if (messageModel.isMine()) {
                holder.mMessage.setBackgroundResource(R.drawable.blue_chat_bg);
                holder.mMineImage.setVisibility(View.VISIBLE);
                holder.mSenderImage.setVisibility(View.GONE);
                holder.mMessage.setText(messageModel.getMessage());
                holder.mMessage.setTextColor(context.getResources().getColor(R.color.white));

            } else {
                holder.mMessage.setBackgroundResource(R.drawable.white_chat_bg);
                holder.mMineImage.setVisibility(View.GONE);
                holder.mSenderImage.setVisibility(View.VISIBLE);
                holder.mMessage.setText(messageModel.getMessage());
                holder.mMessage.setTextColor(context.getResources().getColor(R.color.black));
            }
        }

    }

    @Override
    public int getItemCount() {
        return messageModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView mMessage;
        protected ImageView mMineImage, mSenderImage;

        public ViewHolder(View v) {
            super(v);
            mMessage = (TextView) v.findViewById(R.id.message);
            mMineImage = (ImageView) v.findViewById(R.id.mine_icon);
            mSenderImage = (ImageView) v.findViewById(R.id.sender_icon);

        }
    }
}
