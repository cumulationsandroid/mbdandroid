/*
 *
 *
 *     ServiceListRecycleViewAdapter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.adapters;

import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.microchip.utils.ImageProccess;
import com.microchip.utils.TextProccess;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

/**
 * Created by
 */
public class ServiceListRecycleViewAdapter extends RecyclerView.Adapter<ServiceListRecycleViewAdapter.ServiceViewHolder> {


    Transformation transformation = new Transformation() {

        @Override
        public Bitmap transform(Bitmap source) {
            int targetWidth = 250;

            double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
            int targetHeight = (int) (targetWidth * aspectRatio);
            Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
            if (result != source) {
                // Same bitmap is returned if sizes are the same
                source.recycle();
            }
            return result;
        }

        @Override
        public String key() {
            return "transformation" + " desiredWidth";
        }
    };
    private Context context;
    private List<BluetoothGattService> listAdapter;
    private OnItemClickListener onItemClickLisener;

    public ServiceListRecycleViewAdapter(List<BluetoothGattService> listAdapter, Context context) {
        this.listAdapter = listAdapter;
        this.context = context;
    }

    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.ble_service_item, parent, false);
        return new ServiceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ServiceViewHolder holder, final int position) {

        final BluetoothGattService service = listAdapter.get(position);
        if (service != null) {
            if (service.getUuid().toString().equalsIgnoreCase(GattAttributes.GENERIC_ACCESS_SERVICE)) {
                holder.mServiceName.setText("Generic Information");
            } else {
                holder.mServiceName.setText(GattAttributes.lookup(service.getUuid().toString(),
                        "Unknown service"));
            }
            int drawable = ImageProccess.getImage(context, service.getUuid());
            String descText = TextProccess.getText(context, service.getUuid());
            if (descText.contains("\n")) {
                String[] splitStrings = descText.split("\n");
                holder.mFeatureTextView.setText(splitStrings[1]);
                holder.mbleNumberTextView.setText(splitStrings[0]);
            } else {
                holder.mbleNumberTextView.setText(descText);
            }
            if (drawable != 0)
                Picasso.with(context).load(drawable).into(holder.mImageView);
            holder.mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickLisener.onItemClicked(service, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listAdapter.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void setOnItemClickLisener(OnItemClickListener onItemClickLisener) {
        this.onItemClickLisener = onItemClickLisener;
    }


    public interface OnItemClickListener {
        void onItemClicked(BluetoothGattService service, int pos);
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder {
        protected TextView mServiceName;
        protected ImageView mImageView;
        protected TextView mFeatureTextView;
        protected TextView mbleNumberTextView;
        protected CardView mCardView;

        public ServiceViewHolder(View v) {
            super(v);
            mServiceName = (TextView) v.findViewById(R.id.ble_service_name);
            mCardView = (CardView) v.findViewById(R.id.card_view);
            mImageView = (ImageView) v.findViewById(R.id.serviceImageView);
            mFeatureTextView = (TextView) v.findViewById(R.id.ble_description);
            mbleNumberTextView = (TextView) v.findViewById(R.id.ble_service_number);
        }
    }
}
