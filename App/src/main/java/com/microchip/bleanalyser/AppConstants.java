/*
 *
 *
 *     AppConstants.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.bleanalyser;


public class AppConstants {

    public static final String DEVICE_DISCONNECTED_RESULT_CODE = "DISONNECTED";
    public static final int NEUTRAL_BUTTON = -3;
    public static final String  GATTSERVER_PROFILES= "GATT SERVER PROFILES";
    public static final String ANS_FIRST_TIME_LAUNCH = "ANS_FIRST_TIME_LAUNCH";
    public static final String ANS_NOTIFICATION_CHANGE_REQUEST = "NOTIFICATION_CHANGE_REQUEST";
    public static final String ANS_NEW_SMS_RECEIVED = "ANS_NEW_SMS_RECEIVED";
    public static final String ANS_NEW_MISSED_CALL_RECEIVED = "ANS_NEW_MISSED_CALL";
    public static final String ANS_UNREAD_SMS_UPDATE = "ANS_UNREAD_SMS_UPDATE";
    public static final String ANS_UNREAD_MISSED_CALL_UPDATE = "ANS_UNREAD_MISSED_CALL_UPDATE";
    public static final String ANS_NEW_SMS_COUNT = "ANS_NEW_SMS_RECEIVED_COUNT";
    public static final String ANS_NEW_MISSED_CALL_COUNT = "ANS_NEW_MISSED_CALL_COUNT";
    public static final String ANS_NEW_MISSED_CALL_TEXT = "ANS_NEW_MISSED_CALL_TEXT";
    public static final String ANS_UNREAD_SMS_COUNT = "ANS_UNREAD_SMS_COUNT";
    public static final String ANS_UNREAD_MISSED_CALL_COUNT = "ANS_UNREAD_MISSED_CALL_COUNT";
    public static final String ANS_NEW_SMS_TEXT = "ANS_NEW_SMS_TEXT";
    public static final String PAS_ALERT_STATUS = "Alert Status";
    public static final String PAS_RINGER_STATUS = "Ringer Status";
    public static final String PAS_CONTROL_POINT = "Control Point";
    public static final String PAS_ACTION = "Normal Action";
    public static int TIME_SOURCE_UNKNOWN = 0;
    public static int TIME_SOURCE_NETWORK_TIME_PROTOCOL = 1;
    public static int TIME_SOURCE_GPS = 2;
    public static int TIME_SOURCE_RADIO_TIME_SIGNAL = 3;
    public static int TIME_SOURCE_MANUAL = 4;
    public static int TIME_SOURCE_ATOMIC_CLOCK = 5;
    public static int TIME_SOURCE_CELLULAR_NETWORK = 6;
    //private Enum mAdvertisingCategory;
    public enum ADV_CATEGORY { DEFAULT, TIME, ANS, PHONE_ALERT, SERIAL_PORT }

    //Bundle Params
    public static String GATTDB_SELECTED_SERVICE = "gatt db service";
    public static String GATTDB_SELECTED_CHARACTERISTIC = "selected characterisitics";

}
