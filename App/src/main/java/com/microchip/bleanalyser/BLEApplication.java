/*
 *
 *
 *     BLEApplication.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.bleanalyser;


import android.app.Application;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;

import com.microchip.adapters.SerialPort.MessageModel;
import com.atmel.blecommunicator.com.atmel.Characteristics.BloodPressureFeatureCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.BloodPressureMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.HeartRateMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Services.BloodPressureService;
import com.atmel.blecommunicator.com.atmel.Services.HeartRateService;
import com.atmel.blecommunicator.com.atmel.Services.OTAUService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BLEApplication extends Application {

    public static float PREV_MIN = 0.0f;
    public static float PREV_MAX = 0.0f;
    private static BLEApplication mInstance;
    private BluetoothGattService mBluetoothGattService, mBluetoothOTAUGattService;
    private BluetoothGattService mAdvertisingService;
    private BloodPressureMeasurementCharacteristic mBloodPressureMeasurementCharacteristic;
    private BloodPressureFeatureCharacteristic mBloodPressureFeatureCharacteristic;
    private HeartRateMeasurementCharacteristic mHeartRateMeasurementCharacteristic;
    private BloodPressureService mBloodPressureService;
    private OTAUService mOTAUService;
    private HeartRateService mHeartRateService;
    private List<BluetoothGattService> mAdvertsingList = new ArrayList<BluetoothGattService>();
    private List<MessageModel> mSPPMsgList = new ArrayList<>();

    private BluetoothGattCharacteristic mGattCharacteristic;
    //GATT DB
    private ArrayList<HashMap<String, BluetoothGattService>> mGattServiceMasterData =
            new ArrayList<HashMap<String, BluetoothGattService>>();
    private List<BluetoothGattCharacteristic> mGattCharacteristics;
    private BluetoothGattCharacteristic mBluetoothgattcharacteristic;
    private BluetoothGattDescriptor mBluetoothGattDescriptor;

    public static synchronized BLEApplication getInstance() {
        return mInstance;
    }

    public BloodPressureFeatureCharacteristic getmBloodPressureFeatureCharacteristic() {
        return mBloodPressureFeatureCharacteristic;
    }

    public void setmBloodPressureFeatureCharacteristic(BloodPressureFeatureCharacteristic mBloodPressureFeatureCharacteristic) {
        this.mBloodPressureFeatureCharacteristic = mBloodPressureFeatureCharacteristic;
    }

    public HeartRateMeasurementCharacteristic getmHeartRateMeasurementCharacteristic() {
        return mHeartRateMeasurementCharacteristic;
    }

    public void setmHeartRateMeasurementCharacteristic(HeartRateMeasurementCharacteristic mHeartRateMeasurementCharacteristic) {
        this.mHeartRateMeasurementCharacteristic = mHeartRateMeasurementCharacteristic;
    }

    public HeartRateService getmHeartRateService() {
        return mHeartRateService;
    }

    public void setmHeartRateService(HeartRateService mHeartRateService) {
        this.mHeartRateService = mHeartRateService;
    }

    public BloodPressureMeasurementCharacteristic getmBloodPressureMeasurementCharacteristic() {
        return mBloodPressureMeasurementCharacteristic;
    }

    public void setmBloodPressureMeasurementCharacteristic(BloodPressureMeasurementCharacteristic mBloodPressureMeasurementCharacteristic) {
        this.mBloodPressureMeasurementCharacteristic = mBloodPressureMeasurementCharacteristic;
    }

    public BloodPressureService getmBloodPressureService() {
        return mBloodPressureService;
    }

    public void setmBloodPressureService(BloodPressureService mBloodPressureService) {
        this.mBloodPressureService = mBloodPressureService;
    }

    public OTAUService getOTAUService() {
        return mOTAUService;
    }

    public void setOTAUService(OTAUService OTAService) {
        this.mOTAUService = OTAService;
    }

    public List<MessageModel> getSPPMsgList() {
        return mSPPMsgList;
    }

    public void addSPPMsgList(List<MessageModel> msgModel) {
         this.mSPPMsgList=msgModel;
    }

    public void SPPMsgClear() {
        if (this.mSPPMsgList!=null)
           this.mSPPMsgList.clear();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public BluetoothGattService getmBluetoothGattService() {
        return mBluetoothGattService;
    }

    public void setmBluetoothGattService(BluetoothGattService mBluetoothGattService) {
        this.mBluetoothGattService = mBluetoothGattService;
    }

    public BluetoothGattService getBluetoothOTAGattService() {
        return mBluetoothOTAUGattService;
    }

    public void setBluetoothOTAGattService(BluetoothGattService mBluetoothGattService) {
        this.mBluetoothOTAUGattService = mBluetoothGattService;
    }

    public BluetoothGattService getBluetoothGattAdvService() {
        return mAdvertisingService;
    }

    public void setBluetoothGattAdvService(BluetoothGattService mBluetoothGattService) {
        this.mAdvertisingService = mBluetoothGattService;
    }

    public void addAdvertisingService(BluetoothGattService service){
        mAdvertsingList.add(service);
    }

    public List<BluetoothGattService> getAdvertsingList(){
        return mAdvertsingList;
    }

    public void removeAdvertisingList(){

       if(mAdvertsingList != null && mAdvertsingList.size() > 0){
           mAdvertsingList.clear();
       }
    }

    /**
     * getting app context
     *
     * @return
     */
    public Context getContext() {
        return getApplicationContext();
    }

    /**
     * getter method for Blue tooth GATT characteristic
     *
     * @return {@link BluetoothGattCharacteristic}
     */
    public BluetoothGattCharacteristic getBluetoothgattcharacteristic() {
        return mBluetoothgattcharacteristic;
    }

    /**
     * setter method for Blue tooth GATT characteristics
     *
     * @param bluetoothgattcharacteristic
     */
    public void setBluetoothgattcharacteristic(
            BluetoothGattCharacteristic bluetoothgattcharacteristic) {
        this.mBluetoothgattcharacteristic = bluetoothgattcharacteristic;
    }

    /**
     * getter method for Blue tooth GATT characteristic
     *
     * @return {@link BluetoothGattCharacteristic}
     */
    public BluetoothGattDescriptor getBluetoothgattDescriptor() {
        return mBluetoothGattDescriptor;
    }

    /**
     * setter method for Blue tooth GATT Descriptor
     *
     * @param bluetoothGattDescriptor
     */
    public void setBluetoothgattdescriptor(
            BluetoothGattDescriptor bluetoothGattDescriptor) {
        this.mBluetoothGattDescriptor = bluetoothGattDescriptor;
    }

    /**
     * getter method for blue tooth GATT Characteristic list
     *
     * @return {@link List<BluetoothGattCharacteristic>}
     */
    public List<BluetoothGattCharacteristic> getGattCharacteristics() {
        return mGattCharacteristics;
    }

    /**
     * setter method for blue tooth GATT Characteristic list
     *
     * @param gattCharacteristics
     */
    public void setGattCharacteristics(
            List<BluetoothGattCharacteristic> gattCharacteristics) {
        this.mGattCharacteristics = gattCharacteristics;
    }

    public ArrayList<HashMap<String, BluetoothGattService>> getGattServiceMasterData() {
        return mGattServiceMasterData;
    }

    public void setGattServiceMasterData(
            ArrayList<HashMap<String, BluetoothGattService>> gattServiceMasterData) {
        this.mGattServiceMasterData = gattServiceMasterData;

    }

    public BluetoothGattCharacteristic getmGattCharacteristic() {
        return mGattCharacteristic;
    }

    public void setmGattCharacteristic(BluetoothGattCharacteristic mGattCharacteristic) {
        this.mGattCharacteristic = mGattCharacteristic;
    }
}
