/*
 *
 *
 *     BLEBaseActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.bleanalyser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.microchip.R;
import com.microchip.communicators.BLEDataReceiver;
import com.microchip.utils.PairingAlert;
import com.microchip.utils.ReconnectAlert;


/**
 * Created by Atmel on 11/8/15.
 */
public class BLEBaseActivity extends AppCompatActivity implements BLEDataReceiver.BLEConnection, BLEDataReceiver.GATTProfiles {


    public ReconnectAlert mReconnectionAlert = null;
    public PairingAlert mPairAlert = null;

    public static void setConnectionListener(BLEDataReceiver.BLEConnection mConnectionListener) {
        BLEDataReceiver.setBLEConnectionListener(mConnectionListener);
    }

    public static void setGattListener(BLEDataReceiver.GATTProfiles mGattListener) {
        BLEDataReceiver.setGattListener(mGattListener);
    }

    /*
    public static void setGattServerListener(BLEDataReceiver.GattServerProfiles mGattServerListener) {
        BLEDataReceiver.setGattServerListener(mGattServerListener);
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReconnectionAlert = new ReconnectAlert(this);
        mPairAlert = new PairingAlert(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onLeDeviceConnected(String deviceName) {
        mReconnectionAlert.dismissAlert();
    }

    @Override
    public void onLeServiceDiscovered(boolean status) {

    }

    @Override
    public void onLeDeviceDisconnected() {
//        mReconnectionAlert.showAlert();
//        BLEConnection.autoConnect(this);
        Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLePairingRequest() {

    }

    @Override
    public void onLePairingProgressStatus(boolean status) {

    }

    @Override
    public void onLePairingDoneStatus(boolean status) {

    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLeBluetoothStatusChanged() {

    }

    @Override
    public void onLeServicesDiscovered(boolean status, String serviceUuid) {

    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {

    }

    @Override
    public void onLeCharacteristicWrite(boolean status) {

    }

    @Override
    public void onLeCharacteristicRead(boolean status) {

    }

    @Override
    public void onLeCharacteristicRead(boolean status, String data, int code) {

    }

    @Override
    public void onLeReadRemoteRssi(int rssi) {

    }

    @Override
    public void onLeDescriptorWrite(boolean status) {

    }

}
