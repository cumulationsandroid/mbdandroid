/*
 *
 *
 *     Dashboard.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.bleanalyser;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.atmel.beacon.activities.AtmelMainActivity;
import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.microchip.communicators.BLEDataReceiver;
import com.microchip.fragments.AboutFragment;
import com.microchip.utils.AppUtils;
import com.atmel.wearable.wearables.BaseActivity;
import com.issc.Bluebit;
import com.issc.ui.ActivityMain;
import com.wifiprovision.microchip.wifiprovisioner.MainActivity;

public class Dashboard extends AppCompatActivity implements DrawerLayout.DrawerListener {

    private LinearLayout mSmartConnectLayout, mBeaconLayout,mBM70Layout, mBM78Layout,mSmartMesh, mSmartDiscover, mBM64Layout,mBleProvisioning;
    private DrawerLayout mDrawerLayout;
    private boolean drawerClosed = true;
    private long BACK_PRESS_DELAY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        Intent gattServiceIntent = new Intent(this, BLEConnection.class);
        gattServiceIntent.putExtra(Constants.RESULT_RECEIVER_KEY, new BLEDataReceiver(new Handler()));
        startService(gattServiceIntent);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerListener(this);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        mSmartConnectLayout = (LinearLayout) findViewById(R.id.smartconnect_layout);

        mSmartConnectLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, HomeScreen.class));
            }
        });

        mBeaconLayout = (LinearLayout) findViewById(R.id.beacon_layout);
        mBeaconLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               startActivity(new Intent(Dashboard.this, AtmelMainActivity.class));

            }
        });
       findViewById(R.id.wearable_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.atmel.wearable.wearables.BLEApplication.setInstance(BLEApplication.getInstance());
                startActivity(new Intent(Dashboard.this, BaseActivity.class));
            }
        });

        mBM70Layout = (LinearLayout) findViewById(R.id.bm_70);
        mBM70Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MBD
                // startActivity(new Intent(Dashboard.this, AtmelMainActivity.class));
                Log.d("Dashboard", "onClickbm70");
                Intent i = new Intent(Dashboard.this, ActivityMain.class);
                i.putExtra("board",70);
                startActivity(i);
                Bluebit.board_id = 70;
            }
        });

        mBM78Layout = (LinearLayout) findViewById(R.id.bm_78);
        mBM78Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MBD
                // startActivity(new Intent(Dashboard.this, AtmelMainActivity.class));
                Log.d("Dashboard", "onClickbm78");
                Intent i = new Intent(Dashboard.this, ActivityMain.class);
                i.putExtra("board",78);
                startActivity(i);
                Bluebit.board_id = 78;
            }
        });



        mSmartMesh = (LinearLayout) findViewById(R.id.smart_mesh_layout);
        mSmartMesh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MBD
                // startActivity(new Intent(Dashboard.this, AtmelMainActivity.class));
                Log.d("Dashboard", "onClick Smart Senor app");
                Intent i = new Intent(Dashboard.this, com.microchip.blesensorapp.DeviceSplashActivity.class);
                startActivity(i);

            }
        });

        mSmartDiscover = (LinearLayout) findViewById(R.id.smart_discover_layout);
        mSmartDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MBD

                Log.d("Dashboard", "onClick Smart Discover");
                Intent i = new Intent(Dashboard.this, com.microchip.bluetoothsmartdiscover.DeviceSplashActivity.class);
                startActivity(i);

            }
        });


        mBM64Layout = (LinearLayout) findViewById(R.id.bm_64);
        mBM64Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MBD
                // startActivity(new Intent(Dashboard.this, AtmelMainActivity.class));
                Log.d("Dashboard", "onClickbm64");
                Intent i = new Intent(Dashboard.this, ActivityMain.class);
                i.putExtra("board",70);
                startActivity(i);
                Bluebit.board_id = 70;
                Bluebit.no_burst_mode = 1 ;
            }
        });



        mBleProvisioning = (LinearLayout) findViewById(R.id.ble_prov);
        mBleProvisioning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MBD
                // startActivity(new Intent(Dashboard.this, AtmelMainActivity.class));
                Log.d("Dashboard", "onClick Ble prov");
                Intent i = new Intent(Dashboard.this, MainActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = this.getSupportFragmentManager();
        Log.e("Stack count>>> ", " " + fm.getBackStackEntryCount());
        if (fm.getBackStackEntryCount() == 0) {
            if (!drawerClosed) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            } else {
                if (BACK_PRESS_DELAY + 2000 > System.currentTimeMillis()) {
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "Press again to exit!", Toast.LENGTH_SHORT).show();
                    BACK_PRESS_DELAY = System.currentTimeMillis();
                }
            }
        } else {
            fm.popBackStack("About", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    protected void onDestroy() {
        Intent gattServiceIntent = new Intent(this, BLEConnection.class);
        BLEConnection.closeGattServer();
        stopService(gattServiceIntent);
        super.onDestroy();
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        this.drawerClosed = false;
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        this.drawerClosed = true;
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        if (menuItem.getItemId() == R.id.about) {
                            setAboutFragment();
                        }
                        return true;
                    }
                });
    }


    private void setAboutFragment() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        AboutFragment aboutFragment = new AboutFragment();
        transaction.addToBackStack("About");
        transaction.replace(R.id.fragment_container, aboutFragment, AppUtils.getTagName(aboutFragment)).commit();
    }
}
