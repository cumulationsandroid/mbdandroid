/*
 *
 *
 *     HomeScreen.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.bleanalyser;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.microchip.communicators.BLEDataReceiver;
import com.microchip.fragments.DeviceScanFragment;
import com.microchip.fragments.AboutFragment;
import com.microchip.services.PAService;
import com.microchip.services.SPPService;
import com.microchip.services.TimeService;
import com.microchip.utils.AppUtils;
import com.microchip.services.AnsService;

import java.util.ArrayList;


public class HomeScreen extends AppCompatActivity implements View.OnClickListener,
        View.OnTouchListener, TextWatcher, DrawerLayout.DrawerListener {

    public TextView mDeviceCountTextView = null;
    public boolean mFilterSelected = false;
    public String[] UUIDs = null;
    public EditText mSearchEditText = null;
    public boolean mIsTablet = false;
    private MenuItem mSearchMenuItem = null;
    //private DrawerLayout mDrawerLayout;
    private View mView = null;
    private ProgressBar mProgressBarSmall;
    private TextView mStartStopView;
    private BluetoothAdapter mBluetoothAdapter;
    private ImageView mFindImageView = null;
    private OnTextListener textListener;
    private View mScanSwitchView;
    private boolean drawerClosed = true;
    private Boolean mIsRestarted = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            mIsTablet = true;
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            mIsTablet = false;
        }
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.ble_scaner);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        mDrawerLayout.setDrawerListener(this);
        mView = findViewById(R.id.home_container);
        mScanSwitchView = mView.findViewById(R.id.frameLay);

        BluetoothManager manager = (BluetoothManager) getSystemService
                (Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();
        mFindImageView = (ImageView) findViewById(R.id.findIconImageView);
        mSearchEditText = (EditText) findViewById(R.id.searchEditText);
        mSearchEditText.setOnTouchListener(HomeScreen.this);
        mSearchEditText.addTextChangedListener(this);
        defineView(mView);

        Intent intent = getIntent();
        mIsRestarted = intent.getBooleanExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, false);

        setFragment();
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        if (navigationView != null) {
//            setupDrawerContent(navigationView);
//        }

        Intent gattServiceIntent = new Intent(this, BLEConnection.class);
        gattServiceIntent.putExtra(Constants.RESULT_RECEIVER_KEY, new BLEDataReceiver(new Handler()));
        startService(gattServiceIntent);

        mStartStopView.setOnClickListener(this);
        mFindImageView.setOnClickListener(this);
    }

    @Override
    protected void onResume() {

        if (!checkPermission()) {
            this.finish();
        } else {
            filterSelection();
        }
        if (DeviceScanFragment.getRestartScanParameter()) {
            Fragment deviceScanFragment = getSupportFragmentManager().findFragmentByTag(DeviceScanFragment.class.getName());
            if (deviceScanFragment != null && deviceScanFragment instanceof DeviceScanFragment && deviceScanFragment.isVisible()) {
                ((DeviceScanFragment) deviceScanFragment).startProgress();
            }
            DeviceScanFragment.setRestartScanParameter(false);
        }
        super.onResume();
    }


    /**
     * checking the Location permission granted or not
     */
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void filterSelection() {
        if (AppUtils.getDefaultSharedPreference(this, getString(R.string.deviceFilter))) {
            mFindImageView.setVisibility(View.VISIBLE);
            mFilterSelected = true;
            ArrayList<String> uuidList = new ArrayList<>();
            UUIDs = null;

            if (AppUtils.getDefaultSharedPreference(this, AppUtils.THERMOMETE)) {
                uuidList.add(GattAttributes.HEALTH_TEMP_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.PROXIMITY)) {
                uuidList.add(GattAttributes.LINK_LOSS_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.FIND_ME)) {
                uuidList.add(GattAttributes.IMMEDIATE_ALERT_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.ANS)) {
                uuidList.add(GattAttributes.ALERT_NOTIFICATION_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.BLOOD_PRESSURE)) {
                uuidList.add(GattAttributes.BLOOD_PRESSURE_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.HEART_RATE)) {
                uuidList.add(GattAttributes.HEART_RATE_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.PHONE_ALERT)) {
                uuidList.add(GattAttributes.PHONE_ALERT_STATUS_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.SCAN_PARAM)) {
                uuidList.add(GattAttributes.SCAN_PARAMETERS_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.TIME)) {
                uuidList.add(GattAttributes.CURRENT_TIME_SERVICE);
            }
            if (AppUtils.getDefaultSharedPreference(this, AppUtils.SERIAL_CHAT)) {
                uuidList.add(GattAttributes.SERIAL_PORT);
            }

            if (uuidList.size() > 0) {
                UUIDs = new String[uuidList.size()];
                for (int i = 0; i < uuidList.size(); i++) {
                    UUIDs[i] = uuidList.get(i);
                }
            }
        } else {
            mFindImageView.setVisibility(View.GONE);
            mFilterSelected = false;
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        DeviceScanFragment.setRestartScanParameter(true);
    }


    private void defineView(View mView) {
        mProgressBarSmall = (ProgressBar) mView.findViewById(R.id.progressBarSmall);
        mStartStopView = (TextView) mView.findViewById(R.id.startStopView);
        mDeviceCountTextView = (TextView) mView.findViewById(R.id.deviceCountView);
    }

    private void setFragment() {
        if (mSearchMenuItem != null) {
            mSearchMenuItem.setVisible(false);
            mSearchEditText.setVisibility(View.GONE);
        }
        mScanSwitchView.setVisibility(View.VISIBLE);
//        Fragment aboutFragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
//        if (aboutFragment != null && aboutFragment.isVisible()) {
//            getSupportFragmentManager().beginTransaction().hide(aboutFragment);
//        } else {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
//            transaction.addToBackStack(null);
        DeviceScanFragment scanFragment = new DeviceScanFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, mIsRestarted);
        scanFragment.setArguments(bundle);
        transaction.replace(R.id.mainContainer, scanFragment, AppUtils.getTagName(scanFragment)).commit();
        mDeviceCountTextView.setVisibility(View.GONE);

//        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        mSearchMenuItem = menu.findItem(R.id.action_search);
        setMenuVisibility(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //  int id = item.getItemId();

        //noinspection SimplifiableIfStatement
      /*  if (id == R.id.action_settings) {
            Intent settingsIntent=new Intent(this,FilterSettingsActivity.class);
            startActivity(settingsIntent);
           // startActivity(new Intent(this, SettingsActivity.class));
        }*/
        switch (item.getItemId()) {
            case android.R.id.home:
                //mDrawerLayout.openDrawer(GravityCompat.START);
                finish();
                return true;
            case R.id.action_search:
                disableComponents(true);
                return true;
            case R.id.action_settings:
                Intent settingsIntent = new Intent(this, FilterSettingsActivity.class);//FilterSettingsActivity
                startActivity(settingsIntent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void disableComponents(boolean b) {
        if (b) {
            mStartStopView.setVisibility(View.GONE);
            mDeviceCountTextView.setVisibility(View.GONE);
            mFindImageView.setVisibility(View.GONE);
            mSearchEditText.setVisibility(View.VISIBLE);
            mSearchMenuItem.setVisible(false);
        } else {
            mStartStopView.setVisibility(View.VISIBLE);
            mDeviceCountTextView.setVisibility(View.VISIBLE);
            if (mFilterSelected)
                mFindImageView.setVisibility(View.VISIBLE);
            mSearchEditText.setVisibility(View.GONE);
            mSearchMenuItem.setVisible(true);

        }


    }


    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        //mDrawerLayout.closeDrawers();
                        if (menuItem.getItemId() == R.id.about) {
                            setAboutFragment();
                        }
                        return true;
                    }
                });
    }

    private void setAboutFragment() {
        mSearchMenuItem.setVisible(false);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        AboutFragment aboutFragment = new AboutFragment();
        transaction.replace(R.id.mainContainer, aboutFragment, AppUtils.getTagName(aboutFragment)).commit();
        mScanSwitchView.setVisibility(View.GONE);

    }

    public void setSmallProgress(int itemCount) {
//        mProgressBarSmall.setProgress(itemCount);
        mDeviceCountTextView.setVisibility(View.VISIBLE);
        if (itemCount == 0) {
            mDeviceCountTextView.setText(getString(R.string.no_device_found));
            if (mSearchEditText.getVisibility() == View.GONE) {
                mDeviceCountTextView.setVisibility(View.VISIBLE);

            } else {
                mDeviceCountTextView.setVisibility(View.GONE);
            }

        } else {
            mDeviceCountTextView.setText(itemCount + " " + getString(R.string.device_found));
            if (mSearchEditText.getVisibility() == View.GONE) {
                mDeviceCountTextView.setVisibility(View.VISIBLE);

            } else {
                mDeviceCountTextView.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.startStopView:
                if (mBluetoothAdapter.isEnabled()) {
                    switchView();
                } else {
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag(DeviceScanFragment.class.getName());
                    if (fragment != null && fragment
                            instanceof DeviceScanFragment && fragment.isVisible()) {
                        ((DeviceScanFragment) fragment).showSnackBar();
                    }

                }
                break;
            case R.id.findIconImageView:

                break;
        }
      /*  if (view.getId() == mStartStopView.getId()) {

        }*/

    }

    /**
     *
     */
    private void switchView() {


        Fragment deviceScanFragment = getSupportFragmentManager().findFragmentByTag(DeviceScanFragment.class.getName());

        String s = mStartStopView.getText().toString();

        if (s.equalsIgnoreCase(getString(R.string.start_scan))) {
            if (deviceScanFragment != null && deviceScanFragment instanceof DeviceScanFragment && deviceScanFragment.isVisible()) {
                ((DeviceScanFragment) deviceScanFragment).startProgress();
            }
            setCosmeticChanges(getString(R.string.stop_scan));
        } else {
            if (deviceScanFragment != null && deviceScanFragment instanceof DeviceScanFragment && deviceScanFragment.isVisible()) {
                ((DeviceScanFragment) deviceScanFragment).stopLeScan();
            }
            setCosmeticChanges(getString(R.string.start_scan));

        }

    }

    /**
     * @param label
     */
    public void setCosmeticChanges(String label) {
        mStartStopView.setText(label);
        // mProgressBarSmall.setIndeterminate(inditerminate);
        //mProgressBarSmall.setVisibility(visibility);
    }

    /**
     * @param inditerminate
     * @param visibility
     */
    public void setScanProgressbar(boolean inditerminate, int visibility) {
        //mStartStopView.setText(label);
        mProgressBarSmall.setIndeterminate(inditerminate);
        mProgressBarSmall.setVisibility(visibility);
    }


    //    @Override
//    public void leDeviceDetected(BluetoothDevice bluetoothDevice) {
//        Log.v("DEVICE DETECTED",""+bluetoothDevice.getName());
//    }
    public void setMenuVisibility(boolean visibility) {
        mSearchMenuItem.setVisible(visibility);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
//        final int DRAWABLE_LEFT = 0;
//        final int DRAWABLE_TOP = 1;
        final int DRAWABLE_RIGHT = 2;
//        final int DRAWABLE_BOTTOM = 3;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (event.getRawX() >= (mSearchEditText.getRight() - mSearchEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                // your action here
                mSearchEditText.getText().clear();
                setHideSoftKeyboard(mSearchEditText);
                disableComponents(false);
                setScanProgressbar(false, View.GONE);
                return true;
            }
        }
        return false;
    }

    public void setonTexatChangeListener(OnTextListener textListener) {
        this.textListener = textListener;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        textListener.onTextChanged(charSequence.toString(), count);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    /**
     * Hide soft input keyboard
     *
     * @param editText
     */
    private void setHideSoftKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        if (!drawerClosed) {
            //mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Fragment deviceScanFragment = getSupportFragmentManager().findFragmentByTag(DeviceScanFragment.class.getName());
            if (deviceScanFragment != null && deviceScanFragment instanceof DeviceScanFragment && deviceScanFragment.isVisible()) {
                ((DeviceScanFragment) deviceScanFragment).backButtonPressed = true;
            }
            Intent gattServiceIntent = new Intent(this, BLEConnection.class);
            BLEConnection.closeGattServer();
            stopService(gattServiceIntent);
            finish();
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        this.drawerClosed = false;
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        this.drawerClosed = true;
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    protected void onDestroy() {
        Intent ansService = new Intent(this, AnsService.class);
        stopService(ansService);
        Intent gattServiceIntent = new Intent(this, BLEConnection.class);
        stopService(gattServiceIntent);
        Intent timeService = new Intent(this, TimeService.class);
        stopService(timeService);
        Intent sppService = new Intent(this, SPPService.class);
        stopService(sppService);
        Intent pasService = new Intent(this, PAService.class);
        stopService(pasService);
        super.onDestroy();
        // BLEConnection.closeGattServer();
    }

    public interface OnTextListener {
        void onTextChanged(String value, int count);
    }
}
