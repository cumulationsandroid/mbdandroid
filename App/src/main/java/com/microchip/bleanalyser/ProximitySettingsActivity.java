/*
 *
 *
 *     ProximitySettingsActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.bleanalyser;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.ImmediateAlertService;
import com.microchip.utils.AppUtils;
import com.microchip.views.RangeSeekBar;

/**
 * Created by
 */
public class ProximitySettingsActivity extends BLEBaseActivity implements RadioGroup.OnCheckedChangeListener{
    // BLEDataReceiver.LeCharacteristicReadStatus {

    private final static String TAG = ProximitySettingsActivity.class.getSimpleName();
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    BluetoothGattService mBluetoothGattService;
    ImmediateAlertService mImmediateAlertService;
    // LinkLossService mLinkLossService;
    BLEApplication bleApplication;
    int mRangeBarMinValue = 30;
    int mRangeBarMaxValue = 120;
    LinearLayout mRangeBarParent;
    int mRangeBarSelectedMinValue = 70;
    int mRangeBarSelectedMaxValue = 90;
    private RadioGroup mLinkLossRadioGroup;
    private RadioGroup mDangerRadioGroup;
    private RadioGroup mMidRadioGroup;
    private RadioGroup mSafeRadioGroup;
    private ProgressDialog mProgressDialog;
    //    private RadioButton mLinkLossNoAlertButton;
//    private RadioButton mLinkLossMidAlertButton;
//    private RadioButton mLinkLossHieghAlertButton;
    private RadioButton mDangerNoAlertButton;
    private RadioButton mDangerMidAlertButton;
    private RadioButton mDangerHieghAlertButton;
    private RadioButton mMidNoAlertButton;
    private RadioButton mMidMidAlertButton;
    private RadioButton mMidHieghAlertButton;
    private RadioButton mSafeNoAlertButton;
    private RadioButton mSafeMidAlertButton;
    private RadioButton mSafeHieghAlertButton;
    private Boolean mImmediateAlert = false;
    private RangeSeekBar<Integer> seekBar = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        setContentView(R.layout.activity_proximity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
        mProgressDialog.setCancelable(false);
        mLinkLossRadioGroup = (RadioGroup) findViewById(R.id.linkLossRadioGroup);
        mDangerRadioGroup = (RadioGroup) findViewById(R.id.dangerRadioGroup);
        mMidRadioGroup = (RadioGroup) findViewById(R.id.midRadioGroup);
        mSafeRadioGroup = (RadioGroup) findViewById(R.id.safeRadioGroup);
        mRangeBarParent = (LinearLayout) findViewById(R.id.rangeParent);

        // create RangeSeekBar as Integer range between 20 and 75
        seekBar = new RangeSeekBar<Integer>(mRangeBarMinValue, mRangeBarMaxValue, this);
        int minValue = AppUtils.getIntSharedPreference(this, AppUtils.SAFE_MAX_VALUE);
        int maxValue = AppUtils.getIntSharedPreference(this, AppUtils.DANGER_MIN_VALUE);
        if (minValue != 0 && maxValue != 0) {
            mRangeBarSelectedMinValue = minValue;
            mRangeBarSelectedMaxValue = maxValue;
        }
        seekBar.setSelectedMinValue(mRangeBarSelectedMinValue);
        seekBar.setSelectedMaxValue(mRangeBarSelectedMaxValue);
        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                // handle changed range values
                mRangeBarSelectedMaxValue = maxValue;
                mRangeBarSelectedMinValue = minValue;
                SavingLinkLossValues();
            }
        });
        SavingLinkLossValues();
        mRangeBarParent.addView(seekBar);

        /**
         * Danger Alert
         */
        mDangerNoAlertButton = (RadioButton) findViewById(R.id.dangerNoAlertButton);
        mDangerMidAlertButton = (RadioButton) findViewById(R.id.dangerMidAlertButton);
        mDangerHieghAlertButton = (RadioButton) findViewById(R.id.dangerHieghAlertButton);

        /**
         * Mid Alert
         */
        mMidNoAlertButton = (RadioButton) findViewById(R.id.noMidAlertRadioButton);
        mMidMidAlertButton = (RadioButton) findViewById(R.id.midMidAlertRadioButton);
        mMidHieghAlertButton = (RadioButton) findViewById(R.id.hieghtMidAlertRadioButton);

        /**
         * Safe Alert
         */
        mSafeNoAlertButton = (RadioButton) findViewById(R.id.noSafeRadioButton);
        mSafeMidAlertButton = (RadioButton) findViewById(R.id.midSafeRadioButton);
        mSafeHieghAlertButton = (RadioButton) findViewById(R.id.heightSafeRadioButton);

        mLinkLossRadioGroup.setOnCheckedChangeListener(this);
        mDangerRadioGroup.setOnCheckedChangeListener(this);
        mMidRadioGroup.setOnCheckedChangeListener(this);
        mSafeRadioGroup.setOnCheckedChangeListener(this);
        //BLEDataReceiver.setOnDeviceDisconnectedListener(this);
        setConnectionListener(this);
        bleApplication = (BLEApplication) getApplication();
        mBluetoothGattService = bleApplication.getmBluetoothGattService();

        mImmediateAlertService = ImmediateAlertService.getInstance();
        mImmediateAlertService.setImmediateAlertService(mBluetoothGattService);


        try {
            if ((mBluetoothGattService.getUuid().toString()).equalsIgnoreCase(GattAttributes.IMMEDIATE_ALERT_SERVICE)) {
                mImmediateAlert = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
        setDefaultValues();
    }

    @Override
    protected void onResume() {
        super.onResume();
       // Boolean status = false;
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
    }

    private void bluetoothCheck() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
               goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
    }

    private void SavingLinkLossValues() {

        AppUtils.setIntSharedPreference(this, AppUtils.SAFE_MIN_VALUE, mRangeBarMinValue);
        AppUtils.setIntSharedPreference(this, AppUtils.SAFE_MAX_VALUE, mRangeBarSelectedMinValue);
        AppUtils.setIntSharedPreference(this, AppUtils.MILD_MIN_VALUE, mRangeBarSelectedMinValue);
        AppUtils.setIntSharedPreference(this, AppUtils.MILD_MAX_VALUE, mRangeBarSelectedMaxValue);
        AppUtils.setIntSharedPreference(this, AppUtils.DANGER_MIN_VALUE, mRangeBarSelectedMaxValue);
        AppUtils.setIntSharedPreference(this, AppUtils.DANGER_MAX_VALUE, mRangeBarMaxValue);


    }

    @Override
    protected void onPause() {
        super.onPause();
        float[] floats = seekBar.getprevMinMax();
        BLEApplication.PREV_MIN = floats[0];
        BLEApplication.PREV_MAX = floats[1];
    }



    private void setDefaultValues() {


        //Danger Alert

        String mDanger = AppUtils.getStringSharedPreference(this, AppUtils.DANGER);
        if (mDanger.length() > 0) {
            if (mDanger.equalsIgnoreCase(AppUtils.HIEGH_ALERT)) {
                mDangerHieghAlertButton.setChecked(true);
            } else if (mDanger.equalsIgnoreCase(AppUtils.MILD_ALERT)) {
                mDangerMidAlertButton.setChecked(true);
            } else if (mDanger.equalsIgnoreCase(AppUtils.NO_ALERT)) {
                mDangerNoAlertButton.setChecked(true);
            }
        } else {
            mDangerHieghAlertButton.setChecked(true);
        }
        // Mid Alert

        String mMid = AppUtils.getStringSharedPreference(this, AppUtils.MID);
        if (mMid.length() > 0) {
            if (mMid.equalsIgnoreCase(AppUtils.HIEGH_ALERT)) {
                mMidHieghAlertButton.setChecked(true);
            } else if (mMid.equalsIgnoreCase(AppUtils.MILD_ALERT)) {
                mMidMidAlertButton.setChecked(true);
            } else if (mMid.equalsIgnoreCase(AppUtils.NO_ALERT)) {
                mMidNoAlertButton.setChecked(true);
            }
        } else {
            mMidMidAlertButton.setChecked(true);

        }

        // Safe Alert

        String mSafe = AppUtils.getStringSharedPreference(this, AppUtils.SAFE);
        if (mSafe.length() > 0) {
            if (mSafe.equalsIgnoreCase(AppUtils.HIEGH_ALERT)) {
                mSafeHieghAlertButton.setChecked(true);
            } else if (mSafe.equalsIgnoreCase(AppUtils.MILD_ALERT)) {
                mSafeMidAlertButton.setChecked(true);
            } else if (mSafe.equalsIgnoreCase(AppUtils.NO_ALERT)) {
                mSafeNoAlertButton.setChecked(true);
            }
        } else {
            mSafeNoAlertButton.setChecked(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {


        if (radioGroup.getId() == mDangerRadioGroup.getId()) {
            setDangerValue(radioGroup.getCheckedRadioButtonId());
        } else if (radioGroup.getId() == mMidRadioGroup.getId()) {
            setMidValue(radioGroup.getCheckedRadioButtonId());
        } else if (radioGroup.getId() == mSafeRadioGroup.getId()) {
            setSafeValue(radioGroup.getCheckedRadioButtonId());
        }

    }


    /**
     * Method to convert hex to byteArray
     */
    private void alertImmediateValue(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertStringtoByte(trimmedByte);
            }
        }
        mImmediateAlertService.writeAlertLevelCharacteristic(valueByte);
    }

    /**
     * Convert the string to byte
     *
     * @param string
     * @return
     */
    private int convertStringtoByte(String string) {
        return Integer.parseInt(string, 16);
    }


    /**
     * set Danger {@link RadioButton} values to {@link android.content.SharedPreferences}
     *
     * @param checkedRadioButtonId
     */
    private void setDangerValue(int checkedRadioButtonId) {

        if (mDangerNoAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.DANGER, AppUtils.NO_ALERT);

        } else if (mDangerMidAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.DANGER, AppUtils.MILD_ALERT);

        } else if (mDangerHieghAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.DANGER, AppUtils.HIEGH_ALERT);

        }
    }

    /**
     * set Mid {@link RadioButton} values to {@link android.content.SharedPreferences}
     *
     * @param checkedRadioButtonId
     */
    private void setMidValue(int checkedRadioButtonId) {

        if (mMidNoAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.MID, AppUtils.NO_ALERT);

        } else if (mMidMidAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.MID, AppUtils.MILD_ALERT);

        } else if (mMidHieghAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.MID, AppUtils.HIEGH_ALERT);

        }

    }

    /**
     * set Safe {@link RadioButton} values to {@link android.content.SharedPreferences}
     *
     * @param checkedRadioButtonId
     */
    private void setSafeValue(int checkedRadioButtonId) {

        if (mSafeNoAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.SAFE, AppUtils.NO_ALERT);

        } else if (mSafeMidAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.SAFE, AppUtils.MILD_ALERT);

        } else if (mSafeHieghAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.SAFE, AppUtils.HIEGH_ALERT);

        }

    }

    @Override
    public void onLeDeviceConnected(String deviceName) {
        try{
            mReconnectionAlert.dismissAlert();
            bluetoothCheck();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onLeDeviceDisconnected() {
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            try {
                mReconnectionAlert.showAlert();
            } catch (Exception e) {
                e.printStackTrace();
            }
            BLEConnection.autoConnect(this);
          }
    }

    private void goToHome() {
        //Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);


    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
