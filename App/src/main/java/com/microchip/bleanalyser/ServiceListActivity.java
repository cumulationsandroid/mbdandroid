/*
 *
 *
 *     ServiceListActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.bleanalyser;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.adapters.ServiceListRecycleViewAdapter;
import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic1Indicate;
import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic3Read;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.OTAUService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUCommandGetDeviceInformation;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.atmel.blecommunicator.com.atmel.otau.fileread.FileReader;
import com.microchip.profilescreens.AlertNotification;
import com.microchip.profilescreens.BatteryService;
import com.microchip.profilescreens.BloodPressure;
import com.microchip.profilescreens.CurrentTime;
import com.microchip.profilescreens.DeviceInformation;
import com.microchip.profilescreens.FindMe;
import com.microchip.profilescreens.HealthThermometer;
import com.microchip.profilescreens.HeartRate;
import com.microchip.profilescreens.ImmediateAlert;
import com.microchip.profilescreens.LinkLossAlert;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.profilescreens.PhoneAlert;
import com.microchip.profilescreens.ScanParameter;
import com.microchip.profilescreens.SerialPort;
import com.microchip.profilescreens.TxPower;
import com.microchip.profilescreens.gattdb.GattDbService;
import com.microchip.services.AnsService;
import com.microchip.services.PAService;
import com.microchip.services.SPPService;
import com.microchip.services.TimeService;
import com.microchip.utils.AppUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ServiceListActivity extends BLEBaseActivity implements
        ServiceListRecycleViewAdapter.OnItemClickListener, OTAUManager.OTAUProgressInfoListener {
    private final static String TAG = ServiceListActivity.class.getSimpleName();
    // UUID key
    private static final String LIST_UUID = "UUID";
    public static boolean otaAvailableFlag = false;
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    //GATT DB
    static ArrayList<HashMap<String, BluetoothGattService>> mGattServiceData =
            new ArrayList<HashMap<String, BluetoothGattService>>();
    private static ArrayList<HashMap<String, BluetoothGattService>> mGattdbServiceData =
            new ArrayList<HashMap<String, BluetoothGattService>>();
    public RecyclerView mRecyclerView;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    //    ListView mServiceList;
    BLEApplication bleApplication;
    LinearLayoutManager mLayoutManager;
    ServiceListRecycleViewAdapter adapter;
    AppConstants.ADV_CATEGORY mGattProfile = AppConstants.ADV_CATEGORY.DEFAULT;
    private List<BluetoothGattService> mBluetoothGattServices = new ArrayList<>();
    private Toolbar mToolbar = null;
    private boolean mHtpDetected = false;
    private boolean mIsFromFindMe = false;
    private boolean mIsTablet = false;
    private boolean mProximityDetected = false;
    private Handler mHandler = null;
    private Runnable mHandlerTask = null;
    private Handler mBondHandler = null;
    private Runnable mBondHandlerTask = null;
    private boolean mPairProgress = false;
    private boolean mPairDone = false;
    private boolean mPairFailed = false;
    private boolean mReconnectionRequired = false;
    private boolean mServerReconnection = false;
    private boolean mAnsLaunched = true;
    private boolean mBackPressed = false;
    private boolean mAutoConnectTriggered = false;
    private boolean isSerialPort = false;
    private ProgressDialog mDisconnectProgressDialog, mProgressDialog;
    private Handler mDisconnectionTimeoutHandler;
    private long DISCONNECTION_TIME_OUT = 5000;
    private long BLUETOOTH_TIME_OUT = 3000;
    private long BLUETOOTHON_TIME_OUT = 4000;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGattService mBluetoothGattService, mBluetoothOTAGATTService;
    private OTAUService mOTAUService;
    private Handler handler = new Handler();
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;
    private ProgressDialog mUpdateCheckProgressDialog;
    private OTAUManager otauManager;
    private List<BluetoothGattService> mBluetoothGattDBServices = new ArrayList<>();
    /**
     * This runnable for when the device is not connected and progress is showing concurrently.
     */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (mPairAlert != null) {
                    if (mPairAlert.isShowing()) {
                        mPairAlert.setMessage("Disconnecting ...");
                        if (mBluetoothAdapter != null) {
                            mBluetoothAdapter.disable();

                        }

                        //   if (Build.VERSION_CODES.LOLLIPOP_MR1 == Build.VERSION.SDK_INT) {
                     /*putting delay for after disable the Bluetooth */
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                goToHomeAfterRestartingBT();

                            }
                        }, BLUETOOTH_TIME_OUT);

                        // }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bleApplication = (BLEApplication) getApplication();
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            mIsTablet = true;
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            mIsTablet = false;
        }
        setContentView(R.layout.activity_service_list);
//        mServiceList = (ListView) findViewById(R.id.services_list);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mUpdateCheckProgressDialog = new ProgressDialog(ServiceListActivity.this);
        mUpdateCheckProgressDialog.setCancelable(false);
        mProgressDialog = new ProgressDialog(ServiceListActivity.this);
        mProgressDialog.setCancelable(false);
        mDisconnectProgressDialog = new ProgressDialog(ServiceListActivity.this);
        mDisconnectProgressDialog.setCancelable(false);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        //mRecyclerView.setItemAnimator(new FadeInAnimator());
        otauManager = OTAUManager.getInstance();
        otauManager.otauProgressInfoListener = this;
        Utils.setBooleanSharedPreference(ServiceListActivity.this, Constants.OTAU_PAUSE_STATUS_NAME_KEY, false);

        if (getIntent() != null) {
            Bundle bundle = getIntent().getBundleExtra(AppConstants.GATTSERVER_PROFILES);
            mGattProfile = (AppConstants.ADV_CATEGORY) bundle.getSerializable(AppConstants.GATTSERVER_PROFILES);
            // getIntent().getIntExtra(AppConstants.GATTSERVER_PROFILES, gattProfile);
            switch (mGattProfile != null ? mGattProfile : AppConstants.ADV_CATEGORY.DEFAULT) {
                case DEFAULT:
                    BLEConnection.discoverServices();
                    break;

                case SERIAL_PORT:
                    isSerialPort = true;
                    BLEConnection.discoverServices();
//                    List<BluetoothGattService> spp = bleApplication.getAdvertsingList();
//                    addCustomServiceToList(spp);
                    break;
                case PHONE_ALERT:
                case TIME:
                case ANS:
                    mReconnectionRequired = true;
                    mServerReconnection = true;
                    /*
                    if(BLEConnection.getmBondstate()==10){
                        BLEConnection.pairDevice();
                    }*/
                    List<BluetoothGattService> serviceList = bleApplication.getAdvertsingList();
                    addCustomServiceToList(serviceList);
                    break;
                default:
                    break;
            }

        }

        bleApplication = (BLEApplication) getApplication();

        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
        BLEConnection.needServiceDiscover = false;
    }

    @Override
    protected void onResume() {
        boolean status = false;
        super.onResume();
        Log.e("ServiceLIst", "onResume");
        setConnectionListener(ServiceListActivity.this);
        otauManager.otauProgressInfoListener = this;
        //setGattServerListener(ServiceListActivity.this);
        bluetoothCheck();
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (mGattProfile) {
            case DEFAULT:
            case SERIAL_PORT:
                status = BLEConnection.getmConnectionState() != BLEConnection.STATE_DISCONNECTED;
                break;
            case PHONE_ALERT:
            case TIME:
            case ANS: {
                status = BLEConnection.getmServerConnectionState() != BLEConnection.STATE_DISCONNECTED;
                if (status) {
                    if (mGattProfile == AppConstants.ADV_CATEGORY.ANS) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                AnsService.checkUnreadSmsMissedCall();
                            }
                        }).start();
                    }
                }
                break;
            }
            default:
                break;
        }
        if (BLEConnection.needServiceDiscover && adapter != null) {
            mBluetoothGattServices.clear();
            mBluetoothGattDBServices.clear();
            adapter.notifyDataSetChanged();
            BLEConnection.discoverServices();
            // BLEConnection.autoConnect(getApplicationContext());
            BLEConnection.needServiceDiscover = false;
        }
        if (!status) {
            onLeDeviceDisconnected();
        }
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (otauManager.mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }

    private void bluetoothCheck() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                goToHome();
            }
        }
    }

    private void goToHome() {
        Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }
    /**
     * Prepare GATTServices data.
     *
     *
     */
    private void prepareGattData() {
        boolean mGattSet = false;
        if (mBluetoothGattServices == null)
            return;
        // Clear all array list before entering values.
        mGattServiceData.clear();
        mBluetoothGattDBServices.clear();
        BluetoothGattService gattDBService = null;
        // Loops through available GATT Services.
        for (BluetoothGattService gattService : mBluetoothGattServices) {
            HashMap<String, BluetoothGattService> currentServiceData = new HashMap<String, BluetoothGattService>();
            UUID uuid = gattService.getUuid();
            if (uuid.equals(UUIDDatabase.UUID_GENERIC_ACCESS_SERVICE)
                    || uuid.equals(UUIDDatabase.UUID_GENERIC_ATTRIBUTE_SERVICE)) {
                currentServiceData.put(LIST_UUID, gattService);

                mGattdbServiceData.add(currentServiceData);
                if (!mGattSet) {
                    mGattSet = true;
                    mGattServiceData.add(currentServiceData);
                    gattDBService=gattService;
                    //mBluetoothGattDBServices.add(gattService);
                }
            } else {
                currentServiceData.put(LIST_UUID, gattService);
                mGattServiceData.add(currentServiceData);
                mBluetoothGattDBServices.add(gattService);
            }
        }
        bleApplication.setGattServiceMasterData(mGattdbServiceData);
        mBluetoothGattDBServices.add(gattDBService);
        if (mGattdbServiceData.size() > 0) {
            adapter = new ServiceListRecycleViewAdapter(mBluetoothGattDBServices, this);
            adapter.setOnItemClickLisener(this);
            mRecyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            Log.v(TAG, "Service discovery success");
        } else {
            //mProgressDialog.dismiss();
            //showNoServiceDiscoverAlert();
        }
    }
    @Override
    public void onLeServiceDiscovered(boolean status) {
        Log.e("onServiceDiscoveryStat", "" + status);
        if (status) {

//            if (adapter != null) {
//                adapter.notifyDataSetChanged();
//            }
            List<BluetoothGattService> tempServices = BLEConnection.getSupportedGattServices();
            mBluetoothGattServices.clear();
            // Loops through available GATT Services
            for (BluetoothGattService gattService : tempServices) {
                if (gattService.getUuid().toString()
                        .equalsIgnoreCase(GattAttributes.GENERIC_ACCESS_SERVICE)
                        && gattService.getUuid().toString()
                        .equalsIgnoreCase(GattAttributes.GENERIC_ATTRIBUTE_SERVICE)) {
                    Log.v(TAG, "" + gattService.getUuid());
                        mBluetoothGattServices.add(gattService);
                } else if (gattService.getUuid().toString()
                        .equalsIgnoreCase(GattAttributes.OTAU_SERVICE)) {
                    bleApplication.setBluetoothOTAGattService(gattService);
                    for (BluetoothGattCharacteristic mGattCharacteristic : gattService.getCharacteristics()) {
                        switch (mGattCharacteristic.getUuid().toString()) {
                            case GattAttributes.OTAU_CHARACTERISTIC_1_INDICATE:
                                OTAUCharacteristic1Indicate mOTAUCharacteristicIndicate =
                                        OTAUCharacteristic1Indicate.getInstance();
                                mOTAUCharacteristicIndicate.setOTAUCharacteristic1Indicate(mGattCharacteristic);
                                otaAvailableFlag = true;
                            case GattAttributes.OTAU_CHARACTERISTIC_2_WRITE:
                                OTAUCharacteristic2Write mOTAUCharacteristicWrite =
                                        OTAUCharacteristic2Write.getInstance();
                                mOTAUCharacteristicWrite.setOTAUCharacteristic2Write(mGattCharacteristic);
                            case GattAttributes.OTAU_CHARACTERISTIC_3_READ:
                                OTAUCharacteristic3Read mOTAUCharacteristicRead =
                                        OTAUCharacteristic3Read.getInstance();
                                mOTAUCharacteristicRead.setOTAUCharacteristic3Read(mGattCharacteristic);
                        }
                    }
                } else {
                    Log.v(TAG, "" + gattService.getUuid());
                    mBluetoothGattServices.add(gattService);
                }
            }
            prepareGattData();
            int size = 0;
            if (mBluetoothGattServices != null)
                size = mBluetoothGattServices.size();
            for (int i = 0; i < size; i++) {
                if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.HEALTH_TEMP_SERVICE)) {
                    mHtpDetected = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.IMMEDIATE_ALERT_SERVICE)) {
                    if (!mProximityDetected) {
                        mIsFromFindMe = true;
                    }
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.LINK_LOSS_SERVICE)) {
                    mProximityDetected = true;
                    mReconnectionRequired = true;
                    if (mIsFromFindMe)
                        mIsFromFindMe = false;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.TRANSMISSION_POWER_SERVICE)) {
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.PHONE_ALERT_STATUS_SERVICE)) {
                    mReconnectionRequired = true;
                    mServerReconnection = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.PHONE_ALERT_CONTROL_POINT)) {
                    mReconnectionRequired = true;
                    mServerReconnection = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.PHONE_ALERT_RINGER_SETTINGS)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.PHONE_ALERT_STATUS)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.CURRENT_TIME_SERVICE)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.CURRENT_TIME)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.DST_TIME)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.REFERENCE_TIME_UPDATE_SERVICE)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.ALERT_NOTIFICATION_SERVICE)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.ANS_NEW_ALERT)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.ANS_UNREAD_ALERT_STATUS)) {
                    mServerReconnection = true;
                    mReconnectionRequired = true;
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.SERIAL_PORT_END_POINT)) {
                    isSerialPort = true;
                    bleApplication.setmBluetoothGattService(mBluetoothGattServices.get(i));
                    mReconnectionRequired = true;
                    SPPService.initialize();
                } else if ((mBluetoothGattServices.get(i).getUuid().toString()).equalsIgnoreCase(GattAttributes.SERIAL_PORT)) {
                    isSerialPort = true;
                    bleApplication.setmBluetoothGattService(mBluetoothGattServices.get(i));
                    mReconnectionRequired = true;
                    SPPService.initialize();
                }
            }
            try {
                mReconnectionAlert.dismissAlert();
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }

        } else {
            finish();
        }

        if (otaAvailableFlag) {
            invalidateOptionsMenu();
            OTAUManager.getInstance().startOTAU();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mUpdateCheckProgressDialog.setMessage(getString(R.string.checking_update));
                    mUpdateCheckProgressDialog.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mUpdateCheckProgressDialog.isShowing()) {
                                mUpdateCheckProgressDialog.dismiss();
                            }
                        }
                    }, 5000);
                }
            });
        }
    }

    public void addCustomServiceToList(List<BluetoothGattService> serviceList) {
        //mBluetoothGattServices.add(serviceList);
        adapter = new ServiceListRecycleViewAdapter(serviceList, this);
        adapter.setOnItemClickLisener(this);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {

        /**
         * Check for OTAU Upgrade
         *
         */
        //Log.e(TAG, "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mUpdateCheckProgressDialog.isShowing()) {
                    mUpdateCheckProgressDialog.dismiss();
                }
            }
        });

        if (status.equalsIgnoreCase("initialize ota")) {
            initialiseOTAUSevices();
        } else if (status.equalsIgnoreCase("Resume and Update Available")) {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ServiceListActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Pending Update");
                    builder.setMessage("An Update with firmware version " + OTAUCommandGetDeviceInformation.PENDING_FIRMWARE_VERSION_MAJOR + "." + OTAUCommandGetDeviceInformation.PENDING_FIRMWARE_VERSION_MINOR
                            + "(" + OTAUCommandGetDeviceInformation.PENDING_FIRMWARE_VERSION_BUILD + ")" + " is pending. Please choose an action.");
                    builder.setPositiveButton("RESUME",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    OTAUManager.getInstance().startResumeUpdate();
                                    AppUtils.setIntSharedPreference(ServiceListActivity.this, "PREF_OTAU_PROGRESS", 0);
                                    Intent otauIntent = new Intent(ServiceListActivity.this, OTAUServiceStatus.class);
                                    startActivity(otauIntent);
                                }
                            });
                    builder.setNegativeButton("UPGRADE to " + FileReader.mFile.fwMajor + "." + FileReader.mFile.fwMinor + "(" + FileReader.mFile.fwBuild + ")",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    OTAUManager.getInstance().mOTAUOngoingFlag = false;
                                    AppUtils.setIntSharedPreference(ServiceListActivity.this, "PREF_OTAU_PROGRESS", 0);
                                    OTAUManager.getInstance().startOTAUUpdate();
                                    Intent otauIntent = new Intent(ServiceListActivity.this, OTAUServiceStatus.class);
                                    startActivity(otauIntent);
                                }
                            });
                    builder.setNeutralButton("CANCEL",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                }
            });
        } else if (status.equalsIgnoreCase("Update Available")) {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ServiceListActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Firmware Update available!");
                    builder.setMessage("Update firmware from " + OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MAJOR + "." + OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MINOR + "(" + OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_BUILD + ")"
                            + " to " + FileReader.mFile.fwMajor + "." + FileReader.mFile.fwMinor + "(" + FileReader.mFile.fwBuild + ")");
                    builder.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    OTAUManager.getInstance().startOTAUUpdate();
                                    AppUtils.setIntSharedPreference(ServiceListActivity.this, "PREF_OTAU_PROGRESS", 0);
                                    Intent otauIntent = new Intent(ServiceListActivity.this, OTAUServiceStatus.class);
                                    startActivity(otauIntent);
                                }
                            });
                    builder.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    OTAUManager.getInstance().mOTAUOngoingFlag = false;
                                    AppUtils.setIntSharedPreference(ServiceListActivity.this, "PREF_OTAU_PROGRESS", 0);
                                }
                            });
                    builder.show();
                }
            });
        } else if (status.equalsIgnoreCase("Resume Available")) {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ServiceListActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Resume Update!");
                    builder.setMessage("Would you like to resume the pending update?");
                    builder.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Utils.setBooleanSharedPreference(ServiceListActivity.this, Constants.OTAU_PAUSE_STATUS_NAME_KEY, false);
                                    OTAUManager.getInstance().startResumeUpdate();
                                    Intent otauIntent = new Intent(ServiceListActivity.this, OTAUServiceStatus.class);
                                    startActivity(otauIntent);
                                }
                            });
                    builder.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Utils.setBooleanSharedPreference(ServiceListActivity.this, Constants.OTAU_PAUSE_STATUS_NAME_KEY, false);
                                    AppUtils.setIntSharedPreference(ServiceListActivity.this, "PREF_OTAU_PROGRESS", 0);
                                    //OTAUManager.getInstance().mOTAUOngoingFlag = false;
                                }
                            });
                    builder.show();
                }
            });
        } else if (!status.equalsIgnoreCase("No Update")) {
            this.mOTAUStatusText = status;
            this.mOTAUProgress = ((int) (progress * 100));
            this.runOnUiThread(new Runnable() {
                public void run() {
                    mOtauPercentage.setText(mOTAUProgress + " %");
                    mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
                    AppUtils.setIntSharedPreference(ServiceListActivity.this, "PREF_OTAU_PROGRESS", mOTAUProgress);
                }
            });
        }
    }

    /**
     * OTAU
     */
    public void initialiseOTAUSevices() {
        bleApplication = (BLEApplication) getApplication();
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mBluetoothOTAGATTService = bleApplication.getBluetoothOTAGattService();
        mOTAUService = new OTAUService(mBluetoothOTAGATTService);
        bleApplication.setOTAUService(mOTAUService);
        mOTAUService = bleApplication.getOTAUService();
        //Enable indication for OTAU Characgeristic
        mOTAUService.getOTAUCharacteristic1Indicate();
        mOTAUService.startIndicateOTAUCharacteristic();
    }

    @Override
    public void onItemClicked(BluetoothGattService service, int pos) {
        if (service.getUuid() != null) {
            switch (service.getUuid().toString()) {
                case GattAttributes.HEALTH_TEMP_SERVICE:
                    Log.v(TAG, "Health service ");
                    bleApplication.setmBluetoothGattService(service);
                    Intent HTPIntent = new Intent(ServiceListActivity.this, HealthThermometer.class);
                    startActivity(HTPIntent);
                    break;
                case GattAttributes.IMMEDIATE_ALERT_SERVICE:
                    Log.v(TAG, "Immediate service ");
                    bleApplication.setmBluetoothGattService(service);
                    if (mIsFromFindMe) {
                        Intent fineMeIntent = new Intent(ServiceListActivity.this, FindMe.class);
                        startActivity(fineMeIntent);
                    } else {
                        Intent IMPIntent = new Intent(ServiceListActivity.this, ImmediateAlert.class);
                        startActivity(IMPIntent);
                    }

                    break;
                case GattAttributes.LINK_LOSS_SERVICE:
                    Log.v(TAG, "Link loss service ");
                    bleApplication.setmBluetoothGattService(service);
                    Intent LLPIntent = new Intent(ServiceListActivity.this, LinkLossAlert.class);
                    startActivity(LLPIntent);
                    break;
                case GattAttributes.TRANSMISSION_POWER_SERVICE:
                    Log.v(TAG, "Transmission power service ");
                    bleApplication.setmBluetoothGattService(service);
                    Intent TXPIntent = new Intent(ServiceListActivity.this, TxPower.class);
                    startActivity(TXPIntent);
                    break;
                case GattAttributes.DEVICE_INFORMATION_SERVICE:
                    Log.v(TAG, "Transmission power service ");
                    bleApplication.setmBluetoothGattService(service);
                    Intent deviceIntent = new Intent(ServiceListActivity.this, DeviceInformation.class);
                    startActivity(deviceIntent);
                    break;
                case GattAttributes.HEART_RATE_SERVICE:
                    Log.v(TAG, "Heart Rate service ");
                    bleApplication.setmBluetoothGattService(service);
                    Intent heartRateIntent = new Intent(ServiceListActivity.this, HeartRate.class);
                    startActivity(heartRateIntent);
                    break;
                case GattAttributes.BLOOD_PRESSURE_SERVICE:
                    Log.v(TAG, "Blood Pressure service ");
                    bleApplication.setmBluetoothGattService(service);
                    Intent bloodPressureIntent = new Intent(ServiceListActivity.this, BloodPressure.class);
                    startActivity(bloodPressureIntent);
                    break;
                case GattAttributes.SCAN_PARAMETERS_SERVICE:
                    bleApplication.setmBluetoothGattService(service);
                    Intent scanParamIntent = new Intent(ServiceListActivity.this, ScanParameter.class);
                    startActivity(scanParamIntent);
                    break;
                case GattAttributes.BATTERY_SERVICE:
                    bleApplication.setmBluetoothGattService(service);
                    Intent batteryLevel = new Intent(ServiceListActivity.this, BatteryService.class);
                    startActivity(batteryLevel);
                    break;
                case GattAttributes.CURRENT_TIME_SERVICE:
                case GattAttributes.NEXT_DST_CHANGE_SERVICE:
                case GattAttributes.REFERENCE_TIME_UPDATE_SERVICE:
                    bleApplication.setmBluetoothGattService(service);
                    Intent time = new Intent(ServiceListActivity.this, CurrentTime.class);
                    startActivity(time);
                    break;
                case GattAttributes.ALERT_NOTIFICATION_SERVICE:
                    bleApplication.setBluetoothGattAdvService(service);
                    Intent ansIntent = new Intent(ServiceListActivity.this, AlertNotification.class);
                    ansIntent.putExtra(AppConstants.ANS_FIRST_TIME_LAUNCH, mAnsLaunched);
                    startActivity(ansIntent);
                    //mAnsLaunched = false;
                    break;
                case GattAttributes.PHONE_ALERT_STATUS_SERVICE:
                    bleApplication.setBluetoothGattAdvService(service);
                    Intent phoneAlertIntent = new Intent(ServiceListActivity.this, PhoneAlert.class);
                    startActivity(phoneAlertIntent);
                    break;
                case GattAttributes.SERIAL_PORT:
                    bleApplication.setmBluetoothGattService(service);
                    Intent serialPort = new Intent(ServiceListActivity.this, SerialPort.class);
                    startActivity(serialPort);
                    break;
                case GattAttributes.GENERIC_ACCESS_SERVICE:
                    if (bleApplication.getBluetoothOTAGattService() == null) {
                        bleApplication.setmBluetoothGattService(service);
                        Intent gattDb = new Intent(ServiceListActivity.this, GattDbService.class);
                        startActivity(gattDb);
                    } else {
                        this.runOnUiThread(new Runnable() {
                            public void run() {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(ServiceListActivity.this, R.style.AppCompatAlertDialogStyle);
                                builder.setTitle("OTAU Service detected!");
                                builder.setMessage("Generic Information feature is disabled for OTAU service");
                                builder.setNeutralButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        });
                                builder.show();
                            }
                        });
                    }
                    break;
                default:
                    bleApplication.setmBluetoothGattService(service);
                    this.runOnUiThread(new Runnable() {
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ServiceListActivity.this, R.style.AppCompatAlertDialogStyle);
                            builder.setTitle("No UI available!");
                            builder.setMessage("Would you like to access Generic Information?");
                            builder.setPositiveButton("YES",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent defaultIntent = new Intent(ServiceListActivity.this, GattDbService.class);
                                            startActivity(defaultIntent);
                                        }
                                    });
                            builder.setNegativeButton("NO",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                            builder.show();
                        }
                    });
                    break;
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(ServiceListActivity.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(ServiceListActivity.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        if (otauManager.mOTAUOngoingFlag) {
            mOTAUProgressBar.setVisibility(View.VISIBLE);
            mOtauPercentage.setVisibility(View.VISIBLE);
        } else {
            mOtauPercentage.setVisibility(View.GONE);
            mOTAUProgressBar.setVisibility(View.GONE);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                backPressed();
                //finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        backPressed();
    }

    private void backPressed() {
        if (BLEConnection.getmBondstate() == BluetoothDevice.BOND_BONDING) {
            Toast.makeText(this, "Cancel pairing request or already cancelled please wait ...", Toast.LENGTH_SHORT).show();
        } else {
            mBackPressed = true;
            disconnection();
            // finish();
        }
    }

    private void disconnection() {
        if (mPairAlert != null) {
            mPairAlert.showAlert();
            mPairAlert.setMessage("Disconnecting ...");
            mDisconnectionTimeoutHandler = new Handler();
            mDisconnectionTimeoutHandler.postDelayed(runnable, DISCONNECTION_TIME_OUT);
        }
        switch (mGattProfile) {
            case DEFAULT:
            case SERIAL_PORT:
                BLEConnection.disconnect();
                break;
            case PHONE_ALERT:
            case TIME:
            case ANS:
                BLEConnection.disconnectGattServer();
                break;
            default:
                break;
        }
        unPairDevice();
        switch (mGattProfile) {
            case ANS:
                Intent ansService = new Intent(this, AnsService.class);
                stopService(ansService);
                break;
            case TIME:
                Intent timeService = new Intent(this, TimeService.class);
                stopService(timeService);
                break;
            case SERIAL_PORT:
                Intent sppService = new Intent(this, SPPService.class);
                stopService(sppService);
                break;
            case PHONE_ALERT:
                Intent pasService = new Intent(this, PAService.class);
                stopService(pasService);
                break;
            default:
                break;
        }
    }

    @Override
    public void onLeBluetoothStatusChanged() {
        Log.e("onLeBluetoothStatus", "onLeBluetoothStatusChanged");
        goToHomeAfterRestartingBT();
    }

    private void goToHomeAfterRestartingBT() {

        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        if (mBackPressed) {
            Log.e("waiting for handle", "handler");
            try {
                if (mDisconnectionTimeoutHandler != null && runnable != null) {
                    mDisconnectionTimeoutHandler.removeCallbacks(runnable);
                }
            } catch (Exception ignored) {

            }
            setGattListener(null);
            setConnectionListener(null);
            //setGattServerListener(null);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {

                    Log.e("inside handle", "handler");
                    try {
                        if (mPairAlert != null) {
                            mPairAlert.dismissAlert();
                        }
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    finish();
                    Toast.makeText(ServiceListActivity.this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();

                }
            }, BLUETOOTHON_TIME_OUT);

        }
    }

    @Override
    public void onLeDeviceDisconnected() {
        if (otauManager != null) {
            otauManager.clear();
        }

        Log.d("Device", "Disconnected called in ServiceList");
        if (!mBackPressed) {
            if (isBluetoothDisable()) {
                Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
                setConnectionListener(null);
                finish();
            } else {
                // if (BLEConnection.isBonded()) {
                if (mReconnectionRequired) {
                    try {
                        mReconnectionAlert.showAlert();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (BLEConnection.mReconnectionEnabled) {
                        if (mServerReconnection) {
                            BLEConnection.gattServerAutoConnect(this);
                            BLEConnection.mReconnectionEnabled = false;
                        } else {
                            if (mAutoConnectTriggered == false) {
                                if (isSerialPort) {
                                    BLEConnection.connectSPP(ServiceListActivity.this);
                                } else {

                                    BLEConnection.autoConnect(this);
                                }
                                //BLEConnection.mReconnectionEnabled = false;
                                // mAutoConnectTriggered = true;
                            }
                        }
                    }
                } else {
                    unPairDevice();
                    Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
                    setConnectionListener(null);
                    finish();
                }
            }
        } else {
            if (Build.VERSION_CODES.LOLLIPOP_MR1 != Build.VERSION.SDK_INT) {
                goToHomeAfterRestartingBT();
            }
        }
    }

    @Override
    public void onLeDeviceConnected(String deviceName) {
        try {

            if (isSerialPort) {
                BLEConnection.discoverServices();
            } else {
                BLEConnection.discoverServices();
                mReconnectionAlert.dismissAlert();
            }
            mAutoConnectTriggered = false;
            if (mGattProfile == AppConstants.ADV_CATEGORY.ANS) {
                AppUtils.setIntSharedPreference(this, AppUtils.ANS_SMS_UNREAD_COUNT, -1);
                AppUtils.setIntSharedPreference(this, AppUtils.ANS_MISSEDCALL_UNREAD_COUNT, -1);
                AnsService.mFirstTimeActivity = true;
            }
            bluetoothCheck();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }

    private void unPairDevice() {
        if (mHtpDetected && BLEConnection.isBonded()) {
            BLEConnection.unPairDevice();
        }
    }


    @Override
    public void onLePairingRequest() {
        try {

            mPairAlert.pairingInProgress();
            mPairAlert.showAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        try {
            mPairAlert.pairingDone();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (mPairAlert != null) {
                        mPairAlert.dismissAlert();

                    }
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mPairAlert.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mPairAlert != null && !mPairAlert.isShowing()) {
                mPairAlert.pairingInProgress();
                mPairAlert.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        super.onDestroy();
    }


}


