/*
 *
 *
 *     DeviceScanFragment.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.fragments;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.microchip.adapters.RecyclerViewBleDeviceAdapter;
import com.microchip.animations.FadeInAnimator;
import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Interfaces.BLEDetector;
import com.atmel.blecommunicator.com.atmel.Services.AlertNotificationService;
import com.atmel.blecommunicator.com.atmel.Services.CurrentTimeService;
import com.atmel.blecommunicator.com.atmel.Services.NextDstChangeService;
import com.atmel.blecommunicator.com.atmel.Services.PhoneAlertService;
import com.atmel.blecommunicator.com.atmel.Services.ReferenceTimeUpdateService;
import com.atmel.blecommunicator.com.atmel.Services.SerialPortService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.microchip.communicators.BLEDataReceiver;
import com.microchip.services.SPPService;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;
import com.microchip.services.AnsService;
import com.microchip.services.PAService;
import com.microchip.services.TimeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;


public class DeviceScanFragment extends Fragment implements BLEDetector,
        BLEDataReceiver.BLEConnection, RecyclerViewBleDeviceAdapter.OnDeviceConnectionListener,
        Animation.AnimationListener, HomeScreen.OnTextListener {
    // Stops scanning after 5 seconds.
    private static final long SCAN_PERIOD = 5500;
    private static final int PERMISSION_REQUEST_CODE = 123;
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    private static Boolean mReScan = false;
    public RecyclerView mRecyclerView;
    public SwipeRefreshLayout mSwipeRefreshLayout;
    public boolean backButtonPressed = false;
    public String mAddress = null;
    public String mName = null;
    LinearLayoutManager mLayoutManager;
    boolean isBleSupported = false;
    boolean isLollipopDevice = false;
    RecyclerViewBleDeviceAdapter adapter;
    Animation mUPanimation, mDownAnimation;
    View scanLayout;
    View mCoordinatorLayoutView;
    PairingAlert mProgressDialog;
    Animation rotationAnimation;
    HomeScreen home;
    boolean isAnimationUp = true;
    AppConstants.ADV_CATEGORY category = AppConstants.ADV_CATEGORY.DEFAULT;
    int count = 0;
    boolean mPairRequestInitiated = false;
    String mDeviceName = "";
    private ImageView mAnimationImageView = null;
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<BluetoothDevice> mDevices;
    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {

            if (!(home.mSearchEditText.getVisibility() == View.VISIBLE)) {

                try {
                    home.setCosmeticChanges(getString(R.string.stop_scan));
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
                mDevices.clear();
                adapter.clearList();
                adapter.removeCustomList();
                adapter.notifyDataSetChanged();
                home.mDeviceCountTextView.setVisibility(View.GONE);
                scanStart();
                try {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            if (!backButtonPressed) {
                                mSwipeRefreshLayout.setRefreshing(false);
                                try {
                                    home.setCosmeticChanges(getString(R.string.start_scan));
                                } catch (Exception ignored) {
                                    ignored.printStackTrace();
                                }
                            }
                        }

                    }, SCAN_PERIOD);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }

    };
    private ArrayList<String> permissionList = new ArrayList<>();
    private List<ParcelUuid> mUuids;
    private BLEApplication bleApplication;
    private Handler mDeviceConnectionLossHandler;
    private boolean mPairProgress = false;
    private boolean mPairDone = false;
    private boolean mPairFailed = false;
    private Handler mHandler = null;
    private Runnable mHandlerTask = null;
    private boolean isFirstTime = false;
    private ProgressBar mProgressBar = null;
    private Handler mProgressBarHandler = null;
    private Timer mTimer = null;
    private TimerTask mTimerTask = null;
    private int i = 0;
    private long CONNECTION_TIME_OUT = 10000;
    private String TAG = DeviceScanFragment.class.getSimpleName();
    /**
     * This runnable for when the device is not connected and progress is showing concurrently.
     */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (mProgressDialog != null) {
                    if (mProgressDialog.isShowing()) {
                        Toast.makeText(getActivity(), "Unable to connect the device ..", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismissAlert();
                        BLEConnection.disconnect();
                    }
                    //scanStart();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private boolean restarted;

    public static boolean getRestartScanParameter() {
        return mReScan;
    }

    public static void setRestartScanParameter(boolean b) {
        mReScan = b;
    }

    /**
     * To start LEScan
     */
    public void scanStart() {
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            if (scanLayout.getVisibility() == View.GONE) {
                mSwipeRefreshLayout.setRefreshing(true);
            }
            if (isBleSupported) {
                if (isLollipopDevice) {
                    mDevices.clear();
                    adapter.clearList();
                    adapter.removeCustomList();
                    adapter.notifyDataSetChanged();
                    if (null != mBleLollipopScanner) {
                        if (home.mFilterSelected) {
                            if (home.UUIDs != null && home.UUIDs.length > 0) {
                                mBleLollipopScanner.startLEScanWithFilters(SCAN_PERIOD, home.UUIDs);
                            } else {
                                mBleLollipopScanner.startLEScan(SCAN_PERIOD);
                            }
                        } else {
                            mBleLollipopScanner.startLEScan(SCAN_PERIOD);
                        }
                    } else {
                        mBleLollipopScanner = BLELollipopScanner.getInstance(getActivity());
                        BLELollipopScanner.setScanListner(DeviceScanFragment.this);
                        if (home.mFilterSelected) {
                            if (home.UUIDs != null && home.UUIDs.length > 0) {
                                mBleLollipopScanner.startLEScanWithFilters(SCAN_PERIOD, home.UUIDs);
                            } else {
                                mBleLollipopScanner.startLEScan(SCAN_PERIOD);
                            }
                        } else {
                            mBleLollipopScanner.startLEScan(SCAN_PERIOD);
                        }
                    }
                } else {
                    mDevices.clear();
                    adapter.clearList();
                    adapter.removeCustomList();
                    adapter.notifyDataSetChanged();
                    if (null != mBleKitkatScanner) {
                        if (home.mFilterSelected) {
                            if (home.UUIDs != null && home.UUIDs.length > 0) {
                                mBleKitkatScanner.startLEScanWithFilter(SCAN_PERIOD, home.UUIDs);
                            } else {
                                mBleKitkatScanner.startLEScan(SCAN_PERIOD);
                            }
                        } else {
                            mBleKitkatScanner.startLEScan(SCAN_PERIOD);
                        }
                    } else {
                        mBleKitkatScanner = BLEKitKatScanner.getInstance(getActivity());
                        BLEKitKatScanner.setScanListner(DeviceScanFragment.this);
                        if (home.mFilterSelected) {
                            if (home.UUIDs != null && home.UUIDs.length > 0) {
                                mBleKitkatScanner.startLEScanWithFilter(SCAN_PERIOD, home.UUIDs);
                            } else {
                                mBleKitkatScanner.startLEScan(SCAN_PERIOD);
                            }
                        } else {
                            mBleKitkatScanner.startLEScan(SCAN_PERIOD);
                        }
                    }
                }
                home.setCosmeticChanges(getString(R.string.stop_scan));

            }
        } else {
            showSnackBar();
        }
    }

    public void stopLeScan() {
        if (scanLayout.getVisibility() == View.VISIBLE) {
            mAnimationImageView.clearAnimation();
        }
        if (isBleSupported) {
            if (isLollipopDevice) {
                if (null != mBleLollipopScanner) {

                    mBleLollipopScanner.stopLEScan();
                } else {
                    mBleLollipopScanner = BLELollipopScanner.getInstance(getActivity());
                    BLELollipopScanner.setScanListner(DeviceScanFragment.this);
                    mBleLollipopScanner.stopLEScan();
                }
            } else {
                if (null != mBleKitkatScanner) {
                    mDevices.clear();
                    adapter.clearList();
                    mBleKitkatScanner.stopLEScan();
                } else {
                    mBleKitkatScanner = BLEKitKatScanner.getInstance(getActivity());
                    BLEKitKatScanner.setScanListner(DeviceScanFragment.this);
                    mBleKitkatScanner.stopLEScan();
                }
            }


        }
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        home.setCosmeticChanges(getString(R.string.start_scan));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bleApplication = (BLEApplication) getActivity().getApplication();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.device_list,
                container, false);
        home = (HomeScreen) getActivity();
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.device_recycler_view);
        scanLayout = rootView.findViewById(R.id.scaniningLayout);
        mAnimationImageView = (ImageView) rootView.findViewById(R.id.animationImageView);
        mCoordinatorLayoutView = scanLayout.findViewById(R.id.snackbarPosition);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);

        mDevices = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(getActivity());

//        if (home.mIsTablet) {
//            mRecyclerView.addItemDecoration(new MarginDecoration(getActivity()));
//            mRecyclerView.setHasFixedSize(true);
//        } else {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
//        }


        mRecyclerView.setItemAnimator(new FadeInAnimator());
        home.setonTexatChangeListener(this);

        mRecyclerView.setVisibility(View.GONE);
        mUPanimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out);

        mUPanimation.setAnimationListener(this);
        mDownAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);

        mDownAnimation.setAnimationListener(this);
        isAnimationUp = true;
        adapter = new RecyclerViewBleDeviceAdapter(mDevices,
                getActivity());
        adapter.setonDeviceConnectedLisetener(this);
        mRecyclerView.setAdapter(adapter);


        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);
        mProgressDialog = new PairingAlert(getActivity());

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mSwipeRefreshLayout.setVisibility(View.GONE);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(getActivity());
            BLELollipopScanner.setScanListner(this);
            isLollipopDevice = true;
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(getActivity());
            BLEKitKatScanner.setScanListner(this);
            isLollipopDevice = false;
        }


        if (isLollipopDevice) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                showSnackBar();
            } else {
                if (mBleLollipopScanner.checkBLESupport()) {
                    isBleSupported = true;
                    mDevices.clear();
                    adapter.clearList();
                    adapter.removeCustomList();
                } else {
                    isBleSupported = false;

                }
            }
        } else {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                showSnackBar();
            } else {
                if (mBleKitkatScanner.checkBLESupport()) {
                    isBleSupported = true;
                    mDevices.clear();
                    adapter.clearList();
                    adapter.removeCustomList();
                } else {
                    isBleSupported = false;

                }
            }
        }

        Bundle bundle = getArguments();
        restarted = bundle.getBoolean(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE);

        return rootView;
    }

    /**
     * Snackbar UI to enable Bluetooth if disabled
     */
    public void showSnackBar() {

        Snackbar.make(mCoordinatorLayoutView,
                getString(R.string.bluetooth_message), Snackbar.LENGTH_LONG).setAction(getString(R.string.turn_on),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mBluetoothAdapter.enable();
                    }
                }).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (scanLayout.getVisibility() != View.VISIBLE) {
            if (isLollipopDevice) {
                if (!mBleLollipopScanner.getBluetoothStatus()) {
                    onLeBluetoothStatusChanged();

                }
            } else {
                if (!mBleKitkatScanner.getBluetoothStatus()) {
                    onLeBluetoothStatusChanged();

                    // onLeBluetoothStatusChanged();
                }
            }
        }
        backButtonPressed = false;
        //  mPairDone=false;

        if (mBleKitkatScanner != null) {
            BLEKitKatScanner.setScanListner(this);
        }
        if (mBleLollipopScanner != null) {
            BLELollipopScanner.setScanListner(this);
        }
        if (restarted) {
            scanStart();
        }

     /*   if (mReScan) {
           // scanStart();
        }
        mReScan = false;*/
        AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.THERMOMETER_SELECTION_KEY, false);
        AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.HEART_RATE_SELECTION_KEY, false);
        AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.BLOOD_PRESSURE_SELECTION_KEY, false);
        AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.FIRST_TIME, true);
        AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.BATTERY_NOTIFY_STATUS, false);
        AppUtils.setIntSharedPreference(getActivity(), AppUtils.FINE_ME_SELECTION, 4);
        BLEDataReceiver.setBLEConnectionListener(this);
        //BLEDataReceiver.setGattServerListener(this);
        if (mPairRequestInitiated) {
            if (BLEConnection.getmServerConnectionState() == BLEConnection.STATE_DISCONNECTED) {

                try {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismissAlert();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (BLEConnection.isBonding() || BLEConnection.isBonded()) {
                try {
                    if (mProgressDialog != null)
                        mProgressDialog.showAlert();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * Services initialization done here
     *
     * @param
     */
    private void doServicesInitialization() {
        switch (category) {

            case TIME:
                BluetoothGattService service;
                CurrentTimeService currentTimeService = CurrentTimeService.getInstance();
                service = currentTimeService.initialize();
                if (service != null)
                    bleApplication.addAdvertisingService(service);
                break;
            case ANS:
                BluetoothGattService serviceANS;
                AlertNotificationService ansService = AlertNotificationService.getInstance();
                serviceANS = ansService.initialize();
                if (serviceANS != null)
                    bleApplication.addAdvertisingService(serviceANS);
                break;
            case PHONE_ALERT:

                PhoneAlertService alertService = PhoneAlertService.getInstance();
                BluetoothGattService phoneAlertBluetoothGattService = alertService.getBlueToothGattService();
                if (phoneAlertBluetoothGattService != null)
                    bleApplication.addAdvertisingService(phoneAlertBluetoothGattService);
                break;
            case SERIAL_PORT:
                BluetoothGattService SSPservice;
                SerialPortService sspService = SerialPortService.getInstance();
                SSPservice = sspService.initialize();
                if (SSPservice != null) {
                    bleApplication.addAdvertisingService(SSPservice);
                    ///bleApplication.setmBluetoothGattService(SSPservice);
                }
                break;
            default:
                break;

        }
    }

    @Override
    public void onLeDeviceConnected(final String deviceName) {

        mProgressDialog.showAlert();
        mDeviceName = deviceName;
        if (mDeviceConnectionLossHandler != null && runnable != null) {
            mDeviceConnectionLossHandler.removeCallbacks(runnable);
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (!mPairRequestInitiated) {
                        //notifyCustomSerialChat();
                        //  startService();
                        showMessage(deviceName);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1500);
    }


    private void showMessage(String deviceName) {
        try {
            mProgressDialog.setMessage(deviceName + " Connected successfully");
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    mProgressDialog.dismissAlert();
                    mPairDone = false;
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.GATTSERVER_PROFILES, category);
                    /*
                    Intent ansService = new Intent(getActivity(), com.atmel.profilescreens.AlertNotificationService.class);
                    getActivity().startService(ansService);*/
                    Intent serviceIntent = new Intent(getActivity(), ServiceListActivity.class);
                    serviceIntent.putExtra(AppConstants.GATTSERVER_PROFILES, bundle);
                    startActivity(serviceIntent);
                    backButtonPressed = true;
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void leDeviceDetected(final BluetoothDevice mDevice, final int rssi, final List<ParcelUuid> list) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (scanLayout.getVisibility() == View.VISIBLE && !isFirstTime) {
                    scanLayout.startAnimation(mUPanimation);
                    isFirstTime = true;
                    isAnimationUp = true;
                }
                adapter.addDevices(mDevice, rssi, list);
                int itemCount = adapter.getItemCount();
                FragmentActivity activity = getActivity();
                if (activity instanceof HomeScreen) {
                    HomeScreen homeScreen = (HomeScreen) getActivity();
                    homeScreen.setSmallProgress(itemCount);
                }
            }
        });


    }


    @Override
    public void leScanStopped() {

        home.setScanProgressbar(false, View.GONE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        home.setCosmeticChanges(getString(R.string.start_scan));
        home.mDeviceCountTextView.setVisibility(View.GONE);
        mAnimationImageView.clearAnimation();
        home.setSmallProgress(adapter.getItemCount());
    }

    @Override
    public void onDeviceConnected(String address, String name, List<ParcelUuid> uuidList) {
        if (mProgressDialog != null) {
            mProgressDialog.setMessage(getResources().getString(R.string.connecting_alert));
            mProgressDialog.showAlert();
        }
        mPairFailed = false;
        count = 0;
        mUuids = uuidList;

        mAddress = address;
        mName = name;
        category = AppConstants.ADV_CATEGORY.DEFAULT;
        BLEConnection.closeGattServer();
        category = addServiceToGattServer(uuidList);
        permissionCheckBasedOnCategory(category);
        if (permissionList.size() == 0) {
            doServicesInitialization();
            AppUtils.setIntSharedPreference(getActivity(), AppUtils.ANS_SMS_UNREAD_COUNT, -1);
            AppUtils.setIntSharedPreference(getActivity(), AppUtils.ANS_MISSEDCALL_UNREAD_COUNT, -1);
            AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.ANS_READ_NOTIFICATION_STATUS, false);
            AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.ANS_READ_NOTIFICATION_STATUS, false);
            AppUtils.setBooleanSharedPreference(getActivity(), Utils.PHONE_ALERT_STATUS, false);
            AppUtils.setIntSharedPreference(getActivity(), AppUtils.ANS_SMS_NEW_COUNT, 0);
            AppUtils.setStringSharedPreference(getActivity(), AppUtils.ANS_SMS_NEW_TEXT, null);
            AppUtils.setIntSharedPreference(getActivity(), AppUtils.ANS_MISSEDCALL_NEW_COUNT, 0);

            if (category == AppConstants.ADV_CATEGORY.DEFAULT) {
                BLEConnection.connect(address, name, getActivity());
                mDeviceConnectionLossHandler = new Handler();
                mDeviceConnectionLossHandler.postDelayed(runnable, CONNECTION_TIME_OUT);
            }
        } else {
            requestPermissions(
                    permissionList.toArray(new String[permissionList.size()]), PERMISSION_REQUEST_CODE);
        }
    }


    public void startProgress() {
        if (scanLayout.getVisibility() == View.VISIBLE) {
            scanStart();
            mAnimationImageView.setImageResource(R.drawable.bg_loader_white_img);
            rotationAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotation);
            mAnimationImageView.startAnimation(rotationAnimation);
        } else {
            scanStart();
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {


    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (isAnimationUp) {
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setOnRefreshListener(onRefreshListener);
            scanLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            HomeScreen homeScreen = (HomeScreen) getActivity();
            homeScreen.setMenuVisibility(true);
            homeScreen.setCosmeticChanges(getString(R.string.stop_scan));
            homeScreen.setScanProgressbar(true, View.VISIBLE);
        } else {
            isAnimationUp = true;
            mSwipeRefreshLayout.setVisibility(View.GONE);
            scanLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mAnimationImageView.clearAnimation();
            HomeScreen homeScreen = (HomeScreen) getActivity();
            homeScreen.setMenuVisibility(false);
            homeScreen.setCosmeticChanges(getString(R.string.start_scan));
            homeScreen.setScanProgressbar(false, View.GONE);
            homeScreen.setSmallProgress(0);
            mDevices.clear();
            adapter.clearList();
            adapter.removeCustomList();
        }

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    /**
     * Search Edittext code implementation here
     *
     * @param value
     * @param count
     */
    @Override
    public void onTextChanged(String value, int count) {
        home.setScanProgressbar(true, View.VISIBLE);
        adapter.search(value);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mBleKitkatScanner != null) {
            BLEKitKatScanner.setScanListner(null);
        }
        if (mBleLollipopScanner != null) {
            BLELollipopScanner.setScanListner(null);
        }
    }

    @Override
    public void onLeBluetoothStatusChanged() {


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                BlueToothOnorOFFUIChange();
            }
        }, 2000);

    }

    private void BlueToothOnorOFFUIChange() {
        if (isLollipopDevice) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isAnimationUp = false;
                        isFirstTime = false;
                        scanLayout.startAnimation(mDownAnimation);
                        showSnackBar();
                    }
                });
            } else {
                isBleSupported = mBleLollipopScanner.checkBLESupport();
            }
        } else {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isAnimationUp = false;
                        isFirstTime = false;
                        scanLayout.startAnimation(mDownAnimation);
                        showSnackBar();
                    }
                });
            } else {
                isBleSupported = mBleKitkatScanner.checkBLESupport();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BLEDataReceiver.setBLEConnectionListener(null);
        if (mHandler != null) {
            mHandler.removeCallbacks(mHandlerTask);
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {

        mPairRequestInitiated = true;
        try {
            if (mProgressDialog != null) {

                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.showAlert();
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();
                        if (mPairRequestInitiated) {
                            mPairRequestInitiated = false;
                            //notifyCustomSerialChat();
                            //startService();
                            showMessage(mDeviceName);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);
    }

    @Override
    public void onLePairingFailedStatus(boolean status) {

        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLeServiceDiscovered(boolean status) {

    }

    @Override
    public void onLeDeviceDisconnected() {

        try {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLePairingRequest() {
        mPairRequestInitiated = true;
        mProgressDialog.showAlert();
    }

    public synchronized AppConstants.ADV_CATEGORY addServiceToGattServer(List<ParcelUuid> uuidList) {
        List<UUID> list = new ArrayList<>();
        bleApplication.removeAdvertisingList();

        if (uuidList != null) {
            if (uuidList.size() > 0) {
                for (int i = 0; i < uuidList.size(); i++) {
                    if (uuidList.get(i) != null)
                        list.add(uuidList.get(i).getUuid());
                }
            }
        }

        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) != null) {
                    if ((list.get(i).toString()).equalsIgnoreCase(GattAttributes.CURRENT_TIME_SERVICE)) {
                        category = AppConstants.ADV_CATEGORY.TIME;
                    } else if ((list.get(i).toString()).equalsIgnoreCase(GattAttributes.PHONE_ALERT_STATUS_SERVICE)) {
                        category = AppConstants.ADV_CATEGORY.PHONE_ALERT;
                    } else if ((list.get(i).toString()).equalsIgnoreCase(GattAttributes.ALERT_NOTIFICATION_SERVICE)) {
                        category = AppConstants.ADV_CATEGORY.ANS;
                    } else if ((list.get(i).toString()).equalsIgnoreCase(GattAttributes.SERIAL_PORT)) {
                        category = AppConstants.ADV_CATEGORY.SERIAL_PORT;
                    }
                }
            }
        }

        if (category != AppConstants.ADV_CATEGORY.DEFAULT) {
            BLEConnection.openGattServer();
        }

//        switch (category) {
//
//            case TIME:
//                BluetoothGattService service;
//                CurrentTimeService currentTimeService = CurrentTimeService.getInstance();
//                service = currentTimeService.initialize();
//                if (service != null)
//                    bleApplication.addAdvertisingService(service);
//                break;
//            case ANS:
//                BluetoothGattService serviceANS;
//                AlertNotificationService ansService = AlertNotificationService.getInstance();
//                serviceANS = ansService.initialize();
//                if (serviceANS != null)
//                    bleApplication.addAdvertisingService(serviceANS);
//                break;
//            case PHONE_ALERT:
//                PhoneAlertService alertService = PhoneAlertService.getInstance();
//                BluetoothGattService phoneAlertBluetoothGattService = alertService.getBlueToothGattService();
//                if (phoneAlertBluetoothGattService != null)
//                    bleApplication.addAdvertisingService(phoneAlertBluetoothGattService);
//                break;
//            case SERIAL_PORT:
//                BluetoothGattService SSPservice;
//                SerialPortService sspService = SerialPortService.getInstance();
//                SSPservice = sspService.initialize();
//                if (SSPservice != null)
//                    bleApplication.addAdvertisingService(SSPservice);
//                break;
//            default:
//                break;
//        }
        return category;

    }


    @Override
    public void onLeServicesDiscovered(boolean status, String serviceUuid) {
        BluetoothGattService service;
        if (status) {
            //starting individual service for profile
            startService();

            if (category == AppConstants.ADV_CATEGORY.TIME) {
                if (serviceUuid.equalsIgnoreCase("00001805-0000-1000-8000-00805f9b34fb")) {
                    NextDstChangeService nextDstChangeService = NextDstChangeService.getInstance();
                    service = nextDstChangeService.initialize();
                    if (service != null)
                        bleApplication.addAdvertisingService(service);
                } else if (serviceUuid.equalsIgnoreCase("00001807-0000-1000-8000-00805f9b34fb")) {
                    ReferenceTimeUpdateService referenceTimeUpdateService = ReferenceTimeUpdateService.getInstance();
                    service = referenceTimeUpdateService.innitialize();
                    if (service != null)
                        bleApplication.addAdvertisingService(service);
                } else if (serviceUuid.equalsIgnoreCase("00001806-0000-1000-8000-00805f9b34fb")) {
                    BLEConnection.connectGattServer(mAddress, mName, getActivity());
                    mDeviceConnectionLossHandler = new Handler();
                    mDeviceConnectionLossHandler.postDelayed(runnable, CONNECTION_TIME_OUT);
                }
            } else if (category == AppConstants.ADV_CATEGORY.SERIAL_PORT) {
                BLEConnection.connect(mAddress, mName, getActivity());
                mDeviceConnectionLossHandler = new Handler();
                mDeviceConnectionLossHandler.postDelayed(runnable, CONNECTION_TIME_OUT);
            } else if (category == AppConstants.ADV_CATEGORY.DEFAULT) {
                BLEConnection.connect(mAddress, mName, getActivity());
                mDeviceConnectionLossHandler = new Handler();
                mDeviceConnectionLossHandler.postDelayed(runnable, CONNECTION_TIME_OUT);
            } else {
                BLEConnection.connectGattServer(mAddress, mName, getActivity());
                mDeviceConnectionLossHandler = new Handler();
                mDeviceConnectionLossHandler.postDelayed(runnable, CONNECTION_TIME_OUT);

            }
        }
    }

    private void startService() {
        if (category == AppConstants.ADV_CATEGORY.SERIAL_PORT) {
            Intent sppService = new Intent(getActivity(), SPPService.class);
            getActivity().startService(sppService);
        } else if (category == AppConstants.ADV_CATEGORY.ANS) {
            Intent ansService = new Intent(getActivity(), AnsService.class);
            getActivity().startService(ansService);
        } else if (category == AppConstants.ADV_CATEGORY.TIME) {
            Intent timeService = new Intent(getActivity(), TimeService.class);
            getActivity().startService(timeService);
        } else if (category == AppConstants.ADV_CATEGORY.PHONE_ALERT) {
            Intent timeService = new Intent(getActivity(), PAService.class);
            getActivity().startService(timeService);
        }
    }

    private void notifyCustomSerialChat() {
        if (category == AppConstants.ADV_CATEGORY.SERIAL_PORT) {
            SPPService.sendNotifyCharacteristic();
        }
    }


    private void permissionCheckBasedOnCategory(AppConstants.ADV_CATEGORY advCategory) {
        String[] permissions = new String[0];
        switch (advCategory) {
            case TIME:
                break;

            case ANS:
                permissions = new String[2];
                permissions[0] = Manifest.permission.READ_CALL_LOG;
                permissions[1] = Manifest.permission.READ_SMS;
                break;


            case PHONE_ALERT:
                permissions = new String[1];
                permissions[0] = Manifest.permission.READ_CALL_LOG;
                break;


            case SERIAL_PORT:
                break;
            default:
                break;

        }

        for (String per : permissions) {
            checkPermission(per);
        }
    }

    private void checkPermission(String permission) {
        int result = ContextCompat.checkSelfPermission(getActivity(), permission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            permissionList.remove(permission);
        } else {
            permissionList.add(permission);
        }
    }


    /**
     * On Request permission result call back
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doServicesInitialization();
                    AppUtils.setIntSharedPreference(getActivity(), AppUtils.ANS_SMS_UNREAD_COUNT, -1);
                    AppUtils.setIntSharedPreference(getActivity(), AppUtils.ANS_MISSEDCALL_UNREAD_COUNT, -1);
                    AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.ANS_READ_NOTIFICATION_STATUS, false);
                    AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.ANS_READ_NOTIFICATION_STATUS, false);
                    AppUtils.setBooleanSharedPreference(getActivity(), Utils.PHONE_ALERT_STATUS, false);
                    AppUtils.setIntSharedPreference(getActivity(), AppUtils.ANS_SMS_NEW_COUNT, 0);
                    AppUtils.setStringSharedPreference(getActivity(), AppUtils.ANS_SMS_NEW_TEXT, null);
                    AppUtils.setIntSharedPreference(getActivity(), AppUtils.ANS_MISSEDCALL_NEW_COUNT, 0);

                    if (category == AppConstants.ADV_CATEGORY.DEFAULT) {
                        BLEConnection.connect(mAddress, mName, getActivity());
                        mDeviceConnectionLossHandler = new Handler();
                        mDeviceConnectionLossHandler.postDelayed(runnable, CONNECTION_TIME_OUT);
                    }
                } else {

                }

                break;
            default:
                break;

        }
    }
}
