/*
 *
 *
 *     FilterPrefsFragment.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.fragments;


import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.microchip.R;
import com.microchip.utils.AppUtils;

/**
 * Created by Atmel on 17/07/15.
 */
public class FilterPrefsFragment extends PreferenceFragment {

    CheckBoxPreference mAlertNotification;
    CheckBoxPreference mBloodPressure;
    CheckBoxPreference mFindMe;
    CheckBoxPreference mThermometer;
    CheckBoxPreference mHeartRate;
    CheckBoxPreference mPhoneAlert;
    CheckBoxPreference mProximity;
    CheckBoxPreference mScanParam;
    CheckBoxPreference mSerialChat;
    CheckBoxPreference mTime;
    CheckBoxPreference mOTAU;

    boolean thermometerFlag = false, proximityFlag = false, mAnsFlag = false, mBloodPressureFlag = false, mFindMeFlag = false,
            mHeartRateFlag = false, mPhoneAlertFlag = false, mScanParamFlag = false, mSerailChatFlag = false, mTimeFlag = false, mOTAUFlag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.filter_settings);
        mThermometer = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.thermometer));
        mProximity = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.proximity));
        mAlertNotification = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.alert_notification));
        mBloodPressure = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.blood_pressure));
        mFindMe = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.find_me));
        mHeartRate = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.heart_rate));
        mPhoneAlert = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.phone_alert_status));
        mScanParam = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.title_activity_scan_paramter));
        mSerialChat = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.serial_port));
        mTime = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.time));
        mOTAU = (CheckBoxPreference) findPreference(getActivity().getResources().getString(R.string.otau_filter));

        // getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        preferenceChange();
    }

    private void preferenceChange() {
        mThermometer.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                //AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.THERMOMETE, value);
                mThermometer.setChecked(value);
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.THERMOMETE, value);
                return true;
            }
        });
        mProximity.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                //AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.PROXIMITY, value);
                mProximity.setChecked(value);
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.PROXIMITY, value);
                return true;
            }
        });
        mAlertNotification.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                mAlertNotification.setChecked(value);
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.ANS, value);
                return true;
            }
        });
        mBloodPressure.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                mBloodPressure.setChecked(value);
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.BLOOD_PRESSURE, value);
                return true;
            }
        });
        mFindMe.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.FIND_ME, value);
                mFindMe.setChecked(value);
                return true;
            }
        });
        mHeartRate.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                mHeartRate.setChecked(value);
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.HEART_RATE, value);
                return true;
            }
        });

        mPhoneAlert.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.PHONE_ALERT, value);
                mPhoneAlert.setChecked(value);
                return true;
            }
        });
        mScanParam.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                mScanParam.setChecked(value);
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.SCAN_PARAM, value);
                return true;
            }
        });

        mSerialChat.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.SERIAL_CHAT, value);
                mSerialChat.setChecked(value);
                return true;
            }
        });

        mTime.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.TIME, !value);
                mTime.setChecked(value);
                return true;
            }
        });

        mOTAU.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (Boolean) newValue;
                AppUtils.setDefaultSharedPreference(getActivity(), AppUtils.OTAU, !value);
                mOTAU.setChecked(value);
                return true;
            }
        });
    }
}