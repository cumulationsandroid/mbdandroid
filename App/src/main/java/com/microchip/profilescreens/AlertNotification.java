/*
 *
 *
 *     AlertNotification.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.AlertNotificationService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.services.AnsService;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;

public class AlertNotification extends BLEBaseActivity {
    private static BLELollipopScanner mBleLollipopScanner;
    private static BLEKitKatScanner mBleKitkatScanner;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    private String mCallerPhoneNumber = null;
    private String mNewSms = null;
    private String mNewMissedCall = null;
    private String msgBody = null;
    private AlertNotificationService mAlertNotificationService;
    private TextView mSmsCountValue = null;
    private TextView mSmsTextData = null;
    private TextView mMissedcallCountValue = null;
    private TextView mMissedcallTextData = null;
    private TextView mUnreadMissedcallCount = null;
    private TextView mUnreadSmsCount = null;
    private String mUnreadMissedCallCount = null;
    private String mUnreadSmsCountValue = null;
    private boolean mReadNotificationStatus = false;
    private boolean mUnReadNotificationStatus = false;
    private TextView mReadNotifyStatusText = null;
    private ImageView mReadNotifyImage = null;
    private TextView mUnReadNotifyStatusText = null;
    private ImageView mUnReadNotifyImage = null;
    /*
    The receiver to receive the sms,missed call information from AnsService.
    */
    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case AppConstants.ANS_NOTIFICATION_CHANGE_REQUEST:
                    setNotificationDetails();
                    break;
                case AppConstants.ANS_NEW_MISSED_CALL_RECEIVED:
                    mNewMissedCall = intent.getStringExtra(AppConstants.ANS_NEW_MISSED_CALL_COUNT);
                    mCallerPhoneNumber = intent.getStringExtra(AppConstants.ANS_NEW_MISSED_CALL_TEXT);
                    notifyNewMissedCall();
                    break;
                case AppConstants.ANS_NEW_SMS_RECEIVED:
                    mNewSms = intent.getStringExtra(AppConstants.ANS_NEW_SMS_COUNT);
                    msgBody = intent.getStringExtra(AppConstants.ANS_NEW_SMS_TEXT);
                    notifyNewSms();
                    break;
                case AppConstants.ANS_UNREAD_SMS_UPDATE:
                    mUnreadSmsCountValue = intent.getStringExtra(AppConstants.ANS_UNREAD_SMS_COUNT);
                    notifyUnReadSms();
                    break;
                case AppConstants.ANS_UNREAD_MISSED_CALL_UPDATE:
                    Log.d("mMessageReceiver", "AppConstants.ANS_UNREAD_MISSED_CALL_UPDATE");
                    mUnreadMissedCallCount = intent.getStringExtra(AppConstants.ANS_UNREAD_MISSED_CALL_COUNT);
                    notifyUnreadMissedCall();
                    break;
                default:
                    break;
            }
        }
    };
    private PairingAlert mProgressDialog;
    private boolean mIsPaused = false;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_alertnotification2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAlertNotificationService = com.atmel.blecommunicator.com.atmel.Services.AlertNotificationService.getInstance();
        mProgressDialog = new PairingAlert(this);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
        setUi();
        // The ANS UI interested in the following actions from AnsService
        IntentFilter filter = new IntentFilter();
        filter.addAction(AppConstants.ANS_NOTIFICATION_CHANGE_REQUEST);
        filter.addAction(AppConstants.ANS_NEW_MISSED_CALL_RECEIVED);
        filter.addAction(AppConstants.ANS_NEW_SMS_RECEIVED);
        filter.addAction(AppConstants.ANS_UNREAD_SMS_UPDATE);
        filter.addAction(AppConstants.ANS_UNREAD_MISSED_CALL_UPDATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                filter);
        int newSms = AppUtils.getIntSharedPreference(this, AppUtils.ANS_SMS_NEW_COUNT);
        mNewSms = String.valueOf(newSms);
        msgBody = AppUtils.getStringSharedPreference(this, AppUtils.ANS_SMS_NEW_TEXT);
        int newMissedCall = AppUtils.getIntSharedPreference(this, AppUtils.ANS_MISSEDCALL_NEW_COUNT);
        mNewMissedCall = String.valueOf(newMissedCall);
        int unreadSms = AppUtils.getIntSharedPreference(this, AppUtils.ANS_SMS_UNREAD_COUNT);
        mUnreadSmsCountValue = String.valueOf(unreadSms);
        int unreadMissedCall = AppUtils.getIntSharedPreference(this, AppUtils.ANS_MISSEDCALL_UNREAD_COUNT);
        mUnreadMissedCallCount = String.valueOf(unreadMissedCall);
        updateUi();
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
    }

    public void setUi() {
        mSmsCountValue = (TextView) findViewById(R.id.smsCountValue);
        mSmsTextData = (TextView) findViewById(R.id.smsTextData);
        mMissedcallCountValue = (TextView) findViewById(R.id.missedcallCountValue);
        mMissedcallTextData = (TextView) findViewById(R.id.missedcallTextData);
        mUnreadMissedcallCount = (TextView) findViewById(R.id.unreadMissedcallCountValue);
        mUnreadSmsCount = (TextView) findViewById(R.id.unreadSmsCountValue);
        mReadNotifyStatusText = (TextView) findViewById(R.id.notification1);
        mReadNotifyImage = (ImageView) findViewById(R.id.notificationImage1);
        mUnReadNotifyStatusText = (TextView) findViewById(R.id.notification2);
        mUnReadNotifyImage = (ImageView) findViewById(R.id.notificationImage2);
    }

    public void updateUi() {
        setNotificationDetails();
        notifyNewSms();
        notifyUnReadSms();
        notifyNewMissedCall();
        notifyUnreadMissedCall();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                try {
                    LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void notifyNewSms() {
        mSmsCountValue.setText(String.valueOf(mNewSms));
        if (msgBody != null) {
            mSmsTextData.setText(msgBody);
        }
    }

    private void notifyNewMissedCall() {
        mMissedcallCountValue.setText(String.valueOf(mNewMissedCall));
        mMissedcallTextData.setText(mCallerPhoneNumber);
    }

    private void notifyUnReadSms() {
        mUnreadSmsCount.setText(String.valueOf(mUnreadSmsCountValue));
    }

    private void notifyUnreadMissedCall() {
        Log.d("mMessageReceiver", "notifyUnreadMissedCall");
        Log.d("mMessageReceiver", "mUnreadMissedCallCount" + mUnreadMissedCallCount);
        mUnreadMissedcallCount.setText(String.valueOf(mUnreadMissedCallCount));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(AlertNotification.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(AlertNotification.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
        mIsPaused = false;
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        mIsPaused = true;
    }

    private void init() {
        Boolean status = false;
        bluetoothCheck();
        setConnectionListener(this);
        if (mIsPaused) {
            checkUnreadSmsMissedCall();
            mIsPaused = false;
        }
        try {
            status = BLEConnection.getmServerConnectionState() != BLEConnection.STATE_DISCONNECTED;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!status) {
            onLeDeviceDisconnected();
        }
    }

    private void bluetoothCheck() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }


    @Override
    public void onLeDeviceConnected(String deviceName) {
        mReconnectionAlert.dismissAlert();
        init();
        AnsService.mFirstTimeActivity = true;
        AppUtils.setIntSharedPreference(this, AppUtils.ANS_SMS_UNREAD_COUNT, -1);
        AppUtils.setIntSharedPreference(this, AppUtils.ANS_MISSEDCALL_UNREAD_COUNT, -1);
    }

    @Override
    public void onLeDeviceDisconnected() {
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            try {
                mReconnectionAlert.showAlert();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (BLEConnection.mReconnectionEnabled) {
                BLEConnection.gattServerAutoConnect(this);
                BLEConnection.mReconnectionEnabled = false;
            }
        }
    }


    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    //Notification enabled/disabled status displaying on the application based on the request from
    // peripheral
    private void setNotificationDetails() {
        mReadNotificationStatus = AppUtils.getBooleanSharedPreference(this, Utils.ANS_READ_NOTIFICATION_STATUS);
        mUnReadNotificationStatus = AppUtils.getBooleanSharedPreference(this, Utils.ANS_UNREAD_NOTIFICATION_STATUS);
        if (mReadNotificationStatus) {
            mReadNotifyStatusText.setText(getResources().getText(R.string.notification_enabled));
            mReadNotifyStatusText.setTextColor(getResources().getColor(R.color.black));
            mReadNotifyImage.setImageResource(R.drawable.notification_enabled_icon);
        } else {
            mReadNotifyStatusText.setText(getResources().getText(R.string.notification_disabled));
            mReadNotifyImage.setImageResource(R.drawable.notification_disable_icon);
            mReadNotifyStatusText.setTextColor(getResources().getColor(R.color.colorAccent));
        }
        if (mUnReadNotificationStatus) {
            mUnReadNotifyStatusText.setText(getResources().getText(R.string.notification_enabled));
            mUnReadNotifyStatusText.setTextColor(getResources().getColor(R.color.black));
            mUnReadNotifyImage.setImageResource(R.drawable.notification_enabled_icon);
        } else {
            mUnReadNotifyStatusText.setText(getResources().getText(R.string.notification_disabled));
            mUnReadNotifyImage.setImageResource(R.drawable.notification_disable_icon);
            mUnReadNotifyStatusText.setTextColor(getResources().getColor(R.color.colorAccent));
        }
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);
    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goToHome() {
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    //check bluetooth status
    private boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }

    //onresume of activity recheck on unread sms/missedcall count and update UI
    private void checkUnreadSmsMissedCall() {

        AnsService.checkUnreadSmsMissedCall();
        /*
        int mUnreadSmsCountVal = 0;
        int mUnreadMissedCallCountVal = 0;
        int mPrevUnreadSmsCountValue = AppUtils.getIntSharedPreference(this, AppUtils.ANS_SMS_UNREAD_COUNT);
        int mPrevUnreadMissedCallCount = AppUtils.getIntSharedPreference(this, AppUtils.ANS_MISSEDCALL_UNREAD_COUNT);

        try {
            final Uri SMS_INBOX = Uri.parse("content://sms/inbox");
            Cursor c = getContentResolver().query(SMS_INBOX, null, "read = 0", null, null);
            mUnreadSmsCountVal = c.getCount();

            String where = CallLog.Calls.TYPE + "=" + CallLog.Calls.MISSED_TYPE + " AND " + CallLog.Calls.NEW + "=1";
            String[] projection = {CallLog.Calls.CACHED_NAME, CallLog.Calls.CACHED_NUMBER_LABEL, CallLog.Calls.TYPE};
            Cursor cursor = this.getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, where, null, null);
            mUnreadMissedCallCountVal = cursor.getCount();
        }catch(Exception e){
            e.printStackTrace();
        }
        if (mPrevUnreadSmsCountValue != mUnreadSmsCountVal) {
            mAlertNotificationService.unreadAlertNotifyConnectedDevices(UUIDDatabase.UUID_ANS_UNREAD_ALERT_STATUS,
                    0, mUnreadSmsCountVal);
        }
        if (mPrevUnreadMissedCallCount != mUnreadMissedCallCountVal) {
            mAlertNotificationService.unreadAlertNotifyConnectedDevices(UUIDDatabase.UUID_ANS_UNREAD_ALERT_STATUS,
                    1, mUnreadMissedCallCountVal);
        }
        */
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setUi();
        updateUi();
    }
}
