/*
 *
 *
 *     BatteryService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.OTAUService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.views.VerticalProgressBar;

public class BatteryService extends BLEBaseActivity implements CompoundButton.OnCheckedChangeListener, OTAUManager.OTAUProgressInfoListener {

    private static final int BATTERY_LOW_PERS = 20;
    private static final int BATTERY_MID_PERS = 60;
    //private static final int BATTERY_HIGH_PERS = 60;
    private static final String BATTERY_NOTIFY_STATUS = "STATUS_NOTIFY";
    public String mOTAUStatusText;
    public int mOTAUProgress;
    private BLEApplication mBleApplication;
    private BluetoothGattService mBluetoothGattService, mBluetoothOTAGATTService;
    private OTAUService mOTAUService;
    private com.atmel.blecommunicator.com.atmel.Services.BatteryService mBatteryService;
    private BLELollipopScanner mBleLollipopScanner;
    private BLEKitKatScanner mBleKitkatScanner;
    private Toolbar mToolBar;
    private TextView mBatteryProgressTextView;
    private TextView mStartText;
    private VerticalProgressBar mBateryProgressBar;
    private SwitchCompat mNotifySwitchCompat;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.battery_level_activity);

        mBatteryProgressTextView = (TextView) findViewById(R.id.batteryProgressTextView);
        mBateryProgressBar = (VerticalProgressBar) findViewById(R.id.batteryProgress);
        mNotifySwitchCompat = (SwitchCompat) findViewById(R.id.scanNotifiyswitch);
        mStartText = (TextView) findViewById(R.id.startText);
        mNotifySwitchCompat.setOnCheckedChangeListener(this);

        /**
         * Setting the max value for Battery progress
         */
        mBateryProgressBar.setMax(100);

        /**
         * Setting ToolBar
         */
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        /**
         * Getting the application instance
         */
        mBleApplication = (BLEApplication) getApplication();

        /**
         * Getting the services and the data
         */
        mBluetoothGattService = mBleApplication.getmBluetoothGattService();
        mBluetoothOTAGATTService = mBleApplication.getBluetoothOTAGattService();

        /**
         *
         */
        mBatteryService = com.atmel.blecommunicator.com.atmel.Services.BatteryService.getInstance();
        mBatteryService.setmBatteryLevelService(mBluetoothGattService);
        mBatteryService.getBatteryLevelCharacteristics();

        /**
         * Set listeners
         */
        setGattListener(this);
        setConnectionListener(this);

        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

        checkNotifyStatus();
    }

    private void checkNotifyStatus() {
        if (AppUtils.getBooleanSharedPreference(this, AppUtils.FIRST_TIME)) {
            mStartText.setText("STOP NOTIFY");
            mNotifySwitchCompat.setChecked(true);
            mBatteryService.setNotify(true);
            AppUtils.setBooleanSharedPreference(this, AppUtils.FIRST_TIME, false);
            AppUtils.setBooleanSharedPreference(this, AppUtils.BATTERY_NOTIFY_STATUS, true);
        } else {
            boolean status = AppUtils.getBooleanSharedPreference(this, AppUtils.BATTERY_NOTIFY_STATUS);
            mNotifySwitchCompat.setChecked(status);
            mBatteryService.setNotify(status);
            if (status) {
                mStartText.setText(getResources().getText(R.string.stop_notify));
            } else {
                mStartText.setText(getResources().getText(R.string.start_notify));
            }
            batteryUIUpdate(0);
        }
    }

    private void disableNotify(boolean state) {
        if (state) {
            mBatteryService.setNotify(false);
            AppUtils.setBooleanSharedPreference(this, BATTERY_NOTIFY_STATUS, true);
        } else {
            AppUtils.setBooleanSharedPreference(this, BATTERY_NOTIFY_STATUS, false);
        }
    }


    @Override
    public void onLeCharacteristicChanged(boolean status) {
        super.onLeCharacteristicChanged(status);
        if (status) {
            int batteryLevel = mBatteryService.getBatteryLevel();
            batteryUIUpdate(batteryLevel);
        }
    }

    private void batteryUIUpdate(int level) {
        mBatteryProgressTextView.setText(level + "%");
        animateProgress(level);
    }

    private void animateProgress(int batteryLevel) {
        mBateryProgressBar.setProgress(batteryLevel);
        if (batteryLevel < BATTERY_LOW_PERS) {
            mBateryProgressBar.setProgressDrawable(getResources().getDrawable(R.drawable.vertical_progress_bar_for_battery_red));
        } else if (batteryLevel < BATTERY_MID_PERS) {
            mBateryProgressBar.setProgressDrawable(getResources().getDrawable(R.drawable.vertical_progress_bar_for_battery_orenge));
        } else if (batteryLevel >= BATTERY_MID_PERS) {
            mBateryProgressBar.setProgressDrawable(getResources().getDrawable(R.drawable.vertical_progress_bar_for_battery_green));
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(BatteryService.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(BatteryService.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppUtils.setIntSharedPreference(BatteryService.this, AppUtils.BATTERY_VALUE, mBatteryService.getBatteryLevel());
        disableNotify(mNotifySwitchCompat.isChecked());
        setGattListener(null);
        setConnectionListener(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener = this;
        checkNotifyStatus();
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        setGattListener(this);
        setConnectionListener(this);
        //setGattServerListener(this);
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        mBatteryService.setNotify(b);
        AppUtils.setBooleanSharedPreference(this, BATTERY_NOTIFY_STATUS, b);
        if (b) {
            mStartText.setText("STOP NOTIFY");
        } else {
            mStartText.setText("START NOTIFY");
        }
    }

}

