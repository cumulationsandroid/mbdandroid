/*
 *
 *
 *     BloodPressure.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Characteristics.BloodPressureFeatureCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.BloodPressureMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.BloodPressureService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;

public class BloodPressure extends BLEBaseActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener,OTAUManager.OTAUProgressInfoListener {
    private final static String TAG = BloodPressure.class.getSimpleName();
    private static BLELollipopScanner mBleLollipopScanner;
    private static BLEKitKatScanner mBleKitkatScanner;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    boolean flag = true;
    boolean mIsFromConfigChange = false;
    private BluetoothGattService mBluetoothGattService;
    private BloodPressureService mBloodPressureService;
    private RelativeLayout mInfoViewHolder;
    private View mInfoView;
    private TextView mStartText;
    private SwitchCompat mStartSwitch = null;
    private TextView mSysPressureText, mDiaPressureText, mPulseText, mSysStartValue, mSysMidValue, mDiaStartValue, mDiaMidValue, mSysBetweenValue, mSysBetweenEndValue, mDiabetweenEndValue, mDiaBetweenValue;
    private ProgressBar mSysProgressBar, mDiaProgressBar;
    private ImageView mInfoButton, mInfoClose;
    private ImageView mBodyMovementStatus, mCuffFitStatus, mIrregularPulseStatus, mPulsRateStatus, mMeasurementPositionStatus, mMultipleBondStatus;
    private BLEApplication bleApplication;
    private PairingAlert mProgressDialog;
    private RelativeLayout mDiaParent;
    private TextView mSysTitle;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("onCreate", "onCreate");
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        setContentView(R.layout.activity_blood_pressure);
        /**
         * Getting the application instance
         */
        bleApplication = (BLEApplication) getApplication();
        init(false);

        Log.e("savedInstanceState", "" + savedInstanceState);
       /* if (savedInstanceState!=null){
            mOrientationChanged=true;
            mBluetoothGattService = bleApplication.getmBluetoothGattService();
            mBloodPressureService=bleApplication.getmBloodPressureService();
            onRestoreInstanceState(savedInstanceState);
            GUIUpdates(bleApplication.getmBloodPressureMeasurementCharacteristic());
        }else {*/
        // mOrientationChanged=false;
        /**
         * Initializing the GUI Elements
         */


        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));

        /**
         * Getting the service and data
         */
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mBloodPressureService = new BloodPressureService(mBluetoothGattService);
        bleApplication.setmBloodPressureService(mBloodPressureService);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
        //}
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }
    @Override
    protected void onResume() {
        super.onResume();
        /**
         * Registering the listners
         */
        OTAUManager.getInstance().otauProgressInfoListener=this;
        mIsFromConfigChange = false;
        setConnectionListener(this);
        setGattListener(this);
        bluetoothCheck();
        checkingNotificationEnabling();
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(BloodPressure.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(BloodPressure.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        bleApplication.setmBloodPressureMeasurementCharacteristic(null);
        bleApplication.setmBloodPressureFeatureCharacteristic(null);
    }

    private void checkingNotificationEnabling() {
        if (AppUtils.getBooleanSharedPreference(this, AppUtils.FIRST_TIME)) {
            if (mStartSwitch.isChecked()) {
                if (!mIsFromConfigChange)
                    enabling();
                mStartText.setText(getString(R.string.stop));
            }
            AppUtils.setBooleanSharedPreference(this, AppUtils.FIRST_TIME, false);
            AppUtils.setBooleanSharedPreference(BloodPressure.this, AppUtils.BLOOD_PRESSURE_SELECTION_KEY, true);
        } else {
            if (AppUtils.getBooleanSharedPreference(this, AppUtils.BLOOD_PRESSURE_SELECTION_KEY)) {
                if (mStartSwitch.isChecked()) {
                    if (!mIsFromConfigChange)
                        enabling();

                }
                mStartText.setText(getString(R.string.stop));
            } else {

                mStartSwitch.setChecked(false);
                mStartText.setText(getString(R.string.start));

            }
        }
    }

    private void init(Boolean showInfo) {
        /*
         * Displaying the toolbar
         */
        Toolbar toolbar = (Toolbar) findViewById(R.id.bp_toolbar);
        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mSysPressureText = (TextView) findViewById(R.id.sys_value);
        mDiaPressureText = (TextView) findViewById(R.id.dia_value);
        mPulseText = (TextView) findViewById(R.id.pulse_value);
        mStartText = (TextView) findViewById(R.id.bp_startText);
        mStartSwitch = (SwitchCompat) findViewById(R.id.bp_startSwitch);
        mStartSwitch.setOnCheckedChangeListener(this);
        mInfoButton = (ImageView) findViewById(R.id.bp_info_button);
        mSysTitle = (TextView) findViewById(R.id.sysTitle);
        mDiaParent = (RelativeLayout) findViewById(R.id.diaParent);
        mInfoButton.setOnClickListener(this);

        mSysStartValue = (TextView) findViewById(R.id.sys_start_value);
        mSysMidValue = (TextView) findViewById(R.id.sys_mid_value);
        mDiaStartValue = (TextView) findViewById(R.id.dia_start_value);
        mDiaMidValue = (TextView) findViewById(R.id.dia_mid_value);
        mSysBetweenValue = (TextView) findViewById(R.id.sys_between_mid);
        mSysBetweenEndValue = (TextView) findViewById(R.id.sys_between_end);
        mDiaBetweenValue = (TextView) findViewById(R.id.dia_between_mid);
        mDiabetweenEndValue = (TextView) findViewById(R.id.dia_between_end);

        mInfoViewHolder = (RelativeLayout) findViewById(R.id.info_view);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        mInfoView = getLayoutInflater().inflate(R.layout.activity_blood_pressure_features, null);
        mInfoView.setLayoutParams(params);
        mInfoViewHolder.addView(mInfoView);

        if (showInfo == true) {
            mInfoView.setVisibility(View.VISIBLE);
        } else {
            mInfoView.setVisibility(View.GONE);
        }

        mInfoClose = (ImageView) mInfoView.findViewById(R.id.info_close);
        mInfoClose.setOnClickListener(this);
        mBodyMovementStatus = (ImageView) mInfoView.findViewById(R.id.body_movement_status);
        mCuffFitStatus = (ImageView) mInfoView.findViewById(R.id.cuff_fit_status);
        mIrregularPulseStatus = (ImageView) mInfoView.findViewById(R.id.irregular_pulse);
        mPulsRateStatus = (ImageView) mInfoView.findViewById(R.id.pulse_rate_status);
        mMeasurementPositionStatus = (ImageView) mInfoView.findViewById(R.id.measurement_position_status);
        mMultipleBondStatus = (ImageView) mInfoView.findViewById(R.id.multiple_bond_status);

        mSysProgressBar = (ProgressBar) findViewById(R.id.sysProgressbar);
        mDiaProgressBar = (ProgressBar) findViewById(R.id.diaProgressbar);
        mProgressDialog = new PairingAlert(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
        Log.d("onConfigurationChanged", "onConfigurationChanged");
        setContentView(R.layout.activity_blood_pressure);
        mIsFromConfigChange = true;
        showBPView(mIsFromConfigChange);
    }

    private void showBPView(Boolean isFromConfigChange) {
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mBloodPressureService = bleApplication.getmBloodPressureService();
        BloodPressureMeasurementCharacteristic mBloodPressureMeasurementChar = bleApplication.getmBloodPressureMeasurementCharacteristic();
        BloodPressureFeatureCharacteristic mBloodPressureFeatureChar = bleApplication.getmBloodPressureFeatureCharacteristic();
        if (mBluetoothGattService != null && mBloodPressureService != null) {

            if (mInfoView.getVisibility() == View.VISIBLE) {
                init(true);
                if (mBloodPressureFeatureChar != null)
                    infoViewUIUpdates(mBloodPressureFeatureChar);

            } else {
                init(false);
                if (isFromConfigChange)
                    checkingNotificationEnabling();
                if (mBloodPressureMeasurementChar != null)
                    GUIUpdates(mBloodPressureMeasurementChar);
            }
        } else {
            Log.d("onConfigurationChanged", "else");
        }
    }


    private void bluetoothCheck() {
        /**
         * Always pair the device,
         * When app enter into the page
         */

        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {
        if (status) {
            try {
                BloodPressureMeasurementCharacteristic bloodPressureMeasurementCharacteristic = BloodPressureMeasurementCharacteristic.getInstance();
                bleApplication.setmBloodPressureMeasurementCharacteristic(bloodPressureMeasurementCharacteristic);
                GUIUpdates(bloodPressureMeasurementCharacteristic);
                Log.e("IsIntermediate Cuff ", "" + bloodPressureMeasurementCharacteristic.mIsIntermediateCuff);


            } catch (Exception e) {
                Log.v(TAG, "Exception " + e.getMessage());
            }
        }
    }

    private void GUIUpdates(BloodPressureMeasurementCharacteristic bloodPressureMeasurementCharacteristic) {
        if (bloodPressureMeasurementCharacteristic.mIsIntermediateCuff) {
            mPulseText.setVisibility(View.GONE);
            mDiaParent.setVisibility(View.INVISIBLE);
            mInfoButton.setVisibility(View.INVISIBLE);
            mSysTitle.setText("Current Cuff Pressure");
            mSysTitle.setTextSize(20);
        } else {
            mDiaParent.setVisibility(View.VISIBLE);
            mPulseText.setVisibility(View.VISIBLE);
            mInfoButton.setVisibility(View.VISIBLE);
            mSysTitle.setText(getString(R.string.sys));
            mSysTitle.setTextSize(25);
        }
        if (bloodPressureMeasurementCharacteristic.mPressureUnit.equalsIgnoreCase(getString(R.string.kpa))) {
            mSysProgressBar.setMax(32);
            mDiaProgressBar.setMax(32);
            mSysProgressBar.setSecondaryProgress(32);
            mDiaProgressBar.setSecondaryProgress(32);
            mSysStartValue.setText("0-32");
            mSysBetweenValue.setText("8");
            mSysMidValue.setText("16");
            mSysBetweenEndValue.setText("24");
            mDiaStartValue.setText("0-32");
            mDiaBetweenValue.setText("8");
            mDiaMidValue.setText("16");
            mDiabetweenEndValue.setText("24");
        } else {
            mSysProgressBar.setMax(240);
            mDiaProgressBar.setMax(240);
            mSysProgressBar.setSecondaryProgress(240);
            mDiaProgressBar.setSecondaryProgress(240);
            mSysStartValue.setText("0-240");
            mSysBetweenValue.setText("60");
            mSysMidValue.setText("120");
            mSysBetweenEndValue.setText("180");
            mDiaStartValue.setText("0-240");
            mDiaBetweenValue.setText("60");
            mDiaMidValue.setText("120");
            mDiabetweenEndValue.setText("180");
        }
        Log.d("Systolic Pressure>>>>>", "" + bloodPressureMeasurementCharacteristic.mSysPressure);
        Log.d("Diastolic Pressure>>>>>", "" + bloodPressureMeasurementCharacteristic.mDiaPressure);

        mSysPressureText.setText("" + bloodPressureMeasurementCharacteristic.mSysPressure + " " + bloodPressureMeasurementCharacteristic.mPressureUnit);
        if (bloodPressureMeasurementCharacteristic.mDiaPressure != null) {
            mDiaPressureText.setText("" + bloodPressureMeasurementCharacteristic.mDiaPressure + " " + bloodPressureMeasurementCharacteristic.mPressureUnit);
        }
        mPulseText.setText(getString(R.string.pulse) + "    " + bloodPressureMeasurementCharacteristic.mPulseRate + " " + getString(R.string.bpm));
        Double d = Double.parseDouble(bloodPressureMeasurementCharacteristic.mSysPressure);
        int i = d.intValue();
        mSysProgressBar.setProgress(i);
        d = Double.parseDouble(bloodPressureMeasurementCharacteristic.mDiaPressure);
        i = d.intValue();
        mDiaProgressBar.setProgress(i);
    }

    @Override
    public void onLeCharacteristicRead(boolean status) {
        super.onLeCharacteristicRead(status);
        if (status) {
            try {
                BloodPressureFeatureCharacteristic bloodPressureFeatureCharacteristic = BloodPressureFeatureCharacteristic.getInstance();
                bleApplication.setmBloodPressureFeatureCharacteristic(bloodPressureFeatureCharacteristic);
                infoViewUIUpdates(bloodPressureFeatureCharacteristic);

            } catch (Exception e) {
                Log.v(TAG, "Exception " + e.getMessage());
            }
        }
    }

    private void infoViewUIUpdates(BloodPressureFeatureCharacteristic bloodPressureFeatureCharacteristic) {
        if (bloodPressureFeatureCharacteristic.mBodyMovementStatus.booleanValue()) {
            Log.d("1>>>>>>>", "");
            mBodyMovementStatus.setSelected(true);
        }
        if (bloodPressureFeatureCharacteristic.mCuffFitStatus.booleanValue()) {
            Log.d("2>>>>>>>", "");
            mCuffFitStatus.setSelected(true);
        }
        if (bloodPressureFeatureCharacteristic.mIrregularPulseStatus.booleanValue()) {
            Log.d("3>>>>>>>", "");
            mIrregularPulseStatus.setSelected(true);
        }
        if (bloodPressureFeatureCharacteristic.mPulsRateStatus.booleanValue()) {
            Log.d("4>>>>>>>", "");
            mPulsRateStatus.setSelected(true);
        }
        if (bloodPressureFeatureCharacteristic.mMeasurementPositionStatus.booleanValue()) {
            Log.d("5>>>>>>>", "");
            mMeasurementPositionStatus.setSelected(true);
        }
        if (bloodPressureFeatureCharacteristic.mMultipleBondStatus.booleanValue()) {
            Log.d("6>>>>>>>", "");
            mMultipleBondStatus.setSelected(true);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.d("htp", "onOptionsItemSelected");
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        Log.d("htp", "onpause");
        super.onPause();
        disabling();
//        setConnectionListener(null);
//        setGattListener(null);
    }


    @Override
    public void onLePairingDoneStatus(boolean status) {
        Log.v(TAG, "Pairing Done");
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);

        if (mStartSwitch.isChecked()) {
            enabling();
        }
    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        Log.v(TAG, "Pairing inprocess");
        try {
            if (mProgressDialog != null) {
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        AppUtils.setBooleanSharedPreference(BloodPressure.this, AppUtils.BLOOD_PRESSURE_SELECTION_KEY, b);
        if (b) {
            mStartText.setText(getString(R.string.stop));
            enabling();

        } else {
            mStartText.setText(getString(R.string.start));
            disabling();

        }
    }

    @Override
    public void onLeDeviceDisconnected() {
       /* if (BLEConnection.isBonded()) {
            BLEConnection.unPairDevice();
        }*/
        //BLEConnection.disconnect();
        Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }


    private void disabling() {
        mBloodPressureService.getIntermediateCuffPressureCharacteristic();
        mBloodPressureService.stopNotifyIntermediateCuffPressureCharacteristic();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBloodPressureService.getBloodPressureMeasurementCharacteristic();
                mBloodPressureService.stopIndicateBloodPressureMeasurementCharacteristic();
            }
        }, 1000);
    }

    private void enabling() {
        mBloodPressureService.getIntermediateCuffPressureCharacteristic();
        mBloodPressureService.startNotifyIntermediateCuffPressureCharacteristic();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBloodPressureService.getBloodPressureMeasurementCharacteristic();
                mBloodPressureService.startIndicateBloodPressureMeasurementCharacteristic();
            }
        }, 1000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bp_info_button:
                mBloodPressureService.getBloodPressureFeatureCharacteristic();
                mBloodPressureService.readBloodPressureFeatureCharacteristic();
                mInfoView.setVisibility(View.VISIBLE);
                break;
            case R.id.info_close:
                mInfoView.setVisibility(View.GONE);
                showBPView(mIsFromConfigChange);
                break;
        }
    }
}
