/*
 *
 *
 *     CurrentTime.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.CurrentTimeService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class CurrentTime extends BLEBaseActivity implements OTAUManager.OTAUProgressInfoListener{

    private static final int STD_TIME = 0;
    private static final int HALF_DAYLIGHT_TIME = 1800000;
    private static final int DAYLIGHT_TIME = 3600000;
    private static final int DOUBLE_DAYLIGHT_TIME = 7200000;
    private static BLELollipopScanner mBleLollipopScanner;
    private static BLEKitKatScanner mBleKitkatScanner;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    private long mRefreshInterval = 1000;
    private String mUtc = null;
    private int mUtcValue = 0;
    private TextView mTimeValue = null;
    private TextView mDateValue = null;
    private TextView mAmPmValue = null;
    private TextView mTimezoneValue = null;
    private TextView mDaylighttimeValue = null;
    private TextView mTimesourceValue = null;
    private TextView mNotification = null;
    private TextView mNextDstTimeValue = null;
    private TextView mNextDstDateValue = null;
    private TextView mNextDstOffsetValue = null;
    private ImageView mNotificationImage = null;
    private CurrentTimeService mCurrentTimeService;
    private Handler mHandler = null;
    private Runnable mHandlerTask = null;
    private boolean mNotificationStatus = false;
    private PairingAlert mProgressDialog;
    private int mDstValue = 0;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_time);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mProgressDialog = new PairingAlert(this);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
        //mProgressDialog.setCancelable(false);
        mTimeValue = (TextView) findViewById(R.id.timeValue);
        mDateValue = (TextView) findViewById(R.id.dateValue);
        mAmPmValue = (TextView) findViewById(R.id.amPmValue);
        mTimezoneValue = (TextView) findViewById(R.id.timezoneValue);
        mDaylighttimeValue = (TextView) findViewById(R.id.daylighttimeValue);
        mTimesourceValue = (TextView) findViewById(R.id.timesourceValue);
        mNotification = (TextView) findViewById(R.id.notification);
        mNotificationImage = (ImageView) findViewById(R.id.notificationImage);
        mNextDstTimeValue = (TextView) findViewById(R.id.nextDstTimeValue);
        mNextDstDateValue = (TextView) findViewById(R.id.nextDstDateValue);
        mNextDstOffsetValue = (TextView) findViewById(R.id.nextDstOffsetValue);
        mCurrentTimeService = CurrentTimeService.getInstance();
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
        mNotificationStatus = AppUtils.getBooleanSharedPreference(this, Utils.CURRENT_TIME_NOTIFICATION_STATUS);
        setNotificationDetails();
        mHandler = new Handler();
        mHandlerTask = new Runnable() {
            @Override
            public void run() {
                if (mNotificationStatus) {
                    mCurrentTimeService.notifyConnectedDevices();
                }
                final Calendar now = Calendar.getInstance();
                String amPmValue;
                int value = now.get(Calendar.AM_PM);
                String minute = String.format("%02d", now.get(Calendar.MINUTE));
                mTimeValue.setText(String.valueOf(now.get(Calendar.HOUR)) + ":" + minute);
                mAmPmValue.setText(String.valueOf(now.get(Calendar.AM_PM)));
                if (value == Calendar.AM) {
                    amPmValue = (String) (getResources().getText(R.string.am));
                    mAmPmValue.setText(getResources().getText(R.string.am));
                } else {
                    amPmValue = (String) (getResources().getText(R.string.pm));
                    mAmPmValue.setText(getResources().getText(R.string.pm));
                }
                SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
                String month_name = month_date.format(now.getTime());
                SimpleDateFormat week_day = new SimpleDateFormat("EEEE");
                String weekday = week_day.format(now.getTime());
                mDateValue.setText(weekday + " , " +
                        now.get(Calendar.DAY_OF_MONTH) + " " + month_name + " " + now.get(Calendar.YEAR));
                mNextDstDateValue.setText(weekday + " , " +
                        now.get(Calendar.DAY_OF_MONTH) + " " + month_name + " " + now.get(Calendar.YEAR));
                String dstMinute = String.format("%02d", now.get(Calendar.MINUTE));
                mNextDstTimeValue.setText(String.valueOf(now.get(Calendar.HOUR)) + ":" + dstMinute + " " + amPmValue);
                mTimesourceValue.post(new Runnable() {
                    @Override
                    public void run() {
                        Calendar now = Calendar.getInstance();
                        String minute = String.format("%02d", now.get(Calendar.MINUTE));
                        mTimeValue.setText(String.valueOf(now.get(Calendar.HOUR)) + ":" + minute);

                    }

                });
                mAmPmValue.post(new Runnable() {
                    @Override
                    public void run() {
                        Calendar now = Calendar.getInstance();
                        int value = now.get(Calendar.AM_PM);
                        //mTimeValue.setText(String.valueOf(now.get(Calendar.HOUR)) + ":" + now.get(Calendar.MINUTE) );
                        mAmPmValue.setText(String.valueOf(now.get(Calendar.AM_PM)));
                        if (value == Calendar.AM)
                            mAmPmValue.setText(getResources().getText(R.string.am));
                        else
                            mAmPmValue.setText(getResources().getText(R.string.pm));
                    }

                });
                mDateValue.post(new Runnable() {
                    @Override
                    public void run() {
                        Calendar now = Calendar.getInstance();
                        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
                        String month_name = month_date.format(now.getTime());

                        SimpleDateFormat week_day = new SimpleDateFormat("EEEE");
                        String weekday = week_day.format(now.getTime());


                        mDateValue.setText(weekday + " , " +
                                now.get(Calendar.DAY_OF_MONTH) + " " + month_name + " " + now.get(Calendar.YEAR));

                    }

                });
                mTimezoneValue.post(new Runnable() {
                    @Override
                    public void run() {
                        //mTimezoneValue.setText(TimeZone.getTimeZone("UTC").getDisplayName());
                        mTimezoneValue.setText(now.getTimeZone().getDisplayName());

                    }

                });
                mDaylighttimeValue.post(new Runnable() {
                    @Override
                    public void run() {
                        mUtcValue = now.getTimeZone().getDSTSavings();
                        mUtc = getDst(mUtcValue);
                        mDaylighttimeValue.setText(mUtc);
                    }
                });
                mTimesourceValue.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            int source = Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME);
                            if (source == 1) {
                                mTimesourceValue.setText(getResources().getString(R.string.time_source_cellular_network));
                            } else {
                                mTimesourceValue.setText(getResources().getString(R.string.time_source_manual));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                   }
                });
                mNextDstOffsetValue.post(new Runnable() {
                    @Override
                    public void run() {
                        mUtcValue = TimeZone.getTimeZone("UTC").getDSTSavings();
                        mUtc = getDst(mUtcValue);
                        mNextDstOffsetValue.setText(mUtc);
                    }
                });
                mHandler.postDelayed(mHandlerTask, mRefreshInterval);
            }
        };
        mHandlerTask.run();
        getTime();
    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener=this;
        init();

        boolean status = false;
        try {

            status = BLEConnection.getmServerConnectionState() != BLEConnection.STATE_DISCONNECTED;

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!status){
            onLeDeviceDisconnected();
        }
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(CurrentTime.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(CurrentTime.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }
    private void init() {
        bluetoothCheck();
        setConnectionListener(this);
        //setGattServerListener(this);
    }

    private void bluetoothCheck() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLeDeviceConnected(String deviceName) {
        mReconnectionAlert.dismissAlert();
        init();
    }

    @Override
    public void onLeDeviceDisconnected() {
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            try {
                mReconnectionAlert.showAlert();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(BLEConnection.mReconnectionEnabled) {
                BLEConnection.gattServerAutoConnect(this);
                BLEConnection.mReconnectionEnabled = false;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null && mHandlerTask != null) {
            mHandler.removeCallbacks(mHandlerTask);
        }
    }

    public String getDst(int dstValue) {
        String dst = null;
        switch (dstValue) {
            case STD_TIME:
                dst = getResources().getString(R.string.dst_standard);
                mDstValue = 0;
                break;
            case HALF_DAYLIGHT_TIME:
                dst = getResources().getString(R.string.dst_one);
                mDstValue = 4;
                break;
            case DAYLIGHT_TIME:
                dst = getResources().getString(R.string.dst_half);
                mDstValue = 2;
                break;
            case DOUBLE_DAYLIGHT_TIME:
                dst = getResources().getString(R.string.dst_double);
                mDstValue = 8;
                break;
            default:
                break;
        }
        return dst;
    }

    public void getTime() {
        Calendar now = Calendar.getInstance();
        int value = now.get(Calendar.AM_PM);
        String minute = String.format("%02d", now.get(Calendar.MINUTE));
        mTimeValue.setText(String.valueOf(now.get(Calendar.HOUR)) + ":" + minute);
        mAmPmValue.setText(String.valueOf(now.get(Calendar.AM_PM)));
        if (value == Calendar.AM)
            mAmPmValue.setText(getResources().getText(R.string.am));
        else
            mAmPmValue.setText(getResources().getText(R.string.pm));
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        String month_name = month_date.format(now.getTime());
        SimpleDateFormat week_day = new SimpleDateFormat("EEEE");
        String weekday = week_day.format(now.getTime());
        mDateValue.setText(weekday + " , " +
                now.get(Calendar.DAY_OF_MONTH) + " " + month_name + " " + now.get(Calendar.YEAR));
    }

    public void setNotificationDetails() {
        if (mNotificationStatus) {
            mNotification.setText(getResources().getText(R.string.notification_enabled));
            mNotification.setTextColor(getResources().getColor(R.color.colorPrimary));
            mNotificationImage.setImageResource(R.drawable.notification_enabled_icon);
        } else {
            mNotification.setText(getResources().getText(R.string.notification_disabled));
            mNotificationImage.setImageResource(R.drawable.notification_disable_icon);
            mNotification.setTextColor(getResources().getColor(R.color.gray_light));
        }
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);
    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goToHome() {
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }
}
