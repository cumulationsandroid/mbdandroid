/*
 *
 *
 *     DeviceInformation.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Services.DeviceInformationService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;

import java.util.HashMap;
import java.util.List;

public class DeviceInformation extends BLEBaseActivity implements OTAUManager.OTAUProgressInfoListener {
    public SwipeRefreshLayout mSwipeRefreshLayout;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    BluetoothGattService mBluetoothGattService;
    DeviceInformationService mDeviceInformationService;
    List<BluetoothGattCharacteristic> mgattCharaList = null;
    List<BluetoothGattCharacteristic> mgattCharaListTemp = null;
    HashMap<DEVICE_INFO, String> DISHashMap;
    boolean mRefreshing = false;
    private BLEApplication bleApplication;
    private Toolbar mToolbar = null;
    private LinearLayout mScrollViewChild = null;
    private int index = 0;
    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {

            if (!mRefreshing) {
                mRefreshing = true;
                mScrollViewChild.removeAllViews();
                readingData();
            }

        }

    };
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    private static String byteToString(byte[] in) {
        final StringBuilder stringBuilder = new StringBuilder(in.length);
        if ( in.length > 0) {
            for (byte byteChar : in)
                stringBuilder.append(String.format("%02X ", byteChar));
        }
        return String.valueOf(stringBuilder);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_information);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mScrollViewChild = (LinearLayout) findViewById(R.id.scroll_child);
        mDeviceInformationService = DeviceInformationService.getInstance();
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        /**
         * Getting the application instance
         */
        bleApplication = (BLEApplication) getApplication();
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mDeviceInformationService.setGattService(mBluetoothGattService);

        setConnectionListener(this);
        setGattListener(this);

        DISHashMap = new HashMap<>();
        readingData();

    }

    @Override
    protected void onResume() {
        OTAUManager.getInstance().otauProgressInfoListener=this;
        super.onResume();

        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }

        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }

    private void readingData() {
        index = 0;
        mSwipeRefreshLayout.setRefreshing(true);
        mgattCharaList = mBluetoothGattService.getCharacteristics();// mDeviceInformationService.getData();
        mgattCharaListTemp = mgattCharaList;
        if (mgattCharaListTemp != null && mgattCharaListTemp.size() > 0) {
            mDeviceInformationService.readData(mgattCharaListTemp.get(index));
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(DeviceInformation.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(DeviceInformation.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**
         *  Handle action bar item clicks here. The action bar will
         *  automatically handle clicks on the Home/Up button, so long
         *  as you specify a parent activity in AndroidManifest.xml.
         */

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLeCharacteristicRead(boolean status, String data, int code) {
        super.onLeCharacteristicRead(status, data, code);
         DEVICE_INFO value = getCode(code);
        DISHashMap.put(value, data);


        if (mgattCharaListTemp != null && mgattCharaListTemp.size() > 0) {
            index++;

            if (mgattCharaListTemp != null && mgattCharaListTemp.size() > index) {
                mDeviceInformationService.readData(mgattCharaListTemp.get(index));
            } else {
                displayingView();
            }

        }

    }

    private void displayingView() {
        for (int i = 1; i <= 9; i++) {
            String data=null;
            DEVICE_INFO value = getCode(i);
            if (value!=null) {
                data = DISHashMap.get(value);
            }
            assert value != null;
            switch (value) {

                case MANUFACTURE_NAME:
                    addView("Manufacturer Name", data);
                    break;

                case MODEL_NUMBER:
                    addView("Model Number", data);
                    break;

                case SERIAL_NUMBER:
                    addView("Serial Number", data);
                    break;

                case HARDWARE_REVISION:
                    addView("Hardware Revision", data);
                    break;

                case FIRMWARE_REVISION:
                    addView("Firmware Revision", data);
                    break;

                case SOFTWARE_REVISION:
                    addView("Software Revision", data);
                    break;

                case SYSTEM_ID:
                    SystemID(data);
                    // addView("System ID", data);
                    break;

                case REGULATORY_LIST:
                    addViewIEEE("IEEE 11073-20601 Regulatory Certification Data List", data);
                    break;

                case PnP_ID:
                    if (data!=null)
                    PnPID(data);
                    break;

            }
        }
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
            mRefreshing = false;
        }
    }

    private DEVICE_INFO getCode(int code) {

        switch (code) {
            case 1:
                return DEVICE_INFO.MANUFACTURE_NAME;

            case 2:
                return DEVICE_INFO.MODEL_NUMBER;

            case 3:
                return DEVICE_INFO.SERIAL_NUMBER;

            case 4:
                return DEVICE_INFO.HARDWARE_REVISION;

            case 5:
                return DEVICE_INFO.FIRMWARE_REVISION;

            case 6:
                return DEVICE_INFO.SOFTWARE_REVISION;

            case 7:
                return DEVICE_INFO.SYSTEM_ID;

            case 8:
                return DEVICE_INFO.REGULATORY_LIST;

            case 9:
                return DEVICE_INFO.PnP_ID;

            default:
                return null;
        }


    }

    private void addView(String title, String data) {
        LayoutInflater mInflater = LayoutInflater.from(this);
        View rowView = mInflater.inflate(R.layout.deviceinfo_row, null);
        TextView mTitle = (TextView) rowView.findViewById(R.id.info_name);
        TextView mData = (TextView) rowView.findViewById(R.id.info);
        mTitle.setText(title);
        mData.setText(data);
        mScrollViewChild.addView(rowView);
    }

    private void addViewIEEE(String title, String data) {
        LayoutInflater mInflater = LayoutInflater.from(this);
        View rowView = mInflater.inflate(R.layout.deviceinfo_row, null);
        TextView mTitle = (TextView) rowView.findViewById(R.id.info_name);
        TextView mData = (TextView) rowView.findViewById(R.id.info);
        mTitle.setText(title);
        mData.setText("<0x" + data + ">");
        mScrollViewChild.addView(rowView);
    }


    @Override
    protected void onPause() {
        super.onPause();
        setConnectionListener(null);
        setGattListener(null);
    }

    /**
     * Method to convert hex to byteArray
     */
    private void SystemID(String result) {
        try {
            String[] splited = result.split("\\s+");
            byte[] uniqueIdentifier = new byte[3];
            byte[] manufacturerIdentifier = new byte[5];
            for (int i = 0; i < 3; i++) {
                String trimmedByte = splited[i + 5].trim();
                uniqueIdentifier[i] = (byte) convertStringtoByte(trimmedByte);

            }
            for (int i = 0; i < 5; i++) {
                String trimmedByte = splited[i].trim();
                manufacturerIdentifier[i] = (byte) convertStringtoByte(trimmedByte);

            }
            String Organizationally_Unique_Identifier = byteToString(uniqueIdentifier);
            String ManufacturerIdentifier = byteToString(manufacturerIdentifier);
            addSystemID(ManufacturerIdentifier, Organizationally_Unique_Identifier);
        } catch (Exception ignored) {

        }
    }

    private void addSystemID(String data, String data2) {
        LayoutInflater mInflater = LayoutInflater.from(this);
        View rowView = mInflater.inflate(R.layout.deviceinfo_row, null);
        TextView mTitle = (TextView) rowView.findViewById(R.id.info_name);
        TextView mData = (TextView) rowView.findViewById(R.id.info);
        TextView mTitle1 = (TextView) rowView.findViewById(R.id.info_name2);
        TextView mData1 = (TextView) rowView.findViewById(R.id.info2);
        TextView mTitle2 = (TextView) rowView.findViewById(R.id.info_name3);
        TextView mData2 = (TextView) rowView.findViewById(R.id.info3);
        mTitle.setText("System ID");
        mTitle1.setText("Manufacturer Identifier");
        mTitle2.setText("Organizationally Unique Identifier");
        mData.setVisibility(View.GONE);
        mTitle1.setVisibility(View.VISIBLE);
        mTitle2.setVisibility(View.VISIBLE);
        mData1.setVisibility(View.VISIBLE);
        mData2.setVisibility(View.VISIBLE);
        mData1.setText("<0x" + data + ">");
        mData2.setText("<0x" + data2 + ">");
        mScrollViewChild.addView(rowView);
    }

    /**
     * Method to convert hex to byteArray
     */
    private void PnPID(String result) {
        try {
            String[] splited = result.split("\\s+");
            byte[] VendorIDSource = new byte[1];
            byte[] VendorID = new byte[2];
            byte[] ProductID = new byte[2];
            byte[] ProductVersion = new byte[2];
            VendorIDSource[0] = (byte) convertStringtoByte(splited[0].trim());
            for (int i = 0; i < 2; i++) {
                String trimmedByte = splited[i + 1].trim();
                VendorID[i] = (byte) convertStringtoByte(trimmedByte);

            }
            for (int i = 0; i < 2; i++) {
                String trimmedByte = splited[i + 3].trim();
                ProductID[i] = (byte) convertStringtoByte(trimmedByte);

            }
            for (int i = 0; i < 2; i++) {
                String trimmedByte = splited[i + 5].trim();
                ProductVersion[i] = (byte) convertStringtoByte(trimmedByte);

            }
            String mVendorIDSource = byteToString(VendorIDSource);
            String mVendorID = byteToString(VendorID);
            String mProductID = byteToString(ProductID);
            String mProductVersion = byteToString(ProductVersion);
            addPnPID(mVendorIDSource, mVendorID, mProductID, mProductVersion);
        } catch (Exception ignored) {

        }
    }

    private void addPnPID(String data, String data2, String data3, String data4) {
        LayoutInflater mInflater = LayoutInflater.from(this);
        View rowView = mInflater.inflate(R.layout.deviceinfo_row, null);
        TextView mTitle = (TextView) rowView.findViewById(R.id.info_name);
        TextView mData = (TextView) rowView.findViewById(R.id.info);
        TextView mTitle1 = (TextView) rowView.findViewById(R.id.info_name2);
        TextView mData1 = (TextView) rowView.findViewById(R.id.info2);
        TextView mTitle2 = (TextView) rowView.findViewById(R.id.info_name3);
        TextView mData2 = (TextView) rowView.findViewById(R.id.info3);
        TextView mTitle3 = (TextView) rowView.findViewById(R.id.info_name4);
        TextView mData3 = (TextView) rowView.findViewById(R.id.info4);
        TextView mTitle4 = (TextView) rowView.findViewById(R.id.info_name5);
        TextView mData4 = (TextView) rowView.findViewById(R.id.info5);
        mTitle.setText("PnP ID");
        mTitle1.setText("Vendor ID Source");
        mTitle2.setText("Vendor ID");
        mTitle3.setText("Product ID");
        mTitle4.setText("Product Version");
        mData.setVisibility(View.GONE);
        mTitle1.setVisibility(View.VISIBLE);
        mTitle2.setVisibility(View.VISIBLE);
        mData1.setVisibility(View.VISIBLE);
        mTitle3.setVisibility(View.VISIBLE);
        mData3.setVisibility(View.VISIBLE);
        mTitle4.setVisibility(View.VISIBLE);
        mData4.setVisibility(View.VISIBLE);
        mData2.setVisibility(View.VISIBLE);
        mData1.setText("<0x" + data + ">");
        mData2.setText("<0x" + data2 + ">");
        mData3.setText("<0x" + data3 + ">");
        mData4.setText("<0x" + data4 + ">");
        mScrollViewChild.addView(rowView);
    }

    /**
     * Convert the string to byte
     *
     * @param string
     * @return
     */
    private int convertStringtoByte(String string) {
        return Integer.parseInt(string, 16);
    }

    public enum DEVICE_INFO {MANUFACTURE_NAME, MODEL_NUMBER, HARDWARE_REVISION, SERIAL_NUMBER, FIRMWARE_REVISION, SOFTWARE_REVISION, SYSTEM_ID, REGULATORY_LIST, PnP_ID}
}
