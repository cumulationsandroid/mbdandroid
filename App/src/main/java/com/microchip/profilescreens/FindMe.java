/*
 *
 *
 *     FindMe.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.ImmediateAlertService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;
import com.microchip.views.FindMeDevice;

import java.util.Timer;
import java.util.TimerTask;

public class FindMe extends BLEBaseActivity implements OnClickListener, OTAUManager.OTAUProgressInfoListener {
    private final static String TAG = FindMe.class.getSimpleName();
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    BLEApplication bleApplication;
    BluetoothGattService mBluetoothGattService;
    ImmediateAlertService mImmediateAlertService;
    ImageView mRadar;
    ImageView mNoAlert, mMildAlert, mHighAlert;
    TextView mTxtMsg, mFindMe;
    FindMeDevice mDevice;
    Bitmap mbitmap = null;
    Animation mBlinkAnimation = null;
    Animation rotation;
    int mMinValue = 30;
    int mMaxValue = 120;
    boolean isFirstTime = true;
    private Timer timer = null;
    private TimerTask timerTask = null;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;
    //  EditText sample;
    private PairingAlert mProgressDialog;

    //    enum CURRENT_SELECTION { NORMAL_SCREEN, NO_ALERT, MILD_ALERT, HIGH_ALERT};
//    CURRENT_SELECTION mCurrentSelection = CURRENT_SELECTION.NORMAL_SCREEN;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        setContentView(R.layout.activity_find_me);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mRadar = (ImageView) findViewById(R.id.radar);
        mNoAlert = (ImageView) findViewById(R.id.noAlert);
        mMildAlert = (ImageView) findViewById(R.id.mildAlert);
        mHighAlert = (ImageView) findViewById(R.id.highAlert);
        mTxtMsg = (TextView) findViewById(R.id.txt_msg);
        mFindMe = (TextView) findViewById(R.id.find_me);
        mDevice = (FindMeDevice) findViewById(R.id.device);
        /**
         * Getting the corresponding service and characteristic
         */
        mProgressDialog = new PairingAlert(this);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
        //mProgressDialog.setCancelable(false);
        bleApplication = (BLEApplication) getApplication();
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mImmediateAlertService = ImmediateAlertService.getInstance();
        mImmediateAlertService.setImmediateAlertService(mBluetoothGattService);
        mNoAlert.setOnClickListener(this);
        mMildAlert.setOnClickListener(this);
        mHighAlert.setOnClickListener(this);
        mFindMe.setOnClickListener(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mRadar.setImageAlpha(50);

        mDevice.setRange(mMinValue, mMaxValue);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

        mBlinkAnimation = AnimationUtils.loadAnimation(this, R.anim.tween_fade);
        rotation = AnimationUtils.loadAnimation(this, R.anim.rotation);
        rotation.setRepeatCount(Animation.INFINITE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener = this;
        init();
        bluetoothCheck();
        if (isFirstTime) {
            restoreState();
            isFirstTime = false;
        }
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }

//        BLEDataReceiver.setOnCharacteristicWriteStatusListner(this);
//        BLEDataReceiver.setOnRssiReadListener(this);
//        BLEDataReceiver.setOnDeviceDisconnectedListener(this);
        BLEConnection.readRemoteRssi();
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(FindMe.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(FindMe.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }

    private void bluetoothCheck() {

        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
              /*  Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();*/
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }

    }

    private void goToHome() {
        //Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }

    private void restoreState() {
        int state = AppUtils.getIntSharedPreference(this, AppUtils.FINE_ME_SELECTION);

        switch (state) {
            case 0:
                noAlertClick();
                break;
            case 1:
                mildAlertClick();
                break;
            case 2:
                highAlertClick();
                break;
            default:
                normalScreen();
                break;


        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        clearAnimations();
    }

    private void init() {
        //  timer = new Timer();
        // timerTask = new CustomTask();
        //  timer.scheduleAtFixedRate(timerTask, 0, 1000);
        mRadar.startAnimation(rotation);
        setConnectionListener(this);
        setGattListener(this);
    }

    private void clearAnimations() {
        if (timer != null)
            timer.cancel();
        mRadar.clearAnimation();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.noAlert:
                // if (mCurrentSelection != CURRENT_SELECTION.NO_ALERT)
                noAlertClick();
//                 int value=Integer.parseInt(sample.getText().toString());
//                mDevice.setProgress(value);
                break;
            case R.id.mildAlert:
                // if (mCurrentSelection != CURRENT_SELECTION.MILD_ALERT)
                mildAlertClick();

                break;
            case R.id.highAlert:
                // if (mCurrentSelection != CURRENT_SELECTION.HIGH_ALERT)
                highAlertClick();

                break;

        }
    }


    private void noAlertClick() {
        //mCurrentSelection= CURRENT_SELECTION.NO_ALERT;
        AppUtils.setIntSharedPreference(this, AppUtils.FINE_ME_SELECTION, 0);
        mNoAlert.setImageResource(R.drawable.no_alert_active);
        mMildAlert.setImageResource(R.drawable.mild_alert);
        mHighAlert.setImageResource(R.drawable.high_alert);
        mTxtMsg.setText(getString(R.string.no_alert_set));
        writeValue(Constants.VALUE_NO_ALERT);
        mbitmap = BitmapFactory.decodeResource(getResources(), R.drawable.moving_device);
        mDevice.clearAnimation();
        mDevice.setmBitmap(mbitmap);
    }

    private void mildAlertClick() {
        //mCurrentSelection= CURRENT_SELECTION.MILD_ALERT;
        AppUtils.setIntSharedPreference(this, AppUtils.FINE_ME_SELECTION, 1);
        mNoAlert.setImageResource(R.drawable.no_alert);
        mMildAlert.setImageResource(R.drawable.mild_alert_active);
        mHighAlert.setImageResource(R.drawable.high_alert);
        mTxtMsg.setText(getString(R.string.mild_alert_set));
        writeValue(Constants.VALUE_MID_ALERT);
        mbitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mildalert_icon);
        mDevice.setmBitmap(mbitmap);
        mBlinkAnimation.setDuration(1000);
        mDevice.startAnimation(mBlinkAnimation);
    }

    private void highAlertClick() {
        // mCurrentSelection= CURRENT_SELECTION.HIGH_ALERT;
        AppUtils.setIntSharedPreference(this, AppUtils.FINE_ME_SELECTION, 2);
        mNoAlert.setImageResource(R.drawable.no_alert);
        mMildAlert.setImageResource(R.drawable.mild_alert);
        mHighAlert.setImageResource(R.drawable.high_alert_active);
        mTxtMsg.setText(getString(R.string.hight_alert_set));
        writeValue(Constants.VALUE_HIGH_ALERT);
        mbitmap = BitmapFactory.decodeResource(getResources(), R.drawable.high_alert_icon);
        mDevice.setmBitmap(mbitmap);
        mBlinkAnimation.setDuration(250);
        mDevice.startAnimation(mBlinkAnimation);
    }

    @Override
    public void onLeCharacteristicWrite(boolean status) {

    }


    @Override
    public void onLeDeviceDisconnected() {
        //this for clear the selections
        AppUtils.setIntSharedPreference(this, AppUtils.FINE_ME_SELECTION, 4);
        normalScreen();
        clearAnimations();
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            // if (BLEConnection.isBonded()) {
            try {
                mReconnectionAlert.showAlert();
            } catch (Exception e) {
                e.printStackTrace();
            }
            BLEConnection.autoConnect(this);
           /* } else {
                Toast.makeText(this, R.string.connection_lost, Toast.LENGTH_SHORT).show();
                goToHome();
            }*/
        }
    }

    private void normalScreen() {
        // mCurrentSelection= CURRENT_SELECTION.NORMAL_SCREEN;
        mNoAlert.setImageResource(R.drawable.no_alert);
        mMildAlert.setImageResource(R.drawable.mild_alert);
        mHighAlert.setImageResource(R.drawable.high_alert);
        mTxtMsg.setText("");
        mbitmap = BitmapFactory.decodeResource(getResources(), R.drawable.moving_device);
        mDevice.clearAnimation();
        mDevice.setmBitmap(mbitmap);
    }

    @Override
    public void onLeDeviceConnected(String deviceName) {
        try {
            mReconnectionAlert.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
        bluetoothCheck();
    }

    /**
     * Method to convert hex to byteArray
     */
    private void writeValue(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertstringtobyte(trimmedByte);
            }

        }
        mImmediateAlertService.writeAlertLevelCharacteristic(valueByte);
    }

    /**
     * Convert the string to byte
     *
     * @param string
     * @return
     */
    private int convertstringtobyte(String string) {
        return Integer.parseInt(string, 16);
    }

    @Override
    public void onLeReadRemoteRssi(int rssi) {
        rssi = Math.abs(rssi);
        mDevice.setProgress(rssi);
        BLEConnection.readRemoteRssi();
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);


    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CustomTask extends TimerTask {

        @Override
        public void run() {
            BLEConnection.readRemoteRssi();
        }
    }
}
