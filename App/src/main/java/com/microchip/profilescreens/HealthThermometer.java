/*
 *
 *
 *     HealthThermometer.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Characteristics.TemperatureMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.HealthThermometerService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;


public class HealthThermometer extends BLEBaseActivity implements CompoundButton.OnCheckedChangeListener,OTAUManager.OTAUProgressInfoListener  {
    private final static String TAG = HealthThermometer.class.getSimpleName();
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    BluetoothGattService mBluetoothGattService;
    HealthThermometerService mHealthThermometerService;
    LinearLayout mPercentageLinearBottom = null, mPercentageLinearTop = null;
    private BLEApplication bleApplication;
    private ProgressBar mThermoProgress = null;
    private TextView mDegreeUnitTextView = null;
    // private TextView mDegreeFahrenhiteTextView = null;
    private SwitchCompat mNotyfySwitch = null;
    private TextView mSensorTypeTextView = null, mTextMaxValue, mNotifyText, mTxt_min_value;
    private PairingAlert mProgressDialog;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        setContentView(R.layout.activity_health_thermometer_new);
        /**
         * Getting the application instance
         */
        bleApplication = (BLEApplication) getApplication();
        /**
         * Displaying the toolbar
         */
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        /**
         * Initializing the GUI Elements
         */
        mThermoProgress = (ProgressBar) findViewById(R.id.thermoProgress);
        mDegreeUnitTextView = (TextView) findViewById(R.id.degree_celsiusTextView);
        mNotifyText = (TextView) findViewById(R.id.notifyText);
        mNotyfySwitch = (SwitchCompat) findViewById(R.id.notyfySwitch);
        mSensorTypeTextView = (TextView) findViewById(R.id.sensorTypeTextView);
        mTextMaxValue = (TextView) findViewById(R.id.txt_max_value);
        mPercentageLinearBottom = (LinearLayout) findViewById(R.id.linearPercentage);
        mPercentageLinearTop = (LinearLayout) findViewById(R.id.degree_parent);
        mProgressDialog = new PairingAlert(this);
        mTxt_min_value = (TextView) findViewById(R.id.txt_min_value);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
        //  mProgressDialog.setCancelable(false);

        mNotyfySwitch.setOnCheckedChangeListener(this);

        /**
         * Getting the service and data
         */
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mHealthThermometerService = new
                HealthThermometerService(mBluetoothGattService);
        mHealthThermometerService.
                getTemperatureMeasurementCharacteristic();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mHealthThermometerService.stopNotifyTemperatureMeasurementCharacteristic();

    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener=this;

        /**
         * Always pair the device,
         * When app enter into the page
         */

        /**
         * Registering the listners
         */
        setConnectionListener(this);
        setGattListener(this);
//        checkNotifyOn();
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        } else {
            pairing();
        }

        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(HealthThermometer.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(HealthThermometer.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }
    @Override
    public void onLeCharacteristicChanged(boolean status) {
        if (status) {

            try {
                TemperatureMeasurementCharacteristic temperatureMeasurementCharacteristic =
                        TemperatureMeasurementCharacteristic.getInstance();
                Float mTemperature = temperatureMeasurementCharacteristic.mTemperature;

                String mTemperatureUnit = temperatureMeasurementCharacteristic.mTemperatureUnit;

               // String mTemperatureType = temperatureMeasurementCharacteristic.mTemperatureType;

                /**
                 *Updating the GUI
                 */
                if (mTemperatureUnit.contains("C")) {
                    viewPositionChange(temperatureMeasurementCharacteristic.
                            mTemperature.intValue(), 50);
                    mThermoProgress.setMax(50);
                    mDegreeUnitTextView.setText(mTemperature +
                            getString(R.string.degree_celsius));
                    mTextMaxValue.setText(getString(R.string.degree_celsius_max_value));
//                    mDegreeFahrenhiteTextView.setText(AppUtils.convertToFahrenhite(mTemperature) +
//                            getString(R.string.degree_fahrenheit));
                    mTxt_min_value.setText(getString(R.string.degree_min_value));
                } else {
                    mTextMaxValue.setText(getString(R.string.degree_fahrenheit_max_value));
                    viewPositionChange(temperatureMeasurementCharacteristic.
                            mTemperature.intValue(), 120);
                    mThermoProgress.setMax(120);
                    mDegreeUnitTextView.setText(mTemperature +
                            getString(R.string.degree_fahrenheit));
//                    mDegreeCelsiusTextView.setText(AppUtils.convertToCelsius(mTemperature) +
//                            getString(R.string.degree_celsius));
                    mTxt_min_value.setText(getString(R.string.fahrenheit_min_value));
                }
                mThermoProgress.setProgress(temperatureMeasurementCharacteristic.
                        mTemperature.intValue());

                mSensorTypeTextView.setText(
                        temperatureMeasurementCharacteristic.mTemperatureType);

            } catch (Exception ignored) {

            }
        }

    }

    private void viewPositionChange(int progress, int max) {
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = 0;
        mPercentageLinearBottom.setLayoutParams(new LinearLayout.LayoutParams(width, height, progress));
        mPercentageLinearTop.setLayoutParams(new LinearLayout.LayoutParams(width, height, (max - progress)));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void pairing() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            } else {
                if (!(BLEConnection.isBonded())) {
                    BLEConnection.pairDevice();
                } else {
                    checkNotifyOn();
                }
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            } else {
                if (!(BLEConnection.isBonded())) {
                    BLEConnection.pairDevice();
                } else {
                    checkNotifyOn();
                }
            }
        }
    }

    private void checkNotifyOn() {
        if (AppUtils.getBooleanSharedPreference(this, AppUtils.FIRST_TIME)) {
            if (mNotyfySwitch.isChecked()) {

                mHealthThermometerService.startNotifyTemperatureMeasurementCharacteristic();
                mNotifyText.setText(getString(R.string.stop_notify));
            }
            AppUtils.setBooleanSharedPreference(this, AppUtils.FIRST_TIME, false);
            AppUtils.setBooleanSharedPreference(HealthThermometer.this, AppUtils.THERMOMETER_SELECTION_KEY, true);
        } else {
            if (AppUtils.getBooleanSharedPreference(this, AppUtils.THERMOMETER_SELECTION_KEY)) {
                if (mNotyfySwitch.isChecked()) {
                    mHealthThermometerService.startNotifyTemperatureMeasurementCharacteristic();
                } else {
                    mNotyfySwitch.setChecked(true);
                    mHealthThermometerService.startNotifyTemperatureMeasurementCharacteristic();
                }
                mNotifyText.setText(getString(R.string.stop_notify));
            } else {
                mNotyfySwitch.setChecked(false);
                mNotifyText.setText(getString(R.string.start_notify));
            }
        }
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
       mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();
                       /* if (mNotyfySwitch.isChecked()) {
                            mHealthThermometerService.startNotifyTemperatureMeasurementCharacteristic();
                        }*/
                        checkNotifyOn();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);


    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        AppUtils.setBooleanSharedPreference(HealthThermometer.this, AppUtils.THERMOMETER_SELECTION_KEY, b);
        if (b) {
            mNotifyText.setText(getString(R.string.stop_notify));
            mHealthThermometerService.startNotifyTemperatureMeasurementCharacteristic();

        } else {
            mNotifyText.setText(getString(R.string.start_notify));
            mHealthThermometerService.stopNotifyTemperatureMeasurementCharacteristic();
        }
    }

    @Override
    public void onLeDeviceDisconnected() {

//        mReconnectionAlert.showAlert();
//        BLEConnection.autoConnect(this);
        BLEConnection.unPairDevice();
        Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


}
