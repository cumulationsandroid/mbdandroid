/*
 *
 *
 *     HeartRate.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Characteristics.BodySensorLocationCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.HeartRateControlPointCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.HeartRateMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.HeartRateService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;

public class HeartRate extends BLEBaseActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener,OTAUManager.OTAUProgressInfoListener  {

    private final static String TAG = HeartRate.class.getSimpleName();
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    BluetoothGattService mBluetoothGattService;
    HeartRateService mHeartRateService;
    boolean mIsFromConfigChange=false;
    private TextView mEnergyExpended = null;
    private TextView mHeartRateValue = null;
    private TextView mSensorLocation = null;
    private TextView mStartText;
    private SwitchCompat mStartSwitch = null;
    private Button resetButton;
    private BLEApplication bleApplication;
    private PairingAlert mProgressDialog;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        setContentView(R.layout.activity_heart_rate);
        /**
         * Getting the application instance
         */
        bleApplication = (BLEApplication) getApplication();

        init();

        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
        //  mProgressDialog.setCancelable(false);

        /**
         * Getting the service and data
         */
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mHeartRateService = new
                HeartRateService(mBluetoothGattService);
        bleApplication.setmHeartRateService(mHeartRateService);
//        mHeartRateService.
//                getHeartRateMeasurementCharacteristic();
//        mHeartRateService.getBodySensorLocationCharac\teristic();
//        mHeartRateService.getHeartRateControlPointCharacteristic();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener=this;
        mIsFromConfigChange=false;
        /**
         * Registering the listners
         */
        setConnectionListener(this);
        setGattListener(this);

        mHeartRateService.getBodySensorLocationCharacteristic();
        mHeartRateService.readBodySensorLocationCharacteristic();

        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }

        checkBluetoothAndPairing();
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(HeartRate.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(HeartRate.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }
    private void init() {
        /**
         * Displaying the toolbar
         */
        Toolbar toolbar = (Toolbar) findViewById(R.id.hr_toolbar);
        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        /**
         * Initializing the GUI Elements
         */
        mHeartRateValue = (TextView) findViewById(R.id.heart_rate_value);
        mEnergyExpended = (TextView) findViewById(R.id.energy_expended_value);
        mSensorLocation = (TextView) findViewById(R.id.sensor_location_textView);
        mStartText = (TextView) findViewById(R.id.startText);
        mStartSwitch = (SwitchCompat) findViewById(R.id.startSwitch);
        mStartSwitch.setOnCheckedChangeListener(this);
        resetButton = (Button) findViewById(R.id.reset_button);
        resetButton.setOnClickListener(this);
        mEnergyExpended.setText("-- kj");
        mProgressDialog = new PairingAlert(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("onConfigurationChanged", "onConfigurationChanged");
        setContentView(R.layout.activity_heart_rate);
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mHeartRateService=bleApplication.getmHeartRateService();
        HeartRateMeasurementCharacteristic mHeartRateMeasurementChar =bleApplication.getmHeartRateMeasurementCharacteristic();
        if (mBluetoothGattService!=null && mHeartRateService!=null) {

            init();
            mIsFromConfigChange=true;
            checkNotifyOn();
            if (mHeartRateMeasurementChar!=null)
                GUIUpdates(mHeartRateMeasurementChar);
        }else{
            Log.d("onConfigurationChanged", "else");
        }

    }

    private void checkBluetoothAndPairing() {
        /**
         * Always pair the device,
         * When app enter into the page
         */
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }/* else {
                if (!(BLEConnection.isBonded())) {
                    BLEConnection.pairDevice();
                }
            }*/
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            } /*else {
                if (!(BLEConnection.isBonded())) {
                    Log.d(TAG, "PAIR");
                    BLEConnection.pairDevice();
                }
            }*/
        }
    }

    @Override
    protected void onPause() {
         super.onPause();
        mHeartRateService.
                getHeartRateMeasurementCharacteristic();
        mHeartRateService.stopNotifyHeartRateMeasurementCharacteristic();
//        setConnectionListener(null);
//        setGattListener(null);
    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {
        if (status) {
            try {
                HeartRateMeasurementCharacteristic heartRateMeasurementCharacteristic =
                        HeartRateMeasurementCharacteristic.getInstance();
                bleApplication.setmHeartRateMeasurementCharacteristic(heartRateMeasurementCharacteristic);
                GUIUpdates(heartRateMeasurementCharacteristic);

            } catch (Exception ignored) {

            }
        }
    }

    private void GUIUpdates(HeartRateMeasurementCharacteristic heartRateMeasurementCharacteristic) {

        mHeartRateValue.setText("" + heartRateMeasurementCharacteristic.mHeartRate + " bpm");

        if (heartRateMeasurementCharacteristic.mEnergyExpended != null) {
            mEnergyExpended.setText(""+heartRateMeasurementCharacteristic.mEnergyExpended + " kJ");
        }

        if (BodySensorLocationCharacteristic.getInstance().mBSLValue != null) {
            mSensorLocation.setText(BodySensorLocationCharacteristic.getInstance().mBSLValue);
        }
//                else {
//                    mEnergyExpended.setText("-- kj");
//                }

    }

    @Override
    public void onLeCharacteristicRead(boolean status) {
        super.onLeCharacteristicRead(status);
        if (status) {
            try {
                BodySensorLocationCharacteristic bodySensorLocationCharacteristic = BodySensorLocationCharacteristic.getInstance();
                mSensorLocation.setText(bodySensorLocationCharacteristic.mBSLValue);
                checkNotifyOn();


            } catch (Exception ignored) {

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();
                        checkNotifyOn();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);

       /* if (mStartSwitch.isChecked()) {
            mHeartRateService.getHeartRateMeasurementCharacteristic();
            mHeartRateService.startNotifyHeartRateMeasurementCharacteristic();
        }*/
    }

    private void checkNotifyOn() {
        if (AppUtils.getBooleanSharedPreference(this, AppUtils.FIRST_TIME)) {
            if (mStartSwitch.isChecked()) {
                if (!mIsFromConfigChange) {
                    mHeartRateService.
                            getHeartRateMeasurementCharacteristic();
                    mHeartRateService.startNotifyHeartRateMeasurementCharacteristic();
                }
                mStartText.setText(getString(R.string.stop_notify));
            }
            AppUtils.setBooleanSharedPreference(this, AppUtils.FIRST_TIME, false);
            AppUtils.setBooleanSharedPreference(this, AppUtils.HEART_RATE_SELECTION_KEY, true);
        } else {
            if (AppUtils.getBooleanSharedPreference(this, AppUtils.HEART_RATE_SELECTION_KEY)) {
                if (mStartSwitch.isChecked()) {
                    if (!mIsFromConfigChange) {
                        mHeartRateService.
                                getHeartRateMeasurementCharacteristic();
                        mHeartRateService.startNotifyHeartRateMeasurementCharacteristic();
                    }
                }
                else {
                    mStartSwitch.setChecked(true);
                    mHeartRateService.
                            getHeartRateMeasurementCharacteristic();
                    mHeartRateService.startNotifyHeartRateMeasurementCharacteristic();
                }
                mStartText.setText(getString(R.string.stop_notify));
            } else {
                mStartSwitch.setChecked(false);
                mStartText.setText(getString(R.string.start_notify));
            }
        }
    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        AppUtils.setBooleanSharedPreference(HeartRate.this, AppUtils.HEART_RATE_SELECTION_KEY, b);
        if (b) {
            mStartText.setText(getString(R.string.stop_notify));
            mHeartRateService.
                    getHeartRateMeasurementCharacteristic();
            mHeartRateService.startNotifyHeartRateMeasurementCharacteristic();

        } else {
            mStartText.setText(getString(R.string.start_notify));
            mHeartRateService.
                    getHeartRateMeasurementCharacteristic();
            mHeartRateService.stopNotifyHeartRateMeasurementCharacteristic();
        }
    }

    @Override
    public void onLeDeviceDisconnected() {
       /* if (BLEConnection.isBonded()) {
            BLEConnection.unPairDevice();
        }*/
        //BLEConnection.disconnect();
        Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reset_button:
                mHeartRateService.getHeartRateControlPointCharacteristic();
                HeartRateControlPointCharacteristic mHeartRateControlPointCharacteristic = HeartRateControlPointCharacteristic.getInstance();
                byte[] data = new byte[1];
                data[0] = (byte) 0x01;
                mHeartRateControlPointCharacteristic.write(data);
                break;
        }
    }
}
