/*
 *
 *
 *     ImmediateAlert.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.bleanalyser.ProximitySettingsActivity;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.ImmediateAlertService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;
import com.microchip.views.AnimatingCircle;
import com.microchip.views.CircleView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Activity class for displaying the immediate alert service details
 */
public class ImmediateAlert extends BLEBaseActivity implements View.OnClickListener, OTAUManager.OTAUProgressInfoListener {
    private final static String TAG = ImmediateAlert.class.getSimpleName();
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    BLEApplication bleApplication;
    BluetoothGattService mBluetoothGattService;
    ImmediateAlertService mImmediateAlertService;
    Boolean danger_no_alert = false;
    Boolean danger_mid_alert = false;
    Boolean danger_high_alert = false;
    Boolean mid_no_alert_mid = false;
    Boolean mid_mild_alert_mid = false;
    Boolean mid_high_alert_mid = false;
    Boolean safe_no_alert_safe = false;
    Boolean safe_mid_alert_safe = false;
    Boolean safe_high_alert_safe = false;
    AnimatingCircle mAnimatingCircle = null;
    private ImageView mSettingsImageView = null;
    private CircleView mCircleView = null;
    private TextView mRssiTextView = null;
    private Timer timer = null;
    private TimerTask timerTask = null;
    private int oldRssi = 0;
    private int mPrevRssiValue = 0;
    private Boolean mResumed = false;
    private int mSafeMin = 0;
    private int mSafeMax = 0;
    private int mMidMin = 0;
    private int mMidMax = 0;
    private int mDangerMin = 0;
    private int mDangerMax = 0;
    private PairingAlert mProgressDialog;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        setContentView(R.layout.activity_immediatealert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mProgressDialog = new PairingAlert(this);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
//        mProgressDialog.setCancelable(false);
        /**
         * Getting the corresponding service and characteristic
         */
        bleApplication = (BLEApplication) getApplication();
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mImmediateAlertService = ImmediateAlertService.getInstance();
        mImmediateAlertService.setImmediateAlertService(mBluetoothGattService);
        mSettingsImageView = (ImageView) findViewById(R.id.settingsmageView);
        mSettingsImageView.setOnClickListener(this);
        mCircleView = (CircleView) findViewById(R.id.circleButton);
        mAnimatingCircle = (AnimatingCircle) findViewById(R.id.animatingCircle);
        mRssiTextView = (TextView) findViewById(R.id.rssiValueTextView);
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener = this;
        mResumed = true;
        //init();
        setDefaultValues();
        checkingBluetoothStatus();
        setConnectionListener(this);
        setGattListener(this);
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }

        changeToolbarProgress();
        BLEConnection.readRemoteRssi();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(ImmediateAlert.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(ImmediateAlert.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }

    private void checkingBluetoothStatus() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
    }

    private void goToHome() {
        //Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }

    private void init() {
        timer = new Timer();
        // timerTask = new CustomTask();
        // timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        setConnectionListener(null);
        setGattListener(null);
        stopTimer();

    }

    private void stopTimer() {
        if (timer != null)
            timer.cancel();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.settingsmageView:
                showSettings();
                break;
        }


    }

    private void showSettings() {
        setGattListener(null);
        startActivity(new Intent(this, ProximitySettingsActivity.class));
    }

    /**
     * Method to convert hex to byteArray
     */
    private void alertValue(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertStringtoByte(trimmedByte);
            }
        }
        mImmediateAlertService.writeAlertLevelCharacteristic(valueByte);
    }

    /**
     * Convert the string to byte
     *
     * @param string
     * @return
     */
    private int convertStringtoByte(String string) {
        return Integer.parseInt(string, 16);
    }

    @Override
    public void onLeCharacteristicWrite(boolean status) {
        Log.v(TAG, "Write status" + status);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLeReadRemoteRssi(int rssi) {
        if (oldRssi != rssi) {
            mRssiTextView.setText(rssi + " dBm");
            oldRssi = rssi;
        }
        mRssiTextView.setText(rssi + " dBm");
        rssi = Math.abs(rssi);
        if (mResumed) {
            mPrevRssiValue = 0;
            mResumed = false;
        }
        if (rssi >= mSafeMin && rssi <= mSafeMax) {
            if (!(mPrevRssiValue >= mSafeMin && mPrevRssiValue <= mSafeMax)) {
                if (safe_no_alert_safe) {
                    alertValue(Constants.VALUE_NO_ALERT);
                } else if (safe_mid_alert_safe) {
                    alertValue(Constants.VALUE_MID_ALERT);
                } else {
                    alertValue(Constants.VALUE_HIGH_ALERT);
                }
            }
        } else if (rssi >= mMidMin && rssi <= mMidMax) {
            if (!(mPrevRssiValue >= mMidMin && mPrevRssiValue <= mMidMax)) {
                if (mid_no_alert_mid) {
                    alertValue(Constants.VALUE_NO_ALERT);
                } else if (mid_mild_alert_mid) {
                    alertValue(Constants.VALUE_MID_ALERT);
                } else {
                    alertValue(Constants.VALUE_HIGH_ALERT);
                }
            }
        } else if (rssi < mSafeMin) {
            if (!(mPrevRssiValue < mSafeMin)) {
                if (safe_no_alert_safe) {
                    alertValue(Constants.VALUE_NO_ALERT);
                } else if (safe_mid_alert_safe) {
                    alertValue(Constants.VALUE_MID_ALERT);
                } else {
                    alertValue(Constants.VALUE_HIGH_ALERT);
                }
            }
        } else {
            if (!(mPrevRssiValue >= mDangerMin)) {
                if (danger_no_alert) {
                    alertValue(Constants.VALUE_NO_ALERT);
                } else if (danger_mid_alert) {
                    alertValue(Constants.VALUE_MID_ALERT);
                } else {
                    alertValue(Constants.VALUE_HIGH_ALERT);
                }
            }
        }
        mAnimatingCircle.setProgress(rssi);
        mPrevRssiValue = rssi;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                BLEConnection.readRemoteRssi();
            }
        }, 2000);

    }


    @Override
    public void onLeDeviceConnected(String deviceName) {

        try {
            mReconnectionAlert.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //init();
        setDefaultValues();
        checkingBluetoothStatus();
    }

    @Override
    public void onLeDeviceDisconnected() {
        stopTimer();
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            // if (BLEConnection.isBonded()) {
            try {
                mReconnectionAlert.showAlert();
            } catch (Exception e) {
                e.printStackTrace();
            }
            BLEConnection.autoConnect(getApplicationContext());

        }
            /* else {
                Toast.makeText(this, R.string.connection_lost, Toast.LENGTH_SHORT).show();
                goToHome();
            }*/

    }

    private void setDefaultValues() {

        //Danger Alert

        String mDanger = AppUtils.getStringSharedPreference(this, AppUtils.DANGER);
        if (mDanger.length() > 0) {
            if (mDanger.equalsIgnoreCase(AppUtils.HIEGH_ALERT)) {
                danger_no_alert = false;
                danger_high_alert = true;
                danger_mid_alert = false;

            } else if (mDanger.equalsIgnoreCase(AppUtils.MILD_ALERT)) {
                danger_no_alert = false;
                danger_high_alert = false;
                danger_mid_alert = true;
            } else if (mDanger.equalsIgnoreCase(AppUtils.NO_ALERT)) {
                danger_no_alert = true;
                danger_high_alert = false;
                danger_mid_alert = false;
            }
        } else {
            danger_no_alert = false;
            danger_high_alert = true;
            danger_mid_alert = false;
        }
        // Mid Alert

        String mMid = AppUtils.getStringSharedPreference(this, AppUtils.MID);
        if (mMid.length() > 0) {
            if (mMid.equalsIgnoreCase(AppUtils.HIEGH_ALERT)) {
                mid_no_alert_mid = false;
                mid_high_alert_mid = true;
                mid_mild_alert_mid = false;
            } else if (mMid.equalsIgnoreCase(AppUtils.MILD_ALERT)) {
                mid_no_alert_mid = false;
                mid_high_alert_mid = false;
                mid_mild_alert_mid = true;
            } else if (mMid.equalsIgnoreCase(AppUtils.NO_ALERT)) {
                mid_no_alert_mid = true;
                mid_high_alert_mid = false;
                mid_mild_alert_mid = false;
            }
        } else {
            mid_no_alert_mid = false;
            mid_high_alert_mid = false;
            mid_mild_alert_mid = true;
        }

        // Safe Alert

        String mSafe = AppUtils.getStringSharedPreference(this, AppUtils.SAFE);
        if (mSafe.length() > 0) {
            if (mSafe.equalsIgnoreCase(AppUtils.HIEGH_ALERT)) {
                safe_no_alert_safe = false;
                safe_high_alert_safe = true;
                safe_mid_alert_safe = false;
            } else if (mSafe.equalsIgnoreCase(AppUtils.MILD_ALERT)) {
                safe_no_alert_safe = false;
                safe_high_alert_safe = false;
                safe_mid_alert_safe = true;
            } else if (mSafe.equalsIgnoreCase(AppUtils.NO_ALERT)) {
                safe_no_alert_safe = true;
                safe_high_alert_safe = false;
                safe_mid_alert_safe = false;
            }
        } else {
            safe_no_alert_safe = true;
            safe_high_alert_safe = false;
            safe_mid_alert_safe = false;
        }

        mSafeMin = AppUtils.getIntSharedPreference(this, AppUtils.SAFE_MIN_VALUE);
        mSafeMax = AppUtils.getIntSharedPreference(this, AppUtils.SAFE_MAX_VALUE);
        mMidMin = AppUtils.getIntSharedPreference(this, AppUtils.MILD_MIN_VALUE);
        mMidMax = AppUtils.getIntSharedPreference(this, AppUtils.MILD_MAX_VALUE);
        mDangerMin = AppUtils.getIntSharedPreference(this, AppUtils.DANGER_MIN_VALUE);
        mDangerMax = AppUtils.getIntSharedPreference(this, AppUtils.DANGER_MAX_VALUE);

        mCircleView.setRange(mSafeMin, mSafeMax, mMidMin, mMidMax, mDangerMin, mDangerMax);
        mAnimatingCircle.setRange(mSafeMin, mSafeMax, mMidMin, mMidMax, mDangerMin, mDangerMax);

    }


    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);


    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
