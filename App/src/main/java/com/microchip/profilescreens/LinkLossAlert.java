/*
 *
 *
 *     LinkLossAlert.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Characteristics.AlertLevelCharacteristic;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.LinkLossService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;
import com.microchip.views.CircleView;

import java.util.Timer;
import java.util.TimerTask;

public class LinkLossAlert extends BLEBaseActivity implements

        RadioGroup.OnCheckedChangeListener, OTAUManager.OTAUProgressInfoListener {
    private final static String TAG = LinkLossAlert.class.getSimpleName();
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    BLEApplication bleApplication;
    BluetoothGattService mBluetoothGattService;
    LinkLossService mLinkLossService;
    int mReadValue = 0;
    //    Button mNoAlertBTN;
//    Button mMildAlertBTN;
//    Button mHighAlertBTN;
    private PairingAlert mProgressDialog;
    private ImageView mSettingsImageView = null;
    private CircleView mCircleView = null;
    private TextView mRssiTextView = null;
    private Timer timer = null;
    private TimerTask timerTask = null;
    private int mRssi = 0;
    private ImageView mLockImageView = null;
    private LinearLayout mLockOverLay = null;
    private AnimationDrawable mFrameAnimation = null;
    private RadioGroup mLinkLossRadioGroup = null;
    private RadioButton mLinkLossNoAlertButton = null;
    private RadioButton mLinkLossMidAlertButton = null;
    private RadioButton mLinkLossHieghAlertButton = null;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        setContentView(R.layout.activity_linkloss);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mProgressDialog = new PairingAlert(this);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
        // mProgressDialog.setCancelable(false);
        mLockImageView = (ImageView) findViewById(R.id.LockView);
        mLockOverLay = (LinearLayout) findViewById(R.id.lockOverLay);
        /**
         * LinlLoss Alert
         */
        mLinkLossRadioGroup = (RadioGroup) findViewById(R.id.linkLossRadioGroup);
        mLinkLossNoAlertButton = (RadioButton) findViewById(R.id.linkLossNoAlertRadioButton);
        mLinkLossMidAlertButton = (RadioButton) findViewById(R.id.linkLossMidAlertRadioButton);
        mLinkLossHieghAlertButton = (RadioButton) findViewById(R.id.linkLossHeighAlertRadioButton);
        mLinkLossRadioGroup.setOnCheckedChangeListener(this);
        /**
         * Getting the corresponding service and characteristic
         */
        bleApplication = (BLEApplication) getApplication();
        mBluetoothGattService = bleApplication.getmBluetoothGattService();
        mLinkLossService = LinkLossService.getInstance();
        mLinkLossService.setLinkLossAlertService(mBluetoothGattService);
        mLinkLossService.getAlertLevel();

        /**
         * Getting the default alert value of link loss
         */
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener = this;
        init();


        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(LinkLossAlert.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(LinkLossAlert.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }

    private void init() {

        mLinkLossService.readAlertLevel();
        setGattListener(this);
        setConnectionListener(this);
        bluetoothCheck();
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
    }

    private void bluetoothCheck() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
    }

    private void goToHome() {
        //Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onLeCharacteristicWrite(boolean status) {

    }


    /**
     * Method to convert hex to byteArray
     */
    private void alertLinklossValue(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertstringtobyte(trimmedByte);
            }

        }
        mLinkLossService.writeAlertLevelCharacteristic(valueByte);
    }

    /**
     * Convert the string to byte
     *
     * @param string
     * @return
     */
    private int convertstringtobyte(String string) {
        return Integer.parseInt(string, 16);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mLockOverLay.getVisibility() == View.VISIBLE) {
                goToHomeOnDisconnection();
            } else {
                setGattListener(null);
                setConnectionListener(null);
//                BLEDataReceiver.setOnCharacteristicReadStatusListner(null);
//                BLEDataReceiver.setOnCharacteristicWriteStatusListner(null);
//                BLEDataReceiver.setOnRssiReadListener(null);
//                BLEDataReceiver.setOnDeviceConnectedListner(null);
//                BLEDataReceiver.setOnDeviceDisconnectedListener(null);
//                BLEDataReceiver.setOnCharacteristicReadStatusListner(null);
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLeDeviceDisconnected() {
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            // if (BLEConnection.isBonded()) {
            playSound("pathLoss.wav");

            BLEConnection.autoConnect(getApplicationContext());

            for (int i = 0; i < mLinkLossRadioGroup.getChildCount(); i++) {
                mLinkLossRadioGroup.getChildAt(i).setEnabled(false);
            }
            mLockOverLay.setVisibility(View.VISIBLE);
            mLockImageView.setImageResource(R.drawable.lock_anim);
            // Get the background, which has been compiled to an AnimationDrawable object.
            mFrameAnimation = (AnimationDrawable) mLockImageView.getDrawable();
            // Start the animation (looped playback by default).
            mFrameAnimation.start();

        }
        /*else {
                Toast.makeText(this, R.string.connection_lost, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }*/
    }


    private void goToHomeOnDisconnection() {
        try {
            // BLEConnection.disconnect();
            // setConnectionListener(null);
            // BLEDataReceiver.setOnDeviceConnectedListner(null);
            Intent intent = new Intent(LinkLossAlert.this, HomeScreen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
            startActivity(intent);
            finish();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        //setGattListener(null);
        // setConnectionListener(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //BLEDataReceiver.setOnDeviceDisconnectedListener(null);
        if (mLockOverLay.getVisibility() == View.VISIBLE) {
            goToHomeOnDisconnection();
        }
    }


    @Override
    public void onLeDeviceConnected(String deviceName) {
        for (int i = 0; i < mLinkLossRadioGroup.getChildCount(); i++) {
            mLinkLossRadioGroup.getChildAt(i).setEnabled(true);
        }
        mFrameAnimation.stop();
        mLockImageView.setImageResource(R.drawable.unlock);
        mLockOverLay.setVisibility(View.GONE);

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        if (radioGroup.getId() == mLinkLossRadioGroup.getId()) {
            setLinkLossValue(radioGroup.getCheckedRadioButtonId());

        }

    }

    /**
     * set linkloss {@link android.content.SharedPreferences} values to {@link android.content.SharedPreferences}
     *
     * @param checkedRadioButtonId
     */
    private void setLinkLossValue(int checkedRadioButtonId) {

        if (mLinkLossNoAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(this, AppUtils.LINKLOSS, AppUtils.NO_ALERT);
            if (mReadValue != 0) // to avoid the writing after read the alert value
            {
                mReadValue = 0;
                alertLinklossValue(Constants.VALUE_NO_ALERT);
            }
        } else if (mLinkLossMidAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(this, AppUtils.LINKLOSS, AppUtils.MILD_ALERT);
            if (mReadValue != 1) {
                mReadValue = 1;
                alertLinklossValue(Constants.VALUE_MID_ALERT);
            }
        } else if (mLinkLossHieghAlertButton.getId() == checkedRadioButtonId) {
            AppUtils.setStringSharedPreference(this, AppUtils.LINKLOSS, AppUtils.HIEGH_ALERT);
            if (mReadValue != 2) {
                mReadValue = 2;
                alertLinklossValue(Constants.VALUE_HIGH_ALERT);
            }
        }
    }

    @Override
    public void onLeCharacteristicRead(boolean status) {
        if (status) {
            AlertLevelCharacteristic alertLevelCharacteristic =
                    AlertLevelCharacteristic.getInstance();
            mReadValue = alertLevelCharacteristic.mAlertValue;
            switch (mReadValue) {
                case 0:
                    mLinkLossNoAlertButton.setChecked(true);
                    //  mLinkLossMidAlertButton.setChecked(false);
                    //  mLinkLossHieghAlertButton.setChecked(false);
                    //AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.LINKLOSS, AppUtils.NO_ALERT);
                    break;
                case 1:
                    //  mLinkLossNoAlertButton.setChecked(false);
                    mLinkLossMidAlertButton.setChecked(true);
                    //  mLinkLossHieghAlertButton.setChecked(false);
                    //AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.LINKLOSS, AppUtils.MILD_ALERT);
                    break;
                case 2:
                    //   mLinkLossNoAlertButton.setChecked(false);
                    //   mLinkLossMidAlertButton.setChecked(false);
                    mLinkLossHieghAlertButton.setChecked(true);
                    //AppUtils.setStringSharedPreference(ProximitySettingsActivity.this, AppUtils.LINKLOSS, AppUtils.HIEGH_ALERT);
                    break;
            }

        }
    }

    private void playSound(String fileName) {
        MediaPlayer p = new MediaPlayer();
        try {
            AssetFileDescriptor afd = this.getAssets().openFd(fileName);
            p.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
            p.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        p.start();
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();
                        init();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);


    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
