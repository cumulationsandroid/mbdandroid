/*
 *
 *
 *     OTAUFileListModel.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens.OTAU;

import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUCommandGetDeviceInformation;
import com.atmel.blecommunicator.com.atmel.otau.fileread.FileReader;
import com.atmel.blecommunicator.com.atmel.otau.fileread.HeaderModel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Data Model class for OTA File
 */
public class OTAUFileListModel {
    /**
     * File name
     */
    private String mFileName = null;
    /**
     * File path
     */
    private String mFilePath = null;
    /**
     * File firmware
     */
    private String mFileFirmwareVersion = null;
    /*
    * File parent
    */
    private String mFileUpgradetype = null;

    // Constructor
    public OTAUFileListModel(String fileName, String filePath) {
        super();
        this.mFileName = fileName;
        this.mFilePath = filePath;
        this.mFileFirmwareVersion = readFirmware(filePath);
        this.mFileUpgradetype = readUpdateType(filePath);
    }

    public OTAUFileListModel() {
        super();
    }

    /**
     * Read the given binary file, and return its contents as a byte array.
     */
    public static String readFirmware(String aInputFileName) {
        File file = new File(aInputFileName);
        byte[] result = null;
        try {
            InputStream input = new BufferedInputStream(new FileInputStream(file));
            result = FileReader.readAndClose(input);
            HeaderModel.headerByteArray = result;
            HeaderModel.headerHexString = FileReader.bytesToHex(result);
            return "" + HeaderModel.getFirmwareMajorNumber() + "." + HeaderModel.getFirmwareMinorNumber() + "(" + HeaderModel.getFirmwareBuildNumber() + ")";
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Read the given binary file, and return its contents as a byte array.
     */
    public static String readUpdateType(String aInputFileName) {
        File file = new File(aInputFileName);
        byte[] result = null;
        try {
            InputStream input = new BufferedInputStream(new FileInputStream(file));
            result = FileReader.readAndClose(input);
            HeaderModel.headerByteArray = result;
            HeaderModel.headerHexString = FileReader.bytesToHex(result);
            if (HeaderModel.getFileVersion().compareTo(OTAUCommandGetDeviceInformation.getFileVersion()) > 0) {
                return "Upgrade";
            } else if (HeaderModel.getFileVersion().compareTo(OTAUCommandGetDeviceInformation.getFileVersion()) < 0) {
                return "Force Downgrade";
            } else if (HeaderModel.getFileVersion().compareTo(OTAUCommandGetDeviceInformation.getFileVersion()) == 0) {
                return "Force Upgrade";
            } else {
                return "Upgrade";
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String getmFileUpgradetype() {
        return mFileUpgradetype;
    }

    public void setmFileUpgradetype(String mFileUpgradetype) {
        this.mFileUpgradetype = mFileUpgradetype;
    }

    public String getmFileFirmwareVersion() {
        return mFileFirmwareVersion;
    }

    public void setmFileFirmwareVersion(String mFileFirmwareVersion) {
        this.mFileFirmwareVersion = mFileFirmwareVersion;
    }

    public String getFileName() {
        return mFileName;
    }

    public void setFileName(String mFileName) {
        this.mFileName = mFileName;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setName(String mFilePath) {
        this.mFilePath = mFilePath;
    }
}
