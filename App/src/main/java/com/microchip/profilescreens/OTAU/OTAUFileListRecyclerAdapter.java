/*
 *
 *
 *     OTAUFileListRecyclerAdapter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens.OTAU;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microchip.R;

import java.util.ArrayList;
import java.util.List;

public class OTAUFileListRecyclerAdapter extends RecyclerView.Adapter<OTAUFileListRecyclerAdapter.OTAUFileViewHolder> {


    private List<OTAUFileListModel> fileList;
    private Context mContext;

    public OTAUFileListRecyclerAdapter(Context context, List<OTAUFileListModel> files) {
        this.fileList = files;
        this.mContext = context;
    }

    @Override
    public OTAUFileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_otau_file, parent, false);
        return new OTAUFileViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OTAUFileViewHolder holder, int position) {
        OTAUFileListModel file = fileList.get(position);
        holder.fileName.setText(file.getFileName());
        holder.firmwareVersion.setText(file.getmFileFirmwareVersion());
        holder.upgradeType.setText(file.getmFileUpgradetype());
        if (file.getmFileUpgradetype().equalsIgnoreCase("force upgrade")) {
            holder.upgradeType.setTextColor(mContext.getResources().getColor(R.color.gray));
        } else if (file.getmFileUpgradetype().equalsIgnoreCase("force downgrade")) {
            holder.upgradeType.setTextColor(mContext.getResources().getColor(R.color.red));
        } else {
            holder.upgradeType.setTextColor(mContext.getResources().getColor(R.color.colorTextBlue));
        }
    }

    @Override
    public int getItemCount() {
        return fileList.size();
    }

    public void addFiles(ArrayList<OTAUFileListModel> fileModel) {
        this.fileList = fileModel;
    }

    public class OTAUFileViewHolder extends RecyclerView.ViewHolder {
        public TextView fileName, firmwareVersion, upgradeType;

        public OTAUFileViewHolder(View view) {
            super(view);
            fileName = (TextView) view.findViewById(R.id.file_name);
            firmwareVersion = (TextView) view.findViewById(R.id.file_firm_version);
            upgradeType = (TextView) view.findViewById(R.id.file_upgrade_type);
        }
    }

}
