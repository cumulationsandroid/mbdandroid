/*
 *
 *
 *     OTAUFilesListingActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens.OTAU;

import android.app.Activity;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.microchip.utils.AppUtils;

import java.io.File;
import java.util.ArrayList;

public class OTAUFilesListingActivity extends BLEBaseActivity {

    public static Boolean mApplicationInBackground = false;
    //Constants
    private final ArrayList<OTAUFileListModel> mArrayListFiles = new ArrayList<OTAUFileListModel>();
    private Toolbar mToolBar;
    private BLELollipopScanner mBleLollipopScanner;
    private BLEKitKatScanner mBleKitkatScanner;
    private OTAUFileListRecyclerAdapter mFirmwareAdapter;
    private RecyclerView mFileListRecycler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_otau_file_listing);

        /**
         * Setting ToolBar
         */
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        /**
         * Set listeners
         */
        setGattListener(this);
        setConnectionListener(this);

        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

        mFileListRecycler = (RecyclerView) findViewById(R.id.otau_file_recycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mFileListRecycler.setLayoutManager(mLayoutManager);
        mFileListRecycler.setItemAnimator(new DefaultItemAnimator());

        /**
         * Shows the bin file in the device
         */
        File fileDirectory = new File(Environment.getExternalStorageDirectory()
                + File.separator + "Atmel");
        mFirmwareAdapter = new OTAUFileListRecyclerAdapter(this, mArrayListFiles);
        mFileListRecycler.setAdapter(mFirmwareAdapter);
        mFileListRecycler.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mFileListRecycler, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                OTAUFileListModel file = mArrayListFiles.get(position);
                if (file.getmFileUpgradetype().equalsIgnoreCase("Force Downgrade")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(OTAUFilesListingActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Alert!");
                    builder.setMessage(file.getmFileUpgradetype() + " not possible!");
                    builder.setPositiveButton("OK", null);
                    builder.show();

                } else {

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("filename", file.getFileName());
                    returnIntent.putExtra("filepath", file.getFilePath());
                    returnIntent.putExtra("firmware", file.getmFileFirmwareVersion());
                    returnIntent.putExtra("upgradetype", file.getmFileUpgradetype());

                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }

            @Override
            public void onLongClick(View view, final int position) {
                final int deletePosition = position;
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        OTAUFilesListingActivity.this, R.style.AppCompatAlertDialogStyle);
                alert.setTitle("Delete");
                alert.setMessage("Do you want delete this item?");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        File file = new File(mArrayListFiles.get(deletePosition).getFilePath());
                        file.delete();
                        mArrayListFiles.remove(deletePosition);
                        mFirmwareAdapter.notifyDataSetChanged();
                    }
                });
                alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        }));

        try {
            searchRequiredFile(fileDirectory);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mFirmwareAdapter.notifyDataSetChanged();

    }

    /**
     * Method to search phone/directory for the .bin files
     *
     * @param dir
     */
    void searchRequiredFile(File dir) {
        if (dir.exists()) {
            String filePattern = "bin";
            File[] allFilesList = dir.listFiles();
            for (int pos = 0; pos < allFilesList.length; pos++) {
                File analyseFile = allFilesList[pos];
                if (analyseFile != null) {
                    if (analyseFile.isDirectory()) {
                        searchRequiredFile(analyseFile);
                    } else {
                        Uri selectedUri = Uri.fromFile(analyseFile);
                        String fileExtension
                                = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                        if (fileExtension.equalsIgnoreCase(filePattern)) {
                            OTAUFileListModel fileModel = new OTAUFileListModel(analyseFile.getName(),
                                    analyseFile.getAbsolutePath());
                            mArrayListFiles.add(fileModel);
                            mFirmwareAdapter.addFiles(mArrayListFiles);
                        }
                    }

                }
            }
        } else {
            Toast.makeText(this, "Directory does not exist", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        setGattListener(null);
        setConnectionListener(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        setGattListener(this);
        setConnectionListener(this);
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    /*
    * Item Click listening for RecyclerView
     */
    public interface ClickListener {

        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private OTAUFilesListingActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final OTAUFilesListingActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}



