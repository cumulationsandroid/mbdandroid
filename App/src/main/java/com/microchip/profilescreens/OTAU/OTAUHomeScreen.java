/*
 *
 *
 *     OTAUHomeScreen.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens.OTAU;

import android.app.Activity;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.OTAUService;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUCommandGetDeviceInformation;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.FileReader;
import com.microchip.utils.AppUtils;

import java.io.File;

public class OTAUHomeScreen extends BLEBaseActivity {

    public static final int SELECT_FILE_REQ_CODE = 1;
    private final static String TAG = OTAUHomeScreen.class.getSimpleName();
    public String mOTAUStatusText;
    public int mOTAUProgress;
    private BLEApplication mBleApplication;
    private OTAUService mOTAUService;
    private BLELollipopScanner mBleLollipopScanner;
    private BLEKitKatScanner mBleKitkatScanner;
    private Toolbar mToolBar;
    private TextView mOTAUStatusTextView, mProductIdView, mVendorIdView, mFirmwareVersionView, mHardwareVersionView, mHardwareRevisionView, mOTAUProgressText;
    private TextView mFileName, mFileSize, mFileFirmwareVersion;
    private LinearLayout mSelectFileLayout;
    private Button mUpgradeButton;
    private String upgradetype;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_otau_home);

        mProductIdView = (TextView) findViewById(R.id.product_id);
        mVendorIdView = (TextView) findViewById(R.id.vendor_id);
        mFirmwareVersionView = (TextView) findViewById(R.id.firmware_version);
        mHardwareVersionView = (TextView) findViewById(R.id.hardware_version);
        mHardwareRevisionView = (TextView) findViewById(R.id.hardware_revision);
        mSelectFileLayout = (LinearLayout) findViewById(R.id.otau_file_selector);

        mProductIdView.setText("0x" + String.format("%02X", ((byte) OTAUCommandGetDeviceInformation.PRODUCT_ID)) + String.format("%02X", (byte) (OTAUCommandGetDeviceInformation.PRODUCT_ID >> 8)));
        mVendorIdView.setText("0x" + String.format("%02X", (byte) OTAUCommandGetDeviceInformation.VENDOR_ID) + String.format("%02X", (byte) (OTAUCommandGetDeviceInformation.VENDOR_ID >> 8)));
        mFirmwareVersionView.setText("" + (byte) OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MAJOR + "." + (byte) (OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MINOR)
                + "(" + (byte) (OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_BUILD) + ")");
        mHardwareVersionView.setText("" + (byte) OTAUCommandGetDeviceInformation.HARDWARE_VERSION_MAJOR + "." + (byte) (OTAUCommandGetDeviceInformation.HARDWARE_VERSION_MINOR));
        mHardwareRevisionView.setText("" + OTAUCommandGetDeviceInformation.HARDWARE_REVISION);

        mFileName = (TextView) findViewById(R.id.file_name);
        mFileSize = (TextView) findViewById(R.id.otau_file_size);
        mFileFirmwareVersion = (TextView) findViewById(R.id.otau_update_firm_version);
        mUpgradeButton = (Button) findViewById(R.id.otau_upgrade_start);

        /**
         * Setting ToolBar
         */
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mSelectFileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fileSelection = new Intent(OTAUHomeScreen.this, OTAUFilesListingActivity.class);
                startActivityForResult(fileSelection, SELECT_FILE_REQ_CODE);
            }
        });
        mUpgradeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.setIntSharedPreference(OTAUHomeScreen.this, "PREF_OTAU_PROGRESS", 0);
                Intent otauIntent = new Intent(OTAUHomeScreen.this, OTAUServiceStatus.class);
                startActivity(otauIntent);
                OTAUManager.getInstance().mUpgradeType = upgradetype;
                OTAUManager.getInstance().startOTAUUpdateForced();
                finish();
            }
        });

        /**
         * Getting the application instance
         */
        mBleApplication = (BLEApplication) getApplication();

        /**
         * Getting the services and the data
         */
        BluetoothGattService mBluetoothOTAGATTService = mBleApplication.getBluetoothOTAGattService();

        /**
         * Set listeners
         */
        setGattListener(this);
        setConnectionListener(this);

        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String filename = data.getStringExtra("filename");
                String filepath = data.getStringExtra("filepath");
                String filefwver = data.getStringExtra("firmware");
                upgradetype = data.getStringExtra("upgradetype");

                Log.d("File Info>>> ", filename + " " + filepath + " " + filefwver);
                mFileName.setText(filename);
                mFileFirmwareVersion.setText(filefwver);
                mFileSize.setText(new File(filepath).length() + " bytes");

                //Reading selected file
                FileReader.setFileSelected(filepath);

                if (upgradetype.equalsIgnoreCase("force upgrade")) {
                    mUpgradeButton.setVisibility(View.VISIBLE);
                    mUpgradeButton.setBackgroundColor(getResources().getColor(R.color.gray));
                    mUpgradeButton.setText(upgradetype);
                } else if (upgradetype.equalsIgnoreCase("force downgrade")) {
                    mUpgradeButton.setVisibility(View.VISIBLE);
                    mUpgradeButton.setBackgroundColor(getResources().getColor(R.color.red));
                    mUpgradeButton.setText(upgradetype);
                } else {
                    mUpgradeButton.setVisibility(View.VISIBLE);
                    mUpgradeButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    mUpgradeButton.setText(upgradetype);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                mUpgradeButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {
        super.onLeCharacteristicChanged(status);
        if (status) {
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        setGattListener(null);
        setConnectionListener(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        setGattListener(this);
        setConnectionListener(this);
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
    }


}

