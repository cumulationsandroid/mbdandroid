/*
 *
 *
 *     OTAUServiceStatus.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens.OTAU;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.R;
import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.OTAUService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUCommandGetDeviceInformation;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.microchip.utils.AppUtils;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

public class OTAUServiceStatus extends BLEBaseActivity implements OTAUManager.OTAUProgressInfoListener {

    public String mOTAUStatusText;
    public int mOTAUProgress;
    private BLEApplication mBleApplication;
    private BluetoothGattService mBluetoothGattService, mBluetoothOTAGATTService;
    private OTAUService mOTAUService;
    private BLELollipopScanner mBleLollipopScanner;
    private BLEKitKatScanner mBleKitkatScanner;
    private Toolbar mToolBar;
    private TextView mOTAUStatusTextView, mProductIdView, mVendorIdView, mFirmwareVersionView, mHardwareVersionView, mHardwareRevisionView, mOTAUProgressText;
    private CircularProgressBar mCircularProgressBar;
    private Button mOtauUpgradePauseResume;
    private ProgressBar mProgressBar;
    private TextView mOTAUPauseResume;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_otau_status);
        OTAUManager.getInstance().otauProgressInfoListener = this;
        mOTAUProgressText = (TextView) findViewById(R.id.otau_circular_percentage);
        mOTAUStatusTextView = (TextView) findViewById(R.id.otau_status_text);
        mCircularProgressBar = (CircularProgressBar) findViewById(R.id.otau_cicular_progress);
        mProductIdView = (TextView) findViewById(R.id.product_id);
        mVendorIdView = (TextView) findViewById(R.id.vendor_id);
        mFirmwareVersionView = (TextView) findViewById(R.id.firmware_version);
        mHardwareVersionView = (TextView) findViewById(R.id.hardware_version);
        mHardwareRevisionView = (TextView) findViewById(R.id.hardware_revision);
        mOtauUpgradePauseResume = (Button) findViewById(R.id.otau_upgrade_pause);
        mProgressBar = (ProgressBar) findViewById(R.id.otau_small_progress);

        mProductIdView.setText("0x" + String.format("%02X", ((byte) OTAUCommandGetDeviceInformation.PRODUCT_ID)) + String.format("%02X", (byte) (OTAUCommandGetDeviceInformation.PRODUCT_ID >> 8)));
        mVendorIdView.setText("0x" + String.format("%02X", (byte) OTAUCommandGetDeviceInformation.VENDOR_ID) + String.format("%02X", (byte) (OTAUCommandGetDeviceInformation.VENDOR_ID >> 8)));
        mFirmwareVersionView.setText("" + (byte) OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MAJOR + "." + (byte) (OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MINOR)
                + "(" + (byte) (OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_BUILD) + ")");
        mHardwareVersionView.setText("" + (byte) OTAUCommandGetDeviceInformation.HARDWARE_VERSION_MAJOR + "." + (byte) (OTAUCommandGetDeviceInformation.HARDWARE_VERSION_MINOR));
        mHardwareRevisionView.setText("" + OTAUCommandGetDeviceInformation.HARDWARE_REVISION);
        mCircularProgressBar.setProgressWithAnimation(AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS"), 100);
        mOTAUProgressText.setText(AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS") + "%");

        /**
         * Setting ToolBar
         */
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mOTAUPauseResume = (TextView) findViewById(R.id.action_pause_resume);
        mOTAUPauseResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOTAUPauseResume.getText().toString().equalsIgnoreCase(getResources().getString(R.string.otau_pause))) {
                    OTAUManager.getInstance().pauseOTAUUpdate();
                    Utils.setBooleanSharedPreference(OTAUServiceStatus.this, Constants.OTAU_PAUSE_STATUS_NAME_KEY, true);
                } else {
                    mOTAUPauseResume.setText(getResources().getText(R.string.otau_pause));
                    OTAUManager.getInstance().resumeOTAUUpdate();
                    Utils.setBooleanSharedPreference(OTAUServiceStatus.this, Constants.OTAU_PAUSE_STATUS_NAME_KEY, false);
                }
            }
        });
        /**
         * Getting the application instance
         */
        mBleApplication = (BLEApplication) getApplication();

        /**
         * Getting the services and the data
         */
        mBluetoothGattService = mBleApplication.getmBluetoothGattService();
        mBluetoothOTAGATTService = mBleApplication.getBluetoothOTAGattService();

        /**
         * Set listeners
         */
        setGattListener(this);
        setConnectionListener(this);

        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }


    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {
        super.onLeCharacteristicChanged(status);
        if (status) {
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        setGattListener(null);
        setConnectionListener(null);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener = this;
   /*     if (!OTAUManager.getInstance().mOTAUPauseFlag) {
            mOTAUPauseResume.setText(getResources().getText(R.string.otau_pause));
        } else {
            mOTAUPauseResume.setText(getResources().getText(R.string.otau_resume));
        }*/
        if (OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Force Upgrade") || AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS") >= 98) {
            mOTAUPauseResume.setEnabled(false);
            mOTAUPauseResume.setTextColor(getResources().getColor(com.atmel.beacon.R.color.textColour));
        } else {
            mOTAUPauseResume.setEnabled(true);
            mOTAUPauseResume.setTextColor(getResources().getColor(com.atmel.beacon.R.color.white));
        }

        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            mOTAUPauseResume.setText(getResources().getText(R.string.otau_resume));
            OTAUProgressUpdater("Paused", 0);
        } else {
            mOTAUPauseResume.setText(getResources().getText(R.string.otau_pause));

        }

        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        setGattListener(this);
        setConnectionListener(this);
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
    }


    @Override
    public void OTAUProgressUpdater(final String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));

        Log.e("OTAUServiceStatus", "Status " + status + "Progress " + mOTAUProgress + " " + AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS"));
        this.runOnUiThread(new Runnable() {
            public void run() {
                if (status.equalsIgnoreCase("Upgrading...")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    if (OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Upgrade")) {
                        mOTAUStatusText = "Upgrading...";
                    } else if (OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Force Upgrade")) {
                        mOTAUStatusText = "Force upgrading...";
                    }
                    if (AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS") > 0) {
                        mOTAUStatusText = "Resuming...";
                    }
                } else if (status.equalsIgnoreCase("Resuming Update")) {
                    OTAUManager.getInstance().startOTAUUpdate();
                    mOTAUProgressText.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.VISIBLE);
                } else if (status.equalsIgnoreCase("Pausing...") || status.equalsIgnoreCase("Resuming...")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    if (OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Force Upgrade")) {
                        mOTAUPauseResume.setEnabled(false);
                        mOTAUPauseResume.setTextColor(getResources().getColor(com.atmel.beacon.R.color.textColour));
                    } else {
                        mOTAUPauseResume.setEnabled(false);
                    }
                } else if (status.equalsIgnoreCase("Paused")) {
                    mOTAUProgressText.setText(AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS") + "%");
                    mCircularProgressBar.setProgressWithAnimation(AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS"), 100);
                    mProgressBar.setVisibility(View.GONE);

                    if (OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Force Upgrade")) {
                        mOTAUPauseResume.setEnabled(false);
                        mOTAUPauseResume.setTextColor(getResources().getColor(com.atmel.beacon.R.color.textColour));
                    } else {
                        mOTAUPauseResume.setEnabled(true);
                        mOTAUPauseResume.setText(getResources().getText(R.string.otau_resume));
                    }
                } else if (status.equalsIgnoreCase("Resumed")) {
                    mOTAUProgressText.setText(AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS") + "%");
                    mCircularProgressBar.setProgressWithAnimation(AppUtils.getIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS"), 100);
                    mProgressBar.setVisibility(View.VISIBLE);
                    mOTAUStatusText = "Resuming...";
                } else if (status.equalsIgnoreCase("OTAU Completed")) {
                    Toast.makeText(OTAUServiceStatus.this, "OTAU has been successfully completed", Toast.LENGTH_LONG).show();
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    if (OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Force Upgrade")) {
                        mOTAUPauseResume.setEnabled(false);
                        mOTAUPauseResume.setTextColor(getResources().getColor(com.atmel.beacon.R.color.textColour));
                    } else {
                        mOTAUPauseResume.setEnabled(true);
                    }
                }
                mOTAUStatusTextView.setText(mOTAUStatusText);
                if (mOTAUProgress > 0) {
                    AppUtils.setIntSharedPreference(OTAUServiceStatus.this, "PREF_OTAU_PROGRESS", mOTAUProgress);
                    mOTAUProgressText.setText((mOTAUProgress) + "%");
                    mCircularProgressBar.setProgressWithAnimation(mOTAUProgress, 100);
                }
                if (mOTAUProgress >= 98) {
                    mOTAUPauseResume.setEnabled(false);
                    mOTAUPauseResume.setTextColor(getResources().getColor(com.atmel.beacon.R.color.textColour));
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}

