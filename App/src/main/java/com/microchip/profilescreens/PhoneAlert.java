/*
 *
 *
 *     PhoneAlert.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.microchip.profilescreens;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.services.PAService;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;

/**
 * Created by Atmel on 19/8/15.
 */
public class PhoneAlert extends BLEBaseActivity implements OTAUManager.OTAUProgressInfoListener  {
    final int isRing = 1, isVibrate = 2, isSilent = 3;
    public String mOTAUStatusText;
    public int mOTAUProgress;
//    String RINGER_MODE_SILENT = "0x00";
//    String RINGER_MODE_VIBRATE = "0x00";
//    String RINGER_MODE_NORMAL = "0x01";
    //AudioManager mAudioManager;
    int silent = 0, vibrate = 0, normal = 0, display = 1;
    boolean mNotificationEnable = true;
    TextView mRingerStatus, mControlPoint;
    TextView ringerStatusTextView, vibrateStatusTV, silentTextviView;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;
    private BLELollipopScanner mBleLollipopScanner;
    private BLEKitKatScanner mBleKitkatScanner;
    //private PhoneAlertService mAlertService;
    //private SettingsContentObserver mSettingsContentObserver;
    private PairingAlert mProgressDialog;
    private boolean mAutoConnectTriggered = false;
    private BroadcastReceiver PASServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case AppConstants.PAS_CONTROL_POINT:
                    setRingerStatus(intent.getIntExtra(AppConstants.PAS_ALERT_STATUS,0));
                    mRingerStatus.setText(intent.getStringExtra(AppConstants.PAS_RINGER_STATUS));
                    mControlPoint.setText(intent.getStringExtra(AppConstants.PAS_CONTROL_POINT));
                    break;
                case AppConstants.PAS_ACTION:
                    setRingerStatus(intent.getIntExtra(AppConstants.PAS_ALERT_STATUS,0));
                    mRingerStatus.setText(intent.getStringExtra(AppConstants.PAS_RINGER_STATUS));
                   break;

                default:
                    break;
            }

        }
    };

    private void setRingerStatus(int currentValue) {
        switch (currentValue) {
            case isRing:
                silentTextviView.setText(getString(R.string.silent_inactive));
                vibrateStatusTV.setText(getString(R.string.vibrate_inactive));
                ringerStatusTextView.setText(getString(R.string.ringer_active));
                silent = 0;
                vibrate = 0;
                normal = 1;
                break;
            case isVibrate:
                silentTextviView.setText(getString(R.string.silent_active));
                vibrateStatusTV.setText(getString(R.string.vibrate_active));
                ringerStatusTextView.setText(getString(R.string.ringer_inactive));
                silent = 1;
                vibrate = 1;
                normal = 0;
                break;
            case isSilent:
                silentTextviView.setText(getString(R.string.silent_active));
                vibrateStatusTV.setText(getString(R.string.vibrate_inactive));
                ringerStatusTextView.setText(getString(R.string.ringer_inactive));
                silent = 1;
                vibrate = 0;
                normal = 0;
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }

        setContentView(R.layout.phone_alert_activity);

        /**
         * Setting up Toolbar
         */
        Toolbar toolbar = (Toolbar) findViewById(R.id.bp_toolbar);

        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mProgressDialog = new PairingAlert(this);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));

        ringerStatusTextView = (TextView) findViewById(R.id.ringer_state);
        vibrateStatusTV = (TextView) findViewById(R.id.vibrator_state);
        silentTextviView = (TextView) findViewById(R.id.silent_state);
        mRingerStatus = (TextView) findViewById(R.id.ringerSettingStatus);
        mControlPoint = (TextView) findViewById(R.id.ringerControl);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
        // The ANS UI interested in the following actions from AnsService
        IntentFilter filter = new IntentFilter();
        filter.addAction(AppConstants.PAS_CONTROL_POINT);
        filter.addAction(AppConstants.PAS_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(PASServiceReceiver,
                filter);
//        mNotificationEnable = AppUtils.getBooleanSharedPreference(this, Utils.PHONE_ALERT_STATUS);
//        mAlertService = PhoneAlertService.getInstance();

       /* mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        switch (mAudioManager.getRingerMode()) {
            case AudioManager.RINGER_MODE_VIBRATE:

                if (mNotificationEnable) {
                    ringerValue(RINGER_MODE_SILENT);
                }

                setRingerStatus(isVibrate);

                mRingerStatus.setText("Ringer Silent");

                silent = 1;
                vibrate = 1;
                normal = 0;
                break;
            case AudioManager.RINGER_MODE_SILENT:

                mRingerStatus.setText("Ringer Silent");
                if (mNotificationEnable) {
                    ringerValue(RINGER_MODE_VIBRATE);
                }
                setRingerStatus(isSilent);
                silent = 1;
                vibrate = 0;
                normal = 0;
                break;
            case AudioManager.RINGER_MODE_NORMAL:

                mRingerStatus.setText("Ringer Normal");
                //alertValue("0x01");
                setRingerStatus(isRing);
                silent = 0;
                vibrate = 0;
                normal = 1;
                if (mNotificationEnable) {
                    ringerValue(RINGER_MODE_NORMAL);
                }
                break;
        }*/
       /* IntentFilter filter = new IntentFilter();
        filter.addAction(AudioManager.RINGER_MODE_CHANGED_ACTION);
        filter.addAction("android.media.VOLUME_CHANGED_ACTION");
        try {
            this.registerReceiver(mMessageReceiver, filter);
        } catch (Exception E) {
            E.printStackTrace();
        }*/

       /* mSettingsContentObserver = new SettingsContentObserver(this,new Handler());
        getApplicationContext().getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mSettingsContentObserver);

        if (mNotificationEnable) {
            SendAlertSettings(normal, vibrate, display);
        }*/
      /*  if (!AppUtils.getBooleanSharedPreference(this, AppUtils.FIRST_TIME)) {
            if (AppUtils.getIntSharedPreference(this, AppUtils.PHONE_ALERT_RINGER_CONTROL_VALUE) != 0) {
                int data = AppUtils.getIntSharedPreference(this, AppUtils.PHONE_ALERT_RINGER_CONTROL_VALUE);
                ringerSettingPoint(data);

            }
        } else {
            AppUtils.setBooleanSharedPreference(this, AppUtils.FIRST_TIME, false);
        }*/
        Log.e("PAService.controlPoint",""+PAService.controlPoint);
        setRingerStatus(PAService.mAlertStatus);
        mRingerStatus.setText(PAService.mRingerStatus);
        mControlPoint.setText(PAService.controlPoint);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener=this;
        init();
        // if (BLEConnection.getmConnectionState()== BluetoothProfile.STATE_DISCONNECTED){
        boolean status = false;
        try {
            status = BLEConnection.getmServerConnectionState() != BLEConnection.STATE_DISCONNECTED;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!status) {
            onLeDeviceDisconnected();
        }
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(PhoneAlert.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(PhoneAlert.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  try {
           // unregisterReceiver(mMessageReceiver);
           getApplicationContext().getContentResolver().unregisterContentObserver(mSettingsContentObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void init() {
        bluetoothCheck();
        setConnectionListener(this);
        //setGattServerListener(this);
    }

    private void bluetoothCheck() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
    }

    private void goToHome() {
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }


/* private void SendAlertSettings(int ringer, int vibrate, int display) {
      *//*  ByteBuffer b = ByteBuffer.allocate(6);
        b.order(ByteOrder.nativeOrder());
        short mRingerState = (short) ringer;
        short mVibrateState = (short) vibrate;
        short mDisplayState = (short) display;
        b.putShort(0, mRingerState);
        b.putShort(2, mVibrateState);
        b.putShort(4, mDisplayState);*//*
      byte[] valueByte = new byte[3];
      valueByte[0]=(byte)ringer;
      valueByte[1]=(byte)vibrate;
      valueByte[2]=(byte)display;
        Log.e("ringer", ringer + "");
        Log.e("vibrate", vibrate + "");
        Log.e("display", display+"");
        *//*Log.e("valueOf",""+String.valueOf(b.array()));*//*
     Log.e("SendAlertSettings", "SendAlertSettings");

        mAlertService.notifyAlertStatus(valueByte);
    }*/

   /* private void writeAlertStatus(int ringer, int vibrate, int display, int requestId, int offset) {
      *//*  ByteBuffer b = ByteBuffer.allocate(6);
        b.order(ByteOrder.nativeOrder());
        short mRingerState = (short) ringer;
        short mVibrateState = (short) vibrate;
        short mDisplayState = (short) display;
        b.putShort(0, mRingerState);
        b.putShort(2, mVibrateState);
        b.putShort(4, mDisplayState);
        Log.e("writeAlertStatus", "" + String.valueOf(b.array()));*//*
        byte[] valueByte = new byte[3];
        valueByte[0]=(byte)ringer;
        valueByte[1]=(byte)vibrate;
        valueByte[2]=(byte)display;
        Log.e("ringer", ringer + "");
        Log.e("vibrate", vibrate + "");
        Log.e("display", display + "");
        Log.e("writeAlertStatus", "writeAlertStatus");
        mAlertService.writAlertStatus(requestId, offset, valueByte);
    }*/

    /**
     * Method to convert hex to byteArray
     */
  /*  private void ringerValue(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertStringtoByte(trimmedByte);
            }
        }
        mAlertService.notifyRingerSettings(valueByte);

    }*/

  /*  private byte[] getByteArrya(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertStringtoByte(trimmedByte);
            }
        }

        return valueByte;

    }

    *//**
     * Convert the string to byte
     *
     * @param
     * @return
     *//*
    private int convertStringtoByte(String string) {
        return Integer.parseInt(string, 8);
    }
*/
    /*
    @Override
    public void onLeReadRequest(String characteristics, int requestid, int offset) {

        switch (characteristics) {
            case "00002a3f-0000-1000-8000-00805f9b34fb"://Alert Status
//               ringerValue(RINGER_MODE_SILENT);
                writeAlertStatus(normal, vibrate, display, requestid, offset);
                break;
            case "00002a41-0000-1000-8000-00805f9b34fb"://Ring Settings
                if (silent == 1) {
                    mAlertService.writePhoneAlert(requestid, offset, getByteArrya(RINGER_MODE_SILENT));
                } else {
                    mAlertService.writePhoneAlert(requestid, offset, getByteArrya(RINGER_MODE_NORMAL));
                }
                break;


        }
    }
*/
    @Override
    public void onLeDeviceConnected(String deviceName) {
       /* try {
            mReconnectionAlert.dismissAlert();
        } catch (Exception e) {

        }
        init();*/
        //if (mAutoConnectTriggered) {
        // try {
        //   mAutoConnectTriggered = false;
        try {
            mReconnectionAlert.dismissAlert();
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // }
    }

    @Override
    public void onLeDeviceDisconnected() {
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            // if (BLEConnection.isBonded()) {
            try {
                mReconnectionAlert.showAlert();
                AppUtils.setBooleanSharedPreference(this, Utils.PHONE_ALERT_STATUS, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (BLEConnection.mReconnectionEnabled) {
                BLEConnection.gattServerAutoConnect(this);
                BLEConnection.mReconnectionEnabled = false;
            }
            /*
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        try {
                            if (mAutoConnectTriggered == false) {
                                BLEConnection.gattServerAutoConnect(PhoneAlert.this);
                                mAutoConnectTriggered = true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 500);*/

        }
        /*
            else {
                Toast.makeText(this, R.string.connection_lost, Toast.LENGTH_SHORT).show();
                goToHome();
            }*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        setConnectionListener(null);
        //setGattServerListener(null);
    }

    @Override
    protected void onDestroy() {
       // LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        //getApplicationContext().getContentResolver().unregisterContentObserver(mSettingsContentObserver);
        super.onDestroy();
    }

    /*
    @Override
    public void onLeNotifyRequest() {
        //Toast.makeText(PhoneAlert.this, "onLeNotifyRequest", Toast.LENGTH_SHORT).show();
        super.onLeNotifyRequest();
    }*/

    @Override
    public void onLeDescriptorWrite(boolean status) {
        super.onLeDescriptorWrite(status);
    }
/*
    @Override
    public void onLeWriteRequest(String characteristics, int requestid, int offset, byte[] mvalue) {

        final int data = mvalue[0];
       AppUtils.setIntSharedPreference(this, AppUtils.PHONE_ALERT_RINGER_CONTROL_VALUE, data);
        ringerSettingPoint(data);

    }*/

   /* private void ringerSettingPoint(int data) {


        switch (data) {
            case 1:
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                mRingerStatus.setText("Ringer Silent");
                mControlPoint.setText("Ringer Vibration");
                setRingerStatus(isVibrate);
                if (mNotificationEnable)
                    ringerValue(RINGER_MODE_SILENT);
                break;
            case 2:
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                mRingerStatus.setText("Ringer Silent");
                mControlPoint.setText("Ringer Silent");
                setRingerStatus(isSilent);
                if (mNotificationEnable)
                    ringerValue(RINGER_MODE_SILENT);
                break;
            case 3:
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                mRingerStatus.setText("Ringer Normal");
                mControlPoint.setText("Ringer Normal");
                setRingerStatus(isRing);
                if (mNotificationEnable)
                    ringerValue(RINGER_MODE_NORMAL);
                break;
            case 4:
                break;
        }
    }*/

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);


    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Our handler for received Intents. This will be called whenever an Intent
     * with an action named "custom-event-name" is broadcasted.
     */

    /*public class SettingsContentObserver extends ContentObserver {
        int previousVolume;
        Context context;

        public SettingsContentObserver(Context c, Handler handler) {
            super(handler);
            context=c;

            AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return super.deliverSelfNotifications();
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
           // boolean status = false;
            AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
//            int currentVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
//            int current = am.getStreamVolume(AudioManager.STREAM_RING);
             switch(am.getRingerMode()){
                case AudioManager.RINGER_MODE_NORMAL:

                    mRingerStatus.setText("Ringer Normal");
                    setRingerStatus(isRing);
                    if (mNotificationEnable)
                        ringerValue(RINGER_MODE_NORMAL);
                    silent = 0;
                    vibrate = 0;
                    normal = 1;

                    if (mNotificationEnable)
                        SendAlertSettings(normal, vibrate, display);
                    break;

                case AudioManager.RINGER_MODE_VIBRATE:

                    mRingerStatus.setText("Ringer Silent");
                    if (mNotificationEnable)
                        ringerValue(RINGER_MODE_VIBRATE);
                    setRingerStatus(isVibrate);
                    silent = 0;
                    vibrate = 1;
                    normal = 0;
                    if (mNotificationEnable)
                        SendAlertSettings(normal, vibrate, display);
                    break;

                case AudioManager.RINGER_MODE_SILENT:

                    if (mNotificationEnable)
                        ringerValue(RINGER_MODE_SILENT);
                    mRingerStatus.setText("Ringer Silent");
                    setRingerStatus(isSilent);
                    silent = 1;
                    vibrate = 0;
                    normal = 0;
                    if (mNotificationEnable)
                        SendAlertSettings(normal, vibrate, display);
                    break;

                default:
                    break;
            }

        }
    }*/
}
