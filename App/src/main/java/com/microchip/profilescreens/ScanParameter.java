/*
 *
 *
 *     ScanParameter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;

import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Characteristics.ScanIntervalWindowCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.ScanRefreshCharacteristics;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Services.ScanParameterService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.utils.AppUtils;
import com.microchip.views.RobotoMediumEditText;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.TimeUnit;

/**
 * Created by
 */
public class ScanParameter extends BLEBaseActivity implements CompoundButton.OnCheckedChangeListener, View.OnTouchListener, PopupMenu.OnMenuItemClickListener,OTAUManager.OTAUProgressInfoListener  {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static final int SCAN_INTERVAL_MIN = 2500;
    private static final int SCAN_INTERVAL_MAX = 10240;
    private static final int SCAN_WINDOW_MIN = 2500;
    private static final int SCAN_WINDOW_MAX = 10240;
    private static final int REQUIRED_REFRESH = 0;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    PopupMenu popup;
    boolean isTouchedScanInterval = false;
    AlertDialog.Builder alertDialogBuilder = null;
    AlertDialog alertDialog = null;
    private BLEApplication bleApplication;
    private BluetoothGattService mBluetoothGattService;
    private ScanParameterService mScanParameterService;
    private BLELollipopScanner mBleLollipopScanner;
    private BLEKitKatScanner mBleKitkatScanner;
    private SwitchCompat mScanNotifySwitch;
    private ScanIntervalWindowCharacteristic mScanIntervalWindowCharacteristic;
    private ScanRefreshCharacteristics mScanRefreshCharacteristics;
    private RobotoMediumEditText mScanIntervalasParamsRobotoMediumEditText;
    private RobotoMediumEditText mScanWindowRobotoMediumEditText;
    private Toolbar mToolBar;
    private String MINIMUM_STRING_MILLISECONDS = "3";
    private String MAXIMUM_STRING_MILLISECONDS = "10240";
    private String MINIMUM_STRING_SECONDS = "1";
    private String MAXIMUM_STRING_SECONDS = "10";
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanparams);
        /**
         * ToolBar config
         */

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        /**
         * Initialize UI components
         */
        mScanNotifySwitch = (SwitchCompat) findViewById(R.id.scanNotifiyswitch);
        mScanWindowRobotoMediumEditText = (RobotoMediumEditText) findViewById(R.id.scanWindowParamsEditText);
        mScanIntervalasParamsRobotoMediumEditText = (RobotoMediumEditText) findViewById(R.id.scanIntervalParamsEditText);
        mScanWindowRobotoMediumEditText.setSelection(mScanWindowRobotoMediumEditText.length());
        mScanIntervalasParamsRobotoMediumEditText.setSelection(mScanIntervalasParamsRobotoMediumEditText.length());

        mScanWindowRobotoMediumEditText.enableValidation(true);
        mScanIntervalasParamsRobotoMediumEditText.enableValidation(true);

        /**
         * Settup ScanInterval ranges
         */
        mScanWindowRobotoMediumEditText.setMinValue(SCAN_INTERVAL_MIN);
        mScanWindowRobotoMediumEditText.setMaxValue(SCAN_INTERVAL_MAX);

        mScanWindowRobotoMediumEditText.setTimeUnit(RobotoMediumEditText.MILLIE_SECOND);
        mScanIntervalasParamsRobotoMediumEditText.setTimeUnit(RobotoMediumEditText.MILLIE_SECOND);


        mScanWindowRobotoMediumEditText.append("3");
        mScanIntervalasParamsRobotoMediumEditText.append("3");

        /**
         * Settup ScanWindow ranges
         */
        mScanIntervalasParamsRobotoMediumEditText.setMinValue(SCAN_WINDOW_MIN);
        mScanIntervalasParamsRobotoMediumEditText.setMaxValue(SCAN_WINDOW_MAX);

        /**
         * Enable touch
         */
        mScanIntervalasParamsRobotoMediumEditText.setOnTouchListener(this);
        mScanWindowRobotoMediumEditText.setOnTouchListener(this);
        /**
         * Enable {@link Switch} setOnCheckedChangeListener
         */
        mScanNotifySwitch.setOnCheckedChangeListener(this);
        /**
         * Getting the application instance
         */
        bleApplication = (BLEApplication) getApplication();
        /**
         * Getting the service and data
         */
        mBluetoothGattService = bleApplication.getmBluetoothGattService();

        mScanParameterService = ScanParameterService.getInstance();
        mScanParameterService.setScanParameterService(mBluetoothGattService);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

        /**
         * Get {@link ScanRefreshCharacteristics#getInstance()}
         * Get {@link ScanParameterService#getScanRefreshCharacteristics()}
         */
        mScanRefreshCharacteristics = ScanRefreshCharacteristics.getInstance();
        mScanParameterService.getScanRefreshCharacteristics();
        mScanNotifySwitch.setChecked(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener=this;
        bluetoothChecking();
        init();
        checkNotifyOn();
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }

        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(ScanParameter.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(ScanParameter.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }
    private void checkNotifyOn() {
        if (AppUtils.getBooleanSharedPreference(this, AppUtils.FIRST_TIME)) {
            if (mScanNotifySwitch.isChecked()) {

                mScanParameterService.startNotify(true);

            }
            AppUtils.setBooleanSharedPreference(this, AppUtils.FIRST_TIME, false);
            AppUtils.setBooleanSharedPreference(ScanParameter.this, AppUtils.SCAN_PARAMS_SELECTION_KEY, true);
        } else {
            if (AppUtils.getBooleanSharedPreference(this, AppUtils.SCAN_PARAMS_SELECTION_KEY)) {
                if (mScanNotifySwitch.isChecked()) {
                    mScanParameterService.startNotify(true);
                } else {
                    mScanNotifySwitch.setChecked(true);
                    mScanParameterService.startNotify(true);
                }

            } else {
                mScanNotifySwitch.setChecked(false);

            }

            restoreData();
        }
    }

    private void restoreData() {
        mScanIntervalasParamsRobotoMediumEditText.setText("" + AppUtils.getIntSharedPreference(ScanParameter.this, AppUtils.SCAN_INTERVEL));
        mScanIntervalasParamsRobotoMediumEditText.setTimeUnit(AppUtils.getIntSharedPreference(ScanParameter.this, AppUtils.SCAN_INTERVEL_UNIT));
        mScanWindowRobotoMediumEditText.setText("" + AppUtils.getIntSharedPreference(ScanParameter.this, AppUtils.SCAN_WINDOW));
        mScanWindowRobotoMediumEditText.setTimeUnit(AppUtils.getIntSharedPreference(ScanParameter.this, AppUtils.SCAN_WINDOW_UNIT));
        mScanWindowRobotoMediumEditText.setUnitDrawable(AppUtils.getIntSharedPreference(ScanParameter.this, AppUtils.SCAN_WINDOW_UNIT));
        mScanIntervalasParamsRobotoMediumEditText.setUnitDrawable(AppUtils.getIntSharedPreference(ScanParameter.this, AppUtils.SCAN_INTERVEL_UNIT));

    }

    private void init() {
        setGattListener(this);
        setConnectionListener(this);
        //setGattServerListener(this);
    }

    private void bluetoothChecking() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {

                goToHome();
            }
        }
    }

    private void goToHome() {
        Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onPause() {
        super.onPause();
        savingValues();


        setGattListener(null);
        setConnectionListener(null);
    }

    private void savingValues() {
        AppUtils.setIntSharedPreference(ScanParameter.this, AppUtils.SCAN_INTERVEL, mScanIntervalasParamsRobotoMediumEditText.getFinalValue());
        AppUtils.setIntSharedPreference(ScanParameter.this, AppUtils.SCAN_INTERVEL_UNIT, mScanIntervalasParamsRobotoMediumEditText.getmUnitType());
        AppUtils.setIntSharedPreference(ScanParameter.this, AppUtils.SCAN_WINDOW, mScanWindowRobotoMediumEditText.getFinalValue());
        AppUtils.setIntSharedPreference(ScanParameter.this, AppUtils.SCAN_WINDOW_UNIT, mScanWindowRobotoMediumEditText.getmUnitType());
    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {
        super.onLeCharacteristicChanged(status);
        if (status) {
            int refreshValue = mScanRefreshCharacteristics.getRefreshValue();

            if (refreshValue == REQUIRED_REFRESH) {
                /**
                 * Get two user value
                 */
                sendToPeripheral();
            }
        }
    }

    private void sendToPeripheral() {
        int intervalVal = 0;
        int windowVal = 0;
        int interval_second = 1;
        int interval_milliseconds = 3;
        int window_second = 1;
        int window_millisecond = 3;


        int finalValue = mScanIntervalasParamsRobotoMediumEditText.getFinalValue();
        int scanWindowParam = mScanIntervalasParamsRobotoMediumEditText.getFinalValue() & 0xffff;
        int scanIntervalParam = mScanWindowRobotoMediumEditText.getFinalValue() & 0xffff;

//        if (SCAN_INTERVAL_MAX >= finalValue) {

            //concatenate two string with a space

            if (mScanIntervalasParamsRobotoMediumEditText.getmUnitType() == RobotoMediumEditText.SECOND) {
//                int maxValue = (int) TimeUnit.SECONDS.toMillis((long) SCAN_INTERVAL_MAX);
                if (scanWindowParam >= 1 && scanWindowParam <= 10) {
                    interval_second = (int) TimeUnit.SECONDS.toMillis((long) scanWindowParam);
                } else {
                    if(scanWindowParam > 10) {
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        alert("Maximum should be less than or equal to " + MAXIMUM_STRING_SECONDS + " Second");
                        mScanIntervalasParamsRobotoMediumEditText.setText("" + interval_second);
                    }else{
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        alert(getString(R.string.minimum_seconds_range) + MINIMUM_STRING_SECONDS + " Second");
                        mScanIntervalasParamsRobotoMediumEditText.setText("" + interval_second);
                    }
                }
                intervalVal = interval_second;
            } else {
//                int maxValue = SCAN_INTERVAL_MAX;
//                int minVlaue = SCAN_INTERVAL_MIN;
                if (scanWindowParam >= 3) {
//
                    if (scanWindowParam <= SCAN_INTERVAL_MAX) {
                        interval_milliseconds = scanWindowParam;
                    } else {
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        alert("Maximum should be less than or equal to " + MAXIMUM_STRING_MILLISECONDS + " Second");
                        mScanIntervalasParamsRobotoMediumEditText.setText("" + interval_milliseconds);
                    }

                } else {
                    if (alertDialog != null && alertDialog.isShowing()) {
                        alertDialog.dismiss();
                    }
                    alert("Minimum should be greater than or equal to " + MINIMUM_STRING_MILLISECONDS + " Millisecond");
                    mScanIntervalasParamsRobotoMediumEditText.setText("" + interval_milliseconds);
                }
                if (mScanIntervalasParamsRobotoMediumEditText.getmUnitType() == RobotoMediumEditText.SECOND) {

                }
                intervalVal = interval_milliseconds;
            }

//    } else {
//            if (alertDialog != null && alertDialog.isShowing()) {
//                alertDialog.dismiss();
//            }
//            if (mScanIntervalasParamsRobotoMediumEditText.getmUnitType() == 1) {
//                alert("Maximum should be less than or equal to " + MAXIMUM_STRING_SECONDS + " Second");
//                mScanIntervalasParamsRobotoMediumEditText.setText("" + interval_second);
//            } else {
//                alert("Maximum should be less than or equal to " + MAXIMUM_STRING_MILLISECONDS + " Millisecond");
//                mScanIntervalasParamsRobotoMediumEditText.setText("" + interval_milliseconds);
//            }
//
//        }
        finalValue = mScanWindowRobotoMediumEditText.getFinalValue();
       // if (SCAN_INTERVAL_MAX >= finalValue) {
            if (mScanWindowRobotoMediumEditText.getmUnitType()  == RobotoMediumEditText.SECOND) {
//                int maxValue = (int) TimeUnit.SECONDS.toMillis((long) SCAN_INTERVAL_MAX);
                if ((scanIntervalParam >= 1 && scanIntervalParam <= 10)) {
                    window_second = (int) TimeUnit.SECONDS.toMillis((long) scanIntervalParam);
                } else {
                    if(scanIntervalParam > 10) {
                        // Toast.makeText(this,"Maximum should be less than " + SCAN_INTERVAL_MAX + " Seconds",Toast.LENGTH_SHORT).show();
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        alert("Maximum should be less than or equal to " + MAXIMUM_STRING_SECONDS + " Second");
                        mScanWindowRobotoMediumEditText.setText("" + window_second);
                    }else{
                        // Toast.makeText(this,"Maximum should be less than " + SCAN_INTERVAL_MAX + " Seconds",Toast.LENGTH_SHORT).show();
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        alert("Minimum should be greater than or equal to " + MINIMUM_STRING_SECONDS + " Second");
                        mScanWindowRobotoMediumEditText.setText("" + window_second);
                    }
                }

                windowVal = window_second;

            } else {
//                int maxValue = SCAN_INTERVAL_MAX;
//                int minVlaue = SCAN_INTERVAL_MIN;
                if (scanIntervalParam >= 3) {
                    if (scanIntervalParam <= SCAN_INTERVAL_MAX) {
                        window_millisecond = scanIntervalParam;
                    } else {
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        alert("Maximum should be less than or equal to " + MAXIMUM_STRING_MILLISECONDS + " Second");
                        mScanWindowRobotoMediumEditText.setText("" + window_millisecond);
                    }
                } else {

                    if (alertDialog != null && alertDialog.isShowing()) {
                        alertDialog.dismiss();
                    }
                    alert("Minimum should be greater than or equal to " + MINIMUM_STRING_MILLISECONDS + " Millisecond");
                    mScanWindowRobotoMediumEditText.setText("" + window_millisecond);
                }
                windowVal = window_millisecond;
            }
       /* } else {

            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }

            if (mScanWindowRobotoMediumEditText.getmUnitType() == 1) {
                alert("Maximum should be less than or equal to " + MAXIMUM_STRING_SECONDS + " Second");
                mScanWindowRobotoMediumEditText.setText("" + window_second);
            } else {
                alert("Maximum should be less than or equal to " + MAXIMUM_STRING_MILLISECONDS + " Millisecond");
                mScanWindowRobotoMediumEditText.setText("" + window_millisecond);
            }

            intervalVal = interval_milliseconds;
        }*/
        sendValue(windowVal, intervalVal);
    }

    /**
     * Here converting 2 values to byte[]
     *
     * @param window
     * @param interval
     */
    private void sendValue(int window, int interval) {
        ByteBuffer b = ByteBuffer.allocate(4);
        b.order(ByteOrder.nativeOrder());
        short sWindow = (short) window;
        short sinterval = (short) interval;
        b.putShort(2, sWindow);
        b.putShort(0, sinterval);
        mScanParameterService.writeScanIntervalWindowCharacteristic(b.array());
    }

    private void alert(String msg) {

        alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        //  alertDialogBuilder.setTitle("Your Title");
        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        AppUtils.setBooleanSharedPreference(ScanParameter.this, AppUtils.SCAN_PARAMS_SELECTION_KEY, b);
        if (b) {
            mScanParameterService.startNotify(true);
        } else {
            mScanParameterService.startNotify(false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (view.getId() == mScanWindowRobotoMediumEditText.getId()) {
                isTouchedScanInterval = false;
                return touchDetect(event, mScanWindowRobotoMediumEditText);
            } else if (view.getId() == mScanIntervalasParamsRobotoMediumEditText.getId()) {
                isTouchedScanInterval = true;
                return touchDetect(event, mScanIntervalasParamsRobotoMediumEditText);
            }
        }
        return false;

    }

    private boolean touchDetect(MotionEvent event, RobotoMediumEditText editText) {
        /**
         * Creating Popup Menu
         */
        popup = new PopupMenu(this, editText);
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);

        final int DRAWABLE_RIGHT = 2;
        if (this.getResources().getConfiguration().orientation == 1) {
            if (event.getRawX() >= (editText.getRight() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                editText.setTag(editText.getId());
                popup.show();
                return true;
            }
        } else {

            if (editText.getId() == R.id.scanWindowParamsEditText) {

                if (event.getRawX() >= (editText.getLeft() + (editText.getRight() * 2) - (editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))) {

                    editText.setTag(editText.getId());
                    popup.show();
                    return true;
                }
            } else {

                if (event.getRawX() >= (editText.getRight() - (editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))) {
                    editText.setTag(editText.getId());
                    popup.show();
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        // Object tag = mScanWindowRobotoMediumEditText.getTag();
        if (item.getItemId() == R.id.seconds) {
            if (!isTouchedScanInterval) {
                mScanWindowRobotoMediumEditText.setTimeUnit(RobotoMediumEditText.SECOND);
                mScanWindowRobotoMediumEditText.setUnitDrawable(RobotoMediumEditText.SECOND);

            } else {
                mScanIntervalasParamsRobotoMediumEditText.setTimeUnit(RobotoMediumEditText.SECOND);
                mScanIntervalasParamsRobotoMediumEditText.setUnitDrawable(RobotoMediumEditText.SECOND);
            }
        } else if (item.getItemId() == R.id.millie_seconds) {
            if (!isTouchedScanInterval) {
                mScanWindowRobotoMediumEditText.setTimeUnit(RobotoMediumEditText.MILLIE_SECOND);
                mScanWindowRobotoMediumEditText.setUnitDrawable(RobotoMediumEditText.MILLIE_SECOND);

            } else {
                mScanIntervalasParamsRobotoMediumEditText.setTimeUnit(RobotoMediumEditText.MILLIE_SECOND);
                mScanIntervalasParamsRobotoMediumEditText.setUnitDrawable(RobotoMediumEditText.MILLIE_SECOND);
            }
        }
        popup.dismiss();
        return false;
    }


}
