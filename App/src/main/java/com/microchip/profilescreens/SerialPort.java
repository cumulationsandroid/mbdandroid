/*
 *
 *
 *     SerialPort.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens;


import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.adapters.SerialPort.MessageModel;
import com.microchip.adapters.SerialPort.SerialPortRecycleViewAdapter;
import com.microchip.animations.FadeInAnimator;
import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.microchip.bleanalyser.ServiceListActivity;
import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.microchip.profilescreens.OTAU.OTAUHomeScreen;
import com.microchip.profilescreens.OTAU.OTAUServiceStatus;
import com.microchip.services.SPPService;
import com.microchip.utils.AppUtils;
import com.microchip.utils.PairingAlert;

/**
 * Created by Atmel on 14/8/15.
 */
public class SerialPort extends BLEBaseActivity implements View.OnClickListener, OTAUManager.OTAUProgressInfoListener {

    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    public RecyclerView mRecyclerView;
    public String mOTAUStatusText;
    public int mOTAUProgress;
    // BluetoothGattService mBluetoothGattService;
    //SerialPortService mSerialPortService;
    EditText mText = null;
    ImageButton mSentButton = null;
    LinearLayoutManager mLayoutManager;
    SerialPortRecycleViewAdapter adapter;
    //List<MessageModel> mMessageList = null;
    //BLEApplication application;
    private PairingAlert mProgressDialog;
    private boolean mAutoConnectTriggered = false;
    private boolean mIsPairingRequested = false;
    //OTAU UI
    private ProgressBar mOTAUProgressBar;
    private View mOtaView;
    private TextView mOtauPercentage;
    private BroadcastReceiver mSPPServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("receiver", "Got message: " + intent.getBooleanExtra(AppUtils.SPP_MESSAGE_RECEIVED_STATUS, false));
            if (intent.getBooleanExtra(AppUtils.SPP_MESSAGE_RECEIVED_STATUS, false)) {
                notifyList();
            }

        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }
        setContentView(R.layout.activity_serial_port);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // application = (BLEApplication) getApplicationContext();
        mText = (EditText) findViewById(R.id.text);
        mSentButton = (ImageButton) findViewById(R.id.send_message);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new FadeInAnimator());
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /**
         * Getting the corresponding service and characteristic
         */
        mProgressDialog = new PairingAlert(this);
        mProgressDialog.setMessage(getResources().getString(R.string.pair_inprogress));
        LocalBroadcastManager.getInstance(this).registerReceiver(mSPPServiceReceiver,
                new IntentFilter(AppUtils.SPP_MESSAGE_RECEIVED));
        //  mProgressDialog.setCancelable(false);
        // mMessageList=new ArrayList<>();
//       if (AppUtils.getBooleanSharedPreference(this, AppUtils.FIRST_TIME)) {
//            application.SPPMsgClear();
//            AppUtils.setBooleanSharedPreference(this, AppUtils.FIRST_TIME, false);
//        }
//        mMessageList = application.getSPPMsgList();
        adapter = new SerialPortRecycleViewAdapter(SPPService.mMessageList, this);
        mRecyclerView.setAdapter(adapter);
        if (SPPService.mMessageList.size() > 1) {
            mRecyclerView.scrollToPosition(SPPService.mMessageList.size() - 1);
        }
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
        mSentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newMessage = mText.getText().toString().trim();
                if (newMessage.length() > 0) {
                    mText.setText("");
                    addNewMessage(new MessageModel(newMessage, true));
                    SPPService.sendingMessage(newMessage);
                    //sendingMessage(newMessage);

                }
            }
        });

        //mBluetoothGattService = application.getmBluetoothGattService();
        //mSerialPortService = SerialPortService.getInstance();
        //mSerialPortService.setmSerialPortService(mBluetoothGattService);
        //mSerialPortService.getCharacteristic();

    }

    /*private void sendingMessage(String newMessage) {


        String[] messageArray;
        Log.e("newMessage.length()",""+newMessage.length());
        int intervel;
        if (Build.VERSION_CODES.KITKAT < Build.VERSION.SDK_INT) {
           intervel=150;
        }else{
            intervel=20;
        }
        if (newMessage.length()>intervel){
            messageArray= splitStringEvery(newMessage,intervel);
            for (int i = 0; i < messageArray.length ; i++) {
                Log.e("messageArray ["+i+"]",""+messageArray[i]);
                mSerialPortService.notifyConnectedDevices(messageArray[i]);
            }
        }else {
            mSerialPortService.notifyConnectedDevices(newMessage);
        }

    }*/
   /* public String[] splitStringEvery(String s, int interval) {
        int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
        String[] result = new String[arrayLength];

        int j = 0;
        int lastIndex = result.length - 1;
        for (int i = 0; i < lastIndex; i++) {
            result[i] = s.substring(j, j + interval);
            j += interval;
        } //Add the last bit
        result[lastIndex] = s.substring(j);

        return result;
    }*/
    @Override
    protected void onResume() {
        super.onResume();
        OTAUManager.getInstance().otauProgressInfoListener=this;
        init();
        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
            // goToHome();
        }
        changeToolbarProgress();
    }

    private void changeToolbarProgress() {
        if (mOtaView != null) {
            if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                mOTAUProgressBar.setVisibility(View.VISIBLE);
                mOtauPercentage.setVisibility(View.VISIBLE);
            } else {
                mOtauPercentage.setVisibility(View.GONE);
                mOTAUProgressBar.setVisibility(View.GONE);
            }
        }
        if (Utils.getBooleanSharedPreference(this, Constants.OTAU_PAUSE_STATUS_NAME_KEY)) {
            if (mOtauPercentage != null) {
                mOtauPercentage.setText("Paused");
            }
        }

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem otauAction = menu.findItem(R.id.action_otau);
        if (ServiceListActivity.otaAvailableFlag) {
            otauAction.setVisible(true);
        } else {
            otauAction.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.otau_menu, menu);
        mOtaView = menu.findItem(R.id.action_otau).getActionView();
        mOtauPercentage = (TextView) mOtaView.findViewById(R.id.otau_text);
        mOTAUProgressBar = (ProgressBar) mOtaView.findViewById(R.id.otau_progress);
        try {
            mOTAUProgressBar.setMax(BinModel.getTotalImagesize());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mOtaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OTAUManager.getInstance().mOTAUOngoingFlag) {
                    Intent otauIntent = new Intent(SerialPort.this, OTAUServiceStatus.class);
                    startActivity(otauIntent);
                } else {
                    Intent otauIntent = new Intent(SerialPort.this, OTAUHomeScreen.class);
                    startActivity(otauIntent);
                }
            }
        });
        changeToolbarProgress();
        return true;
    }

    @Override
    public void OTAUProgressUpdater(String status, final float progress) {
        this.mOTAUStatusText = status;
        this.mOTAUProgress = ((int) (progress * 100));
        Log.e("Progress>>", "Status " + status + "Progress " + mOTAUProgress + " " + progress);
        this.runOnUiThread(new Runnable() {
            public void run() {
                mOtauPercentage.setText(mOTAUProgress + " %");
                mOTAUProgressBar.setProgress((int) (progress * BinModel.getTotalImagesize()));
            }
        });
    }

    private void init() {
        Log.e("mIsPairingRequested", "" + mIsPairingRequested);
       /* if (!mIsPairingRequested) {

            SPPService.sendNotifyCharacteristic();
        }*/
        checkingBluetoothStatus();
        setConnectionListener(this);
        startService(new Intent(this, SPPService.class));
        SPPService.initialize();

        // setGattListener(this);
        //setGattServerListener(this);
    }

  /*  private void sendNotifyCharacteristic() {
        mSerialPortService.startNotifyCharacteristic();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                mSerialPortService.requestMtuChange();

            }
        }, 1000);
    }*/


    @Override
    public void onClick(View view) {

    }

    private void checkingBluetoothStatus() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                goToHome();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, SPPService.class));
        //mSerialPortService.stopNotifyCharacteristic();
        // setGattListener(null);
        //setConnectionListener(null);
        //setGattServerListener(null);

    }


   /* @Override
    public void onLeCharacteristicWrite(boolean status) {
        super.onLeCharacteristicWrite(status);
    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {
        if (status) {
            String relievedMessage = mSerialPortService.mRecevicedMessage;
            addNewMessage(new MessageModel(relievedMessage, false));
        }
    }

    @Override
    public void onLeDescriptorWrite(boolean status) {
        super.onLeDescriptorWrite(status);
    }*/

    void addNewMessage(MessageModel m) {
        //mMessageList.add(m);
        SPPService.mMessageList.add(m);
        notifyList();
        //application.addSPPMsgList(mMessageList);


    }

    private void notifyList() {
        adapter.notifyDataSetChanged();
        //mRecyclerView.scrollToPosition(mMessageList.size() - 1);
        mRecyclerView.scrollToPosition(SPPService.mMessageList.size() - 1);
    }

    /*

    @Override
    public void onLeNotifyRequest() {
        super.onLeNotifyRequest();
    }

    @Override
    public void onLeReadRequest(String characteristics, int requestid, int offset) {
        super.onLeReadRequest(characteristics, requestid, offset);
    }*/

    @Override
    public void onLeDeviceConnected(String deviceName) {
        Log.v("onLeDeviceConnected", mAutoConnectTriggered + " onLeDeviceConnected " + deviceName);
        Toast.makeText(this, "Device Connected ", Toast.LENGTH_SHORT).show();
        if (mAutoConnectTriggered) {
            try {
//                mAutoConnectTriggered = false;
//                mReconnectionAlert.dismissAlert();
//                init();
                Toast.makeText(this, "Discover Service Started", Toast.LENGTH_SHORT).show();
                BLEConnection.discoverServices();
            } catch (Exception ignored) {

            }
        }
    }

    @Override
    public void onLeDeviceDisconnected() {
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            Log.e("SerialPort", "DeviceDisconnected");

            //  if (BLEConnection.isBonded()) {
            try {
                mReconnectionAlert.showAlert();
            } catch (Exception ignored) {

            }
            if (!mAutoConnectTriggered) {
                Toast.makeText(this, "Got Disconnected, Initiated Connection ", Toast.LENGTH_SHORT).show();
                BLEConnection.connectSPP(SerialPort.this);
                mAutoConnectTriggered = true;
            }
           /* } else {

                Toast.makeText(this, R.string.connection_lost, Toast.LENGTH_SHORT).show();
                goToHome();
            }*/


        }

    }

    @Override
    public void onLeServiceDiscovered(boolean status) {
        Log.e("oServDiscover status", "" + status);
        Toast.makeText(this, "Service Discovered..", Toast.LENGTH_SHORT).show();
        mReconnectionAlert.dismissAlert();
        if (status) {
            mAutoConnectTriggered = false;
            mReconnectionAlert.dismissAlert();
            if (!mIsPairingRequested) {
                SPPService.sendNotifyCharacteristic();
            }
            init();
        } else {
            goToHome();
        }
    }

    private void goToHome() {
        // Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onLePairingRequest() {
        super.onLePairingRequest();
        mIsPairingRequested = true;
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.setMessage(getResources().getString(R.string.pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();
                        // mSerialPortService.startNotifyCharacteristic();
                        SPPService.sendNotifyCharacteristic();
                        mIsPairingRequested = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);


    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.pair_progress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  /*  @Override
    public void onLeServicesDiscovered(boolean status, String serviceUuid) {
        Log.e("onLeServicesDiscovered","status "+status);
        BLEConnection.autoConnect(getApplicationContext());
    }*/
}

