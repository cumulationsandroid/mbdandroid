/*
 *
 *
 *     GattDbCharacteristic.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens.gattdb;

import android.app.Service;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.GattErrorEvent;
import com.atmel.blecommunicator.com.atmel.Attributes.GattEvent;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.microchip.utils.AppUtils;
import com.microchip.utils.DialogListner;
import com.microchip.utils.HexKeyBoard;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class GattDbCharacteristic extends BLEBaseActivity implements DialogListner, View.OnClickListener {

    private final static String TAG = GattDbCharacteristic.class.getSimpleName();
    // Indicate/Notify/Read Flag
    public static boolean mIsNotifyEnabled;
    public static boolean mIsIndicateEnabled;
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    private final String LIST_PROPERTIES = "PROPERTIES";
    public EventBus mBus = EventBus.getDefault();
    private BLEApplication mApplication;
    private BluetoothGattCharacteristic mGattCharacteristic, mGattEventCharacteristic;
    private String mGattServiceName = "";
    private String mGattCharacteristicName = "";
    private TextView mServiceText, mCharacteristicText;
    private Button mReadButton, mWriteButton, mNotifyButton, mIndicateButton, mDescriptors;
    private EditText mValueEditText, mHexEditText, mDateEditText, mTimeEditText;
    private RelativeLayout mCharaFrame;
    private int mCharacteristicProperties = 0;
    private int mDescriptorCount = 0;
    private HexKeyBoard hexKeyBoard;
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            Log.e(TAG, "hex edt Clicked");
            hexKeyBoard.setDialogListner(GattDbCharacteristic.this);
            hexKeyBoard.show();
            return true; // the listener has consumed the event
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.gattdb_charecteristic_detail);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        /**
         * Getting the application instance
         */
        mApplication = (BLEApplication) getApplication();

        init();

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

        // Register as a subscriber
        //mBus.register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /**
         * Registering the listeners
         */
        setConnectionListener(this);
        setGattListener(this);

        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
        checkBluetoothAndPairing();
    }

    private void init() {
        /**
         * Displaying the toolbar
         */
        Toolbar toolbar = (Toolbar) findViewById(R.id.gattdb_char_toolbar);
        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Getting the selected service from the arguments
        Intent mIntent = getIntent();
        mGattServiceName = mIntent.getStringExtra(AppConstants.GATTDB_SELECTED_SERVICE);
        mGattCharacteristicName = mIntent.getStringExtra(AppConstants.GATTDB_SELECTED_CHARACTERISTIC);

        mServiceText = (TextView) findViewById(R.id.service_name);
        mCharacteristicText = (TextView) findViewById(R.id.characteristic_name);
        mReadButton = (Button) findViewById(R.id.property_read_btn);
        mWriteButton = (Button) findViewById(R.id.property_write);
        mNotifyButton = (Button) findViewById(R.id.property_notify);
        mIndicateButton = (Button) findViewById(R.id.property_indicate);
        mDescriptors = (Button) findViewById(R.id.descriptors);
        mValueEditText = (EditText) findViewById(R.id.value_edittext);
        mHexEditText = (EditText) findViewById(R.id.hex_edittext);
        mDateEditText = (EditText) findViewById(R.id.date_edittext);
        mTimeEditText = (EditText) findViewById(R.id.time_edittext);
        mReadButton.setOnClickListener(this);
        mNotifyButton.setOnClickListener(this);
        mWriteButton.setOnClickListener(this);
        mIndicateButton.setOnClickListener(this);
        mDescriptors.setOnClickListener(this);
        mHexEditText.setOnClickListener(this);
        mHexEditText.setOnTouchListener(onTouchListener);
        mValueEditText.setOnClickListener(this);
        mValueEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mValueEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String result = mValueEditText.getText().toString();
                    String hexValue = asciiToHex(result);
                    Log.e("Hex value-->", hexValue);
                    byte[] convertedBytes = Utils.hexStringToByteArray(hexValue);
                    // Displaying the hex value in hex field
                    displayHexValue(convertedBytes);
                    writeCharaValue(convertedBytes);
                    hideAsciiKeyboard();
                    return true;
                }
                return false;
            }
        });
        mValueEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) {
                    //clearall();
                }
            }
        });

        mValueEditText.setEnabled(false);
        mHexEditText.setEnabled(false);
        mDateEditText.setEnabled(false);
        mTimeEditText.setEnabled(false);

        mServiceText.setText(mGattServiceName);
        mCharacteristicText.setText(mGattCharacteristicName);

        mGattCharacteristic = mApplication.getmGattCharacteristic();

        hexKeyBoard = new HexKeyBoard(GattDbCharacteristic.this, mGattCharacteristic, true);

        mCharacteristicProperties = mGattCharacteristic.getProperties();

        mDescriptorCount = mGattCharacteristic.getDescriptors().size();
        if (mDescriptorCount > 0) {
            mDescriptors.setVisibility(View.VISIBLE);
        } else {
            mDescriptors.setVisibility(View.GONE);
        }

        if (BLEConnection.isCharacteristicReadable(mGattCharacteristic)) {
            mReadButton.setVisibility(View.VISIBLE);
        }

        if (BLEConnection.isCharacteristicWriteable(mGattCharacteristic)) {
            mWriteButton.setVisibility(View.VISIBLE);
            mValueEditText.setEnabled(true);
            mHexEditText.setEnabled(true);
            mValueEditText.clearFocus();
            mHexEditText.clearFocus();
        } else {
            mHexEditText.setOnClickListener(null);
            mHexEditText.setFocusable(false);
            mHexEditText.setClickable(false);
        }

        if (BLEConnection.isCharacteristicNotifiable(mGattCharacteristic)) {
            mNotifyButton.setVisibility(View.VISIBLE);
        }

        if (BLEConnection.isCharacteristicIndicatable(mGattCharacteristic)) {
            mIndicateButton.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(GattEvent event) {
        mGattEventCharacteristic = event.getCharacteristic();
        Log.e("Service>> " + mGattEventCharacteristic.getService().getUuid().toString(),
                "CharacteristicName>> " + mGattEventCharacteristic.getUuid().toString());
        mValueEditText.setText(Utils.parseASCII(mGattCharacteristic.getValue()));
        mHexEditText.setText(Utils.parse(mGattCharacteristic));
        mTimeEditText.setText(AppUtils.getTimeFromMilliseconds());
        mDateEditText.setText(AppUtils.getTimeandDate());
    }

    @Subscribe
    public void onEvent(GattErrorEvent errorEvent) {
        Log.e("Gatt Error>> ", "" + errorEvent.getErrorEvent());
        displayAlertWithMessage(errorEvent.getErrorEvent());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mIsIndicateEnabled) {
            BLEConnection.setCharacteristicIndication(mGattCharacteristic, false);
            mIsIndicateEnabled = false;
            Toast.makeText(this, getResources().
                            getString(R.string.profile_control_stop_both_notify_indicate_toast),
                    Toast.LENGTH_SHORT).show();
        }
        if (mIsNotifyEnabled) {
            BLEConnection.setCharacteristicNotification(mGattCharacteristic, false);
            mIsNotifyEnabled = false;
            Toast.makeText(this, getResources().
                            getString(R.string.profile_control_stop_both_notify_indicate_toast),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mBus.register(GattDbCharacteristic.this);
    }

    @Override
    public void onStop() {
        mBus.unregister(this);
        super.onStop();
    }

    @Override
    public void onLePairingRequest() {
        try {
            mPairAlert.pairingInProgress();
            mPairAlert.showAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        try {
            mPairAlert.pairingDone();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (mPairAlert != null) {
                        mPairAlert.dismissAlert();
                    }
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mPairAlert.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        try {
            if (mPairAlert != null && !mPairAlert.isShowing()) {
                mPairAlert.pairingInProgress();
                mPairAlert.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkBluetoothAndPairing() {
        /**
         * Always pair the device,
         * When app enter into the page
         */
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.property_read_btn:
                Log.e(TAG, "Read Clicked");
                if ((mCharacteristicProperties | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                    BLEConnection.readCharacteristic(mGattCharacteristic);
                }
                break;
            case R.id.property_write:
                Log.e(TAG, "write Clicked");
                hexKeyBoard.setDialogListner(GattDbCharacteristic.this);
                hexKeyBoard.show();
                break;
            case R.id.hex_edittext:
                Log.e(TAG, "hex edt Clicked");
                hexKeyBoard.setDialogListner(GattDbCharacteristic.this);
                hexKeyBoard.show();
                break;
            case R.id.value_edittext:
                Log.e(TAG, "ascii edt Clicked");
                mValueEditText.requestFocus();
                mValueEditText.setEnabled(true);
                InputMethodManager imm = (InputMethodManager) this
                        .getSystemService(Service.INPUT_METHOD_SERVICE);
                imm.showSoftInput(mValueEditText, InputMethodManager.SHOW_FORCED);
                break;
            case R.id.property_notify:
                Log.e(TAG, "Notify Clicked");
                if ((mCharacteristicProperties | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                    BLEConnection.setCharacteristicNotification(
                            mGattCharacteristic, true);
                    mIsNotifyEnabled = true;
                }
                break;
            case R.id.property_indicate:
                Log.e(TAG, "Indicate Clicked");
                if ((mCharacteristicProperties | BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0) {
                    BLEConnection.setCharacteristicIndication(
                            mGattCharacteristic, true);
                    mIsIndicateEnabled = true;
                }
                break;
            case R.id.descriptors:
                FragmentManager fragmentManager = getSupportFragmentManager();
                GattDescriptorFragment gattDescriptorFragment = new GattDescriptorFragment()
                        .create();
                //gattDescriptorFragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .replace(R.id.parent_frame_layout, gattDescriptorFragment)
                        .addToBackStack(null).commit();
                break;
            default:

        }
    }

    @Override
    public void dialog0kPressed(String result) {
        byte[] convertedBytes = Utils.convertingTobyteArray(result);
        // Displaying the hex and ASCII values
        mHexEditText.setText(Utils.parse(convertedBytes));
        mValueEditText.setText(Utils.parseASCII(convertedBytes));
        writeCharaValue(convertedBytes);
    }

    @Override
    public void dialogCancelPressed(Boolean aBoolean) {

    }

    /**
     * Method to display the hexValue after converting from byte array
     *
     * @param array
     */
    void displayHexValue(byte[] array) {
        StringBuffer sb = new StringBuffer();
        for (byte byteChar : array) {
            sb.append(String.format("%02x", byteChar));
        }
        mHexEditText.setText(sb.toString());
    }

    /**
     * Method to convert the hexvalue to ascii value and displaying to the user
     *
     * @param hexValue
     */
    void displayASCIIValue(String hexValue) {
        mValueEditText.setText("");
        StringBuilder output = new StringBuilder("");
        try {
            for (int i = 0; i < hexValue.length(); i += 2) {
                String str = hexValue.substring(i, i + 2);
                output.append((char) Integer.parseInt(str, 16));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mValueEditText.setText(output.toString());
    }

    /**
     * Method to write the byte value to the characteristic
     *
     * @param value
     */
    private void writeCharaValue(byte[] value) {
        displayTimeandDate();
        // Writing the hexValue to the characteristic
        try {
            BLEConnection.setCharacteristicWriteWithoutResponse(mGattCharacteristic,
                    value);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to display time and date
     */
    private void displayTimeandDate() {
        mTimeEditText.setText(AppUtils.getTimeFromMilliseconds());
        mDateEditText.setText(AppUtils.getDateFromMilliseconds());
    }

    /**
     * Method to convert ascii to hex
     *
     * @param asciiValue
     * @return
     */
    private String asciiToHex(String asciiValue) {
        char[] chars = asciiValue.toCharArray();
        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            hex.append(Integer.toHexString((int) chars[i]));
        }
        return hex.toString();
    }

    /**
     * Method to hide the ascii keyboard
     */
    private void hideAsciiKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        mValueEditText.clearFocus();
    }

    /**
     * Error code alert message
     *
     * @param errorcode
     */

    private void displayAlertWithMessage(int errorcode) {
        String errorMessage = getResources().getString(R.string.alert_message_write_error) +
                "\n" + getResources().getString(R.string.alert_message_write_error_code) + errorcode +
                "\n" + getResources().getString(R.string.alert_message_try_again);
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Write Error !");
        builder.setMessage(errorMessage);
        builder.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        builder.show();
    }
}