/*
 *
 *
 *     GattDbService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens.gattdb;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.microchip.bleanalyser.AppConstants;
import com.microchip.bleanalyser.BLEApplication;
import com.microchip.bleanalyser.BLEBaseActivity;
import com.microchip.bleanalyser.HomeScreen;
import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.microchip.utils.AppUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GattDbService extends BLEBaseActivity {

    private final static String TAG = GattDbService.class.getSimpleName();
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    private static List<BluetoothGattService> mBluetoothGattDBMasterServices;
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    private final String LIST_PROPERTIES = "PROPERTIES";
    GattServiceExpandableAdapter mListAdapter;
    private BLEApplication mBleApplication;
    private ExpandableListView mGattServicesList;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {

                        int tempChild = childPosition - 1;
                        Intent intent = new Intent(GattDbService.this, GattDbCharacteristic.class);
                        String characteristicUUID = mGattCharacteristics.get(groupPosition).get(tempChild).getUuid().toString();
                        String characteristicsName = GattAttributes.lookup(mGattCharacteristics.get(groupPosition).get(tempChild).getUuid().toString(),
                                characteristicUUID);

                        mBleApplication.setmGattCharacteristic(mGattCharacteristics.get(groupPosition).get(tempChild));

                        /**
                         * Passing the characteristic details to GattDetailsFragment and
                         * adding that fragment to the view
                         */
                        intent.putExtra(AppConstants.GATTDB_SELECTED_SERVICE, GattAttributes.lookup(mGattCharacteristics.get(groupPosition).get(tempChild).getService().getUuid().toString(),
                                characteristicsName));
                        intent.putExtra(AppConstants.GATTDB_SELECTED_CHARACTERISTIC,
                                characteristicsName);
                        startActivity(intent);
                        return true;
                    }
                    return false;
                }
            };
    private BLEConnection mBluetoothLeService = new BLEConnection();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppUtils.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_gattdb);
        /**
         * Getting the application instance
         */
        mBleApplication = (BLEApplication) getApplication();

        init();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /**
         * Registering the listeners
         */
        setConnectionListener(this);
        setGattListener(this);

        if (BLEConnection.getmConnectionState() == BluetoothProfile.STATE_DISCONNECTED) {
            onLeDeviceDisconnected();
        }
        checkBluetoothAndPairing();
    }

    private void init() {
        /**
         * Displaying the toolbar
         */
        Toolbar toolbar = (Toolbar) findViewById(R.id.gattdb_toolbar);
        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mBluetoothGattDBMasterServices =
                new ArrayList<BluetoothGattService>();
        mBluetoothGattDBMasterServices = BLEConnection.getSupportedGattServices();
        displayGattServices(mBluetoothGattDBMasterServices);
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        ArrayList<ArrayList<HashMap<String, BluetoothGattCharacteristic>>> gattCharacteristicProp
                = new ArrayList<ArrayList<HashMap<String, BluetoothGattCharacteristic>>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, GattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            ArrayList<HashMap<String, BluetoothGattCharacteristic>> gattCharacteristicGroupProp =
                    new ArrayList<HashMap<String, BluetoothGattCharacteristic>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                HashMap<String, BluetoothGattCharacteristic> currentCharaMap = new HashMap<String, BluetoothGattCharacteristic>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, GattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                currentCharaMap.put(LIST_PROPERTIES, gattCharacteristic);
                gattCharacteristicGroupData.add(currentCharaData);
                gattCharacteristicGroupProp.add(currentCharaMap);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
            gattCharacteristicProp.add(gattCharacteristicGroupProp);
        }

        mListAdapter = new GattServiceExpandableAdapter(this, gattServiceData, gattCharacteristicData, gattCharacteristicProp);
        mGattServicesList.setAdapter(mListAdapter);

    }

    private void checkBluetoothAndPairing() {
        /**
         * Always pair the device,
         * When app enter into the page
         */
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, HomeScreen.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
