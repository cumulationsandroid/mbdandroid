/*
 *
 *
 *     GattDescriptorDetails.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.microchip.profilescreens.gattdb;


import android.app.ProgressDialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.microchip.bleanalyser.BLEApplication;
import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Attributes.GattDescriptorEvent;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.parsers.DescriptorParser;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Descriptor Details Class
 */
public class GattDescriptorDetails extends Fragment implements
        View.OnClickListener {

    public EventBus mBus = EventBus.getDefault();
    //Characteristic
    private BluetoothGattCharacteristic mBluetoothGattCharacteristic;
    //Descriptor
    private BluetoothGattDescriptor mDescriptor;
    // View
    private ViewGroup mContainer;
    // Application
    private BLEApplication mApplication;
    //View fields
    private TextView mCharacteristicName;
    private TextView mDescriptorName;
    private TextView mDescriptorValue;
    private TextView mHexValue;
    private Button mReadButton;
    private Button mNotifyButton;
    private Button mIndicateButton;
    private ImageView mBackBtn;
    private ProgressDialog mProgressDialog;
    private String mDescriptorStatus = "";
    private String mStartNotifyText;
    private String mStopNotifyText;
    private String mStartIndicateText;
    private String stopIndicateText;
    // flag
    private boolean mGUIUpdateFlag = false;

    public GattDescriptorDetails create() {
        GattDescriptorDetails fragment = new GattDescriptorDetails();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.gatt_descriptor_details, container,
                false);
        this.mContainer = container;
        mApplication = (BLEApplication) getActivity().getApplication();
        mCharacteristicName = (TextView) rootView.findViewById(R.id.txtcharacteristicname);
        mDescriptorName = (TextView) rootView.findViewById(R.id.txtdescriptorname);
        mDescriptorValue = (TextView) rootView.findViewById(R.id.txtdescriptorvalue);
        mHexValue = (TextView) rootView.findViewById(R.id.txtdescriptorHexvalue);

        mProgressDialog = new ProgressDialog(getActivity());

        mBluetoothGattCharacteristic = mApplication.getmGattCharacteristic();
        String characteristicUUID = mBluetoothGattCharacteristic.getUuid().toString();
        mCharacteristicName.setText(GattAttributes.lookup(mBluetoothGattCharacteristic.getUuid().toString(), characteristicUUID));

        mDescriptor = mApplication.getBluetoothgattDescriptor();
        String DescriptorUUID = mDescriptor.getUuid().toString();
        mDescriptorName.setText(GattAttributes.lookup(mDescriptor.getUuid().toString(), DescriptorUUID));

        mReadButton = (Button) rootView.findViewById(R.id.btn_read);
        mNotifyButton = (Button) rootView.findViewById(R.id.btn_write_notify);
        mIndicateButton = (Button) rootView.findViewById(R.id.btn_write_indicate);
        if (DescriptorUUID.equalsIgnoreCase(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG)) {
            if (getGattCharacteristicsPropertices(mBluetoothGattCharacteristic.getProperties(),
                    BluetoothGattCharacteristic.PROPERTY_NOTIFY)) {
                mNotifyButton.setVisibility(View.VISIBLE);
                mNotifyButton.setText(getResources().getString(R.string.gatt_services_notify));
            }
            if (getGattCharacteristicsPropertices(mBluetoothGattCharacteristic.getProperties(),
                    BluetoothGattCharacteristic.PROPERTY_INDICATE)) {
                mIndicateButton.setVisibility(View.VISIBLE);
                mIndicateButton.setText(getResources().getString(R.string.gatt_services_indicate));
            }
        } else {
            mNotifyButton.setVisibility(View.GONE);
        }
        mStartNotifyText = getResources().getString(R.string.gatt_services_notify);
        mStopNotifyText = getResources().getString(R.string.gatt_services_stop_notify);
        mStartIndicateText = getResources().getString(R.string.gatt_services_indicate);
        stopIndicateText = getResources().getString(R.string.gatt_services_stop_indicate);
        mReadButton.setOnClickListener(this);
        mNotifyButton.setOnClickListener(this);
        mIndicateButton.setOnClickListener(this);

        if (mDescriptor != null) {
            BLEConnection.readDescriptor(mDescriptor);
        }
        return rootView;
    }

    private void displayDescriptorValue(String value) {
        mDescriptorValue.setText(value);
    }

    void displayHexValue(byte[] array) {
        String descriptorValue = Utils.ByteArraytoHex(array);
        mHexValue.setText(descriptorValue);
    }

    private void updateButtonStatus(byte[] array) {
        int status = array[0];
        switch (status) {
            case 0:
                if (mNotifyButton.getVisibility() == View.VISIBLE)
                    mNotifyButton.setText(mStartNotifyText);

                if (mIndicateButton.getVisibility() == View.VISIBLE)
                    mIndicateButton.setText(mStartIndicateText);
                break;
            case 1:
                if (mNotifyButton.getVisibility() == View.VISIBLE)
                    mNotifyButton.setText(mStopNotifyText);
                break;
            case 2:
                if (mIndicateButton.getVisibility() == View.VISIBLE)
                    mIndicateButton.setText(stopIndicateText);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mBus.register(this);
    }

    @Override
    public void onStop() {
        mBus.unregister(this);
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(GattDescriptorEvent event) {
        mDescriptor = event.getDescriptor();
        Log.e("Desc>> " + mDescriptor.getUuid().toString(),
                "Characteristic>> " + mDescriptor.getCharacteristic().getUuid().toString() +
                        "Property>> " + mDescriptor.getPermissions());
        displayHexValue(mDescriptor.getValue());
        updateButtonStatus(mDescriptor.getValue());
        if (mDescriptor.getUuid().toString().equalsIgnoreCase(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG)) {
            displayDescriptorValue(DescriptorParser.getClientCharacteristicConfiguration(mDescriptor, getActivity()));
        } else if (mDescriptor.getUuid().toString().equalsIgnoreCase(GattAttributes.CHARACTERISTIC_PRESENTATION_FORMAT)) {
            displayDescriptorValue(DescriptorParser.getCharacteristicPresentationFormat(mDescriptor, getActivity()));
        } else if (mDescriptor.getUuid().toString().equalsIgnoreCase(GattAttributes.CHARACTERISTIC_EXTENDED_PROPERTIES)) {
            //displayDescriptorValue(DescriptorParser.getCharacteristicExtendedProperties(mDescriptor, getActivity()));
        } else if (mDescriptor.getUuid().toString().equalsIgnoreCase(GattAttributes.CHARACTERISTIC_USER_DESCRIPTION)) {
            displayDescriptorValue(DescriptorParser.getCharacteristicUserDescription(mDescriptor));
        } else if (mDescriptor.getUuid().toString().equalsIgnoreCase(GattAttributes.SERVER_CHARACTERISTIC_CONFIGURATION)) {
            displayDescriptorValue(DescriptorParser.getServerCharacteristicConfiguration(mDescriptor, getActivity()));
        } else if (mDescriptor.getUuid().toString().equalsIgnoreCase(GattAttributes.REPORT_REFERENCE)) {
            //displayDescriptorValue(DescriptorParser.getReportReference(mDescriptor, getActivity()));
        } else if (mDescriptor.getUuid().toString().equalsIgnoreCase(GattAttributes.VALID_RANGE)) {
            displayDescriptorValue(DescriptorParser.getValidRange(mDescriptor, getActivity()));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_write_notify:
                Button btn = (Button) view;
                String btnText = btn.getText().toString();
                mGUIUpdateFlag = true;
                if (btnText.equalsIgnoreCase(mStartNotifyText)) {
                    prepareBroadcastDataNotify(mBluetoothGattCharacteristic);
                    GattDbCharacteristic.mIsNotifyEnabled = true;
                    btn.setText(mStopNotifyText);
                }
                if (btnText.equalsIgnoreCase(mStopNotifyText)) {
                    stopBroadcastDataNotify(mBluetoothGattCharacteristic);
                    GattDbCharacteristic.mIsNotifyEnabled = false;
                    btn.setText(mStartNotifyText);
                }
                break;
            case R.id.btn_write_indicate:
                Button btnIndicate = (Button) view;
                String btnTextIndicate = btnIndicate.getText().toString();
                mGUIUpdateFlag = true;
                if (btnTextIndicate.equalsIgnoreCase(mStartIndicateText)) {
                    prepareBroadcastDataIndicate(mBluetoothGattCharacteristic);
                    GattDbCharacteristic.mIsIndicateEnabled = true;
                    btnIndicate.setText(stopIndicateText);
                }
                if (btnTextIndicate.equalsIgnoreCase(stopIndicateText)) {
                    stopBroadcastDataIndicate(mBluetoothGattCharacteristic);
                    GattDbCharacteristic.mIsIndicateEnabled = false;
                    btnIndicate.setText(mStartIndicateText);
                }
                break;
            case R.id.btn_read:
                if (mDescriptor != null) {
                    BLEConnection.readDescriptor(mDescriptor);
                }
                break;

        }
    }

    /**
     * Preparing Broadcast receiver to broadcast notify characteristics
     *
     * @param gattCharacteristic
     */
    void prepareBroadcastDataNotify(
            BluetoothGattCharacteristic gattCharacteristic) {
        Log.i("", "Notify called");
        final BluetoothGattCharacteristic characteristic = gattCharacteristic;
        final int charaProp = characteristic.getProperties();
        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
            BLEConnection.setCharacteristicNotification(characteristic,
                    true);

        }

    }

    /**
     * Stopping Broadcast receiver to broadcast notify characteristics
     *
     * @param gattCharacteristic
     */
    void stopBroadcastDataNotify(
            BluetoothGattCharacteristic gattCharacteristic) {
        Log.i("", "Notify stopped");
        final BluetoothGattCharacteristic characteristic = gattCharacteristic;
        final int charaProp = characteristic.getProperties();

        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
            if (gattCharacteristic != null) {
                BLEConnection.setCharacteristicNotification(
                        gattCharacteristic, false);
            }

        }

    }

    /**
     * Preparing Broadcast receiver to broadcast indicate characteristics
     *
     * @param gattCharacteristic
     */
    void prepareBroadcastDataIndicate(
            BluetoothGattCharacteristic gattCharacteristic) {
        Log.i("", "Indicate called");
        final BluetoothGattCharacteristic characteristic = gattCharacteristic;
        final int charaProp = characteristic.getProperties();

        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0) {
            BLEConnection.setCharacteristicIndication(
                    gattCharacteristic, true);
        }
    }

    /**
     * Stopping Broadcast receiver to broadcast indicate characteristics
     *
     * @param gattCharacteristic
     */
    void stopBroadcastDataIndicate(
            BluetoothGattCharacteristic gattCharacteristic) {
        Log.i("", "Indicate stopped");
        final BluetoothGattCharacteristic characteristic = gattCharacteristic;
        final int charaProp = characteristic.getProperties();

        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0) {
            if (gattCharacteristic != null) {
                Log.i("", "Stopped notification");
                BLEConnection.setCharacteristicIndication(
                        gattCharacteristic, false);
            }
        }
    }

    /**
     * Return the property enabled in the characteristic
     *
     * @param characteristics
     * @param characteristicsSearch
     * @return
     */
    boolean getGattCharacteristicsPropertices(int characteristics,
                                              int characteristicsSearch) {
        return (characteristics & characteristicsSearch) == characteristicsSearch;
    }

}
