/*
 *
 *
 *     GattDescriptorFragment.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.microchip.profilescreens.gattdb;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.microchip.bleanalyser.BLEApplication;
import com.microchip.R;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Fragment class for GATT Descriptor
 */
public class GattDescriptorFragment extends Fragment {

    private List<BluetoothGattDescriptor> mBluetoothGattDescriptors;
    private BluetoothGattCharacteristic mBluetoothGattCharacteristic;

    // Application
    private BLEApplication mApplication;
    // Text Heading
    private TextView mTextHeading;
    // GATT Service name
    private String mGattServiceName = "";
    //GATT Characteristic name
    private String mGattCharacteristicName = "";
    // ListView
    private ListView mGattListView;
    // Back button
    private ImageView mBackButton;

    public GattDescriptorFragment create() {
        GattDescriptorFragment fragment = new GattDescriptorFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.gatt_descriptor_list_fragment,
                container, false);
        mApplication = (BLEApplication) getActivity().getApplication();
        //getActivity().getActionBar().setTitle(R.string.gatt_descriptor);
        mGattListView = (ListView) rootView
                .findViewById(R.id.gatt_descriptors_list);
        mTextHeading = (TextView) rootView.findViewById(R.id.txtservices);
        //mTextHeading.setText(getString(R.string.gatt_descriptors_heading));
        mBluetoothGattCharacteristic = mApplication.getmGattCharacteristic();

        //Preparing list data
        List<BluetoothGattDescriptor> tempList = mBluetoothGattCharacteristic.getDescriptors();
        mBluetoothGattDescriptors = new ArrayList<BluetoothGattDescriptor>();

        for (BluetoothGattDescriptor tempDesc : tempList) {
            int mainListSize = mBluetoothGattDescriptors.size();
            if (mainListSize > 0) {
                //Getting the UUID of list descriptors
                ArrayList<UUID> mainUUID = new ArrayList<UUID>();
                for (int incr = 0; incr < mainListSize; incr++) {
                    mainUUID.add(mBluetoothGattDescriptors.get(incr).getUuid());
                }
                if (!mainUUID.contains(tempDesc.getUuid())) {
                    mBluetoothGattDescriptors.add(tempDesc);
                }
            } else {
                mBluetoothGattDescriptors.add(tempDesc);
            }
        }
        GattCharacteristicDescriptorsAdapter gattCharacteristicDescriptorsAdapter =
                new GattCharacteristicDescriptorsAdapter(getActivity(), mBluetoothGattDescriptors);
        if (gattCharacteristicDescriptorsAdapter != null) {
            mGattListView.setAdapter(gattCharacteristicDescriptorsAdapter);
        }
        mGattListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Log.i("Descriptor selected ","" + mBluetoothGattDescriptors.get(position).getUuid());
                mApplication.setBluetoothgattdescriptor(mBluetoothGattDescriptors.get(position));
                FragmentManager fragmentManager = getFragmentManager();
                GattDescriptorDetails gattDescriptorDetails = new GattDescriptorDetails()
                        .create();
                fragmentManager.beginTransaction()
                        .add(R.id.parent_frame_layout, gattDescriptorDetails)
                        .addToBackStack(null).commit();
            }
        });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

}
