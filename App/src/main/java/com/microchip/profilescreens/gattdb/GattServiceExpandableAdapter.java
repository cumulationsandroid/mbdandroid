/*
 *
 *
 *     GattServiceExpandableAdapter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.profilescreens.gattdb;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GattServiceExpandableAdapter extends BaseExpandableListAdapter {

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    private final String LIST_PROPERTIES = "PROPERTIES";
    List<Boolean> properties = new ArrayList<>();
    private int previousGroup = -1;
    private Context mContext;
    private ArrayList<HashMap<String, String>> mListDataHeader; // header titles
    // child data in format of header title, child title
    private ArrayList<ArrayList<HashMap<String, String>>> mListDataChild;
    private ArrayList<ArrayList<HashMap<String, BluetoothGattCharacteristic>>> mListPropChild;
    List<Boolean> permissions = new ArrayList<>();

    public GattServiceExpandableAdapter(Context context, ArrayList<HashMap<String, String>> listDataHeader,
                                        ArrayList<ArrayList<HashMap<String, String>>> listChildData,
                                        ArrayList<ArrayList<HashMap<String, BluetoothGattCharacteristic>>> listChildProp) {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.mListDataChild = listChildData;
        this.mListPropChild = listChildProp;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        BluetoothGattCharacteristic chara = this.mListPropChild.get(groupPosition)
                .get(childPosititon).get(LIST_PROPERTIES);
        properties.add(0, BLEConnection.isCharacteristicReadable(chara));
        properties.add(1, BLEConnection.isCharacteristicWriteable(chara));
        properties.add(2, BLEConnection.isCharacteristicNotifiable(chara));
        properties.add(3, BLEConnection.isCharacteristicIndicatable(chara));

        permissions.add(0, isCharacteristicPermissionAvailable(chara,BluetoothGattCharacteristic.PERMISSION_READ));
        permissions.add(1, isCharacteristicPermissionAvailable(chara,BluetoothGattCharacteristic.PERMISSION_WRITE));
        permissions.add(2, isCharacteristicPermissionAvailable(chara,BluetoothGattCharacteristic.PERMISSION_READ_ENCRYPTED));
        permissions.add(3, isCharacteristicPermissionAvailable(chara,BluetoothGattCharacteristic.PERMISSION_WRITE_ENCRYPTED));
        return this.mListDataChild.get(groupPosition)
                .get(childPosititon);
    }

    private Boolean isCharacteristicPermissionAvailable(BluetoothGattCharacteristic chara, int permissionType) {
        return ((chara.getPermissions() & permissionType) != 0);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        LayoutInflater infalInflater = (LayoutInflater) this.mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (childPosition == 0) {
            convertView = infalInflater.inflate(R.layout.gatt_chara_exp_child_list_item_header, null);
            return convertView;
        }
        final HashMap<String, String> childMap = (HashMap) getChild(groupPosition, childPosition - 1);

        convertView = infalInflater.inflate(R.layout.gatt_chara_exp_child_list_item, null);

        TextView charaChildName = (TextView) convertView
                .findViewById(R.id.characteristic_name);
        TextView charaChildUuid = (TextView) convertView
                .findViewById(R.id.characteristic_uuid);
        TextView charaChildProperties = (TextView) convertView
                .findViewById(R.id.characteristic_properties);
        TextView charaChildPermissions = (TextView) convertView
                .findViewById(R.id.characteristic_permissions);

        charaChildName.setText(childMap.get(LIST_NAME));
        charaChildUuid.setText(childMap.get(LIST_UUID).toUpperCase());
        charaChildProperties.setText("");
        charaChildPermissions.setText("");

        if (properties.get(0)) {
            charaChildProperties.append(" Read".toUpperCase());
        }
        if (properties.get(1)) {
            if (properties.get(0)) {
                charaChildProperties.append(", Write".toUpperCase());
            } else {
                charaChildProperties.append(" Write".toUpperCase());
            }
        }
        if (properties.get(2)) {
            if (properties.get(1) || properties.get(0)) {
                charaChildProperties.append(", Notify".toUpperCase());
            } else {
                charaChildProperties.append(" Notify".toUpperCase());
            }
        }
        if (properties.get(3)) {
            if (properties.get(2)) {
                charaChildProperties.append("& Indicate".toUpperCase());
            } else {
                charaChildProperties.append(" Indicate".toUpperCase());
            }
        }


        if (permissions.get(0)) {
            charaChildPermissions.append(" Read".toUpperCase());
        }
        if (permissions.get(1)) {
            if (permissions.get(0)) {
                charaChildPermissions.append(", Write".toUpperCase());
            } else {
                charaChildPermissions.append(" Write".toUpperCase());
            }
        }
        if (permissions.get(2)) {
            if (permissions.get(1) || permissions.get(0)) {
                charaChildPermissions.append(", Read encrypted".toUpperCase());
            } else {
                charaChildPermissions.append(" Read encrypted".toUpperCase());
            }
        }
        if (permissions.get(3)) {
            if (permissions.get(2)) {
                charaChildPermissions.append("& Write encrypted".toUpperCase());
            } else {
                charaChildPermissions.append(" Write encrypted".toUpperCase());
            }
        }
        //Log.e("Count >>", " " + count);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.mListDataChild.get(groupPosition)
                .size() + 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mListDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        HashMap<String, String> headerMap = (HashMap) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.gatt_service_exp_list_item, null);
        }

        TextView serviceName = (TextView) convertView
                .findViewById(R.id.service_name);
        TextView serviceUuid = (TextView) convertView
                .findViewById(R.id.service_uuid);
        TextView serviceUuidText = (TextView) convertView
                .findViewById(R.id.service_uuid_text);
        ImageView indicator = (ImageView) convertView.findViewById(R.id.group_indicator);

        serviceName.setText(headerMap.get(LIST_NAME));
        serviceUuid.setText(headerMap.get(LIST_UUID).toUpperCase());

        if (isExpanded) {
            indicator.setImageResource(R.drawable.ic_expand_less_black_36dp);
            serviceName.setTextColor(Color.BLACK);
            serviceUuid.setTextColor(Color.BLACK);
            serviceUuidText.setTextColor(Color.BLACK);
        } else {
            indicator.setImageResource(R.drawable.ic_expand_more_black_36dp);
            serviceName.setTextColor(Color.GRAY);
            serviceUuid.setTextColor(Color.GRAY);
            serviceUuidText.setTextColor(Color.GRAY);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        if (childPosition == 0) {
            return false;
        }
        return true;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {


        ExpandableListView list = (ExpandableListView) ((GattDbService) mContext).findViewById(R.id.gatt_services_list);
        if (previousGroup != groupPosition && previousGroup != -1) {
            list.collapseGroup(previousGroup);
        }
        previousGroup = groupPosition;
        list.smoothScrollToPosition(groupPosition);

    }
}

