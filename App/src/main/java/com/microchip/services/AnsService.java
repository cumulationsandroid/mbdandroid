/*
 *
 *
 *     AnsService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.services;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.CallLog;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.microchip.bleanalyser.AppConstants;
import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.microchip.communicators.BLEDataReceiver;
import com.microchip.utils.AppUtils;

public class AnsService extends Service implements BLEDataReceiver.GattServerProfiles {
    public static boolean mFirstTimeActivity = true;
    private static String callerPhoneNumber = null;
    private static int mNewSms = 0;
    private static int mNewMissedCall = 0;
    private static String msgBody = null;
    private static com.atmel.blecommunicator.com.atmel.Services.AlertNotificationService mAlertNotificationService;
    private static int mUnreadMissedCallCount = 0;
    private static int mUnreadSmsCountValue = 0;
    private static boolean mUnReadNotificationStatus = false;
    private static AnsService instance;
    private boolean mRing = false;
    private boolean mCallReceived = false;
    private boolean mReadNotificationStatus = false;
    // Our handler for received Intents. This will be called whenever an Intent
    // with an action is broadcast
    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Got message: " + message);
            if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
                Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
                SmsMessage[] msgs;
                String msg_from = null;
                msgBody = null;
                mNewSms++;
                if (bundle != null) {
                    //---retrieve the SMS message received---
                    try {
                        Object[] pdus = (Object[]) bundle.get("pdus");
                        msgs = new SmsMessage[pdus.length];
                        for (int i = 0; i < msgs.length; i++) {
                            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                            msg_from = msgs[i].getOriginatingAddress();
                            msgBody = msgs[i].getMessageBody();
                        }
                        AppUtils.setIntSharedPreference(instance, AppUtils.ANS_SMS_NEW_COUNT, mNewSms);
                        AppUtils.setStringSharedPreference(instance, AppUtils.ANS_SMS_NEW_TEXT, msgBody);
                        notifyNewSms();
                        notifyUnReadSms();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (TelephonyManager.ACTION_PHONE_STATE_CHANGED.equalsIgnoreCase(intent.getAction())) {
                // Get the current Phone State
                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                if (state == null)
                    return;
                // If phone state "Ringing"
                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    callerPhoneNumber = null;
                    mRing = true;
                    // Get the Caller's Phone Number
                    Bundle bundle = intent.getExtras();
                    callerPhoneNumber = bundle.getString("incoming_number");
                }
                // If incoming call is received
                if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    mCallReceived = true;
                }
                // If phone is Idle
                if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    // If phone was ringing(ring=true) and not received(callReceived=false) , then it is a missed call
                    if (mRing && !mCallReceived) {
                        mNewMissedCall++;
                        //Toast.makeText(AlertNotification.this, "It was A MISSED CALL from : "+ callerPhoneNumber, Toast.LENGTH_LONG).show();
                        AppUtils.setIntSharedPreference(instance, AppUtils.ANS_MISSEDCALL_NEW_COUNT, mNewMissedCall);
                        notifyNewMissedCall();
                        notifyUnreadMissedCall();
                    }
                    mRing = false;
                    mCallReceived = false;
                }
            }
        }
    };

    private static void notifyUnReadSms() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //get unread sms count from provider
        int mPrevUnreadSmsCountValue = AppUtils.getIntSharedPreference(instance, AppUtils.ANS_SMS_UNREAD_COUNT);
        final Uri SMS_INBOX = Uri.parse("content://sms/inbox");

        if (permissionCheck()) {
            Cursor c = instance.getContentResolver().query(SMS_INBOX, null, "read = 0", null, null);
            mUnreadSmsCountValue = c.getCount();
            c.close();
            AppUtils.setIntSharedPreference(instance, AppUtils.ANS_SMS_UNREAD_COUNT, mUnreadSmsCountValue);
            if (mUnReadNotificationStatus) {
                if (mPrevUnreadSmsCountValue != mUnreadSmsCountValue) {
                    mAlertNotificationService.unreadAlertNotifyConnectedDevices(UUIDDatabase.UUID_ANS_UNREAD_ALERT_STATUS,
                            0, mUnreadSmsCountValue);
                    sendMessageToUi(AppConstants.ANS_UNREAD_SMS_UPDATE);
                }
            }
        } else {
            return;
        }


    }

    private static boolean permissionCheck() {

        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(instance.getBaseContext(),
                Manifest.permission.WRITE_CONTACTS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private static void notifyUnreadMissedCall() {
        //get unread missed call count from provider
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int mPrevUnreadMissedCallCount = AppUtils.getIntSharedPreference(instance, AppUtils.ANS_MISSEDCALL_UNREAD_COUNT);
        String where = CallLog.Calls.TYPE + "=" + CallLog.Calls.MISSED_TYPE + " AND " + CallLog.Calls.NEW + "=1";
        String[] projection = {CallLog.Calls.CACHED_NAME, CallLog.Calls.CACHED_NUMBER_LABEL, CallLog.Calls.TYPE};
        Cursor cursor = instance.getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, where, null, null);
        mUnreadMissedCallCount = cursor.getCount();
        cursor.close();
        Log.d("sendMessageToUi", "notifyUnreadMissedCall" + mUnreadMissedCallCount);
        Log.d("sendMessageToUi", "notifyUnreadMissedCall prev" + mPrevUnreadMissedCallCount);
        if (mUnReadNotificationStatus) {
            if (mPrevUnreadMissedCallCount != mUnreadMissedCallCount) {
                mAlertNotificationService.unreadAlertNotifyConnectedDevices(UUIDDatabase.UUID_ANS_UNREAD_ALERT_STATUS,
                        1, mUnreadMissedCallCount);
                sendMessageToUi(AppConstants.ANS_UNREAD_MISSED_CALL_UPDATE);
            }
        }
        AppUtils.setIntSharedPreference(instance, AppUtils.ANS_MISSEDCALL_UNREAD_COUNT, mUnreadMissedCallCount);
    }

    private synchronized static void sendMessageToUi(String action) {
        Intent intent = new Intent();
        intent.setAction(action);
        switch (action) {
            case AppConstants.ANS_NEW_MISSED_CALL_RECEIVED:
                intent.putExtra(AppConstants.ANS_NEW_MISSED_CALL_COUNT, String.valueOf(mNewMissedCall));
                intent.putExtra(AppConstants.ANS_NEW_MISSED_CALL_TEXT, callerPhoneNumber);
                break;
            case AppConstants.ANS_NEW_SMS_RECEIVED:
                intent.putExtra(AppConstants.ANS_NEW_SMS_COUNT, String.valueOf(mNewSms));
                intent.putExtra(AppConstants.ANS_NEW_SMS_TEXT, msgBody);
                break;
            case AppConstants.ANS_UNREAD_SMS_UPDATE:
                intent.putExtra(AppConstants.ANS_UNREAD_SMS_COUNT, String.valueOf(mUnreadSmsCountValue));
                break;
            case AppConstants.ANS_UNREAD_MISSED_CALL_UPDATE:
                Log.d("sendMessageToUi", "AppConstants.ANS_UNREAD_MISSED_CALL_UPDATE");
                Log.d("sendMessageToUi", "mUnreadMissedCallCount" + mUnreadMissedCallCount);
                intent.putExtra(AppConstants.ANS_UNREAD_MISSED_CALL_COUNT, String.valueOf(mUnreadMissedCallCount));
                break;
            case AppConstants.ANS_NOTIFICATION_CHANGE_REQUEST:
                break;
            default:
                break;
        }
        LocalBroadcastManager.getInstance(instance).sendBroadcast(intent);
    }

    public static void checkUnreadSmsMissedCall() {
        notifyUnReadSms();
        notifyUnreadMissedCall();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v("ANS", "Service  ANS created again");
        instance = this;
        mNewSms = 0;
        mNewMissedCall = 0;
        BLEDataReceiver.setGattServerListener(this);
        mReadNotificationStatus = AppUtils.getBooleanSharedPreference(this, Utils.ANS_READ_NOTIFICATION_STATUS);
        mUnReadNotificationStatus = AppUtils.getBooleanSharedPreference(this, Utils.ANS_UNREAD_NOTIFICATION_STATUS);
        mAlertNotificationService = com.atmel.blecommunicator.com.atmel.Services.AlertNotificationService.getInstance();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        filter.addAction("android.intent.action.PHONE_STATE");
        try {
            this.registerReceiver(mMessageReceiver, filter);
        } catch (Exception E) {
            E.printStackTrace();
        }
        final Uri SMS_INBOX = Uri.parse("content://sms/inbox");
        if (checkPermission(this)) {
            Cursor c = getContentResolver().query(SMS_INBOX, null, "read = 0", null, null);
            mUnreadSmsCountValue = c.getCount();
            c.close();
            AppUtils.setIntSharedPreference(instance, AppUtils.ANS_SMS_UNREAD_COUNT, mUnreadSmsCountValue);
            String where = CallLog.Calls.TYPE + "=" + CallLog.Calls.MISSED_TYPE + " AND " + CallLog.Calls.NEW + "=1";
            String[] projection;
            projection = new String[]{CallLog.Calls.CACHED_NAME, CallLog.Calls.CACHED_NUMBER_LABEL, CallLog.Calls.TYPE};
            Cursor cursor = this.getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, where, null, null);
            mUnreadMissedCallCount = cursor.getCount();
            cursor.close();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void notifyNewSms() {
        if (mReadNotificationStatus) {
            mAlertNotificationService.newAlertNotifyConnectedDevices(UUIDDatabase.UUID_ANS_NEW_ALERT, 0, mNewSms, msgBody);
        }
        sendMessageToUi(AppConstants.ANS_NEW_SMS_RECEIVED);
    }

    private void notifyNewMissedCall() {
        if (mReadNotificationStatus) {
            mAlertNotificationService.newAlertNotifyConnectedDevices(UUIDDatabase.UUID_ANS_NEW_ALERT, 1, mNewMissedCall,
                    callerPhoneNumber);
        }
        sendMessageToUi(AppConstants.ANS_NEW_MISSED_CALL_RECEIVED);
    }

    @Override
    public void onLeWriteRequest(String characteristics, int requestId, int offset) {

    }

    @Override
    public void onLeReadRequest(String characteristics, int requestId, int offset) {
        //sms and missed call
        String SUPPORTED_CATEGORY = "30";
        byte[] value = null;
        if (offset == 0) {
            value = Utils.hexStringToByteArray(SUPPORTED_CATEGORY);
        }
        BLEConnection.GattServerSendResponse(requestId, offset, value);
    }

    public void onLeNotifyRequest() {
        setNotificationDetails();
        if (mFirstTimeActivity) {
            //first time launch of service should inform peripheral
            notifyUnReadSms();
            notifyUnreadMissedCall();
            notifyNewMissedCall();
            notifyNewSms();
            mFirstTimeActivity = false;
        }
    }

    private void setNotificationDetails() {
        mReadNotificationStatus = AppUtils.getBooleanSharedPreference(this, Utils.ANS_READ_NOTIFICATION_STATUS);
        mUnReadNotificationStatus = AppUtils.getBooleanSharedPreference(this, Utils.ANS_UNREAD_NOTIFICATION_STATUS);
        // Send an Intent with an action named "custom-event-name". The Intent sent should
// be received by the ReceiverActivity.
        sendMessageToUi(AppConstants.ANS_NOTIFICATION_CHANGE_REQUEST);
    }

    @Override
    public void onLeWriteRequest(String characteristics, int requestId, int offset, byte[] value) {

    }

    private  boolean checkPermission(final Context context) {
        return (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED);


    }


}
