/*
 *
 *
 *     PAService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.microchip.bleanalyser.AppConstants;
import com.atmel.blecommunicator.com.atmel.Services.PhoneAlertService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.microchip.communicators.BLEDataReceiver;
import com.microchip.utils.AppUtils;

public class PAService extends Service implements BLEDataReceiver.GattServerProfiles {
    public static int mAlertStatus;
    public static String mRingerStatus;
    public static String controlPoint;
    private static PAService instance;
    final int isRing = 1, isVibrate = 2, isSilent = 3;
    String RINGER_MODE_SILENT = "0x00";
    String RINGER_MODE_VIBRATE = "0x00";
    String RINGER_MODE_NORMAL = "0x01";
    String RINGER_SILENT = "Ringer Silent";
    String RINGER_VIBRATION = "Ringer Vibration";
    String RINGER_NORMAL = "Ringer Normal";
    AudioManager mAudioManager;
    int silent = 0, vibrate = 0, normal = 0, display = 1;
    boolean mNotificationEnable = true;
    private PhoneAlertService mAlertService;
    private SettingsContentObserver mSettingsContentObserver;
    private boolean isRinging = false;
    private int alertStatuscout = 0;
    private boolean selfSettingsChange = false;

    /*
    * This method is using to send the Broadcast for the GUI updates
    * @param mAlertStatus this value denotes the current Alert Status 1 is Ringer 2 is Vibrate  3 is Silent
    * @param mRingerStatus this value denotes Ringer Silent or Ringer Normal
    * @param controlPoint this value denotes for Ringer Silent/Ringer Vibrate/Ringer Normal
    *
    * */
    private synchronized static void sendMessageToUi(int mAlertStatus, String mRingerStatus, String action, String controlPoint) {
        Intent intent = new Intent();
        intent.setAction(action);
        switch (action) {
            case AppConstants.PAS_CONTROL_POINT:
                intent.putExtra(AppConstants.PAS_ALERT_STATUS, mAlertStatus);
                intent.putExtra(AppConstants.PAS_RINGER_STATUS, mRingerStatus);
                intent.putExtra(AppConstants.PAS_CONTROL_POINT, controlPoint);

                break;
            case AppConstants.PAS_ACTION:
                intent.putExtra(AppConstants.PAS_ALERT_STATUS, mAlertStatus);
                intent.putExtra(AppConstants.PAS_RINGER_STATUS, mRingerStatus);
                break;
        }
        LocalBroadcastManager.getInstance(instance).sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("PASservice", "service started");
        instance = this;
        controlPoint = "";
        BLEDataReceiver.setGattServerListener(this);
        mNotificationEnable = AppUtils.getBooleanSharedPreference(this, Utils.PHONE_ALERT_STATUS);
        mAlertService = PhoneAlertService.getInstance();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mUpdateMode(mAudioManager.getRingerMode());
        mSettingsContentObserver = new SettingsContentObserver(this, new Handler());
        getApplicationContext().getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mSettingsContentObserver);

        if (mNotificationEnable) {
            SendAlertSettings(normal, vibrate, display);
        }

    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLeWriteRequest(String characteristics, int requestid, int offset) {

    }


    @Override
    public void onLeReadRequest(String characteristics, int requestid, int offset) {
        Log.d("PASservice", "onLeReadRequest");
        switch (characteristics) {
            case "00002a3f-0000-1000-8000-00805f9b34fb"://Alert Status
//               ringerValue(RINGER_MODE_SILENT);
                writeAlertStatus(normal, vibrate, display, requestid, offset);
                break;
            case "00002a41-0000-1000-8000-00805f9b34fb"://Ring Settings
                if (silent == 1) {
                    mAlertService.writePhoneAlert(requestid, offset, getByteArray(RINGER_MODE_SILENT));
                } else {
                    mAlertService.writePhoneAlert(requestid, offset, getByteArray(RINGER_MODE_NORMAL));
                }
                break;


        }
    }

    @Override
    public void onLeNotifyRequest() {

    }

    @Override
    public void onLeWriteRequest(String characteristics, int requestid, int offset, byte[] value) {
        Log.d("PASservice", "onLeWriteRequest value");
        final int data = value[0];
        AppUtils.setIntSharedPreference(this, AppUtils.PHONE_ALERT_RINGER_CONTROL_VALUE, data);

        ringerSettingPoint(data);

    }

    private void ringerSettingPoint(int data) {
        mNotificationEnable = AppUtils.getBooleanSharedPreference(this, Utils.PHONE_ALERT_STATUS);
        switch (data) {
            case 1:
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
//                mRingerStatus.setText("Ringer Silent");
//                mControlPoint.setText("Ringer Vibration");
//                setRingerStatus(isVibrate);
                sendMessageToUi(isVibrate, RINGER_SILENT, AppConstants.PAS_CONTROL_POINT, RINGER_VIBRATION);
                if (mNotificationEnable)
                    ringerValue(RINGER_MODE_SILENT);
                mAlertStatus = isVibrate;
                mRingerStatus = RINGER_SILENT;
                controlPoint = RINGER_VIBRATION;
                silent = 1;
                vibrate = 1;
                normal = 0;
                break;
            case 2:
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//                mRingerStatus.setText("Ringer Silent");
//                mControlPoint.setText("Ringer Silent");
//                setRingerStatus(isSilent);
                sendMessageToUi(isSilent, RINGER_SILENT, AppConstants.PAS_CONTROL_POINT, RINGER_SILENT);
                if (mNotificationEnable)
                    ringerValue(RINGER_MODE_SILENT);
                mAlertStatus = isSilent;
                mRingerStatus = RINGER_SILENT;
                controlPoint = RINGER_SILENT;
                silent = 1;
                vibrate = 0;
                normal = 0;
                break;
            case 3:
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

//                mRingerStatus.setText("Ringer Normal");
//                mControlPoint.setText("Ringer Normal");
//                setRingerStatus(isRing);
                sendMessageToUi(isRing, RINGER_NORMAL, AppConstants.PAS_CONTROL_POINT, RINGER_NORMAL);
                if (mNotificationEnable)
                    ringerValue(RINGER_MODE_NORMAL);
                mAlertStatus = isRing;
                mRingerStatus = RINGER_NORMAL;
                controlPoint = RINGER_NORMAL;
                silent = 0;
                vibrate = 0;
                normal = 1;
                Log.e("controlPoint", "" + controlPoint);
                break;
            case 4:
                break;
        }
    }

    private void mUpdateMode(int ringerMode) {
        mNotificationEnable = AppUtils.getBooleanSharedPreference(this, Utils.PHONE_ALERT_STATUS);
        switch (ringerMode) {
            case AudioManager.RINGER_MODE_VIBRATE:

                if (mNotificationEnable) {
                    ringerValue(RINGER_MODE_SILENT);
                }
//                setRingerStatus(isVibrate);
//                mRingerStatus.setText("Ringer Silent");
                sendMessageToUi(isVibrate, RINGER_SILENT, AppConstants.PAS_ACTION, "");
                mAlertStatus = isVibrate;
                mRingerStatus = RINGER_SILENT;
                // controlPoint="";
                silent = 1;
                vibrate = 1;
                normal = 0;
                break;
            case AudioManager.RINGER_MODE_SILENT:

                // mRingerStatus.setText("Ringer Silent");
                if (mNotificationEnable) {
                    ringerValue(RINGER_MODE_VIBRATE);
                }
                //setRingerStatus(isSilent);
                sendMessageToUi(isSilent, RINGER_SILENT, AppConstants.PAS_ACTION, "");
                mAlertStatus = isSilent;
                mRingerStatus = RINGER_SILENT;
                //controlPoint="";
                silent = 1;
                vibrate = 0;
                normal = 0;
                break;
            case AudioManager.RINGER_MODE_NORMAL:

//                mRingerStatus.setText("Ringer Normal");
//                setRingerStatus(isRing);
                sendMessageToUi(isRing, RINGER_NORMAL, AppConstants.PAS_ACTION, "");
                mAlertStatus = isRing;
                mRingerStatus = RINGER_NORMAL;
                //controlPoint="";
                silent = 0;
                vibrate = 0;
                normal = 1;
                if (mNotificationEnable) {
                    ringerValue(RINGER_MODE_NORMAL);
                }
                break;
        }
    }

    private void SendAlertSettings(int ringer, int vibrate, int display) {
      /*  ByteBuffer b = ByteBuffer.allocate(6);
        b.order(ByteOrder.nativeOrder());
        short mRingerState = (short) ringer;
        short mVibrateState = (short) vibrate;
        short mDisplayState = (short) display;
        b.putShort(0, mRingerState);
        b.putShort(2, mVibrateState);
        b.putShort(4, mDisplayState);*/

      /*  valueByte[0] = (byte) ringer;
        valueByte[1] = (byte) vibrate;
        valueByte[2] = (byte) display;
        Log.e("ringer", ringer + "");
        Log.e("vibrate", vibrate + "");
        Log.e("display", display + "");*/
        /*Log.e("valueOf",""+String.valueOf(b.array()));*/
        byte[] valueByte = new byte[1];
        byte val;
        val = (byte) ringer;
        val = (byte) (val | ((byte) vibrate << 1));
        val = (byte) (val | ((byte) display << 2));
        valueByte[0] = val;
        mAlertService.notifyAlertStatus(valueByte);
        Log.e("SendAlertSettings", valueByte[0] + "");

    }

    /**
     * Method to convert hex to byteArray
     */
    private void ringerValue(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertStringtoByte(trimmedByte);
            }
        }
        mAlertService.notifyRingerSettings(valueByte);

    }

    /**
     * Convert the string to byte
     *
     * @param string
     * @return
     */
    private int convertStringtoByte(String string) {
        return Integer.parseInt(string, 8);
    }

    private void writeAlertStatus(int ringer, int vibrate, int display, int requestId, int offset) {
      /*  ByteBuffer b = ByteBuffer.allocate(6);
        b.order(ByteOrder.nativeOrder());
        short mRingerState = (short) ringer;
        short mVibrateState = (short) vibrate;
        short mDisplayState = (short) display;
        b.putShort(0, mRingerState);
        b.putShort(2, mVibrateState);
        b.putShort(4, mDisplayState);
        Log.e("writeAlertStatus", "" + String.valueOf(b.array()));*/
        /*byte[] valueByte = new byte[3];
        valueByte[0] = (byte) ringer;
        valueByte[1] = (byte) vibrate;
        valueByte[2] = (byte) display;
        Log.e("ringer", ringer + "");
        Log.e("vibrate", vibrate + "");
        Log.e("display", display + "");
        Log.e("writeAlertStatus", "writeAlertStatus");
*/
        byte[] valueByte = new byte[1];
        byte val;
        val = (byte) ringer;
        val = (byte) (val | ((byte) vibrate << 1));
        val = (byte) (val | ((byte) display << 2));
        valueByte[0] = val;
        mAlertService.writAlertStatus(requestId, offset, valueByte);
    }

    private byte[] getByteArray(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertStringtoByte(trimmedByte);
            }
        }

        return valueByte;

    }

    @Override
    public void onDestroy() {
        Log.d("onDestroy", "unregister");
        try {
            this.getContentResolver().unregisterContentObserver(mSettingsContentObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    /**
     * Our handler for received Intents. This will be called whenever an Intent
     * with an action named "custom-event-name" is broadcasted.
     */

    public class SettingsContentObserver extends ContentObserver {
        int previousVolume;
        Context context;

        public SettingsContentObserver(Context c, Handler handler) {
            super(handler);
            context = c;

            AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        }


        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Log.d("SettingsContentObserver", "onChange");
            mNotificationEnable = AppUtils.getBooleanSharedPreference(PAService.this, Utils.PHONE_ALERT_STATUS);
            // boolean status = false;
            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//            int currentVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
//            int current = am.getStreamVolume(AudioManager.STREAM_RING);
            switch (am.getRingerMode()) {
                case AudioManager.RINGER_MODE_NORMAL:
                    if (!isRinging) {
                        isRinging = true;
//                    mRingerStatus.setText("Ringer Normal");
//                    setRingerStatus(isRing);
                        sendMessageToUi(isRing, RINGER_NORMAL, AppConstants.PAS_ACTION, "");
                        if (mNotificationEnable)
                            ringerValue(RINGER_MODE_NORMAL);
                        silent = 0;
                        vibrate = 0;
                        normal = 1;
                        mAlertStatus = isRing;
                        mRingerStatus = RINGER_NORMAL;
                        //controlPoint="";
                        if (mNotificationEnable)
                            SendAlertSettings(normal, vibrate, display);
                    }
                    break;

                case AudioManager.RINGER_MODE_VIBRATE:
                    isRinging = false;
                    sendMessageToUi(isVibrate, RINGER_SILENT, AppConstants.PAS_ACTION, "");
                    // mRingerStatus.setText("Ringer Silent");
                    if (mNotificationEnable)
                        ringerValue(RINGER_MODE_VIBRATE);
                    // setRingerStatus(isVibrate);
                    silent = 1;
                    vibrate = 1;
                    normal = 0;
                    mAlertStatus = isVibrate;
                    mRingerStatus = RINGER_SILENT;
                    // controlPoint="";
                    if (mNotificationEnable)
                        SendAlertSettings(normal, vibrate, display);
                    break;

                case AudioManager.RINGER_MODE_SILENT:

                    sendMessageToUi(isSilent, RINGER_SILENT, AppConstants.PAS_ACTION, "");
                    if (mNotificationEnable)
                        ringerValue(RINGER_MODE_SILENT);
                    // mRingerStatus.setText("Ringer Silent");
                    //setRingerStatus(isSilent);
                    silent = 1;
                    vibrate = 0;
                    normal = 0;
                    mAlertStatus = isSilent;
                    mRingerStatus = RINGER_SILENT;
                    // controlPoint="";
                    if (mNotificationEnable)
                        SendAlertSettings(normal, vibrate, display);
                    break;

                default:
                    break;
            }


        }
    }
}
