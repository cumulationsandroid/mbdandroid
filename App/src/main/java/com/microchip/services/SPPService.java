/*
 *
 *
 *     SPPService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.services;

import android.app.Service;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.microchip.adapters.SerialPort.MessageModel;
import com.microchip.bleanalyser.BLEApplication;
import com.atmel.blecommunicator.com.atmel.Services.SerialPortService;
import com.microchip.communicators.BLEDataReceiver;
import com.microchip.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class SPPService extends Service implements BLEDataReceiver.GATTProfiles {

    static public List<MessageModel> mMessageList = null;
    static BluetoothGattService mBluetoothGattService;
    static SerialPortService mSerialPortService;
    static BLEApplication application;

    static public void initialize() {
        mBluetoothGattService = application.getmBluetoothGattService();
        Log.d("mBluetoothGattService", "" + mBluetoothGattService);
        mSerialPortService = SerialPortService.getInstance();
        mSerialPortService.setmSerialPortService(mBluetoothGattService);
        mSerialPortService.getCharacteristic();
        sendNotifyCharacteristic();
    }

    /**
     * this method is using for sending message. In this method we will check the Android version,
     * in Kitkat we can send only 20 characters but above Kitkat we can send 150 characters . So we are creating the message
     * array list by spliting the actual message but the corresponding values.
     *
     * @param newMessage this the is actual message string
     * @return Nothing
     */
    static public void sendingMessage(String newMessage) {


        String[] messageArray;
        int interval;
        if (Build.VERSION_CODES.KITKAT < Build.VERSION.SDK_INT) {
            interval = 150;
        } else {
            interval = 20;
        }
        if (newMessage.length() > interval) {
            messageArray = splitStringEvery(newMessage, interval);
            for (int i = 0; i < messageArray.length; i++) {
                mSerialPortService.notifyConnectedDevices(messageArray[i]);
            }
        } else {
            mSerialPortService.notifyConnectedDevices(newMessage);
        }

    }

    /**
     * this method is using to split the String as per the interval count
     *
     * @param s        this is the entire string
     * @param interval interval count
     * @return String[] resultant string array
     */
    static public String[] splitStringEvery(String s, int interval) {
        int arrayLength = (int) Math.ceil(((s.length() / (double) interval)));
        String[] result = new String[arrayLength];

        int j = 0;
        int lastIndex = result.length - 1;
        for (int i = 0; i < lastIndex; i++) {
            result[i] = s.substring(j, j + interval);
            j += interval;
        } //Add the last bit
        result[lastIndex] = s.substring(j);

        return result;
    }

    /*
    * this method is using to enable the notify characteristic
    * also we are changing the Mtu size, because in lolopop we can send and receive more than 20 characters
    * */
    static public void sendNotifyCharacteristic() {
        mSerialPortService.startNotifyCharacteristic();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                mSerialPortService.requestMtuChange();
            }
        }, 1000);
    }

    public static void setGattListener() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        BLEDataReceiver.setGattListener(this);
        if (mMessageList == null) {
            mMessageList = new ArrayList<>();
        }
        if (application == null) {
            application = (BLEApplication) getApplication();
        }
        //initialize();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*
    * if we got any message from peer device we will get that message in this override method
    * after getting the actual message we are creating a MessageModel with that actual string for showing
    * after we will send a Broadcast for GUI updates
    * */
    @Override
    public void onLeCharacteristicChanged(boolean status) {
        if (status) {
            String relievedMessage = mSerialPortService.mRecevicedMessage;
            addNewMessage(new MessageModel(relievedMessage, false));
            Intent intent = new Intent(AppUtils.SPP_MESSAGE_RECEIVED);
            intent.putExtra(AppUtils.SPP_MESSAGE_RECEIVED_STATUS, true);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

    @Override
    public void onLeCharacteristicWrite(boolean status) {

    }

    @Override
    public void onLeCharacteristicRead(boolean status) {

    }

    @Override
    public void onLeCharacteristicRead(boolean status, String data, int code) {

    }

    @Override
    public void onLeReadRemoteRssi(int rssi) {

    }

    @Override
    public void onLeDescriptorWrite(boolean status) {

    }

    /*
    * adding MessageModel to existing Message Array */
    void addNewMessage(MessageModel m) {
        mMessageList.add(m);
        //application.addSPPMsgList(mMessageList);
    }


}
