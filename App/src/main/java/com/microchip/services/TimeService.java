/*
 *
 *
 *     TimeService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.TextView;

import com.microchip.bleanalyser.AppConstants;
import com.atmel.blecommunicator.com.atmel.Services.CurrentTimeService;
import com.atmel.blecommunicator.com.atmel.Services.NextDstChangeService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.microchip.communicators.BLEDataReceiver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;


public class TimeService extends Service implements BLEDataReceiver.GattServerProfiles {
    //UUIDs-small
    private static final String  UUID_CURRENT_TIME = "00002a2b-0000-1000-8000-00805f9b34fb";
    private static final String  UUID_LOCAL_TIME = "00002a0f-0000-1000-8000-00805f9b34fb";
    private static final String  UUID_DST_TIME = "00002a11-0000-1000-8000-00805f9b34fb";
    private static final String  UUID_REFERENCE_TIME = "00002a14-0000-1000-8000-00805f9b34fb";
    private static final String  UUID_TIME_UPDATE_CONTROL_POINT = "00002a16-0000-1000-8000-00805f9b34fb";
    private static final String  UUID_TIME_UPDATE_STATE = "00002a17-0000-1000-8000-00805f9b34fb";
    private static final int STD_TIME = 0;
    private static final int HALF_DAYLIGHT_TIME = 1800000;
    private static final int DAYLIGHT_TIME = 3600000;
    private static final int DOUBLE_DAYLIGHT_TIME = 7200000;
    private static final int DAYS_IN_LEAP_YEAR = 256;
    private static final int ACCURACY_UNKNOWN = 255;
    private static final int DAYS_SINCE_UPDATE = 255;
    private static final int HOURS_SINCE_UPDATE = 255;
    private static final int TIME_UPDATE_CURRENTSTATE = 255;
    private static final int TIME_UPDATE_RESULT = 0;
    private static TimeService instance;
    private CurrentTimeService mCurrentTimeService;
    private NextDstChangeService mNextDstChangeService;
    private int mDstValue = 0;
    private TextView mTimeValue = null;
    private TextView mDateValue = null;
    private TextView mAmPmValue = null;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mCurrentTimeService = CurrentTimeService.getInstance();
        mNextDstChangeService = NextDstChangeService.getInstance();
        BLEDataReceiver.setGattServerListener(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLeWriteRequest(String characteristics, int requestId, int offset, byte[] value) {

    }

    @Override
    public void onLeWriteRequest(String characteristics, int requestId, int offset) {

    }

    @Override
    public void onLeReadRequest(String characteristics, int requestId, int offset) {

        switch (characteristics) {
            case UUID_CURRENT_TIME: {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int monthday = c.get(Calendar.DAY_OF_MONTH);
                int hour = c.get(Calendar.HOUR);
                int minute = c.get(Calendar.MINUTE);
                int second = c.get(Calendar.SECOND);
                int weekday = c.get(Calendar.DAY_OF_WEEK);
                byte[] timeData = new byte[10];
                timeData[0] = (byte) (year % DAYS_IN_LEAP_YEAR);
                timeData[1] = (byte) (year / DAYS_IN_LEAP_YEAR);
                timeData[2] = (byte) (month + 1);
                timeData[3] = (byte) (monthday);
                timeData[4] = (byte) (hour);
                timeData[5] = (byte) (minute);
                timeData[6] = (byte) (second);
                timeData[7] = (byte) (weekday - 1);
                timeData[8] = 0;
                timeData[9] = 0;
                mCurrentTimeService.writeCurrentTime(requestId, offset, timeData);
                break;
            }
            case UUID_LOCAL_TIME: {
                byte[] timeZone = new byte[2];
                String utc;
                int timezoneUtcValue;
                // int timezoneDstValue = 0;
                utc = timeZone();
                utc = "UTC" + utc;
                timezoneUtcValue = convertUtcToValue(utc);
                timeZone[0] = (byte) (timezoneUtcValue);
                // timezoneDstValue = TimeZone.getTimeZone("UTC").getDSTSavings();
                timeZone[1] = (byte) (mDstValue);
                mCurrentTimeService.writeLocalTime(requestId, 0, timeZone);
                break;
            }
            case  UUID_DST_TIME: {
                byte[] value;
                int utcValue = 0;
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int monthday = c.get(Calendar.DAY_OF_MONTH);
                int hour = c.get(Calendar.HOUR);
                int minute = c.get(Calendar.MINUTE);
                int second = c.get(Calendar.SECOND);
                byte[] timeData = new byte[8];
                timeData[0] = (byte) (year % DAYS_IN_LEAP_YEAR);
                timeData[1] = (byte) (year / DAYS_IN_LEAP_YEAR);
                timeData[2] = (byte) (month + 1);
                timeData[3] = (byte) (monthday);
                timeData[4] = (byte) (hour);
                timeData[5] = (byte) (minute);
                timeData[6] = (byte) (second);
                value = Utils.bytesFromInt(utcValue);
                //utcValue = TimeZone.getTimeZone("UTC").getDSTSavings();
                timeData[7] = (byte) (mDstValue);
                mNextDstChangeService.writeLocalTime(requestId, 0, timeData);
                mNextDstChangeService.writeLocalTime(requestId, offset, value);
            }
            break;

            case UUID_REFERENCE_TIME: {
                byte[] referenceTime = new byte[4];
                try {
                    int source = Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME);
                    if (source == 1) {
                        referenceTime[0] = (byte) (AppConstants.TIME_SOURCE_CELLULAR_NETWORK);
                    } else {
                        referenceTime[0] = (byte) (AppConstants.TIME_SOURCE_MANUAL);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //TO DO, need to change hard coding
                referenceTime[1] = (byte) (ACCURACY_UNKNOWN);
                referenceTime[2] = (byte) (DAYS_SINCE_UPDATE);
                referenceTime[3] = (byte) (HOURS_SINCE_UPDATE);
                mCurrentTimeService.writeLocalTime(requestId, 0, referenceTime);
                break;
            }
            case UUID_TIME_UPDATE_STATE: {
                byte[] referenceTime = new byte[2];
                //TO DO, need to change hard coding
                referenceTime[0] = (byte) (TIME_UPDATE_CURRENTSTATE);
                referenceTime[1] = (byte) (TIME_UPDATE_RESULT);
                mCurrentTimeService.writeLocalTime(requestId, offset, referenceTime);
                break;
            }
        }
    }

    @Override
    public void onLeNotifyRequest() {
        /*
        super.onLeNotifyRequest();
        if (mNotificationStatus) {
            mNotificationStatus = true;
            mHandler.removeCallbacks(mHandlerTask);
            mHandlerTask.run();
        } else {
            mNotificationStatus = false;

        }
        setNotificationDetails();*/
    }

    private String timeZone() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault());
        String timeZone = new SimpleDateFormat("Z").format(calendar.getTime());
        return timeZone.substring(0, 3) + ":" + timeZone.substring(3, 5);
    }

    private int convertUtcToValue(String utc) {
        int utcValue = 0;
        switch (utc) {
            case "UTC-12:00":
                utcValue = -48;
                break;
            case "UTC-11:00":
                utcValue = -44;
                break;
            case "UTC-10:00":
                utcValue = -40;
                break;
            case "UTC-09:30":
                utcValue = -38;
                break;
            case "UTC-09:00":
                utcValue = -36;
                break;
            case "UTC-08:00":
                utcValue = -32;
                break;
            case "UTC-07:00":
                utcValue = -28;
                break;
            case "UTC-06:00":
                utcValue = -24;
                break;
            case "UTC-05:00":
                utcValue = -20;
                break;
            case "UTC-04:30":
                utcValue = -18;
                break;
            case "UTC-04:00":
                utcValue = -16;
                break;
            case "UTC-03:30":
                utcValue = -14;
                break;
            case "UTC-03:00":
                utcValue = -12;
                break;
            case "UTC-02:00":
                utcValue = -8;
                break;
            case "UTC-01:00":
                utcValue = -4;
                break;
            case "UTC+00:00":
                utcValue = 0;
                break;
            case "UTC+14:00":
                utcValue = 56;
                break;
            case "UTC+13:00":
                utcValue = 52;
                break;
            case "UTC+12:45":
                utcValue = 51;
                break;
            case "UTC+12:00":
                utcValue = 48;
                break;
            case "UTC+11:30":
                utcValue = 46;
                break;
            case "UTC+11:00":
                utcValue = 44;
                break;
            case "UTC+10:30":
                utcValue = 42;
                break;
            case "UTC+10:00":
                utcValue = 40;
                break;
            case "UTC+09:30":
                utcValue = 38;
                break;
            case "UTC+09:00":
                utcValue = 36;
                break;
            case "UTC+08:45":
                utcValue = 35;
                break;
            case "UTC+08:00":
                utcValue = 32;
                break;
            case "UTC+07:00":
                utcValue = 28;
                break;
            case "UTC+06:30":
                utcValue = 26;
                break;
            case "UTC+06:00":
                utcValue = 24;
                break;
            case "UTC+05:45":
                utcValue = 23;
                break;
            case "UTC+05:30":
                utcValue = 22;
                break;
            case "UTC+05:00":
                utcValue = 20;
                break;
            case "UTC+04:30":
                utcValue = 18;
                break;
            case "UTC+04:00":
                utcValue = 16;
                break;
            case "UTC+03:30":
                utcValue = 14;
                break;
            case "UTC+03:00":
                utcValue = 12;
                break;
            case "UTC+02:00":
                utcValue = 8;
                break;
            case "UTC+01:00":
                utcValue = 4;
                break;
        }
        return utcValue;
    }

}
