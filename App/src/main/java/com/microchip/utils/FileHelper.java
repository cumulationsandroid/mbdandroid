/*
 *
 *
 *     FileHelper.java
 *   
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.microchip.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileHelper {
    public static final String ATMEL_FOLDER = "Atmel";
    private static final String TAG = "FileHelper";

//	public static boolean newSamplesAvailable(final Context context) {
//		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//		final int version = preferences.getInt(PREFS_SAMPLES_VERSION, 0);
//		return version < CURRENT_SAMPLES_VERSION;
//	}

    public static void createSamples(final Context context) {
//		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//		final int version = preferences.getInt(PREFS_SAMPLES_VERSION, 0);
//		if (version == CURRENT_SAMPLES_VERSION)
//			return;

		/*
         * Copy example bin files to the external storage.
		 */
        final File root = new File(Environment.getExternalStorageDirectory(), ATMEL_FOLDER);
        if (!root.exists()) {
            root.mkdir();
        }

     /*   // nrf6310 files
        File f = new File(root, "otau_bas_dis_1_2_2.bin");
        if (!f.exists()) {
            copyRawResource(context, R.raw.otau_bas_dis_1_2_2, f);
        }

        f = new File(root, "otau_bas_dis_1_2_3.bin");
        if (!f.exists()) {
            copyRawResource(context, R.raw.otau_bas_dis_1_2_3, f);
        }*/

    }

    /**
     * Copies the file from res/raw with given id to given destination file. If dest does not exist it will be created.
     *
     * @param context  activity context
     * @param rawResId the resource id
     * @param dest     destination file
     */
    private static void copyRawResource(final Context context, final int rawResId, final File dest) {
        try {
            final InputStream is = context.getResources().openRawResource(rawResId);
            final FileOutputStream fos = new FileOutputStream(dest);

            final byte[] buf = new byte[1024];
            int read;
            try {
                while ((read = is.read(buf)) > 0)
                    fos.write(buf, 0, read);
            } finally {
                is.close();
                fos.close();
            }
        } catch (final IOException e) {
            Log.e(TAG, "Error while copying HEX file " + e.toString());
        }
    }
}
