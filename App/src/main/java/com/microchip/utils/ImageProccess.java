/*
 *
 *
 *     ImageProccess.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.utils;

import android.content.Context;

import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;

import java.util.UUID;

/**
 * Created by
 */
public class ImageProccess {
    /**
     * @param context
     * @param uuid
     * @return
     */
    public static int getImage(Context context, UUID uuid) {
        int drawable = 0;
        if (GattAttributes.HEALTH_TEMP_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.ht_service_img;
        } else if (GattAttributes.IMMEDIATE_ALERT_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.proximity_immediate_img;
        } else if (GattAttributes.LINK_LOSS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.proximity_linkloss_img;
        } else if (GattAttributes.TRANSMISSION_POWER_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.proximity_img;
        } else if (GattAttributes.DEVICE_INFORMATION_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.ht_device_img;
        } else if (GattAttributes.HEART_RATE_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.heartrate_service;
        } else if (GattAttributes.BLOOD_PRESSURE_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.blood_pressure_service;
        } else if (GattAttributes.SCAN_PARAMETERS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.scan_parameters_service;
        } else if (GattAttributes.BATTERY_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.battery_service;
        } else if (GattAttributes.PHONE_ALERT_STATUS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.phone_alert_service;
        }else if (GattAttributes.PHONE_ALERT_STATUS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.phone_alert_service;
        }else if (GattAttributes.CURRENT_TIME_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.current_time_service;
        }else if (GattAttributes.NEXT_DST_CHANGE_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.dst_service;
        }else if (GattAttributes.REFERENCE_TIME_UPDATE_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.reference_time_update_service;
        }else if (GattAttributes.SERIAL_PORT.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.serialport_service;
        }else if (GattAttributes.SERIAL_PORT_END_POINT.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.serialport_service;
        }else if (GattAttributes.ALERT_NOTIFICATION_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.alert_notification_service;
        }else if (GattAttributes.GENERIC_ACCESS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            drawable = R.drawable.gatt_db;
        } else {
            drawable = R.drawable.gatt_db;
        }
        return drawable;
    }
}
