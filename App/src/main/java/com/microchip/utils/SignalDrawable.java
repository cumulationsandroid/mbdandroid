/*
 *
 *
 *     SignalDrawable.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.utils;

import com.microchip.R;

/**
 * Created by
 */
public class SignalDrawable {
    /**
     *
     * @param strength
     * @return
     */
    public static int getStrength(Integer strength) {
        int drawableID;
        if (strength == 127) {     // value of 127 reserved for RSSI not available
            drawableID = R.drawable.signal_null;
        } else if (strength <= -84) {
            drawableID = R.drawable.signal_a;
        } else if (strength <= -72) {
            drawableID = R.drawable.signal_ab;
        } else if (strength <= -60) {
            drawableID = R.drawable.signal_abc;
        } else if (strength <= -48) {
            drawableID = R.drawable.signal_abcd;
        } else {
            drawableID = R.drawable.signal_abcde;
        }
        return drawableID;
    }
}
