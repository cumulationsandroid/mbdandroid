/*
 *
 *
 *     TextProccess.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.utils;

import android.content.Context;

import com.microchip.R;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;

import java.util.UUID;

/**
 * Created by
 */
public class TextProccess {
    /**
     *
     * @param context
     * @param uuid
     * @return
     */
    public static String getText(Context context, UUID uuid) {
        String text = "";
        if (GattAttributes.HEALTH_TEMP_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_helth_thermo_meter);
        } else if (GattAttributes.IMMEDIATE_ALERT_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_immideate_alert);
        } else if (GattAttributes.LINK_LOSS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_linkloss);
        } else if (GattAttributes.TRANSMISSION_POWER_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_transmition_power);
        } else if (GattAttributes.DEVICE_INFORMATION_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.device_info_descr);
        } else if (GattAttributes.HEART_RATE_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_heart_rate);
        } else if (GattAttributes.BLOOD_PRESSURE_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_blood_pressure);
        } else if (GattAttributes.BATTERY_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_battery);
        } else if (GattAttributes.SCAN_PARAMETERS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_scanparam);
        } else if (GattAttributes.PHONE_ALERT_STATUS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_phone_alert);
        } else if (GattAttributes.CURRENT_TIME_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_current_time);
        } else if (GattAttributes.NEXT_DST_CHANGE_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_next_dst_change);
        } else if (GattAttributes.REFERENCE_TIME_UPDATE_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_refrence_time_update);
        } else if (GattAttributes.SERIAL_PORT.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_serial_port);
        } else if (GattAttributes.SERIAL_PORT_END_POINT.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_serial_port);
        } else if (GattAttributes.ALERT_NOTIFICATION_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_ans);
        } else if (GattAttributes.GENERIC_ACCESS_SERVICE.equalsIgnoreCase(uuid.toString())) {
            text = context.getString(R.string.temp_desc_for_gatt_db);
        }
        return text;
    }
}
