/*
 *
 *
 *     CircleView.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;

import com.microchip.R;

/**
 * Created by
 */
public class CircleView extends View {
    protected float waveScale;
    protected int alpha;
    Paint paint;
    Path path;
    private int centerY;
    private int centerX;
    private int outerRadius;
    private Paint mPaint, animatedCircle;
    private float decreaseRadius = 1.5f;
    private Bitmap mBitmap;
    private Boolean isProgressSet = false;
    private int progress = 0;
    private Interpolator waveInterpolator;
    private Interpolator alphaInterpolator;
    private Animator animator;
    private AnimatorSet animatorSet;
    private long animationTime = 2000;
    private int mSafeMin = 0;
    private int mSafeMax = 0;
    private int mMidMin = 0;
    private int mMidMax = 0;
    private int mDangerMin = 0;
    private int mDangerMax = 0;

    public CircleView(Context context) {
        super(context);
        init();
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
//        context.obtainStyledAttributes(attrs, R.attr.circle_layout, defStyle, 0);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        mBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.moving_device);
        animatedCircle = new Paint();
        animatedCircle.setColor(Color.BLACK);
        animatedCircle.setStyle(Paint.Style.STROKE);
        animatedCircle.setStrokeWidth(5);
        this.alpha = 255;
        this.waveScale = 0f;
        animatedCircle.setAlpha(alpha);
        animatorSet = new AnimatorSet();
        setInterpolator();

    }

    @Override
    protected void onDraw(Canvas canvas) {
       super.onDraw(canvas);
        int pors = outerRadius / 3;

        // Main circle
        drawCircle(canvas, centerX, centerY, outerRadius, paint);
        mPaint = getPaint(Color.parseColor("#f44336"), Paint.Style.FILL);
        drawCircle(canvas, centerX, centerY, outerRadius, mPaint);

        //second circle
        drawCircle(canvas, centerX, centerY, pors * 2, paint);
        mPaint = getPaint(Color.parseColor("#ff9800"), Paint.Style.FILL);
        drawCircle(canvas, centerX, centerY, pors * 2, mPaint);
//
        // third circle
        drawCircle(canvas, centerX, centerY, pors, paint);
        mPaint = getPaint(Color.parseColor("#8bc34a"), Paint.Style.FILL);
        drawCircle(canvas, centerX, centerY, pors, mPaint);
//
//        // animation circle
        // mPaint = getPaint(Color.parseColor("#000000"), Paint.Style.STROKE);
        // mPaint.setStrokeWidth(5);
//        if (!isProgressSet) {
//            decreaseRadius = outerRadius;
//        }
//
//        // Draw images
//        int height = mBitmap.getHeight() / 2;
////        int width = mBitmap.getWidth() / 2;
//
//        if (decreaseRadius != 0) {
//            canvas.drawCircle(centerX, centerY, decreaseRadius, animatedCircle);
//        } else {
//            canvas.drawCircle(centerX, centerY, height, animatedCircle);
//        }
//        canvas.drawBitmap(mBitmap, centerX, ((centerY) - decreaseRadius) - height, mPaint);
    }

    private Paint getPaint(int i, Paint.Style style) {
        Paint paint = new Paint();
        paint.setColor(i);
        paint.setStyle(style);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        return paint;
    }

    private synchronized void drawCircle(Canvas canvas, int centerX, int centerY, int outerRadius, Paint paint) {
        canvas.drawCircle(centerX, centerY, outerRadius, paint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        centerX = w / 2;
        centerY = h / 2;
        double v = ((w > h) ? h : w) / 2.5;
        outerRadius = (int) v;
        paint.setStrokeWidth(outerRadius / 20);


        //setProgress(progress);
    }

    /**
     * Set Progress
     *
     * @param progress
     */
    public synchronized void setProgress(int progress) {
        //  invalidate();
        this.progress = progress;
        int region;
        int pors = outerRadius / 3;

        isProgressSet = true;

        /*

        if (progress < 30) {
            region = 0;
        } else if (progress >= 30 && progress <= 80) {
            region = pors / 50;
            int i = Math.abs(30 - progress);
            region = region * i;
        } else if (progress >= 80 && progress <= 95) {
            region = ((pors * 2) - pors) / 15;
            int i = Math.abs(80 - progress);
            region = (region * i) + pors;
        } else if (progress >= 95 && progress <= 120) {
            region = (outerRadius - (pors * 2)) / 25;
            int i = Math.abs(95 - progress);
            region = (region * i) + (pors * 2);
        } else {
            region = outerRadius;
        }
        */

        if (progress < mSafeMin) {
            region = 0;
        } else if (progress >= mSafeMin && progress <= mSafeMax) {
            region = pors / (mSafeMax - mSafeMin);
            int i = Math.abs(mSafeMin - progress);
            region = region * i;
        } else if (progress >= mMidMin && progress <= mMidMax) {
            region = ((pors * 2) - pors) / (mMidMax - mMidMin);
            int i = Math.abs(mMidMin - progress);
            region = (region * i) + pors;
        } else if (progress >= mDangerMin && progress <= mDangerMax) {
            region = (outerRadius - (pors * 2)) / (mDangerMax - mDangerMin);
            int i = Math.abs(mDangerMin - progress);
            region = (region * i) + (pors * 2);
        } else {
            region = outerRadius;
        }

        decreaseRadius = region;
        startAnimation();
        postInvalidateOnAnimation();
    }


    public void setRange(int safeMin, int safeMax, int midMin, int midMax, int dangerMin, int dangerMax) {
        mSafeMin = safeMin;
        mSafeMax = safeMax;
        mMidMin = midMin;
        mMidMax = midMax;
        mDangerMin = dangerMin;
        mDangerMax = dangerMax;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
        invalidate();
    }

    protected float getWaveScale() {
        return waveScale;
    }

    protected void setWaveScale(float waveScale) {
        this.waveScale = waveScale;
        invalidate();
    }

    private Animator generateAnimation() {

        //Wave animation
        ObjectAnimator waveAnimator = ObjectAnimator.ofFloat(this, "waveScale", 0f, 1f);
        waveAnimator.setDuration(animationTime);
        if (waveInterpolator != null) {
            waveAnimator.setInterpolator(waveInterpolator);
        }
        //The animation is repeated
        waveAnimator.setRepeatCount(Animation.INFINITE);
        waveAnimator.setRepeatMode(Animation.INFINITE);

        //alpha animation
        ObjectAnimator alphaAnimator = ObjectAnimator.ofInt(this, "alpha", 255, 0);
        alphaAnimator.setDuration(animationTime);
        if (alphaInterpolator != null) {
            alphaAnimator.setInterpolator(alphaInterpolator);
        }
        alphaAnimator.setRepeatCount(Animation.INFINITE);
        alphaAnimator.setRepeatMode(Animation.INFINITE);

        animatorSet.playTogether(waveAnimator, alphaAnimator);

        return animatorSet;
    }

    public void setInterpolator() {
        this.waveInterpolator = new LinearOutSlowInInterpolator();
    }

    public void startAnimation() {
        animator = generateAnimation();
        animator.start();
    }

    public void stopAnimation() {
        if (animator.isRunning()) {
            animator.end();
        }
    }
}
