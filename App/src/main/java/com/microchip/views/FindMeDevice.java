/*
 *
 *
 *     FindMeDevice.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.microchip.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;

import com.microchip.R;

/**
 * Created by
 */
public class FindMeDevice extends View {
    protected float waveScale;
    protected int alpha;
    Paint paint;
    Path path;
    private int centerY;
    private int centerX;
    private int outerRadius;
    private float increaseRadius = 1.5f;
    private Bitmap mBitmap;
    private Boolean isProgressSet = false;
    private Interpolator waveInterpolator;
    private Interpolator alphaInterpolator;
    private Animator animator;
    private AnimatorSet animatorSet;
    private long animationTime = 2000;
    private int progress = 0;
    private int mSafeMin = 0;
    private int mDangerMax = 0;

    public FindMeDevice(Context context) {
        super(context);
        init();
    }

    public FindMeDevice(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FindMeDevice(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
//        context.obtainStyledAttributes(attrs, R.attr.circle_layout, defStyle, 0);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        mBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.moving_device);
        this.alpha = 255;
        this.waveScale = 0f;
        animatorSet = new AnimatorSet();
        setInterpolator();

    }

    public void setRange(int safeMin, int dangerMax) {
        mSafeMin = safeMin;
        mDangerMax = dangerMax;
    }

    public void setmBitmap(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
       super.onDraw(canvas);
       // int pors = outerRadius / 3;
        if (!isProgressSet) {
            increaseRadius = outerRadius;
            //  Log.e("increaseRadius isPro",""+increaseRadius);
        }
        // Draw images
        int width = mBitmap.getWidth() / 2;
        int height = mBitmap.getHeight() / 2;
        if (increaseRadius == 0) {
            increaseRadius = 30;
        }
        //  Log.e("increaseRadius onDraw",""+increaseRadius);
        drawBitmap(canvas, ((centerX + 30) + increaseRadius) - width, centerY - height, mBitmap, paint);
    }

    private synchronized void drawBitmap(Canvas canvas, float centerX, float centerY, Bitmap bitmap, Paint paint) {
        canvas.drawBitmap(bitmap, centerX, centerY, paint);
        // canvas.drawCircle(centerX, centerY, outerRadius, paint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        centerX = w / 2;
        centerY = h / 2;
        double v = ((w > h) ? h : w) / 2.5;
        outerRadius = (int) v;
        paint.setStrokeWidth(outerRadius / 20);


        //setProgress(progress);
    }


    public void setAlpha(int alpha) {
        this.alpha = alpha;
        invalidate();
    }

    public void setInterpolator() {
        this.waveInterpolator = new LinearOutSlowInInterpolator();
    }

    /**
     * Set Progress
     *
     * @param progress
     */
    public synchronized void setProgress(int progress) {
        //  invalidate();
        this.progress = progress;
        int region ;
        int pors = outerRadius / 3;

        isProgressSet = true;


        if (progress < mSafeMin) {
            region = 0;
        } else if (progress >= mSafeMin && progress < 50) {
            region = pors / (50 - mSafeMin);
            int i = Math.abs(mSafeMin - progress);
            region = region * i;
//            region = pors / (50-mSafeMin);
//            int i = Math.abs(mSafeMin - progress);
//            region = region * i;
        } else if (progress >= 50 && progress < 80) {

            region = ((pors * 2) - pors) / (80 - 50);
            int i = Math.abs(50 - progress);
            region = (region * i) + pors;
//            region = pors / (80-50);
//            int i = Math.abs(mSafeMin - progress);
//            region = region * i;
        } else if (progress >= 80 && progress < mDangerMax) {
            region = (outerRadius - (pors * 2)) / (mDangerMax - 80);
            int i = Math.abs(80 - progress);
            region = (region * i) + (pors * 2);
//            region = pors / (mDangerMax-80);
//            int i = Math.abs(mSafeMin - progress);
//            region = region * i;
        } else {
            region = outerRadius;
        }
        increaseRadius = region;
        postInvalidateOnAnimation();

    }

    public void startAnimation() {
        animator = generateAnimation();
        animator.start();
    }

    public void stopAnimation() {
        if (animator.isRunning()) {
            animator.end();
        }
    }

    private Animator generateAnimation() {

        //Wave animation
        ObjectAnimator waveAnimator = ObjectAnimator.ofFloat(this, "waveScale", 0f, 1f);
        waveAnimator.setDuration(animationTime);
        if (waveInterpolator != null) {
            waveAnimator.setInterpolator(waveInterpolator);
        }
        //The animation is repeated
        waveAnimator.setRepeatCount(Animation.INFINITE);
        waveAnimator.setRepeatMode(Animation.INFINITE);

        //alpha animation
        ObjectAnimator alphaAnimator = ObjectAnimator.ofInt(this, "alpha", 255, 0);
        alphaAnimator.setDuration(animationTime);
        if (alphaInterpolator != null) {
            alphaAnimator.setInterpolator(alphaInterpolator);
        }
        alphaAnimator.setRepeatCount(Animation.INFINITE);
        alphaAnimator.setRepeatMode(Animation.INFINITE);

        animatorSet.playTogether(waveAnimator, alphaAnimator);

        return animatorSet;
    }

}
