package com.wifiprovision.microchip.wifiprovisioner;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.wifiprovision.microchip.wifiprovisioner.AccessPointScanData;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class BLEManager extends Service {
    // --------------------------------------------------------------------------------------------
    // -- INNER CLASSES
    // --------------------------------------------------------------------------------------------
    public class LocalBinder extends Binder {
        BLEManager  getService() { return BLEManager.this; }
    }

    // --------------------------------------------------------------------------------------------
    // -- Private BLEManager members
    // --------------------------------------------------------------------------------------------
    private static final String TAG                 = BLEManager.class.getSimpleName();
    private final IBinder mBinder                   = new LocalBinder();
    private static final int    STATE_DISCONNECTED  = 0;
    private static final int    STATE_CONNECTING    = 1;
    private static final int    STATE_CONNECTED     = 2;

    private BluetoothManager    mBluetoothManager;
    private BluetoothAdapter    mBluetoothAdapter;
    private BluetoothGatt       mBluetoothGatt;
    private String              mBluetoothDeviceAddress;
    private int                 mConnectionState;   // TODO - this doesn't seem to be needed!

    // --------------------------------------------------------------------------------------------
    // -- Create callback methods for the GATT events our app is interested in
    // -- TODO - right now this is almost an exhaustive list - later we'll prune to what we need
    // --------------------------------------------------------------------------------------------
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            String intentAction;

            Log.i(TAG, "onConnectionStateChange(): status=" + status + " newState=" + newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "onConnectionStateChange(): Connected to GATT server.");
                intentAction        = ACTION_GATT_CONNECTED;
                mConnectionState    = STATE_CONNECTED;
            } else {
                Log.i(TAG, "onConnectionStateChange(): Disconnected from GATT server.");
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
            }
            broadcastUpdate(intentAction, status);

            if (mConnectionState == STATE_CONNECTED) {
                Log.i(TAG, "Attempting to start service discovery: " +
                        mBluetoothGatt.discoverServices());
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERV_DISC);
            } else {
                Log.v(TAG, "onServicesDiscovered(): received bad status: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
//            super.onCharacteristicRead(gatt, characteristic, status);

            Log.d(TAG, "onCharacteristicRead(): fired.." + status);

            // TODO - wrap with success check on status for "BluetoothGatt.GATT_SUCCESS"
            broadcastUpdate(ACTION_GATT_DATA_READY, characteristic);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

            // TODO - Implement me...
            Log.i(TAG, "OnCharacteristicChanged(): " + characteristic.getUuid().toString());
            if (characteristic.getUuid().equals(UUID.fromString("fb8c0002-d224-11e4-85a1-0002a5d5c51b"))) {
                if (characteristic.getValue()[0] == 1) { //scanning started
                    String intentAction = ACTION_SCANLIST_SCANNING;
                    broadcastUpdate(intentAction);
                }
                else if (characteristic.getValue()[0] == 2) { //scanning complete
                    String intentAction = ACTION_SCANLIST_UPDATED;
                    broadcastUpdate(intentAction);
                }
            }
            else if (characteristic.getUuid().equals((UUID.fromString("77880002-d229-11e4-8689-0002a5d5c51b")))) {
                if (characteristic.getValue()[0] == 2) { //Connected
                    String intentAction = ACTION_WIFI_CONNECT_SUCCESS;
                    broadcastUpdate(intentAction);
                }
                else if (characteristic.getValue()[0] == 1) { //Connecting
                    String intentAction = ACTION_WIFI_CONNECT_INPROGRESS;
                    broadcastUpdate(intentAction);
                }
                else if (characteristic.getValue()[0] == 0) { //Disconnected
                    String intentAction = ACTION_WIFI_CONNECT_FAIL;
                    broadcastUpdate(intentAction);
                }
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);

            // TODO - Implement me?
            Log.i(TAG, "OnDescriptorWrite()//....");
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);

            Log.i(TAG, "OnCharacteristicWrite()...." + status);
            Log.d(TAG, "gatt = " + gatt.toString());
            if (characteristic.getUuid().equals(UUID.fromString("77880003-d229-11e4-8689-0002a5d5c51b"))) {
                //Provisioning data has been written to the device.
                String intentAction = ACTION_PROVISIONING_DONE;
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);

            // TODO - Implement me?
        }
    };

    // --------------------------------------------------------------------------------------------
    // -- PRIVATE METHODS
    // --------------------------------------------------------------------------------------------
    private void broadcastUpdate(final String action, final int status) {
        final Intent    intent = new Intent(action);
        if (status != -1) {
            intent.putExtra("status", status);
        }
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action) {
        broadcastUpdate(action, -1);
    }

    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic ch) {
        final Intent    intent = new Intent(action);
        final byte[]    data   = ch.getValue();

        // TODO - this is just some debug code...
        if (ch.getUuid().toString().equals("fb8c0100-d224-11e4-85a1-0002a5d5c51b")) {

            if (data != null) {
                AccessPointScanData ap = new AccessPointScanData(data);

                Log.i(TAG, "created an AP object - " + ap);
                intent.putExtra(AP_DATA, ap);
            } else {
                Log.e(TAG, "BluetoothGattCharacteristic has NULL value!");
                return;
            }
        }
        else if (ch.getUuid().toString().equals("fb8c0003-d224-11e4-85a1-0002a5d5c51b")) {
            if (data != null) {
                byte apCount = data[0];
                intent.putExtra(AP_COUNT, apCount);
            }
        }
        sendBroadcast(intent);
    }

    // --------------------------------------------------------------------------------------------
    // -- PUBLIC MEMBERS
    // --------------------------------------------------------------------------------------------
    // TODO - need to create some public strings for broadcasting events such as
    //        "GATT CONNECTED", "DATA AVAILABLE", "DATA WRITTEN"
    public static final String ACTION_GATT_CONNECTED    = "com.atmel.bleprov.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "com.atmel.bleprov.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_SERV_DISC    = "com.atmel.bleprov.ACTION_GATT_SERV_DISC";
    public static final String AP_DATA    = "com.atmel.bleprov.AP_DATA";
    public static final String AP_COUNT    = "com.atmel.bleprov.AP_COUNT";
    public static final String ACTION_GATT_DATA_READY   = "com.atmel.bleprov.ACTION_GATT_DATA_READY";
    public static final String ACTION_PROVISIONING_DONE = "com.atmel.bleprov.ACTION_PROVISIONING_DONE";
    public static final String ACTION_SCANLIST_UPDATED = "com.atmel.bleprov.ACTION_SCANLIST_UPDATED";
    public static final String ACTION_SCANLIST_SCANNING = "com.atmel.bleprov.ACTION_SCANLIST_SCANNING";
    public static final String ACTION_WIFI_CONNECT_SUCCESS = "com.atmel.bleprov.ACTION_WIFI_CONNECT_SUCCESS";
    public static final String ACTION_WIFI_CONNECT_FAIL = "com.atmel.bleprov.ACTION_WIFI_CONNECT_FAIL";
    public static final String ACTION_WIFI_CONNECT_INPROGRESS = "com.atmel.bleprov.ACTION_WIFI_CONNECT_INPROGRESS";

    public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";



    // --------------------------------------------------------------------------------------------
    // -- PUBLIC METHODS
    // --------------------------------------------------------------------------------------------
    @Override
    public IBinder onBind(Intent intent)    { return mBinder; }

    @Override
    public boolean onUnbind(Intent intent) {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }
        return super.onUnbind(intent);
    }

    public boolean initialise() {
        // Get a reference to BluetoothAdapter through the BluetoothManager.
        if (mBluetoothManager ==  null) {
            mBluetoothManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialise BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    public boolean connect(final String addr) {
        if (mBluetoothAdapter == null || addr == null) {
            Log.v(TAG, "BluetoothAdapter not initialised or address not specified.");
            return false;
        }

        // Check for currently connected device... try to reconnect
        if (mBluetoothDeviceAddress != null && addr.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }
        if (BluetoothAdapter.checkBluetoothAddress(addr)) {
            final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(addr);
            mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
            refreshDeviceCache(mBluetoothGatt);
            Log.d(TAG, "Trying to create a new connection.");
            mBluetoothDeviceAddress = addr;
            mConnectionState        = STATE_CONNECTING;
            return true;
        }
        else {
            Log.v(TAG, "connect(): Invalid device address " + addr);
            return false;
        }
    }

    private boolean refreshDeviceCache(BluetoothGatt gatt) {
        try {
            BluetoothGatt localgatt = gatt;
            Method localMethod = localgatt.getClass().getMethod("refresh");
            if (localMethod != null) {
                return ((Boolean)localMethod.invoke(localgatt));
            }
        }
        catch (Exception localException) {
            Log.e(TAG, "An exception occurred while refreshing device");
        }
        return false;
    }

    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.v(TAG, "disconnected(): BluetoothAdapter not initialised.");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public void readCharacteristic(BluetoothGattCharacteristic ch) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.v(TAG, "readCharacteristic(): BluetoothAdapter not enabled.");
            return;
        }
        mBluetoothGatt.readCharacteristic(ch);
    }

    public void writeCharacteristicBytes(BluetoothGattCharacteristic ch, byte[] rawdata) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.v(TAG, "writeCharacteristic(): BluetoothAdapter not initialised.");
            return;
        }
        Log.i(TAG, "Calling writeCharacteristic...");
        Log.d(TAG, "mBluetoothGatt = " + mBluetoothGatt.toString());
        Log.i(TAG, "ch.setValue(rawdata): rawdata.length" + rawdata.length + " val: " + Arrays.toString(rawdata));
        ch.setValue(rawdata);
        mBluetoothGatt.writeCharacteristic(ch);
    }

    public boolean setCharacteristicNotification(BluetoothGattCharacteristic ch, boolean ena) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.v(TAG, "setCharacteristicNotification(): BluetoothAdapter not enabled.");
            return false;
        }
        Log.i(TAG, "Calling setCharacteristicNotification...");
        if (mBluetoothGatt.setCharacteristicNotification(ch, ena)) {
            BluetoothGattDescriptor descriptor = ch.getDescriptor(UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
            descriptor.setValue((ena)?BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE:
                    BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            return mBluetoothGatt.writeDescriptor(descriptor);
        }
        return false;
    }

    public boolean isBonded() {
        if (mBluetoothAdapter == null) {
            return false;
        }
        Log.i(TAG, "****** " + mBluetoothGatt.getDevice().getAddress());
        for (BluetoothDevice device : mBluetoothAdapter.getBondedDevices()) {
            Log.i(TAG, "==== " + device.getAddress() + " ==== " + mBluetoothGatt.getDevice().getAddress());
            if (device.getAddress().equals(mBluetoothGatt.getDevice().getAddress())) {
                return true;
            }
        }
        return false;
    }
}


