package com.wifiprovision.microchip.wifiprovisioner;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class MainActivity extends ListActivity {

    // --------------------------------------------------------------------------------------------
    // -- Private BleService members
    // --------------------------------------------------------------------------------------------
    private static final String TAG = "MainActivity";
    private static final boolean DEBUG = false;
    private static final long SCAN_PERIOD = 10000;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private final int PROVISIONING_RESULT = 0;
    private boolean mScanning;
    private Context mContext;
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;
    private Toast mToast;
    private final BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
            if (parseUuids(scanRecord).contains(UUID.fromString("77880001-d229-11e4-8689-0002a5d5c51b"))) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mLeDeviceListAdapter.isEmpty())
                            mToast.cancel();
                        mLeDeviceListAdapter.addDevice(device);
                        mLeDeviceListAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    };
    private boolean mScanOnResume;
    private int REQUEST_ENABLE_BT = 1;

    // --------------------------------------------------------------------------------------------
    // -- PUBLIC METHODS
    // --------------------------------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "MainActivity:onCreate\n");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect beacons.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION
                        }, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
        }

        mContext = this;
//        getActionBar().setTitle(R.string.blescantitle);
        mHandler = new Handler();
        mToast = Toast.makeText(getApplicationContext(), R.string.warning_provisioning, Toast.LENGTH_LONG);
        mScanOnResume = true;

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.warning_ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        final BluetoothManager bm = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bm.getAdapter();

       /* if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.warning_ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Toast.makeText(this, R.string.warning_ble_not_enabled, Toast.LENGTH_SHORT).show();
            finish();
        }*/
    }

    @Override
    public void onResume() {
        Log.i(TAG, "OnResume\n");
        super.onResume();

        // TODO - ensure BLE is enabled on the device. If not, fire an intent to display a
        //        dialog asking the user to grant permission to enable it. Will need to
        //        prove an onActivityResult implementation to catch if the user elects not
        //        to grant permission (in which case we will terminate the app).

        // Set the ListAdapater
        if (mLeDeviceListAdapter == null) {
            mLeDeviceListAdapter = new LeDeviceListAdapter();
        }
        setListAdapter(mLeDeviceListAdapter);

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }


        // Do scan every when this activity opened!
        if (mScanOnResume) {
            if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
                scanLeDevice(true);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "OnPause\n");
        mScanOnResume = mScanning;
        scanLeDevice(false);
    }

    // Called to create the options menu (top right of screen) as per a given layout
    // (in this case res/menu/options.xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "MainActivity:onCreateOptionsMenu\n");

        // Inflate the xml resource
        getMenuInflater().inflate(R.menu.mainmenu, menu);

        // Determine, based on the current scanning activity, which menu
        // item to make visible and which to make invisible. Also set the
        // state of the menu item declared for indicating progress (i.e. spinner
        // while scanning is in progress).
        if (!mScanning) {
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_progress_meter).setActionView(null);
            mToast.cancel();
        } else {
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_progress_meter).setActionView(R.layout.actionbar_ind_progress_spinner);
            mToast.show();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "MainActivity:onOptionsItemSelected\n");

        int id = item.getItemId();
        if(id == R.id.menu_scan){
            mLeDeviceListAdapter.clear();
            scanLeDevice(true);
        }else if(id == R.id.menu_stop){
            scanLeDevice(false);
        }
                /*
        switch (item.getItemId()) {
            case R.id.menu_scan:
                mLeDeviceListAdapter.clear();
                scanLeDevice(true);
                break;

            case R.id.menu_stop:
                scanLeDevice(false);
                break;
        }*/
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int pos, long id) {
        Log.i(TAG, "onListItemClick(): selected " + id);
        final BluetoothDevice dev = mLeDeviceListAdapter.getDevice(pos);
        if (dev == null)
            return;

        if (mScanning) {
            scanLeDevice(false);
        }

        try {
            Method m = dev.getClass().getMethod("removeBond", (Class[]) null);
            m.invoke(dev, (Object[]) null);
        } catch (NoSuchMethodException e) {
            Log.e(TAG, e.getMessage());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        mScanOnResume = false;
        final Intent intent = new Intent(this, ProvisioningActivity.class);
        intent.putExtra(ProvisioningActivity.EXTRAS_DEV_NAME, dev.getName());
        intent.putExtra(ProvisioningActivity.EXTRAS_DEV_ADDR, dev.getAddress());
        //startActivity(intent);
        startActivityForResult(intent, PROVISIONING_RESULT);

        //Clear the BLE device list now so if we come back here it doesn't contain outdated results
        mLeDeviceListAdapter.clear();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult(): requestCode:" + requestCode + " resultCode:" + resultCode);
        if (requestCode == PROVISIONING_RESULT) {
            mScanOnResume = (resultCode == Activity.RESULT_OK);
        }
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
                return;
            }
            if (mScanOnResume) {
                if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
                    scanLeDevice(true);
                }
            }

        }
    }

    private List<UUID> parseUuids(byte[] advertisedData) {
        List<UUID> uuids = new ArrayList<UUID>();

        ByteBuffer buffer = ByteBuffer.wrap(advertisedData).order(ByteOrder.LITTLE_ENDIAN);
        while (buffer.remaining() > 2) {
            byte length = buffer.get();
            if (length == 0) break;

            byte type = buffer.get();
            switch (type) {
                case 0x02: // Partial list of 16-bit UUIDs
                case 0x03: // Complete list of 16-bit UUIDs
                    while (length >= 2) {
                        buffer.getShort(); //move along the buffer
                        length -= 2;
                    }
                    break;

                case 0x06: // Partial list of 128-bit UUIDs
                case 0x07: // Complete list of 128-bit UUIDs
                    while (length >= 16) {
                        long lsb = buffer.getLong();
                        long msb = buffer.getLong();
                        uuids.add(new UUID(msb, lsb));
                        length -= 16;
                    }
                    break;

                default:
                    buffer.position(buffer.position() + length - 1);
                    break;
            }
        }

        return uuids;
    }

    private void scanLeDevice(final boolean enabled) {
        if (enabled) {

            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);

            mLeDeviceListAdapter.clear();
            mScanning = true;

            //TODO startLEScan is deprecated, move to new API
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            //Filtering doesn't work due to an android bug, so we have to do it manually for now
//            UUID[] uuid = new UUID[1];
//            uuid[0] = UUID.fromString("fb8c0001-d224-11e4-85a1-0002a5d5c51b");
//            mBluetoothAdapter.startLeScan(uuid, mLeScanCallback);

        } else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mScanning = false;
        }
        invalidateOptionsMenu();
    }

    // --------------------------------------------------------------------------------------------
    // -- INNER CLASSES
    // --------------------------------------------------------------------------------------------
    static class ViewHolder {
        TextView devName;
        TextView devAddr;
        TextView devRssi;
    }

    private class LeDeviceListAdapter extends BaseAdapter {
        private final ArrayList<BluetoothDevice> mLeDevices;
        private final LayoutInflater mInflater;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflater = ((Activity) mContext).getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View v, ViewGroup vg) {
            ViewHolder viewHolder; // allows us to store view objects to avoid findViewById calls if a view is recycled

            if (v == null) {
                // There are no views that can be recycled, we'll have to create one...
                v = mInflater.inflate(R.layout.ble_device_listitem, vg, false);

                viewHolder = new ViewHolder();
                viewHolder.devName = (TextView) v.findViewById(R.id.bldevlst_name);
                viewHolder.devAddr = (TextView) v.findViewById(R.id.bledevlst_addr);
                viewHolder.devRssi = (TextView) v.findViewById(R.id.bledevlst_rssi);

                // Assign this to the view - if it is recycled we can recover via getTag
                v.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) v.getTag();
            }

            // OK, let's muck around...
            BluetoothDevice dev = mLeDevices.get(i);
            final String devName = dev.getName();
            if (devName != null && devName.length() > 0) {
                viewHolder.devName.setText(devName);
            } else {
                viewHolder.devName.setText(R.string.devunknwn);
            }
            viewHolder.devAddr.setText(dev.getAddress());

            return v;
        }

        public void addDevice(BluetoothDevice dev) {
            if (!mLeDevices.contains(dev)) {
                mLeDevices.add(dev);
                Log.i(TAG, "Adding dev " + dev);
            }
        }

        public BluetoothDevice getDevice(int pos) {
            return mLeDevices.get(pos);
        }

        public void clear() {
            mLeDevices.clear();
            mLeDeviceListAdapter.notifyDataSetChanged();
        }
    }
}
