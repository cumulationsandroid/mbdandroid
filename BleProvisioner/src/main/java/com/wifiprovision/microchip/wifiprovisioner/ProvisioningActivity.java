package com.wifiprovision.microchip.wifiprovisioner;

import android.app.Activity;
import android.app.AlertDialog;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class ProvisioningActivity extends Activity {

    // --------------------------------------------------------------------------------------------
    // -- INNER CLASSES
    // --------------------------------------------------------------------------------------------
    static class ViewHolder {
        ImageView security;
        TextView ssid;
    }

    private class ApListAdapter extends BaseAdapter {
        private final ArrayList<AccessPointScanData> mAccessPoints;
        private final LayoutInflater mInflater;

        public ApListAdapter() {
            super();
            mAccessPoints = new ArrayList<AccessPointScanData>();
            mInflater = ((Activity) mContext).getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mAccessPoints.size();
        }

        @Override
        public Object getItem(int i) {
            return mAccessPoints.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View v, ViewGroup vg) {
            ViewHolder viewHolder; // allows us to store view objects to avoid findViewById calls if a view is recycled

            if (v == null) {
                // There are no views that can be recycled, we'll have to create one...
                v = mInflater.inflate(R.layout.access_point_listitem, vg, false);

                viewHolder = new ViewHolder();
                viewHolder.security = (ImageView) v.findViewById(R.id.aplist_secure);
                viewHolder.ssid = (TextView) v.findViewById(R.id.aplist_ssid);

                // Assign this to the view - if it is recycled we can recover via getTag
                v.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) v.getTag();
            }

            // OK, let's muck around...
            AccessPointScanData ap = mAccessPoints.get(i);
            final String apSsid = ap.getSsid();
            final int apSecurity = ap.getSecurity();
            final int apRssi = ap.getRssi();

            viewHolder.ssid.setText(apSsid);
            if (apSecurity != 1)    // 1 = open, 2=wpa, 3=wep
                viewHolder.security.setImageResource(android.R.drawable.ic_secure);
            else
                viewHolder.security.setImageResource(android.R.drawable.ic_partial_secure);

            return v;
        }

        public void addDevice(AccessPointScanData ap) {
            if (!mAccessPoints.contains(ap)) {
                mAccessPoints.add(ap);
                mApListAdapter.notifyDataSetChanged();
            }
        }

        public AccessPointScanData getDevice(int pos) {
            return mAccessPoints.get(pos);
        }

        public void clear() {
            mAccessPoints.clear();
            mApListAdapter.notifyDataSetChanged();
        }
    }

    // --------------------------------------------------------------------------------------------
    // -- Private ProvisioningActivity members
    // --------------------------------------------------------------------------------------------
    private static final String TAG = ProvisioningActivity.class.getSimpleName();
    private Context mContext;
    private BLEManager mBleService;
    private String mDeviceAddress;
    private String mDeviceName;
    private ApListAdapter mApListAdapter;
    private EditText mSsidTextEntry;
    private EditText mKeyTextEntry;
    private Button mProvisionButton;
    private Button mScanButton;
    private Spinner mSpinner;
    private CheckBox mCheckbox;
    private ArrayAdapter<String> mSecurityAdapter;

    private final ArrayList<BluetoothGattCharacteristic> mScannedApArray = new ArrayList<>();
    private int mApIndex;
    private int mApCount; //Number of valid APs in scanlist

    private BluetoothGattService mBleScanGattService;
    private BluetoothGattCharacteristic mApProvCharacteristic;
    private BluetoothGattCharacteristic mApCountChar;

    // --- Keys for sending BLE device name and address to this activity (Intent EXTRAs) ---
    public static final String EXTRAS_DEV_NAME = "DEV_NAME";
    public static final String EXTRAS_DEV_ADDR = "DEV_ADDR";


    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBleService = ((BLEManager.LocalBinder) service).getService();
            if (!mBleService.initialise()) {
                Log.e(TAG, "onServiceConnected(): Unable to initialise Bluetooth");
                requestBleScan();
                finish();
            }
            Log.d(TAG, "onServiceConnected(): OK. Attempting to connect to dev " + mDeviceAddress);
            mBleService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBleService = null;
        }
    };

    private void DiscoverServices() {
        List<BluetoothGattService> serviceList = mBleService.getSupportedGattServices();

        // Iterate through the services for now...
        for (BluetoothGattService service : serviceList) {
            List<BluetoothGattCharacteristic> gattCharacteristics = service.getCharacteristics();


            if (service.getUuid().toString().equals("fb8c0001-d224-11e4-85a1-0002a5d5c51b")) {
                Log.i(TAG, "AP ScanStatus characteristic found..");
                mBleScanGattService = service;
            }
            Log.i(TAG, "onReceive(): service found: " + service.getUuid());
            Log.i(TAG, "onReceive(): with " + gattCharacteristics.size() + " characteristics");

            for (BluetoothGattCharacteristic bgc : gattCharacteristics) {
                Log.d(TAG, "characteristic found: " + bgc.getUuid().toString());
                if (bgc.getUuid().toString().equals("fb8c0100-d224-11e4-85a1-0002a5d5c51b")) {
                    // TODO - add this to the mAccessPoints list
                    Log.i(TAG, "AP ScanInfo characteristic found..");
                    mScannedApArray.add(bgc);
                }
                if (bgc.getUuid().toString().equals("fb8c0002-d224-11e4-85a1-0002a5d5c51b")) {
                    //TODO stop doing this everytime we re-discover - only need to do it on ble connection
                    mBleService.setCharacteristicNotification(bgc, true);
                }
                if (bgc.getUuid().toString().equals("fb8c0003-d224-11e4-85a1-0002a5d5c51b")) {
                    mApCountChar = bgc;
                }
                if (bgc.getUuid().toString().equals("77880002-d229-11e4-8689-0002a5d5c51b")) {
                    mBleService.setCharacteristicNotification(bgc, true);
                }

                if (bgc.getUuid().toString().equals("77880003-d229-11e4-8689-0002a5d5c51b")) {
                    Log.i(TAG, "AP Cred characteristic found..");
                    mApProvCharacteristic = bgc;
                }
            }
        }

        // Let's iterate through all of the characteristics and sort them somehow
      //  mScanButton.setEnabled(true);
        invalidateOptionsMenu();
        // kick off a read of all of the AP details characteristics..
        if (mScannedApArray.size() > 0) {
            mApIndex = 0;
            Log.i(TAG, "kicking off read of first Scanned AP characteristic: ");
            if (mScannedApArray.get(mApIndex) == null) {
                Log.i(TAG, "!NULL!");
            }
            else {
                //TODO - WHY is this delay needed?
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        mBleService.readCharacteristic(mApCountChar);
                    }
                }, 2000);

            }
        } else {
            Log.e(TAG, "HAVEN'T FOUND ANY ACCESS POINTS....");
        }
    }


    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            //Log.i(TAG, "mGattUpdateReceiver.onReceive: " + action);
            if (BLEManager.ACTION_GATT_CONNECTED.equals(action)) {
                Log.i(TAG, "GATT CONNECTED");
                // Already connected, but show message as "connecting device"
                Toast.makeText(context, R.string.message_connecting_device, Toast.LENGTH_SHORT).show();

            } else if (BLEManager.ACTION_GATT_DISCONNECTED.equals(action)) {
                int status = intent.getIntExtra("status", 0);
                Log.i(TAG, "GATT DISCONNECT! " + status);

                if ((status == 0x3e) || (status == 0x85) || (status == 0x08)) { //failed to establish connection, GATT_ERROR, GATT_INSUF_AUTHORIZATION
                    // Recoverable status, try again
                    Log.i(TAG, "TRYING TO RECONNECT");
                    mBleService.connect(mDeviceAddress);
                    //Disconnect with GATT_SUCCESS (0) is received when we disconnect in our onPause(). Swallow it.
                } else if (status != 0) {
                    // Back to BleDeviceScan.
                    Log.i(TAG, "PEER DISCONNECT");
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(R.string.bledisconnected);
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestBleScan();
                            finish();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } else if (BLEManager.ACTION_GATT_SERV_DISC.equals(action)) {
                Log.i(TAG, "onReceive(): Services have been discovered");

                DiscoverServices();
            } else if (BLEManager.ACTION_GATT_DATA_READY.equals(action)) {
                //A characteristicRead has completed
                //Is this an Access Point characteristic read?
                AccessPointScanData ap = intent.getParcelableExtra(BLEManager.AP_DATA);
                if (ap != null) {
                    mApListAdapter.addDevice(ap);
                    Log.i(TAG, ">> data for index " + mApIndex + " of " + mScannedApArray.size() + " is " + ap);

                    mApIndex++;
                    if (mApIndex < mApCount) {
                        mBleService.readCharacteristic(mScannedApArray.get(mApIndex));
                    } else {
                        mScanButton.setEnabled(true);
                        invalidateOptionsMenu();
                    }
                }
                else {
                    //No, is it an access point count read?
                    mApCount = intent.getByteExtra(BLEManager.AP_COUNT, (byte) 0);
                    if (mApCount > 0) {
                        //Kick off a read of the full AP characteristic list
                        mBleService.readCharacteristic(mScannedApArray.get(mApIndex));
                    }
                    else {
                        mScanButton.setEnabled(true);
                    }
                }
            } else if (BLEManager.ACTION_PROVISIONING_DONE.equals(action)) {
                Toast.makeText(context, R.string.message_press_button, Toast.LENGTH_LONG).show();
            }
            else if (BLEManager.ACTION_SCANLIST_UPDATED.equals(action)) {
                DiscoverServices();
            }
            else if (BLEManager.ACTION_SCANLIST_SCANNING.equals(action)) {
                mScannedApArray.clear();
                mApListAdapter.clear();
                Toast.makeText(context, R.string.message_scanning_ap, Toast.LENGTH_LONG).show();
            }
            else if (BLEManager.ACTION_WIFI_CONNECT_SUCCESS.equals(action)) {
                Log.i(TAG, "PROVISIONING COMPLETE.................");
                //WORKAROUND - Disconnect the BLE connection now, in case the user leaves this dialog up and
                //tries to reprovision.
                if (mBleService != null) {
                    mBleService.disconnect();
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(R.string.message_provisioning_complete);
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            } else if (BLEManager.ACTION_WIFI_CONNECT_INPROGRESS.equals(action)) {
                Toast.makeText(context, R.string.message_connecting_to_wifi, Toast.LENGTH_LONG).show();
            }
            else if (BLEManager.ACTION_WIFI_CONNECT_FAIL.equals(action)) {
                Log.i(TAG, "PROVISIONING COMPLETE.................");
                //WORKAROUND - Disconnect the BLE connection now, in case the user leaves this dialog up and
                //tries to reprovision.
                if (mBleService != null) {
                    mBleService.disconnect();
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(R.string.message_provisioning_failed);
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestBleScan();
                        finish();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    };

    // --------------------------------------------------------------------------------------------
    // -- PRIVATE METHODS
    // --------------------------------------------------------------------------------------------
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(BLEManager.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BLEManager.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BLEManager.ACTION_GATT_SERV_DISC);
        intentFilter.addAction(BLEManager.ACTION_GATT_DATA_READY);
        intentFilter.addAction(BLEManager.ACTION_PROVISIONING_DONE);
        intentFilter.addAction(BLEManager.ACTION_SCANLIST_UPDATED);
        intentFilter.addAction(BLEManager.ACTION_SCANLIST_SCANNING);
        intentFilter.addAction(BLEManager.ACTION_WIFI_CONNECT_SUCCESS);
        intentFilter.addAction(BLEManager.ACTION_WIFI_CONNECT_FAIL);
        intentFilter.addAction(BLEManager.ACTION_WIFI_CONNECT_INPROGRESS);

        return intentFilter;
    }

    private boolean isProvisioningOk()
    {
        boolean ok = false;

        if (!mScanButton.isEnabled()) {
            // Scan is in progress
            return false;
        }
        if ((mSsidTextEntry.getText().length() == 0) || (mSsidTextEntry.getText().length() > 32)) {
            // No SSID
            return false;
        }
        if (mSpinner.getSelectedItem().equals("WEP") && (mKeyTextEntry.getText().length() != 5 && mKeyTextEntry.getText().length() != 10
                && mKeyTextEntry.getText().length() != 13 && mKeyTextEntry.getText().length() != 26)) {
            // Passphrase too short or too long
            return false;
        }
        return !(mSpinner.getSelectedItem().equals("WPA") && (mKeyTextEntry.getText().length() < 8 || mKeyTextEntry.getText().length() > 63));
    }

    private void updateProvisionButton() {
        mProvisionButton.setEnabled(isProvisioningOk());
    }


    private void doProvisioning(AccessPointProvData apProv) {
        if (!mBleService.isBonded()) {
            Toast.makeText(getApplicationContext(), R.string.warning_ble_pairing, Toast.LENGTH_SHORT).show();
        }
        mBleService.writeCharacteristicBytes(mApProvCharacteristic, apProv.serialise());
    }

    private void requestBleScan() {
        Intent intent = new Intent();
        intent.putExtra("WiFiProvidioningActivity", 0);
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, intent);
        } else {
            getParent().setResult(Activity.RESULT_OK, intent);
        }
    }

    // --------------------------------------------------------------------------------------------
    // -- PUBLIC MEMBERS
    // --------------------------------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provisioning);

        mContext = this;
        mDeviceAddress = getIntent().getStringExtra(EXTRAS_DEV_ADDR);
        mDeviceName = getIntent().getStringExtra(EXTRAS_DEV_NAME);
        mSsidTextEntry = (EditText) findViewById(R.id.wifiprovisioning_ssid_entry);
        mKeyTextEntry = (EditText) findViewById(R.id.wifiprovisioning_key_entry);
        mSpinner = (Spinner) findViewById(R.id.spinner);
        mProvisionButton = (Button) findViewById(R.id.wifiprovisioning_go);
        mScanButton = (Button) findViewById(R.id.wifiprovisioning_rescan);
        mCheckbox = (CheckBox) findViewById(R.id.chkpwd);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        mProvisionButton.setEnabled(false);

        mSsidTextEntry.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mSsidTextEntry.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                updateProvisionButton();
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        mKeyTextEntry.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mKeyTextEntry.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                updateProvisionButton();
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        mSecurityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                new ArrayList<String>());
        mSecurityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // TODO - must be a better way to handle this
        mSecurityAdapter.add("open");
        mSecurityAdapter.add("WEP");
        mSecurityAdapter.add("WPA");
        mSecurityAdapter.add("WPA Enterprise");
        mSpinner.setAdapter(mSecurityAdapter);
        mSpinner.setSelection(2);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateProvisionButton();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                updateProvisionButton();
            }
        });

        // Bind callback to the clear button
        Button bclear = (Button) findViewById(R.id.wifiprovisioning_clear);
        bclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "CLEAR button pressed");
                mSsidTextEntry.setFocusable(true);
                mSsidTextEntry.setText(null);
                mKeyTextEntry.setText(null);
                mSpinner.setSelection(0);
            }
        });


        // Bind callback to the provision button
        mProvisionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mKeyTextEntry.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mSsidTextEntry.getWindowToken(), 0);


                String ssid = mSsidTextEntry.getText().toString();
                String key = mKeyTextEntry.getText().toString();
                String sec = mSpinner.getSelectedItem().toString();
                Integer secI = AccessPointScanData.SecurityStringToInteger(sec);

                Log.i(TAG, "Provisioning button clicked: ssid=\"" + ssid + "\" key=\"" + key +
                        "\" security=\"" + sec + "\"");

                final AccessPointProvData apProv = new AccessPointProvData(ssid, key, secI);
                mProvisionButton.setEnabled(false);
                mScanButton.setEnabled(false);
                if (ssid.length() == 0) {
                    Log.i(TAG, "Dialog:No SSID entry");
                    return;
                }
                else if (sec.equals("WEP") && (key.length() != 5 && key.length() != 10 && key.length() != 13 && key.length() != 26)) {
                    Log.i(TAG, "Dialog: Invalid Passphrase"); //TODO  ungrey out provision box
                    Toast.makeText(getApplicationContext(), R.string.warning_passphrase_len, Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (sec.equals("WPA") && (key.length() < 8 || key.length() > 63)) {
                    Log.i(TAG, "Dialog: Invalid Passphrase");
                    Toast.makeText(getApplicationContext(), R.string.warning_passphrase_len, Toast.LENGTH_SHORT).show();
                    return;
                } else if (sec.equals("open")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(R.string.warning_open_connection);
                    builder.setCancelable(false);
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i(TAG, "Dialog:Cancel");
                            mProvisionButton.setEnabled(true);
                        }
                    });
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i(TAG, "Dialog:OK");
                            doProvisioning(apProv);
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                if (sec.equals("open") == false)
                    doProvisioning(apProv);
            }
        });

        mScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Scan button clicked!");

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mKeyTextEntry.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mSsidTextEntry.getWindowToken(), 0);


                UUID test = UUID.fromString("fb8c0002-d224-11e4-85a1-0002a5d5c51b");
                BluetoothGattCharacteristic charac = mBleScanGattService.getCharacteristic(test);
                if (charac == null) {
                    Log.e(TAG, "char not found!");
                }
                else {
                    byte[] value = new byte[1];
                    value[0] = (byte) (1 & 0xFF);
                    charac.setValue(value);
                    mBleService.writeCharacteristicBytes(charac, value);
                    mScanButton.setEnabled(false);
                    invalidateOptionsMenu();
                }
            }
        });

        mCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mKeyTextEntry.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    mKeyTextEntry.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                mKeyTextEntry.setSelection(mKeyTextEntry.getText().length());
                mKeyTextEntry.setTypeface(Typeface.DEFAULT);
            }
        });

        // Set up the list
        ListView lv = (ListView) findViewById(R.id.wifiprovisioning_ap_list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AccessPointScanData ap = mApListAdapter.getDevice(position);
                Log.i(TAG, "User selected access point SSID " + ap.getSsid() + " with sec type " + ap.getSecurityString());

                // Update the UI - we need to:
                //   1. Set the text of the SSID EditText, prevent it from being modified manually
                //   2. Update the security pull down menu (spinner)
                //   3. Update the Key EditText:
                //       a. Enabling it if the SSID EditText isn't NULL and the security type isn't open
                //       b. Disabling it otherwise
                mSsidTextEntry.setText(ap.getSsid());
//                mSsidTextEntry.setFocusable(false);

//                if (ap.getSecurity() != AccessPointScanData.SECURITY_OPEN) {
//                    mKeyTextEntry.setFocusable(true);
//                    mKeyTextEntry.setFocusableInTouchMode(true);
//                    mKeyTextEntry.requestFocus();
//                }
                mSpinner.setSelection(mSecurityAdapter.getPosition(ap.getSecurityString()));
                updateProvisionButton();
            }
        });

        Log.i(TAG, "Attempting to start BleService.");
        Intent gattSrvcIntent = new Intent(this, BLEManager.class);
        bindService(gattSrvcIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
        if (mBleService != null) {
            final boolean result = mBleService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);

            if (!result) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Fail to connect BLE service");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
            else
            {
                mScanButton.setEnabled(true);
            }
        }
        invalidateOptionsMenu();

        if (mApListAdapter == null)
        {
            // Set the ListAdapater
            mApListAdapter = new ApListAdapter();
            ((ListView) findViewById(R.id.wifiprovisioning_ap_list)).setAdapter(mApListAdapter);

            //Hide the soft keypad until user presses on an EditText
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
        //Clear the keyboard on resume, in case we have just come from the PIN entry app and are
        //still provisioning and don't want to confuse the user.
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mKeyTextEntry.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mSsidTextEntry.getWindowToken(), 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
        if (mBleService != null) {
            //  mBleService.disconnect();
        }
        mScanButton.setEnabled(false);
        invalidateOptionsMenu();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
        unbindService(mServiceConnection);
        mBleService = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMen()");
     /*   getMenuInflater().inflate(R.menu.options, menu);
        menu.findItem(R.id.menu_scan).setVisible(false);
        menu.findItem(R.id.menu_stop).setVisible(false);
        if (mScanButton.isEnabled()) {
            menu.findItem(R.id.menu_progress_meter).setActionView(null);
        } else {
            menu.findItem(R.id.menu_progress_meter).setActionView(R.layout.actionbar_ind_progress_spinner);
        }
        */return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Implement the home feature
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
