package com.wifiprovision.microchip.wifiprovisioner;

import java.util.HashMap;

/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class ProvisioningAttributes {
    private static HashMap<String, String> attributes = new HashMap();

    static {
        // -----------------------------------------------------------------------------------------
        // -- Important Service UUIDs
        // -----------------------------------------------------------------------------------------
        attributes.put("00001800-0000-1000-8000-00805f9b34fb", "Generic Access");
        attributes.put("77880001-d229-11e4-8689-0002a5d5c51b", "Provisioning Service");
        attributes.put("fb8c0001-d224-11e4-85a1-0002a5d5c51b", "Wi-Fi Scan Service");

        // -----------------------------------------------------------------------------------------
        // -- Scanning Service UUIDs
        // -----------------------------------------------------------------------------------------
        attributes.put("fb8c0002-d224-11e4-85a1-0002a5d5c51b", "Scanning Mode");
        attributes.put("fb8c0003-d224-11e4-85a1-0002a5d5c51b", "AP List Status");
        attributes.put("fb8c0100-d224-11e4-85a1-0002a5d5c51b", "AP Details");

        // -----------------------------------------------------------------------------------------
        // -- Provisioning Service UUIDs
        // -----------------------------------------------------------------------------------------
        attributes.put("77880002-d229-11e4-8689-0002a5d5c51b", "Connection State");
        attributes.put("77880003-d229-11e4-8689-0002a5d5c51b", "AP Parameters");
    }
}
