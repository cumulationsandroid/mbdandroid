package com.atmel.bleprovision;

import android.util.Log;

/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class AccessPointProvData {
    private static final String TAG=AccessPointProvData.class.getSimpleName();
    private String  mSsidString;
    private String  mKeyString;
    private Integer mSecurity;

    AccessPointProvData(String ssid, String keyString, Integer securityType) {
        Log.i(TAG, "dataset constructed...");
        mSsidString = ssid;
        mKeyString  = keyString;
        mSecurity   = securityType;
    }

    public byte[] serialise() {
        byte[] rawData   = new byte[98];
        byte[] ssidBytes = mSsidString.getBytes();

        rawData[0] = mSecurity.byteValue();
        rawData[1] = (byte) Math.min((byte)mSsidString.length(), 32);
        System.arraycopy(ssidBytes, 0, rawData, 2, rawData[1]);
        rawData[34] = (byte) Math.min((byte)mKeyString.length(), 63);
        System.arraycopy(mKeyString.getBytes(), 0, rawData, 35, rawData[34]);

        Log.i(TAG, "Serialised: " + rawData);

        return rawData;
    }
}
