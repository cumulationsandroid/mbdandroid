package com.atmel.bleprovision;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;
import java.util.HashMap;

import static java.util.Arrays.*;


/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class AccessPointScanData implements Parcelable {
    private int mSecurity;
    private int mRssi;
    private String mSsid;

    private final static String TAG = AccessPointScanData.class.getSimpleName();
    public final static int     SECURITY_OPEN = 1;
    public final static int     SECURITY_WPA = 2;    // NB: Includes WPA2
    public final static int     SECURITY_WEP = 3;
    private final static HashMap<Integer, String> securityMap = new HashMap<Integer, String>();
    static {
        securityMap.put(SECURITY_OPEN, "open");
        securityMap.put(SECURITY_WPA, "WPA");
        securityMap.put(SECURITY_WEP, "WEP");
    }
    private final static HashMap<String, Integer> reverseSecMap = new HashMap<String, Integer>();
    static {
        reverseSecMap.put("open", SECURITY_OPEN);
        reverseSecMap.put("WPA", SECURITY_WPA);
        reverseSecMap.put("WEP", SECURITY_WEP);
    }

    AccessPointScanData(byte[] rawData) {
        if (rawData.length > 0) {
            mSecurity = (int) rawData[0];
            mRssi = (int) rawData[1];

            byte[] tmpdata = copyOfRange(rawData, 3, 3 + (int) rawData[2]);
            mSsid = new String(tmpdata);
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("AP Object {" + mSsid + " Security: " + mSecurity + " Rssi:" + mRssi + "}");
        return result.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mSecurity);
        dest.writeInt(mRssi);
        dest.writeString(mSsid);
    }

    public String getSsid()             { return mSsid; }

    public int getRssi()                { return mRssi; }

    public int getSecurity()            { return mSecurity; }

    public String getSecurityString()   { return securityMap.get(mSecurity); }

    public static Integer SecurityStringToInteger(String s) { return reverseSecMap.get(s); }

    public AccessPointScanData(Parcel in) {
        mSecurity = in.readInt();
        mRssi = in.readInt();
        mSsid = in.readString();
    }

    public static final AccessPointScanData.Creator<AccessPointScanData> CREATOR =
            new Creator<AccessPointScanData>() {
                @Override
                public AccessPointScanData createFromParcel(Parcel source) {
                    return new AccessPointScanData(source);
                }

                @Override
                public AccessPointScanData[] newArray(int size) {
                    return new AccessPointScanData[size];
                }
            };
}
