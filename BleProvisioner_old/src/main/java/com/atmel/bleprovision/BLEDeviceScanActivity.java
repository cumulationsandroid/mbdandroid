package com.atmel.bleprovision;

import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class BLEDeviceScanActivity extends ListActivity {

    // --------------------------------------------------------------------------------------------
    // -- INNER CLASSES
    // --------------------------------------------------------------------------------------------
    static class ViewHolder {
        TextView devName;
        TextView devAddr;
        TextView devRssi;
    }

    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflater;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflater = ((Activity)mContext).getLayoutInflater();
        }

        @Override
        public int getCount()           { return mLeDevices.size(); }

        @Override
        public Object getItem(int i)    { return mLeDevices.get(i); }

        @Override
        public long getItemId(int i)    { return i; }

        @Override
        public View getView(int i, View v, ViewGroup vg) {
            ViewHolder viewHolder; // allows us to store view objects to avoid findViewById calls if a view is recycled

            if (v == null) {
                // There are no views that can be recycled, we'll have to create one...
                v = mInflater.inflate(R.layout.ble_device_listitem, null);

                viewHolder = new ViewHolder();
                viewHolder.devName = (TextView)v.findViewById(R.id.bldevlst_name);
                viewHolder.devAddr = (TextView)v.findViewById(R.id.bledevlst_addr);
                viewHolder.devRssi = (TextView) v.findViewById(R.id.bledevlst_rssi);

                // Assign this to the view - if it is recycled we can recover via getTag
                v.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) v.getTag();
            }

            // OK, let's muck around...
            BluetoothDevice dev = mLeDevices.get(i);
            final String devName = dev.getName();
            if (devName != null && devName.length() > 0) {
                viewHolder.devName.setText(devName);
            } else {
                viewHolder.devName.setText("Unknown device");
            }
            viewHolder.devAddr.setText(dev.getAddress());

            return v;
        }

        public void addDevice(BluetoothDevice dev) {
            if (!mLeDevices.contains(dev)) {
                mLeDevices.add(dev);
                Log.i(TAG, "Adding dev " + dev);
            }
        }

        public BluetoothDevice getDevice(int pos) { return mLeDevices.get(pos); }

        public void clear()                       { mLeDevices.clear(); }
    }

    // --------------------------------------------------------------------------------------------
    // -- Private BleService members
    // --------------------------------------------------------------------------------------------
    private static final String     TAG = "BLEDeviceScanActivity";
    private boolean                 mScanning;
    private Context                 mContext;
    private LeDeviceListAdapter     mLeDeviceListAdapter;
    private BluetoothAdapter        mBluetoothAdapter;

    private static final boolean    DEBUG = false;

    // --------------------------------------------------------------------------------------------
    // -- PUBLIC METHODS
    // --------------------------------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "BLEDeviceScanActivity:onCreate\n");
        mContext = this;
        // MBD
       // getActionBar().setTitle("BLE Device Scanner");

        if (!DEBUG) {
            if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
                Toast.makeText(this, "BLE is not supported on this platform", Toast.LENGTH_SHORT).show();
                finish();
            }

            final BluetoothManager bm = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = bm.getAdapter();

            if (mBluetoothAdapter == null) {
                Toast.makeText(this, "Bluetooth is not supported.", Toast.LENGTH_SHORT).show();
                finish();
                return; // TODO - is this necessary, doesn't finish() shut everything down?
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // TODO - ensure BLE is enabled on the device. If not, fire an intent to display a
        //        dialog asking the user to grant permission to enable it. Will need to
        //        prove an onActivityResult implementation to catch if the user elects not
        //        to grant permission (in which case we will terminate the app).


        // Set the ListAdapater
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        setListAdapter(mLeDeviceListAdapter);

        // TODO - probably a good idea to kick of a scan here... (scanLeDevice(true));
    }

    @Override
    public void onPause() {
        super.onPause();
        scanLeDevice(false);
        mLeDeviceListAdapter.clear();
    }

    // Called to create the options menu (top right of screen) as per a given layout
    // (in this case res/menu/options.xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "BLEDeviceScanActivity:onCreateOptionsMenu\n");

        // Inflate the xml resource
        getMenuInflater().inflate(R.menu.options, menu);

        // Determine, based on the current scanning activity, which menu
        // item to make visible and which to make invisible. Also set the
        // state of the menu item declared for indicating progress (i.e. spinner
        // while scanning is in progress).
        if (!mScanning) {
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_progress_meter).setActionView(null);
        } else {
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_progress_meter).setActionView(R.layout.actionbar_ind_progress_spinner);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "BLEDeviceScanActivity:onOptionsItemSelected\n");

       /*  MBD switch (item.getItemId()) {
            case R.id.menu_scan:
                // TODO Clear the list adapter!!
                scanLeDevice(true);
                break;

            case R.id.menu_stop:
                scanLeDevice(false);
                break;
        }*/

        int id = item.getItemId();

        if (id == R.id.menu_scan ) {
            // TODO Clear the list adapter!!
            scanLeDevice(true);
        } else  if (id == R.id.menu_stop ) {
            scanLeDevice(false);
        }
        return true;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int pos, long id) {
        Log.i(TAG, "onListItemClick(): selected " + id);
        final BluetoothDevice dev = mLeDeviceListAdapter.getDevice(pos);
        if (dev == null)
            return;

        if (mScanning) { scanLeDevice(false); }

        try {
            Method m = dev.getClass().getMethod("removeBond", (Class[])null);
            m.invoke(dev, (Object[])null);
        } catch (NoSuchMethodException e) {
            Log.e(TAG, e.getMessage());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        final Intent intent = new Intent(this, WifiProvisionActivity.class);
        intent.putExtra(WifiProvisionActivity.EXTRAS_DEV_NAME, dev.getName());
        intent.putExtra(WifiProvisionActivity.EXTRAS_DEV_ADDR, dev.getAddress());
        startActivity(intent);
    }

    private void scanLeDevice(final boolean enabled) {
        if (enabled) {
            mLeDeviceListAdapter.clear();
            mScanning = true;
            // TODO need to have a timer stop the scan and invalidate the list so we don't scan forever
            if (!DEBUG) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }
        } else {
            if (DEBUG) {
                // OK let's temporarily create some dummy BLE devices for the list adapter...
                BluetoothAdapter ba = null;
                ba = BluetoothAdapter.getDefaultAdapter();
                BluetoothDevice bd1 = ba.getRemoteDevice("11:11:11:11:11:11");
                BluetoothDevice bd2 = ba.getRemoteDevice("22:22:22:22:22:22");
                BluetoothDevice bd3 = ba.getRemoteDevice("33:33:33:33:33:33");
                mLeDeviceListAdapter.addDevice(bd1);
                mLeDeviceListAdapter.addDevice(bd2);
                mLeDeviceListAdapter.addDevice(bd3);
                mLeDeviceListAdapter.notifyDataSetChanged();
            } else {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }
            mScanning = false;
        }
        invalidateOptionsMenu();
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLeDeviceListAdapter.addDevice(device);
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }
            });
        }
    };
}
