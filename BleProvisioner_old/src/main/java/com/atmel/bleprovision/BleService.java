package com.atmel.bleprovision;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.Arrays;
import java.util.List;

/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class BleService extends Service {
    // --------------------------------------------------------------------------------------------
    // -- INNER CLASSES
    // --------------------------------------------------------------------------------------------
    public class LocalBinder extends Binder {
        BleService  getService() { return BleService.this; }
    }

    // --------------------------------------------------------------------------------------------
    // -- Private BleService members
    // --------------------------------------------------------------------------------------------
    private static final String TAG                 = BleService.class.getSimpleName();
    private final IBinder mBinder                   = new LocalBinder();
    private static final int    STATE_DISCONNECTED  = 0;
    private static final int    STATE_CONNECTING    = 1;
    private static final int    STATE_CONNECTED     = 2;

    private BluetoothManager    mBluetoothManager;
    private BluetoothAdapter    mBluetoothAdapter;
    private BluetoothGatt       mBluetoothGatt;
    private String              mBluetoothDeviceAddress;
    private int                 mConnectionState;   // TODO - this doesn't seem to be needed!
    private int                 mCharWriteIndex;
    private int                 mCharEndIndex;
    private byte[]              mCharRawData;

    // --------------------------------------------------------------------------------------------
    // -- Create callback methods for the GATT events our app is interested in
    // -- TODO - right now this is almost an exhaustive list - later we'll prune to what we need
    // -- TODO - think we can move this - it's only used when connecting...
    // --------------------------------------------------------------------------------------------
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            String intentAction;

            Log.i(TAG, "onConnectionStateChange(): status=" + status + " newState=" + newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "onConnectionStateChange(): Connected to GATT server.");
                intentAction        = ACTION_GATT_CONNECTED;
                mConnectionState    = STATE_CONNECTED;
            } else {
                Log.i(TAG, "onConnectionStateChange(): Disconnected from GATT server.");
                intentAction        = ACTION_GATT_DISCONNECTED;
                mConnectionState    = STATE_DISCONNECTED;
            }
            broadcastUpdate(intentAction);

            if (mConnectionState == STATE_CONNECTED) {
                Log.i(TAG, "Attempting to start service discovery: " +
                        mBluetoothGatt.discoverServices());
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERV_DISC);
            } else {
                Log.v(TAG, "onServicesDiscovered(): received bad status: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
//            super.onCharacteristicRead(gatt, characteristic, status);

            Log.d(TAG, "onCharacteristicRead(): fired.." + status);

            // TODO - wrap with success check on status for "BluetoothGatt.GATT_SUCCESS"
            broadcastUpdate(ACTION_GATT_DATA_READY, characteristic);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

            // TODO - Implement me...
            Log.i(TAG, "OnCharacteristicChanged()....");
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);

            // TODO - Implement me?
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);

            Log.i(TAG, "OnCharacteristicWrite()...." + status);
            Log.d(TAG, "gatt = " + gatt.toString());
            if (mCharWriteIndex < mCharEndIndex) {
                doWrite(characteristic);
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);

            // TODO - Implement me?
        }
    };

    // --------------------------------------------------------------------------------------------
    // -- PRIVATE METHODS
    // --------------------------------------------------------------------------------------------
    private void broadcastUpdate(final String action) {
        final Intent    intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic ch) {
        final Intent    intent = new Intent(action);
        final byte[]    data   = ch.getValue();

        // TODO - this is just some debug code...
        if (ch.getUuid().toString().equals("fb8c0100-d224-11e4-85a1-0002a5d5c51b")) {

            if (data != null) {
                AccessPointScanData ap = new AccessPointScanData(data);

                Log.i(TAG, "created an AP object - " + ap);
                intent.putExtra(EXTRA_DATA, ap);
            } else {
                Log.e(TAG, "BluetoothGattCharacteristic has NULL value!");
                return;
            }
        }
        sendBroadcast(intent);
    }

    // --------------------------------------------------------------------------------------------
    // -- PUBLIC MEMBERS
    // --------------------------------------------------------------------------------------------
    // TODO - need to create some public strings for broadcasting events such as
    //        "GATT CONNECTED", "DATA AVAILABLE", "DATA WRITTEN"
    public static final String ACTION_GATT_CONNECTED    = "com.atmel.bleprov.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "com.atmel.bleprov.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_SERV_DISC    = "com.atmel.bleprov.ACTION_GATT_SERV_DISC";
    public static final String ACTION_GATT_DATA_READY   = "com.atmel.bleprov.ACTION_GATT_DATA_READY";
    public static final String EXTRA_DATA               = "com.atmel.bleprov.EXTRA_DATA";


    // --------------------------------------------------------------------------------------------
    // -- PUBLIC METHODS
    // --------------------------------------------------------------------------------------------
    @Override
    public IBinder onBind(Intent intent)    { return mBinder; }

    @Override
    public boolean onUnbind(Intent intent) {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }
        return super.onUnbind(intent);
    }

    public boolean initialise() {
        // Get a reference to BluetoothAdapter through the BluetoothManager.
        if (mBluetoothManager ==  null) {
            mBluetoothManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialise BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    public boolean connect(final String addr) {
        if (mBluetoothAdapter == null || addr == null) {
            Log.v(TAG, "BluetoothAdapter not initialised or address not specified.");
            return false;
        }

        // Check for currently connected device... try to reconnect
        if (mBluetoothDeviceAddress != null && addr.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(addr);
        if (device == null) {
            Log.v(TAG, "connect(): Device not found, unable to connect to " + addr);
            return false;
        }

        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = addr;
        mConnectionState        = STATE_CONNECTING;
        return true;
    }

    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.v(TAG, "disconnected(): BluetoothAdapter not initialised.");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public void readCharacteristic(BluetoothGattCharacteristic ch) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.v(TAG, "readCharacteristic(): BluetoothAdapter not enabled.");
            return;
        }
        mBluetoothGatt.readCharacteristic(ch);
    }

    public void writeCharacteristicBytes(BluetoothGattCharacteristic ch, byte[] rawdata) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.v(TAG, "writeCharacteristic(): BluetoothAdapter not initialised.");
            return;
        }

        Log.i(TAG, "Calling writeCharacteristic...");
        mCharWriteIndex = 0;
        mCharEndIndex   = rawdata.length - 1;
        mCharRawData    = rawdata;

        Log.d(TAG, "mBluetoothGatt = " +mBluetoothGatt.toString());
        doWrite(ch);
    }

    private void doWrite(BluetoothGattCharacteristic ch) {
        byte[] pld    = new byte[20];
        pld[0] = (byte) mCharWriteIndex;
        int    maxLen = Math.min(19, mCharEndIndex - mCharWriteIndex);

        System.arraycopy(mCharRawData, mCharWriteIndex, pld, 1, maxLen);

        Log.i(TAG, "doWrite(): " + maxLen + " bytes: " + Arrays.toString(pld));
        ch.setValue(pld);
        mBluetoothGatt.writeCharacteristic(ch);
        mCharWriteIndex += maxLen;
    }
}
