package com.atmel.bleprovision;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rob.tebbs on 14/05/2015.
 */
public class WifiProvisionActivity extends Activity {

    // --------------------------------------------------------------------------------------------
    // -- INNER CLASSES
    // --------------------------------------------------------------------------------------------
    static class ViewHolder {
        ImageView security;
        TextView ssid;
    }

    private class ApListAdapter extends BaseAdapter {
        private ArrayList<AccessPointScanData> mAccessPoints;
        private LayoutInflater mInflater;

        public ApListAdapter() {
            super();
            mAccessPoints = new ArrayList<AccessPointScanData>();
            mInflater = ((Activity) mContext).getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mAccessPoints.size();
        }

        @Override
        public Object getItem(int i) {
            return mAccessPoints.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View v, ViewGroup vg) {
            ViewHolder viewHolder; // allows us to store view objects to avoid findViewById calls if a view is recycled

            if (v == null) {
                // There are no views that can be recycled, we'll have to create one...
                v = mInflater.inflate(R.layout.access_point_listitem, null);

                viewHolder = new ViewHolder();
                viewHolder.security = (ImageView) v.findViewById(R.id.aplist_secure);
                viewHolder.ssid = (TextView) v.findViewById(R.id.aplist_ssid);

                // Assign this to the view - if it is recycled we can recover via getTag
                v.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) v.getTag();
            }

            // OK, let's muck around...
            AccessPointScanData ap = mAccessPoints.get(i);
            final String apSsid = ap.getSsid();
            final int apSecurity = ap.getSecurity();
            final int apRssi = ap.getRssi();

            viewHolder.ssid.setText(apSsid);
            if (apSecurity != 1)    // 1 = open, 2=wpa, 3=wep
                viewHolder.security.setImageResource(android.R.drawable.ic_secure);
            else
                viewHolder.security.setImageResource(android.R.drawable.ic_partial_secure);

            return v;
        }

        public void addDevice(AccessPointScanData ap) {
            if (!mAccessPoints.contains(ap)) {
                mAccessPoints.add(ap);
                mApListAdapter.notifyDataSetChanged();
            }
        }

        public AccessPointScanData getDevice(int pos) {
            return mAccessPoints.get(pos);
        }

        public void clear() {
            mAccessPoints.clear();
        }
    }

    // --------------------------------------------------------------------------------------------
    // -- Private WifiProvisionActivity members
    // --------------------------------------------------------------------------------------------
    private static final String TAG = WifiProvisionActivity.class.getSimpleName();
    private Context mContext;
    private BleService mBleService;
    private String mDeviceAddress;
    private String mDeviceName;
    private ApListAdapter mApListAdapter;
    private EditText mSsidTextEntry;
    private EditText mKeyTextEntry;
    private Button mProvisionButton;
    private Spinner mSpinner;

    ArrayAdapter<String> mSecurityAdapter;

    private ArrayList<BluetoothGattCharacteristic> mScannedApArray = new ArrayList<>();
    private int mApIndex;

    private BluetoothGattService mBleProvGattService;
    private BluetoothGattCharacteristic mApProvCharacteristic;

    // --- Keys for sending BLE device name and address to this activity (Intent EXTRAs) ---
    public static final String EXTRAS_DEV_NAME = "DEV_NAME";
    public static final String EXTRAS_DEV_ADDR = "DEV_ADDR";


    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBleService = ((BleService.LocalBinder) service).getService();
            if (!mBleService.initialise()) {
                Log.e(TAG, "onServiceConnected(): Unable to initialise Bluetooth");
                finish();
            }
            Log.d(TAG, "onServiceConnected(): OK. Attempting to connect to dev " + mDeviceAddress);
            mBleService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBleService = null;
        }
    };

    final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (BleService.ACTION_GATT_CONNECTED.equals(action)) {

                // TODO - implement something!

            } else if (BleService.ACTION_GATT_DISCONNECTED.equals(action)) {

                // TODO - implement something!

            } else if (BleService.ACTION_GATT_SERV_DISC.equals(action)) {
                Log.i(TAG, "onReceive(): Services have been discovered");
                List<BluetoothGattService> serviceList = mBleService.getSupportedGattServices();

                // Iterate through the services for now...
                for (BluetoothGattService service : serviceList) {
                    List<BluetoothGattCharacteristic> gattCharacteristics = service.getCharacteristics();

                    if (service.getUuid().equals("77880001-d229-11e4-8689-0002a5d5c51b")) {
                        mBleProvGattService = service;
                    }

                    Log.i(TAG, "onReceive(): service found: " + service.getUuid());
                    Log.i(TAG, "onReceive(): with " + gattCharacteristics.size() + " characteristics");

                    for (BluetoothGattCharacteristic bgc : gattCharacteristics) {
                        Log.d(TAG, "characteristic found " + bgc);
                        if (bgc.getUuid().toString().equals("fb8c0100-d224-11e4-85a1-0002a5d5c51b")) {
                            // TODO - add this to the mAccessPoints list
                            Log.i(TAG, "AP ScanInfo characteristic found..");
                            mScannedApArray.add(bgc);
                        }

                        if (bgc.getUuid().toString().equals("77880003-d229-11e4-8689-0002a5d5c51b")) {
                            Log.i(TAG, "AP Cred characteristic found..");
                            mApProvCharacteristic = bgc;
                        }
                    }
                }

                // Let's iterate through all of the characteristics and sort them somehow

                // kick off a read of all of the AP details characteristics..
                if (mScannedApArray.size() > 0) {
                    mApIndex = 0;
                    Log.i(TAG, "kicking off read of first Scanned AP characteristic");
                    mBleService.readCharacteristic(mScannedApArray.get(mApIndex));
                } else {
                    Log.e(TAG, "HAVEN'T FOUND ANY ACCESS POINTS....");
                }
                // TODO - Lots more work to do in here...


            } else if (BleService.ACTION_GATT_DATA_READY.equals(action)) {
                AccessPointScanData ap = (AccessPointScanData) intent.getParcelableExtra(BleService.EXTRA_DATA);
                mApListAdapter.addDevice(ap);

                Log.i(TAG, ">> data for index " + mApIndex + " of " + mScannedApArray.size() + " is " + ap);

                mApIndex++;
                if (mApIndex < mScannedApArray.size()) {
                    mBleService.readCharacteristic(mScannedApArray.get(mApIndex));
                }

            }
        }
    };

    // --------------------------------------------------------------------------------------------
    // -- PRIVATE METHODS
    // --------------------------------------------------------------------------------------------
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(BleService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BleService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BleService.ACTION_GATT_SERV_DISC);
        intentFilter.addAction(BleService.ACTION_GATT_DATA_READY);

        return intentFilter;
    }

    public void updateSpinner(int arg) {

    }

    // --------------------------------------------------------------------------------------------
    // -- PUBLIC MEMBERS
    // --------------------------------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.provision_activity_layout);

        mContext = this;
        mDeviceAddress = getIntent().getStringExtra(EXTRAS_DEV_ADDR);
        mDeviceName = getIntent().getStringExtra(EXTRAS_DEV_NAME);
        mSsidTextEntry = (EditText) findViewById(R.id.wifiprovisioning_ssid_entry);
        mKeyTextEntry = (EditText) findViewById(R.id.wifiprovisioning_key_entry);
        mSpinner = (Spinner) findViewById(R.id.spinner);
        mProvisionButton = (Button) findViewById(R.id.wifiprovisioning_go);

        // MBD
        //getActionBar().setTitle(mDeviceName);
        // MBD
        //getActionBar().setDisplayHomeAsUpEnabled(true);


        mSecurityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                new ArrayList<String>());
        mSecurityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // TODO - must be a better way to handle this
        mSecurityAdapter.add("None");
        mSecurityAdapter.add("WEP");
        mSecurityAdapter.add("WPA");

        mSpinner.setAdapter(mSecurityAdapter);

        // Bind callback to the clear button
        Button bclear = (Button) findViewById(R.id.wifiprovisioning_clear);
        bclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "CLEAR button pressed");
                mSsidTextEntry.setFocusable(true);
                mSsidTextEntry.setText(null);
                mKeyTextEntry.setText(null);
                mKeyTextEntry.setFocusable(false);
            }
        });

        // Bind callback to the provision button
        mProvisionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ssid = mSsidTextEntry.getText().toString();
                String key = mKeyTextEntry.getText().toString();
                String sec = mSpinner.getSelectedItem().toString();
                Integer secI = AccessPointScanData.SecurityStringToInteger(sec);

                Log.i(TAG, "Provisioning button clicked: ssid=" + ssid + " key=" + key +
                        " security = " + sec);

                AccessPointProvData apProv = new AccessPointProvData(ssid, key, secI);
                mBleService.writeCharacteristicBytes(mApProvCharacteristic, apProv.serialise());
            }
        });

        // Set up the list
        ListView lv = (ListView) findViewById(R.id.wifiprovisioning_ap_list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AccessPointScanData ap = mApListAdapter.getDevice(position);
                Log.i(TAG, "User selected access point SSID " + ap.getSsid() + " with sec type " + ap.getSecurityString());

                // Update the UI - we need to:
                //   1. Set the text of the SSID EditText, prevent it from being modified manually
                //   2. Update the security pull down menu (spinner)
                //   3. Update the Key EditText:
                //       a. Enabling it if the SSID EditText isn't NULL and the security type isn't open
                //       b. Disabling it otherwise
                mSsidTextEntry.setText(ap.getSsid());
                mSsidTextEntry.setFocusable(false);

                if (ap.getSecurity() != AccessPointScanData.SECURITY_OPEN) {
                    mKeyTextEntry.setFocusable(true);
                    mKeyTextEntry.setFocusableInTouchMode(true);
                    mKeyTextEntry.requestFocus();
                }
                mSecurityAdapter.clear();
                mSecurityAdapter.add(ap.getSecurityString());
                mSecurityAdapter.notifyDataSetChanged();
            }
        });

        Log.i(TAG, "Attempting to start BleService.");
        Intent gattSrvcIntent = new Intent(this, BleService.class);
        bindService(gattSrvcIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBleService != null) {
            final boolean result = mBleService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }

        // Set the ListAdapater
        mApListAdapter = new ApListAdapter();
        ((ListView) findViewById(R.id.wifiprovisioning_ap_list)).setAdapter(mApListAdapter);

        // Let's try to give the button focus to see if it gets rid of the stupid soft-keypad
        ((Button) findViewById(R.id.wifiprovisioning_clear)).requestFocus();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBleService = null;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//      TODO - implement me!!!!!!!
//    }
}
