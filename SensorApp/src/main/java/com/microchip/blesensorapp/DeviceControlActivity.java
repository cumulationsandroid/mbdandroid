package com.microchip.blesensorapp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

public class DeviceControlActivity extends Activity {

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static final String EXTRAS_SCAN_RESULT = "SCAN_RESULT";

    private TextView mConnectionState;
    private TextView mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BLEService mBleService;
    private List<BluetoothGattCharacteristic> mGattCharacteristics;
    private boolean mConnected = false;
    private boolean mRead = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private ScanResult mScanResult;
    private byte[] mScanResultDep;
    private BluetoothGattCharacteristic ledSensor = null;
    private Double temperature;
    private Double batteryStatus = -1.0;
    private CountDownTimer timeoutTimer = null;
    private boolean statusTimer = false;
    private byte viewIndex = 0;

    private android.os.Handler mHandler = new android.os.Handler();
    private LineGraphSeries<DataPoint> mSeriesAccX;
    private LineGraphSeries<DataPoint> mSeriesAccY;
    private LineGraphSeries<DataPoint> mSeriesAccZ;
    private LineGraphSeries<DataPoint> mSeriesGyrX;
    private LineGraphSeries<DataPoint> mSeriesGyrY;
    private LineGraphSeries<DataPoint> mSeriesGyrZ;
    private Runnable mTimer;
    private double graph2LastAValue = 5d;
    private double graph2LastGValue = 5d;


    private void processSensorData(String uuidCharacteristic, String data) {

        switch (uuidCharacteristic) {
            case "bf3fbd80-063f-11e5-9e69-0002a5d5c501":        // Light
                if (Integer.decode("0x" + data) < 0x0CE4 && Integer.decode("0x" + data) > 0x0000) {
                    int fillColor = (((SeekArc) findViewById(R.id.seekArc2)).getProgress() * 255) / 100;


                    if (batteryStatus > 0)
                        ((SeekArc) findViewById(R.id.seekArc2)).setProgress((int)(((double) Integer.decode("0x" + data) * 100) / batteryStatus));
                    else
                        ((SeekArc) findViewById(R.id.seekArc2)).setProgress(((Integer.decode("0x" + data) * 100) / 3600));

                    ((SeekArc) findViewById(R.id.seekArc2)).setArcColor(Color.rgb(fillColor, fillColor, fillColor));

                    Double luxValue;
                    if (batteryStatus > 0)
                        luxValue = ((Double) ((double) Integer.decode("0x" + data) / batteryStatus));
                    else
                        luxValue = ((Double) ((double) Integer.decode("0x" + data) / 3600));

                    if (luxValue < 0)
                        luxValue = 0.0;

                    luxValue = (((1 / (1 - luxValue)) - 1) * 500 / 12);

                    if (luxValue > 10000)
                        luxValue = 10000.0;

                    if (luxValue < 1000)
                        ((TextView) findViewById(R.id.seekArcProgress2)).setText(String.format("%.0f", luxValue) + "lux");
                    else
                        ((TextView) findViewById(R.id.seekArcProgress2)).setText(String.format("%.0f", luxValue / 1000) + "Klux");
                }

                break;

            case "bf3fbd80-063f-11e5-9e69-0002a5d5c502":        // Pot

                if (Integer.decode("0x" + data) < 0x0CE4 && Integer.decode("0x" + data) > 0x0000) {
                    if (batteryStatus > 0)
                        ((SeekArc) findViewById(R.id.seekArc)).setProgress((int)(((double) Integer.decode("0x" + data) * 100) / batteryStatus));
                    else
                        ((SeekArc) findViewById(R.id.seekArc)).setProgress(((Integer.decode("0x" + data) * 100) / 3600));

                    ((TextView) findViewById(R.id.seekArcProgress)).setText(String.format("%.2f", ((Float) ((float) Integer.decode("0x" + data) / 1000))) + "V");
                }
                break;

            case "bf3fbd80-063f-11e5-9e69-0002a5d5c503":        // Switch
                if (Integer.decode("0x" + data) == 0x0000 || Integer.decode("0x" + data) == 0x0001) {
                    if (data.equals("0001"))
                        ((ImageView) findViewById(R.id.switch_image)).setImageResource(R.drawable.bulbonicon);
                    else
                        ((ImageView) findViewById(R.id.switch_image)).setImageResource(R.drawable.bulbofficon);
                }
                break;

            case "bf3fbd80-063f-11e5-9e69-0002a5d5c504":        // Temperature

                if (Integer.decode("0x" + data) < 0x08F8 && Integer.decode("0x" + data) > 0x03F6) {

                    Double tempValue;
                    if (batteryStatus > 0)
                        tempValue = ((Double) ((double) Integer.decode("0x" + data) / batteryStatus)) * 4096.0;
                    else
                        tempValue = ((Double) ((double) Integer.decode("0x" + data) / 3600)) * 4096.0;

                    if (tempValue < 1154 || tempValue > 2613) {
                        //tempView.progress = 0
                        ((ProgressBar) findViewById(R.id.temp_progress)).setProgress(0);
                        ((TextView) findViewById(R.id.temp_text)).setText("...");
                    } else {
                        temperature = (((tempValue / 1459 * 90) - 91.2) * 9 / 5) + 32 - 10;
                        if (temperature.isNaN())
                            temperature = 0.0;

                        ((ProgressBar) findViewById(R.id.temp_progress)).post(new Runnable() {
                            @Override
                            public void run() {
                                ((ProgressBar) findViewById(R.id.temp_progress)).setProgress(temperature.intValue());
                            }
                        });

                        ((TextView) findViewById(R.id.temp_text)).setText(String.format("%.2f", temperature) + "F");
                    }
                }
                break;

            case "bf3fbd80-063f-11e5-9e69-0002a5d5c505":        // Battery

                //if (Integer.decode("0x" + data) < 0x0839 && Integer.decode("0x" + data) > 0x03A1) {
                    Double batteryValue = ((Double) ((double) Integer.decode("0x" + data) / 1000));
                    batteryStatus = batteryValue * 1000;
                    batteryValue = (((batteryValue - 1.9) / (3.3 - 1.9)) * 100);

                    if (batteryValue > 100)
                        batteryValue = 100.0;

                    ((TextView) findViewById(R.id.battery_text)).setText(batteryValue.intValue() + "%");
                //}
                break;

            case "1bc5d5a5-0200-a687-e511-3639d7ba5af0":
                //Log.i("Accelerometer", data.substring(0, 4) + "-" + String.valueOf((((short)Integer.parseInt(data.substring(0, 4), 16))))+ " : " +
                //        data.substring(4, 8) + "-" + String.valueOf((((short)Integer.parseInt(data.substring(4, 8), 16)))) + " : " +
                //        data.substring(8) + "-" + String.valueOf((((short)Integer.parseInt(data.substring(8), 16)))));//data + " : " + data.substring(0, 4) + data.substring(4, 8) + data.substring(8));
                graph2LastAValue += 1d;
                mSeriesAccX.appendData(new DataPoint(graph2LastAValue, (((short)Integer.parseInt(data.substring(0, 4), 16)))), true, 100);
                mSeriesAccY.appendData(new DataPoint(graph2LastAValue, (((short)Integer.parseInt(data.substring(4, 8), 16)))), true, 100);
                mSeriesAccZ.appendData(new DataPoint(graph2LastAValue, (((short)Integer.parseInt(data.substring(8), 16)))), true, 100);
                break;


            case "1bc5d5a5-0200-a687-e511-3639d4ba5af0":
                //Log.i("Gyroscope", String.valueOf((((short)Integer.parseInt(data.substring(0, 4), 16))))+ " : " +
                //        String.valueOf((((short)Integer.parseInt(data.substring(4, 8), 16)))) + " : " +
                //        String.valueOf((((short)Integer.parseInt(data.substring(8), 16)))));//data + " : " + data.substring(0, 4) + data.substring(4, 8) + data.substring(8));
                graph2LastGValue += 1d;
                mSeriesGyrX.appendData(new DataPoint(graph2LastGValue, (((short)Integer.parseInt(data.substring(0, 4), 16))>>4)), true, 100);
                mSeriesGyrY.appendData(new DataPoint(graph2LastGValue, (((short)Integer.parseInt(data.substring(4, 8), 16))>>4)), true, 100);
                mSeriesGyrZ.appendData(new DataPoint(graph2LastGValue, (((short)Integer.parseInt(data.substring(8), 16))>>4)), true, 100);
                break;
        }
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBleService = ((BLEService.LocalBinder) service).getService();
            if (!mBleService.initialize()) {
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBleService.connect(mDeviceAddress);

            timeoutTimer = new CountDownTimer(40000,100) {
                @Override
                public void onTick(long millisUntilFinished) {
                    statusTimer = true;
                }

                @Override
                public void onFinish() {
                    statusTimer = false;
                    Toast.makeText(DeviceControlActivity.this, R.string.ble_connect_error, Toast.LENGTH_SHORT).show();
                    finish();
                }
            };
            timeoutTimer.start();

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBleService = null;
        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BLEService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                invalidateOptionsMenu();
            } else if (BLEService.ACTION_GATT_DISCONNECTED.equals(action)) {
                if (!statusTimer) {
                    if (timeoutTimer != null) {
                        timeoutTimer.cancel();
                    }
                    mConnected = false;
                    invalidateOptionsMenu();
                    finish();
                } else {
                    if (mBleService != null) {
                        mBleService.connect(mDeviceAddress);
                    }
                }

            } else if (BLEService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                timeoutTimer.cancel();
                //displayGattServices(mBleService.getSupportedGattServices());
                for (BluetoothGattService gattService: mBleService.getSupportedGattServices()) {
                    //Log.i("Service", gattService.getUuid().toString());
                    if (gattService.getUuid().toString().equals("ad11cf40-063f-11e5-be3e-0002a5d5c51b")) {
                        mGattCharacteristics = gattService.getCharacteristics();
                        for (BluetoothGattCharacteristic gattCharacteristic : gattService.getCharacteristics()) {
                            //Log.i("Characteristic", gattCharacteristic.getUuid().toString());

                            if (viewIndex == 3) {
                                mRead = true;
                            } else {
                                mRead = false;
                                if (((gattCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) || ((gattCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0)) {
                                    mBleService.setCharacteristicNotification(gattCharacteristic, true);

                                    try {
                                        Thread.sleep(350);
                                    } catch (InterruptedException e) {
                                    }

                                /*
                                long i=10000000;
                                while (i>0) {
                                    i--;
                                }
                                */
                                }
                            }
                            if (gattCharacteristic.getUuid().toString().equals("bf3fbd80-063f-11e5-9e69-0002a5d5c503"))
                                ledSensor = gattCharacteristic;
                        }

                        ((SeekBar) findViewById(R.id.seekBar)).setEnabled(true);
                        ((SeekArc) findViewById(R.id.seekArc)).setEnabled(true);
                        ((SeekArc) findViewById(R.id.seekArc)).setSelector(true);
                    } else if (gattService.getUuid().toString().equals("f05abac1-3936-11e5-87a6-0002a5d5c51b")) {
                        for (BluetoothGattCharacteristic gattCharacteristic : gattService.getCharacteristics()) {
                            if (((gattCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) || ((gattCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0)) {
                                mBleService.setCharacteristicNotification(gattCharacteristic, true);

                                try {
                                    Thread.sleep(350);
                                } catch (InterruptedException e) {
                                }

                            }
                        }

                    }
                }

                if (mRead) {

                    mTimer = new Runnable() {
                        @Override
                        public void run() {
                            if (mRead) {
                                for (BluetoothGattCharacteristic gattCharacteristic : mGattCharacteristics) {
                                    if ((gattCharacteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                                        mBleService.readCharacteristic(gattCharacteristic);
                                        try {
                                            Thread.sleep(10);
                                        } catch (InterruptedException e) {
                                        }
                                    }
                                }
                                mHandler.postDelayed(this, 0);
                            }
                        }
                    };
                    mHandler.postDelayed(mTimer, 100);

                }


                invalidateOptionsMenu();

            } else if (BLEService.ACTION_DATA_AVAILABLE.equals(action)) {
                //displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                //Log.i("Data:", intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                String[] sensorData = (intent.getStringExtra(BLEService.EXTRA_DATA).split(":"));
                processSensorData(sensorData[0], sensorData[1]);
            }
        }
    };

    private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                    case BluetoothAdapter.STATE_OFF:
                        //case BluetoothAdapter.STATE_TURNING_OFF:
                        finish();
                        break;

                    case BluetoothAdapter.STATE_ON:
                        //case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerReceiver(bluetoothReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());



        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        getActionBar().setTitle(R.string.title_control);
        getActionBar().setDisplayHomeAsUpEnabled(true);


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            mScanResultDep = intent.getByteArrayExtra(EXTRAS_SCAN_RESULT);
            if ((new String(mScanResultDep)).contains(new String(new byte[]{(byte)0xA5, (byte)0xD5, (byte)0xC5, (byte)0x3D}))) {
                viewIndex = 3;
            } else if ((new String(mScanResultDep)).contains(new String(new byte[]{(byte)0xA5, (byte)0xD5, (byte)0xC5, (byte)0x2C}))) {
                viewIndex = 2;
            } else {
                viewIndex = 1;
            }
        }
        else {
            mScanResult = intent.getParcelableExtra(EXTRAS_SCAN_RESULT);
            if (mScanResult.getScanRecord().getManufacturerSpecificData(0x00cd)[17] == 0x3d) {
                viewIndex = 3;
            } else if (mScanResult.getScanRecord().getManufacturerSpecificData(0x00cd)[17] == 0x2c) {
                viewIndex = 2;
            } else {
                viewIndex = 1;
            }
        }


        //((SeekArc) findViewById(R.id.seekArc)).setEnabled(false);

        if (viewIndex == 2){ //mScanResult.getScanRecord().getManufacturerSpecificData(). .toString().endsWith("2C")) { //getServiceUuids().contains(ParcelUuid.fromString("f05abac0-3936-11e5-87a6-0002a5d5c51b"))) {
            setContentView(R.layout.activity_device_control2);
            GraphView graphAcc = (GraphView) findViewById(R.id.accelerometer_graph);
            graphAcc.getViewport().setXAxisBoundsManual(true);
            graphAcc.getViewport().setMinX(0);
            graphAcc.getViewport().setMaxX(100);
            graphAcc.getViewport().setScrollable(true);
            graphAcc.setBackgroundColor(Color.LTGRAY);
            graphAcc.getViewport().setYAxisBoundsManual(true);
            graphAcc.getViewport().setMinY(-2000);
            graphAcc.getViewport().setMaxY(2000);
            StaticLabelsFormatter staticLabelsFormatterX = new StaticLabelsFormatter(graphAcc);
            staticLabelsFormatterX.setHorizontalLabels(new String[] {"", "", ""});
            staticLabelsFormatterX.setVerticalLabels(new String[] {" -2g", " -1g", "0", " +1g", " +2g"});
            graphAcc.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatterX);
            graphAcc.getGridLabelRenderer().setVerticalAxisTitle("Acceleration (m/s2)");
            graphAcc.getGridLabelRenderer().setHorizontalAxisTitle("Time");
            graphAcc.getGridLabelRenderer().setNumVerticalLabels(5);
            graphAcc.getLegendRenderer().setVisible(true);
            graphAcc.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

            mSeriesAccX = new LineGraphSeries<>();
            mSeriesAccX.setTitle("Acceleration X");
            mSeriesAccX.setColor(Color.RED);
            mSeriesAccX.setThickness(5);
            graphAcc.addSeries(mSeriesAccX);

            mSeriesAccY = new LineGraphSeries<>();
            mSeriesAccY.setTitle("Acceleration Y");
            mSeriesAccY.setColor(Color.GREEN);
            mSeriesAccY.setThickness(5);
            graphAcc.addSeries(mSeriesAccY);

            mSeriesAccZ = new LineGraphSeries<>();
            mSeriesAccZ.setTitle("Acceleration Z");
            mSeriesAccZ.setColor(Color.BLUE);
            mSeriesAccZ.setThickness(5);
            graphAcc.addSeries(mSeriesAccZ);


            GraphView graphGyr = (GraphView) findViewById(R.id.gyroscope_graph);
            graphGyr.getViewport().setXAxisBoundsManual(true);
            graphGyr.getViewport().setMinX(0);
            graphGyr.getViewport().setMaxX(100);
            graphGyr.getViewport().setScrollable(true);
            graphGyr.setBackgroundColor(Color.LTGRAY);
            StaticLabelsFormatter staticLabelsFormatterGX = new StaticLabelsFormatter(graphGyr);
            staticLabelsFormatterGX.setHorizontalLabels(new String[] {"", "", ""});
            staticLabelsFormatterGX.setVerticalLabels(new String[] {"-500", "-250", "0", "+250", "+500"});
            graphGyr.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatterGX);
            graphGyr.getViewport().setYAxisBoundsManual(true);
            graphGyr.getViewport().setMinY(-2000);
            graphGyr.getViewport().setMaxY(2000);
            graphGyr.getGridLabelRenderer().setVerticalAxisTitle("Rotation (dps)");
            graphGyr.getGridLabelRenderer().setHorizontalAxisTitle("Time");
            graphGyr.getGridLabelRenderer().setNumVerticalLabels(5);
            graphGyr.getLegendRenderer().setVisible(true);
            graphGyr.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);


            mSeriesGyrX = new LineGraphSeries<>();
            mSeriesGyrX.setTitle("Rotation X");
            mSeriesGyrX.setColor(Color.RED);
            mSeriesGyrX.setThickness(5);
            graphGyr.addSeries(mSeriesGyrX);

            mSeriesGyrY = new LineGraphSeries<>();
            mSeriesGyrY.setTitle("Rotation Y");
            mSeriesGyrY.setColor(Color.GREEN);
            mSeriesGyrY.setThickness(5);
            graphGyr.addSeries(mSeriesGyrY);

            mSeriesGyrZ = new LineGraphSeries<>();
            mSeriesGyrZ.setTitle("Rotation Z");
            mSeriesGyrZ.setColor(Color.BLUE);
            mSeriesGyrZ.setThickness(5);
            graphGyr.addSeries(mSeriesGyrZ);


        } else {
            setContentView(R.layout.activity_device_control);
            //

            ((SeekBar) findViewById(R.id.seekBar)).setEnabled(false);
            ((SeekBar) findViewById(R.id.seekBar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                private int progressFinal;

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    progressFinal = progress;
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                    if (mRead) {
                        mHandler.removeCallbacks(mTimer);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                    }
                    Integer sliderValue = 0x7000 - (progressFinal * (0x7000 / 100));
                    if (sliderValue < 500)
                        sliderValue = 500;

                    if (sliderValue == 0x7000)
                        sliderValue = 0;

                    String param1 = Integer.toHexString(sliderValue);
                    if (param1.length() == 3)
                        param1 = "0" + param1;

                    if (param1.length() == 2)
                        param1 = "00" + param1;

                    if (param1.length() == 1)
                        param1 = "000" + param1;

                    String param2 = Integer.toHexString(sliderValue / 2);
                    if (param2.length() == 3)
                        param2 = "0" + param2;

                    if (param2.length() == 2)
                        param2 = "00" + param2;

                    if (param2.length() == 1)
                        param2 = "000" + param2;

                    param1 = param1 + "," + param2;

                    mBleService.writeCharacteristic(ledSensor, param1.getBytes());

                    if (mRead) {
                        mHandler.postDelayed(mTimer, 20);
                    }
                }
            });


        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent gattServiceIntent = new Intent(this, BLEService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*
        mTimer = new Runnable() {
            @Override
            public void run() {
                //graph2LastXValue += 1d;
                //mSeriesAccX.appendData(new DataPoint(graph2LastXValue, getRandom()), true, 40);
                //mSeriesAccY.appendData(new DataPoint(graph2LastXValue, getRandom()), true, 40);
                //mSeriesAccZ.appendData(new DataPoint(graph2LastXValue, getRandom()), true, 40);
                //mHandler.postDelayed(this, 200);
            }
        };
        mHandler.postDelayed(mTimer, 1000);
        */
    }

    /*
    double mLastRandom = 0;//2;
    Random mRand = new Random();
    private double getRandom() {
        //return mLastRandom += mRand.nextDouble()*0.5 - 0.25;
        return mLastRandom = mRand.nextDouble();//*0.5 - 0.25;
    }
    */

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBleService != null) {
            if (mBleService.serviceStatus) {
                unbindService(mServiceConnection);
            }
            mBleService.close();
            mBleService = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRead) {
            mHandler.removeCallbacks(mTimer);
        }

        unregisterReceiver(bluetoothReceiver);
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        if (ledSensor == null) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(false);
            //menu.findItem(R.id.menu_help).setVisible(false);
            // MBD
            //menu.findItem(R.id.menu_refresh).setActionView(
                   // R.layout.actionbar_indeterminate_progress);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(false);
           // menu.findItem(R.id.menu_help).setVisible(false);
            // MBD
          //  menu.findItem(R.id.menu_refresh).setActionView(null);
        }
        return true;
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BLEService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BLEService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BLEService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BLEService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
}
