/*
 *
 *
 *     BeaconListener.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import com.atmel.beacon.activities.AtmelMainActivity;
import com.atmel.beacon.util.Constants;
import com.atmel.beacon.util.Util;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.service.RangedBeacon;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class BeaconListener implements MonitorNotifier, RangeNotifier {
    private static final String TAG = "BeaconListener";
    private static BeaconListener me;
    private static ArrayList<AtmelBeaconListener> mListeners = new ArrayList<AtmelBeaconListener>();
    private static Context mContext;
    private static BeaconManager beaconManager;
    private static JSONObject jsonBeaconInfo;
    private static String beaconUDID;
    private static boolean isListenerRegistered = false;
    private static HashMap<String, JSONObject> mhmBeaconInfoJsonCache = new HashMap<>();

    static {
        me = new com.atmel.beacon.BeaconListener();
    }

    public static com.atmel.beacon.BeaconListener getInstance() {
        return me;
    }

    public static void addListeners(AtmelBeaconListener listener) {
        if (mListeners.indexOf(listener) < 0) {
            mListeners.add(listener);
        }
    }
    public static void initBeaconManager(AtmelMainActivity activity) {
        beaconManager = BeaconManager.getInstanceForApplication(activity);
        beaconManager.getBeaconParsers().set(0,
                new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25")
        );
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_URL_LAYOUT));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.ALTBEACON_LAYOUT));

        //konkakt?
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:0-3=4c000215,i:4-19,i:20-21,i:22-23,p:24-24"));
        // Detect the telemetry (TLM) frame:
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("x,s:0-1=feaa,m:2-2=20,d:3-3,d:4-5,d:6-7,d:8-11,d:12-15"));


        // Balaji beaconManager.setBackgroundScanPeriod(2000L);          // default is 10000L
        beaconManager.setBackgroundScanPeriod(100L);
        beaconManager.setForegroundBetweenScanPeriod(0L);      // default is 0L
        //beaconManager.setForegroundScanPeriod(750L);          // Default is 1100L
        beaconManager.setForegroundScanPeriod(250L);
        beaconManager.bind(activity);

        try {
            if (beaconManager.isAnyConsumerBound()) {
                beaconManager.updateScanPeriods();
            }
        } catch (RemoteException e) {
        }
    }

    public static void setupBeaconManager(AtmelMainActivity activity) {
        beaconManager = BeaconManager.getInstanceForApplication(activity);
        beaconManager.getBeaconParsers().set(0,
                new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25")
        );

        // Detect the main identifier (UID) frame:
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("s:0-1=feaa,m:2-2=00,p:3-3:-41,i:4-13,i:14-19"));
        // Detect the telemetry (TLM) frame:
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("x,s:0-1=feaa,m:2-2=20,d:3-3,d:4-5,d:6-7,d:8-11,d:12-15"));
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("x,s:0-1=feaa,m:2-2=20,d:3-3,d:4-5,d:6-7,d:8-11,d:12-15"));
        // Detect the URL frame:
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("s:0-1=feaa,m:2-2=10,p:3-3:-41,i:4-20v"));

        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
       /* try {
            beaconManager.setForegroundScanPeriod(750l); // 250 mS
            beaconManager.setForegroundBetweenScanPeriod(10l); // 10ms
            beaconManager.updateScanPeriods();
        }catch (Exception e){
            e.printStackTrace();
        }

        }catch (Exception e){}
*/
        beaconManager.bind(activity);
    }

    public static void releaseBeaconManager(AtmelMainActivity activity) {
        beaconManager.unbind(activity);
        isListenerRegistered = false;
        mhmBeaconInfoJsonCache.clear();
    }

    public static  Region  regionMonitor = null;
    public static  Region  regionRanging = null;

    public static void stopMonitorBeacons() {

        try {
            beaconManager.stopMonitoringBeaconsInRegion(regionMonitor);
            beaconManager.stopRangingBeaconsInRegion(regionRanging);
            regionMonitor = null;
            regionRanging = null;
            isListenerRegistered = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startMonitorBeacons() {
        String uniqueId = null;
        Log.i(TAG, "Bound to service");

        if (isListenerRegistered) {
            return;
        }

        //Eddystone support
            Identifier myBeaconNamespaceId = Identifier.parse("0x2f234454f4911ba9ffa6");
            Identifier myBeaconInstanceId = Identifier.parse("0x000000000001");
            Region region = new Region("my-beacon-region", myBeaconNamespaceId, myBeaconInstanceId, null);

        //Distance callbacks are very slow, to get faster response set expiry time as 5 seconds
        RangedBeacon.setSampleExpirationMilliseconds(2000);

        //TO DO REMOVE
        //loadBeaconInfo(AtmelMainActivity.getActivity().getApplicationContext());

        beaconManager.setMonitorNotifier(BeaconListener.getInstance());
        beaconManager.setRangeNotifier(BeaconListener.getInstance());

        uniqueId = "com.AtmelBeacons";

        regionMonitor = new Region(uniqueId, null, null, null);
        regionRanging = new Region(uniqueId, null, null, null);

        try {
            beaconManager.startMonitoringBeaconsInRegion(regionMonitor);
            beaconManager.startRangingBeaconsInRegion(regionRanging);
            isListenerRegistered = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * Helper Functions Starts
     ***/
    private static void callListenerMethod(String methodName, Object... arg) {
        try {
            for (AtmelBeaconListener listener : mListeners) {
                for (Method declaredMethods : listener.getClass().getDeclaredMethods()) {
                    if (declaredMethods.getName().equalsIgnoreCase(methodName)) {
                        switch (arg.length) {
                            case 1:
                                declaredMethods.invoke(listener, arg[0]);
                                break;

                            case 2:
                                declaredMethods.invoke(listener, arg[0], arg[1]);
                                break;

                            case 3:
                                declaredMethods.invoke(listener, arg[0], arg[1], arg[2]);
                                break;
                        }
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadBeaconInfo(Context context) {
        try {
            String infoFilePath = Util.cacheDirectoryPath + "/" + Constants.BEACON_INFO_FILE_NAME;
            InputStream is = new FileInputStream(infoFilePath);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuffer content = new StringBuffer();
            String line;

            while ((line = br.readLine()) != null) {
                content.append(line + "\n");
            }

            jsonBeaconInfo = new JSONObject(content.toString());
            beaconUDID = jsonBeaconInfo.getString("uuid");

            //Cache beacon details
            mhmBeaconInfoJsonCache.clear();
            try {
                JSONArray jsonBeaconArray = jsonBeaconInfo.getJSONArray("beaconsInfo");
                JSONObject jsonBeacon;

                for (int i = 0; i < jsonBeaconArray.length(); i++) {
                    jsonBeacon = jsonBeaconArray.getJSONObject(i);
                    mhmBeaconInfoJsonCache.put("" + jsonBeacon.getInt("major") + "/" + jsonBeacon.getInt("minor"), jsonBeacon);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

        public static String getBeaconUDID() {
        return beaconUDID;
    }

    public static String getBeaconUrl(Beacon beacon) {
        try {
            JSONArray jsonBeaconArray = jsonBeaconInfo.getJSONArray("beaconsInfo");
            JSONObject jsonBeacon;

            for (int i = 0; i < jsonBeaconArray.length(); i++) {
                jsonBeacon = jsonBeaconArray.getJSONObject(i);

                if (jsonBeacon.getInt("major") == beacon.getIdentifiers().get(1).toInt()
                        && jsonBeacon.getInt("minor") == beacon.getIdentifiers().get(2).toInt()) {
                    return jsonBeacon.getString("content");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Returns content titles configured on against all beacons
     *
     * @return
     */
    public static JSONArray getContentTitles() {
        JSONArray jsonContentTitles = new JSONArray();
        try {
            JSONArray jsonBeaconArray = jsonBeaconInfo.getJSONArray("beaconsInfo");
            JSONObject jsonBeacon;

            //jsonContentTitles.put("None");
            for (int i = 0; i < jsonBeaconArray.length(); i++) {
                jsonBeacon = jsonBeaconArray.getJSONObject(i);
                jsonContentTitles.put(jsonBeacon.getString("contentTitle"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonContentTitles;
    }

    /****
     * BeaconManager Listeneres End
     ****/

    public static JSONObject getByContentTitle(String contentTitle) {
        try {
            JSONArray jsonBeaconArray = jsonBeaconInfo.getJSONArray("beaconsInfo");
            JSONObject jsonBeacon;

            //jsonContentTitles.put("None");
            for (int i = 0; i < jsonBeaconArray.length(); i++) {
                jsonBeacon = jsonBeaconArray.getJSONObject(i);
                if (jsonBeacon.getString("contentTitle").equalsIgnoreCase(contentTitle)) {
                    return jsonBeacon;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static JSONObject getBeaconConfiguration(Beacon beacon) {
        try {
            JSONArray jsonBeaconArray = jsonBeaconInfo.getJSONArray("beaconsInfo");
            JSONObject jsonBeacon;

            for (int i = 0; i < jsonBeaconArray.length(); i++) {
                jsonBeacon = jsonBeaconArray.getJSONObject(i);
                List<Identifier> beaconIdentifiers = beacon.getIdentifiers();

                if (jsonBeacon.getInt("major") == beaconIdentifiers.get(1).toInt()
                        && jsonBeacon.getInt("minor") == beaconIdentifiers.get(2).toInt()) {
                    return jsonBeacon;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void storeConfiguration() {
        try {
            JSONArray jsonBeaconArray = jsonBeaconInfo.getJSONArray("beaconsInfo");
            JSONObject jsonBeacon;

            for (int i = 0; i < jsonBeaconArray.length(); i++) {
                jsonBeacon = jsonBeaconArray.getJSONObject(i);
                //This key added by configuration fragment for its internal purpose, remove it before saving
                jsonBeacon.remove("rssi");

                //Pull temprorary key changes to the appropriate key
                if (jsonBeacon.has("tmpContentTitle")) {
                    jsonBeacon.put("contentTitle", jsonBeacon.getString("tmpContentTitle"));
                    jsonBeacon.remove("tmpContentTitle");
                }

                if (jsonBeacon.has("tmpContent")) {
                    jsonBeacon.put("content", jsonBeacon.getString("tmpContent"));
                    jsonBeacon.remove("tmpContent");
                }
            }

            String infoFilePath = Util.cacheDirectoryPath + "/" + Constants.BEACON_INFO_FILE_NAME;
            FileOutputStream fos = new FileOutputStream(infoFilePath);
            fos.write(jsonBeaconInfo.toString().getBytes());
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getByBeaconInfo(com.atmel.beacon.customcontrols.RadarLayout.BeaconInfo beaconInfo) {
        return mhmBeaconInfoJsonCache.get("" + beaconInfo.max + "/" + beaconInfo.min);
    }

    /****
     * BeaconManager Listeneres Start
     ****/
    @Override
    public void didEnterRegion(Region region) {
        //Log.i(TAG, "I just saw an beacon for the first time!");
        callListenerMethod("didEnterRegion", region);
    }

    @Override
    public void didExitRegion(Region region) {
        //Log.i(TAG, "I no longer see an beacon");
        callListenerMethod("didExitRegion", region);
    }

    @Override
    public void didDetermineStateForRegion(int state, Region region) {
        callListenerMethod("didDetermineStateForRegion", state, region);
        //Log.i(TAG, "I have just switched from seeing/not seeing beacons: " + state);
    }

    @Override
    public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
//        if( beacons.size() == 0 )
//        {
//            return;
//        }

        printBeaconDetails(beacons);

        callListenerMethod("didRangeBeaconsInRegion", beacons, region);
    }

    private void printBeaconDetails(Collection<Beacon> beacons) {
        if (beacons.size() <= 0) {
            return;
        }

//       System.out.println("Beacons Size : " + beacons.size());
        for (Beacon beacon : beacons) {
            for (Identifier id : beacon.getIdentifiers()) {
                System.out.println("Identifier : " + id);
            }
////
//////            System.out.println("Power : " + beacon.getTxPower());
//////            System.out.println("RSSI : " + beacon.getRssi());
////
            Log.i(TAG, "The first beacon I see is about " + beacon.getDistance() + " meters away.");
        }
    }

    public interface AtmelBeaconListener {
        public void didEnterRegion(Region region);

        public void didExitRegion(Region region);

        public void didDetermineStateForRegion(int i, Region region);

        public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region);
    }
    /*** Helper Functions End ***/
}
