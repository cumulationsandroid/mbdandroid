/*
 *
 *
 *     AtmelMainActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.atmel.beacon.R;
import com.atmel.beacon.database.BeaconListDbHandler;
import com.atmel.beacon.datasource.BeaconInformationModel;
import com.atmel.beacon.fragments.MainScreenFragment;
import com.atmel.beacon.util.Constants;
import com.atmel.beacon.util.Util;

import org.altbeacon.beacon.BeaconConsumer;

public class AtmelMainActivity extends AppCompatActivity implements BeaconConsumer {

    protected static final String TAG = "MonitoringActivity";
    private static AtmelMainActivity mActivity;
    public ImageView mImageView;
    public ImageView mConfigureImageView;
    // Flag for enabling/Disabling the proximity state
    public Boolean mProximityState = false;
    private MainScreenFragment mMainScreenFragment;

    /**
     * Replaces the current fragment.
     *
     * @param frag
     * @param tag  to identify fragment
     */
    public static void replaceFragment(Fragment frag, String tag) {
        FragmentTransaction transaction = mActivity.getFragmentManager().beginTransaction();
        transaction.replace(R.id.activityAtmelFragmentContainer, frag, tag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        if (Util.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_atmel_main);
        //insert default beacons to db
        if (!Util.getBooleanSharedPreference(this, Constants.PREF_DEFAULT_BEACON_ADDED)) {
            addDefaultBeacons();
            Util.setBooleanSharedPreference(this, Constants.PREF_DEFAULT_BEACON_ADDED, true);
        }
        //Setup Beacon Manager
        com.atmel.beacon.BeaconListener.initBeaconManager(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Action bar default back action enabling
        try {
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // iBeacon altbeacon configuration mode chooser
        mImageView = (ImageView) findViewById(R.id.add_new);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AtmelMainActivity.this, BeaconListingActivity.class);
                startActivity(intent);
            }
        });

        mConfigureImageView = (ImageView) findViewById(R.id.configure_button);
        mConfigureImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(AtmelMainActivity.this)
                        .setMessage(getString(R.string.eddystone_config_dialog))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(AtmelMainActivity.this, EddystoneScanActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert);

                final AlertDialog dialog = builder.create();
                dialog.show();
                final int version = Build.VERSION.SDK_INT;
                int colour = 0;
                if (version >= 23) {
                    colour = ContextCompat.getColor(AtmelMainActivity.this, R.color.colorPrimary);
                } else {
                    colour = getResources().getColor(R.color.colorPrimary);
                }
                dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(colour);
                dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(colour);
            }
        });

        // main screen fragment used for handling views of RAnge screen ,info screen and Proximity view
        mMainScreenFragment = new MainScreenFragment();
        replaceFragment(mMainScreenFragment, MainScreenFragment.TAG);
    }

    private void addDefaultBeacons() {
        BeaconListDbHandler mBeaconListDbHandler = new BeaconListDbHandler(this);
        BeaconInformationModel beacon = new BeaconInformationModel();
        beacon.mDefault = true;
        beacon.mName = "Atmel_iBeacon";
        beacon.mUuid = "218af652-73e3-40b3-b41c-1953242c72f4";
        beacon.mMajor = "187";
        beacon.mMinor = "69";
        beacon.mUrl = "http://www.atmel.com/technologies/wireless/bluetooth.aspx";
        beacon.mType = Constants.IBEACON_IDEFR;
        mBeaconListDbHandler.addBeacon(beacon);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackAction();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
//        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(true);
    }

    @Override
    public void onStop() {
        super.onStop();
//        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        com.atmel.beacon.BeaconListener.stopMonitorBeacons();
        com.atmel.beacon.BeaconListener.releaseBeaconManager(this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public void onBeaconServiceConnect() {
        com.atmel.beacon.BeaconListener.startMonitorBeacons();
    }

    @Override
    public void onBackPressed() {
        onBackAction();
    }

    /**
     * back action handling for Information view and Proximity view.
     * The action is back to range screen
     */
    private void onBackAction() {
        mImageView.setVisibility(View.VISIBLE);
        mConfigureImageView.setVisibility(View.VISIBLE);
        if (mMainScreenFragment.checkInformationViewVisisbility()) {
            mMainScreenFragment.disableInformationView();
            mImageView.setVisibility(View.VISIBLE);
            mConfigureImageView.setVisibility(View.VISIBLE);
        } else if (mMainScreenFragment.checkProximityViewVisisbility()) {
            mMainScreenFragment.setDistanceImageSelected();
            mProximityState = false;
            mMainScreenFragment.disableProximityView();
            mMainScreenFragment.updateProximityState();
        } else {
            finish();
        }
    }

    /**
     * Open the web URL of the beacon/Eddystone which enters
     * the near region.
     *
     * @param url
     */
    public void showProximityLayout(String url) {
        // Disabling configuration button in proximity view
        mImageView.setVisibility(View.INVISIBLE);
        mConfigureImageView.setVisibility(View.INVISIBLE);
        mMainScreenFragment.showProximityView(url);
    }

    /**
     * Disable proximityLayout when the beacon/Eddystone
     * move away from the region
     * Or user manually select the back button
     */
    public void disableProximityLayout() {
        // Enabling configuration button when back in range screen
        mImageView.setVisibility(View.VISIBLE);
        mImageView.setVisibility(View.VISIBLE);
        mMainScreenFragment.disableProximityView();
    }
}
