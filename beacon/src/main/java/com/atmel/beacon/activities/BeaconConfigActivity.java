/*
 *
 *
 *     BeaconConfigActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.activities;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.atmel.beacon.R;
import com.atmel.beacon.communicators.BLEDataReceiver;
import com.atmel.beacon.util.Constants;
import com.atmel.beacon.util.Util;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Services.EddystoneConfigService;

import java.util.ArrayList;
import java.util.List;

public class BeaconConfigActivity extends AppCompatActivity implements BLEDataReceiver.BLEConnection,
        BLEDataReceiver.GATTProfiles, View.OnClickListener, SwitchCompat.OnCheckedChangeListener {

    public static final int TXLEVEL_LOWEST = -100;
    public static final int TXLEVEL_HIGH = 20;
    public static final int BEACONPERIOD_LOW = 100;
    public static final int BEACONPERIOD_HIGH = 10240;
    private static final int TXMODE_LOWEST = 0;
    private static final int TXMODE_LOW = 1;
    private static final int TXMODE_MEDIUM = 2;
    private static final int TXMODE_HIGH = 3;
    private List<BluetoothGattCharacteristic> mGattCharaList = null;
    private List<BluetoothGattCharacteristic> mGattCharaListImplementedRead = new ArrayList<>();
    private int index = 0;
    private EddystoneConfigService mEddystoneConfigService;
    private SwitchCompat mSwitchCompact;
    private EditText mUrlDataEditText;
    private EditText mUrlFlagsEditText;
    private EditText mBeaconPeriodEditText;
    private TextView mWriteUrlData;
    private TextView mWriteUrlFlags;
    private TextView mWriteTxPowerLevel;
    private TextView mWriteTxPowerMode;
    private TextView mWriteBeaconPeriod;
    private TextView mResetButton;
    private Spinner mTxModeSpinner;
    private EditText mTxLevelLowEditText;
    private EditText mTxLevelLowestEditText;
    private EditText mTxLevelMediumEditText;
    private EditText mTxLevelHighEditText;
    private String TAG = BeaconConfigActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Util.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_beacon_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mSwitchCompact = (SwitchCompat) findViewById(R.id.eddystone_lockswitch);
        mUrlDataEditText = (EditText) findViewById(R.id.eddystone_urldata);
        mUrlFlagsEditText = (EditText) findViewById(R.id.eddystone_flags);
        mBeaconPeriodEditText = (EditText) findViewById(R.id.eddystone_beaconperiod);
        mResetButton = (TextView) findViewById(R.id.eddystone_reset);
        mWriteUrlData = (TextView) findViewById(R.id.eddystone_url_write);
        mWriteUrlFlags = (TextView) findViewById(R.id.eddystone_flags_write);
        mWriteTxPowerLevel = (TextView) findViewById(R.id.eddystone_txpower_level_write);
        mWriteTxPowerMode = (TextView) findViewById(R.id.eddystone_txpower_mode_write);
        mWriteBeaconPeriod = (TextView) findViewById(R.id.eddystone_beaconperiod_write);
        mTxModeSpinner = (Spinner) findViewById(R.id.eddystone_spinner);
        mTxLevelLowEditText = (EditText) findViewById(R.id.eddystone_txlevel_low_edittext);
        mTxLevelLowestEditText = (EditText) findViewById(R.id.eddystone_txlevel_lowest_edittext);
        mTxLevelMediumEditText = (EditText) findViewById(R.id.eddystone_txlevel_medium_edittext);
        mTxLevelHighEditText = (EditText) findViewById(R.id.eddystone_txlevel_high_edittext);
        mWriteUrlData.setOnClickListener(this);
        mWriteUrlFlags.setOnClickListener(this);
        mWriteTxPowerLevel.setOnClickListener(this);
        mWriteTxPowerMode.setOnClickListener(this);
        mWriteBeaconPeriod.setOnClickListener(this);
        mResetButton.setOnClickListener(this);
        mEddystoneConfigService = EddystoneConfigService.getInstance();
        BLEDataReceiver mBLEDataReceiver = new BLEDataReceiver(new Handler());
        BLEDataReceiver.setBLEConnectionListener(this);
        BLEDataReceiver.setGattListener(this);
        BLEConnection.setBLEDataReceiver(mBLEDataReceiver);
        BLEConnection.discoverServices();
    }

    @Override
    protected void onResume() {
        super.onResume();
        BLEDataReceiver.setBLEConnectionListener(this);
        BLEDataReceiver.setGattListener(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Update UI with new values
     */
    public void updateData() {

        if (mEddystoneConfigService != null) {
            mSwitchCompact.setChecked(mEddystoneConfigService.mLockState);

            if (mEddystoneConfigService.mUrlData != null) {
                mUrlDataEditText.setText(mEddystoneConfigService.mUrlData);
            }
            mUrlFlagsEditText.setText(String.valueOf(mEddystoneConfigService.mFlags));
            mBeaconPeriodEditText.setText(String.valueOf(mEddystoneConfigService.mBeaconPeriod));
            mTxLevelLowestEditText.setText(String.valueOf(mEddystoneConfigService.mTxPowerLevels1));
            mTxLevelLowEditText.setText(String.valueOf(mEddystoneConfigService.mTxPowerLevels2));
            mTxLevelMediumEditText.setText(String.valueOf(mEddystoneConfigService.mTxPowerLevels3));
            mTxLevelHighEditText.setText(String.valueOf(mEddystoneConfigService.mTxPowerLevels4));
            mTxModeSpinner.setSelection(mEddystoneConfigService.mTxPowerMode);
            if (mEddystoneConfigService.mLockState) {
                disableWrite();
            }
        }
        mSwitchCompact.setOnCheckedChangeListener(this);
    }

    private void readingData() {
        index = 0;
        if (mEddystoneConfigService != null) {
            mGattCharaList = mEddystoneConfigService.getCharacteristics();
            if (mGattCharaList != null) {
                for (int i = 0; i < mGattCharaList.size(); i++) {
                    String charcteristics = mGattCharaList.get(i).getUuid().toString();
                    Log.d(TAG, "charcteristics " + charcteristics);
                    switch (charcteristics) {
                        case GattAttributes.EddyStoneLockStateCharacteristics:
                            mGattCharaListImplementedRead.add(mGattCharaList.get(i));
                            break;
                        case GattAttributes.EddyStoneURICharacteristics:
                            mGattCharaListImplementedRead.add(mGattCharaList.get(i));
                            break;
                        case GattAttributes.EddyStoneFlagsCharacteristics:
                            mGattCharaListImplementedRead.add(mGattCharaList.get(i));
                            break;
                        case GattAttributes.EddyStoneTxPowerLevelCharacteristics:
                            mGattCharaListImplementedRead.add(mGattCharaList.get(i));
                            break;
                        case GattAttributes.EddyStoneTxModeCharacteristics:
                            mGattCharaListImplementedRead.add(mGattCharaList.get(i));
                            break;
                        case GattAttributes.EddyStoneBeaconPeriodCharacteristics:
                            mGattCharaListImplementedRead.add(mGattCharaList.get(i));
                            break;
                    }
                }
                if (mGattCharaListImplementedRead != null && mGattCharaListImplementedRead.size() > 0 && index < mGattCharaList.size()) {
                    mEddystoneConfigService.readData(mGattCharaListImplementedRead.get(index));
                }
            }
        }
    }

    @Override
    public void onLeDeviceConnected(String deviceName) {

    }

    @Override
    public void onLeServiceDiscovered(boolean status) {
        List<BluetoothGattService> tempServices = BLEConnection.getSupportedGattServices();
        if (mEddystoneConfigService != null && tempServices != null) {
            // Loops through available GATT Services.
            for (int i = 0; i < tempServices.size(); i++) {
                if (tempServices.get(i).getUuid().toString().equalsIgnoreCase(GattAttributes.EddyStoneConfigService)) {
                    mEddystoneConfigService.setGattService(tempServices.get(i));
                    break;
                }
            }
            readingData();
        }
    }

    @Override
    public void onLeDeviceDisconnected() {
        finish();
    }

    @Override
    public void onLePairingRequest() {

    }

    @Override
    public void onLePairingProgressStatus(boolean status) {

    }

    @Override
    public void onLePairingDoneStatus(boolean status) {

    }

    @Override
    public void onLePairingFailedStatus(boolean status) {

    }

    @Override
    public void onLeBluetoothStatusChanged() {


    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {

    }

    @Override
    public void onLeCharacteristicWrite(boolean status) {
        Toast.makeText(this, "Write successful", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLeCharacteristicWrite(boolean status, String characteristics) {
        mSwitchCompact.setOnCheckedChangeListener(null);
        if (mEddystoneConfigService != null) {
            switch (characteristics) {
                case GattAttributes.EddyStoneLockCodeCharacteristics:
                    if (status) {
                        mEddystoneConfigService.mLockState = true;
                        mSwitchCompact.setChecked(true);
                        Toast.makeText(this, R.string.lock_success, Toast.LENGTH_LONG).show();
                    } else {
                        mEddystoneConfigService.mLockState = false;
                        mSwitchCompact.setChecked(false);
                        Toast.makeText(this, R.string.lock_failure, Toast.LENGTH_LONG).show();
                    }
                    break;
                case GattAttributes.EddyStoneUnlockCharacteristics:
                    if (status) {
                        mEddystoneConfigService.mLockState = false;
                        mSwitchCompact.setChecked(false);
                        Toast.makeText(this, R.string.unlock_success, Toast.LENGTH_LONG).show();
                    } else {
                        mEddystoneConfigService.mLockState = true;
                        mSwitchCompact.setChecked(true);
                        Toast.makeText(this, R.string.unlock_failure, Toast.LENGTH_LONG).show();
                    }
                    mSwitchCompact.setChecked(mEddystoneConfigService.mLockState);
                    break;
                case GattAttributes.EddyStoneResetCharacteristics:
                    if (status) {
                        Toast.makeText(this, R.string.eddystone_reset_successful, Toast.LENGTH_SHORT).show();
                        index = 0;
                        if (mGattCharaListImplementedRead != null && mGattCharaListImplementedRead.size() > 0 && index < mGattCharaList.size()) {
                            mEddystoneConfigService.readData(mGattCharaListImplementedRead.get(index));
                        }
                    }
                    break;
                default:
                    Toast.makeText(this, R.string.eddystone_write_successful, Toast.LENGTH_SHORT).show();
                    break;
            }
            mSwitchCompact.setOnCheckedChangeListener(this);
            if (mEddystoneConfigService.mLockState) {
                disableWrite();
            } else {
                enableWrite();
            }
        }
    }


    @Override
    public void onLeCharacteristicRead(boolean status) {
        if (mEddystoneConfigService != null) {
            if (mGattCharaListImplementedRead != null && mGattCharaListImplementedRead.size() > 0 &&
                    index < (mGattCharaListImplementedRead.size() - 1)) {
                index++;
                mEddystoneConfigService.readData(mGattCharaListImplementedRead.get(index));
            } else {
                index = 0;
                updateData();
            }
        }
    }

    @Override
    public void onLeCharacteristicRead(boolean status, String data, int code) {

    }

    @Override
    public void onLeReadRemoteRssi(int rssi) {

    }

    @Override
    public void onLeDescriptorWrite(boolean status) {
    }

    @Override
    public void onLeServicesDiscovered(boolean status, String serviceUuid) {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Boolean error = false;
        hideKeyboard(v);
        if (mEddystoneConfigService != null) {
            if (id == R.id.eddystone_url_write) {
                String url = mUrlDataEditText.getText().toString().trim();
                String[] urlContents = new String[0];
                byte urlPrefix = 0;
                if (url.length() != 0) {
                    if (url.contains(EddystoneConfigService.URI_CONTENT_TYPE1)) {
                        urlContents = url.split(EddystoneConfigService.URI_CONTENT_TYPE1);
                        urlPrefix = (byte) EddystoneConfigService.URI_VALUE_TYPE1;

                    } else if (url.contains(EddystoneConfigService.URI_CONTENT_TYPE2)) {
                        urlContents = url.split(EddystoneConfigService.URI_CONTENT_TYPE2);
                        urlPrefix = (byte) EddystoneConfigService.URI_VALUE_TYPE2;
                    } else if (url.contains(EddystoneConfigService.URI_CONTENT_TYPE3)) {
                        urlContents = url.split(EddystoneConfigService.URI_CONTENT_TYPE3);
                        urlPrefix = (byte) EddystoneConfigService.URI_VALUE_TYPE3;
                    } else if (url.contains(EddystoneConfigService.URI_CONTENT_TYPE4)) {
                        urlContents = url.split(EddystoneConfigService.URI_CONTENT_TYPE4);
                        urlPrefix = (byte) EddystoneConfigService.URI_VALUE_TYPE4;
                    }
                    if (urlContents.length != 2) {
                        Toast.makeText(this, R.string.eddystone_url_incorrect, Toast.LENGTH_LONG).show();
                    } else {
                        if (urlContents[1].getBytes().length <= 17) {
                            mEddystoneConfigService.writeUrlData(urlPrefix, urlContents[1]);
                        } else {
                            Toast.makeText(this, R.string.eddystone_url_incorrect, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            } else if (id == R.id.eddystone_flags_write) {
                if (mUrlFlagsEditText.getText().toString().trim().length() != 0) {
                    mEddystoneConfigService.writeUrlFlags(Integer.parseInt(mUrlFlagsEditText.getText().toString()));
                }
            } else if (id == R.id.eddystone_txpower_mode_write) {
                int data = 0;
                Spinner spinner = (Spinner) findViewById(R.id.eddystone_spinner);
                String text = spinner.getSelectedItem().toString();

                switch (text) {
                    case Constants.EDDYSTONE_TXPOWERMODE_LOWEST:
                        data = TXMODE_LOWEST;
                        break;
                    case Constants.EDDYSTONE_TXPOWERMODE_LOW:
                        data = TXMODE_LOW;
                        break;
                    case Constants.EDDYSTONE_TXPOWERMODE_MEDIUM:
                        data = TXMODE_MEDIUM;
                        break;
                    case Constants.EDDYSTONE_TXPOWERMODE_HIGH:
                        data = TXMODE_HIGH;
                        break;
                    default:
                        break;
                }
                mEddystoneConfigService.writeTxPowerMOde(data);
            } else if (id == R.id.eddystone_beaconperiod_write) {
                if (mBeaconPeriodEditText.getText().toString().trim().length() != 0) {
                    int value = Integer.parseInt(mBeaconPeriodEditText.getText().toString());
                    // Beacon period also support 0
                    if (value == 0 || value >= BEACONPERIOD_LOW && value <= BEACONPERIOD_HIGH) {
                        mEddystoneConfigService.writeBeaconPeriod(Integer.parseInt(mBeaconPeriodEditText.getText().toString()));
                    } else {
                        Toast.makeText(this, R.string.beaconperiod_values_range, Toast.LENGTH_LONG).show();
                    }
                }
            } else if (id == R.id.eddystone_reset) {
                mEddystoneConfigService.reset();
            } else if (id == R.id.eddystone_txpower_level_write) {
                String txLevel1 = mTxLevelLowestEditText.getText().toString().trim();
                String txLevel2 = mTxLevelLowEditText.getText().toString().trim();
                String txLevel3 = mTxLevelMediumEditText.getText().toString().trim();
                String txLevel4 = mTxLevelHighEditText.getText().toString().trim();
                if (txLevel1.length() == 0) {
                    mTxLevelLowestEditText.setError(getResources().getString(R.string.blank_field));
                    error = true;
                }
                if (txLevel2.length() == 0) {
                    mTxLevelLowEditText.setError(getResources().getString(R.string.blank_field));
                    error = true;
                }
                if (txLevel3.length() == 0) {
                    mTxLevelMediumEditText.setError(getResources().getString(R.string.blank_field));
                    error = true;
                }
                if (txLevel4.length() == 0) {
                    mTxLevelHighEditText.setError(getResources().getString(R.string.blank_field));
                    error = true;
                }
                if (!error) {
                    if (Integer.parseInt(txLevel1) >= TXLEVEL_LOWEST && Integer.parseInt(txLevel4) <= TXLEVEL_HIGH) {
                        int txLevel1Value = Integer.parseInt(txLevel1);
                        int txLevel2Value = Integer.parseInt(txLevel2);
                        int txLevel3Value = Integer.parseInt(txLevel3);
                        int txLevel4Value = Integer.parseInt(txLevel4);

                        if (txLevel2Value > txLevel1Value &&
                                txLevel3Value > txLevel2Value &&
                                txLevel4Value > txLevel3Value) {
                            mEddystoneConfigService.writeTxPowerLevel(txLevel1Value, txLevel2Value,
                                    txLevel3Value, txLevel4Value);
                        } else {
                            Toast.makeText(this, R.string.txlevel_values, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(this, R.string.range_txlevel, Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }


    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void handleLockUnlock() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.lock_unlock_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        final EditText password = (EditText) dialogView.findViewById(R.id.eddystone_password);
        dialogBuilder.setTitle(R.string.eddystone_enter_password);


        dialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                mSwitchCompact.setOnCheckedChangeListener(null);
                mSwitchCompact.setChecked(mEddystoneConfigService.mLockState);
                mSwitchCompact.setOnCheckedChangeListener(BeaconConfigActivity.this);
            }
        });
        final AlertDialog dialog = dialogBuilder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        if (mEddystoneConfigService != null) {
                            String passwordCode = password.getText().toString().trim();
                            if (passwordCode.isEmpty()) {
                                Toast.makeText(BeaconConfigActivity.this, "Enter password!", Toast.LENGTH_SHORT).show();
                            } else {
                                if (mEddystoneConfigService.mLockState) {
                                    mEddystoneConfigService.writeUnlockCode(Integer.parseInt(passwordCode));
                                    dialog.dismiss();
                                } else {
                                    mEddystoneConfigService.writeLockCode(Integer.parseInt(passwordCode));
                                }
                                dialog.dismiss();
                            }
                        }
                    }
                });
            }
        });
        dialog.show();

        final int version = Build.VERSION.SDK_INT;
        int colour = 0;
        if (version >= 23) {
            colour = ContextCompat.getColor(this, R.color.colorPrimary);
        } else {
            colour = getResources().getColor(R.color.colorPrimary);
        }
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(colour);
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(colour);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        handleLockUnlock();
    }

    public void disableWrite() {
        mWriteUrlData.setBackgroundResource(R.drawable.button_background_disable);
        mWriteUrlFlags.setBackgroundResource(R.drawable.button_background_disable);
        mWriteTxPowerLevel.setBackgroundResource(R.drawable.button_background_disable);
        mWriteTxPowerMode.setBackgroundResource(R.drawable.button_background_disable);
        mWriteBeaconPeriod.setBackgroundResource(R.drawable.button_background_disable);
        mResetButton.setBackgroundResource(R.drawable.button_background_disable);
    }

    public void enableWrite() {
        mWriteUrlData.setBackgroundResource(R.drawable.button_background);
        mWriteUrlFlags.setBackgroundResource(R.drawable.button_background);
        mWriteTxPowerLevel.setBackgroundResource(R.drawable.button_background);
        mWriteTxPowerMode.setBackgroundResource(R.drawable.button_background);
        mWriteBeaconPeriod.setBackgroundResource(R.drawable.button_background);
        mResetButton.setBackgroundResource(R.drawable.button_background);
    }
}
