/*
 *
 *
 *     BeaconEditActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.activities;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.atmel.beacon.R;
import com.atmel.beacon.customcontrols.InputFilterMinMax;
import com.atmel.beacon.database.BeaconListDbHandler;
import com.atmel.beacon.datasource.BeaconInformationModel;
import com.atmel.beacon.util.Constants;
import com.atmel.beacon.util.Util;

import java.util.ArrayList;

public class BeaconEditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int MAXVALUE_UNSIGNEDINT = 65535;
    public String mName;
    public String mUuid;
    public String mMajor;
    public String mMinor;
    public String mUrl;
    public int mType;
    //To identify opened in edit mode or new insert
    public Boolean mInserMode = false;
    public EditText mNameEditText;
    public EditText mMajorEditText;
    public EditText mMinorEditText;
    public EditText mUrlEditText;
    public TextView mSaveTextView;
    public AutoCompleteTextView mUuidAutoCompleteTextView;
    private BeaconListDbHandler mBeaconListDbHandler;
    private int mBeaconId;
    private ArrayList<String> mUUIDS = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Util.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            int mode = getIntent().getIntExtra(Constants.EXTRA_BEACON_INSERT_EDIT_MODE, 0);
            mType = getIntent().getIntExtra(Constants.EXTRA_BEACON_TYPE, 0);
            mBeaconListDbHandler = new BeaconListDbHandler(this);
            //check type is ibeacon or eddystone
            //if altbeacon set id1,id2 and id3
            if (mType == Constants.ALTBEACON_IDEFR) {
                setContentView(R.layout.activity_beacon_edit_altbeacon);
            } else {
                setContentView(R.layout.activity_beacon_edit_ibeacon);
            }
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            try {
                if (getSupportActionBar() != null)
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mUuidAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.beaconedit_uuid);
            mNameEditText = (EditText) findViewById(R.id.beaconedit_name);
            mMajorEditText = (EditText) findViewById(R.id.beaconedit_major);
            mMajorEditText.setFilters(new InputFilter[]{new InputFilterMinMax(0, MAXVALUE_UNSIGNEDINT)});
            mMinorEditText = (EditText) findViewById(R.id.beaconedit_minor);
            mMinorEditText.setFilters(new InputFilter[]{new InputFilterMinMax(0, MAXVALUE_UNSIGNEDINT)});
            mUrlEditText = (EditText) findViewById(R.id.beaconedit_url);
            mSaveTextView = (TextView) findViewById(R.id.save_details);
            mSaveTextView.setOnClickListener(this);
            if (mode == 1) {
                mBeaconId = getIntent().getIntExtra(Constants.EXTRA_BEACON_ID, 0);
                mName = getIntent().getStringExtra(Constants.EXTRA_BEACON_NAME);
                mUuid = getIntent().getStringExtra(Constants.EXTRA_IBEACON_UUID);
                mMajor = getIntent().getStringExtra(Constants.EXTRA_IBEACON_MAJOR);
                mMinor = getIntent().getStringExtra(Constants.EXTRA_IBEACON_MINOR);
                mUrl = getIntent().getStringExtra(Constants.EXTRA_IBEACON_URL);
                mType = getIntent().getIntExtra(Constants.EXTRA_BEACON_TYPE, 0);
                mNameEditText.setText(mName);
                mUuidAutoCompleteTextView.setText(mUuid);
                mMajorEditText.setText(mMajor);
                mMinorEditText.setText(mMinor);
                mUrlEditText.setText(mUrl);
            } else {
                mInserMode = true;
            }
            if (mType != Constants.ALTBEACON_IDEFR) {
                setSavedBeaconUUIDS();
            }
        }
    }

    private void setSavedBeaconUUIDS() {

        mUuidAutoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {
                // TODO Auto-generated method stub
                mUuidAutoCompleteTextView.showDropDown();
                mUuidAutoCompleteTextView.requestFocus();
                return false;
            }
        });
        mUUIDS = mBeaconListDbHandler.getAllBeaconUUIDs();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(BeaconEditActivity.this, R.layout.dropdown_item_simple, mUUIDS);
        mUuidAutoCompleteTextView.setAdapter(arrayAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Boolean fieldBlank = false;
        if (v.getId() == R.id.save_details) {

            BeaconInformationModel beacon = new BeaconInformationModel();
            if (TextUtils.getTrimmedLength(mNameEditText.getText()) != 0) {
                beacon.mName = mNameEditText.getText().toString();
            } else {
                mNameEditText.setError(getResources().getString(R.string.blank_field));
                fieldBlank = true;
            }

            if (TextUtils.getTrimmedLength(mUuidAutoCompleteTextView.getText()) != 0) {
                beacon.mUuid = mUuidAutoCompleteTextView.getText().toString();
                //check UUID is in correct format
                if (!beacon.mUuid.matches(getString(R.string.uuid_pattern))) {
                    mUuidAutoCompleteTextView.setError(getResources().getString(R.string.incorrect_uuid));
                    return;
                }
            } else {
                mUuidAutoCompleteTextView.setError(getResources().getString(R.string.blank_field));
                fieldBlank = true;
            }

            if (TextUtils.getTrimmedLength(mMajorEditText.getText()) != 0) {
                beacon.mMajor = (mMajorEditText.getText().toString());
                if (Integer.parseInt(beacon.mMajor) > MAXVALUE_UNSIGNEDINT) {
                    mMajorEditText.setError(getResources().getString(R.string.incorrect_value));
                    return;
                }
            } else {
                mMajorEditText.setError(getResources().getString(R.string.blank_field));
                fieldBlank = true;
            }
            if (TextUtils.getTrimmedLength(mMinorEditText.getText()) != 0) {
                beacon.mMinor = (mMinorEditText.getText().toString());
                if (Integer.parseInt(beacon.mMinor) > MAXVALUE_UNSIGNEDINT) {
                    mMinorEditText.setError(getResources().getString(R.string.incorrect_value));
                    return;
                }
            } else {
                mMinorEditText.setError(getResources().getString(R.string.blank_field));
                fieldBlank = true;
            }
            if (TextUtils.getTrimmedLength(mUrlEditText.getText()) != 0) {
                beacon.mUrl = mUrlEditText.getText().toString();
                if (!URLUtil.isValidUrl(beacon.mUrl)) {
                    mUrlEditText.setError(getResources().getString(R.string.invalid_url));
                    return;
                }

              /*  if (!Util.checkURL(beacon.mUrl)) {
                    mUrlEditText.setError(getResources().getString(R.string.invalid_url));
                    return;
                }*/
            } else {
                mUrlEditText.setError(getResources().getString(R.string.blank_field));
                fieldBlank = true;
            }
            if (fieldBlank) {
                return;
            }
            beacon.mType = mType;
            beacon.mBeaconId = mBeaconId;
            int count = mBeaconListDbHandler.getCountOfMatchingBeacons(beacon.mUuid,
                    beacon.mMajor, beacon.mMinor);

            if (mInserMode == true) {
                if (count > 0) {
                    Toast.makeText(BeaconEditActivity.this, R.string.beacon_exists, Toast.LENGTH_SHORT).show();
                    return;
                }
                mBeaconListDbHandler.addBeacon(beacon);
            } else {
                if (count > 0&&mBeaconListDbHandler.getIdOfMatchingBeacon(beacon.mUuid,
                        beacon.mMajor, beacon.mMinor)!=beacon.mBeaconId) {
                    Toast.makeText(BeaconEditActivity.this, R.string.beacon_exists, Toast.LENGTH_SHORT).show();
                    return;
                }
                mBeaconListDbHandler.updateBeacon(beacon);
            }
            finish();
        }
    }
}
