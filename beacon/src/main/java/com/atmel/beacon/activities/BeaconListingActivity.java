/*
 *
 *
 *     BeaconListingActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.atmel.beacon.R;
import com.atmel.beacon.adapters.RecyclerViewBeaconListAdapter;
import com.atmel.beacon.database.BeaconListDbHandler;
import com.atmel.beacon.datasource.BeaconInformationModel;
import com.atmel.beacon.fragments.SelectBeaconTypeFragment;
import com.atmel.beacon.util.Constants;
import com.atmel.beacon.util.Util;

import java.util.List;

public class BeaconListingActivity extends AppCompatActivity implements RecyclerViewBeaconListAdapter.CardClickListener,
        View.OnClickListener {
    private RecyclerView mRecyclerView;
    private RecyclerViewBeaconListAdapter mRecycleViewAdapter;
    private List<BeaconInformationModel> mBeaconList;
    private BeaconListDbHandler mBeaconListDbHandler;
    private ImageView mAddNewBeaconImageView;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       if (Util.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_beacon_listing);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.beacon_recycler_view);
        mAddNewBeaconImageView = (ImageView) findViewById(R.id.add_new);
        mAddNewBeaconImageView.setOnClickListener(this);
        mBeaconListDbHandler = new BeaconListDbHandler(this);
        mBeaconList = mBeaconListDbHandler.getAllBeacons();
        mLayoutManager = new LinearLayoutManager(this);
        mRecycleViewAdapter = new RecyclerViewBeaconListAdapter(mBeaconList,
                this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mRecycleViewAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onInformationEdit();
    }

    @Override
    public void onItemClicked(BeaconInformationModel beaconInfo) {
        Intent intent = new Intent(BeaconListingActivity.this, BeaconEditActivity.class);
        intent.putExtra(Constants.EXTRA_BEACON_ID, beaconInfo.mBeaconId);
        intent.putExtra(Constants.EXTRA_BEACON_NAME, beaconInfo.mName);
        intent.putExtra(Constants.EXTRA_IBEACON_UUID, beaconInfo.mUuid);
        intent.putExtra(Constants.EXTRA_IBEACON_MAJOR, beaconInfo.mMajor);
        intent.putExtra(Constants.EXTRA_IBEACON_MINOR, beaconInfo.mMinor);
        intent.putExtra(Constants.EXTRA_IBEACON_URL, beaconInfo.mUrl);
        intent.putExtra(Constants.EXTRA_BEACON_TYPE, beaconInfo.mType);
        intent.putExtra(Constants.EXTRA_BEACON_INSERT_EDIT_MODE, Constants.EXTRA_BEACON_EDIT_MODE);
        startActivityForResult(intent, Constants.REQUEST_CODE_BEACON_EDIT);
    }

    @Override
    public void onDeleteClicked(BeaconInformationModel beaconInfo) {
        mBeaconListDbHandler.deleteBeacon(beaconInfo);
        mBeaconList.clear();
        mBeaconList = mBeaconListDbHandler.getAllBeacons();

        mRecycleViewAdapter = new RecyclerViewBeaconListAdapter(mBeaconList,
                this);
        //mRecycleViewAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mRecycleViewAdapter);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.add_new) {
            SelectBeaconTypeFragment selectBeaconTypeFragment = new SelectBeaconTypeFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            selectBeaconTypeFragment.show(fragmentManager, SelectBeaconTypeFragment.TAG);
        }
    }


    public void onInformationEdit() {
        mBeaconList = mBeaconListDbHandler.getAllBeacons();
        mRecycleViewAdapter.update(mBeaconList);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRecycleViewAdapter.notifyDataSetChanged();
            }
        });
    }
}
