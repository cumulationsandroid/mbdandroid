/*
 *
 *
 *     EddystoneScanActivity.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.activities;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.atmel.beacon.R;
import com.atmel.beacon.adapters.RecyclerViewEddystoneAdapter;
import com.atmel.beacon.communicators.BLEDataReceiver;
import com.atmel.beacon.customcontrols.PairingAlert;
import com.atmel.beacon.interfaces.AdapterCallBack;
import com.atmel.beacon.util.Util;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Connection.BLEKitKatScanner;
import com.atmel.blecommunicator.com.atmel.Connection.BLELollipopScanner;
import com.atmel.blecommunicator.com.atmel.Interfaces.BLEDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class EddystoneScanActivity extends AppCompatActivity implements BLEDetector, AdapterCallBack,
        BLEDataReceiver.BLEConnection, RecyclerViewEddystoneAdapter.OnDeviceConnectionListener, View.OnClickListener {

    // Stops scanning after 5 seconds.
    private static final long SCAN_PERIOD = 5500;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static BLELollipopScanner mBleLollipopScanner;
    private static BLEKitKatScanner mBleKitkatScanner;
    private static Boolean mReScan = false;
    public BLEDataReceiver mBLEDataReceiver;
    View mCoordinatorLayoutView;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean backButtonPressed = false;
    private String mAddress = null;
    private String mName = null;
    private LinearLayoutManager mLayoutManager;
    private boolean isBleSupported = false;
    private boolean isLollipopDevice = false;
    private RecyclerViewEddystoneAdapter mRecyclerViewEddystoneAdapter;
    private PairingAlert mProgressDialog;
    private boolean isAnimationUp = true;
    private int count = 0;
    private boolean mPairRequestInitiated = false;
    private String mDeviceName = "";
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<BluetoothDevice> mDevices;
    private Handler mDeviceConnectionLossHandler;

    private Handler mHandler = null;
    private Runnable mHandlerTask = null;
    private boolean isFirstTime = false;
    private Timer mTimer = null;
    private TimerTask mTimerTask = null;
    private int i = 0;
    private boolean mScanning;
    private long CONNECTION_TIME_OUT = 10000;
    private String TAG = EddystoneScanActivity.class.getSimpleName();
    private ProgressBar mProgressBar;
    private TextView mCountTextview;
    private TextView mStartStopTextView;
    private RelativeLayout mScandetailsLayout;
    private String EDDYSTONE_CONF_SERVICE_UUID = "ee0c2080-8786-40ba-ab96-99b91ac981d8";
    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            mDevices.clear();
            mRecyclerViewEddystoneAdapter.clearList();
            mRecyclerViewEddystoneAdapter.removeCustomList();
            mRecyclerViewEddystoneAdapter.notifyDataSetChanged();
            scanStart();
            try {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (!backButtonPressed) {
                            mScanning = false;
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }

                }, SCAN_PERIOD);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    /**
     * This runnable for when the device is not connected and progress is showing concurrently.
     */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (mProgressDialog != null) {
                    if (mProgressDialog.isShowing()) {
                        mScanning = false;
                        Toast.makeText(EddystoneScanActivity.this, "Unable to connect the device ..", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismissAlert();
                        BLEConnection.disconnect();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Util.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.eddystone_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mBLEDataReceiver = new BLEDataReceiver(new Handler());
        BLEConnection.setBLEDataReceiver(mBLEDataReceiver);
        mRecyclerView = (RecyclerView) findViewById(R.id.device_recycler_view);
        mCoordinatorLayoutView = findViewById(R.id.snackbarPosition);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBarSmall);
        mCountTextview = (TextView) findViewById(R.id.deviceCountView);
        mStartStopTextView = (TextView) findViewById(R.id.startStopView);
        mScandetailsLayout = (RelativeLayout) findViewById(R.id.scandetails_layout);
        mCoordinatorLayoutView = findViewById(R.id.snackbarPosition);
        mStartStopTextView.setOnClickListener(this);
        mDevices = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerViewEddystoneAdapter = new RecyclerViewEddystoneAdapter(mDevices,
                this);
        mRecyclerViewEddystoneAdapter.setonDeviceConnectedListener(this);
        mRecyclerView.setAdapter(mRecyclerViewEddystoneAdapter);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        mProgressDialog = new PairingAlert(this);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
            BLELollipopScanner.setScanListner(this);
            isLollipopDevice = true;
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
            BLEKitKatScanner.setScanListner(this);
            isLollipopDevice = false;
        }

        if (isLollipopDevice) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                showSnackBar();
            } else {
                if (mBleLollipopScanner.checkBLESupport()) {
                    isBleSupported = true;
                    mDevices.clear();
                    mRecyclerViewEddystoneAdapter.clearList();
                    mRecyclerViewEddystoneAdapter.removeCustomList();
                } else {
                    isBleSupported = false;

                }
            }
        } else {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                showSnackBar();
            } else {
                if (mBleKitkatScanner.checkBLESupport()) {
                    isBleSupported = true;
                    mDevices.clear();
                    mRecyclerViewEddystoneAdapter.clearList();
                    mRecyclerViewEddystoneAdapter.removeCustomList();
                } else {
                    isBleSupported = false;
                }
            }
        }
        scanStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * To start LEScan
     */
    private void scanStart() {
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            if (isBleSupported) {
                mScanning = true;
                mScandetailsLayout.setVisibility(View.VISIBLE);
                mDevices.clear();
                mRecyclerViewEddystoneAdapter.clearList();
                mRecyclerViewEddystoneAdapter.removeCustomList();
                mRecyclerViewEddystoneAdapter.notifyDataSetChanged();
                //}
                mRecyclerView.setVisibility(View.VISIBLE);
                setScanProgressbar(true, View.VISIBLE, R.string.stop_scan);
                if (isLollipopDevice) {
                    scanLollipopDevice();
                } else {
                    scanNonLollipopDevices();
                }
            } else {
                showSnackBarBleunsupported();
            }
        } else {
            // show message to activate Bluetooth
            showSnackBar();
        }
        this.invalidateOptionsMenu();
    }


    /***
     * scanning for lollipop devices
     ***/
    private void scanLollipopDevice() {
       /* String[] UUIDs = new String[1];
        UUIDs[0] = "ee0c2080-8786-40ba-ab96-99b91ac981d8";*/
        if (null != mBleLollipopScanner) {
            mBleLollipopScanner.startLEScan(SCAN_PERIOD);
        } else {
            // Do Nothing
        }

    }


    /***
     * Scanning non lollipop devices
     */
    private void scanNonLollipopDevices() {

        if (null != mBleKitkatScanner) {
/*
                *//*
                if (baseActivity.UUIDs != null && baseActivity.UUIDs.length > 0) {
                    mBleKitkatScanner.startLEScanWithFilter(SCAN_PERIOD, baseActivity.UUIDs);
                } else {*//*
            mBleKitkatScanner.startLEScan(SCAN_PERIOD);
            //  }*/
//            String[] UUIDs = new String[1];
//            UUIDs[0] = "ee0c2080-8786-40ba-ab96-99b91ac981d8";
//            mBleKitkatScanner.startLEScanWithFilter(SCAN_PERIOD, UUIDs);
            mBleKitkatScanner.startLEScan(SCAN_PERIOD);

        } else {

            mBleKitkatScanner.startLEScan(SCAN_PERIOD);
        }

    }


    private void stopScan() {
        mScanning = false;
        setScanProgressbar(false, View.GONE, R.string.start_scan);
        setCountView();
        if (isBleSupported) {
            if (isLollipopDevice) {
                if (null != mBleLollipopScanner) {
                    mBleLollipopScanner.stopLEScan();
                } else {
                    mBleLollipopScanner = BLELollipopScanner.getInstance(this);
                    BLELollipopScanner.setScanListner(this);
                    mBleLollipopScanner.stopLEScan();
                }
            } else {
                if (null != mBleKitkatScanner) {
                    mBleKitkatScanner.stopLEScan();
                } else {
                    mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
                    BLEKitKatScanner.setScanListner(this);
                    mBleKitkatScanner.stopLEScan();
                }
            }
        }
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }


    /**
     *
     */
    private void showSnackBar() {
        mRecyclerView.setVisibility(View.GONE);
        Snackbar.make(mCoordinatorLayoutView,
                getString(R.string.bluetooth_message), Snackbar.LENGTH_LONG).setAction(getString(R.string.turn_on),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mBluetoothAdapter.enable();
                    }
                }).show();
    }

    private void showSnackBarBleunsupported() {
        Snackbar.make(mCoordinatorLayoutView,
                getString(R.string.bluetooth_unsupported_message), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        BLEDataReceiver.setBLEConnectionListener(this);
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mRecyclerView != null) {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    if (mRecyclerViewEddystoneAdapter != null) {
                        mRecyclerView.setAdapter(mRecyclerViewEddystoneAdapter);
                    }
                }
            }
        });*/
        permissionCheck();
    }

    private void permissionCheck() {
        //Marshmallow permission check
        if (!checkPermission()) {
            requestPermission();
        } else {
            if (mDevices.isEmpty()) {
                scanStart();
            } else {
                mRecyclerViewEddystoneAdapter.notifyDataSetChanged();
                if (isLollipopDevice) {
                    if (!mBleLollipopScanner.getBluetoothStatus()) {
                        onLeBluetoothStatusChanged();
                    }
                } else {
                    if (!mBleKitkatScanner.getBluetoothStatus()) {
                        onLeBluetoothStatusChanged();
                    }
                }
                backButtonPressed = false;
                //  mPairDone=false;

                if (mBleKitkatScanner != null) {
                    BLEKitKatScanner.setScanListner(this);
                }
                if (mBleLollipopScanner != null) {
                    BLELollipopScanner.setScanListner(this);
                }

                if (mDevices.isEmpty()) {
                    mReScan = false;
                    scanStart();
                }
                mReScan = false;
            }
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            ActivityCompat.requestPermissions(EddystoneScanActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
            //Toast.makeText(this, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(mCoordinatorLayoutView, "Permission Granted, Now you can access location data.", Snackbar.LENGTH_LONG).show();
                    if (mBluetoothAdapter != null) {
                        if (!mBluetoothAdapter.isEnabled()) {
                            showSnackBar();
                        }
                    }
                } else {
                    Snackbar.make(mCoordinatorLayoutView, "Permission Denied, You cannot access location data.", Snackbar.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // this.finish();
                        }
                    }, 1000);
                }
                break;
        }
    }

    @Override
    public void onLeDeviceConnected(final String deviceName) {
        // //Log.e("OnLeDevice connected", "Callbacked");
        mProgressDialog.showAlert();
        mDeviceName = deviceName;
        if (mDeviceConnectionLossHandler != null && runnable != null) {
            mDeviceConnectionLossHandler.removeCallbacks(runnable);
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (!mPairRequestInitiated) {
                        //notifyCustomSerialChat();
                        //  startService();
                        showMessage(deviceName);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1500);

    }


    private void showMessage(String deviceName) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.connect_success));
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    mProgressDialog.dismissAlert();
                    Bundle bundle = new Bundle();
                    mScandetailsLayout.setVisibility(View.GONE);
                    //bundle.putSerializable(AppConstants.GATTSERVER_PROFILES, category);
                    Intent serviceIntent = new Intent(EddystoneScanActivity.this, BeaconConfigActivity.class);
                    // serviceIntent.putExtra(AppConstants.GATTSERVER_PROFILES, bundle);
                    startActivity(serviceIntent);
                    backButtonPressed = true;

                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void leDeviceDetected(final BluetoothDevice mDevice, final int rssi, final List<ParcelUuid> list) {
        final List<ParcelUuid> filteredList = new ArrayList<ParcelUuid>();
        ParcelUuid esUuid = ParcelUuid.fromString(EDDYSTONE_CONF_SERVICE_UUID);
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).equals(esUuid)) {
                    filteredList.add(list.get(i));
                }
            }
        }
        if (filteredList.isEmpty()) {
            // No Eddystone URL-Configuration service is found
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRecyclerViewEddystoneAdapter.addDevices(mDevice, rssi, filteredList);
                setCountView();
            }
        });
    }


    @Override
    public void leScanStopped() {
        if (mScanning) {
            mScanning = false;
            setScanProgressbar(false, View.GONE, R.string.start_scan);
            setCountView();
            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    @Override
    public void onDeviceConnected(String address, String name, List<ParcelUuid> uuidList) {
        if (mProgressDialog != null) {
            stopScan();
            BLEDataReceiver.setBLEConnectionListener(this);
            mProgressDialog.setMessage(getResources().getString(R.string.connecting_alert));
            mProgressDialog.showAlert();
        }
        count = 0;
        //category = AppConstants.ADV_CATEGORY.DEFAULT;
        mAddress = address;
        mName = name;

        BLEConnection.connect(address);
        mDeviceConnectionLossHandler = new Handler();
        mDeviceConnectionLossHandler.postDelayed(runnable, CONNECTION_TIME_OUT);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mBleKitkatScanner != null) {
            BLEKitKatScanner.setScanListner(null);
        }
        if (mBleLollipopScanner != null) {
            BLELollipopScanner.setScanListner(null);
        }
        mReScan = true;
    }


    @Override
    public void onLeBluetoothStatusChanged() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                BlueToothOnorOFFUIChange();
            }
        }, 2000);

    }

    private void BlueToothOnorOFFUIChange() {
        if (isLollipopDevice) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showSnackBar();
                    }
                });
            } else {
                isBleSupported = mBleLollipopScanner.checkBLESupport();
            }
        } else {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isAnimationUp = false;
                        isFirstTime = false;
                        showSnackBar();
                    }
                });
            } else {
                isBleSupported = mBleKitkatScanner.checkBLESupport();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BLEConnection.disconnect();
        BLEDataReceiver.setBLEConnectionListener(null);
        if (mHandler != null) {
            mHandler.removeCallbacks(mHandlerTask);
        }
    }


    @Override
    public void onLeServiceDiscovered(boolean status) {

    }

    @Override
    public void onLeDeviceDisconnected() {
        mScandetailsLayout.setVisibility(View.VISIBLE);
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
        scanStart();
    }

    @Override
    public void onLePairingRequest() {

    }

    @Override
    public void onLePairingProgressStatus(boolean status) {
        mPairRequestInitiated = true;
        try {
            if (mProgressDialog != null) {
                mProgressDialog.setMessage(getResources().getString(R.string.eddystone_pair_inprogress));
                mProgressDialog.showAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLePairingDoneStatus(boolean status) {
        mProgressDialog.showAlert();
        mProgressDialog.setMessage(getResources().getString(R.string.eddystone_pair_done));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismissAlert();
                        if (mPairRequestInitiated) {
                            mPairRequestInitiated = false;
                            //notifyCustomSerialChat();
                            //startService();
                            showMessage(mDeviceName);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500);
    }

    @Override
    public void onLePairingFailedStatus(boolean status) {
        try {
            mProgressDialog.setMessage(getResources().getString(R.string.eddystone_pair_none));
            mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLeServicesDiscovered(boolean status, String serviceUuid) {
    }

    @Override
    public void onMethodCallback() {
        goIfAlreadyConnected();
    }

    private void goIfAlreadyConnected() {
        Bundle bundle = new Bundle();
        Intent serviceIntent = new Intent(EddystoneScanActivity.this, BeaconConfigActivity.class);
        startActivity(serviceIntent);
        backButtonPressed = true;
    }

    /**
     * @param indeterminate
     * @param visibility
     */
    public void setScanProgressbar(boolean indeterminate, int visibility, int label) {
        mStartStopTextView.setText(label);
        mProgressBar.setIndeterminate(indeterminate);
        mProgressBar.setVisibility(visibility);
        if (indeterminate) {
            mCountTextview.setVisibility(View.GONE);
        }
    }

    public void setCountView() {
        int itemCount = mRecyclerViewEddystoneAdapter.getItemCount();
        mCountTextview.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        if (itemCount == 0) {
            mCountTextview.setText(getString(R.string.no_device_found));
        } else {
            mCountTextview.setText(itemCount + " " + getString(R.string.device_found));
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.startStopView) {
            if (mScanning) {
                stopScan();
            } else {
                scanStart();
            }
        }
    }
}
