/*
 *
 *
 *     RecyclerViewBeaconListAdapter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */


package com.atmel.beacon.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.atmel.beacon.R;
import com.atmel.beacon.datasource.BeaconInformationModel;
import com.atmel.beacon.util.Constants;

import java.util.HashMap;
import java.util.List;


public class RecyclerViewBeaconListAdapter extends RecyclerView.Adapter<RecyclerViewBeaconListAdapter.BeaconViewHolder> {

    public CardClickListener mCardClickListener;
    private Activity mContext;
    private List<BeaconInformationModel> mBeaconList;
    private HashMap<String, Integer> mHashMap;

    public RecyclerViewBeaconListAdapter(List<BeaconInformationModel> mList, Activity context) {
        this.mBeaconList = mList;
        mContext = context;
        try {
            this.mCardClickListener = ((CardClickListener) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    @Override
    public int getItemCount() {
        return mBeaconList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void add(BeaconInformationModel beacon, int position) {
        mBeaconList.add(position, beacon);
        notifyItemInserted(position);
    }

    public void update(List<BeaconInformationModel> list) {
        mBeaconList = list;
        //notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final BeaconViewHolder beaconHolder, final int position) {
        final BeaconInformationModel beacon = mBeaconList.get(position);
        if (beacon.mType == Constants.IBEACON_IDEFR) {
            beaconHolder.mBeaconName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ibeacon_icon, 0, 0, 0);
        } else {
            beaconHolder.mBeaconName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.altbeacon_icon, 0, 0, 0);
        }
        beaconHolder.mBeaconName.setText(beacon.mName);
        String sourceStringUuid = "<b>" + mContext.getResources().getString(R.string.beaconedit_uuid) +
                "  : " + "</b> " + beacon.mUuid;
        beaconHolder.mUuid.setText(Html.fromHtml(sourceStringUuid));
        String sourceStringMajor = "<b>" + mContext.getResources().getString(R.string.beaconedit_major) +
                "  : " + "</b> " + String.valueOf(beacon.mMajor);
        beaconHolder.mMajor.setText(Html.fromHtml(sourceStringMajor));
        String sourceStringMinor = "<b>" + mContext.getResources().getString(R.string.beaconedit_minor) +
                "  : " + "</b> " + String.valueOf(beacon.mMinor);
        beaconHolder.mMinor.setText(Html.fromHtml(sourceStringMinor));
        String sourceStringUrl = "<b>" + mContext.getResources().getString(R.string.beacon_url) +
                "  : " + "</b> " + String.valueOf(beacon.mUrl);
        beaconHolder.mUrl.setText(Html.fromHtml(sourceStringUrl));
        if (beacon.mDefault) {
            beaconHolder.mDeleteButton.setVisibility(View.INVISIBLE);
        } else {
            beaconHolder.mDeleteButton.setVisibility(View.VISIBLE);
        }

        beaconHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCardClickListener.onItemClicked(mBeaconList.get(position));
            }
        });

        beaconHolder.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askConfirmation(position);
            }
        });
    }


    /**
     * Asking confirmation for delete action
     */

    private void askConfirmation(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle("Delete beacon");
        builder.setMessage("Are you sure?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                mCardClickListener.onDeleteClicked(mBeaconList.get(position));
                dialog.dismiss();
            }

        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public BeaconViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.beacon_list_row, viewGroup, false);
        return new BeaconViewHolder(itemView);
    }

    public interface CardClickListener {
        void onItemClicked(BeaconInformationModel beaconInfo);

        void onDeleteClicked(BeaconInformationModel beaconInfo);
    }

    public static class BeaconViewHolder extends RecyclerView.ViewHolder {
        protected TextView mBeaconName;
        protected TextView mUuid;
        protected TextView mMajor;
        protected TextView mMinor;
        protected ImageButton mDeleteButton;
        protected TextView mUrl;
        protected CardView cardView;

        public BeaconViewHolder(View v) {
            super(v);
            mBeaconName = (TextView) v.findViewById(R.id.beacon_name);
            mUuid = (TextView) v.findViewById(R.id.uid);
            mMajor = (TextView) v.findViewById(R.id.major);
            mMinor = (TextView) v.findViewById(R.id.minor);
            cardView = (CardView) v.findViewById(R.id.card_view);
            mDeleteButton = (ImageButton) v.findViewById(R.id.delete);
            mUrl = (TextView) v.findViewById(R.id.url);
        }
    }

}
