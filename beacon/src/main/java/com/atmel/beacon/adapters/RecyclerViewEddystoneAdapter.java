/*
 *
 *
 *     RecyclerViewEddystoneAdapter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */


package com.atmel.beacon.adapters;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.ParcelUuid;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.atmel.beacon.R;
import com.atmel.beacon.interfaces.AdapterCallBack;
import com.atmel.beacon.util.Constants;
import com.atmel.beacon.util.Util;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecyclerViewEddystoneAdapter extends RecyclerView.Adapter<RecyclerViewEddystoneAdapter.DeviceViewHolder> {

    private Activity mContext;
    private List<BluetoothDevice> mDeviceList;
    private List<Device> mCustomDeviceList = new ArrayList<>();
    private List<BluetoothDevice> mDeviceListSecond;
    private HashMap<String, Integer> mHashMap;
    private OnDeviceConnectionListener mListener;
    private AdapterCallBack mAdapterCallback;

    public RecyclerViewEddystoneAdapter(List<BluetoothDevice> mList, Activity context) {
        this.mDeviceList = mList;
        this.mDeviceListSecond = new ArrayList<>();
        this.mDeviceListSecond.addAll(mList);
        this.mContext = context;
        this.mHashMap = new HashMap<>();
        try {
            this.mAdapterCallback = ((AdapterCallBack) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    public void removeCustomList() {
        if (mCustomDeviceList != null && mCustomDeviceList.size() > 0) {
            mCustomDeviceList.clear();
        }
    }

    @Override
    public int getItemCount() {
        return mDeviceList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void add(BluetoothDevice device, int position) {
        mDeviceList.add(position, device);
        notifyItemInserted(position);
    }

    @Override
    public void onBindViewHolder(final DeviceViewHolder contactViewHolder, final int pos) {
        final BluetoothDevice device = mDeviceList.get(pos);
        final String deviceName = device.getName();
        final String deviceAddress = device.getAddress();
        contactViewHolder.deviceAddress.setText(deviceAddress);
        if (deviceName != null && deviceName.length() > 0) {
            contactViewHolder.deviceName.setText(deviceName);
            if (Util.getConnectionState(device.getAddress(), mContext)) {
                contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.disconnect));
            } else {
                contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
            }
        } else {
            contactViewHolder.deviceName.setText(mContext.getResources().getString(R.string.unknown));
            contactViewHolder.deviceName.setSelected(true);
            //contactViewHolder.deviceAddress.setText(mDeviceList.get(pos).getAddress());
        }
        if (BLEConnection.getDevice() == null) {
            contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
        } else {
            if (BLEConnection.getmConnectionState() == BLEConnection.STATE_CONNECTED) {
                if (BLEConnection.getDevice().getAddress().equals(device.getAddress())) {
                    contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.disconnect));
                }
            }
        }

        contactViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ParcelUuid> uuid = null;
                try {
                    uuid = mCustomDeviceList.get(pos).uuid;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (BLEConnection.getDevice() != null) {
                    if (BLEConnection.getmConnectionState() == BLEConnection.STATE_CONNECTED) {
                        if (BLEConnection.getDevice().getAddress().equals(device.getAddress())) {
                            mAdapterCallback.onMethodCallback();
                        } else {
                            BLEConnection.disconnect();
                        }
                        //mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                    } else {
                        mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                    }
                } else {
                    Util.setStringSharedPreference(mContext,
                            Constants.PREF_DEV_ADDRESS, "");
                    Util.setIntSharedPreference(mContext,
                            Constants.PREF_CONNECTION_STATE, 0);
                    contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
                    mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                }

            }
        });

        contactViewHolder.mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ParcelUuid> uuid = null;
                try {
                    uuid = mCustomDeviceList.get(pos).uuid;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (BLEConnection.getDevice() != null) {
                    if (BLEConnection.getmConnectionState() == BLEConnection.STATE_CONNECTED) {
                        BLEConnection.disconnect();
                        //mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                    } else {
                        mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                    }
                } else {
                    Util.setStringSharedPreference(mContext,
                            Constants.PREF_DEV_ADDRESS, "");
                    Util.setIntSharedPreference(mContext,
                            Constants.PREF_CONNECTION_STATE, 0);
                    contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
                    mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                }
            }
        });
    }


    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.eddystone_list_row, viewGroup, false);
        return new DeviceViewHolder(itemView);
    }


    public void addDevices(BluetoothDevice mDevice, int rssi, List<ParcelUuid> uuidList) {
        if (!mDeviceList.contains(mDevice)) {
            mDeviceList.add(mDevice);
            mDeviceListSecond.add(mDevice);
            mHashMap.put(mDevice.getAddress(), rssi);
            if (mCustomDeviceList != null) {
                Device device = new Device();
                device.device = mDevice;
                device.rssi = rssi;
                device.uuid = uuidList;
                mCustomDeviceList.add(device);
            }
            notifyDataSetChanged();
        } else {
            mHashMap.put(mDevice.getAddress(), rssi);
            notifyDataSetChanged();
        }

    }

    public void clearList() {
        mDeviceListSecond.clear();
    }

    public void setonDeviceConnectedListener(OnDeviceConnectionListener listener) {
        this.mListener = listener;
    }

    /**
     * Setting call back for getting
     */
    public void setAdapterCallback(AdapterCallBack callback) {
        this.mAdapterCallback = callback;
    }


    public interface OnDeviceConnectionListener {
        void onDeviceConnected(String address, String name, List<ParcelUuid> uuidList);
    }

    public static class DeviceViewHolder extends RecyclerView.ViewHolder {
        protected TextView deviceName;
        protected TextView signalStrengthValue;
        protected TextView deviceAddress;
        protected CardView cardView;
        protected ImageView mSignalStrengthImageView;
        protected ImageView mDeviceImageView;
        protected TextView mConnectButton;

        public DeviceViewHolder(View v) {
            super(v);
            deviceName = (TextView) v.findViewById(R.id.ble_device_name);
            cardView = (CardView) v.findViewById(R.id.card_view);
            mConnectButton = (TextView) v.findViewById(R.id.button_connect_disconnect);
            deviceAddress = (TextView) v.findViewById(R.id.ble_device_address);
        }
    }

    public static class Device {
        BluetoothDevice device;
        int rssi;
        List<ParcelUuid> uuid;
        int mConnectionState;
    }

}
