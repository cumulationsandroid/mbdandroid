/*
 *
 *
 *     BLEDataReceiver.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.communicators;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import com.atmel.beacon.util.Constants;

public class BLEDataReceiver extends ResultReceiver {

    public static BLEConnection mConnectionListener;
    public static GATTProfiles mGattListener;

    private String TAG = BLEDataReceiver.class.getSimpleName();

    public BLEDataReceiver(Handler handler) {
        super(handler);
    }

    public static void setBLEConnectionListener(BLEConnection listener) {
        mConnectionListener = listener;

    }

    public static void setGattListener(GATTProfiles gattListener) {
        mGattListener = gattListener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        super.onReceiveResult(resultCode, resultData);
        switch (resultCode) {
            case Constants.DEVICE_CONNECTED_KEY:

                String connectedDevice = resultData.getString(Constants.DEVICE_CONNECTED_NAME_KEY);
                /* if (mLeDeviceConnected != null) {
                    mLeDeviceConnected.onLeDeviceConnected(connectedDevice);
                }*/
                if (mConnectionListener != null) {
                    mConnectionListener.onLeDeviceConnected(connectedDevice);
                }
                break;
            case Constants.SERVICE_DISCOVERED_KEY:
                boolean status = resultData.getBoolean(Constants.SERVICE_DISCOVERED_STATUS_KEY);
                if (mConnectionListener != null)
                    mConnectionListener.onLeServiceDiscovered(status);
                break;
            case Constants.CHARACTERISTICS_CHANGED_KEY:
                boolean changestatus = resultData.getBoolean(
                        Constants.CHARACTERISTIC_CHANGE_STATUS_KEY);
                if (mGattListener != null)
                    mGattListener.onLeCharacteristicChanged(changestatus);
                break;
            case Constants.CHARACTERISTICS_WRITE_KEY:
                int writestatus = resultData.getInt(
                        com.atmel.blecommunicator.com.atmel.Attributes.Constants.CHARACTERISTIC_WRITE_STATUS_KEY);
                Boolean statusValue = false;
                if (writestatus == Constants.GATT_SUCCESS) {
                    statusValue = true;
                }
                if (mGattListener != null) {
                    String characteristics = resultData.getString(com.atmel.blecommunicator.com.atmel.Attributes.Constants.CHARACTERISTIC_WRITE_STATUS_CODE, null);
                    if (characteristics != null) {
                        mGattListener.onLeCharacteristicWrite(statusValue, characteristics);
                    } else {
                        mGattListener.onLeCharacteristicWrite(statusValue);
                    }
                }
                break;
            case Constants.DEVICE_PAIR_IN_PROCESS_KEY:
                boolean pairInprogress = resultData.getBoolean(
                        Constants.DEVICE_PAIRING_STATUS_IN_PROCESS_KEY);
                if (mConnectionListener != null) {
                    mConnectionListener.onLePairingProgressStatus(pairInprogress);

                }
                break;
            case Constants.DEVICE_PAIR_DONE_KEY:

                boolean pairDone = resultData.getBoolean(
                        Constants.DEVICE_PAIRING_STATUS_DONE_KEY);
                if (mConnectionListener != null) {
                    mConnectionListener.onLePairingDoneStatus(pairDone);

                }
                break;
            case Constants.DEVICE_PAIR_FAILED_KEY:

                boolean pairFailed = resultData.getBoolean(
                        Constants.DEVICE_PAIRING_STATUS_FAILED_KEY);
                if (mConnectionListener != null) {
                    mConnectionListener.onLePairingFailedStatus(pairFailed);
                }
                break;
            case Constants.CHARACTERISTICS_READ_KEY:

                boolean readinfo = resultData.getBoolean(Constants.
                        CHARACTERISTIC_READ_STATUS_KEY);
                if (mGattListener != null) {
                    if (resultData.getString(Constants.CHARACTERISTIC_READ_DATA) != null) {
                        String readData = resultData.getString(Constants.
                                CHARACTERISTIC_READ_DATA);
                        int code = resultData.getInt(Constants.
                                CHARACTERISTIC_READ_DATA_CODE);
                        mGattListener.onLeCharacteristicRead(readinfo, readData, code);
                    } else {
                        mGattListener.onLeCharacteristicRead(readinfo);
                    }

                }
                break;
            case Constants.REMOTE_RSSI_KEY:

                int rssi = resultData.getInt(Constants.REMOTE_RSSI_VALUE);
                if (mGattListener != null) {
                    mGattListener.onLeReadRemoteRssi(rssi);
                }
                break;
            case Constants.DEVICE_DISCONNECTED_KEY:

                if (mConnectionListener != null) {
                    mConnectionListener.onLeDeviceDisconnected();
                }
                break;
            case Constants.DEVICE_BLUETOOTH_ON_KEY:
                if (mConnectionListener != null) {
                    mConnectionListener.onLeBluetoothStatusChanged();
                }
                break;
            case Constants.DEVICE_BLUETOOTH_OFF_KEY:
                if (mConnectionListener != null) {
                    mConnectionListener.onLeBluetoothStatusChanged();
                }
                break;
            case Constants.DEVICE_PAIR_REQUEST:

                if (mConnectionListener != null) {
                    mConnectionListener.onLePairingRequest();
                }
                break;

            case Constants.DESCRIPTOR_WRITE_KEY:
                boolean notifyStatus = resultData.getBoolean(
                        Constants.DESCRIPTOR_WRITE_STATUS_KEY);
                if (mGattListener != null)
                    mGattListener.onLeDescriptorWrite(notifyStatus);
                break;

            default:
                break;

        }
    }

    public interface BLEConnection {
        //BLE connection Related
        void onLeDeviceConnected(String deviceName);

        void onLeServiceDiscovered(boolean status);

        void onLeDeviceDisconnected();

        //BLE paring related
        void onLePairingRequest();

        void onLePairingProgressStatus(boolean status);

        void onLePairingDoneStatus(boolean status);

        void onLePairingFailedStatus(boolean status);

        //Fro Bluetooth Status
        void onLeBluetoothStatusChanged();

        void onLeServicesDiscovered(boolean status, String serviceUuid);


    }

    public interface GATTProfiles {
        void onLeCharacteristicChanged(boolean status);

        void onLeCharacteristicWrite(boolean status);

        void onLeCharacteristicWrite(boolean status, String characteristics);

        void onLeCharacteristicRead(boolean status);

        void onLeCharacteristicRead(boolean status, String data, int code);

        void onLeReadRemoteRssi(int rssi);

        void onLeDescriptorWrite(boolean status);

    }

}
