/*
 *
 *
 *     AtmelImageButton.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.customcontrols;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.atmel.beacon.R;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

public class AtmelImageButton extends LinearLayout {
    private SVG svgImage;
    private SVG svgSelectedImage;
    private int mTextNormalColor;
    private int mTextSelectedColor;
    private int mImageNormalStateColor;
    private int mImageSelectedStateColor;
    private BitmapDrawable bmpNormalImage;
    private BitmapDrawable bmpSelectedImage;
    private ImageView mImgView;
    private TextView mTextView;
    private String mTitle;
    private View child;
    private boolean isSelectedState;

    @SuppressLint("InflateParams")
    public AtmelImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.AtmelSvgImageView);

        //Get Drawable resource Ids
        TypedValue value = new TypedValue();
        ta.getValue(R.styleable.AtmelSvgImageView_svgDrawableId, value);
        int imageId = value.resourceId;

        TypedValue value1 = new TypedValue();
        ta.getValue(R.styleable.AtmelSvgImageView_svgDrawableSelectedId, value1);
        int selectedImageId = value1.resourceId;

        //Load SVG Images
        if (imageId != 0) {
            try {
                svgImage = SVG.getFromResource(getResources(), imageId);
            } catch (SVGParseException e) {
                e.printStackTrace();
            }
        }

        if (selectedImageId != 0) {
            try {
                svgSelectedImage = SVG.getFromResource(getResources(), selectedImageId);
            } catch (SVGParseException e) {
                e.printStackTrace();
            }
        }

        //Load Text Colors
        mTitle = ta.getString(R.styleable.AtmelSvgImageView_titleContent);
        mTextNormalColor = ta.getColor(R.styleable.AtmelSvgImageView_normalStateTextColor, Color.BLACK);
        mTextSelectedColor = ta.getColor(R.styleable.AtmelSvgImageView_selectedStateTextColor, mTextNormalColor);

        //Get Image State colors
        mImageNormalStateColor = ta.getColor(R.styleable.AtmelSvgImageView_normalStateImageColor, Color.BLACK);
        mImageSelectedStateColor = ta.getColor(R.styleable.AtmelSvgImageView_selectedStateImageColor, Color.BLACK);

        ta.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        child = inflater.inflate(R.layout.atmel_image_button, null);
        mImgView = (ImageView) child.findViewById(R.id.atmelImageButtonImage);
        mTextView = (TextView) child.findViewById(R.id.atmelImageButtonText);

        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addView(child, params);
    }

    private BitmapDrawable getImageWithColors(SVG src, int width, int height, int srcColor, int dstColor) {
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas();
        c.setBitmap(bmp);
        src.setDocumentHeight(height);
        src.setDocumentWidth(width);
        src.renderToCanvas(c);
        BitmapDrawable drawable = new BitmapDrawable(getResources(), bmp);
        drawable.setColorFilter(new LightingColorFilter(srcColor, dstColor));

        return drawable;
    }

    public void initChildren() {
        bmpNormalImage = getImageWithColors(svgImage, mImgView.getWidth(), mImgView.getHeight(), Color.BLACK, mImageNormalStateColor);
        bmpSelectedImage = getImageWithColors(svgImage, mImgView.getWidth(), mImgView.getHeight(), Color.BLACK, mImageSelectedStateColor);
        mImgView.setImageDrawable(bmpNormalImage);
        mTextView.setText(mTitle);
        mTextView.setTextColor(mTextNormalColor);
        mTextView.setTextSize(15);
        invalidate();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        //Release bitmap soon after detached from parent
        if (bmpNormalImage != null) {
            bmpNormalImage.getBitmap().recycle();
            bmpNormalImage = null;
        }

        if (bmpSelectedImage != null) {
            bmpSelectedImage.getBitmap().recycle();
            bmpSelectedImage = null;
        }
    }

    public void changeSelectionState() {
        isSelectedState = !isSelectedState;
        mImgView.setImageDrawable(isSelectedState ? bmpSelectedImage : bmpNormalImage);
        mTextView.setTextColor(isSelectedState ? mTextSelectedColor : mTextNormalColor);
        child.invalidate();
    }

    public boolean getSelected() {
        return isSelectedState;
    }
}
