/*
 *
 *
 *     AtmelListViewAdapter.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.customcontrols;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class AtmelListViewAdapter extends BaseAdapter {
    private ListDataModel listDataModel;
    private int[] sectionRows;
    private boolean[] isViewAvailableForSection;

    @Override
    public int getCount() {
        if (listDataModel != null) {
            int totalCount = 0;
            int sections = listDataModel.getNumberOfSections();
            if (sections == 0) {
                return 0;
            }

            isViewAvailableForSection = new boolean[sections];
            int viewNotAvailableSections = 0;
            for (int i = 0; i < sections; i++) {
                if (isViewAvailableForSection[i] == false) {
                    isViewAvailableForSection[i] = listDataModel.isSectionHavingHeaderView(i);
                }
                if (isViewAvailableForSection[i] == false) {
                    viewNotAvailableSections++;
                }
            }
            sectionRows = new int[sections];
            totalCount = sections - viewNotAvailableSections;
            for (int i = 0; i < sectionRows.length; i++) {
                sectionRows[i] = listDataModel.RowsForSection(i);
                totalCount += sectionRows[i];
            }

            return totalCount;
        }

        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (listDataModel == null) {
            return null;
        }

        //Calculate sections and rows only if more than one section is available
        int section = 0;
        int sectionPos = (isViewAvailableForSection[0] == true) ? 1 : 0;
        boolean isSection = false;

        for (int i = 0; i < sectionRows.length && (sectionPos + sectionRows[i]) <= position; i++) {
            section++;
            sectionPos += sectionRows[i];
            if (sectionPos == position) {
                isSection = true;
                break;
            }

            if (isViewAvailableForSection[section] == true) {
                sectionPos++; //+1 for section if view is available for that
            }

        }

        //Get Section Header view
        if (isViewAvailableForSection[section] == true && (position == 0 || isSection)) {
            return listDataModel.viewForSectionHeader(section, convertView);
        }

        //Get row view
        return listDataModel.viewAtSectionRow(section, (position - sectionPos), convertView);
    }

    /**
     * Collect DataModel from the list screen
     *
     * @param listDataModel
     */
    public void setListDataModel(ListDataModel listDataModel) {
        this.listDataModel = listDataModel;
    }

    public interface ListDataModel {
        public int getNumberOfSections();

        public int RowsForSection(int sectionNumber);

        public boolean isSectionHavingHeaderView(int section);

        public View viewForSectionHeader(int section, View convertView);

        public View viewAtSectionRow(int section, int row, View convertView);
    }
}
