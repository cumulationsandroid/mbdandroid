/*
 *
 *
 *     AtmelPopover.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.customcontrols;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.atmel.beacon.R;
import com.atmel.beacon.util.Constants;


public class AtmelPopover extends TextView {
    private static Point ARROW_HEIGHT_WIDTH;
    private static int UP_DIRECTION_PADDING_TOP;
    private static int DOWN_DIRECTION_PADDING_TOP;
    private int[] location;
    private View mViewToShowPopover;
    private BitmapDrawable mRotatedBackground;
    private RadarLayout.BeaconInfo mBeaconInfo;
    private Path path;
    private ePopupDirection mPopupDirection = ePopupDirection.POPUP_DIRECTION_UP;
    private RectF mPopupRect = new RectF();
    private Paint mPaint = new Paint();
    private Context mContext;

    public AtmelPopover(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public AtmelPopover(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mPaint.setColor(Color.WHITE);
        ARROW_HEIGHT_WIDTH = new Point();
        ARROW_HEIGHT_WIDTH.x = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
        ARROW_HEIGHT_WIDTH.y = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
        UP_DIRECTION_PADDING_TOP = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
        DOWN_DIRECTION_PADDING_TOP = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mRotatedBackground != null) {
            mRotatedBackground.getBitmap().recycle();
            mRotatedBackground = null;
        }
    }

    @Override
    public void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
        super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
    }

    public void showPopup(final View view, RadarLayout.BeaconInfo beaconInfo) {
        setVisibility(View.INVISIBLE);
        mBeaconInfo = beaconInfo;
        updateInfo();

        postDelayed(new Runnable() {
            @Override
            public void run() {
                location = new int[2];
                view.getLocationOnScreen(location);
                Rect viewRect = new Rect(location[0], location[1], location[0] + view.getWidth(), location[1] + view.getHeight());
                int directionToAdd;
                int topMargin;
                int drawableId;

                if ((view.getTop() - getHeight()) <= 0) {
                    mPopupDirection = ePopupDirection.POPUP_DIRECTION_DOWN;
                    directionToAdd = RelativeLayout.BELOW;
                    topMargin = 0;
                    drawableId = R.drawable.icon_popover_arrow_down;
                } else {
                    mPopupDirection = ePopupDirection.POPUP_DIRECTION_UP;
                    directionToAdd = RelativeLayout.ABOVE;
                    topMargin = 0;
                    drawableId = R.drawable.icon_popover_arrow_up;
                }
                int marginLeft = (view.getLeft() - (getWidth() / 2) + (getPaddingLeft() + getPaddingRight()) / 2);
                RelativeLayout.LayoutParams viewParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
                //params.addRule(directionToAdd, view.getId());
                params.leftMargin = marginLeft;
                // params.leftMargin = 0;
                if (mPopupDirection == ePopupDirection.POPUP_DIRECTION_UP) {
                    params.topMargin = (view.getTop() - getHeight());
                    setPadding(getPaddingLeft(), UP_DIRECTION_PADDING_TOP, getPaddingRight(), DOWN_DIRECTION_PADDING_TOP);
                } else {
                    params.topMargin = (view.getTop() + view.getHeight());
                    setPadding(getPaddingLeft(), DOWN_DIRECTION_PADDING_TOP, getPaddingRight(), UP_DIRECTION_PADDING_TOP);
                }
                if (marginLeft < 0) {
                    params.leftMargin = 0;
                }

                setLayoutParams(params);

                setVisibility(View.VISIBLE);
            }
        }, 100);

    }

    public void showPopup(int x, int y, final View view, RadarLayout.BeaconInfo beaconInfo) {
        int marginLeft = 0;
        int topMargin = 0;

        location = new int[2];
        view.getLocationOnScreen(location);

        mBeaconInfo = beaconInfo;
        updateInfo();

        location = new int[2];
        view.getLocationOnScreen(location);
        if ((view.getTop() - getHeight()) <= 0) {
            mPopupDirection = ePopupDirection.POPUP_DIRECTION_DOWN;
        } else {
            mPopupDirection = ePopupDirection.POPUP_DIRECTION_UP;
        }
        marginLeft = (x - (getWidth() / 2) + (getPaddingLeft() + getPaddingRight()) / 2);

        if (mPopupDirection == ePopupDirection.POPUP_DIRECTION_UP) {
            topMargin = (y - getHeight());
            setPadding(getPaddingLeft(), UP_DIRECTION_PADDING_TOP, getPaddingRight(), DOWN_DIRECTION_PADDING_TOP);
        } else {
            topMargin = (y + view.getHeight());
            setPadding(getPaddingLeft(), DOWN_DIRECTION_PADDING_TOP, getPaddingRight(), UP_DIRECTION_PADDING_TOP);
        }
        if (marginLeft < 0) {
            marginLeft = 0;
        }
        // setLayoutParams(params);
        animate().x(marginLeft).y(topMargin);

        setVisibility(View.VISIBLE);


    }

    @Override
    public void onDraw(Canvas canvas) {
        path = new Path();
        canvas.drawColor(Color.TRANSPARENT);
        int s;
        if (getWidth() / 2 > location[0]) {
            s = location[0] + ARROW_HEIGHT_WIDTH.x;
        } else {
            s = getWidth() / 2;
        }

        if (mPopupDirection == ePopupDirection.POPUP_DIRECTION_UP) {
            path.moveTo(0, 0);
            path.lineTo(getWidth(), 0);
            path.lineTo(getWidth(), getHeight() - ARROW_HEIGHT_WIDTH.y);
            path.lineTo(s + ARROW_HEIGHT_WIDTH.x / 2, getHeight() - ARROW_HEIGHT_WIDTH.y);
            path.lineTo(s, getHeight());
            path.lineTo(s - ARROW_HEIGHT_WIDTH.x / 2, getHeight() - ARROW_HEIGHT_WIDTH.y);
            path.lineTo(0, getHeight() - ARROW_HEIGHT_WIDTH.y);
            path.lineTo(0, 0);
        } else {
            path.moveTo(0, ARROW_HEIGHT_WIDTH.y);
            path.lineTo(s - ARROW_HEIGHT_WIDTH.x / 2, ARROW_HEIGHT_WIDTH.y);
            path.lineTo(s, 0);
            path.lineTo(s + ARROW_HEIGHT_WIDTH.x / 2, ARROW_HEIGHT_WIDTH.y);
            path.lineTo(getWidth(), ARROW_HEIGHT_WIDTH.y);
            path.lineTo(getWidth(), getHeight());
            path.lineTo(0, getHeight());
            path.lineTo(0, ARROW_HEIGHT_WIDTH.y);
        }

        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setStrokeWidth(1);
        canvas.drawPath(path, mPaint);
        super.onDraw(canvas);
    }

    public void updateInfo() {
        String text = null;
        if (mBeaconInfo != null) {
            if (mBeaconInfo.mType == Constants.IBEACON_IDEFR) {
                text = "UUID : " + mBeaconInfo.mUuid + "\nMajor : " + mBeaconInfo.max + "\nMinor : " + mBeaconInfo.min + "\nRSSI : " + mBeaconInfo.rssi;
            } else if (mBeaconInfo.mType == Constants.EDDYSTONE_IDEFR) {

                if (mBeaconInfo.mUrl != null && mBeaconInfo.mNameSpaceId != null && mBeaconInfo.mInstanceId != null) {
                    text = "NamespaceId : " + mBeaconInfo.mNameSpaceId + "\nInstanceId : " + mBeaconInfo.mInstanceId +
                            "\nRSSI : " +
                            mBeaconInfo.rssi;
                } else if (mBeaconInfo.mUrl != null) {
                    text = "URL : " + mBeaconInfo.mUrl + "\nRSSI : " +
                            mBeaconInfo.rssi;
                } else {
                    text = "NamespaceId : " + mBeaconInfo.mNameSpaceId + "\nInstanceId : " + mBeaconInfo.mInstanceId + "\nRSSI : " +
                            mBeaconInfo.rssi;
                }
            } else if (mBeaconInfo.mType == Constants.ALTBEACON_IDEFR) {
                text = "UUID : " + mBeaconInfo.mUuid + "\nId1 : " + mBeaconInfo.max + "\nId2 : " + mBeaconInfo.min + "\nRSSI : " +
                        mBeaconInfo.rssi;
            } else {
                //Do Nothing
            }
        }
        setTextSize(10);
        setText(text);


    }

    public void hidePopup() {
        setVisibility(View.GONE);
    }

    private enum ePopupDirection {POPUP_DIRECTION_UP, POPUP_DIRECTION_DOWN, POPUP_DIRECTION_LEFT, POPUP_DIRECTION_RIGHT}


}
