/*
 *
 *
 *     AtmelSvgImageView.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.customcontrols;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ImageView;

import com.atmel.beacon.R;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;


public class AtmelSvgImageView extends ImageView {
    private final int TYPE_IMAGE = 0;
    private final int TYPE_IMAGE_TEXT = 1;
    private final int TEXT_IMAGE_GAP = 40;
    private SVG svgImage;
    private SVG svgSelectedImage;
    private BitmapDrawable mBitmap;
    private BitmapDrawable mSelectedBitmap;
    private String mTitle;
    private Paint mPaint = new Paint();
    private int mTextNormalColor;
    private int mTextSelectedColor;
    private int mImageNormalStateColor;
    private int mImageSelectedStateColor;
    private int mControlType;
    private boolean mIsSelected;

    public AtmelSvgImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.AtmelSvgImageView);

        //Get Drawable resource Ids
        TypedValue value = new TypedValue();
        ta.getValue(R.styleable.AtmelSvgImageView_svgDrawableId, value);
        int imageId = value.resourceId;

        TypedValue value1 = new TypedValue();
        ta.getValue(R.styleable.AtmelSvgImageView_svgDrawableSelectedId, value1);
        int selectedImageId = value1.resourceId;

        //Load SVG Images
        if (imageId != 0) {
            try {
                svgImage = SVG.getFromResource(getResources(), imageId);
            } catch (SVGParseException e) {
                e.printStackTrace();
            }
        }

        if (selectedImageId != 0) {
            try {
                svgSelectedImage = SVG.getFromResource(getResources(), selectedImageId);
            } catch (SVGParseException e) {
                e.printStackTrace();
            }
        }

        //Load Text properties
        float textSize = ta.getDimension(R.styleable.AtmelSvgImageView_textSize, 14.0f);
        mTitle = ta.getString(R.styleable.AtmelSvgImageView_titleContent);

        //Load Text Colors
        mTextNormalColor = ta.getColor(R.styleable.AtmelSvgImageView_normalStateTextColor, Color.BLACK);
        mTextSelectedColor = ta.getColor(R.styleable.AtmelSvgImageView_selectedStateTextColor, mTextNormalColor);

        //Set Text paint properties
        mPaint.setColor(mTextNormalColor);
        mPaint.setTextSize(textSize);

        //Get Image Type
        mControlType = ta.getInteger(R.styleable.AtmelSvgImageView_type, TYPE_IMAGE);

        //Get Image State colors
        mImageNormalStateColor = ta.getColor(R.styleable.AtmelSvgImageView_normalStateImageColor, Color.BLACK);
        mImageSelectedStateColor = ta.getColor(R.styleable.AtmelSvgImageView_selectedStateImageColor, Color.BLACK);

        ta.recycle();
    }

    @Override
    public void onDetachedFromWindow() {
        if (mBitmap != null) {
            mBitmap.getBitmap().recycle();
            mBitmap = null;
        }

        if (mSelectedBitmap != null) {
            mSelectedBitmap.getBitmap().recycle();
            mSelectedBitmap = null;
        }

        svgImage = svgSelectedImage = null;

        super.onDetachedFromWindow();
    }

    /**
     * After measuring the view re calculate svg images height & width
     *
     * @param width
     * @param height
     */
    private void setSvgImagesWidthAndHeight(int width, int height) {
        if (svgImage != null) {
            svgImage.setDocumentWidth(width);
            svgImage.setDocumentHeight(height);
        }

        if (svgSelectedImage != null) {
            svgSelectedImage.setDocumentWidth(width);
            svgSelectedImage.setDocumentHeight(height);
        }
    }

    /**
     * Returns height of the control based on Image & Text
     *
     * @return
     */
    private int getControlHeight() {
        if (mControlType == TYPE_IMAGE) {
            return (int) svgImage.getDocumentHeight();
        }

        int svgHeight = (int) svgImage.getDocumentHeight();
        int textHeight = (int) mPaint.getTextSize();

        return (svgHeight + textHeight + TEXT_IMAGE_GAP);
    }

    /**
     * Returns max width of the view based on Image & Text
     *
     * @return
     */
    private int getControlWidth() {
        if (mControlType == TYPE_IMAGE) {
            return (int) svgImage.getDocumentWidth();
        }

        int svgWidth = (int) svgImage.getDocumentWidth();
        int textWidth = (int) mPaint.measureText(mTitle);

        return Math.max(svgWidth, textWidth);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = svgImage != null ? getControlWidth() : 100;
        int desiredHeight = svgImage != null ? getControlHeight() : 100;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;

        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        //setSvgImagesWidthAndHeight(width, height);

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

    /**
     * Creates a bitmap from svg image
     *
     * @param src
     * @return
     */
    private BitmapDrawable getBitmapFromSVG(SVG src, boolean isSelectedImage) {
        Bitmap bmp = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        if (getScaleType() == ScaleType.FIT_XY) {
            if (mControlType == TYPE_IMAGE) {
                src.setDocumentHeight(getHeight());
                src.setDocumentWidth(getWidth());
            } else {
                src.setDocumentHeight(getHeight() - (mPaint.getTextSize() + TEXT_IMAGE_GAP));
                src.setDocumentWidth(getWidth());
            }
        }
        src.renderToCanvas(c);

        BitmapDrawable drawable = new BitmapDrawable(getResources(), bmp);
        drawable.setColorFilter(new LightingColorFilter(Color.BLACK, isSelectedImage ? mImageSelectedStateColor : mImageNormalStateColor));

        return drawable;
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (mBitmap == null && svgImage != null) {
            mBitmap = getBitmapFromSVG(svgImage, false);
        }

        if (mSelectedBitmap == null) {
            //If Selection image is different load it otherwise just change the color of the normal bitmap
            if (svgSelectedImage != null) {
                mSelectedBitmap = getBitmapFromSVG(svgSelectedImage, true);
            } else if (mControlType == TYPE_IMAGE_TEXT) {
                mSelectedBitmap = getBitmapFromSVG(svgImage, true);
            }
        }

        //Draw bitmap based on the selection state
        Bitmap bitmap = mIsSelected ? mSelectedBitmap.getBitmap() : mBitmap.getBitmap();
        if (bitmap != null) {
            int x = 0;
            int y = 0;
            int textWidth = (mTitle != null ? (int) mPaint.measureText(mTitle) : 0);
            int textHeight = (mTitle != null ? (int) mPaint.getTextSize() : 0);

            //Draw Image center if scale type is not fitXY. If scaleType is fitXY draw from 0, 0
            if (getScaleType() == ScaleType.FIT_XY) {
                canvas.drawBitmap(bitmap, x, y, null);
            } else {
                System.out.println("Width : " + getWidth() + " Height : " + getHeight());
                if (mControlType == TYPE_IMAGE) {
                    x = ((getWidth() - bitmap.getWidth()) / 2);
                    y = ((getHeight() - bitmap.getHeight()) / 2);
                } else {
                    x = ((getWidth() - bitmap.getWidth()) / 2);
                    y = 0;
                }

                mBitmap.setBounds(x, y, mBitmap.getIntrinsicWidth(), mBitmap.getIntrinsicHeight());
                mBitmap.draw(canvas);
                //canvas.drawBitmap(bitmap, x, y, null);
            }

            //Draw Text
            if (mTitle != null) {
                x = (getWidth() - textWidth) / 2;
                y += bitmap.getHeight();
                mPaint.setColor(mIsSelected ? mTextSelectedColor : mTextNormalColor);
                canvas.drawText(mTitle, x, y, mPaint);
            }
        }
    }

    public void changeSelection() {
        mIsSelected = !mIsSelected;
        postInvalidate();
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    /**
     * Dynamically changes the image of the view
     *
     * @param resId
     */
    public void setSVGImageResourceId(int resId) {
        try {
            svgImage = SVG.getFromResource(getContext(), resId);
            requestLayout();
            invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
