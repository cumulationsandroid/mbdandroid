/*
 *
 *
 *     AtmelTableView.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.customcontrols;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import org.json.JSONException;
import org.json.JSONObject;

public class AtmelTableView extends TableLayout implements View.OnClickListener {
    private ClubsTableViewDataSource dataSource;
    private CampsTableViewListener listener;
    private int rowCount = 1;
    private int[] sections;
    private boolean[] isSectionHavingHeader;

    /**
     * Dynamic creation of the view
     *
     * @param context
     */
    public AtmelTableView(Context context) {
        super(context);
    }

    /**
     * Creates from XML file
     *
     * @param context
     * @param attrs
     */
    public AtmelTableView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Sets table data source
     *
     * @param dataSource
     */
    public void setTableDataSource(ClubsTableViewDataSource dataSource) {
        //Reload table view with new items
        this.removeAllViews();

        this.dataSource = dataSource;
        initTableRow();
    }

    /**
     * Adds given view to table row
     *
     * @param view
     * @throws JSONException
     */
    private void addViewToTableRow(View view, int section, int row) throws JSONException {
        JSONObject jsonObj = new JSONObject();

        rowCount++;

        //Set Row, section combination tag
        jsonObj.put("section", section);
        jsonObj.put("row", row);
        view.setTag(jsonObj);
        view.setClickable(true);
        view.setOnClickListener(this);
        addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    /**
     * Initializes the table rows of this view
     */
    private void initTableRow() {
        try {
            int sectionCount = dataSource.getNumberOfSections();

            //No Sections are available don't do anything
            if (sectionCount == 0) {
                return;
            }

            sections = new int[sectionCount];
            isSectionHavingHeader = new boolean[sectionCount];

            //Add table rows
            View view;
            for (int i = 0; i < sections.length; i++) {
                //Add section view
                isSectionHavingHeader[i] = dataSource.isSectionHavingHeaderView(i);
                if (isSectionHavingHeader[i]) {
                    view = dataSource.viewForSectionHeader(i, null);
                    if (view != null) {
                        addViewToTableRow(view, -1, i);
                    }
                }

                //Add Rows View
                sections[i] = dataSource.RowsForSection(i);
                for (int j = 0; j < sections[i]; j++) {
                    view = dataSource.viewAtSectionRow(i, j, null);
                    if (view != null) {
                        addViewToTableRow(view, i, j);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(this.getClass().getName(), "Unable to add Table Row ");
            e.printStackTrace();
        }
    }

    /**
     * Adds given view at given index
     *
     * @param index
     * @param v
     */
    public void addRowAtIndex(int index, View v) throws JSONException {
        this.addView(v, index);

        //update existing rows information from the suppliedIndex
        JSONObject jsonData;
        int row;
        for (int i = (index + 1); i < this.getChildCount(); i++) {
            jsonData = (JSONObject) (this.getChildAt(i)).getTag();
            row = jsonData.getInt("row") + 1;
            jsonData.put("row", row);
        }
    }

    /**
     * Removes the view from supplied index
     *
     * @param start
     * @throws JSONException
     */
    public void removeViewsRange(int start, int count) throws JSONException {
        if ((start + count) > this.getChildCount()) {
            //Invalid request
            return;
        }

        this.removeViews(start, count);

        //update remaining rows information from the suppliedIndex
        JSONObject jsonData;
        int row;
        for (int i = start; i < this.getChildCount(); i++) {
            jsonData = (JSONObject) (this.getChildAt(i)).getTag();
            row = jsonData.getInt("row") - count;
            jsonData.put("row", row);
        }
    }

    /**
     * Collects action listener
     *
     * @param listener
     */
    public void setCampsTableViewClickListener(CampsTableViewListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener == null) {
            return;
        }

        try {
            JSONObject jsonObj = (JSONObject) v.getTag();
            if (jsonObj.getInt("section") != -1) {
                listener.didSelectRowAtSection(jsonObj.getInt("section"), jsonObj.getInt("row"), v);
            }
        } catch (Exception e) {
            Log.e(this.getClass().getName(), "Unable process table click");
            e.printStackTrace();
        }
    }

    /**
     * Sets background color of item
     *
     * @param section
     * @param row
     * @param color
     */
    public void setItemBackgroundColor(int section, int row, int color) {
        try {
            ViewGroup viewGroup;
            JSONObject tagJsonObj;
            for (int i = 0; i < this.getChildCount(); i++) {
                viewGroup = (ViewGroup) this.getChildAt(i);
                tagJsonObj = (JSONObject) viewGroup.getTag();
                if (tagJsonObj.getInt("section") == section && tagJsonObj.getInt("row") == row) {
                    viewGroup.setBackgroundColor(color);
                }
            }
        } catch (Exception e) {
            Log.e(this.getClass().getName(), "Unable to set selected color");
        }
    }

    /**
     * Get List Item View by section and row
     *
     * @param section
     * @param row
     * @return
     */
    private ViewGroup getListItemBySectionAndRow(int section, int row) {
        try {
            ViewGroup viewGroup;
            JSONObject tagJsonObj;
            for (int i = 0; i < this.getChildCount(); i++) {
                viewGroup = (ViewGroup) this.getChildAt(i);
                tagJsonObj = (JSONObject) viewGroup.getTag();
                if (tagJsonObj.getInt("section") == section && tagJsonObj.getInt("row") == row) {
                    return viewGroup;
                }
            }
        } catch (Exception e) {
            Log.e(this.getClass().getName(), "Unable to get list item view by section and row");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns rows per section
     *
     * @param section
     * @return
     */
    public int getRowCountForSection(int section) {
        if (section > sections.length) {
            return -1;
        }

        return sections[section];
    }

    /**
     * Returns no.of sections in
     *
     * @return
     */
    public int getSectionCount() {
        return sections.length;
    }

    /**
     * Removes the view at given position
     *
     * @param section
     * @param row
     */
    public void removeViewBySectionAndRow(int section, int row) {
        ViewGroup view = getListItemBySectionAndRow(section, row);
        this.removeView(view);
    }

    public interface ClubsTableViewDataSource {
        public int getNumberOfSections();

        public int RowsForSection(int sectionNumber);

        public boolean isSectionHavingHeaderView(int section);

        public View viewForSectionHeader(int section, View convertView);

        public View viewAtSectionRow(int section, int row, View convertView);
    }

    public interface CampsTableViewListener {
        public void didSelectRowAtSection(int section, int row, View view);
    }
}
