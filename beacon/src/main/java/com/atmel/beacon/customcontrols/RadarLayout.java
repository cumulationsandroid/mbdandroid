/*
 *
 *
 *     RadarLayout.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.customcontrols;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.atmel.beacon.R;
import com.atmel.beacon.activities.AtmelMainActivity;
import com.atmel.beacon.database.BeaconListDbHandler;
import com.atmel.beacon.fragments.MainScreenFragment;
import com.atmel.beacon.util.Constants;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.utils.UrlBeaconUrlCompressor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


/**
 * Beacon range plotting view
 */
public class RadarLayout extends RelativeLayout {
    private static final int MAX_ROWS_FOR_LESS_THAN_ONE_METER = 1;
    private static final int MAX_RADAR_PORTIONS = 3;
    private static final int COLUMN_EMPTY = 0;
    private static final int MOVE_ANIMATION_DURATION = 500;
    private static final long BEACON_TIMEOUT_TO_REMOVE_FROM_SCREEN = 5000;
    private static final int SAMPLING_COUNT = 10;
    private static int MAX_COLS_PER_SECTOR = 15;
    private static int MAX_ROWS_PER_SECTOR = 3;
    private final String TAG = "RadorLayout";
    public int mHeight = 0;
    float mInnerRadius = 0;
    float mRadarRadius = 0;
    private int mBeaconWidth;
    private int mBeaconHeight;
    private float mRadarPortionHeight;
    private float mRadarTopPortionHeight;
    private float mBeaconCircleRadius;
    private Bitmap mBmpRadarBackground;
    private Bitmap mBmpBeacon;
    private int mSafeMin = 10;
    private int mSafeMax = 50;
    private int mMidMin = 50;
    private int mMidMax = 75;
    private int mDangerMin = 75;
    private int mDangerMax = 100;
    private ArrayList<BeaconInfo> mBeaconsInfoCache = new ArrayList<>();
    private View mSelectedBeacon;
    private PopoverView mCurrentPopOverView;
    private AtmelPopover mPopOver;
    private float mFirstCircleRadius = 0;
    private float mSecondCircleRadius = 0;
    private float mThirdCircleRadius = 0;
    private int mWidth = 0;
    private AtmelMainActivity mainActivity;
    private BeaconRowDataModel beaconRowDataModel;
    private BeaconListDbHandler beaconListDbHandler;
    /**
     * Click will result pop over above corresponding beacon
     */
    private OnClickListener onBeaconClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mSelectedBeacon == v) {
                return;
            }
            mainActivity = (AtmelMainActivity) getContext();
            MainScreenFragment mainScreenFragment = (MainScreenFragment) mainActivity.getFragmentManager().
                    findFragmentByTag(MainScreenFragment.TAG);
            if (!mainScreenFragment.checkInformationViewVisisbility()) {
                mPopOver.showPopup(v, (BeaconInfo) v.getTag());

            }
            mSelectedBeacon = v;
        }
    };
    private ArrayList<int[]> mFreeColumnsOnRow;
    private HashMap<String, BeaconInfo> mhmBeacons;
    private ArrayList<Integer> mColumnList = new ArrayList<>();

    /***
     * Constructors
     ***/
    public RadarLayout(Context context) {
        super(context);
        init();
    }

    public RadarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RadarLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentPopOverView != null) {
                    mCurrentPopOverView.dissmissPopover(false);
                    mCurrentPopOverView = null;
                }

                mPopOver.hidePopup();

                mSelectedBeacon = null;
            }
        });
        mhmBeacons = new HashMap<>();
        mhmBeacons.clear();
        mBeaconsInfoCache.clear();
        beaconListDbHandler = new BeaconListDbHandler(getContext());
    }

    /***
     * Overridden Methods
     ***/
    @Override
    public void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);

        System.out.println("Size change called");
        //TODO: Goe the widht & height of the layout, init it now.
        initRadarLayout(xNew, yNew);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        releaseBitmaps();
    }

    /***
     * Helper functions
     ***/
    private void initRadarLayout(int width, int height) {
        mWidth = width;
        mHeight = height;
        //Radar portion properties
        mRadarTopPortionHeight = getHeight() * 0.1f;
        mRadarPortionHeight = (getHeight() - mRadarTopPortionHeight) / MAX_RADAR_PORTIONS;

        //Calculate Beacon Properties
        mBeaconWidth = width / MAX_COLS_PER_SECTOR;
        mBeaconHeight = (int) (mRadarPortionHeight / MAX_ROWS_PER_SECTOR);
        mBeaconCircleRadius = Math.min(mBeaconWidth, mBeaconHeight) / 2;
        mBeaconWidth = mBeaconHeight = (int) (mBeaconCircleRadius * 2);
        MAX_COLS_PER_SECTOR = (int) (width / (mBeaconCircleRadius * 2));

        //Get Static Bitmaps
        releaseBitmaps();
        if (width < height) {
            mBmpRadarBackground = getRadorBackground(width, height);
        } else {
            mBmpRadarBackground = getRadorBackgroundForLandscale(width, height);
        }
        //Set Radar as a background
        setBackgroundDrawable(new BitmapDrawable(getResources(), mBmpRadarBackground));
    }

    /***
     * Constructors
     ***/

    private Bitmap getRadorBackground(int width, int height) {
        Bitmap mDeviceBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.beacon_phone);
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        int mRadarTopPadding = 100;
        mInnerRadius = (float) (width / 4);
        mRadarRadius = (height - mInnerRadius - mRadarTopPadding) / 3;
        mFirstCircleRadius = mInnerRadius + mRadarRadius;
        mSecondCircleRadius = mFirstCircleRadius + mRadarRadius;
        mThirdCircleRadius = mSecondCircleRadius + mRadarRadius;
        Paint paintTwo = new Paint(Paint.ANTI_ALIAS_FLAG);
        //Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        Paint paintWhite = new Paint(Paint.ANTI_ALIAS_FLAG);
        Paint paintText = new Paint(Paint.LINEAR_TEXT_FLAG);
        paintText.setFakeBoldText(true);
        paintText.setColor(getResources().getColor(R.color.black));
        int scaledSize = getResources().getDimensionPixelSize(R.dimen.text_medium);
        paintText.setTextSize(scaledSize);


        paintTwo.setStyle(Paint.Style.STROKE);
        paintTwo.setColor(getResources().getColor(R.color.featureTitleColor));
        paintTwo.setStrokeWidth(7);

        paintWhite.setStyle(Paint.Style.STROKE);
        paintWhite.setColor(getResources().getColor(R.color.white));
        paintWhite.setStrokeWidth(7);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(getResources().getColor(R.color.color_radar));
        //applyFilter(paintTwo, new float[]{0f, -1f, 0.5f}, 0.9f, 15f, 1f);

        //Init radar paint
        // paint.setStyle(Paint.Style.FILL);
        // paint.setFlags(Paint.ANTI_ALIAS_FLAG);

        //Init Text Paint
        //txtPaint.setColor(Color.BLACK);
        // txtPaint.setTextSize(40);

        //Draw Background to Bitmap
        canvas.setBitmap(bmp);

        /*
        canvas.drawColor(Color.parseColor("#CDEFF9"));

        String[] distance   = {">2m", ">1m", "<1m"};
        String[] colors     = {"#AEE7F8", "#95E0F7", "#7EDAF3"};
        for( int i = 0; i < MAX_RADAR_PORTIONS; i++ )
        {
            paint.setColor(Color.parseColor(colors[i]));
            arcRect.set(-getWidth() / 2, mRadarTopPortionHeight + (mRadarPortionHeight * i), getWidth() + getWidth() / 2, (mRadarTopPortionHeight + (mRadarPortionHeight * i)) + mRadarPortionHeight * 3);
            canvas.drawArc(arcRect, 180, 180, true, paint);
            canvas.drawText(distance[i], 50, mRadarTopPortionHeight * 2 + (mRadarPortionHeight * i), txtPaint);
        }*/

        canvas.drawCircle(width / 2, height, width / 4, paint);
        //Drawing Device image

        //canvas.drawBitmap(mDeviceBitmap, width / 2 - (mDeviceBitmap.getWidth() / 2), height - mDeviceBitmap.getHeight(), paint);

        canvas.drawCircle(width / 2, height, mInnerRadius + 5, paintTwo);

        //Drawing Device image
        //canvas.drawBitmap(mDeviceBitmap, centerX - (mDeviceBitmap.getWidth() / 2), centerY -
        //mDeviceBitmap.getHeight() , paint);

        canvas.drawCircle(width / 2, height, mFirstCircleRadius, paintTwo);
        canvas.drawCircle(width / 2, height, mFirstCircleRadius - 7, paintWhite);
        canvas.drawText("far", width / 8, mInnerRadius + (mFirstCircleRadius - mInnerRadius) / 2, paintText);

        canvas.drawCircle(width / 2, height, mSecondCircleRadius, paintTwo);
        canvas.drawCircle(width / 2, height, mSecondCircleRadius - 7, paintWhite);
        canvas.drawText("near", width / 8, mFirstCircleRadius + (mSecondCircleRadius - mFirstCircleRadius) / 2, paintText);

        canvas.drawCircle(width / 2, height, mThirdCircleRadius, paintTwo);
        canvas.drawCircle(width / 2, height, mThirdCircleRadius - 7, paintWhite);
        canvas.drawText("immediate", width / 8, mSecondCircleRadius + (mThirdCircleRadius - mSecondCircleRadius) / 2, paintText);

        return bmp;
    }

    private Bitmap getRadorBackgroundForLandscale(int width, int height) {
        float centerX = width / 2;
        float centerY = height;
        Bitmap mDeviceBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.beacon_phone);
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        int mRadarTopPadding = 100;
        mInnerRadius = (float) (height / 6);
        mRadarRadius = (height - mInnerRadius - mRadarTopPadding) / 3;
        mFirstCircleRadius = mInnerRadius + mRadarRadius;
        mSecondCircleRadius = mFirstCircleRadius + mRadarRadius;
        mThirdCircleRadius = mSecondCircleRadius + mRadarRadius;
        Paint paintTwo = new Paint(Paint.ANTI_ALIAS_FLAG);
        Paint paintWhite = new Paint(Paint.ANTI_ALIAS_FLAG);
        Paint paintText = new Paint(Paint.LINEAR_TEXT_FLAG);
        paintText.setFakeBoldText(true);
        paintText.setColor(getResources().getColor(R.color.black));
        int scaledSize = getResources().getDimensionPixelSize(R.dimen.text_medium);
        paintText.setTextSize(scaledSize);
        paintTwo.setStyle(Paint.Style.STROKE);
        paintTwo.setColor(getResources().getColor(R.color.featureTitleColor));
        paintTwo.setStrokeWidth(7);
        paintWhite.setStyle(Paint.Style.STROKE);
        paintWhite.setColor(getResources().getColor(R.color.white));
        paintWhite.setStrokeWidth(7);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(getResources().getColor(R.color.color_radar));

        //Draw Background to Bitmap
        canvas.setBitmap(bmp);

        /*
        canvas.drawColor(Color.parseColor("#CDEFF9"));

        String[] distance   = {">2m", ">1m", "<1m"};
        String[] colors     = {"#AEE7F8", "#95E0F7", "#7EDAF3"};
        for( int i = 0; i < MAX_RADAR_PORTIONS; i++ )
        {
            paint.setColor(Color.parseColor(colors[i]));
            arcRect.set(-getWidth() / 2, mRadarTopPortionHeight + (mRadarPortionHeight * i), getWidth() + getWidth() / 2, (mRadarTopPortionHeight + (mRadarPortionHeight * i)) + mRadarPortionHeight * 3);
            canvas.drawArc(arcRect, 180, 180, true, paint);
            canvas.drawText(distance[i], 50, mRadarTopPortionHeight * 2 + (mRadarPortionHeight * i), txtPaint);
        }*/

        canvas.drawCircle(centerX, centerY, mInnerRadius, paint);
        canvas.drawCircle(width / 2, height, mInnerRadius + 5, paintTwo);

        //Drawing Device image
        //canvas.drawBitmap(mDeviceBitmap, centerX - (mDeviceBitmap.getWidth() / 2), centerY -
        //mDeviceBitmap.getHeight() , paint);
        Rect bounds = new Rect();
        canvas.drawCircle(width / 2, height, mFirstCircleRadius, paintTwo);
        canvas.drawCircle(width / 2, height, mFirstCircleRadius - 7, paintWhite);
        String text = "far";
        paintText.getTextBounds(text, 0, text.length(), bounds);
        canvas.drawText(text, width / 2 - (bounds.width() / 2), height - mThirdCircleRadius + mRadarRadius / 2, paintText);

        canvas.drawCircle(width / 2, height, mSecondCircleRadius, paintTwo);
        canvas.drawCircle(width / 2, height, mSecondCircleRadius - 7, paintWhite);
        text = "near";
        paintText.getTextBounds(text, 0, text.length(), bounds);
        canvas.drawText(text, width / 2 - (bounds.width() / 2), height - mSecondCircleRadius + mRadarRadius / 2, paintText);

        canvas.drawCircle(width / 2, height, mThirdCircleRadius, paintTwo);
        canvas.drawCircle(width / 2, height, mThirdCircleRadius - 7, paintWhite);
        text = "immediate";
        paintText.getTextBounds(text, 0, text.length(), bounds);
        canvas.drawText(text, width / 2 - (bounds.width() / 2), height - mFirstCircleRadius + mRadarRadius / 2, paintText);
        //Drawing Device image
        canvas.drawBitmap(mDeviceBitmap, width / 2 - (mDeviceBitmap.getWidth() / 2), height - mDeviceBitmap.getHeight(), paint);

        return bmp;
    }

    /***
     * Overridden Methods
     ***/

    private Bitmap getBeaconBitmap() {
        Bitmap bmp = Bitmap.createBitmap(mBeaconWidth, mBeaconHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        Paint paint = new Paint();

        //Set Beacon color
        paint.setStyle(Paint.Style.FILL);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);

        //Draw circle to bitmap
        canvas.drawColor(Color.TRANSPARENT);
        canvas.setBitmap(bmp);
        canvas.drawCircle(mBeaconWidth / 2, mBeaconHeight / 2, mBeaconCircleRadius, paint);

        return bmp;
    }

    private void releaseBitmaps() {
        //Release bitmaps
        Bitmap[] bmps = {mBmpBeacon, mBmpRadarBackground};
        for (Bitmap bmp : bmps) {
            if (bmp != null) {
                bmp.recycle();
                bmp = null;
            }
        }
        mBmpBeacon = mBmpRadarBackground = null;
    }

//    private void updatePopoverText()
//    {
//        if( mSelectedBeacon == null )
//        {
//            return;
//        }
//
//        BeaconInfo beaconInfo = (BeaconInfo) mSelectedBeacon.getTag();
//        //mtvPopoverText.setText("Max : " + beaconInfo.max + "\nMin : " + beaconInfo.min + "\nRssi: " + beaconInfo.rssi + "\nDistance : " + beaconInfo.distance );
//    }
//
//    private void cancelPopOver()
//    {
//        if( mCurrentPopOverView != null )
//        {
//            mCurrentPopOverView.dissmissPopover(false);
//            mCurrentPopOverView = null;
//        }
//        mSelectedBeacon = null;
//    }
//
//    private void creaetPopover()
//    {
//        mCurrentPopOverView = new PopoverView(getContext(), R.layout.popover_showed_view);
//        mCurrentPopOverView.setRadarLayout(this);
//        mtvPopoverText = mCurrentPopOverView.getMsgTextView();
//        updatePopoverText();
//        mCurrentPopOverView.setContentSizeForViewInPopover(new Point(200, 150));
//        mCurrentPopOverView.showPopoverFromRectInViewGroup(RadorLayout.this, PopoverView.getFrameForView(mSelectedBeacon), PopoverView.PopoverArrowDirectionUp, true);
//    }

    /***
     * API functions
     ***/
    public void onBeaconCBReceived(Collection<Beacon> beacons) {
        if (beaconRowDataModel == null) {
            beaconRowDataModel = new BeaconRowDataModel();
        }

        beaconRowDataModel.onBeaconCBReceived(beacons);
    }

    public BeaconInfo getNearestBeacon() {
        Collections.sort(mBeaconsInfoCache, new Comparator<BeaconInfo>() {
            @Override
            public int compare(BeaconInfo lhs, BeaconInfo rhs) {
                return Double.compare(lhs.distance, rhs.distance);
            }
        });
        if (mBeaconsInfoCache.size() > 0) {
            return mBeaconsInfoCache.get(0);
        }

        return null;
    }

    public BeaconInfo getBeaconByInfo(BeaconInfo beaconInfo) {
        if (beaconInfo == null) {
            return null;
        }
        return beaconRowDataModel.getBeaconInfoFromVisibleBeacons(beaconInfo);
    }

    /***
     * Helper functions
     ***/

    public ArrayList<BeaconInfo> getAllVisibleBeaconsInfo() {
        return mBeaconsInfoCache;
    }

    public void setPopupView(AtmelPopover popover) {
        mPopOver = popover;

        mPopOver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
             /*  // Intent intent = new Intent(getContext(), BeaconInformationActivity.class);
                BeaconInfo info = (BeaconInfo)mSelectedBeacon.getTag();
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.EXTRA_BEACON_TYPE, info.mType);
                switch(info.mType){
                    case Constants.IBEACON_IDEFR:
                        bundle.putString(Constants.EXTRA_IBEACON_UUID, info.mUuid);
                        bundle.putInt(Constants.EXTRA_IBEACON_MAJOR, info.max);
                        bundle.putInt(Constants.EXTRA_IBEACON_MINOR, info.min);
                        bundle.putString(Constants.EXTRA_IBEACON_URL, info.mUrl);
                        break;
                    case Constants.ALTBEACON_IDEFR:
                        break;
                    case Constants.EDDYSTONE_IDEFR:
                        bundle.putLong(Constants.EXTRA_EDDYSTONE_BATTERY_VOLTAGE, info.mTlmBatteryVoltage);
                       // intent.putExtra(Constants.EXTRA_EDDYSTONE_TEMPARATURE,info);
                        String url  = info.mUrl;
                        long temp = info.mTlmTemparature;
                        bundle.putString(Constants.EXTRA_EDDYSTONE_UID, info.mUuid);
                        bundle.putLong(Constants.EXTRA_EDDYSTONE_UPTIME, info.mTlmUptime);
                        bundle.putString(Constants.EXTRA_EDDYSTONE_URL, info.mUrl);
                        bundle.putString(Constants.EXTRA_EDDYSTONE_NAMESPACEID, info.mNameSpaceId);
                        bundle.putString(Constants.EXTRA_EDDYSTONE_INSTANCEID, info.mInstanceId);
                        bundle.putInt(Constants.EXTRA_EDDYSTONE_TXPOWER, info.mTxPower);
                        break;
                    default:
                        break;
                }

                mMainActivity = (AtmelMainActivity)getContext();*/
               /* informationFragment = new BeaconInformationFragment();
                informationFragment.setArguments(bundle);
                if(mMainActivity != null) {
                     mMainActivity.replaceFragment(informationFragment, false);
                }*/
                try {
                    BeaconInfo info = (BeaconInfo) mSelectedBeacon.getTag();
             /*   try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                    mPopOver.hidePopup();
                    mainActivity = (AtmelMainActivity) getContext();
                    MainScreenFragment mainScreenFragment = (MainScreenFragment) mainActivity.getFragmentManager().
                            findFragmentByTag(MainScreenFragment.TAG);
                    mainActivity.mImageView.setVisibility(View.INVISIBLE);
                    mainActivity.mConfigureImageView.setVisibility(View.INVISIBLE);
                    mainScreenFragment.enableInformationView();
                    mainScreenFragment.updateBeaconInformation(info);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void onPopOverDismiss() {
        //cancelPopOver();
        mCurrentPopOverView = null;
        mSelectedBeacon = null;
    }

    /***
     * API Functions
     ***/

    public String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for (byte b : a)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }

    /**
     * Set Progress
     *
     * @param progress
     */
    public synchronized int setProgress(double progress) {
        //int progress = progress;
        int region = 0;
        int pors = 0;
        progress = Math.abs(progress);
        Bitmap bmp = BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.eddystone, null);
        int w = bmp.getWidth();
        int h = bmp.getHeight();

        //int pors = (int) outerRadius / 4;

        if (progress < mSafeMin) {
            pors = (int) (mInnerRadius);
            region = pors / (mSafeMin);
            int i = (int) Math.abs(progress);
            region = region * i;
            //region = (int)outerRadius;
        } else if (progress > mSafeMin && progress <= mSafeMax) {
            pors = (int) (mFirstCircleRadius - mInnerRadius);
            region = pors / (mSafeMax - mSafeMin);
            int i = (int) Math.abs(mSafeMin - progress);
            region = region * i + (int) mInnerRadius;
        } else if (progress > mMidMin && progress <= mMidMax) {
            pors = (int) (mSecondCircleRadius - mFirstCircleRadius);
            // region = ((pors * 2) - pors) / (mMidMax - mMidMin);
            region = pors / (mMidMax - mMidMin);
            int i = (int) Math.abs(mMidMin - progress);
            region = (region * i) + (int) mFirstCircleRadius;
        } else if (progress > mDangerMin && progress <= mDangerMax) {
            pors = (int) (mThirdCircleRadius - mSecondCircleRadius);
            // region = ((int) outerRadius - (pors * 2)) / (mDangerMax - mDangerMin);
            region = pors / (mDangerMax - mDangerMin);
            int i = (int) Math.abs(mDangerMin - progress);
            region = (region * i) + (int) mSecondCircleRadius;
        } else if (progress > mDangerMax) {
            /*pors = (int) ( centerY - 10 - outerRadius*6 );
            // region = ((int) outerRadius - (pors * 2)) / (mDangerMax - mDangerMin);
            region = pors/ (120 - mDangerMax);
            int i = Math.abs(mDangerMax - progress);
            region = (region * i) + (int)(outerRadius*6);
        }else{*/
            region = (int) mThirdCircleRadius;
        }

        region = region + (h / 2);
        return region;
    }

    enum eBeaconState {BEACON_STATE_UNCHANGED, BEACON_STATE_NEW, BEACON_STATE_MOVED, BEACON_STATE_REMOVED}

    private class BeaconRowDataModel {


        public BeaconRowDataModel() {
            mFreeColumnsOnRow = new ArrayList<>(MAX_RADAR_PORTIONS * 2 + MAX_ROWS_FOR_LESS_THAN_ONE_METER);
            for (int i = 0; i < (MAX_RADAR_PORTIONS * 2 + MAX_ROWS_FOR_LESS_THAN_ONE_METER); i++) {
                mFreeColumnsOnRow.add(i, new int[MAX_COLS_PER_SECTOR]);
            }
            mhmBeacons = new HashMap<>();
        }

        /**
         * Frames the key of the beacon and returns
         *
         * @param identifiers
         * @return
         */
        private String beaconKey(List<Identifier> identifiers) {
            return ("" + identifiers.get(1).toInt() + "/" + identifiers.get(2).toInt());
        }


        /**
         * Create new BeaconInfo data model
         *
         * @param identifiers
         * @param beacon
         * @return
         */
        private BeaconInfo createOrUpdateBeaconInfo(BeaconInfo beaconInfo, int curBeaconRow, List<Identifier> identifiers, Beacon beacon,
                                                    int eddystoneFrameType, int type) {
            if (beaconInfo == null) {
                beaconInfo = new BeaconInfo();
                beaconInfo.radarRow = beaconInfo.radarCol = -1;
            }

            if (type == Constants.ALTBEACON_IDEFR || type == Constants.IBEACON_IDEFR) {
                beaconInfo.mUuid = identifiers.get(0).toString();
                beaconInfo.max = identifiers.get(1).toInt();
                beaconInfo.min = identifiers.get(2).toInt();
            } else {
                if (eddystoneFrameType == 1) {
                    beaconInfo.mNameSpaceId = byteArrayToHex(beacon.getId1().toByteArray());
                    beaconInfo.mInstanceId = byteArrayToHex(beacon.getId2().toByteArray());
                }
            }
            beaconInfo.distance = beacon.getDistance();
            Log.e("Beacon Txpower", beacon.getTxPower() + "");
            if (type == Constants.EDDYSTONE_IDEFR) {
                beaconInfo.rssi = beacon.getRssi() - (beacon.getTxPower() + 41);
            } else if (type == Constants.ALTBEACON_IDEFR) {
                beaconInfo.rssi = beacon.getRssi() - (beacon.getTxPower() + 41);
            } else {
                beaconInfo.rssi = beacon.getRssi()-(beacon.getTxPower() + 47);
            }
            beaconInfo.lastUpdateTimestamp = System.currentTimeMillis();
            beaconInfo.mType = type;

            beaconInfo.mTxPower = beacon.getTxPower() + 41;

            if (beaconInfo.radarRow == -1) {
                beaconInfo.beaconState = eBeaconState.BEACON_STATE_NEW;
                beaconInfo.radarRow = curBeaconRow;
                return beaconInfo;
            }

            //Update Beacon Info on Messagebox if it is open
            if (mSelectedBeacon == beaconInfo.vBeacon) {
                mPopOver.post(new Runnable() {
                    @Override
                    public void run() {
                        mPopOver.updateInfo();
                    }
                });
            }

            if (beaconInfo.radarRow == curBeaconRow) {
                beaconInfo.beaconState = eBeaconState.BEACON_STATE_UNCHANGED;
                return beaconInfo;
            }

            if (beaconInfo.radarRow != curBeaconRow) {
                beaconInfo.beaconState = eBeaconState.BEACON_STATE_MOVED;
                beaconInfo.newRadarRow = curBeaconRow;
                return beaconInfo;
            }


            return beaconInfo;
        }

        /**
         * Returns the row number whether the beacon fits depends upon its distance
         *
         * @param distance
         * @return
         */
        private int getRowNumberByInstance(double distance) {
            double ranges[] = {1.0, 1.33, 1.66, 2.0, 3.0, 5.0, 7.0,9.0,11.0,13.0,15.0,17.0,
                    20.0,23.0,26.0,30.0,35.0,40.0,45.0,80.0};
            int row = ranges.length - 1;

            for (double range : ranges) {
                if (Double.compare(distance, range) < 0) {
                    //Log("DEBUG_LOG","getRowNumberByInstance row no: "+ row);
                    return row;
                }
                row--;
            }
            return -1;
        }

        /**
         * Addes a new beacon to the given row position and registers click listener to the beacon view
         * to show popover.
         *
         * @param beaconInfo
         */
        private void addNewBeaconToRow(BeaconInfo beaconInfo) {
            int radius = setProgress(beaconInfo.rssi);
            int columnPosition = getColumnPosition();
            beaconInfo.mColumn = columnPosition;
            // int rowPosition = (int)Math.sqrt((double) (radius * radius) - (columnPosition * columnPosition));
            final int rowPosition = radius;
            beaconInfo.mRow = rowPosition;

            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View beaconView = layoutInflater.inflate(R.layout.beacon, null);
            ImageView image = (ImageView) beaconView.findViewById(R.id.image);


            Bitmap bmp;
            if (beaconInfo.mType == Constants.IBEACON_IDEFR) {
                //beaconView.setBackgroundResource(R.drawable.ibeacon_icon);
                bmp = BitmapFactory.decodeResource(getContext().getResources(),
                        R.drawable.ibeacon_icon);
                image.setImageBitmap(bmp);
            } else if (beaconInfo.mType == Constants.ALTBEACON_IDEFR) {
                // beaconView.setBackgroundResource(R.drawable.alt_beacon);
                bmp = BitmapFactory.decodeResource(getContext().getResources(),
                        R.drawable.alt_beacon);
                image.setImageBitmap(bmp);
            } else if (beaconInfo.mType == Constants.EDDYSTONE_IDEFR) {
                // beaconView.setBackgroundResource(R.drawable.eddystone);
                bmp = BitmapFactory.decodeResource(getContext().getResources(),
                        R.drawable.eddystone);
                image.setImageBitmap(bmp);
            } else {
                // Do nothing
            }

            bmp = BitmapFactory.decodeResource(getContext().getResources(),
                    R.drawable.eddystone, null);
            int w = bmp.getWidth();
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mBeaconWidth, mBeaconHeight);
            params.leftMargin = columnPosition;
            //params.leftMargin = columnPosition - (mImageWidth/2);
            params.topMargin = mHeight - beaconInfo.mRow;
            beaconView.setLayoutParams(params);

            RadarLayout.this.addView(beaconView);

            beaconView.setOnClickListener(onBeaconClickListener);
            beaconView.setId(beaconInfo.max + beaconInfo.min);

            //Update Beacons Information
            beaconInfo.vBeacon = beaconView;
            beaconInfo.radarCol = 0;
            beaconInfo.radarRow = 0;

            //Set Beacon Min & Max to fetch it's details on click
            String tag = "" + beaconInfo.max + "/" + beaconInfo.min;
            beaconView.setTag(beaconInfo);
            mBeaconsInfoCache.add(beaconInfo);

            //Re show popup on previous beacon if re added and not cancelled by user
            if (mSelectedBeacon != null) {
                BeaconInfo selBeaconInfo = (BeaconInfo) mSelectedBeacon.getTag();
                if (selBeaconInfo.isEquals(beaconInfo)) {
                    mSelectedBeacon = beaconView;
                    mainActivity = (AtmelMainActivity) getContext();
                    MainScreenFragment mainScreenFragment = (MainScreenFragment) mainActivity.getFragmentManager().
                            findFragmentByTag(MainScreenFragment.TAG);
                    if (!mainScreenFragment.checkInformationViewVisisbility()) {
                        mPopOver.showPopup(beaconView, beaconInfo);
                    }
                }
            }
            //}

            mhmBeacons.put(beaconInfo.mBeaconKey, beaconInfo);

            /*for(int i=0; i< 10 ; i++) {
                float t[] = {beaconInfo.mRow, beaconInfo.mRow + 10};

                Log.d("animation", "mrow" + beaconInfo.mRow);
                Log.d("animation", "rowPosition" + rowPosition);
                ObjectAnimator translate = ObjectAnimator.ofPropertyValuesHolder(beaconInfo.vBeacon,
                        PropertyValuesHolder.ofFloat("translationY", t));
                translate.setDuration(MOVE_ANIMATION_DURATION);
                translate.start();
                beaconInfo.mRow = beaconInfo.mRow + 10;
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }*/

        }

        /**
         * Moves existing Beacon from current row to new row
         *
         * @param beaconInfo
         */
        private void moveBeaconToNewRow(final BeaconInfo beaconInfo) {
            int count = 0;
            int radius = setProgress(beaconInfo.rssi);
            // final int rowPosition = (int)Math.sqrt((double) (radius * radius) - (beaconInfo.mColumn * beaconInfo.mColumn));
            final int rowPosition = radius;

            if (beaconInfo.vBeacon != null) {

                if (beaconInfo.vBeacon == mSelectedBeacon) {
                    //   mPopOver.hidePopup();
                }

                beaconInfo.mRow = rowPosition;
                final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) beaconInfo.vBeacon.getLayoutParams();
                RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) beaconInfo.vBeacon.getLayoutParams();
                /*params.leftMargin = beaconInfo.mColumn;
                params.topMargin = mHeight - beaconInfo.mRow;
                beaconInfo.vBeacon.setLayoutParams(params);
                invalidate();*/
                beaconInfo.vBeacon.animate().x(beaconInfo.mColumn).y(mHeight - beaconInfo.mRow);
                //  mPopOver.doAnimation(beaconInfo.mColumn, mHeight - beaconInfo.mRow);
              /*  TranslateAnimation anim = new TranslateAnimation(0, 0, 0, mHeight - beaconInfo.mRow-params.topMargin+mBeaconHeight );
                anim.setDuration(150);
                anim.setFillAfter(true);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        params.leftMargin = beaconInfo.mColumn;
                        params.topMargin = mHeight - beaconInfo.mRow;
                        beaconInfo.vBeacon.setLayoutParams(params);
                        invalidate();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                beaconInfo.vBeacon.startAnimation(anim);*/


                if (beaconInfo.vBeacon == mSelectedBeacon) {
                    mSelectedBeacon = beaconInfo.vBeacon;
                    //Applying new position to beacon taking some time, so delaying creating popover operation
                    mainActivity = (AtmelMainActivity) getContext();
                    final MainScreenFragment mainScreenFragment = (MainScreenFragment) mainActivity.getFragmentManager().
                            findFragmentByTag(MainScreenFragment.TAG);
                    if (!mainScreenFragment.checkInformationViewVisisbility()) {
                        mPopOver.showPopup(beaconInfo.mColumn, mHeight - beaconInfo.mRow, beaconInfo.vBeacon, beaconInfo);
                    }
                   /* mPopOver.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 50);*/

                }
                mainActivity = (AtmelMainActivity) getContext();
                MainScreenFragment mainScreenFragment = (MainScreenFragment) mainActivity.getFragmentManager().
                        findFragmentByTag(MainScreenFragment.TAG);
                if (beaconInfo.vBeacon == mSelectedBeacon) {
                    //update information view of beacons with new data
                    mainScreenFragment.updateBeaconInformation(beaconInfo);
                }

                if (!mainScreenFragment.checkProximityViewVisisbility()) {
                    beaconInfo.mProximityState = false;
                }
                //check proximity state is selected or not
                if (mainActivity.mProximityState) {
                    Log.e("beaconInfo.rssi",Math.abs(beaconInfo.rssi)+"");
                    if (Math.abs(beaconInfo.rssi) < Constants.IMMEDIATE_RANGE) {
                        //check particular beacon's proximity view is showing currently
                        if (!beaconInfo.mProximityState && !beaconInfo.mUrl.isEmpty()) {
                            mainActivity.showProximityLayout(beaconInfo.mUrl);
                            beaconInfo.mProximityState = true;
                        }

                    } else {
                        //since the beacon moved out of near region, close the proximity view
                        if (beaconInfo.mProximityState) {
                            mainActivity.disableProximityLayout();
                            beaconInfo.mProximityState = false;
                        }
                    }
                }
            }
        }


        public int getColumnPosition() {
            int columnPosition = 0;
            //Integer row  =  new Integer(rowPostiton);
            ArrayList<Integer> columnList = new ArrayList<>();
            Integer halfWidth = Integer.valueOf(mWidth / 2);
           /* if(!mColumnList.contains(halfWidth)){
                mColumnList.add(halfWidth);
            }else {*/
            // if (mColumnList != null && mColumnList.size() > 0) {

                    /*
                    if (!mColumnList.contains(halfWidth)) {
                        mColumnList.add(halfWidth);
                        columnPosition = mWidth/2;
                        mColumnList.add(columnPosition);
                    } else {*/
            int coulumnsCount = 0;
            if (columnList != null) {
                coulumnsCount = columnList.size();
            }
            int diff = 0;
            int prevVal = 0;
            int prevDiff = 0;
            int selectedDiff = 0;
            Integer zeroWidth = Integer.valueOf(0);

            Integer fullWidth = Integer.valueOf(mWidth);

            //columnList.add(halfWidth);
            if (mColumnList != null && mColumnList.size() > 0) {
                columnList.addAll(mColumnList);
            }
            columnList.add(zeroWidth);
            columnList.add(fullWidth);
            Collections.sort(columnList);



                /*for (Integer column : columnList) {
                      diff = column - prevVal;
                    if(diff > selectedDiff){
                        selectedDiff = diff;
                    }
                }*/
            diff = 0;
            selectedDiff = 0;
            for (int i = 0; i < columnList.size(); i++) {

                // for (int j = 0; j < columnList.size(); j++) {
                if (i + 1 < columnList.size()) {
                    diff = Math.abs(columnList.get(i + 1) - columnList.get(i));
                    if (diff > selectedDiff) {
                        prevVal = columnList.get(i);
                        selectedDiff = diff;
                    }
                }
                //}
            }
            columnPosition = prevVal + selectedDiff / 2;
            mColumnList.add(columnPosition);
            System.out.println("coloum list " + mColumnList.toString());
            // }
               /* }else{
                    columnList.add(halfWidth);
                    columnPosition = mWidth/2;
                    mColumnList.add(columnPosition);
                }*/


            return columnPosition;
        }

        /**
         * This will be called from MainScreenFragment on receiving beacon details update
         *
         * @param beaconsCB
         * @returid cal
         */
        public void onBeaconCBReceived(Collection<Beacon> beaconsCB) {
            final ArrayList<Beacon> beacons = new ArrayList<>(beaconsCB);
            final ArrayList<BeaconInfo> allBeaconInfo = new ArrayList<>(mBeaconsInfoCache);
            final ArrayList<BeaconInfo> beaconShouldBeUpdatedList = new ArrayList<>();
            final HashMap<String, BeaconInfo> receivedMap = new HashMap<>();
            String beaconKey = null;
            long telemetryVersion = 0;
            long batteryMilliVolts = 0;
            long pduCount = 0;
            long uptime = 0;
            long tempararture = 0;
            String url = null;
            int eddystoneFrameType = 0;
            int type = 0;
          /*  mColumnList.clear();
            for (Beacon beacon : beacons) {
                for (BeaconInfo beaconInfo : allBeaconInfo) {
                    if (beaconInfo.mAddress.contentEquals(beacon.getBluetoothAddress())) {
                        mColumnList.add(beaconInfo.mColumn);
                        break;
                    }
                }


            }*/
            Log.i(RadarLayout.class.getSimpleName(), "Beacon State colums" + mColumnList.toString());

            for (final Beacon beacon : beacons) {
                int code = beacon.getBeaconTypeCode();
                if (code == 533) {
                    type = 0;
                } else {
                    type = 2;
                }

                if (beacon.getServiceUuid() == 0xfeaa && beacon.getBeaconTypeCode() == 0x00) {
                    type = 1;
                    eddystoneFrameType = 1;
                    // This is a Eddystone-UID frame
                    Identifier namespaceId = beacon.getId1();
                    Identifier instanceId = beacon.getId2();
                    Log.d(TAG, "I see a beacon transmitting namespace id: " + namespaceId +
                            " and instance id: " + instanceId +
                            " approximately " + beacon.getDistance() + " meters away.");

                    // Do we have telemetry data?
                    if (beacon.getExtraDataFields().size() > 0) {
                        telemetryVersion = beacon.getExtraDataFields().get(0);
                        batteryMilliVolts = beacon.getExtraDataFields().get(1);
                        tempararture = beacon.getExtraDataFields().get(2);
                        pduCount = beacon.getExtraDataFields().get(3);
                        uptime = beacon.getExtraDataFields().get(4);

                        Log.d(TAG, "The above beacon is sending telemetry version " + telemetryVersion +
                                ", has been up for : " + uptime + " seconds" +
                                ", has a battery level of " + batteryMilliVolts + " mV" +
                                ", and has transmitted " + pduCount + " advertisements.");

                    }
                } else if ((beacon.getServiceUuid() == 0xfeaa && beacon.getBeaconTypeCode() == 0x10)) {
                    eddystoneFrameType = 2;
                    type = 1;
                    // This is a Eddystone-URL frame
                    url = UrlBeaconUrlCompressor.uncompress(beacon.getId1().toByteArray());

                    Log.d(TAG, "I see a beacon transmitting a url: " + url +
                            " approximately " + beacon.getDistance() + " meters away.");
                    // Do we have telemetry data?
                    if (beacon.getExtraDataFields().size() > 0) {
                        telemetryVersion = beacon.getExtraDataFields().get(0);
                        batteryMilliVolts = beacon.getExtraDataFields().get(1);
                        tempararture = beacon.getExtraDataFields().get(2);
                        pduCount = beacon.getExtraDataFields().get(3);
                        uptime = beacon.getExtraDataFields().get(4);

                        Log.d(TAG, "The above beacon is sending telemetry version " + telemetryVersion +
                                ", has been up for : " + uptime + " seconds" +
                                ", has a battery level of " + batteryMilliVolts + " mV" +
                                ", and has transmitted " + pduCount + " advertisements.");

                    }
                }
                List<Identifier> identifiers = beacon.getIdentifiers();
                if (type == Constants.IBEACON_IDEFR || type == Constants.ALTBEACON_IDEFR) {
                    beaconKey = beaconKey(identifiers);
                } else {
                    beaconKey = beacon.getBluetoothAddress();
                }

                BeaconInfo beaconInfo = mhmBeacons.get(beaconKey);
                int beaconRowPos = getRowNumberByInstance(beacon.getDistance());
                beaconInfo = createOrUpdateBeaconInfo(beaconInfo, beaconRowPos, identifiers, beacon, eddystoneFrameType, type);

                if ((eddystoneFrameType == 1) || (eddystoneFrameType == 2)) {
                    beaconInfo.mTlmBatteryVoltage = batteryMilliVolts;
                    beaconInfo.mPduCount = pduCount;
                    beaconInfo.mTlmUptime = uptime;
                    beaconInfo.mTlmTemparature = tempararture;
                }
                beaconInfo.mUrl = "";
                if (eddystoneFrameType == 2 && type == Constants.EDDYSTONE_IDEFR) {
                    beaconInfo.mUrl = url;
                } else if(eddystoneFrameType!=1){
                    beaconInfo.mUrl = beaconListDbHandler.getUrlOfMatchingBeacon(beaconInfo.mUuid, String.valueOf(beaconInfo.max),
                            String.valueOf(beaconInfo.min));
                }

                beaconInfo.mBluetoothName = beacon.getBluetoothName();
                beaconInfo.mManufacture = beacon.getManufacturer();
                beaconInfo.mAddress = beacon.getBluetoothAddress();

                beaconInfo.mBeaconKey = beaconKey;
                mhmBeacons.put(beaconKey, beaconInfo);
                receivedMap.put(beaconKey, beaconInfo);
                beaconShouldBeUpdatedList.add(beaconInfo);
               /* if (beaconInfo.beaconState != eBeaconState.BEACON_STATE_UNCHANGED) {
                    beaconShouldBeUpdatedList.add(beaconInfo);
                }*/
            }

            //Update It on seperate thread
            post(new Runnable() {
                @Override
                public void run() {

                    //if selectedBeacon is null, hide popup
                    if (mSelectedBeacon == null) {
                        mPopOver.hidePopup();
                    }

                    //Remove unreceivied beacons info in the callback from the view
                    Iterator<String> beaconInfoCollectionKeyst = mhmBeacons.keySet().iterator();
                    ArrayList<String> keysShouldBeRemoved = new ArrayList<>();
                    while (beaconInfoCollectionKeyst.hasNext()) {
                        String key = beaconInfoCollectionKeyst.next();
                        BeaconInfo beaconInfo = receivedMap.get(key);
                        if (beaconInfo == null) {
                            //We didn't receive information of this beacon in this callback, so remove it
                            beaconInfo = mhmBeacons.get(key);

                            if ((System.currentTimeMillis() - beaconInfo.lastUpdateTimestamp) > BEACON_TIMEOUT_TO_REMOVE_FROM_SCREEN) {
                                mainActivity = (AtmelMainActivity) getContext();
                                if (beaconInfo.mProximityState) {
                                    mainActivity.disableProximityLayout();
                                    beaconInfo.mProximityState = false;
                                }
                                if (beaconInfo.vBeacon == mSelectedBeacon) {
                                    mPopOver.hidePopup();
                                }
                                removeView(beaconInfo.vBeacon);
                                int in = mColumnList.indexOf(beaconInfo.mColumn);
                                if (in > -1) {
                                    mColumnList.remove(in);
                                }
                                mBeaconsInfoCache.remove(beaconInfo);
                                keysShouldBeRemoved.add(key);
                            }


                            //mFreeColumnsOnRow.get(beaconInfo.radarRow)[beaconInfo.radarCol] = COLUMN_EMPTY;
                        }
                    }
                    for (String keyToRemove : keysShouldBeRemoved) {
                        mhmBeacons.remove(keyToRemove);
                    }
                    //Update Existing Beacons Information
                    for (BeaconInfo beaconInfo : beaconShouldBeUpdatedList) {
                        switch (beaconInfo.beaconState) {
                            case BEACON_STATE_NEW:
                                Log.i(RadarLayout.class.getSimpleName(), "Beacon State Newly Added");
                                addNewBeaconToRow(beaconInfo);
                                beaconInfo.mRssivalues.add(beaconInfo.mCount, beaconInfo.rssi);
                                break;

                            case BEACON_STATE_MOVED:
                                Log.i(RadarLayout.class.getSimpleName(), "Beacon State Moved");
                                beaconInfo.mCount++;
                                if (beaconInfo.mCount == SAMPLING_COUNT) {
                                    beaconInfo.mCount = 0;
                                }
                                try {
                                    beaconInfo.mRssivalues.add(beaconInfo.mCount, beaconInfo.rssi);
                                    beaconInfo.rssi = ((beaconInfo.mRssivalues.get(0) + beaconInfo.mRssivalues.get(1) + beaconInfo.mRssivalues.get(2) +
                                            beaconInfo.mRssivalues.get(3) + beaconInfo.mRssivalues.get(4) +
                                            beaconInfo.mRssivalues.get(5) + beaconInfo.mRssivalues.get(6) + beaconInfo.mRssivalues.get(7)
                                            + beaconInfo.mRssivalues.get(8) + beaconInfo.mRssivalues.get(9)) / SAMPLING_COUNT);
                                    //beaconInfo.rssi = ((beaconInfo.mRssivalues.get(0) + beaconInfo.mRssivalues.get(1) + beaconInfo.mRssivalues.get(2) +
                                     //      beaconInfo.mRssivalues.get(3)) / SAMPLING_COUNT);
                                    //beaconInfo.rssi = beaconInfo.mRssivalues.get(0);
                                    Log.d("DEBUG_LOG","beaconInfo.rssi = "+beaconInfo.rssi);
                                    Log.d("DEBUG_LOG", "beaconInfo.distance = "+ beaconInfo.distance);
                                    Log.d("DEBUG_LOG", "beaconInfo.mrow = "+ beaconInfo.mRow);
                                    Log.d("DEBUG_LOG", "beaconInfo.radarrow = "+ beaconInfo.radarRow);
                                    Log.d("DEBUG_LOG", "beaconInfo.mTxPower = "+ beaconInfo.mTxPower);
                                            moveBeaconToNewRow(beaconInfo);
                                    //Log.e("RSSI>> " + rssi.getRSSI(), "Avg RSSI>>" + average);

                                } catch (IndexOutOfBoundsException e) {
                                    moveBeaconToNewRow(beaconInfo);
                                    e.printStackTrace();
                                }
                                break;

                            case BEACON_STATE_REMOVED:
                                Log.i(RadarLayout.class.getSimpleName(), "Beacon State removed");
                                if (mColumnList != null && mColumnList.size() > 0) {

                                    if (mColumnList.contains(Integer.valueOf(beaconInfo.mColumn))) {

                                        mColumnList.remove(Integer.valueOf(beaconInfo.mColumn));
                                    }
                                }
                                //TODO: Handle removal
                                break;

                            case BEACON_STATE_UNCHANGED:
                                //Do Nothing
                                Log.i(RadarLayout.class.getSimpleName(), "Beacon State unchanged");
                                break;
                        }
                        beaconInfo.beaconState = eBeaconState.BEACON_STATE_UNCHANGED;
                    }

                    //updatePopoverText();
                }
            });

        }

        /**
         * Returns valid empty position, -1 otherwise
         *
         * @return
         */
        public int getFreeColumnIndex(int row) {
            int colPos = MAX_COLS_PER_SECTOR / 2;
            int direction = -1;
            boolean isFreeColFound = false;
            int[] colBeacons = mFreeColumnsOnRow.get(row);
            for (int i = 0; i < MAX_COLS_PER_SECTOR; i++) {
                if (colBeacons[colPos] == COLUMN_EMPTY) {
                    isFreeColFound = true;
                    break;
                }

                colPos = (MAX_COLS_PER_SECTOR / 2) + ((i / 2 + 1) * direction);
                direction *= -1;

                /**
                 * if max col position reached check whether lower positions are available,
                 * if not it means we left with no column. Just break and don't add it.
                 */
                if (colPos >= MAX_COLS_PER_SECTOR) {
                    colPos = (MAX_COLS_PER_SECTOR / 2) + ((i / 2 + 1) * direction);
                }

                if (colPos < 0) {
                    //We can't find any more free columns just return
                    break;
                }
            }

            return isFreeColFound ? colPos : -1;
        }

        //API Method, it will be requested from main screne fragment.
        public BeaconInfo getBeaconInfoFromVisibleBeacons(BeaconInfo beaconInfo) {
            return mhmBeacons.get("" + beaconInfo.max + "/" + beaconInfo.min);
        }
    }

    public class BeaconInfo {
        public View vBeacon;
        public double distance;
        public eBeaconState beaconState;
        public double rssi;
        public long lastUpdateTimestamp;
        public int max;
        public int min;
        public int radarRow;
        public int radarCol;
        public int newRadarRow;
        public int newRadarCol;
        public String mNameSpaceId;
        public String mInstanceId;
        public int mType;
        public String mUrl = "";
        public String mUuid;
        public long mTlmBatteryVoltage;
        public long mTlmTemparature;
        public long mTlmUptime;
        public int mTxPower;
        public int mRow;
        public int mColumn;
        public long mPduCount;
        public String mBeaconKey;
        //sampling rate of rssi update
        public int mCount = 0;
        public int mManufacture = 0;
        public String mBluetoothName;
        public ArrayList<Double> mRssivalues = new ArrayList<>(10);
        public String mAddress;
        //flag to identify the proximity view is visible
        public Boolean mProximityState = false;

        public BeaconInfo() {

        }

        public BeaconInfo getCopy() {
            BeaconInfo newObj = new BeaconInfo();
            newObj.max = this.max;
            newObj.min = this.min;
            newObj.distance = this.distance;
            newObj.rssi = this.rssi;
            newObj.vBeacon = this.vBeacon;
            newObj.radarRow = this.radarRow;
            newObj.radarCol = this.radarCol;
            newObj.beaconState = this.beaconState;
            newObj.newRadarRow = this.radarRow;
            newObj.newRadarCol = this.radarCol;

            return newObj;
        }

        public boolean isEquals(BeaconInfo beaconInfo) {
            return (this.max == beaconInfo.max && this.min == beaconInfo.min);
        }

    }

}
