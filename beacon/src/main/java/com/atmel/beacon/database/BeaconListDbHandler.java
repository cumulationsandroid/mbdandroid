/*
 *
 *
 *     BeaconListDbHandler.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.beacon.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.atmel.beacon.datasource.BeaconInformationModel;

import java.util.ArrayList;
import java.util.List;

public class BeaconListDbHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "beaconManager";

    // table name
    private static final String TABLE_BEACONS = "beacons";

    // Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_MAJOR = "major";
    private static final String KEY_MINOR = "minor";
    private static final String KEY_UUID = "uuid";
    private static final String KEY_URL = "url";
    private static final String KEY_TYPE = "type";
    private static final String KEY_DEFAULT = "default_beacon";

    public BeaconListDbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BEACON_TABLE = "CREATE TABLE " + TABLE_BEACONS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_NAME + " TEXT,"
                + KEY_UUID + " TEXT,"
                + KEY_MAJOR + " TEXT,"
                + KEY_MINOR + " TEXT,"
                + KEY_TYPE + " TEXT,"
                + KEY_URL + " TEXT,"
                + KEY_DEFAULT + " INT NOT NULL DEFAULT '0'" + ")";
        db.execSQL(CREATE_BEACON_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACONS);
        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new beacon
    public void addBeacon(BeaconInformationModel beaconInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, beaconInfo.mName); // beacon name
        values.put(KEY_UUID, beaconInfo.mUuid); //beacon uuid
        values.put(KEY_MAJOR, beaconInfo.mMajor); // beacon major id
        values.put(KEY_MINOR, beaconInfo.mMinor); //beacon minor id
        values.put(KEY_URL, beaconInfo.mUrl); //beacon url
        values.put(KEY_TYPE, beaconInfo.mType); //beacon type
        values.put(KEY_DEFAULT, (beaconInfo.mDefault ? 1 : 0));//default beacon or not
        // Inserting Row
        db.insert(TABLE_BEACONS, null, values);
        db.close(); // Closing database connection
    }


    // Getting All Contacts
    public List<BeaconInformationModel> getAllBeacons() {
        List<BeaconInformationModel> beaconList = new ArrayList<BeaconInformationModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BEACONS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    BeaconInformationModel beacon = new BeaconInformationModel();
                    beacon.mBeaconId = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                    beacon.mName = cursor.getString(cursor.getColumnIndex(KEY_NAME));
                    beacon.mUuid = cursor.getString(cursor.getColumnIndex(KEY_UUID));
                    beacon.mMajor = cursor.getString(cursor.getColumnIndex(KEY_MAJOR));
                    beacon.mMinor = cursor.getString(cursor.getColumnIndex(KEY_MINOR));
                    beacon.mUrl = cursor.getString(cursor.getColumnIndex(KEY_URL));
                    beacon.mType = cursor.getInt(cursor.getColumnIndex(KEY_TYPE));
                    beacon.mDefault = cursor.getInt(cursor.getColumnIndex(KEY_DEFAULT)) == 1;
                    beaconList.add(beacon);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        // return beacon list
        return beaconList;
    }

    // Updating single beacon
    public int updateBeacon(BeaconInformationModel beaconInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, beaconInfo.mName); // beacon name
        values.put(KEY_UUID, beaconInfo.mUuid); //beacon uuid
        values.put(KEY_MAJOR, beaconInfo.mMajor); // beacon major id
        values.put(KEY_MINOR, beaconInfo.mMinor); //beacon minor id
        values.put(KEY_URL, beaconInfo.mUrl); //beacon url
        values.put(KEY_TYPE, beaconInfo.mType); //beacon type
        // updating row
        return db.update(TABLE_BEACONS, values, KEY_ID + " = ?",
                new String[]{("" + beaconInfo.mBeaconId)});
    }

    // Deleting single beacon
    public void deleteBeacon(BeaconInformationModel beaconInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = "uuid = ? AND  major = ?  AND minor = ?";
        String[] whereArgs = new String[]{
                beaconInfo.mUuid,
                beaconInfo.mMajor,
                beaconInfo.mMinor
        };
        db.delete(TABLE_BEACONS, whereClause,
                whereArgs);
        db.close();
    }


    // Getting contacts Count
    public int getBeaconsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_BEACONS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }

    public int getCountOfMatchingBeacons(String id1, String id2, String id3) {
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String whereClause = "uuid = ? AND  major = ?  AND MINOR = ?";
        String[] countQuery = new String[]{
                KEY_NAME
        };
        String[] whereArgs = new String[]{
                id1,
                id2,
                id3
        };
        final Cursor cursor = db.query(true, TABLE_BEACONS, countQuery,
                whereClause, whereArgs, null,
                null, null, null);
        if (cursor != null) {
            count = cursor.getCount();
            cursor.close();
        }

        return count;
    }

    public String getUrlOfMatchingBeacon(String id1, String id2, String id3) {
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String whereClause = "uuid = ? AND  major = ?  AND MINOR = ?";
        String[] countQuery = new String[]{
                KEY_URL
        };
        String[] whereArgs = new String[]{
                id1,
                id2,
                id3
        };
        final Cursor cursor = db.query(true, TABLE_BEACONS, countQuery,
                whereClause, whereArgs, null,
                null, null, null);
        String url = "";

        if (cursor != null) {
            count = cursor.getCount();
            if (count > 0) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    url = cursor.getString(cursor.getColumnIndex(KEY_URL));
                }
            }
            cursor.close();
        }
        return url;
    }

    public int getIdOfMatchingBeacon(String id1, String id2, String id3) {
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String whereClause = "uuid = ? AND  major = ?  AND MINOR = ?";
        String[] countQuery = new String[]{
                KEY_ID
        };
        String[] whereArgs = new String[]{
                id1,
                id2,
                id3
        };
        final Cursor cursor = db.query(true, TABLE_BEACONS, countQuery,
                whereClause, whereArgs, null,
                null, null, null);
        int id = -1;

        if (cursor != null) {
            count = cursor.getCount();
            if (count > 0) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                }
            }
            cursor.close();
        }
        return id;
    }
    public ArrayList<String> getAllBeaconUUIDs() {
        ArrayList<String> beaconUUIDs = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  DISTINCT " + KEY_UUID + " FROM " + TABLE_BEACONS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    beaconUUIDs.add(cursor.getString(cursor.getColumnIndex(KEY_UUID)));
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        // return beacon list
        return beaconUUIDs;
    }
}

