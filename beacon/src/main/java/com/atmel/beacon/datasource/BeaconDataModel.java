/*
 *
 *
 *     BeaconDataModel.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.datasource;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.Identifier;

import java.util.List;

public class BeaconDataModel {

    public String mUUID;
    public int mMin;
    public int mMax;
    public double distance;
    public Beacon currentBeacon;
    public long lastUpdatedTimestamp;

    public BeaconDataModel(Beacon beacon) {
        List<Identifier> identifiers = beacon.getIdentifiers();
        mUUID = identifiers.get(0).toString();
        mMin = identifiers.get(1).toInt();
        mMax = identifiers.get(2).toInt();
        distance = beacon.getDistance();
        currentBeacon = beacon;
        lastUpdatedTimestamp = System.currentTimeMillis();
    }

    public static boolean isBeaconsAreEqual(Beacon b1, Beacon b2) {
        List<Identifier> b1Identifiers = b1.getIdentifiers();
        List<Identifier> b2Identifiers = b2.getIdentifiers();
        return (/*b1.getIdentifiers().get(0).toString().equalsIgnoreCase(b2.getIdentifiers().get(0).toString()) &&*/
                b1Identifiers.get(1).toInt() == b2Identifiers.get(1).toInt()
                        && b1Identifiers.get(2).toInt() == b2Identifiers.get(2).toInt());
    }

    public boolean isEqual(Beacon beacon) {
        List<Identifier> identifiers = beacon.getIdentifiers();
        return (/*mUUID.equalsIgnoreCase(identifiers.get(0).toString()) &&*/ mMin == identifiers.get(1).toInt()
                && mMax == identifiers.get(2).toInt());
    }
}
