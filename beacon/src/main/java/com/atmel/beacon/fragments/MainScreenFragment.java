/*
 *
 *
 *     MainScreenFragment.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.atmel.beacon.BeaconListener;
import com.atmel.beacon.R;
import com.atmel.beacon.activities.AtmelMainActivity;
import com.atmel.beacon.activities.EddystoneScanActivity;
import com.atmel.beacon.customcontrols.AtmelPopover;
import com.atmel.beacon.customcontrols.RadarLayout;
import com.atmel.beacon.database.BeaconListDbHandler;
import com.atmel.beacon.datasource.BeaconDataModel;
import com.atmel.beacon.util.Constants;
import com.atmel.beacon.util.Util;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.Region;

import java.util.ArrayList;
import java.util.Collection;


public class MainScreenFragment extends Fragment implements BeaconListener.AtmelBeaconListener,
        View.OnClickListener {
    public static final String TAG = "MainScreenFragment";
    // Unseen beacon expiry time
    private static final int UNSEEN_BEACON_EXPIRY_TIME = 1000 * 30;
    public final int ATMEL_MANUFACTURE_ID = 19;
    // AtmelMainActivity object
    public AtmelMainActivity mMainActivity;
    public RelativeLayout mRadarLayout;
    public RelativeLayout mProximityLayout;
    // Beacons DB handler
    BeaconListDbHandler mBeaconListDbHandler = null;
    long lastCallbackReceivedTime = 0;
    // Beacons ArrayList
    private ArrayList<BeaconDataModel> mBeaconsList = new ArrayList<>();
    // Beacons plotting layout
    private RadarLayout mRadarView;
    // Object to identify beacon current mode
    private eBeaconMode mBeaconCurrentMode = eBeaconMode.MODE_DISTANCE;
    // CoordinatorLayout view for showing snack bar
    private View mCoordinatorLayoutView;
    // Object of BluetoothAdapter
    private BluetoothAdapter mBluetoothAdapter;
    private ScrollView mBeaconInformationLayout;
    private RelativeLayout mIbeaconLayout = null;
    private RelativeLayout mEddystoneLayout = null;
    private RelativeLayout mAltbeaconLayout = null;
    private RelativeLayout mConfigureLayout = null;
    //  views for displaying iBeacon information's
    private TextView mIbeaconUrlContentTextView = null;
    private TextView mIbeaconMajorContentTextView = null;
    private TextView mIbeaconMinorContentTextView = null;
    private TextView mIbeaconUuidContentTextView = null;
    private TextView mIbeaconRssiContentTextView = null;
    //  views for displaying Eddystone information's
    private TextView mEddystoneNamespaceTextView = null;
    private TextView mEddystoneInstanceTextView = null;
    private TextView mEddystoneTxpowerTextView = null;
    private TextView mEddystoneUrlContentTextView = null;
    private TextView mEddystoneUrlTextView = null;
    private TextView mEddystoneTlmVoltageTextView = null;
    private TextView mEddystoneTlmTempTextView = null;
    private TextView mEddystoneTlmUptimeTextView = null;
    private TextView mConfigureTextView = null;
    private TextView mEddystoneRssiTextView = null;
    private TextView mEddystonePDUCountTextView = null;
    //  views for displaying Alt beacon information's
    private TextView mAltbeaconId1ContentTextView = null;
    private TextView mAltbeaconId2ContentTextView = null;
    private TextView mAltbeaconId3ContentTextView = null;
    private TextView mAltbeaconRssiContentTextView = null;
    private TextView mAltbeaconUrlContentTextView = null;
    private TextView mAltbeaconManufactureTextView = null;
    // Bottom buttons and Layout
    private Button mProximityButton;
    private Button mDistanceButton;
    private LinearLayout mBottomLayout;
    // Web View for loading the URL
    private WebView mWebview;
    // Progress bar object for  webview loading
    private ProgressBar mProgressBar;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_mainscreen, parent, false);
        Util.consumeTouch(view);
        mRadarView = (RadarLayout) view.findViewById(R.id.fragmentMainScreenRaderView);
        final AtmelPopover bubbleText = (AtmelPopover) view.findViewById(R.id.fragmentMainScreentvBubble);
        mRadarView.setPopupView(bubbleText);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mCoordinatorLayoutView = view.findViewById(R.id.snackbarPosition);
        mBeaconInformationLayout = (ScrollView) view.findViewById(R.id.beacon_information);
        BeaconListener.addListeners(this);
        mIbeaconMajorContentTextView = (TextView) view.findViewById(R.id.major_content);
        mIbeaconMinorContentTextView = (TextView) view.findViewById(R.id.minor_content);
        mIbeaconUuidContentTextView = (TextView) view.findViewById(R.id.uuid_content);
        mIbeaconRssiContentTextView = (TextView) view.findViewById(R.id.ibeacon_rssi_content);
        mIbeaconUrlContentTextView = mEddystoneUrlTextView = (TextView) view.findViewById(R.id.url_content);
        mEddystoneUrlContentTextView = (TextView) view.findViewById(R.id.eddystone_url_content);
        mEddystoneTlmVoltageTextView = (TextView) view.findViewById(R.id.eddystone_voltage_content);
        mEddystoneTlmTempTextView = (TextView) view.findViewById(R.id.eddystone_temparature_content);
        mEddystoneTlmUptimeTextView = (TextView) view.findViewById(R.id.eddystone__uptime_content);
        mConfigureTextView = (TextView) view.findViewById(R.id.configure_button);
        mEddystoneNamespaceTextView = (TextView) view.findViewById(R.id.eddystone_namespace_content);
        mEddystoneInstanceTextView = (TextView) view.findViewById(R.id.eddystone_instanceid_content);
        mEddystoneTxpowerTextView = (TextView) view.findViewById(R.id.txpower_title_content);
        mIbeaconLayout = (RelativeLayout) view.findViewById(R.id.iBeacon_infoLayout);
        mEddystoneLayout = (RelativeLayout) view.findViewById(R.id.eddystone_infoLayout);
        mEddystoneRssiTextView = (TextView) view.findViewById(R.id.rssi_content);
        mEddystonePDUCountTextView = (TextView) view.findViewById(R.id.eddystone_pdu_content);
        mAltbeaconLayout = (RelativeLayout) view.findViewById(R.id.altBeacon_infoLayout);
        mAltbeaconId1ContentTextView = (TextView) view.findViewById(R.id.altbeacon_id1_content);
        mAltbeaconId2ContentTextView = (TextView) view.findViewById(R.id.altbeacon_id2_content);
        mAltbeaconId3ContentTextView = (TextView) view.findViewById(R.id.altbeacon_id3_content);
        mAltbeaconRssiContentTextView = (TextView) view.findViewById(R.id.altbeacon_rssi_content);
        mAltbeaconUrlContentTextView = (TextView) view.findViewById(R.id.altbeacon_url_content);
        mConfigureLayout = (RelativeLayout) view.findViewById(R.id.configure_layout);
        mAltbeaconManufactureTextView = (TextView) view.findViewById(R.id.altbeacon_manufacture_content);
        mProximityButton = (Button) view.findViewById(R.id.proximity_imageview);
        mProximityButton.setOnClickListener(this);
        mDistanceButton = (Button) view.findViewById(R.id.distance_imageview);
        mDistanceButton.setOnClickListener(this);
        mProximityButton.setTransformationMethod(null);
        mDistanceButton.setTransformationMethod(null);
        mBottomLayout = (LinearLayout) view.findViewById(R.id.bottom_layout);
        mConfigureTextView.setOnClickListener(this);
        mRadarLayout = (RelativeLayout) view.findViewById(R.id.radar_layout);
        mProximityLayout = (RelativeLayout) view.findViewById(R.id.proximity_layout);
        mBeaconListDbHandler = new BeaconListDbHandler(this.getActivity());
        mMainActivity = (AtmelMainActivity) getActivity();
        //proximity view
        mWebview = (WebView) view.findViewById(R.id.url_webview);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mWebview.setWebViewClient(new urlWebviewClient());
        mWebview.getSettings().setLoadsImagesAutomatically(true);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setDisplayZoomControls(true);
        mWebview.getSettings().setSupportZoom(true);
        mWebview.getSettings().setBuiltInZoomControls(true);
        // mWebview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mDistanceButton.setSelected(true);
        mDistanceButton.setTextColor(getResources().getColor(R.color.colorPrimary));
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        //check bluetotoh status, if off show snackbar
        if (mBluetoothAdapter != null) {
            if (!mBluetoothAdapter.isEnabled()) {
                showSnackBar();
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    /**
     * Show message if the Bluetooth is turned off
     */
    public void showSnackBar() {

        Snackbar.make(mCoordinatorLayoutView,
                getString(R.string.bluetooth_message), Snackbar.LENGTH_LONG).setAction(getString(R.string.turn_on),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mBluetoothAdapter.enable();
                    }
                }).show();
    }


    /***
     * AtmelBeacon Listener methods start
     ****/
    @Override
    public void didEnterRegion(Region region) {

    }

    @Override
    public void didExitRegion(Region region) {

    }

    @Override
    public void didDetermineStateForRegion(int i, Region region) {

    }

    /**
     * Function call when the beacon in immediate region
     *
     * @param beacons
     * @param region
     */
    @Override
    public void didRangeBeaconsInRegion(final Collection<Beacon> beacons, Region region) {


        if (lastCallbackReceivedTime == 0) {
            lastCallbackReceivedTime = System.currentTimeMillis();
        } else {
            //   System.out.println("Callback received interval : " + (System.currentTimeMillis() - lastCallbackReceivedTime) % 1000);
            lastCallbackReceivedTime = System.currentTimeMillis();
        }
        System.out.println("Beacons Size : " + beacons.size());

        mRadarView.onBeaconCBReceived(beacons);
        //System.out.println("CurTime : " + System.currentTimeMillis() + "EnteredTime : " + curTime + "Difference : " + (System.currentTimeMillis() - curTime));

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mBeaconCurrentMode == eBeaconMode.MODE_PROXIMITY) {
                        RadarLayout.BeaconInfo nearest = mRadarView.getNearestBeacon();
                        //launchBeaconInfoScreen(nearest);
                    } else if (mBeaconCurrentMode == eBeaconMode.MODE_NOTIFICATION) {
                        RadarLayout.BeaconInfo nearest = mRadarView.getNearestBeacon();
                        //handleNotificationMode(nearest);
                    }
                }
            });
        }
    }

    /***
     * Run time Permission handling for Android 6
     */

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Update details showing in the information page
     * depends on the type of the beacon
     *
     * @param info Beacon info
     */
    public void updateBeaconInformation(RadarLayout.BeaconInfo info) {
        if (checkInformationViewVisisbility()) {
            switch (info.mType) {
                case Constants.IBEACON_IDEFR:
                    mIbeaconLayout.setVisibility(View.VISIBLE);
                    mEddystoneLayout.setVisibility(View.GONE);
                    mAltbeaconLayout.setVisibility(View.GONE);
                    //check database for assigned url
                    if (mBeaconListDbHandler != null) {
                        mIbeaconUrlContentTextView.setText(mBeaconListDbHandler.getUrlOfMatchingBeacon(info.mUuid, String.valueOf(info.max),
                                String.valueOf(info.min)));
                    }
                    mIbeaconMajorContentTextView.setText(String.valueOf(info.max));
                    mIbeaconMinorContentTextView.setText(String.valueOf(info.min));
                    mIbeaconUuidContentTextView.setText(info.mUuid);
                    mIbeaconRssiContentTextView.setText(String.valueOf(info.rssi) + getString(R.string.dbm));
                    break;
                case Constants.EDDYSTONE_IDEFR:
                    mIbeaconLayout.setVisibility(View.GONE);
                    mAltbeaconLayout.setVisibility(View.GONE);
                    mEddystoneLayout.setVisibility(View.VISIBLE);
                    //show information based on
                    //eddystoneframe
                    if (info.mUrl != null && info.mNameSpaceId != null && info.mInstanceId != null) {
                        mEddystoneUrlContentTextView.setText(info.mUrl);
                        mEddystoneNamespaceTextView.setText(String.valueOf(info.mNameSpaceId));
                        mEddystoneInstanceTextView.setText(String.valueOf(info.mInstanceId));
                        mConfigureTextView.setVisibility(View.GONE);
                    } else if (info.mUrl != null) {
                        mEddystoneNamespaceTextView.setText(R.string.not_applicable);
                        mEddystoneInstanceTextView.setText(R.string.not_applicable);
                        mEddystoneUrlContentTextView.setText(info.mUrl);
                        mConfigureTextView.setVisibility(View.VISIBLE);
                        mConfigureLayout.setVisibility(View.VISIBLE);

                    } else {
                        mEddystoneUrlContentTextView.setText(R.string.not_applicable);
                        mEddystoneNamespaceTextView.setText(String.valueOf(info.mNameSpaceId));
                        mEddystoneInstanceTextView.setText(String.valueOf(info.mInstanceId));
                        mConfigureTextView.setVisibility(View.VISIBLE);
                        mConfigureLayout.setVisibility(View.INVISIBLE);
                    }

                    mEddystoneTxpowerTextView.setText(String.valueOf(info.mTxPower) + getString(R.string.dbm));
                    mEddystoneTlmUptimeTextView.setText(String.valueOf(info.mTlmUptime));
                    mEddystoneTlmVoltageTextView.setText(String.valueOf(info.mTlmBatteryVoltage) + getString(R.string.millivolts));
                    // The value get is in 8.8 fixed point format. So divided the value by a 256 (SCALE FACTOR).
                    mEddystoneTlmTempTextView.setText(String.valueOf(((double) info.mTlmTemparature) / 256) + getString(R.string.celsius));
                    mEddystonePDUCountTextView.setText(String.valueOf(info.mPduCount));
                    mEddystoneRssiTextView.setText(String.valueOf(info.rssi) + getString(R.string.dbm));
                    break;
                case Constants.ALTBEACON_IDEFR:
                    mIbeaconLayout.setVisibility(View.GONE);
                    mAltbeaconLayout.setVisibility(View.VISIBLE);
                    mEddystoneLayout.setVisibility(View.GONE);
                    mAltbeaconId1ContentTextView.setText(info.mUuid);
                    mAltbeaconId2ContentTextView.setText(getResources().getString(R.string.hex_prefix) +
                            getHexValue(info.max));
                    mAltbeaconId3ContentTextView.setText(getResources().getString(R.string.hex_prefix)
                            + getHexValue(info.min));
                    mAltbeaconRssiContentTextView.setText(String.valueOf(info.rssi) + getString(R.string.dbm));
                    if (info.mManufacture == ATMEL_MANUFACTURE_ID) {
                        mAltbeaconManufactureTextView.setText(R.string.atmel_corporation);
                    } else {
                        mAltbeaconManufactureTextView.setText(R.string.not_applicable);
                    }
                    //check database for assigned url
                    if (mBeaconListDbHandler != null) {
                        mAltbeaconUrlContentTextView.setText(mBeaconListDbHandler.getUrlOfMatchingBeacon(info.mUuid, String.valueOf(info.max),
                                String.valueOf(info.min)));
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * User clicked on pop over, open the information view
     */
    public void enableInformationView() {
        mMainActivity.mProximityState = false;
        mRadarView.setVisibility(View.GONE);
        mBottomLayout.setVisibility(View.GONE);
        mBeaconInformationLayout.setVisibility(View.VISIBLE);
    }

    /**
     * User selected back action from information page
     */
    public void disableInformationView() {
        if (mProximityButton.isSelected()) {
            setProximityImageSelected();
        }
        mRadarView.setVisibility(View.VISIBLE);
        mBottomLayout.setVisibility(View.VISIBLE);
        mBeaconInformationLayout.setVisibility(View.GONE);
    }

    /**
     * Check information view is visible
     *
     * @return boolean Value
     */
    public Boolean checkInformationViewVisisbility() {
        return mBeaconInformationLayout.getVisibility() == View.VISIBLE;
    }

    /**
     * Check proximity view is visible
     *
     * @return
     */
    public Boolean checkProximityViewVisisbility() {
        return mProximityLayout.getVisibility() == View.VISIBLE;
    }

    /**
     * Hide the range screen and infirmation view and show the proximity
     * layout
     *
     * @param url (String)
     */
    public void showProximityView(String url) {
        if (mProximityLayout.getVisibility() == View.GONE) {
            mRadarLayout.setVisibility(View.GONE);
            mProximityLayout.setVisibility(View.VISIBLE);
            mWebview.loadUrl(url);
        } else {
            changeView(url);
        }
    }

    /**
     * Show range screen
     */
    public void disableProximityView() {
        mRadarLayout.setVisibility(View.VISIBLE);
        mProximityLayout.setVisibility(View.GONE);
    }

    /**
     * If proximity is already visible and another beacon in near region
     * show dialog to handle
     *
     * @param url url of new beacon in near region
     */
    public void changeView(final String url) {
  /*      final String newUrl = url;
        final String newAddress = address;*/
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                //.setTitle("Delete entry")
                .setMessage(getString(R.string.new_beacon_immediate_range))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mWebview.loadUrl(url);
                        /*mUrl = newUrl;
                        mAddress = newAddress;*/
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert);

        final AlertDialog dialog = builder.create();

        dialog.show();

        final int version = Build.VERSION.SDK_INT;
        int colour = 0;
        if (version >= 23) {
            colour = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
        } else {
            colour = getResources().getColor(R.color.colorPrimary);
        }
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(colour);
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(colour);
    }

    /**
     * Handle proximity/distance button selection
     */
    public void updateProximityState() {
        if (mMainActivity.mProximityState) {
            setProximityImageSelected();
        } else {
            setDistanceImageSelected();
        }
    }

    /**
     * Handling onClick actions
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.configure_button) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                    //.setTitle("Delete entry")
                    .setMessage(getString(R.string.eddystone_config_dialog))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(getActivity(), EddystoneScanActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert);

            final AlertDialog dialog = builder.create();

            dialog.show();

            final int version = Build.VERSION.SDK_INT;
            int colour = 0;
            if (version >= 23) {
                colour = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
            } else {
                colour = getResources().getColor(R.color.colorPrimary);
            }

            dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(colour);
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(colour);

        } else if (id == R.id.proximity_imageview) {
           setProximityImageSelected();
        } else if (id == R.id.distance_imageview) {
           setDistanceImageSelected();
        }

    }

    public void setDistanceImageSelected() {
        mMainActivity.mProximityState = false;
        mProximityButton.setSelected(false);
        mDistanceButton.setSelected(true);
        mDistanceButton.setTextColor(getResources().getColor(R.color.colorPrimary));
        mProximityButton.setTextColor(getResources().getColor(R.color.textColour));
    }

    public void setProximityImageSelected() {
        mMainActivity.mProximityState = true;
        mProximityButton.setSelected(true);
        mDistanceButton.setSelected(false);
        mDistanceButton.setTextColor(getResources().getColor(R.color.textColour));
        mProximityButton.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    /**
     * Append Zero if the int is less than 255
     * If Value is one digit ,then append two 0's
     */

    private String getHexValue(int value) {
        if (value > 255) {
            return String.valueOf(Integer.parseInt(Integer.toHexString(value >> 8), 16)) + String.valueOf(Integer.toHexString(value));
        } else if (value >= 0 && value < 10) {
            return "00" + String.valueOf(Integer.parseInt(Integer.toHexString(value >> 8), 16)) + String.valueOf(Integer.toHexString(value));
        } else {
            return "0" + String.valueOf(Integer.parseInt(Integer.toHexString(value >> 8), 16)) + String.valueOf(Integer.toHexString(value));
        }

    }

    // Beacons Mode
    private enum eBeaconMode {
        MODE_PROXIMITY,
        MODE_DISTANCE,
        MODE_DISTANCE_SINGLE_BEACON,
        MODE_NOTIFICATION
    }

    /**
     * Load the URL to webView and Dismiss the progress bar on page finish
     */
    private class urlWebviewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mProgressBar.setVisibility(View.GONE);
        }
    }

}