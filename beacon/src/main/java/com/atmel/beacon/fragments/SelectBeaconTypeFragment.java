/*
 *
 *
 *     SelectBeaconTypeFragment.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.atmel.beacon.R;
import com.atmel.beacon.activities.BeaconEditActivity;
import com.atmel.beacon.util.Constants;


public class SelectBeaconTypeFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "SelectBeaconTypeFragment";
    private ImageView mAltBeaconImageView;
    private ImageView mIBeaconImageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_beacon_type, container, false);
        mAltBeaconImageView = (ImageView) view.findViewById(R.id.altbeacon);
        mIBeaconImageView = (ImageView) view.findViewById(R.id.ibeacon);
        mAltBeaconImageView.setOnClickListener(this);
        mIBeaconImageView.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent = new Intent(getActivity(), BeaconEditActivity.class);
        if (id == R.id.altbeacon) {
            intent.putExtra(Constants.EXTRA_BEACON_TYPE, Constants.ALTBEACON_IDEFR);
        } else if (id == R.id.ibeacon) {
            intent.putExtra(Constants.EXTRA_BEACON_TYPE, Constants.IBEACON_IDEFR);
        }
        intent.putExtra(Constants.EXTRA_BEACON_INSERT_EDIT_MODE, Constants.EXTRA_BEACON_INSERT_MODE);
        startActivityForResult(intent, Constants.REQUEST_CODE_BEACON_EDIT);
        dismiss();
    }
}
