/*
 *
 *
 *     Constants.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.util;

public final class Constants {
    public static final long SPLASH_SCREEN_DURATION = 1000 * 2; //2 SECONDS

    //FILE Constants
    public static final String BEACON_INFO_DIR_NAME = "BeaconDetailResources";
    public static final String BEACON_INFO_FILE_NAME = BEACON_INFO_DIR_NAME + "/config.json";
    //Shared preference
    public static final String SHARED_PREF_FILE_NAME = "atmelSharedPref";
    public static final String SHARED_PREF_IS_PREPOPULATED_DATABASE_STORED = "isPrePopStored";

    //Eddystone/beacon identifier
    public static final int IBEACON_IDEFR = 0;
    public static final int EDDYSTONE_IDEFR = 1;
    public static final int ALTBEACON_IDEFR = 2;

    //ACTIONBAR HEIGHT
    public static final String ACTIONBAR_HEIGHT = "ActionBarHeight";

    //EXTRA DATA SENT FROM POP UP TO INFORMATION PAGE
    public static final int EXTRA_BEACON_INSERT_MODE = 0;
    public static final int EXTRA_BEACON_EDIT_MODE = 1;
    public static final String EXTRA_BEACON_INSERT_EDIT_MODE = "BeaconEditMode";
    public static final String EXTRA_BEACON_TYPE = "BeaconType";
    public static final String EXTRA_IBEACON_UUID = "IBeaconUUID";
    public static final String EXTRA_IBEACON_URL = "IBeaconUrl";
    public static final String EXTRA_IBEACON_MAJOR = "IBeaconMajor";
    public static final String EXTRA_IBEACON_MINOR = "IBeaconMinor";
    public static final String EXTRA_EDDYSTONE_UID = "EddystoneUUID";
    public static final String EXTRA_EDDYSTONE_URL = "EddystoneUrl";
    public static final String EXTRA_EDDYSTONE_BATTERY_VOLTAGE = "EddystoneVoltage";
    public static final String EXTRA_EDDYSTONE_TEMPARATURE = "EddystoneTemparature";
    public static final String EXTRA_EDDYSTONE_UPTIME = "EddystoneUpTime";
    public static final String EXTRA_BEACON_NAME = "BeaconName";
    public static final String EXTRA_BEACON_ID = "BeaconId";
    public static final String EXTRA_EDDYSTONE_INSTANCEID = "InstanceId";
    public static final String EXTRA_EDDYSTONE_NAMESPACEID = "NamepsaceId";
    public static final String EXTRA_EDDYSTONE_TXPOWER = "Txpower";
    public static final String EXTRA_PROXIMITY_ADDRESS = "Address";
    public static final String EXTRA_PROXIMITY_URL = "Url";
    public static final int IMMEDIATE_RANGE = 40;


    public static final String EDDYSTONE_TXPOWERMODE_LOW = "Low";
    public static final String EDDYSTONE_TXPOWERMODE_LOWEST = "Lowest";
    public static final String EDDYSTONE_TXPOWERMODE_MEDIUM = "Medium";
    public static final String EDDYSTONE_TXPOWERMODE_HIGH = "High";

    //set tesult request for beacon edit activity
    public static final int REQUEST_CODE_BEACON_EDIT = 3333;

    //connection related information
    public static final int DEVICE_DISCONNECTED_KEY = 5646;
    public static final int DEVICE_CONNECTED_KEY = 5647;
    public static final int SERVICE_DISCOVERED_KEY = 5648;
    public static final int CHARACTERISTICS_CHANGED_KEY = 5649;
    public static final int CHARACTERISTICS_WRITE_KEY = 5650;
    public static final int DEVICE_PAIR_IN_PROCESS_KEY = 5651;
    public static final int DEVICE_PAIR_DONE_KEY = 5652;
    public static final int DEVICE_PAIR_FAILED_KEY = 5653;
    public static final int DEVICE_BLUETOOTH_ON_KEY = 5654;
    public static final int DEVICE_BLUETOOTH_OFF_KEY = 5655;
    public static final int CHARACTERISTICS_READ_KEY = 5656;
    public static final int REMOTE_RSSI_KEY = 5657;
    public static final int DEVICE_PAIR_REQUEST = 5658;
    public static final int DEVICE_NOTIFY_REQUEST = 5659;
    public static final int DEVICE_READ_REQUEST = 5660;
    public static final int DESCRIPTOR_WRITE_KEY = 5661;
    public static final int DEVICE_WRITE_REQUEST = 5662;
    public static final int DEVICE_SERVICES_DISCOVERED = 5663;
    //PREFS TAGS
    public static final String PREF_DEV_ADDRESS = "PREF DEV ADDRESS";
    public static final String PREF_CONNECTION_STATE = "PREF CONNECTION STATE";
    public static final String PREF_DEFAULT_BEACON_ADDED = "PREF DEFAULT BEACON ADDED";
    public static final int GATT_SUCCESS = 0;
    private static final String PACKAGE_NAME = Constants.class.getPackage().getName();
    public static final String RESULT_RECEIVER_KEY = PACKAGE_NAME + "RESULT_RECEIVER_KEY";
    public static final String DEVICE_CONNECTED_NAME_KEY = PACKAGE_NAME +
            "DEVICE_CONNECTED_NAME_KEY";
    public static final String DEVICE_DISCONNECTED_NAME_KEY = PACKAGE_NAME +
            "DEVICE_DISCONNECTED_NAME_KEY";
    public static final String DEVICE_PAIRING_STATUS_IN_PROCESS_KEY = PACKAGE_NAME +
            "DEVICE_PAIRING_STATUS_IN_PROCESS_KEY";
    public static final String DEVICE_PAIRING_STATUS_DONE_KEY = PACKAGE_NAME +
            "DEVICE_PAIRING_STATUS_DONE_KEY";
    public static final String DEVICE_PAIRING_STATUS_FAILED_KEY = PACKAGE_NAME +
            "DEVICE_PAIRING_STATUS_FAILED_KEY";
    public static final String DEVICE_BLUETOOTH_STATUS_OFF_KEY = PACKAGE_NAME +
            "DEVICE_BLUETOOTH_STATUS_OFF_KEY";
    public static final String DEVICE_BLUETOOTH_STATUS_ON_KEY = PACKAGE_NAME +
            "DEVICE_BLUETOOTH_STATUS_ON_KEY";
    public static final String SERVICE_DISCOVERED_STATUS_KEY = PACKAGE_NAME +
            "SERVICE_DISCOVERED_STATUS_KEY";
    public static final String CHARACTERISTIC_CHANGE_STATUS_KEY = PACKAGE_NAME +
            "CHARACTERISTIC_CHANGE_STATUS_KEY";
    public static final String CHARACTERISTIC_WRITE_STATUS_KEY = PACKAGE_NAME +
            "CHARACTERISTIC_WRITE_STATUS_KEY";
    public static final String CHARACTERISTIC_READ_STATUS_KEY = PACKAGE_NAME +
            "CHARACTERISTIC_READ_STATUS_KEY";
    public static final String CHARACTERISTIC_READ_DATA = PACKAGE_NAME +
            "CHARACTERISTIC_READ_DATA";
    public static final String CHARACTERISTIC_READ_DATA_CODE = PACKAGE_NAME +
            "CHARACTERISTIC_READ_DATA_CODE";
    public static final String REMOTE_RSSI_VALUE = PACKAGE_NAME +
            "REMOTE_RSSI_VALUE";
    public static final String DEVICE_BLUETOOTH_KEY = PACKAGE_NAME +
            "DEVICE_BLUETOOTH_STATUS_KEY";
    public static final String GATTSERVER_READ_REQUESTID = PACKAGE_NAME +
            "GATTSERVER_READ_REQUESTID";
    public static final String GATTSERVER_READ_CHARACTERISTICS = PACKAGE_NAME +
            "GATTSERVER_READ_CHARACTERISTICS";
    public static final String GATTSERVER_READ_OFFSET = PACKAGE_NAME +
            "GATTSERVER_READ_OFFSET";
    public static final String DESCRIPTOR_WRITE_STATUS_KEY = PACKAGE_NAME +
            "DESCRIPTOR_WRITE_STATUS";
    public static final String DESCRIPTOR_WRITE_CHARACTERISTIC = PACKAGE_NAME +
            "DESCRIPTOR_WRITE_CHARACTERISTIC";

}
