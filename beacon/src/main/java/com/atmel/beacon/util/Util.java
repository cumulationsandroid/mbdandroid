/*
 *
 *
 *     Util.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.beacon.util;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.URLUtil;

import com.atmel.beacon.BeaconListener;
import com.atmel.beacon.R;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.caverock.androidsvg.SVG;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public final class Util {
    public static final String TAG = "Util.java";
    public static final String COMMON_PREFE = "ATMEL_PREFERENCES";
    public static String cacheDirectoryPath;

    /**
     * Stores key pair value in internal storage
     *
     * @param context
     * @param key
     * @param value
     */
    public static void storePreference(Context context, String key, String value) {
        SharedPreferences pref = context.getSharedPreferences(Constants.SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getPreference(Context context, String key, String defValue) {
        SharedPreferences pref = context.getSharedPreferences(Constants.SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        return pref.getString(key, defValue);
    }

    public static boolean isBlutoothisEnabled() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                return false;
            }
        }
        return true;
    }

    public static void storePrepopulatedData(Context context) {
        if (Boolean.valueOf(getPreference(context, Constants.SHARED_PREF_IS_PREPOPULATED_DATABASE_STORED, "false"))) {
            Log.i(TAG, "Pre populated data already stored.");
            return;
        }

        try {
            ZipInputStream zipIn = new ZipInputStream(context.getResources().openRawResource(R.raw.beacon_detail_resources));
            byte[] readBuffer = new byte[1024 * 3]; //3kb at a fetch
            String rootPath = cacheDirectoryPath;
            ZipEntry entry;
            int readBytes;

            while ((entry = zipIn.getNextEntry()) != null) {
                if (entry.getName().startsWith("__MACOSX")) {
                    continue;
                }

                //Destination directories are already created, just continue
                File directory = null;
                if (entry.isDirectory() && (directory = new File(rootPath + "/" + entry.getName())).exists() == false) {
                    directory.mkdirs();
                    continue;
                }

                //Copy files to the destination folder
                FileOutputStream fos = new FileOutputStream(rootPath + "/" + entry.getName());
                while (-1 != (readBytes = zipIn.read(readBuffer))) {
                    fos.write(readBuffer, 0, readBytes);
                }
                fos.flush();
                fos.close();
            }

            storePreference(context, Constants.SHARED_PREF_IS_PREPOPULATED_DATABASE_STORED, "true");

            BeaconListener.startMonitorBeacons();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void consumeTouch(View view) {
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public static Bitmap convertSVGToBitmap(Context context, int resId, int reqWidth, int reqHeight) {
        try {
            Bitmap bmp = Bitmap.createBitmap(reqWidth, reqHeight, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bmp);
            SVG svg = SVG.getFromResource(context, resId);
            svg.setDocumentWidth(reqWidth);
            svg.setDocumentHeight(reqHeight);
            svg.renderToCanvas(c);

            return bmp;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Bitmap getFlipSVGBitmap(Context context, int resId, int reqWidth, int reqHeight) {
        Bitmap bmp = convertSVGToBitmap(context, resId, reqWidth, reqHeight);

        Bitmap flipBitmp = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(flipBitmp);
        Matrix flipHorizontalMatrix = new Matrix();
        flipHorizontalMatrix.setScale(-1, 1);
        flipHorizontalMatrix.postTranslate(bmp.getWidth(), 0);

        c.drawBitmap(bmp, flipHorizontalMatrix, null);

        bmp.recycle();
        bmp = null;

        return flipBitmp;
    }

    public static BitmapDrawable getColorChangedBitmap(Context context, int resId, int width, int height, int srcColor, int dstColor) {
        try {
            SVG src = SVG.getFromResource(context, resId);
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas();
            c.setBitmap(bmp);
            src.setDocumentHeight(height);
            src.setDocumentWidth(width);
            src.renderToCanvas(c);
            BitmapDrawable drawable = new BitmapDrawable(context.getResources(), bmp);
            drawable.setColorFilter(new LightingColorFilter(srcColor, dstColor));

            return drawable;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Store default String value to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @param value   {@link String}
     */
    public static void setStringSharedPreference(Context context, String key, String value) {
        SharedPreferences.Editor edit = createSharedPreferences(context, COMMON_PREFE).edit();
        edit.putString(key, value);
        edit.apply();
    }

    /**
     * Store default Boolean value to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @param value   {@link Boolean}
     */
    public static void setBooleanSharedPreference(Context context, String key, boolean value) {
        SharedPreferences.Editor edit = createSharedPreferences(context, COMMON_PREFE).edit();
        edit.putBoolean(key, value);
        edit.apply();
    }

    /**
     * Store default String value to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @param value   {@link int}
     */
    public static void setIntSharedPreference(Context context, String key, int value) {
        SharedPreferences.Editor edit = createSharedPreferences(context, COMMON_PREFE).edit();
        edit.putInt(key, value);
        edit.apply();
    }

    /**
     * Get String values from default {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @return {@link String}
     */
    public static String getStringSharedPreference(Context context, String key) {
        return createSharedPreferences(context, COMMON_PREFE).getString(key, "");
    }

    /**
     * Get Boolean values from default {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @return {@link Boolean}
     */
    public static boolean getBooleanSharedPreference(Context context, String key) {
        return createSharedPreferences(context, COMMON_PREFE).getBoolean(key, false);
    }

    /**
     * Get String values from default {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @return {@link int}
     */
    public static int getIntSharedPreference(Context context, String key) {
        return createSharedPreferences(context, COMMON_PREFE).getInt(key, 0);
    }

    /**
     * Initializing SharedPreferences
     *
     * @param context {@link Context}
     * @return {@link SharedPreferences}
     */
    private static SharedPreferences createSharedPreferences(Context context, String type) {
        return context.getSharedPreferences(type, Context.MODE_PRIVATE);
    }

    /**
     * This function gives the information of device connection state
     *
     * @param address device address
     * @return connection status true if connected and viceversa
     */
    public static boolean getConnectionState(final String address, Context context) {
        ////Log.e("Adrress Scanned>>", "" + address);
        BluetoothAdapter adapter = BLEConnection.getBluetoothAdapter();
        if (adapter == null || address.equalsIgnoreCase("")) {
            return false;
        }
        return address.equalsIgnoreCase(getDeviceAddress(context)) && getDeviceState(context) == 2;
    }

    /**
     * Saves connected devices address
     */
    public static String getDeviceAddress(Context context) {
        return getStringSharedPreference(context,
                Constants.PREF_DEV_ADDRESS);
    }

    /**
     * Saves connected devices state
     */
    public static int getDeviceState(Context context) {
        return getIntSharedPreference(context,
                Constants.PREF_CONNECTION_STATE);
    }

    /**
     * Check url is valid
     *
     * @param input url
     * @return status of url is valid or not
     */
    public static boolean checkURL(CharSequence input) {
        if (TextUtils.isEmpty(input)) {
            return false;
        }
        Pattern URL_PATTERN = Patterns.WEB_URL;
        boolean isURL = URL_PATTERN.matcher(input).matches();
        if (!isURL) {
            String urlString = input + "";
            if (URLUtil.isNetworkUrl(urlString)) {
                try {
                    new URL(urlString);
                    isURL = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return isURL;
    }

    /**
     * Method to detect whether the device is beacon_phone or tablet
     */
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}
