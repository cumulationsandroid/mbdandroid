/*
 *
 *
 *     GattAttributes.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Attributes;

import java.util.HashMap;

/**
 * This class includes a subset of standard GATT attributes and carousel image
 * mapping
 */
public class GattAttributes {

    /**
     * Services
     */
    public static final String HEART_RATE_SERVICE = "0000180d-0000-1000-8000-00805f9b34fb";
    public static final String DEVICE_INFORMATION_SERVICE = "0000180a-0000-1000-8000-00805f9b34fb";
    public static final String HEALTH_TEMP_SERVICE = "00001809-0000-1000-8000-00805f9b34fb";
    public static final String BATTERY_SERVICE = "0000180f-0000-1000-8000-00805f9b34fb";
    public static final String IMMEDIATE_ALERT_SERVICE = "00001802-0000-1000-8000-00805f9b34fb";
    public static final String LINK_LOSS_SERVICE = "00001803-0000-1000-8000-00805f9b34fb";
    public static final String TRANSMISSION_POWER_SERVICE = "00001804-0000-1000-8000-00805f9b34fb";
    public static final String BLOOD_PRESSURE_SERVICE = "00001810-0000-1000-8000-00805f9b34fb";
    public static final String SCAN_PARAMETERS_SERVICE = "00001813-0000-1000-8000-00805f9b34fb";

    /**
     * Unused Service UUIDS
     */
    public static final String AUTOMATION_IO_SERVICE = "00001815-0000-1000-8000-00805f9b34fb";
    public static final String CONTINUOUS_TIME_SERVICE = "00001805-0000-1000-8000-00805f9b34fb";
    public static final String HTTP_PROXY_SERVICE = "00001823-0000-1000-8000-00805f9b34fb";
    public static final String INDOOR_POSITIONING_SERVICE = "00001821-0000-1000-8000-00805f9b34fb";
    public static final String INTERNET_PROTOCOL_SERVICE = "00001820-0000-1000-8000-00805f9b34fb";
    public static final String OBJECT_TRANSFER_SERVICE = "00001825-0000-1000-8000-00805f9b34fb";
    public static final String PULSE_OXIMETER_SERVICE = "00001822-0000-1000-8000-00805f9b34fb";
    public static final String TRANSPORT_DISCOVERY = "00001824-0000-1000-8000-00805f9b34fb";
    public static final String USER_DATA = "0000181c-0000-1000-8000-00805f9b34fb";
    public static final String WEIGHT_SCALE = "0000181d-0000-1000-8000-00805f9b34fb";

    public static final String BODY_COMPOSITION_SERVICE = "0000181b-0000-1000-8000-00805f9b34fb";
    public static final String CONTINUOUS_GLUCOSE_MONITORING_SERVICE = "0000181f-0000-1000-8000-00805f9b34fb";
    public static final String CYCLING_POWER_SERVICE = "00001818-0000-1000-8000-00805f9b34fb";
    public static final String ENVIRONMENTAL_SENSING_SERVICE = "0000181a-0000-1000-8000-00805f9b34fb";
    public static final String LOCATION_NAVIGATION_SERVICE = "00001819-0000-1000-8000-00805f9b34fb";
    public static final String USER_DATA_SERVICE = "0000181c-0000-1000-8000-00805f9b34fb";
    public static final String WEIGHT_SCALE_SERVICE = "0000181d-0000-1000-8000-00805f9b34fb";
    public static final String HEALTH_THERMOMETER_SERVICE = "00001809-0000-1000-8000-00805f9b34fb";
    public static final String BOND_MANAGEMENT_SERVICE = "0000181e-0000-1000-8000-00805f9b34fb";
    public static final String GLUCOSE_SERVICE = "00001808-0000-1000-8000-00805f9b34fb";
    public static final String RSC_SERVICE = "00001814-0000-1000-8000-00805f9b34fb";
    public static final String BAROMETER_SERVICE = "00040001-0000-1000-8000-00805f9b0131";
    public static final String ACCELEROMETER_SERVICE = "00040020-0000-1000-8000-00805f9b0131";
    public static final String ANALOG_TEMPERATURE_SERVICE = "00040030-0000-1000-8000-00805f9b0131";
    public static final String CSC_SERVICE = "00001816-0000-1000-8000-00805f9b34fb";
    public static final String HUMAN_INTERFACE_DEVICE_SERVICE = "00001812-0000-1000-8000-00805f9b34fb";

    /**
     * Scan param characteristics
     */
    public static final String SCAN_INTERVAL_WINDOW = "00002a4f-0000-1000-8000-00805f9b34fb";
    public static final String SCAN_REFRESH = "00002a31-0000-1000-8000-00805f9b34fb";

    /**
     * Heart rate characteristics
     */
    public static final String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
    public static final String BODY_SENSOR_LOCATION = "00002a38-0000-1000-8000-00805f9b34fb";
    /**
     * Device information characteristics
     */
    public static final String SYSTEM_ID = "00002a23-0000-1000-8000-00805f9b34fb";
    public static final String MODEL_NUMBER_STRING = "00002a24-0000-1000-8000-00805f9b34fb";
    public static final String SERIAL_NUMBER_STRING = "00002a25-0000-1000-8000-00805f9b34fb";
    public static final String FIRMWARE_REVISION_STRING = "00002a26-0000-1000-8000-00805f9b34fb";
    public static final String HARDWARE_REVISION_STRING = "00002a27-0000-1000-8000-00805f9b34fb";
    public static final String SOFTWARE_REVISION_STRING = "00002a28-0000-1000-8000-00805f9b34fb";
    public static final String MANUFACTURER_NAME_STRING = "00002a29-0000-1000-8000-00805f9b34fb";
    public static final String PNP_ID = "00002a50-0000-1000-8000-00805f9b34fb";
    public static final String IEEE = "00002a2a-0000-1000-8000-00805f9b34fb";
    /**
     * Battery characteristics
     */
    public static final String BATTERY_LEVEL = "00002a19-0000-1000-8000-00805f9b34fb";

    /**
     * Gatt services
     */
    public static final String GENERIC_ACCESS_SERVICE = "00001800-0000-1000-8000-00805f9b34fb";
    public static final String GENERIC_ATTRIBUTE_SERVICE = "00001801-0000-1000-8000-00805f9b34fb";
    /**
     * Find me characteristics
     */
    public static final String ALERT_LEVEL = "00002a06-0000-1000-8000-00805f9b34fb";
    public static final String TRANSMISSION_POWER_LEVEL = "00002a07-0000-1000-8000-00805f9b34fb";

    /**
     * Blood Pressure service Characteristics
     */
    public static final String BLOOD_PRESSURE_MEASUREMENT = "00002a35-0000-1000-8000-00805f9b34fb";

    /*
    * Serial Port Profile
    * */
    public static final String SERIAL_PORT = "fd5abba0-3935-11e5-85a6-0002a5d5c51b";
    public static final String SERIAL_PORT_END_POINT = "fd5abba1-3935-11e5-85a6-0002a5d5c51b";
    public static final String SERIAL_PORT_CREDITS = "fd5abba2-3935-11e5-85a6-0002a5d5c51b";


    /**
     * Descriptor UUID's
     */
    public static final String CHARACTERISTIC_EXTENDED_PROPERTIES = "00002900-0000-1000-8000-00805f9b34fb";
    public static final String CHARACTERISTIC_USER_DESCRIPTION = "00002901-0000-1000-8000-00805f9b34fb";
    public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    public static final String SERVER_CHARACTERISTIC_CONFIGURATION = "00002903-0000-1000-8000-00805f9b34fb";
    public static final String CHARACTERISTIC_PRESENTATION_FORMAT = "00002904-0000-1000-8000-00805f9b34fb";
    public static final String CHARACTERISTIC_AGGREGATE_FORMAT = "00002905-0000-1000-8000-00805f9b34fb";
    public static final String VALID_RANGE = "00002906-0000-1000-8000-00805f9b34fb";
    public static final String EXTERNAL_REPORT_REFERENCE = "00002907-0000-1000-8000-00805f9b34fb";
    public static final String REPORT_REFERENCE = "00002908-0000-1000-8000-00805f9b34fb";
    public static final String NUMBER_OF_DIGITALS = "00002909-0000-1000-8000-00805f9b34fb";
    public static final String VALUE_TRIGGER_SETTINGS = "0000290A-0000-1000-8000-00805f9b34fb";
    public static final String ENVIRONMENTAL_SENSING_CONFIGURATION = "0000290B-0000-1000-8000-00805f9b34fb";
    public static final String ENVIRONMENTAL_SENSING_MEASUREMENT = "0000290C-0000-1000-8000-00805f9b34fb";
    public static final String ENVIRONMENTAL_SENSING_TRIGGER_SETTING = "0000290D-0000-1000-8000-00805f9b34fb";
    public static final String TIME_TRIGGER_SETTINGS = "0000290E-0000-1000-8000-00805f9b34fb";

    /*
    Characteristics UUID's
     */
    public static final String HEART_RATE_CONTROL_POINT = "00002a39-0000-1000-8000-00805f9b34fb";
    public static final String GLUCOSE_MEASUREMENT_CONTEXT = "00002a34-0000-1000-8000-00805f9b34fb";
    public static final String GLUCOSE_MEASUREMENT = "00002a18-0000-1000-8000-00805f9b34fb";
    public static final String GLUCOSE_FEATURE = "00002a51-0000-1000-8000-00805f9b34fb";
    public static final String RECORD_ACCESS_CONTROL_POINT = "00002a52-0000-1000-8000-00805f9b34fb";
    public static final String BLOOD_INTERMEDIATE_CUFF_PRESSURE = "00002a36-0000-1000-8000-00805f9b34fb";
    public static final String BLOOD_PRESSURE_FEATURE = "00002a49-0000-1000-8000-00805f9b34fb";
    public static final String RSC_FEATURE = "00002a54-0000-1000-8000-00805f9b34fb";
    public static final String SC_SENSOR_LOCATION = "00002a5d-0000-1000-8000-00805f9b34fb";
    public static final String SC_CONTROL_POINT = "00002a55-0000-1000-8000-00805f9b34fb";
    public static final String CSC_FEATURE = "00002a5c-0000-1000-8000-00805f9b34fb";
    public static final String RSC_MEASUREMENT = "00002a53-0000-1000-8000-00805f9b34fb";
    public static final String CSC_MEASUREMENT = "00002a5b-0000-1000-8000-00805f9b34fb";
    public static final String HEALTH_TEMP_MEASUREMENT = "00002a1c-0000-1000-8000-00805f9b34fb";
    public static final String TEMPERATURE_TYPE = "00002a1d-0000-1000-8000-00805f9b34fb";
    /**
     * current time service
     */
    public static final String CURRENT_TIME_SERVICE = "00001805-0000-1000-8000-00805f9b34fb";
    public static final String NEXT_DST_CHANGE_SERVICE = "00001807-0000-1000-8000-00805f9b34fb";
    public static final String REFERENCE_TIME_UPDATE_SERVICE = "00001806-0000-1000-8000-00805f9b34fb";
    public static final String CURRENT_TIME = "00002A2B-0000-1000-8000-00805f9b34fb";
    public static final String LOCAL_TIME = "00002A0F-0000-1000-8000-00805f9b34fb";
    public static final String DST_TIME = "00002A11-0000-1000-8000-00805f9b34fb";
    public static final String REFERENCE_TIME = "00002A14-0000-1000-8000-00805f9b34fb";
    public static final String TIME_UPDATE_CONTROL_POINT = "00002A16-0000-1000-8000-00805f9b34fb";
    public static final String TIME_UPDATE_STATE = "00002A17-0000-1000-8000-00805f9b34fb";
    /**
     * ANS
     */
    public static final String ALERT_NOTIFICATION_SERVICE = "00001811-0000-1000-8000-00805f9b34fb";
    public static final String ANS_SUPPORTED_NEW_ALERT_CATEGORY = "00002A47-0000-1000-8000-00805f9b34fb";
    public static final String ANS_NEW_ALERT = "00002A46-0000-1000-8000-00805f9b34fb";
    public static final String ANS_SUPPORTED_UNREAD_ALERT_CATEGORY = "00002A48-0000-1000-8000-00805f9b34fb";
    public static final String ANS_UNREAD_ALERT_STATUS = "00002A45-0000-1000-8000-00805f9b34fb";
    public static final String ANS_CONTROL_POINT = "00002A44-0000-1000-8000-00805f9b34fb";
    /**
     * Phone Alert characteristics
     *///                                                    00002902-0000-1000-8000-00805f9b34fb
    public static final String PHONE_ALERT_STATUS_SERVICE = "0000180e-0000-1000-8000-00805f9b34fb";
    public static final String PHONE_ALERT_STATUS = "00002a3f-0000-1000-8000-00805f9b34fb";
    public static final String PHONE_ALERT_RINGER_SETTINGS = "00002a41-0000-1000-8000-00805f9b34fb";
    public static final String PHONE_ALERT_CONTROL_POINT = "00002a40-0000-1000-8000-00805f9b34fb";
    /**
     * OTAU
     */
    public static final String OTAU_SERVICE = "8b698d5b-04e1-48b1-9617-ac80aa64e0d0";//; //8b698d5b-04e1-48b1-9617-ac80aa64e0d0
    public static final String OTAU_CHARACTERISTIC_1_INDICATE = "8b698d5b-04e1-48b1-9617-ac80aa64e0e0";
    public static final String OTAU_CHARACTERISTIC_2_WRITE = "8b698d5b-04e1-48b1-9617-ac80aa64e0e5";
    public static final String OTAU_CHARACTERISTIC_3_READ = "8b698d5b-04e1-48b1-9617-ac80aa64e0ea";
    /**
     * Eddystone characteristics
     */
    public static final String EddyStoneConfigService = "ee0c2080-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneLockStateCharacteristics = "ee0c2081-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneLockCodeCharacteristics = "ee0c2082-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneUnlockCharacteristics = "ee0c2083-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneURICharacteristics = "ee0c2084-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneFlagsCharacteristics = "ee0c2085-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneTxPowerLevelCharacteristics = "ee0c2086-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneTxModeCharacteristics = "ee0c2087-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneBeaconPeriodCharacteristics = "ee0c2088-8786-40ba-ab96-99b91ac981d8";
    public static final String EddyStoneResetCharacteristics = "ee0c2089-8786-40ba-ab96-99b91ac981d8";
    public static final String HEALTH_THERMO_SERVICE = "00001809-0000-1000-8000-00805f9b34fb";
    public static final String TEMPERATURE_INTERMEDIATE = "00002a1e-0000-1000-8000-00805f9b34fb";
    public static final String TEMPERATURE_MEASUREMENT_INTERVAL = "00002a21-0000-1000-8000-00805f9b34fb";
    /**
     * Unused Service characteristics
     */
    public static final String AEROBIC_HEART_RATE_LOWER_LIMIT = "00002a7e-0000-1000-8000-00805f9b34fb";
    public static final String AEROBIC_HEART_RATE_UPPER_LIMIT = "00002a84-0000-1000-8000-00805f9b34fb";
    public static final String AEROBIC_THRESHOLD = "00002a7f-0000-1000-8000-00805f9b34fb";
    public static final String AGE = "00002a80-0000-1000-8000-00805f9b34fb";
    public static final String ALERT_CATEGORY_ID = "00002a43-0000-1000-8000-00805f9b34fb";
    public static final String ALERT_CATEGORY_ID_BIT_MASK = "00002a42-0000-1000-8000-00805f9b34fb";
    public static final String ALERT_STATUS = "00002a3F-0000-1000-8000-00805f9b34fb";
    public static final String ANAEROBIC_HEART_RATE_LOWER_LIMIT = "00002a81-0000-1000-8000-00805f9b34fb";
    public static final String ANAEROBIC_HEART_RATE_UPPER_LIMIT = "00002a82-0000-1000-8000-00805f9b34fb";
    public static final String ANAEROBIC_THRESHOLD = "00002aA83-0000-1000-8000-00805f9b34fb";
    public static final String APPARENT_WIND_DIRECTION = "00002a73-0000-1000-8000-00805f9b34fb";
    public static final String APPARENT_WIND_SPEED = "00002a72-0000-1000-8000-00805f9b34fb";
    public static final String APPEARANCE = "00002a01-0000-1000-8000-00805f9b34fb";
    public static final String BAROMETRIC_PRESSURE_TREND = "00002aa3-0000-1000-8000-00805f9b34fb";
    public static final String BODY_COMPOSITION_FEATURE = "00002a9B-0000-1000-8000-00805f9b34fb";
    public static final String BODY_COMPOSITION_MEASUREMENT = "00002a9C-0000-1000-8000-00805f9b34fb";
    public static final String BOND_MANAGEMENT_CONTROL_POINT = "00002aa4-0000-1000-8000-00805f9b34fb";
    public static final String BOND_MANAGEMENT_FEATURE = "00002aa5-0000-1000-8000-00805f9b34fb";
    public static final String CENTRAL_ADDRESS_RESOLUTION = "00002aa6-0000-1000-8000-00805f9b34fb";
    public static final String CGM_FEATURE = "00002aa8-0000-1000-8000-00805f9b34fb";
    public static final String CGM_MEASUREMENT = "00002aa7-0000-1000-8000-00805f9b34fb";
    public static final String CGM_SESSION_RUN_TIME = "00002aab-0000-1000-8000-00805f9b34fb";
    public static final String CGM_SESSION_START_TIME = "00002aaa-0000-1000-8000-00805f9b34fb";
    public static final String CGM_SPECIFIC_OPS_CONTROL_POINT = "00002aaC-0000-1000-8000-00805f9b34fb";
    public static final String CGM_STATUS = "00002aa9-0000-1000-8000-00805f9b34fb";
    public static final String CYCLING_POWER_CONTROL_POINT = "00002a66-0000-1000-8000-00805f9b34fb";
    public static final String CYCLING_POWER_FEATURE = "00002a65-0000-1000-8000-00805f9b34fb";
    public static final String CYCLING_POWER_MEASUREMENT = "00002a63-0000-1000-8000-00805f9b34fb";
    public static final String CYCLING_POWER_VECTOR = "00002a64-0000-1000-8000-00805f9b34fb";
    public static final String DATABASE_CHANGE_INCREMENT = "00002a99-0000-1000-8000-00805f9b34fb";
    public static final String DATE_OF_BIRTH = "00002a85-0000-1000-8000-00805f9b0131";
    public static final String DATE_OF_THRESHOLD_ASSESSMENT = "00002a86-0000-1000-8000-00805f9b0131";
    public static final String DATE_TIME = "00002a08-0000-1000-8000-00805f9b34fb";
    public static final String DAY_DATE_TIME = "00002a0a-0000-1000-8000-00805f9b34fb";
    public static final String DAY_OF_WEEK = "00002A09-0000-1000-8000-00805f9b34fb";
    public static final String DESCRIPTOR_VALUE_CHANGED = "00002a7d-0000-1000-8000-00805f9b34fb";
    public static final String DEVICE_NAME = "00002a00-0000-1000-8000-00805f9b34fb";
    public static final String DEW_POINT = "00002a7b-0000-1000-8000-00805f9b34fb";
    public static final String DST_OFFSET = "00002a0d-0000-1000-8000-00805f9b34fb";
    public static final String ELEVATION = "00002a6c-0000-1000-8000-00805f9b34fb";
    public static final String EMAIL_ADDRESS = "00002a87-0000-1000-8000-00805f9b34fb";
    public static final String EXACT_TIME_256 = "00002a0c-0000-1000-8000-00805f9b34fb";
    public static final String FAT_BURN_HEART_RATE_LOWER_LIMIT = "00002a88-0000-1000-8000-00805f9b34fb";
    public static final String FAT_BURN_HEART_RATE_UPPER_LIMIT = "00002a89-0000-1000-8000-00805f9b34fb";
    public static final String FIRSTNAME = "00002a8a-0000-1000-8000-00805f9b34fb";
    public static final String FIVE_ZONE_HEART_RATE_LIMITS = "00002A8b-0000-1000-8000-00805f9b34fb";
    public static final String GENDER = "00002a8c-0000-1000-8000-00805f9b34fb";
    public static final String GUST_FACTOR = "00002a74-0000-1000-8000-00805f9b34fb";
    public static final String HEAT_INDEX = "00002a89-0000-1000-8000-00805f9b34fb";
    public static final String HEIGHT = "00002a8a-0000-1000-8000-00805f9b34fb";
    public static final String HEART_RATE_MAX = "00002a8d-0000-1000-8000-00805f9b34fb";
    public static final String HIP_CIRCUMFERENCE = "00002a8f-0000-1000-8000-00805f9b34fb";
    public static final String HUMIDITY = "00002a6f-0000-1000-8000-00805f9b34fb";
    public static final String INTERMEDIATE_CUFF_PRESSURE = "00002a36-0000-1000-8000-00805f9b34fb";
    public static final String INTERMEDIATE_TEMPERATURE = "00002a1e-0000-1000-8000-00805f9b34fb";
    public static final String IRRADIANCE = "00002a77-0000-1000-8000-00805f9b34fb";
    public static final String LANGUAGE = "00002aa2-0000-1000-8000-00805f9b34fb";
    public static final String LAST_NAME = "00002a90-0000-1000-8000-00805f9b34fb";
    public static final String LN_CONTROL_POINT = "00002a6b-0000-1000-8000-00805f9b34fb";
    public static final String LN_FEATURE = "00002a6a-0000-1000-8000-00805f9b34fb";
    public static final String LOCAL_TIME_INFORMATION = "00002a0f-0000-1000-8000-00805f9b34fb";
    public static final String LOCATION_AND_SPEED = "00002a67-0000-1000-8000-00805f9b34fb";
    public static final String MAGNETIC_DECLINATION = "00002a2c-0000-1000-8000-00805f9b34fb";
    public static final String MAGNETIC_FLUX_DENSITY_2D = "00002aa0-0000-1000-8000-00805f9b34fb";
    public static final String MAGNETIC_FLUX_DENSITY_3D = "00002aa1-0000-1000-8000-00805f9b34fb";
    public static final String MANUFACTURE_NAME_STRING = "00002a29-0000-1000-8000-00805f9b34fb";
    public static final String MAXIMUM_RECOMMENDED_HEART_RATE = "00002a91-0000-1000-8000-00805f9b34fb";
    public static final String MEASUREMENT_INTERVAL = "00002a21-0000-1000-8000-00805f9b34fb";
    public static final String NAVIGATION = "00002a68-0000-1000-8000-00805f9b34fb";
    public static final String NEW_ALERT = "00002a46-0000-1000-8000-00805f9b34fb";
    public static final String PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS = "00002a04-0000-1000-8000-00805f9b34fb";
    public static final String PERIPHERAL_PRIVACY_FLAG = "00002a02-0000-1000-8000-00805f9b34fb";
    public static final String POLLEN_CONCENTRATION = "00002a75-0000-1000-8000-00805f9b34fb";
    public static final String POSITION_QUALITY = "00002a69-0000-1000-8000-00805f9b34fb";
    public static final String PRESSURE = "00002a6d-0000-1000-8000-00805f9b34fb";
    public static final String RAINFALL = "00002a78-0000-1000-8000-00805f9b34fb";
    public static final String RECONNECTION_ADDRESS = "00002a03-0000-1000-8000-00805f9b34fb";
    public static final String REFERNCE_TIME_INFORMATION = "00002a14-0000-1000-8000-00805f9b34fb";
    public static final String RESTING_HEART_RATE = "00002a92-0000-1000-8000-00805f9b34fb";
    public static final String RINGER_CONTROL_POINT = "00002a40-0000-1000-8000-00805f9b34fb";
    public static final String RINGER_SETTING = "00002a41-0000-1000-8000-00805f9b34fb";
    public static final String SENSOR_LOCATION = "00002a5d-0000-1000-8000-00805f9b34fb";
    public static final String SERVICE_CHANGED = "00002a05-0000-1000-8000-00805f9b34fb";
    public static final String SPORT_TYPE_FOR_AEROBIC_AND_ANAEROBIC_THRESHOLDS = "00002a93-0000-1000-8000-00805f9b34fb";
    public static final String SUPPORTED_NEW_ALERT_CATEGORY = "00002a47-0000-1000-8000-00805f9b34fb";
    public static final String SUPPORTED_UNREAD_ALERT_CATEGORY = "00002a48-0000-1000-8000-00805f9b34fb";
    public static final String TEMPERATURE = "00002a6e-0000-1000-8000-00805f9b34fb";
    public static final String TEMPERATURE_MEASUREMENT = "00002a1c-0000-1000-8000-00805f9b34fb";
    public static final String THREE_ZONE_HEART_RATE_LIMITS = "00002a94-0000-1000-8000-00805f9b34fb";
    public static final String TIME_ACCURACY = "00002a12-0000-1000-8000-00805f9b34fb";
    public static final String TIME_SOURCE = "00002a13-0000-1000-8000-00805f9b34fb";
    public static final String TIME_WITH_DST = "00002a11-0000-1000-8000-00805f9b34fb";
    public static final String TIME_ZONE = "00002a0e-0000-1000-8000-00805f9b34fb";
    public static final String TRUE_WIND_DIRECTION = "00002a71-0000-1000-8000-00805f9b34fb";
    public static final String TRUE_WIND_SPEED = "00002a70-0000-1000-8000-00805f9b34fb";
    public static final String TWO_ZONE_HEART_RATE = "00002a95-0000-1000-8000-00805f9b34fb";
    public static final String TX_POWER = "00002a07-0000-1000-8000-00805f9b34fb";
    public static final String UNCERTAINITY = "00002ab4-0000-1000-8000-00805f9b34fb";
    public static final String UNREAD_ALERT_STATUS = "00002a45-0000-1000-8000-00805f9b34fb";
    public static final String USER_CONTROL_POINT = "00002a9f-0000-1000-8000-00805f9b34fb";
    public static final String USER_INDEX = "00002a9a-0000-1000-8000-00805f9b34fb";
    public static final String UV_INDEX = "00002a76-0000-1000-8000-00805f9b34fb";
    public static final String VO2_MAX = "00002a96-0000-1000-8000-00805f9b34fb";
    public static final String WAIST_CIRCUMFERENCE = "00002a97-0000-1000-8000-00805f9b34fb";
    public static final String WEIGHT = "00002a98-0000-1000-8000-00805f9b34fb";
    public static final String WEIGHT_SCALE_FEATURE = "00002a9e-0000-1000-8000-00805f9b34fb";
    public static final String WIND_CHILL = "00002a7-0000-1000-8000-00805f9b34fb";

    private static final HashMap<String, String> attributes = new HashMap<String, String>();
    private static final HashMap<String, String> descriptorAttributes = new HashMap<String, String>();

    static {

        // Services.
        attributes.put(HEART_RATE_SERVICE, "Heart Rate Service");
        attributes.put(HEALTH_THERMO_SERVICE, "Health Thermometer Service");
        attributes.put(GENERIC_ACCESS_SERVICE, "Generic Access Service");
        attributes.put(GENERIC_ATTRIBUTE_SERVICE, "Generic Attribute Service");
        attributes
                .put(DEVICE_INFORMATION_SERVICE, "Device Information Service");
        attributes.put(BATTERY_SERVICE,// "0000180f-0000-1000-8000-00805f9b34fb",
                "Battery Service");
        attributes.put(IMMEDIATE_ALERT_SERVICE, "Immediate Alert");
        attributes.put(LINK_LOSS_SERVICE, "Link Loss");
        attributes.put(TRANSMISSION_POWER_SERVICE, "Tx Power");
        attributes.put(BLOOD_PRESSURE_SERVICE, "Blood Pressure Service");
        attributes.put(SERIAL_PORT, "Custom Serial Chat");
        attributes.put(SERIAL_PORT_END_POINT, "Custom Serial Chat");

        // Unused Services
        attributes
                .put(ALERT_NOTIFICATION_SERVICE, "Alert Notification Service");
        attributes.put(PHONE_ALERT_STATUS_SERVICE, "Phone Alert Status");
        attributes.put(CURRENT_TIME_SERVICE, "Current Time Service");
        attributes.put(NEXT_DST_CHANGE_SERVICE, "Next DST Change Service");
        attributes
                .put(PHONE_ALERT_STATUS_SERVICE, "Phone Alert Status Service");
        attributes.put(REFERENCE_TIME_UPDATE_SERVICE,
                "Reference Time Update Service");
        attributes.put(SCAN_PARAMETERS_SERVICE, "Scan Parameter service");

        // Heart Rate Characteristics.
        attributes.put(HEART_RATE_MEASUREMENT, "Heart Rate Measurement");
        attributes.put(BODY_SENSOR_LOCATION, "Body Sensor Location");
        attributes.put(HEART_RATE_CONTROL_POINT, "Heart Rate Control Point");

        // Health thermometer Characteristics.
        attributes.put(HEALTH_TEMP_MEASUREMENT,
                "Health Thermometer Measurement");
        attributes.put(TEMPERATURE_TYPE, "Temperature Type");
        attributes.put(TEMPERATURE_INTERMEDIATE, "Intermediate Temperature");
        attributes
                .put(TEMPERATURE_MEASUREMENT_INTERVAL, "Measurement Interval");

        // Device Information Characteristics
        attributes.put(SYSTEM_ID, "System ID");
        attributes.put(MODEL_NUMBER_STRING, "Model Number String");
        attributes.put(SERIAL_NUMBER_STRING, "Serial Number String");
        attributes.put(FIRMWARE_REVISION_STRING, "Firmware Revision String");
        attributes.put(HARDWARE_REVISION_STRING, "Hardware Revision String");
        attributes.put(SOFTWARE_REVISION_STRING, "Software Revision String");
        attributes.put(MANUFACTURER_NAME_STRING, "Manufacturer Name String");
        attributes.put(PNP_ID, "PnP ID");
        attributes.put(IEEE,
                "IEEE 11073-20601 Regulatory Certification Data List");

        // Battery service characteristics
        attributes.put(BATTERY_LEVEL, "Battery Level");

        // Find me service characteristics
        attributes.put(ALERT_LEVEL, "Alert Level");
        attributes.put(TRANSMISSION_POWER_LEVEL, "Tx Power Level");

        // Blood pressure service characteristics
        attributes.put(BLOOD_INTERMEDIATE_CUFF_PRESSURE,
                "Intermediate Cuff Pressure");
        attributes.put(BLOOD_PRESSURE_FEATURE, "Blood Pressure Feature");
        attributes
                .put(BLOOD_PRESSURE_MEASUREMENT, "Blood Pressure Measurement");

        // Unused Services
        attributes
                .put(ALERT_NOTIFICATION_SERVICE, "Alert notification Service");
        attributes.put(BODY_COMPOSITION_SERVICE, "Body Composition Service");
        attributes.put(BOND_MANAGEMENT_SERVICE, "Bond Management Service");
        attributes.put(CONTINUOUS_GLUCOSE_MONITORING_SERVICE,
                "Continuous Glucose Monitoring Service");
        attributes.put(CURRENT_TIME_SERVICE, "Current Time Service");
        attributes.put(CYCLING_POWER_SERVICE, "Cycling Power Service");
        attributes.put(ENVIRONMENTAL_SENSING_SERVICE,
                "Environmental Sensing Service");
        attributes.put(GLUCOSE_SERVICE,
                "Glucose");
        attributes.put(HUMAN_INTERFACE_DEVICE_SERVICE,
                "Human Interface Device Service");
        attributes.put(LOCATION_NAVIGATION_SERVICE,
                "Location and Navigation Service");
        attributes.put(NEXT_DST_CHANGE_SERVICE, "Next DST Change Service");
        attributes
                .put(PHONE_ALERT_STATUS_SERVICE, "Phone Alert Status Service");
        attributes.put(REFERENCE_TIME_UPDATE_SERVICE,
                "Reference Time Update Service");
        attributes.put(SCAN_PARAMETERS_SERVICE, "Scan Paramenters Service");
        attributes.put(USER_DATA_SERVICE, "User Data");
        attributes.put(WEIGHT_SCALE_SERVICE, "Weight Scale");
        attributes.put(AUTOMATION_IO_SERVICE, "Automation IO");
        attributes.put(CONTINUOUS_TIME_SERVICE, "Continuous Time Service");
        attributes.put(HTTP_PROXY_SERVICE, "HTTP Proxy");
        attributes.put(INDOOR_POSITIONING_SERVICE, "Indoor Positioning");
        attributes.put(INTERNET_PROTOCOL_SERVICE, "Internet Protocol Support");
        attributes.put(PULSE_OXIMETER_SERVICE, "Pulse Oximeter");
        attributes.put(TRANSPORT_DISCOVERY, "Transport Discovery");
        attributes.put(RSC_SERVICE, "Running Speed and Cadence");

        // Heart Rate Characteristics.
        attributes.put(HEART_RATE_MEASUREMENT, "Heart Rate Measurement");
        attributes.put(BODY_SENSOR_LOCATION, "Body Sensor Location");
        attributes.put(HEART_RATE_CONTROL_POINT, "Heart Rate Control Point");

        // Health thermometer Characteristics.
        attributes.put(HEALTH_TEMP_MEASUREMENT,
                "Health Thermometer Measurement");
        attributes.put(TEMPERATURE_TYPE, "Temperature Type");
        attributes.put(TEMPERATURE_INTERMEDIATE, "Intermediate Temperature");
        attributes
                .put(TEMPERATURE_MEASUREMENT_INTERVAL, "Measurement Interval");

        // Device Information Characteristics
        attributes.put(SYSTEM_ID, "System ID");
        attributes.put(MODEL_NUMBER_STRING, "Model Number String");
        attributes.put(SERIAL_NUMBER_STRING, "Serial Number String");
        attributes.put(FIRMWARE_REVISION_STRING, "Firmware Revision String");
        attributes.put(HARDWARE_REVISION_STRING, "Hardware Revision String");
        attributes.put(SOFTWARE_REVISION_STRING, "Software Revision String");
        attributes.put(MANUFACTURE_NAME_STRING, "Manufacturer Name String");
        attributes.put(PNP_ID, "PnP ID");
        attributes.put(IEEE,
                "IEEE 11073-20601 Regulatory Certification Data List");

        // Battery service characteristics
        attributes.put(BATTERY_LEVEL, "Battery Level");

        // Find me service characteristics
        attributes.put(ALERT_LEVEL, "Alert Level");
        attributes.put(TRANSMISSION_POWER_LEVEL, "Tx Power Level");

        // Glucose Characteristics
        attributes.put(GLUCOSE_MEASUREMENT, "Glucose Measurement");
        attributes.put(GLUCOSE_MEASUREMENT_CONTEXT,
                "Glucose Measurement Context");
        attributes.put(GLUCOSE_FEATURE, "Glucose Feature");
        attributes.put(RECORD_ACCESS_CONTROL_POINT,
                "Record Access Control Point");

        // Blood pressure service characteristics
        attributes.put(BLOOD_INTERMEDIATE_CUFF_PRESSURE,
                "Intermediate Cuff Pressure");
        attributes.put(BLOOD_PRESSURE_FEATURE, "Blood Pressure Feature");
        attributes
                .put(BLOOD_PRESSURE_MEASUREMENT, "Blood Pressure Measurement");

        // Running Speed Characteristics
        attributes.put(RSC_MEASUREMENT, "Running Speed and Cadence Measurement");
        attributes.put(RSC_FEATURE, "Running Speed and Cadence Feature");
        attributes.put(SC_CONTROL_POINT, "Speed and Cadence Control Point");
        attributes.put(SC_SENSOR_LOCATION, "Speed and Cadence Sensor Location");

        // Cycling Speed Characteristics
        attributes.put(CSC_SERVICE, "Cycling Speed and Cadence");
        attributes.put(CSC_MEASUREMENT, "Cycling Speed and Cadence Measurement");
        attributes.put(CSC_FEATURE, "Cycling Speed and Cadence Feature");

        //OTAU Characteristics
        attributes.put(OTAU_SERVICE, "OTAU Service");
        attributes.put(OTAU_CHARACTERISTIC_1_INDICATE, "OTAU Characteristic Indicate");
        attributes.put(OTAU_CHARACTERISTIC_2_WRITE, "OTAU Characteristic Write");
        attributes.put(OTAU_CHARACTERISTIC_3_READ, "OTAU Characteristic Read");

        // Unused Characteristics
        attributes.put(AEROBIC_HEART_RATE_LOWER_LIMIT,
                "Aerobic Heart Rate Lower Limit");
        attributes.put(AEROBIC_HEART_RATE_UPPER_LIMIT,
                "Aerobic Heart Rate Upper Limit");
        attributes.put(AGE, "Age");
        attributes.put(ALERT_CATEGORY_ID, "Alert Category Id");
        attributes
                .put(ALERT_CATEGORY_ID_BIT_MASK, "Alert Category_id_Bit_Mask");
        attributes.put(ALERT_STATUS, "Alert_Status");
        attributes.put(ANAEROBIC_HEART_RATE_LOWER_LIMIT,
                "Anaerobic Heart Rate Lower Limit");
        attributes.put(ANAEROBIC_HEART_RATE_UPPER_LIMIT,
                "Anaerobic Heart Rate Upper Limit");
        attributes.put(ANAEROBIC_THRESHOLD, "Anaerobic Threshold");
        attributes.put(APPARENT_WIND_DIRECTION, "Apparent Wind Direction");
        attributes.put(APPARENT_WIND_SPEED, "Apparent Wind Speed");
        attributes.put(APPEARANCE, "Appearance");
        attributes.put(BAROMETRIC_PRESSURE_TREND, "Barometric pressure Trend");
        attributes.put(BLOOD_PRESSURE_MEASUREMENT, "Blood Pressure Measurement");
        attributes.put(BODY_COMPOSITION_FEATURE, "Body Composition Feature");
        attributes.put(BODY_COMPOSITION_MEASUREMENT, "Body Composition Measurement");
        attributes.put(BOND_MANAGEMENT_CONTROL_POINT, "Bond Management Control Point");
        attributes.put(BOND_MANAGEMENT_FEATURE, "Bond Management feature");
        attributes.put(CGM_FEATURE, "CGM Feature");
        attributes.put(CENTRAL_ADDRESS_RESOLUTION, "Central Address Resolution");
        attributes.put(FIRSTNAME, "First Name");
        attributes.put(GUST_FACTOR, "Gust Factor");
        attributes.put(CGM_MEASUREMENT, "CGM Measurement");
        attributes.put(CGM_SESSION_RUN_TIME, "CGM Session Run Time");
        attributes.put(CGM_SESSION_START_TIME, "CGM Session Start Time");
        attributes.put(CGM_SPECIFIC_OPS_CONTROL_POINT, "CGM Specific Ops Control Point");
        attributes.put(CGM_STATUS, "CGM Status");
        attributes.put(CYCLING_POWER_CONTROL_POINT, "Cycling Power Control Point");
        attributes.put(CYCLING_POWER_VECTOR, "Cycling Power Vector");
        attributes.put(CYCLING_POWER_FEATURE, "Cycling Power Feature");
        attributes.put(CYCLING_POWER_MEASUREMENT, "Cycling Power Measurement");
        attributes.put(DATABASE_CHANGE_INCREMENT, "Database Change Increment");
        attributes.put(DATE_OF_BIRTH, "Date Of Birth");
        attributes.put(DATE_OF_THRESHOLD_ASSESSMENT, "Date Of Threshold Assessment");
        attributes.put(DATE_TIME, "Date Time");
        attributes.put(DAY_DATE_TIME, "Day Date Time");
        attributes.put(DAY_OF_WEEK, "Day Of Week");
        attributes.put(DESCRIPTOR_VALUE_CHANGED, "Descriptor Value Changed");
        attributes.put(DEVICE_NAME, "Device Name");
        attributes.put(DEW_POINT, "Dew Point");
        attributes.put(DST_OFFSET, "DST Offset");
        attributes.put(ELEVATION, "Elevation");
        attributes.put(EMAIL_ADDRESS, "Email Address");
        attributes.put(EXACT_TIME_256, "Exact Time 256");
        attributes.put(FAT_BURN_HEART_RATE_LOWER_LIMIT, "Fat Burn Heart Rate lower Limit");
        attributes.put(FAT_BURN_HEART_RATE_UPPER_LIMIT, "Fat Burn Heart Rate Upper Limit");
        attributes.put(FIRMWARE_REVISION_STRING, "Firmware Revision String");
        attributes.put(FIVE_ZONE_HEART_RATE_LIMITS, "Five Zone Heart Rate Limits");
        attributes.put(MANUFACTURE_NAME_STRING, "Manufacturer Name String");
        attributes.put(GENDER, "Gender");
        attributes.put(GLUCOSE_FEATURE, "Glucose Feature");
        attributes.put(GLUCOSE_MEASUREMENT, "Glucose Measurement");
        attributes.put(HEART_RATE_MAX, "Heart Rate Max");
        attributes.put(HEAT_INDEX, "Heat Index");
        attributes.put(HEIGHT, "Height");
        attributes.put(HIP_CIRCUMFERENCE, "Hip Circumference");
        attributes.put(HUMIDITY, "Humidity");
        attributes.put(INTERMEDIATE_CUFF_PRESSURE, "Intermediate Cuff Pressure");
        attributes.put(INTERMEDIATE_TEMPERATURE, "Intermediate Temperature");
        attributes.put(IRRADIANCE, "Irradiance");
        attributes.put(LANGUAGE, "Language");
        attributes.put(LAST_NAME, "Last Name");
        attributes.put(LN_CONTROL_POINT, "LN Control Point");
        attributes.put(LN_FEATURE, "LN Feature");
        attributes.put(LOCAL_TIME_INFORMATION, "Local Time Information");
        attributes.put(LOCATION_AND_SPEED, "Location and Speed");
        attributes.put(MAGNETIC_DECLINATION, "Magenetic Declination");
        attributes.put(MAGNETIC_FLUX_DENSITY_2D, "Magentic Flux Density 2D");
        attributes.put(MAGNETIC_FLUX_DENSITY_3D, "Magentic Flux Density 3D");
        attributes.put(MAXIMUM_RECOMMENDED_HEART_RATE, "Maximum Recommended Heart Rate");
        attributes.put(MEASUREMENT_INTERVAL, "Measurement Interval");
        attributes.put(MODEL_NUMBER_STRING, "Model Number String");
        attributes.put(NEW_ALERT, "New Alert");
        attributes.put(NAVIGATION, "Navigation");
        attributes.put(PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS, "Peripheral Preferred Connection Parameters");
        attributes.put(PERIPHERAL_PRIVACY_FLAG, "Peripheral Privacy Flag");
        attributes.put(POLLEN_CONCENTRATION, "Pollen Concentration");
        attributes.put(POSITION_QUALITY, "Position Quality");
        attributes.put(PRESSURE, "Pressure");
        attributes.put(SERVICE_CHANGED, "Service Changed");
        attributes.put(RAINFALL, "Rainfall");

        // Descriptors
        attributes.put(CHARACTERISTIC_EXTENDED_PROPERTIES, "Characteristic Extended Properties");
        attributes.put(CHARACTERISTIC_USER_DESCRIPTION, "Characteristic User Description");
        attributes.put(CLIENT_CHARACTERISTIC_CONFIG, "Client Characteristic Configuration");
        attributes.put(SERVER_CHARACTERISTIC_CONFIGURATION, "Server Characteristic Configuration");
        attributes.put(CHARACTERISTIC_PRESENTATION_FORMAT, "Characteristic Presentation Format");
        attributes.put(CHARACTERISTIC_AGGREGATE_FORMAT, "Characteristic Aggregate Format");
        attributes.put(VALID_RANGE, "Valid Range");
        attributes.put(EXTERNAL_REPORT_REFERENCE, "External Report Reference");
        attributes.put(REPORT_REFERENCE, "Report Reference");
        attributes.put(NUMBER_OF_DIGITALS, "Number of Digitals");
        attributes.put(VALUE_TRIGGER_SETTINGS, "Value Trigger Settings");
        attributes.put(TIME_TRIGGER_SETTINGS, "Time Trigger Settings");
        attributes.put(ENVIRONMENTAL_SENSING_CONFIGURATION, "Environmental Sensing Configuration");
        attributes.put(ENVIRONMENTAL_SENSING_MEASUREMENT, "Environmental Sensing Measurement");
        attributes.put(ENVIRONMENTAL_SENSING_TRIGGER_SETTING, "Environmental Sensing Trigger Setting");


        /**
         * Descriptor key value mapping
         */
        descriptorAttributes.put("0", "Reserved For Future Use");
        descriptorAttributes.put("1", "Boolean");
        descriptorAttributes.put("2", "unsigned 2-bit integer");
        descriptorAttributes.put("3", "unsigned 4-bit integer");
        descriptorAttributes.put("4", "unsigned 8-bit integer");
        descriptorAttributes.put("5", "unsigned 12-bit integer");
        descriptorAttributes.put("6", "unsigned 16-bit integer");
        descriptorAttributes.put("7", "unsigned 24-bit integer");
        descriptorAttributes.put("8", "unsigned 32-bit integer");
        descriptorAttributes.put("9", "unsigned 48-bit integer");
        descriptorAttributes.put("10", "unsigned 64-bit integer");
        descriptorAttributes.put("11", "unsigned 128-bit integer");
        descriptorAttributes.put("12", "signed 8-bit integer");
        descriptorAttributes.put("13", "signed 12-bit integer");
        descriptorAttributes.put("14", "signed 16-bit integer");
        descriptorAttributes.put("15", "signed 24-bit integer");
        descriptorAttributes.put("16", "signed 32-bit integer");
        descriptorAttributes.put("17", "signed 48-bit integer");
        descriptorAttributes.put("18", "signed 64-bit integer");
        descriptorAttributes.put("19", "signed 128-bit integer");
        descriptorAttributes.put("20", "IEEE-754 32-bit floating point");
        descriptorAttributes.put("21", "IEEE-754 64-bit floating point");
        descriptorAttributes.put("22", "IEEE-11073 16-bit SFLOAT");
        descriptorAttributes.put("23", "IEEE-11073 32-bit FLOAT");
        descriptorAttributes.put("24", "IEEE-20601 format");
        descriptorAttributes.put("25", "UTF-8 string");
        descriptorAttributes.put("26", "UTF-16 string");
        descriptorAttributes.put("27", "Opaque Structure");

    }


    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }

    public static String lookCharacteristicPresentationFormat(String key) {
        String value = descriptorAttributes.get(key);
        return value == null ? "Reserved" : value;
    }


    public static boolean lookupreqHRMCharacateristics(String uuid) {
        String name = attributes.get(uuid);
        return name != null;

    }

    public static String getname(String uuid) {
        String name = attributes.get(uuid);
        return name == null ? "Not found" : name;
    }
}