/*
 *
 *
 *     UUIDDatabase.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Attributes;

import java.util.UUID;

/**
 * This class will store the UUID of the GATT services and characteristics
 */
public class UUIDDatabase {
    /**
     * Generic related UUID
     */
    public final static UUID UUID_GENERIC_SERVICE = UUID
            .fromString(GattAttributes.GENERIC_ACCESS_SERVICE);
    public final static UUID UUID_GENERIC_ATTRIBUTR = UUID
            .fromString(GattAttributes.GENERIC_ATTRIBUTE_SERVICE);

    /**
     * Heart rate related UUID
     */
    public final static UUID UUID_HEART_RATE_SERVICE = UUID
            .fromString(GattAttributes.HEART_RATE_SERVICE);
    public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID
            .fromString(GattAttributes.HEART_RATE_MEASUREMENT);
    public final static UUID UUID_BODY_SENSOR_LOCATION = UUID
            .fromString(GattAttributes.BODY_SENSOR_LOCATION);
    /**
     * Device information related UUID
     */
    public final static UUID UUID_DEVICE_INFORMATION_SERVICE = UUID
            .fromString(GattAttributes.DEVICE_INFORMATION_SERVICE);
    public final static UUID UUID_SYSTEM_ID = UUID
            .fromString(GattAttributes.SYSTEM_ID);
    public static final UUID UUID_MANUFATURE_NAME_STRING = UUID
            .fromString(GattAttributes.MANUFACTURER_NAME_STRING);
    public static final UUID UUID_MODEL_NUMBER_STRING = UUID
            .fromString(GattAttributes.MODEL_NUMBER_STRING);
    public static final UUID UUID_SERIAL_NUMBER_STRING = UUID
            .fromString(GattAttributes.SERIAL_NUMBER_STRING);
    public static final UUID UUID_HARDWARE_REVISION_STRING = UUID
            .fromString(GattAttributes.HARDWARE_REVISION_STRING);
    public static final UUID UUID_FIRWARE_REVISION_STRING = UUID
            .fromString(GattAttributes.FIRMWARE_REVISION_STRING);
    public static final UUID UUID_SOFTWARE_REVISION_STRING = UUID
            .fromString(GattAttributes.SOFTWARE_REVISION_STRING);
    public static final UUID UUID_PNP_ID = UUID
            .fromString(GattAttributes.PNP_ID);
    public static final UUID UUID_IEEE = UUID
            .fromString(GattAttributes.IEEE);

    /**
     * Health thermometer related UUID
     */
    public final static UUID UUID_HEALTH_SERVICE = UUID
            .fromString(GattAttributes.HEALTH_TEMP_SERVICE);

    /*
    * Proximity related uuid
    */
    public final static UUID UUID_PROXIMITY_LINK_LOSS_SERVICE = UUID
            .fromString(GattAttributes.LINK_LOSS_SERVICE);


    /**
     * TIME UUID
     */
    public final static UUID UUID_DST_TIME = UUID.fromString(GattAttributes.DST_TIME);
    public final static UUID UUID_CURRENT_TIME = UUID.fromString(GattAttributes.CURRENT_TIME);
    public final static UUID UUID_LOCAL_TIME = UUID.fromString(GattAttributes.LOCAL_TIME);
    public final static UUID UUID_REFERENCE_TIME = UUID.fromString(GattAttributes.REFERENCE_TIME);
    public final static UUID UUID_TIME_UPDATE_CONTROL_POINT = UUID.fromString(GattAttributes.TIME_UPDATE_CONTROL_POINT);
    public final static UUID UUID_TIME_UPDATE_STATE = UUID.fromString(GattAttributes.TIME_UPDATE_STATE);

    /**
     * Phone Alert UUID
     */
    public final static UUID UUID_PHONE_ALERT_SERVICE = UUID.fromString(GattAttributes.PHONE_ALERT_STATUS_SERVICE);
    public final static UUID UUID_PHONE_ALERT_STATUS = UUID.fromString(GattAttributes.PHONE_ALERT_STATUS);
    public final static UUID UUID_PHONE_ALERT_RINGER_SETTINGS = UUID.fromString(GattAttributes.PHONE_ALERT_RINGER_SETTINGS);
    public final static UUID UUID_PHONE_ALERT_CONTROL_POINT = UUID.fromString(GattAttributes.PHONE_ALERT_CONTROL_POINT);

    /**
     * Power consumption
     */
    /*
    * Serial Port UUID
    * */
    public static final UUID UUID_SCAN_INTERVAL_WINDOW_PARAM = UUID.fromString(GattAttributes.SCAN_INTERVAL_WINDOW);
    public static final UUID UUID_SCAN_REFRESH_PARAM = UUID.fromString(GattAttributes.SCAN_REFRESH);
    public final static UUID UUID_SERIAL_PORT = UUID
            .fromString(GattAttributes.SERIAL_PORT);
    public final static UUID UUID_SERIAL_PORT_END_POINT = UUID
            .fromString(GattAttributes.SERIAL_PORT_END_POINT);
    public final static UUID UUID_SERIAL_PORT_CREDITS = UUID
            .fromString(GattAttributes.SERIAL_PORT_CREDITS);

    /*
    * ANS
     */
    public static final UUID UUID_ANS_SUPPORTED_NEW_ALERT_CATEGORY =
            UUID.fromString(GattAttributes.ANS_SUPPORTED_NEW_ALERT_CATEGORY);
    public final static UUID UUID_ANS_NEW_ALERT = UUID
            .fromString(GattAttributes.ANS_NEW_ALERT);
    public final static UUID UUID_ANS_SUPPORTED_UNREAD_ALERT_CATEGORY = UUID
            .fromString(GattAttributes.ANS_SUPPORTED_UNREAD_ALERT_CATEGORY);
    public final static UUID UUID_ANS_UNREAD_ALERT_STATUS = UUID
            .fromString(GattAttributes.ANS_UNREAD_ALERT_STATUS);
    public final static UUID UUID_ANS_CONTROL_POINT = UUID
            .fromString(GattAttributes.ANS_CONTROL_POINT);

    /*
    * OTAU
    */
    public static final UUID UUID_OTAU_SERVICE = UUID.fromString(GattAttributes.OTAU_SERVICE);
    public static final UUID UUID_OTAU_CHARACTERISTIC_1_INDICATE =
            UUID.fromString(GattAttributes.OTAU_CHARACTERISTIC_1_INDICATE);
    public final static UUID UUID_OTAU_CHARACTERISTIC_2_WRITE = UUID
            .fromString(GattAttributes.OTAU_CHARACTERISTIC_2_WRITE);
    public final static UUID UUID_OTAU_CHARACTERISTIC_3_READ = UUID
            .fromString(GattAttributes.OTAU_CHARACTERISTIC_3_READ);

    /**
     * Eddystone
     */

    public static final UUID UUID_EddyStoneConfigService = UUID.fromString(GattAttributes.EddyStoneConfigService);
    public static final UUID UUID_EddyStoneLockStateCharacteristics =
            UUID.fromString(GattAttributes.EddyStoneLockStateCharacteristics);
    public static final UUID  UUID_EddyStoneURICharacteristics = UUID.fromString(GattAttributes.EddyStoneURICharacteristics);
    public static final UUID  UUID_EddyStoneFlagsCharacteristics = UUID.fromString(GattAttributes.EddyStoneFlagsCharacteristics);
    public static final UUID  UUID_EddyStoneTxPowerLevelCharacteristics =
            UUID.fromString(GattAttributes.EddyStoneTxPowerLevelCharacteristics);
    public static final UUID  UUID_EddyStoneTxModeCharacteristics =
            UUID.fromString(GattAttributes.EddyStoneTxModeCharacteristics);
    public static final UUID  UUID_EddyStoneBeaconPeriodCharacteristics = UUID.fromString(GattAttributes.EddyStoneBeaconPeriodCharacteristics);
    public static final UUID  UUID_EddyStoneResetCharacteristics = UUID.fromString(GattAttributes.EddyStoneConfigService);


    /**
     * Health thermometer related UUID
     */
    public final static UUID UUID_HEALTH_THERMOMETER_SERVICE = UUID
            .fromString(GattAttributes.HEALTH_THERMOMETER_SERVICE);
    public final static UUID UUID_HEALTH_THERMOMETER = UUID
            .fromString(GattAttributes.HEALTH_TEMP_MEASUREMENT);
    public final static UUID UUID_HEALTH_THERMOMETER_SENSOR_LOCATION = UUID
            .fromString(GattAttributes.TEMPERATURE_TYPE);
    /**
     * Battery Level related uuid
     */
    public final static UUID UUID_BATTERY_SERVICE = UUID
            .fromString(GattAttributes.BATTERY_SERVICE);
    public final static UUID UUID_BATTERY_LEVEL = UUID
            .fromString(GattAttributes.BATTERY_LEVEL);

    /**
     * Find me related uuid
     */
    public final static UUID UUID_IMMEDIATE_ALERT_SERVICE = UUID
            .fromString(GattAttributes.IMMEDIATE_ALERT_SERVICE);
    public final static UUID UUID_TRANSMISSION_POWER_SERVICE = UUID
            .fromString(GattAttributes.TRANSMISSION_POWER_SERVICE);
    public final static UUID UUID_ALERT_LEVEL = UUID
            .fromString(GattAttributes.ALERT_LEVEL);
    public final static UUID UUID_TRANSMISSION_POWER_LEVEL = UUID
            .fromString(GattAttributes.TRANSMISSION_POWER_LEVEL);
    public final static UUID UUID_LINK_LOSS_SERVICE = UUID
            .fromString(GattAttributes.LINK_LOSS_SERVICE);


    /**
     * GlucoseService related uuid
     */
    public final static UUID UUID_GLUCOSE_MEASUREMENT = UUID
            .fromString(GattAttributes.GLUCOSE_MEASUREMENT);
    public final static UUID UUID_GLUCOSE_SERVICE = UUID
            .fromString(GattAttributes.GLUCOSE_SERVICE);
    public final static UUID UUID_GLUCOSE_MEASUREMENT_CONTEXT = UUID
            .fromString(GattAttributes.GLUCOSE_MEASUREMENT_CONTEXT);
    public final static UUID UUID_GLUCOSE_FEATURE = UUID
            .fromString(GattAttributes.GLUCOSE_FEATURE);
    public final static UUID UUID_RECORD_ACCESS_CONTROL_POINT = UUID
            .fromString(GattAttributes.RECORD_ACCESS_CONTROL_POINT);
    /**
     * Blood pressure related uuid
     */
    public final static UUID UUID_BLOOD_PRESSURE_SERVICE = UUID
            .fromString(GattAttributes.BLOOD_PRESSURE_SERVICE);
    public final static UUID UUID_BLOOD_PRESSURE_MEASUREMENT = UUID
            .fromString(GattAttributes.BLOOD_PRESSURE_MEASUREMENT);
    public final static UUID UUID_BLOOD_INTERMEDIATE_CUFF_PRESSURE = UUID
            .fromString(GattAttributes.BLOOD_INTERMEDIATE_CUFF_PRESSURE);
    public final static UUID UUID_BLOOD_PRESSURE_FEATURE = UUID
            .fromString(GattAttributes.BLOOD_PRESSURE_FEATURE);
    /**
     * Running Speed & Cadence related uuid
     */
    public final static UUID UUID_RSC_MEASURE = UUID
            .fromString(GattAttributes.RSC_MEASUREMENT);
    public final static UUID UUID_RSC_SERVICE = UUID
            .fromString(GattAttributes.RSC_SERVICE);
    public final static UUID UUID_RSC_FEATURE = UUID
            .fromString(GattAttributes.RSC_FEATURE);
    public final static UUID UUID_SC_CONTROL_POINT = UUID
            .fromString(GattAttributes.SC_CONTROL_POINT);
    public final static UUID UUID_SC_SENSOR_LOCATION = UUID
            .fromString(GattAttributes.SC_SENSOR_LOCATION);


    /**
     * Cycling Speed & Cadence related uuid
     */
    public final static UUID UUID_CSC_SERVICE = UUID
            .fromString(GattAttributes.CSC_SERVICE);
    public final static UUID UUID_CSC_MEASURE = UUID
            .fromString(GattAttributes.CSC_MEASUREMENT);
    public final static UUID UUID_CSC_FEATURE = UUID
            .fromString(GattAttributes.CSC_FEATURE);

    /**
     * Descriptor UUID
     */
    public final static UUID UUID_CLIENT_CHARACTERISTIC_CONFIG = UUID
            .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG);
    public final static UUID UUID_CHARACTERISTIC_EXTENDED_PROPERTIES = UUID
            .fromString(GattAttributes.CHARACTERISTIC_EXTENDED_PROPERTIES);
    public final static UUID UUID_CHARACTERISTIC_USER_DESCRIPTION = UUID
            .fromString(GattAttributes.CHARACTERISTIC_USER_DESCRIPTION);
    public final static UUID UUID_SERVER_CHARACTERISTIC_CONFIGURATION = UUID
            .fromString(GattAttributes.SERVER_CHARACTERISTIC_CONFIGURATION);
    public final static UUID UUID_REPORT_REFERENCE = UUID
            .fromString(GattAttributes.REPORT_REFERENCE);
    public final static UUID UUID_CHARACTERISTIC_PRESENTATION_FORMAT = UUID
            .fromString(GattAttributes.CHARACTERISTIC_PRESENTATION_FORMAT);

    /**
     * GATT related UUID
     */
    public final static UUID UUID_GENERIC_ACCESS_SERVICE = UUID
            .fromString(GattAttributes.GENERIC_ACCESS_SERVICE);
    public final static UUID UUID_GENERIC_ATTRIBUTE_SERVICE = UUID
            .fromString(GattAttributes.GENERIC_ATTRIBUTE_SERVICE);


    /**
     * Alert Notification UUID
     */
    public final static UUID UUID_ALERT_NOTIFICATION_SERVICE = UUID
            .fromString(GattAttributes.ALERT_NOTIFICATION_SERVICE);
    /**
     * Unused Service UUID's
     */
    public final static UUID UUID_BODY_COMPOSITION_SERVICE = UUID
            .fromString(GattAttributes.BODY_COMPOSITION_SERVICE);
    public final static UUID UUID_BOND_MANAGEMENT_SERVICE = UUID
            .fromString(GattAttributes.BOND_MANAGEMENT_SERVICE);
    public final static UUID UUID_CONTINUOUS_GLUCOSE_MONITORING_SERVICE = UUID
            .fromString(GattAttributes.CONTINUOUS_GLUCOSE_MONITORING_SERVICE);
    public final static UUID UUID_CURRENT_TIME_SERVICE = UUID
            .fromString(GattAttributes.CURRENT_TIME_SERVICE);
    public final static UUID UUID_CYCLING_POWER_SERVICE = UUID
            .fromString(GattAttributes.CYCLING_POWER_SERVICE);
    public final static UUID UUID_ENVIRONMENTAL_SENSING_SERVICE = UUID
            .fromString(GattAttributes.ENVIRONMENTAL_SENSING_SERVICE);
    public final static UUID UUID_LOCATION_NAVIGATION_SERVICE = UUID
            .fromString(GattAttributes.LOCATION_NAVIGATION_SERVICE);
    public final static UUID UUID_NEXT_DST_CHANGE_SERVICE = UUID
            .fromString(GattAttributes.NEXT_DST_CHANGE_SERVICE);
    public final static UUID UUID_PHONE_ALERT_STATUS_SERVICE = UUID
            .fromString(GattAttributes.PHONE_ALERT_STATUS_SERVICE);
    public final static UUID UUID_REFERENCE_TIME_UPDATE_SERVICE = UUID
            .fromString(GattAttributes.REFERENCE_TIME_UPDATE_SERVICE);
    public final static UUID UUID_SCAN_PARAMETERS_SERVICE = UUID
            .fromString(GattAttributes.SCAN_PARAMETERS_SERVICE);
    public final static UUID UUID_USER_DATA_SERVICE = UUID
            .fromString(GattAttributes.USER_DATA_SERVICE);
    public final static UUID UUID_WEIGHT_SCALE_SERVICE = UUID
            .fromString(GattAttributes.WEIGHT_SCALE_SERVICE);
    public final static UUID UUID_HEART_RATE_CONTROL_POINT = UUID
            .fromString(GattAttributes.HEART_RATE_CONTROL_POINT);
    public final static UUID UUID_TEMPERATURE_INTERMEDIATE = UUID
            .fromString(GattAttributes.TEMPERATURE_INTERMEDIATE);
    public final static UUID UUID_TEMPERATURE_MEASUREMENT_INTERVAL = UUID
            .fromString(GattAttributes.TEMPERATURE_MEASUREMENT_INTERVAL);


    /**
     * Unused Characteristic UUID's
     */
    public final static UUID UUID_AEROBIC_HEART_RATE_LOWER_LIMIT = UUID
            .fromString(GattAttributes.AEROBIC_HEART_RATE_LOWER_LIMIT);
    public final static UUID UUID_AEROBIC_HEART_RATE_UPPER_LIMIT = UUID
            .fromString(GattAttributes.AEROBIC_HEART_RATE_UPPER_LIMIT);
    public final static UUID UUID_AGE = UUID
            .fromString(GattAttributes.AGE);
    public final static UUID UUID_ALERT_CATEGORY_ID = UUID
            .fromString(GattAttributes.ALERT_CATEGORY_ID);
    public final static UUID UUID_ALERT_CATEGORY_ID_BIT_MASK = UUID
            .fromString(GattAttributes.ALERT_CATEGORY_ID_BIT_MASK);
    public final static UUID UUID_ALERT_STATUS = UUID
            .fromString(GattAttributes.ALERT_STATUS);
    public final static UUID UUID_ANAEROBIC_HEART_RATE_LOWER_LIMIT = UUID
            .fromString(GattAttributes.ANAEROBIC_HEART_RATE_LOWER_LIMIT);
    public final static UUID UUID_ANAEROBIC_HEART_RATE_UPPER_LIMIT = UUID
            .fromString(GattAttributes.ANAEROBIC_HEART_RATE_UPPER_LIMIT);
    public final static UUID UUID_ANAEROBIC_THRESHOLD = UUID
            .fromString(GattAttributes.ANAEROBIC_THRESHOLD);
    public final static UUID UUID_APPARENT_WIND_DIRECTION = UUID
            .fromString(GattAttributes.APPARENT_WIND_DIRECTION);
    public final static UUID UUID_APPARENT_WIND_SPEED = UUID
            .fromString(GattAttributes.APPARENT_WIND_SPEED);
    public final static UUID UUID_APPEARANCE = UUID
            .fromString(GattAttributes.APPEARANCE);
    public final static UUID UUID_BAROMETRIC_PRESSURE_TREND = UUID
            .fromString(GattAttributes.BAROMETRIC_PRESSURE_TREND);
    public final static UUID UUID_BODY_COMPOSITION_FEATURE = UUID
            .fromString(GattAttributes.BODY_COMPOSITION_FEATURE);
    public final static UUID UUID_BODY_COMPOSITION_MEASUREMENT = UUID
            .fromString(GattAttributes.BODY_COMPOSITION_MEASUREMENT);
    public final static UUID UUID_BOND_MANAGEMENT_CONTROL_POINT = UUID
            .fromString(GattAttributes.BOND_MANAGEMENT_CONTROL_POINT);
    public final static UUID UUID_BOND_MANAGEMENT_FEATURE = UUID
            .fromString(GattAttributes.BOND_MANAGEMENT_FEATURE);
    public final static UUID UUID_CGM_FEATURE = UUID
            .fromString(GattAttributes.CGM_FEATURE);
    public final static UUID UUID_CENTRAL_ADDRESS_RESOLUTION = UUID
            .fromString(GattAttributes.CENTRAL_ADDRESS_RESOLUTION);
    public final static UUID UUID_FIRSTNAME = UUID
            .fromString(GattAttributes.FIRSTNAME);
    public final static UUID UUID_GUST_FACTOR = UUID
            .fromString(GattAttributes.GUST_FACTOR);
    public final static UUID UUID_CGM_MEASUREMENT = UUID
            .fromString(GattAttributes.CGM_MEASUREMENT);
    public final static UUID UUID_CGM_SESSION_RUN_TIME = UUID
            .fromString(GattAttributes.CGM_SESSION_RUN_TIME);
    public final static UUID UUID_CGM_SESSION_START_TIME = UUID
            .fromString(GattAttributes.CGM_SESSION_START_TIME);
    public final static UUID UUID_CGM_SPECIFIC_OPS_CONTROL_POINT = UUID
            .fromString(GattAttributes.CGM_SPECIFIC_OPS_CONTROL_POINT);
    public final static UUID UUID_CGM_STATUS = UUID
            .fromString(GattAttributes.CGM_STATUS);
    public final static UUID UUID_CYCLING_POWER_CONTROL_POINT = UUID
            .fromString(GattAttributes.CYCLING_POWER_CONTROL_POINT);
    public final static UUID UUID_CYCLING_POWER_VECTOR = UUID
            .fromString(GattAttributes.CYCLING_POWER_VECTOR);
    public final static UUID UUID_CYCLING_POWER_FEATURE = UUID
            .fromString(GattAttributes.CYCLING_POWER_FEATURE);
    public final static UUID UUID_CYCLING_POWER_MEASUREMENT = UUID
            .fromString(GattAttributes.CYCLING_POWER_MEASUREMENT);
    public final static UUID UUID_DATABASE_CHANGE_INCREMENT = UUID
            .fromString(GattAttributes.DATABASE_CHANGE_INCREMENT);
    public final static UUID UUID_DATE_OF_BIRTH = UUID
            .fromString(GattAttributes.DATE_OF_BIRTH);
    public final static UUID UUID_DATE_OF_THRESHOLD_ASSESSMENT = UUID
            .fromString(GattAttributes.DATE_OF_THRESHOLD_ASSESSMENT);
    public final static UUID UUID_DATE_TIME = UUID
            .fromString(GattAttributes.DATE_TIME);
    public final static UUID UUID_DAY_DATE_TIME = UUID
            .fromString(GattAttributes.DAY_DATE_TIME);
    public final static UUID UUID_DAY_OF_WEEK = UUID
            .fromString(GattAttributes.DAY_OF_WEEK);
    public final static UUID UUID_DESCRIPTOR_VALUE_CHANGED = UUID
            .fromString(GattAttributes.DESCRIPTOR_VALUE_CHANGED);
    public final static UUID UUID_DEVICE_NAME = UUID
            .fromString(GattAttributes.DEVICE_NAME);
    public final static UUID UUID_DEW_POINT = UUID
            .fromString(GattAttributes.DEW_POINT);
    public final static UUID UUID_DST_OFFSET = UUID
            .fromString(GattAttributes.DST_OFFSET);
    public final static UUID UUID_ELEVATION = UUID
            .fromString(GattAttributes.ELEVATION);
    public final static UUID UUID_EMAIL_ADDRESS = UUID
            .fromString(GattAttributes.EMAIL_ADDRESS);
    public final static UUID UUID_EXACT_TIME_256 = UUID
            .fromString(GattAttributes.EXACT_TIME_256);
    public final static UUID UUID_FAT_BURN_HEART_RATE_LOWER_LIMIT = UUID
            .fromString(GattAttributes.FAT_BURN_HEART_RATE_LOWER_LIMIT);
    public final static UUID UUID_FAT_BURN_HEART_RATE_UPPER_LIMIT = UUID
            .fromString(GattAttributes.FAT_BURN_HEART_RATE_UPPER_LIMIT);
    public final static UUID UUID_FIVE_ZONE_HEART_RATE_LIMITS = UUID
            .fromString(GattAttributes.FIVE_ZONE_HEART_RATE_LIMITS);
    public final static UUID UUID_GENDER = UUID
            .fromString(GattAttributes.GENDER);
    public final static UUID UUID_HEART_RATE_MAX = UUID
            .fromString(GattAttributes.HEART_RATE_MAX);
    public final static UUID UUID_HEAT_INDEX = UUID
            .fromString(GattAttributes.HEAT_INDEX);
    public final static UUID UUID_HEIGHT = UUID
            .fromString(GattAttributes.HEIGHT);
    public final static UUID UUID_HIP_CIRCUMFERENCE = UUID
            .fromString(GattAttributes.HIP_CIRCUMFERENCE);
    public final static UUID UUID_HUMIDITY = UUID
            .fromString(GattAttributes.HUMIDITY);
    public final static UUID UUID_INTERMEDIATE_CUFF_PRESSURE = UUID
            .fromString(GattAttributes.INTERMEDIATE_CUFF_PRESSURE);
    public final static UUID UUID_INTERMEDIATE_TEMPERATURE = UUID
            .fromString(GattAttributes.INTERMEDIATE_TEMPERATURE);
    public final static UUID UUID_IRRADIANCE = UUID
            .fromString(GattAttributes.IRRADIANCE);
    public final static UUID UUID_LANGUAGE = UUID
            .fromString(GattAttributes.LANGUAGE);
    public final static UUID UUID_LAST_NAME = UUID
            .fromString(GattAttributes.LAST_NAME);
    public final static UUID UUID_LN_CONTROL_POINT = UUID
            .fromString(GattAttributes.LN_CONTROL_POINT);
    public final static UUID UUID_LN_FEATURE = UUID
            .fromString(GattAttributes.LN_FEATURE);
    public final static UUID UUID_LOCAL_TIME_INFORMATION = UUID
            .fromString(GattAttributes.LOCAL_TIME_INFORMATION);
    public final static UUID UUID_LOCATION_AND_SPEED = UUID
            .fromString(GattAttributes.LOCATION_AND_SPEED);
    public final static UUID UUID_MAGNETIC_DECLINATION = UUID
            .fromString(GattAttributes.MAGNETIC_DECLINATION);
    public final static UUID UUID_MAGNETIC_FLUX_DENSITY_2D = UUID
            .fromString(GattAttributes.MAGNETIC_FLUX_DENSITY_2D);
    public final static UUID UUID_MAGNETIC_FLUX_DENSITY_3D = UUID
            .fromString(GattAttributes.MAGNETIC_FLUX_DENSITY_3D);
    public final static UUID UUID_MAXIMUM_RECOMMENDED_HEART_RATE = UUID
            .fromString(GattAttributes.MAXIMUM_RECOMMENDED_HEART_RATE);
    public final static UUID UUID_MEASUREMENT_INTERVAL = UUID
            .fromString(GattAttributes.MEASUREMENT_INTERVAL);
    public final static UUID UUID_NEW_ALERT = UUID
            .fromString(GattAttributes.NEW_ALERT);
    public final static UUID UUID_NAVIGATION = UUID
            .fromString(GattAttributes.NAVIGATION);
    public final static UUID UUID_PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS = UUID
            .fromString(GattAttributes.PERIPHERAL_PREFERRED_CONNECTION_PARAMETERS);
    public final static UUID UUID_PERIPHERAL_PRIVACY_FLAG = UUID
            .fromString(GattAttributes.PERIPHERAL_PRIVACY_FLAG);
    public final static UUID UUID_POLLEN_CONCENTRATION = UUID
            .fromString(GattAttributes.POLLEN_CONCENTRATION);
    public final static UUID UUID_POSITION_QUALITY = UUID
            .fromString(GattAttributes.POSITION_QUALITY);
    public final static UUID UUID_PRESSURE = UUID
            .fromString(GattAttributes.PRESSURE);

    // Descriptors UUID's
    public final static UUID UUID_CHARACTERISTIC_AGGREGATE_FORMAT = UUID
            .fromString(GattAttributes.CHARACTERISTIC_AGGREGATE_FORMAT);
    public final static UUID UUID_VALID_RANGE = UUID
            .fromString(GattAttributes.VALID_RANGE);
    public final static UUID UUID_EXTERNAL_REPORT_REFERENCE = UUID
            .fromString(GattAttributes.EXTERNAL_REPORT_REFERENCE);
    public final static UUID UUID_ENVIRONMENTAL_SENSING_CONFIGURATION = UUID
            .fromString(GattAttributes.ENVIRONMENTAL_SENSING_CONFIGURATION);
    public final static UUID UUID_ENVIRONMENTAL_SENSING_MEASUREMENT = UUID
            .fromString(GattAttributes.ENVIRONMENTAL_SENSING_MEASUREMENT);
    public final static UUID UUID_ENVIRONMENTAL_SENSING_TRIGGER_SETTING = UUID
            .fromString(GattAttributes.ENVIRONMENTAL_SENSING_TRIGGER_SETTING);

}
