/*
 *
 *
 *     AlertLevelCharacteristic.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;


public class AlertLevelCharacteristic {
    private static AlertLevelCharacteristic mAlertLevelCharacteristic;
    public int mAlertValue = -1;
    private BluetoothGattCharacteristic mCharacteristic;

    /**
     * SingletonClass
     * Create private constructor
     */
    private AlertLevelCharacteristic() {
    }

    /**
     * Single instance creation
     */
    public static AlertLevelCharacteristic getInstance() {
        if (mAlertLevelCharacteristic == null) {
            mAlertLevelCharacteristic = new AlertLevelCharacteristic();
        }
        return mAlertLevelCharacteristic;
    }

    public void setAlertLevelCharacteristic(BluetoothGattCharacteristic alertLevelCharacteristic) {
        mCharacteristic = alertLevelCharacteristic;
    }

    public void writeLevel(byte[] data) {
        BLEConnection.setCharacteristicWriteWithoutResponse(mCharacteristic, data);
    }

    public void getLevel() {
        Log.e("mCharacteristic", " " + mCharacteristic);
        BLEConnection.readCharacteristic(mCharacteristic);
    }

    /**
     * Sets the Alert level information from the characteristic
     *
     * @return {@link android.R.integer}
     */
    public void parseData() {
        mAlertValue = mCharacteristic.getIntValue(
                BluetoothGattCharacteristic.FORMAT_UINT8, 0);
    }
}
