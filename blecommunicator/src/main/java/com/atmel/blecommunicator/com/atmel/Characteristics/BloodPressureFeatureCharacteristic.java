/*
 *
 *
 *     BloodPressureFeatureCharacteristic.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.parsers.BloodPressureParser;

public class BloodPressureFeatureCharacteristic {
    private static BloodPressureFeatureCharacteristic mBPFCharacteristic;
    public BluetoothGattCharacteristic mCharacteristic;
    public Boolean mBodyMovementStatus, mCuffFitStatus, mIrregularPulseStatus, mPulsRateStatus, mMeasurementPositionStatus, mMultipleBondStatus;

    /**
     * SingletonClass
     * Create private constructor
     */
    private BloodPressureFeatureCharacteristic() {
    }

    /**
     * Single instance creation
     */
    public static BloodPressureFeatureCharacteristic getInstance() {
        if (mBPFCharacteristic == null) {
            mBPFCharacteristic = new BloodPressureFeatureCharacteristic();
        }
        return mBPFCharacteristic;
    }

    public void setBloodPressureFeatureCharacteristic(BluetoothGattCharacteristic bloodPressureFeatureCharacteristic) {
        mCharacteristic = bloodPressureFeatureCharacteristic;
    }

    public void read() {
        BLEConnection.readCharacteristic(mCharacteristic);
//        parseData();
    }

    public void parseData() {
        Log.d("Parsing feature chars", ">>>>>>>>>>>>>>");
        BloodPressureParser.parseBPFeature(mCharacteristic);
        mBodyMovementStatus = BloodPressureParser.getBodyMovementStatus();
        mCuffFitStatus = BloodPressureParser.getCuffFitStatus();
        mIrregularPulseStatus = BloodPressureParser.getIrregularPulseStatus();
        mPulsRateStatus = BloodPressureParser.getPulsRateStatus();
        mMeasurementPositionStatus = BloodPressureParser.getMeasurementPositionStatus();
        mMultipleBondStatus = BloodPressureParser.getMultipleBondStatus();
    }
}
