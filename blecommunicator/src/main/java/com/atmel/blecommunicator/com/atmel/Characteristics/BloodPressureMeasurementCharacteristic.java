/*
 *
 *
 *     BloodPressureMeasurementCharacteristic.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.parsers.BloodPressureParser;


public class BloodPressureMeasurementCharacteristic {
    private static BloodPressureMeasurementCharacteristic mBPMCharacteristic;
    public BluetoothGattCharacteristic mCharacteristic;
    public String mSysPressure, mDiaPressure, mPressureUnit;
    public Float mPulseRate;
    public boolean mIsIntermediateCuff;

    /**
     * SingletonClass
     * Create private constructor
     */
    private BloodPressureMeasurementCharacteristic() {
    }

    /**
     * Single instance creation
     */
    public static BloodPressureMeasurementCharacteristic getInstance() {
        if (mBPMCharacteristic == null) {
            mBPMCharacteristic = new BloodPressureMeasurementCharacteristic();
        }
        return mBPMCharacteristic;
    }

    public void setBloodPressureMeasurementCharacteristic(BluetoothGattCharacteristic bloodPressureMeasurementCharacteristic) {
        mCharacteristic = bloodPressureMeasurementCharacteristic;
    }

    public void indicate(Boolean enabled) {
        BLEConnection.setCharacteristicIndication(mCharacteristic, enabled);
    }

    public void notify(Boolean enabled) {
        BLEConnection.setCharacteristicNotification(mCharacteristic, enabled);
    }

    public void parseData(boolean isIntermediateCuff) {
        BloodPressureParser.parse(mCharacteristic, isIntermediateCuff);
        mSysPressure = BloodPressureParser.getSystolicBloodPressure();
        mDiaPressure = BloodPressureParser.getDiastolicBloodPressure();
        mPressureUnit = BloodPressureParser.getBloodPressureUnit();
        mPulseRate = BloodPressureParser.getPulseRate();
        mIsIntermediateCuff = BloodPressureParser.ismIsIntermediateCuff();
    }
}
