/*
 *
 *
 *     BodySensorLocationCharacteristic.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.parsers.HRMParser;

/**
 * Created by Atmel on 13/08/15.
 */
public class BodySensorLocationCharacteristic {
    private static BodySensorLocationCharacteristic mBSLCharacteristic;
    public BluetoothGattCharacteristic mCharacteristic;
    public String mBSLValue;

    /**
     * SingletonClass
     * Create private constructor
     */
    private BodySensorLocationCharacteristic() {

    }

    /**
     * Single instance creation
     */
    public static BodySensorLocationCharacteristic getInstance() {
        if (mBSLCharacteristic == null) {
            mBSLCharacteristic = new BodySensorLocationCharacteristic();
        }
        return mBSLCharacteristic;
    }

    public void setBodySensorLocationCharacteristic(BluetoothGattCharacteristic
                                                            bodySensorLocationCharacteristic) {
        mCharacteristic = bodySensorLocationCharacteristic;
    }

    public void read() {
        BLEConnection.readCharacteristic(mCharacteristic);
    }

    public void parseData() {
        mBSLValue = HRMParser.getBodySensorLocation(mCharacteristic);
    }
}
