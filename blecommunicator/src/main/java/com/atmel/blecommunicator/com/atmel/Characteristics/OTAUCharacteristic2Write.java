/*
 *
 *
 *     OTAUCharacteristic2Write.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

import java.util.List;

public class OTAUCharacteristic2Write {

    private static OTAUCharacteristic2Write mOTAUCharacteristic2Write;
    public BluetoothGattCharacteristic mCharacteristic;

    /**
     * SingletonClass
     * Create private constructor
     */
    public OTAUCharacteristic2Write() {
    }

    /**
     * Single instance creation
     */
    public static OTAUCharacteristic2Write getInstance() {
        if (mOTAUCharacteristic2Write == null) {
            mOTAUCharacteristic2Write = new OTAUCharacteristic2Write();
        }
        return mOTAUCharacteristic2Write;
    }

    /**
     * @param OTAUCharacteristic2Write
     */
    public void setOTAUCharacteristic2Write(BluetoothGattCharacteristic OTAUCharacteristic2Write) {
        mCharacteristic = OTAUCharacteristic2Write;
    }

    /**
     * @param frameLength
     * @param command
     * @param productId
     * @param vendorId
     * @param byteFlags
     * @param firmwareVersion
     * @param hardwareVersion
     * @param hardwareRevision
     * @param securityLevel
     */
    public void writeNewImageNotification(int frameLength, int command, int productId, int vendorId,
                                          byte byteFlags, int firmwareVersion, int hardwareVersion, int hardwareRevision, int securityLevel) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[16]);
        mCharacteristic.setValue(frameLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(command, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        mCharacteristic.setValue(productId, BluetoothGattCharacteristic.FORMAT_UINT16, 3);
        mCharacteristic.setValue(vendorId, BluetoothGattCharacteristic.FORMAT_UINT16, 5);
        mCharacteristic.setValue(byteFlags, BluetoothGattCharacteristic.FORMAT_UINT8, 7);
        mCharacteristic.setValue(firmwareVersion, BluetoothGattCharacteristic.FORMAT_UINT32, 8);
        mCharacteristic.setValue(hardwareVersion, BluetoothGattCharacteristic.FORMAT_UINT16, 12);
        mCharacteristic.setValue(hardwareRevision, BluetoothGattCharacteristic.FORMAT_UINT8, 14);
        mCharacteristic.setValue(securityLevel, BluetoothGattCharacteristic.FORMAT_UINT8, 15);
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param frameLength
     * @param command
     * @param totalSections
     * @param totalImageSize
     * @param patchSectionId
     * @param sizeOfPatchSection
     * @param patchStartAddress
     * @param librarySectionId
     * @param sizeOfLibrarySection
     * @param libraryStartAddress
     * @param applicationSectionId
     * @param sizeOfApplicationSection
     * @param applicationStartAddress
     * @param totalImageCrc
     * @param patchSectionCrc
     * @param librarySectionCrc
     * @param applicationSectionCrc
     */
    public void writeImageInfoNotification(int frameLength, int command, int totalSections, int totalImageSize, int patchSectionId,
                                           int sizeOfPatchSection, int patchStartAddress, int librarySectionId, int sizeOfLibrarySection,
                                           int libraryStartAddress, int applicationSectionId, int sizeOfApplicationSection, int applicationStartAddress,
                                           int totalImageCrc, int patchSectionCrc, int librarySectionCrc, int applicationSectionCrc) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[40]);
        mCharacteristic.setValue(frameLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(command, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        mCharacteristic.setValue(totalSections, BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        mCharacteristic.setValue(totalImageSize, BluetoothGattCharacteristic.FORMAT_UINT32, 4);
        mCharacteristic.setValue(patchSectionId, BluetoothGattCharacteristic.FORMAT_UINT8, 8);
        mCharacteristic.setValue(sizeOfPatchSection, 0x13, 9);
        mCharacteristic.setValue(patchStartAddress, BluetoothGattCharacteristic.FORMAT_UINT32, 12);
        mCharacteristic.setValue(librarySectionId, BluetoothGattCharacteristic.FORMAT_UINT8, 16);
        mCharacteristic.setValue(sizeOfLibrarySection, 0x13, 17);
        mCharacteristic.setValue(libraryStartAddress, BluetoothGattCharacteristic.FORMAT_UINT32, 20);
        mCharacteristic.setValue(applicationSectionId, BluetoothGattCharacteristic.FORMAT_UINT8, 24);
        mCharacteristic.setValue(sizeOfApplicationSection, 0x13, 25);
        mCharacteristic.setValue(applicationStartAddress, BluetoothGattCharacteristic.FORMAT_UINT32, 28);
        mCharacteristic.setValue(totalImageCrc, BluetoothGattCharacteristic.FORMAT_UINT16, 32);
        mCharacteristic.setValue(patchSectionCrc, BluetoothGattCharacteristic.FORMAT_UINT16, 34);
        mCharacteristic.setValue(librarySectionCrc, BluetoothGattCharacteristic.FORMAT_UINT16, 36);
        mCharacteristic.setValue(applicationSectionCrc, BluetoothGattCharacteristic.FORMAT_UINT16, 38);
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param dataLength
     * @param command
     * @param sectionId
     * @param pageNo
     * @param pageData
     */
    public void writePageDataNotification(int framelength, int dataLength, int command, int sectionId, int pageNo, int pageData) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[framelength]);
        mCharacteristic.setValue(dataLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(command, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        mCharacteristic.setValue(sectionId, BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        mCharacteristic.setValue(pageNo, BluetoothGattCharacteristic.FORMAT_UINT16, 4);
        //logic pending
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param frameLength
     * @param dataLength
     * @param atOtauBlockDataNotifyRequest
     * @param sectionId
     * @param blockNo
     * @param blockData
     */
    public void writeBlockDataNotification(int frameLength, int dataLength, byte atOtauBlockDataNotifyRequest, int sectionId, int blockNo, int blockData) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[frameLength]);
        mCharacteristic.setValue(dataLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(atOtauBlockDataNotifyRequest, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        mCharacteristic.setValue(sectionId, BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        mCharacteristic.setValue(blockNo, BluetoothGattCharacteristic.FORMAT_UINT16, 4);
        //logic pending
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param frameLength
     * @param atOtauImagePageEndNotifyRequest
     * @param sectionId
     * @param pageNo
     */
    public void writePageDataEndNotification(int frameLength, byte atOtauImagePageEndNotifyRequest, int sectionId, int pageNo) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[frameLength]);
        mCharacteristic.setValue(frameLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(atOtauImagePageEndNotifyRequest, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        mCharacteristic.setValue(sectionId, BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        mCharacteristic.setValue(pageNo, BluetoothGattCharacteristic.FORMAT_UINT16, 4);
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param frameLength
     * @param atOtauImageSectionEndNotifyRequest
     * @param sectionId
     */
    public void writeImageSectionEndNotification(int frameLength, byte atOtauImageSectionEndNotifyRequest, int sectionId) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[frameLength]);
        mCharacteristic.setValue(frameLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(atOtauImageSectionEndNotifyRequest, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        mCharacteristic.setValue(sectionId, BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param frameLength
     * @param atOtauImageEndNotifyRequest
     * @param totalSections
     * @param totalImageSize
     * @param totalImageCrc
     */
    public void writeImageEndNotification(int frameLength, byte atOtauImageEndNotifyRequest, int totalSections, int totalImageSize, int totalImageCrc) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[frameLength]);
        mCharacteristic.setValue(frameLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(atOtauImageEndNotifyRequest, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        mCharacteristic.setValue(totalSections, BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        mCharacteristic.setValue(totalImageSize, BluetoothGattCharacteristic.FORMAT_UINT32, 4);
        mCharacteristic.setValue(totalImageCrc, BluetoothGattCharacteristic.FORMAT_UINT16, 8);
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param frameLength
     * @param atOtauImageSwitchRequest
     * @param totalSections
     * @param firmwareVersion
     */
    public void writeImageSwitchNotification(int frameLength, byte atOtauImageSwitchRequest, int totalSections, int firmwareVersion) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[frameLength]);
        mCharacteristic.setValue(frameLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(atOtauImageSwitchRequest, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        mCharacteristic.setValue(totalSections, BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        mCharacteristic.setValue(firmwareVersion, BluetoothGattCharacteristic.FORMAT_UINT32, 4);
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param frameLength
     * @param atOtauResumeOtauRequest
     */
    public void writeImageResume(int frameLength, byte atOtauResumeOtauRequest) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[3]);
        mCharacteristic.setValue(frameLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(atOtauResumeOtauRequest, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param dataLength
     * @param atOtauGetDeviceInfoRequest
     */
    public void writeGetDeviceInfo(int frameLength, int dataLength, byte atOtauGetDeviceInfoRequest) {
        mCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        mCharacteristic.setValue(new byte[frameLength]);
        mCharacteristic.setValue(dataLength, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        mCharacteristic.setValue(atOtauGetDeviceInfoRequest, BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        BLEConnection.gattWriteWithoutResponse(mCharacteristic);
    }

    /**
     * @param data
     */
    public void writeByteData(byte[] data) {
        BLEConnection.setCharacteristicWriteWithoutResponse(mCharacteristic, data);
    }

    private BluetoothGattCharacteristic getCha() {

        BluetoothDevice mDevice = BLEConnection.getDevice();
        BluetoothGatt mBG = mDevice.connectGatt(BLEConnection.mContext, false, null);


        List<BluetoothGattService> tempServices = mBG.getServices();

        // Loops through available GATT Services
        for (BluetoothGattService gattService : tempServices) {
            if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.OTAU_SERVICE)) {
                for (BluetoothGattCharacteristic mGattCharacteristic : gattService.getCharacteristics()) {
                    switch (mGattCharacteristic.getUuid().toString()) {
                        case GattAttributes.OTAU_CHARACTERISTIC_2_WRITE:
                            OTAUCharacteristic2Write mOTAUCharacteristicWrite =
                                    OTAUCharacteristic2Write.getInstance();
                            mOTAUCharacteristicWrite.setOTAUCharacteristic2Write(mGattCharacteristic);
                            return mGattCharacteristic;
                    }
                }
            }
        }
        return null;
    }

}
