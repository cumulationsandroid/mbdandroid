/*
 *
 *
 *     OTAUCharacteristic3Read.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

public class OTAUCharacteristic3Read {

    private static OTAUCharacteristic3Read mOTAUIndicateCharacteristic;
    public BluetoothGattCharacteristic mCharacteristic;

    /**
     * SingletonClass
     * Create private constructor
     */
    private OTAUCharacteristic3Read() {
    }

    /**
     * Single instance creation
     */
    public static OTAUCharacteristic3Read getInstance() {
        if (mOTAUIndicateCharacteristic == null) {
            mOTAUIndicateCharacteristic = new OTAUCharacteristic3Read();
        }
        return mOTAUIndicateCharacteristic;
    }

    public void setOTAUCharacteristic3Read(BluetoothGattCharacteristic OTAUCharacteristic3Read) {
        mCharacteristic = OTAUCharacteristic3Read;
    }

    /**
     *
     */
    public void read() {
        BLEConnection.readCharacteristic(mCharacteristic);
    }

}
