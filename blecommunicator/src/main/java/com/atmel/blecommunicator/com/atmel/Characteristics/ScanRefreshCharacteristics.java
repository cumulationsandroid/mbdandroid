/*
 *
 *
 *     ScanRefreshCharacteristics.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.parsers.ScanParamPrser;

/**
 * Created by
 */
public class ScanRefreshCharacteristics {
    private static ScanRefreshCharacteristics mScanRefreshCharacteristics;
    private BluetoothGattCharacteristic mBluetoothGattCharacteristic;
    private int refreshValue = -1;

    /**
     * Disable constructor
     */
    public ScanRefreshCharacteristics() {
    }

    /**
     * Making SingleTone
     *
     * @return
     */
    public static ScanRefreshCharacteristics getInstance() {
        return mScanRefreshCharacteristics == null ? mScanRefreshCharacteristics = new ScanRefreshCharacteristics() : mScanRefreshCharacteristics;
    }

    public int getRefreshValue() {
        return refreshValue;
    }

    public void setRefreshValue(int refreshValue) {
        this.refreshValue = refreshValue;
    }

    /**
     * @param indication
     */
    public void startStopNotifyScanInterval(Boolean indication) {
        BLEConnection.setCharacteristicNotification(mBluetoothGattCharacteristic, indication);
    }

    /**
     * @param scanRefreshCharacteristics
     */
    public void setScanRefreshCharacteristics(BluetoothGattCharacteristic scanRefreshCharacteristics) {
        mBluetoothGattCharacteristic = scanRefreshCharacteristics;
    }

    public void parseData() {
        refreshValue = ScanParamPrser.getvalue(mBluetoothGattCharacteristic);
    }
}
