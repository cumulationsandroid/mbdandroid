/*
 *
 *
 *     SerialPortCharacteristic.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;


public class SerialPortCharacteristic {
    private static SerialPortCharacteristic mSerialPortCharacteristic;
    public BluetoothGattCharacteristic mCharacteristic;
    public int mAlertValue = -1;

    /**
     * SingletonClass
     * Create private constructor
     */
    private SerialPortCharacteristic() {
    }

    /**
     * Single instance creation
     */
    public static SerialPortCharacteristic getInstance() {
        if (mSerialPortCharacteristic == null) {
            mSerialPortCharacteristic = new SerialPortCharacteristic();
        }
        return mSerialPortCharacteristic;
    }

    public void setSerialPortCharacteristic(BluetoothGattCharacteristic alertLevelCharacteristic) {
        mCharacteristic = alertLevelCharacteristic;
    }

    public void writeData(byte[] data) {
        BLEConnection.setCharacteristicWriteWithoutResponse(mCharacteristic, data);
    }

    public void getData() {
        BLEConnection.readCharacteristic(mCharacteristic);
    }

    /**
     * Returns the Alert level information from the characteristic
     *
     * @return {@link android.R.integer}
     */
    public void parseData() {
        mAlertValue = mCharacteristic.getIntValue(
                BluetoothGattCharacteristic.FORMAT_UINT8, 0);
    }

    /**
     * notify data from devices
     */
    public void notifySPP(boolean isEnabled) {
        BLEConnection.setCharacteristicNotification(mCharacteristic, isEnabled);
    }
}
