/*
 *
 *
 *     TxPowerLevelCharacteristic.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Characteristics;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;


public class TxPowerLevelCharacteristic {
    private static TxPowerLevelCharacteristic mTxLevelCharacteristic;
    public BluetoothGattCharacteristic mCharacteristic;
    public int mTxvalue = 0;

    /**
     * SingletonClass
     * Create private constructor
     */
    private TxPowerLevelCharacteristic() {
    }

    /**
     * Single instance creation
     */
    public static TxPowerLevelCharacteristic getInstance() {
        if (mTxLevelCharacteristic == null) {
            mTxLevelCharacteristic = new TxPowerLevelCharacteristic();
        }
        return mTxLevelCharacteristic;
    }

    public void setTxPowerLevelCharacteristic(BluetoothGattCharacteristic txLevelCharacteristic) {
        mCharacteristic = txLevelCharacteristic;
    }

    public void read() {
        BLEConnection.readCharacteristic(mCharacteristic);
    }

    /**
     * Returns the Transmission power information from the characteristic
     *
     * @return {@link android.R.integer}
     */
    public void parseData() {
        mTxvalue = mCharacteristic.getIntValue(
                BluetoothGattCharacteristic.FORMAT_SINT8, 0);
    }
}
