/*
 *
 *
 *     BLEConnection.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Connection;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ResultReceiver;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Attributes.Constants;
import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Attributes.GattDescriptorEvent;
import com.atmel.blecommunicator.com.atmel.Attributes.GattErrorEvent;
import com.atmel.blecommunicator.com.atmel.Attributes.GattEvent;
import com.atmel.blecommunicator.com.atmel.Characteristics.AlertLevelCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.BloodPressureFeatureCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.BloodPressureMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.BodySensorLocationCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.HeartRateMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.ScanRefreshCharacteristics;
import com.atmel.blecommunicator.com.atmel.Characteristics.TemperatureMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.TemperatureTypeCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.TxPowerLevelCharacteristic;
import com.atmel.blecommunicator.com.atmel.Services.BatteryService;
import com.atmel.blecommunicator.com.atmel.Services.DeviceInformationService;
import com.atmel.blecommunicator.com.atmel.Services.EddystoneConfigService;
import com.atmel.blecommunicator.com.atmel.Services.SerialPortService;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;
import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUManager;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class BLEConnection extends Service {

    /**
     * Connection status Constants
     */
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_DISCONNECTING = 4;
    public static final int GATT_SUCCESS = 0;
    public static final Handler handlerDelayed = new Handler(Looper.getMainLooper());
    private final static String TAG = BLEConnection.class.getSimpleName();
    public static Boolean mReconnectionEnabled = false;
    public static int mBondstate = BluetoothDevice.BOND_NONE;
    public static BluetoothGattServer mGattServer;
    public static OTAResponseListener mOtaResponseListener = null;
    public static BluetoothGatt mBluetoothGatt;
    public static Context mContext;
    public static boolean needServiceDiscover = false;
    /**
     * EventBus for GATT DB
     */
    public static EventBus mConnectionBus = EventBus.getDefault();
    public static GattEvent mEvent = null;
    public static GattDescriptorEvent mDescEvent = null;
    public static GattErrorEvent mErrorEvent = null;
    static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    /*    static ThreadPoolExecutor executor = new ThreadPoolExecutor(
                NUMBER_OF_CORES * 2,
                NUMBER_OF_CORES * 2,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>()
        );*/
    static ExecutorService executor = Executors.newSingleThreadExecutor();
    private static BluetoothAdapter mBluetoothAdapter;
    private static BluetoothDevice mDevice;
    private static int mConnectionState = STATE_DISCONNECTED;
    private static int mGattServerConnectionState = STATE_DISCONNECTED;
    private static ResultReceiver mResultReceiver;
    private static BatteryService mBatteryService;
    private static boolean canWrite = true;
    private static final Runnable writeCharaRunnable = new Runnable() {
        @Override
        public void run() {
            canWrite = true;
        }
    };
    private static final Runnable IndicationCharaRunnable = new Runnable() {
        @Override
        public void run() {
            canWrite = true;
        }
    };
    private static boolean noCallback = true;
    private static Runnable readCharaRunnable = new Runnable() {
        @Override
        public void run() {
            canWrite = true;
        }
    };
    private static Runnable notificationCharRunnable = new Runnable() {
        @Override
        public void run() {
            canWrite = true;
        }
    };
    private static Runnable addServiceRunnable = new Runnable() {
        @Override
        public void run() {
            canWrite = true;
        }
    };
    private static Runnable notifyDeviceRunnable = new Runnable() {
        @Override
        public void run() {
            canWrite = true;
        }
    };
    /**
     * Device address
     */
    private static String mBluetoothDeviceAddress;
    private static String mBluetoothDeviceName;
    private static EddystoneConfigService mEddystoneConfigService;
    private final static BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.v(TAG, "onConnectionStateChange  -- " + newState);
            canWrite = true;

            // GATT Server connected
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mConnectionState = STATE_CONNECTED;
                mBondstate = BluetoothDevice.BOND_NONE;
                Bundle bundle = new Bundle();
                bundle.putString(Constants.DEVICE_CONNECTED_NAME_KEY, gatt.getDevice().getName());

                /**
                 * Checking if the application running sent to background or foreground.
                 */
                if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                    if (!isApplicationBackgroundCompat(mContext.getApplicationContext())) {
                        mResultReceiver.send(Constants.DEVICE_CONNECTED_KEY, bundle);
                    }
                } else {
                    if (!isApplicationBackground(mContext.getApplicationContext())) {
                        Log.e("inside ", "isApplicationBackground");
                        mResultReceiver.send(Constants.DEVICE_CONNECTED_KEY, bundle);
                    }
                }
                Log.v(TAG, "STATE_CONNECTED");
            }

            // GATT Server disconnected
            else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                //     executor.shutdown();
                OTAUManager otauManager = OTAUManager.getInstance();
                if (otauManager != null) {
                    otauManager.clear();
                }
                mConnectionState = STATE_DISCONNECTED;
                Bundle bundle = new Bundle();
                bundle.putString(Constants.DEVICE_DISCONNECTED_NAME_KEY, gatt.getDevice().getName());

                /**
                 * Checking if the application running sent to background or foreground.
                 */
                if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                    if (!isApplicationBackgroundCompat(mContext.getApplicationContext())) {
                        mResultReceiver.send(Constants.DEVICE_DISCONNECTED_KEY, bundle);
                    }
                } else {
                    if (!isApplicationBackground(mContext.getApplicationContext())) {
                        Log.e("inside ", "isApplicationBackground");
                        mResultReceiver.send(Constants.DEVICE_DISCONNECTED_KEY, bundle);
                    }
                }
                Log.v(TAG, "STATE_DISCONNECTED");
            }

            // GATT Server disconnected
            else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                mConnectionState = STATE_DISCONNECTING;
                Log.v(TAG, "STATE_DISCONNECTING");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            // GATT Services discovered
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.SERVICE_DISCOVERED_STATUS_KEY,
                        true);
                mResultReceiver.send(Constants.SERVICE_DISCOVERED_KEY, bundle);
            } else {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.SERVICE_DISCOVERED_STATUS_KEY,
                        false);
                mResultReceiver.send(Constants.SERVICE_DISCOVERED_KEY, bundle);
                Log.v(TAG, "Error status:" + status);
            }
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            canWrite = true;
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.REMOTE_RSSI_VALUE, rssi);
            mResultReceiver.send(Constants.REMOTE_RSSI_KEY, bundle);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            canWrite = true;
           /* handlerDelayed.removeCallbacks(notificationCharRunnable);
            handlerDelayed.removeCallbacks(IndicationCharaRunnable);*/
            byte[] val = descriptor.getValue();
            String characteristic = descriptor.getCharacteristic().getUuid().toString();
            Bundle bundle = new Bundle();

            mDescEvent = new GattDescriptorEvent(descriptor);
            mConnectionBus.post(mDescEvent);

            switch (val[0]) {
                case 0:
                    bundle.putBoolean(Constants.DESCRIPTOR_WRITE_STATUS_KEY,
                            false);
                    break;
                case 1:
                    bundle.putBoolean(Constants.DESCRIPTOR_WRITE_STATUS_KEY,
                            true);
                    break;
                case 2:
                    bundle.putBoolean(Constants.DESCRIPTOR_WRITE_STATUS_KEY,
                            true);
                    break;
            }

            bundle.putString(Constants.DESCRIPTOR_WRITE_CHARACTERISTIC, characteristic);
            mResultReceiver.send(Constants.DESCRIPTOR_WRITE_KEY, bundle);

            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.v(TAG, "Descriptor write success");
            } else if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) {
                Log.v(TAG, "Pairing required");
                mErrorEvent = new GattErrorEvent(status);
                mConnectionBus.post(mErrorEvent);
            } else {
                mErrorEvent = new GattErrorEvent(status);
                mConnectionBus.post(mErrorEvent);
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,
                                     int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                byte[] val = descriptor.getValue();
                String characteristic = descriptor.getCharacteristic().getUuid().toString();
                Bundle bundle = new Bundle();

                mDescEvent = new GattDescriptorEvent(descriptor);
                mConnectionBus.post(mDescEvent);

            } else if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) {
                Log.v(TAG, "Pairing required");
                mErrorEvent = new GattErrorEvent(status);
                mConnectionBus.post(mErrorEvent);
            } else {
                Log.v(TAG, "Descriptor read status code--" + status);
                mErrorEvent = new GattErrorEvent(status);
                mConnectionBus.post(mErrorEvent);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic
                characteristic) {
            SerialPortService mSerialPortService;
            super.onCharacteristicChanged(gatt, characteristic);

            //Log.e(TAG, "on characteristics changed >> " + characteristic.getUuid().toString());
            switch (characteristic.getUuid().toString()) {
                case GattAttributes.HEALTH_TEMP_MEASUREMENT:
                    TemperatureMeasurementCharacteristic htpTempMeasurementChar =
                            TemperatureMeasurementCharacteristic.getInstance();
                    htpTempMeasurementChar.setTemperatureMeasurementCharacteristic(characteristic);
                    htpTempMeasurementChar.parseData();
                    break;
                case GattAttributes.HEART_RATE_MEASUREMENT:
                    HeartRateMeasurementCharacteristic mHRMCharacteristicChar =
                            HeartRateMeasurementCharacteristic.getInstance();
                    mHRMCharacteristicChar.setHeartRateMeasurementCharacteristic(characteristic);
                    mHRMCharacteristicChar.parseData();
                    break;
                case GattAttributes.BLOOD_PRESSURE_MEASUREMENT:
                    Log.d("Blood Pressure", ">>>>>>>>>>>>>>>");
                    BloodPressureMeasurementCharacteristic mBPMeasurementChar =
                            BloodPressureMeasurementCharacteristic.getInstance();
                    mBPMeasurementChar.setBloodPressureMeasurementCharacteristic(characteristic);
                    mBPMeasurementChar.parseData(false);
                    break;
                case GattAttributes.BLOOD_INTERMEDIATE_CUFF_PRESSURE:
                    Log.d("Inter Cuff Pressure", ">>>>>>>>>>>>>>>");
                    BloodPressureMeasurementCharacteristic mCuffPressureChar =
                            BloodPressureMeasurementCharacteristic.getInstance();
                    mCuffPressureChar.setBloodPressureMeasurementCharacteristic(characteristic);
                    mCuffPressureChar.parseData(true);
                    break;
                case GattAttributes.SCAN_REFRESH:
                    ScanRefreshCharacteristics instance = ScanRefreshCharacteristics.getInstance();
                    instance.setScanRefreshCharacteristics(characteristic);
                    instance.parseData();
                    break;
                case GattAttributes.BATTERY_LEVEL:
                    mBatteryService = BatteryService.getInstance();
                    mBatteryService.parseValue(characteristic);
                    break;
                case GattAttributes.SERIAL_PORT_END_POINT:
                    mSerialPortService = SerialPortService.getInstance();
                    Log.e("HashCode SPP", "" + System.identityHashCode(characteristic.getService()));
                    Log.e("characteristic size", "" + characteristic.getService().getCharacteristics().size());
                    Log.e("HashCode characteristic", "" + System.identityHashCode(characteristic));
                    mSerialPortService.parseValue(characteristic);
                    break;
                case GattAttributes.OTAU_CHARACTERISTIC_2_WRITE:
                    Log.d("OTAU Write", ">>>>>>>>>>>>>>>");
//                    OTAUCharacteristic2Write mOTAUWrite =
//                            OTAUCharacteristic2Write.getInstance();
//                    mOTAUWrite.setOTAUCharacteristic2Write(characteristic);
                    break;
                case GattAttributes.OTAU_CHARACTERISTIC_1_INDICATE:
                    //Here we get the response to each command
                    byte[] response = characteristic.getValue();
                    Log.e("OtaResponse class>>> ", mOtaResponseListener.getClass().toString());
                    Log.e("Response>>> ", ByteArraytoHex(response));
                    mOtaResponseListener.handleCommandResponse(characteristic);
                    break;
            }


            mEvent = new GattEvent(characteristic);
            mConnectionBus.post(mEvent);

            final String data = Utils.parse(characteristic);
            final BluetoothGattDescriptor cccd = characteristic.getDescriptor(UUID.fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
            final boolean notifications = cccd == null || cccd.getValue() == null || cccd.getValue().length != 2 || cccd.getValue()[0] == 0x01;

            if (notifications) {
                Log.d("Notification received>", characteristic.getUuid().toString() + ", value: " + data);
            } else { // indications
                Log.d("Indication received>", characteristic.getUuid().toString() + ", value: " + data);
            }

            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.CHARACTERISTIC_CHANGE_STATUS_KEY,
                    true);
            mResultReceiver.send(Constants.CHARACTERISTICS_CHANGED_KEY, bundle);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Boolean statusValue = false;
            canWrite = true;
//            handlerDelayed.removeCallbacks(writeCharaRunnable);
            super.onCharacteristicWrite(gatt, characteristic, status);

            mEvent = new GattEvent(characteristic);
            mConnectionBus.post(mEvent);

            Bundle bundle = new Bundle();
            bundle.putInt(Constants.CHARACTERISTIC_WRITE_STATUS_KEY,
                    status);
            String characteristics = characteristic.getUuid().toString();
            switch (characteristics) {
                case GattAttributes.EddyStoneResetCharacteristics:
                case GattAttributes.EddyStoneUnlockCharacteristics:
                case GattAttributes.EddyStoneLockCodeCharacteristics:
                    bundle.putString(Constants.CHARACTERISTIC_WRITE_STATUS_CODE,
                            characteristic.getUuid().toString());
                    break;
                case GattAttributes.OTAU_CHARACTERISTIC_2_WRITE:
                    Log.e("BLE Connection ", "onCharacteristicWrite " + characteristic.getUuid());
                    break;
                default:
                    break;
            }
            mResultReceiver.send(Constants.CHARACTERISTICS_WRITE_KEY, bundle);

            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.e("BLE Connection ", "onCharacteristicWrite " + "GATT Success");
            } else {
                mErrorEvent = new GattErrorEvent(status);
                mConnectionBus.post(mErrorEvent);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            DeviceInformationService mDeviceInformationService;
            super.onCharacteristicRead(gatt, characteristic, status);

            mEvent = new GattEvent(characteristic);
            mConnectionBus.post(mEvent);

            canWrite = true;
//            handlerDelayed.removeCallbacks(readCharaRunnable);
            Log.e("BLE Connection ", "onCharacteristicRead " + characteristic.getUuid());
            String mValue;
            Bundle bundle = new Bundle();
            switch (characteristic.getUuid().toString()) {
                case GattAttributes.TEMPERATURE_TYPE:
                    TemperatureTypeCharacteristic htpTempTypeChar = TemperatureTypeCharacteristic.getInstance();
                    htpTempTypeChar.parseData(characteristic.getValue());
                    break;
                case GattAttributes.TRANSMISSION_POWER_LEVEL:
                    TxPowerLevelCharacteristic txPowerLevelChar = TxPowerLevelCharacteristic.getInstance();
                    txPowerLevelChar.parseData();
                    break;
                case GattAttributes.ALERT_LEVEL:
                    AlertLevelCharacteristic alertLevelCharacteristic =
                            AlertLevelCharacteristic.getInstance();
                    alertLevelCharacteristic.parseData();
                    break;

                case GattAttributes.MANUFACTURER_NAME_STRING:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getManufacturerNameString(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 1);
                    break;

                case GattAttributes.MODEL_NUMBER_STRING:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getModelNumberString(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 2);
                    break;

                case GattAttributes.SERIAL_NUMBER_STRING:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getSerialNumberString(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 3);
                    break;

                case GattAttributes.HARDWARE_REVISION_STRING:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getHardwareRevisionString(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 4);
                    break;

                case GattAttributes.FIRMWARE_REVISION_STRING:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getFirmwareRevisionString(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 5);
                    break;

                case GattAttributes.SOFTWARE_REVISION_STRING:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getSoftwareRevisionString(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 6);
                    break;

                case GattAttributes.SYSTEM_ID:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getSYSID(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 7);
                    break;

                case GattAttributes.IEEE:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getRegulatoryDataList(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 8);
                    break;

                case GattAttributes.PNP_ID:
                    mDeviceInformationService = DeviceInformationService.getInstance();
                    mValue = mDeviceInformationService.getPNPID(characteristic);
                    bundle.putString(Constants.CHARACTERISTIC_READ_DATA, mValue);
                    bundle.putInt(Constants.CHARACTERISTIC_READ_DATA_CODE, 9);
                    break;

                case GattAttributes.BODY_SENSOR_LOCATION:
                    BodySensorLocationCharacteristic bslCharacteristic = BodySensorLocationCharacteristic.getInstance();
                    bslCharacteristic.parseData();
                    break;


                case GattAttributes.BLOOD_PRESSURE_FEATURE:
                    BloodPressureFeatureCharacteristic bCharacteristic = BloodPressureFeatureCharacteristic.getInstance();
                    bCharacteristic.parseData();
                    break;

                case GattAttributes.BATTERY_LEVEL:
                    mBatteryService = BatteryService.getInstance();
                    mBatteryService.parseValue(characteristic);
                    break;

                //Eddystone config
                case GattAttributes.EddyStoneLockStateCharacteristics:
                    mEddystoneConfigService = EddystoneConfigService.getInstance();
                    mEddystoneConfigService.setLockStateCharacteristics(characteristic);
                    break;

                //Eddystone config
                case GattAttributes.EddyStoneURICharacteristics:
                    mEddystoneConfigService = EddystoneConfigService.getInstance();
                    mEddystoneConfigService.setUriCharacteristics(characteristic);
                    //bundle.putString(Constants.CHARACTERISTIC_READ_DATA, GattAttributes.EddyStoneLockStateCharacteristics);
                    break;

                case GattAttributes.EddyStoneFlagsCharacteristics:
                    mEddystoneConfigService = EddystoneConfigService.getInstance();
                    mEddystoneConfigService.setFlags(characteristic);
                    break;

                case GattAttributes.EddyStoneTxModeCharacteristics:
                    mEddystoneConfigService = EddystoneConfigService.getInstance();
                    mEddystoneConfigService.setTxPowerMode(characteristic);
                    break;

                case GattAttributes.EddyStoneTxPowerLevelCharacteristics:
                    mEddystoneConfigService = EddystoneConfigService.getInstance();
                    mEddystoneConfigService.setTxPowerLevels(characteristic);
                    break;

                case GattAttributes.EddyStoneBeaconPeriodCharacteristics:
                    mEddystoneConfigService = EddystoneConfigService.getInstance();
                    mEddystoneConfigService.setBeaconPeriod(characteristic);
                    break;

                default:
                    break;
            }

            bundle.putBoolean(Constants.CHARACTERISTIC_READ_STATUS_KEY,
                    true);
            mResultReceiver.send(Constants.CHARACTERISTICS_READ_KEY, bundle);
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
            Log.e("onMtuChanged", "onMtuChanged " + mtu);
        }
    };

    private static String mOtaFileSelected = null;
    private static OTAUManager otauManager = null;
    private static BluetoothLeAdvertiser mBluetoothLeAdvertiser;
    private static BluetoothGattServerCallback mGattServerCallback = new BluetoothGattServerCallback() {
        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            Log.d(TAG, "Server onConnectionStateChange " + newState);
            super.onConnectionStateChange(device, status, newState);
            canWrite = true;

            // GATT Server connected
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mConnectionState = STATE_CONNECTED;
                mGattServerConnectionState = STATE_CONNECTED;
                mBondstate = BluetoothDevice.BOND_NONE;
                Bundle bundle = new Bundle();
                bundle.putString(Constants.DEVICE_CONNECTED_NAME_KEY, device.getName());
                /**
                 * Checking if the application running sent to background or foreground.
                 */

                if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                    if (!isApplicationBackgroundCompat(mContext.getApplicationContext())) {
                        mResultReceiver.send(Constants.DEVICE_CONNECTED_KEY, bundle);
                    }
                } else {
                    if (!isApplicationBackground(mContext.getApplicationContext())) {
                        Log.e("inside ", "isApplicationBackground");
                        mResultReceiver.send(Constants.DEVICE_CONNECTED_KEY, bundle);
                    }
                }

                Log.v(TAG, "Server STATE_CONNECTED");
            }
            // GATT Server disconnected
            else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

                mConnectionState = STATE_DISCONNECTED;
                mGattServerConnectionState = STATE_DISCONNECTED;
                Bundle bundle = new Bundle();
                bundle.putString(Constants.DEVICE_DISCONNECTED_NAME_KEY, device.getName());

                /**
                 * Checking if the application running sent to background or foreground.
                 */

                if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                    if (!isApplicationBackgroundCompat(mContext.getApplicationContext())) {
                        mResultReceiver.send(Constants.DEVICE_DISCONNECTED_KEY, bundle);
                    }
                } else {
                    if (!isApplicationBackground(mContext.getApplicationContext())) {
                        Log.e("inside ", "isApplicationBackground");
                        mResultReceiver.send(Constants.DEVICE_DISCONNECTED_KEY, bundle);
                    }
                }
                Log.v(TAG, "Server STATE_DISCONNECTED");
            }
            // GATT Server disconnected
            else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                mConnectionState = STATE_DISCONNECTING;
                mGattServerConnectionState = STATE_DISCONNECTING;
                Log.v(TAG, "Server STATE_DISCONNECTING");
            }
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onNotificationSent(BluetoothDevice device, int status) {
            canWrite = true;
//            handlerDelayed.removeCallbacks(notifyDeviceRunnable);
            Log.d(TAG, "onNotificationSent");
            super.onNotificationSent(device, status);
        }


        @Override
        public void onServiceAdded(int status, BluetoothGattService service) {
            canWrite = true;
//            handlerDelayed.removeCallbacks(addServiceRunnable);
            boolean added;
            super.onServiceAdded(status, service);
            added = status == 0;
            Log.d(TAG, "onServiceAdded");
            Bundle serviceBundle = new Bundle();
            serviceBundle.putBoolean(Constants.GATTSERVER_SERVICES_DISCOVERED_STATUS, added);
            try {
                serviceBundle.putString(Constants.GATTSERVER_SERVICES_ADDED_SERVICE, service.getUuid().toString());
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
            mResultReceiver.send(Constants.DEVICE_SERVICES_DISCOVERED, serviceBundle);
            //notifyConnectedDevices();
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
            Log.d(TAG, "onCharacteristicReadRequest");
            switch (characteristic.getUuid().toString()) {

                case "00002a47-0000-1000-8000-00805f9b34fb":
                case "00002a48-0000-1000-8000-00805f9b34fb":

                    Log.d(TAG, "onCharacteristicReadRequest" + characteristic.getUuid().toString());

                    byte[] value = null;
                    // String utc = null;
                    // int utcValue = 0;

                    if (offset == 0) {

                        value = Utils.hexStringToByteArray("30");
                    }

                    BLEConnection.GattServerSendResponse(requestId, offset, value);
                    break;

                default:
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.GATTSERVER_READ_CHARACTERISTICS, characteristic.getUuid().toString());
                    bundle.putInt(Constants.GATTSERVER_READ_REQUESTID, requestId);
                    bundle.putInt(Constants.GATTSERVER_READ_OFFSET, offset);
                    mResultReceiver.send(Constants.DEVICE_READ_REQUEST, bundle);
                    break;
            }

            /*
            Bundle bundle = new Bundle();
            bundle.putString(Constants.GATTSERVER_READ_CHARACTERISTICS, characteristic.getUuid().toString());
            bundle.putInt(Constants.GATTSERVER_READ_REQUESTID, requestId);
            bundle.putInt(Constants.GATTSERVER_READ_OFFSET, offset);
            mResultReceiver.send(Constants.DEVICE_READ_REQUEST, bundle);
            */
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId, BluetoothGattCharacteristic characteristic, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
            Log.d(TAG, "onCharacteristicWriteRequest " + characteristic.getUuid().toString());
            super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value);

            switch (characteristic.getUuid().toString()) {
                case "00001806-0000-1000-8000-00805f9b34fb":
                    byte[] val = new byte[2];
                    val[0] = (byte) 0;
                    val[0] = (byte) 0;
                    BLEConnection.GattServerSendResponse(requestId, offset, val);
                    break;

                case "00002a46-0000-1000-8000-00805f9b34fb":
                case "00002a45-0000-1000-8000-00805f9b34fb":
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.GATTSERVER_WRITE_CHARACTERISTICS, characteristic.getUuid().toString());
                    bundle.putInt(Constants.GATTSERVER_READ_REQUESTID, requestId);
                    bundle.putInt(Constants.GATTSERVER_READ_OFFSET, offset);
                    mResultReceiver.send(Constants.DEVICE_WRITE_REQUEST, bundle);

                    break;

                case "00002a40-0000-1000-8000-00805f9b34fb":
                    Bundle bundle1 = new Bundle();
                    bundle1.putByteArray(Utils.PHONE_ALERT_VALUE, value);
                    bundle1.putString(Constants.GATTSERVER_READ_CHARACTERISTICS, characteristic.getUuid().toString());
                    bundle1.putInt(Constants.GATTSERVER_READ_REQUESTID, requestId);
                    bundle1.putInt(Constants.GATTSERVER_READ_OFFSET, offset);
                    mResultReceiver.send(Constants.CHARACTERISTICS_WRITE_KEY, bundle1);
                    break;

                default:
                    break;
            }
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattDescriptor descriptor) {
            Log.d(TAG, "onDescriptorReadRequest");
            super.onDescriptorReadRequest(device, requestId, offset, descriptor);
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device, int requestId, BluetoothGattDescriptor descriptor,
                                             boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
            super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded, offset, value);
            Log.d(TAG, "onDescriptorWriteRequest " + descriptor.getUuid().toString());

            boolean status;
            switch (descriptor.getUuid().toString()) {
                case GattAttributes.CLIENT_CHARACTERISTIC_CONFIG:
                    String characteristics = descriptor.getCharacteristic().getUuid().toString();
                    Log.d(TAG, "characteristics " + characteristics);
                    // characteristics = characteristics.toUpperCase();

                    switch (characteristics) {
                        case "00002a2b-0000-1000-8000-00805f9b34fb":
                            status = value[0] != 0;
                            Utils.setBooleanSharedPreference(mContext, Utils.CURRENT_TIME_NOTIFICATION_STATUS, status);

                            break;
                        case "00002a46-0000-1000-8000-00805f9b34fb":
                            status = value[0] != 0;
                            Utils.setBooleanSharedPreference(mContext, Utils.ANS_READ_NOTIFICATION_STATUS, status);
                            break;
                        case "00002a45-0000-1000-8000-00805f9b34fb":
                            status = value[0] != 0;
                            Utils.setBooleanSharedPreference(mContext, Utils.ANS_UNREAD_NOTIFICATION_STATUS, status);
                            break;
                        case "00002a41-0000-1000-8000-00805f9b34fb":// For phone alert
                            status = value[0] == 1;
                            Utils.setBooleanSharedPreference(mContext, Utils.PHONE_ALERT_STATUS, status);
                            break;
                        case "00002a3f-0000-1000-8000-00805f9b34fb":// For phone alert 00002902-0000-1000-8000-00805f9b34fb
                            status = value[0] == 1;
                            Utils.setBooleanSharedPreference(mContext, Utils.PHONE_ALERT_STATUS, status);
                            break;
                        default:
                            break;
                    }

                    // if(responseNeeded) {
                    mGattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value);
                    //}

                    mResultReceiver.send(Constants.DEVICE_NOTIFY_REQUEST, null);

                    break;
                default:
                    break;

            }

        }

        @Override
        public void onExecuteWrite(BluetoothDevice device, int requestId, boolean execute) {
            super.onExecuteWrite(device, requestId, execute);
            Log.d(TAG, "onExecuteWrite ");
        }

        /*
        @Override
        public void onNotificationSent(BluetoothDevice device, int status) {
            super.onNotificationSent(device, status);
        }*/
    };
    private static BluetoothManager mBluetoothManager;
    /**
     * Broadcast receiver for detecting the device bluetooth sttus change and pairing process
     */
    private final BroadcastReceiver bluetoothStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            //Received when the bond state is changed
            Log.e("bluetoothStatusReceiver", "" + mResultReceiver);
            Log.e("Context in BT Brdcast", "" + mContext);

            if (mResultReceiver != null) {

                /**
                 * Checking if the application running sent to background or foreground.
                 */

                if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                    if (!isApplicationBackgroundCompat(mContext.getApplicationContext())) {
                        bluetoothStatusChecking(action, intent);
                    }
                } else {
                    if (!isApplicationBackground(mContext.getApplicationContext())) {
                        Log.e("inside ", "isApplicationBackground");
                        bluetoothStatusChecking(action, intent);
                    } else {
                        Log.e("else ", "isApplicationBackground");

                    }
                }


            }
        }
    };
    /**
     * indicates how to behave if the service is killed
     */
    int mStartMode;
    /**
     * interface for clients that bind
     */
    IBinder mBinder;
    /**
     * indicates whether onRebind should be used
     */
    boolean mAllowRebind;

    public static int getmConnectionState() {
        return mConnectionState;
    }

    public static int getmServerConnectionState() {
        return mGattServerConnectionState;
    }

    /**
     * Connects to the GATT server hosted on the BlueTooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The
     * connection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public static void connect(final String address, final String devicename, Context context) {
        canWrite = true;
        noCallback = true;
        mContext = context;
        if (mBluetoothAdapter == null || address == null) {
            return;
        }
        mDevice = mBluetoothAdapter
                .getRemoteDevice(address);
        /*if (mDevice == null) {
            return;
        }*/
        // We want to directly connect to the device, so we are setting the
        // autoConnect
        // parameter to false.
        if (mBluetoothGatt != null)
            mBluetoothGatt.close();
        mBluetoothGatt = mDevice.connectGatt(mContext, false, mGattCallback);
        refreshDeviceCache(mBluetoothGatt);
        mBluetoothDeviceAddress = address;

        mConnectionState = STATE_CONNECTING;
        mReconnectionEnabled = true;
    }

    public static void connectGattServer(final String address, final String devicename, Context context) {

        mContext = context;
        if (mBluetoothAdapter == null || address == null) {
            return;
        }
        mDevice = mBluetoothAdapter
                .getRemoteDevice(address);
        /*if (mDevice == null) {
            return;
        }*/
        if (mGattServer != null) {

            mGattServer.connect(mDevice, false);
        }
        mReconnectionEnabled = true;
    }

    /**
     * Connects to the GATT server hosted on the BlueTooth LE device.
     *
     * @return Return true if the connection is initiated successfully. The
     * connection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public static void autoConnect(Context context) {

        needServiceDiscover = true;
        canWrite = true;
        if (mDevice == null) {
            return;
        }
        // We want to directly connect to the device, so we are setting the
        // autoConnect
        // parameter to false.
        mDevice = mBluetoothAdapter
                .getRemoteDevice(mBluetoothDeviceAddress);

        mContext = context;
        if (mBluetoothGatt != null)
            mBluetoothGatt.close();

        mBluetoothGatt = mDevice.connectGatt(context, true, mGattCallback);
        //  refreshDeviceCache(mBluetoothGatt);
        mConnectionState = STATE_CONNECTING;
    }

    public static void connectSPP(Context context) {
        mContext = context;

        if (mDevice == null) {
            return;
        }
        // We want to directly connect to the device, so we are setting the
        // autoConnect
        // parameter to false.
        if (mBluetoothGatt != null) mBluetoothGatt.close();
        mBluetoothGatt = mDevice.connectGatt(mContext, true, mGattCallback);
        refreshDeviceCache(mBluetoothGatt);
        mConnectionState = STATE_CONNECTING;
    }

    public static void gattServerAutoConnect(Context context) {
        canWrite = true;
        try {
            mContext = context;
            if (mBluetoothAdapter == null) {
                return;
            }
            if (mDevice == null) {
                return;
            }
            if (mGattServer != null) {

                mGattServer.connect(mDevice, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean refreshDeviceCache(BluetoothGatt gatt) {
        try {
            Method localMethod = gatt.getClass().getMethod("refresh");
            if (localMethod != null) {
                return (Boolean) localMethod.invoke(gatt);
            }
        } catch (Exception localException) {
            Log.v(TAG, "Exeception occured while clearing cache");
        }
        return false;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The
     * disconnection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public static void disconnect() {
        try {
            if (mBluetoothAdapter == null || mBluetoothGatt == null) {
                return;
            }
            mDevice = null;
            mBluetoothGatt.disconnect();
            // mBluetoothGatt.close();
            //  mBluetoothGatt = null;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    try {
                        mBluetoothGatt.close();
                        mBluetoothGatt = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // mBluetoothGatt = null;
    }

    public static void disconnectGattServer() {
        try {
            if (mGattServer != null && mDevice != null) {
                mGattServer.clearServices();
                mGattServer.cancelConnection(mDevice);
                mGattServer.close();
               /* Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        try {
                            mGattServer.close();
                           // mGattServer = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 500);*/

                Log.e("disconnectGattServer", "disconnectGattServer");
            }
            //  mGattServer = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void discoverServices() {
        if (mBluetoothAdapter != null && mBluetoothGatt != null) {
            mBluetoothGatt.discoverServices();
        }
    }

    public static List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null)
            return null;

        return mBluetoothGatt.getServices();
    }

    public static void setCharacteristicIndication(final BluetoothGattCharacteristic characteristic,
                                                   final Boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            return;
        }
        final Handler handler = new Handler(Looper.getMainLooper());
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    // while (!canWrite) ;
                    long time = 0;
                    while (!canWrite && time < 3500) {
                        time = time + 100;
                        Thread.currentThread().sleep(100);
                    }
                    canWrite = false;
                    noCallback = true;
//                    handlerDelayed.postDelayed(IndicationCharaRunnable, 1500);
                    if (characteristic.getDescriptor(UUID
                            .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG)) != null) {
                        if (enabled) {
                            BluetoothGattDescriptor descriptor = characteristic
                                    .getDescriptor(UUID
                                            .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                            descriptor
                                    .setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);

                            mBluetoothGatt.writeDescriptor(descriptor);
                        } else {
                            BluetoothGattDescriptor descriptor = characteristic
                                    .getDescriptor(UUID
                                            .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                            descriptor
                                    .setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                            mBluetoothGatt.writeDescriptor(descriptor);
                        }
                    }
                    mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled        If true, enable notification. False otherwise.
     */


    public static void setCharacteristicNotification(
            final BluetoothGattCharacteristic characteristic, final boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            return;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    //while (!canWrite) ;
                    long time = 0;
                    while (!canWrite && time < 3500) {
                        time = time + 100;
                        Thread.currentThread().sleep(100);
                    }
                    canWrite = false;
                    noCallback = true;
//                    handlerDelayed.postDelayed(notificationCharRunnable, 1500);
                    if (characteristic.getDescriptor(UUID
                            .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG)) != null) {
                        if (enabled) {
                            BluetoothGattDescriptor descriptor = characteristic
                                    .getDescriptor(UUID
                                            .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                            descriptor
                                    .setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            mBluetoothGatt.writeDescriptor(descriptor);
                        } else {
                            BluetoothGattDescriptor descriptor = characteristic
                                    .getDescriptor(UUID
                                            .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                            descriptor
                                    .setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                            mBluetoothGatt.writeDescriptor(descriptor);
                        }
                    }
                    mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public static void setCharacteristicWriteWithoutResponse(final BluetoothGattCharacteristic
                                                                     bluetoothGattCharacteristic,
                                                             final byte[] data) {
        Handler handler = new Handler(Looper.getMainLooper());
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    //while (!canWrite) ;
                    long time = 0;
                    while (!canWrite && time < 3500) {
                        time = time + 100;
                        Thread.currentThread().sleep(100);
                    }
                    canWrite = false;
                    noCallback = true;
//                    handlerDelayed.postDelayed(writeCharaRunnable, 1500);
                    if (mBluetoothAdapter != null && mBluetoothGatt != null) {
                        if (bluetoothGattCharacteristic.setValue(data)) {
                            boolean status = mBluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic);
                            Log.v(TAG, "Set Packet " + ByteArraytoHex(data));
                            Log.v(TAG, "Write Packet " + status);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public static boolean setCharacteristicOTAUWriteWithoutResponse(BluetoothGattCharacteristic
                                                                            bluetoothGattCharacteristic,
                                                                    byte[] data) {

        if (mBluetoothAdapter != null && mBluetoothGatt != null) {
            bluetoothGattCharacteristic.setValue(data);

            if (mBluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic)) {
                Log.v(TAG, "Write Packet " + ByteArraytoHex(data));
            } else {
                connect(mBluetoothDeviceAddress);
                Log.v(TAG, "Write Packet Again" + ByteArraytoHex(data));
                setCharacteristicOTAUWriteWithoutResponse(bluetoothGattCharacteristic, data);
            }

        }
        return true;
    }

    public static void readCharacteristic(
            final BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            return;
        }
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    //while (!canWrite) ;
                    long time = 0;
                    while (!canWrite && time < 3500) {
                        time = time + 100;
                        Thread.currentThread().sleep(100);
                    }
                    canWrite = false;
//                    handlerDelayed.postDelayed(readCharaRunnable, 1500);
                    Log.d("Trying to read", characteristic.getUuid().toString());
                    boolean status = mBluetoothGatt.readCharacteristic(characteristic);
                    Log.d("status", "" + status);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static String ByteArraytoHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();

    }

    public static void readRemoteRssi() {
        executor.submit(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    //while (!canWrite) ;
                                    long time = 0;
                                    while (!canWrite && time < 3500) {
                                        time = time + 100;
                                        Thread.currentThread().sleep(100);
                                    }
                                    canWrite = false;

                                    mBluetoothGatt.readRemoteRssi();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

        );
    }

    public static void changeMtu(int size) {
        try {
            if (Build.VERSION_CODES.KITKAT < Build.VERSION.SDK_INT) {
                mBluetoothGatt.requestMtu(size);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isBonded() {
        Boolean bonded = false;
        try {
            BluetoothDevice device = mBluetoothAdapter
                    .getRemoteDevice(mDevice.getAddress());
            bonded = device.getBondState() == BluetoothDevice.BOND_BONDED;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bonded;
    }

    public static boolean isBonding() {
        Boolean bonded = false;
        try {
            BluetoothDevice device = mBluetoothAdapter
                    .getRemoteDevice(mDevice.getAddress());
            bonded = device.getBondState() == BluetoothDevice.BOND_BONDING;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bonded;
    }

    public static int getmBondstate() {
        int bondstate = 0;
        try {
            if (mBluetoothAdapter != null) {
                BluetoothDevice device = mBluetoothAdapter
                        .getRemoteDevice(mDevice.getAddress());
                bondstate = device.getBondState();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bondstate;

    }

    public static void pairDevice() {
        try {
            Method m = mBluetoothGatt.getDevice()
                    .getClass().getMethod("createBond", (Class[]) null);
            m.invoke(mBluetoothGatt.getDevice(), (Object[]) null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void unPairDevice() {
        try {
            Method m = mBluetoothGatt.getDevice()
                    .getClass().getMethod("removeBond", (Class[]) null);
            m.invoke(mBluetoothGatt.getDevice(), (Object[]) null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openGattServer() {
        try {
            mGattServer = mBluetoothManager.openGattServer(mContext, mGattServerCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeGattServer() {
        try {
            if (mGattServer != null) {
                mGattServer.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void addService(final BluetoothGattService service) {

        Handler handler = new Handler(Looper.getMainLooper());
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    //while (!canWrite) ;
                    long time = 0;
                    while (!canWrite && time < 3500) {
                        time = time + 100;
                        Thread.currentThread().sleep(100);
                    }
                    canWrite = false;
//                    handlerDelayed.postDelayed(addServiceRunnable, 1500);
                    if (mGattServer != null) {
                        try {
                            mGattServer.addService(service);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static void clearServices() {

        try {
            if (mGattServer != null) {
                mGattServer.clearServices();
                mGattServer = null;
                Log.e("clearServices", "clearServices");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BluetoothDevice getDevice() {
        return mDevice;
    }

    public synchronized static void notifyConnectedDevices(final BluetoothGattCharacteristic characteristic) {
        Handler handler = new Handler(Looper.getMainLooper());
        Log.d(TAG, "notifyConnectedDevices");
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    //while (!canWrite) ;
                    long time = 0;
                    while (!canWrite && time < 3500) {
                        time = time + 100;
                        Thread.currentThread().sleep(100);
                    }
                    canWrite = false;
//                    handlerDelayed.postDelayed(notifyDeviceRunnable, 1500);
                    if (mGattServer != null) {
                        mGattServer.notifyCharacteristicChanged(mDevice, characteristic, false);
                    } else {
                        canWrite = true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public static synchronized boolean GattServerSendResponse(int requestId, int offset, byte[] value) {
        boolean status = false;
        try {
            status = mGattServer.sendResponse(mDevice,
                    requestId,
                    BluetoothGatt.GATT_SUCCESS,
                    offset,
                    value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public static boolean isGattServerConnected() {
        try {
            return mBluetoothManager.getConnectionState(mDevice, BluetoothProfile.GATT_SERVER) == BluetoothProfile.STATE_CONNECTED;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    public static boolean isGattConnected() {
        try {
            return mBluetoothManager.getConnectionState(mDevice, BluetoothProfile.GATT) == BluetoothProfile.STATE_CONNECTED;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    /**
     * Checks if the application is being sent in the background (i.e behind
     * another application's Activity).
     *
     * @param context the context
     * @return <code>true</code> if another application will be above this one.
     */

    public static boolean isApplicationBackgroundCompat(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isApplicationBackground(Context context) {
        String[] activePackages = getActivePackages(context);
        if (activePackages.length > 0) {
            for (String activePackage : activePackages) {
                if (activePackage.equals(context.getPackageName())) {
                    return false;
                }
            }
        }
        return true;
    }

    private static String[] getActivePackages(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final Set<String> activePackages = new HashSet<String>();
        final List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        if (processInfos != null) {
            for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    activePackages.addAll(Arrays.asList(processInfo.pkgList));
                }
            }
        }
        return activePackages.toArray(new String[activePackages.size()]);
    }

    /**
     * This function initiates OTA update
     */
    public static void startOTAUpdate() {
        OTAUManager otauManager = OTAUManager.getInstance();
        otauManager.newImageNotification();
    }

    public static void gattWriteWithoutResponse(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        if (mBluetoothAdapter != null && mBluetoothGatt != null) {
            mBluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic);
        }
    }

    public static void setOTAResponseListener(OTAResponseListener commandObj) {
        mOtaResponseListener = commandObj;
    }

    public static BluetoothAdapter getBluetoothAdapter() {
        return mBluetoothAdapter;
    }

    /**
     * Connects to the GATT server hosted on the BlueTooth LE device.
     *
     * @param address The device address of the destination device.
     *                connection result is reported asynchronously through the
     *                {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *                callback.
     */
    public static void connect(final String address) {
        //mContext = context;
        canWrite = true;

        if (mBluetoothAdapter == null || address == null) {
            return;
        }
        mDevice = mBluetoothAdapter.getRemoteDevice(address);
        /*if (mDevice == null) {
            return;
        }*/
        // We want to directly connect to the device, so we are setting the
        // autoConnect
        // parameter to false.
        if (mBluetoothGatt != null)
            mBluetoothGatt.close();
        mBluetoothGatt = mDevice.connectGatt(mContext, false, mGattCallback);
        refreshDeviceCache(mBluetoothGatt);
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        mReconnectionEnabled = true;
    }

    /*

    private static  void notifyConnectedDevices() {

        ByteBuffer b = ByteBuffer.allocate(4);
//b.order(ByteOrder.BIG_ENDIAN); // optional, the initial order of a byte buffer is always BIG_ENDIAN.
        b.putInt(0xAABBCCDD);

        byte[] result = b.array();

            BluetoothGattCharacteristic readCharacteristic = mGattServer.getService(UUIDDatabase.UUID_CURRENT_TIME_SERVICE)
                    .getCharacteristic(UUIDDatabase.UUID_CURRENT_TIME);
            readCharacteristic.setValue(result);
            mGattServer.notifyCharacteristicChanged(mDevice, readCharacteristic, false);
        }


    public static void startAdvertising() {
        if (mBluetoothLeAdvertiser == null) return;

        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .build();

        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .addServiceUuid(new ParcelUuid(UUIDDatabase.UUID_CURRENT_TIME_SERVICE))
                .build();

        mBluetoothLeAdvertiser.startAdvertising(settings, data, mAdvertiseCallback);
    }



    private static AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            Log.i(TAG, "Peripheral Advertise Started.");
            //postStatusMessage("GATT Server Ready");
        }

        @Override
        public void onStartFailure(int errorCode) {
            Log.w(TAG, "Peripheral Advertise Failed: "+errorCode);
           // postStatusMessage("GATT Server Error "+errorCode);
        }
    };
*/

    public static void setBLEDataReceiver(ResultReceiver receiver) {
        mResultReceiver = receiver;
    }

    /**
     * @return Returns <b>true</b> if property is writable
     */
    public static boolean isCharacteristicWriteable(BluetoothGattCharacteristic pChar) {
        return (pChar.getProperties() & (BluetoothGattCharacteristic.PROPERTY_WRITE | BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) != 0;
    }

    /**
     * @return Returns <b>true</b> if property is Readable
     */
    public static boolean isCharacteristicReadable(BluetoothGattCharacteristic pChar) {
        return ((pChar.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) != 0);
    }

    /**
     * @return Returns <b>true</b> if property is supports notification
     */
    public static boolean isCharacteristicNotifiable(BluetoothGattCharacteristic pChar) {
        return (pChar.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0;
    }

    /**
     * @return Returns <b>true</b> if property is supports notification
     */
    public static boolean isCharacteristicIndicatable(BluetoothGattCharacteristic pChar) {
        return (pChar.getProperties() & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0;
    }

    /**
     * Request a read on a given {@code BluetoothGattDescriptor }.
     *
     * @param descriptor The descriptor to read from.
     */
    public static void readDescriptor(
            BluetoothGattDescriptor descriptor) {
        String serviceUUID = descriptor.getCharacteristic().getService().getUuid().toString();
        String serviceName = GattAttributes.lookup(descriptor.getCharacteristic().getService().getUuid().toString(), serviceUUID);

        String characteristicUUID = descriptor.getCharacteristic().getUuid().toString();
        String characteristicName = GattAttributes.lookup(descriptor.getCharacteristic().getUuid().toString(), characteristicUUID);
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            return;
        }
        //Logger.datalog(mContext.getResources().getString(R.string.dl_descriptor_read_request));
        mBluetoothGatt.readDescriptor(descriptor);
        Log.i("Read Descriptor> ", "Chara> " + characteristicName + " Service> " + serviceName);
    }

    private void bluetoothStatusChecking(String action, Intent intent) {
        if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
            Log.v(TAG, "state " + state);
            if (state == BluetoothDevice.BOND_BONDING) {
                mBondstate = BluetoothDevice.BOND_BONDING;
                // Bonding...
                Log.v(TAG, "Bonding");
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_PAIRING_STATUS_IN_PROCESS_KEY,
                        true);
                mResultReceiver.send(Constants.DEVICE_PAIR_IN_PROCESS_KEY, bundle);

            } else if (state == BluetoothDevice.BOND_BONDED) {
                mBondstate = BluetoothDevice.BOND_BONDED;
                // Bonded...
                Log.v(TAG, "Bonded");
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_PAIRING_STATUS_DONE_KEY,
                        true);
                mResultReceiver.send(Constants.DEVICE_PAIR_DONE_KEY, bundle);

            } else if (state == BluetoothDevice.BOND_NONE) {
                mBondstate = BluetoothDevice.BOND_NONE;
                // Not bonded...
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_PAIRING_STATUS_FAILED_KEY,
                        true);
                mResultReceiver.send(Constants.DEVICE_PAIR_FAILED_KEY, bundle);
            }
        }
        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) ==
                    BluetoothAdapter.STATE_OFF) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_BLUETOOTH_STATUS_OFF_KEY,
                        true);
                bundle.putBoolean(Constants.DEVICE_BLUETOOTH_KEY,
                        false);
                mResultReceiver.send(Constants.DEVICE_BLUETOOTH_OFF_KEY, bundle);
            }
            if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) ==
                    BluetoothAdapter.STATE_ON) {

                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_BLUETOOTH_STATUS_ON_KEY,
                        true);
                bundle.putBoolean(Constants.DEVICE_BLUETOOTH_KEY,
                        true);
                mResultReceiver.send(Constants.DEVICE_BLUETOOTH_ON_KEY, bundle);
            }

        }
        if (action.equals(BluetoothDevice.ACTION_PAIRING_REQUEST)) {
            Log.e("ACTION_PAIRING_REQUEST", "DEVICE_PAIR_REQUEST");
            mResultReceiver.send(Constants.DEVICE_PAIR_REQUEST, null);
        }

    }

    /**
     * Called when the service is being created.
     */
    @Override
    public void onCreate() {
        mContext = this;

        // Initializing the service
        if (!initialize()) {
            Log.v(TAG, "Service not initialized");
        } else {
            Log.v(TAG, "Service  initialized");
        }

        /**
         * Broadcast receiver register
         */
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);
        registerReceiver(bluetoothStatusReceiver, filter);
    }

    /**
     * The service is starting, due to a call to startService()
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            mResultReceiver = intent.getParcelableExtra(Constants.RESULT_RECEIVER_KEY);
        } catch (Exception ignored) {

        }
        return mStartMode;
    }

    /**
     * A client is binding to the service with bindService()
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Called when all clients have unbound with unbindService()
     */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /**
     * Called when a client is binding to the service with bindService()
     */
    @Override
    public void onRebind(Intent intent) {

    }

    /**
     * Called when The service is no longer used and is being destroyed
     */
    @Override
    public void onDestroy() {
        unregisterReceiver(bluetoothStatusReceiver);
        Log.v(TAG, "Service stopped");
    }

    /**
     * Initializes a reference to the local BlueTooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter
        // through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                return false;
            }
        }
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        //openGattServer();
        return mBluetoothAdapter != null;

    }

    public interface OTAResponseListener {
        void handleCommandResponse(BluetoothGattCharacteristic characteristic);
    }

}
