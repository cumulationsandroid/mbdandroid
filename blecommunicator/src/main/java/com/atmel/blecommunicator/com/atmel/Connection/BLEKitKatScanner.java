/*
 *
 *
 *     BLEKitKatScanner.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Connection;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Interfaces.BLEDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class BLEKitKatScanner {


    private static BLEKitKatScanner mBleScanner;
    private static BluetoothAdapter mBluetoothAdapter;
    private static Context mContext;
    private static BLEDetector mBleDetector;
    private BluetoothAdapter.LeScanCallback mLeScanCallback;
    private Handler stopScanHandler;
    private Runnable stopScanRunnable;

    {
        mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] scanRecord) {


                if (mBleDetector != null) {
                    List<AdRecord> records = AdRecord.parseScanRecord(scanRecord);
                    List<ParcelUuid> list = new ArrayList<>();
                    if (records != null && records.size() > 0) {
                        for (AdRecord packet : records) {
                            try {
                                //Log.e("packet.getType()", " " + packet.getType());
                                if (packet.getType() == 3) {
                                    int uuid = AdRecord.getServiceDataUuid(packet);
                                    String hex = "0000" + Integer.toHexString(uuid);
                                    String uuidName = hex + "-0000-1000-8000-00805f9b34fb";
                                    ParcelUuid id = ParcelUuid.fromString(uuidName);
                                    list.add(id);
                                } else if (packet.getType() == 7) {
                                    int uuid = AdRecord.getServiceDataUuidSPP(packet);

                                    String hex = Integer.toHexString(uuid);

                                    String uuidName = "fd5abba0-3935-11e5-85a6-0002" + hex;
                                    Log.e("uuidName", " " + uuidName);
                                    ParcelUuid id = ParcelUuid.fromString(uuidName);
                                    list.add(id);
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    mBleDetector.leDeviceDetected(bluetoothDevice, rssi, list);
                }
            }
        };
    }

    /**
     * SingletonClass
     * Create private constructor
     */
    private BLEKitKatScanner() {
        stopScanHandler = new Handler();
        stopScanRunnable = new Runnable() {
            @Override
            public void run() {
                stopLEScan();
            }
        };
    }

    public static String ByteArrayToString(byte[] ba) {
        StringBuilder hex = new StringBuilder(ba.length * 2);
        for (byte b : ba)
            hex.append(b).append(" ");

        return hex.toString();
    }

    /**
     * Single instance creation
     */
    public static BLEKitKatScanner getInstance(Context context) {
        if (mBleScanner == null) {
            mBleScanner = new BLEKitKatScanner();
            mContext = context;
            BluetoothManager manager = (BluetoothManager) mContext.getSystemService
                    (Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = manager.getAdapter();
        }
        return mBleScanner;
    }

    public static void setScanListner(BLEDetector bleDetector) {
        mBleDetector = bleDetector;
    }

   /* public void printScanRecord(byte[] scanRecord) {

        // Simply print all raw bytes
        try {
            String decodedRecord = new String(scanRecord, "UTF-8");
            Log.e("DEBUG", "decoded String : " + ByteArrayToString(scanRecord));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Parse data bytes into individual records
        List<AdRecord> records = AdRecord.parseScanRecord(scanRecord);


        // Print individual records
        if (records.size() == 0) {
            Log.e("DEBUG", "Scan Record Empty");
        } else {
            String dd = TextUtils.join(",", records);
            Log.e("DEBUG", "Scan Record: " + dd);
        }

    }
*/

    /**
     * We need to enforce that Bluetooth is first enabled, and take the
     * user to settings to enable it if they have not done so.
     */

    public boolean getBluetoothStatus() {

        return !(mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled());
    }

    /**
     * Check for Bluetooth LE SuppuuidArrayort.  In production, our manifest entry will keep this
     * from installing on these devices, but this will allow test devices or other
     * side loads to report whether or not the feature exists.
     *
     * @return boolean
     */
    public boolean checkBLESupport() {
        return mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    public void startLEScan(long duration) {
        if (getBluetoothStatus()) {
            stopScanHandler.removeCallbacks(stopScanRunnable);
            stopScanHandler.postDelayed(stopScanRunnable, duration);
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            Log.v("BLE SCAN", "Before LOLLIPOP");
        }
    }

    public void startLEScanWithFilter(long duration, String[] serviceUUIDs) {
        UUID[] uuidArray = new UUID[serviceUUIDs.length];
        for (int i = 0; i < serviceUUIDs.length; i++) {
            uuidArray[i] = UUID.fromString(serviceUUIDs[i]);
        }
        if (getBluetoothStatus()) {
            stopScanHandler.removeCallbacks(stopScanRunnable);
            stopScanHandler.postDelayed(stopScanRunnable, duration);
            mBluetoothAdapter.startLeScan(uuidArray, mLeScanCallback);
            Log.v("BLE SCAN with filters", "Before LOLLIPOP");
        }
    }

    public void stopLEScan() {
        if (getBluetoothStatus()) {
            if (mBleDetector != null)
                mBleDetector.leScanStopped();
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

}
