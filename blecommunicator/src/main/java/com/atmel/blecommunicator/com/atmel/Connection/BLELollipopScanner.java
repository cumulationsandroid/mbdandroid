/*
 *
 *
 *     BLELollipopScanner.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Connection;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Interfaces.BLEDetector;

import java.util.ArrayList;
import java.util.List;

/**
 * Class created for scanning BLE Devices
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class BLELollipopScanner {

    private static BLELollipopScanner mBleScanner;
    private static BluetoothAdapter mBluetoothAdapter;
    private static Context mContext;
    private static BluetoothLeScanner mBluetoothLeScanner;
    private static BLEDetector mBleDetector;
    private Handler stopScanHandler;
    private Runnable stopScanRunnable;
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            ScanRecord record = null;
            List<ParcelUuid> list = null;
            super.onScanResult(callbackType, result);
            if (result != null) {
                record = result.getScanRecord();
            }
            if (record != null) {
                list = record.getServiceUuids();
            }
            if (mBleDetector != null && result != null) {
                mBleDetector.leDeviceDetected(result.getDevice(), result.getRssi(), list);
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };

    /**
     * SingletonClass
     * Create private constructor
     */
    private BLELollipopScanner() {
        stopScanHandler=new Handler();
        stopScanRunnable=new Runnable() {
            @Override
            public void run() {
                stopLEScan();
            }
        };

    }

    /**
     * Single instance creation
     */
    public static BLELollipopScanner getInstance(Context context) {
        if (mBleScanner == null) {
            mBleScanner = new BLELollipopScanner();
            mContext = context;
            BluetoothManager manager = (BluetoothManager) mContext.getSystemService
                    (Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = manager.getAdapter();
        }
        return mBleScanner;
    }

    public static void setScanListner(BLEDetector bleDetector) {
        mBleDetector = bleDetector;
    }

    /**
     * We need to enforce that Bluetooth is first enabled, and take the
     * user to settings to enable it if they have not done so.
     */

    public boolean getBluetoothStatus() {

        return !(mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled());
    }

    /**
     * Check for Bluetooth LE Support.  In production, our manifest entry will keep this
     * from installing on these devices, but this will allow test devices or other
     * side loads to report whether or not the feature exists.
     *
     * @return boolean
     */
    public boolean checkBLESupport() {
        return mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    public void startLEScan(long duration) {
        int currentapiVersion = Build.VERSION.SDK_INT;
        stopScanHandler.removeCallbacks(stopScanRunnable);
        stopScanHandler.postDelayed(stopScanRunnable, duration);
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            // Do something for lollipop and above versions
            ScanSettings settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
            if (getBluetoothStatus()) {
                mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
                //mBluetoothLeScanner.startScan(mScanCallback);
                mBluetoothLeScanner.startScan(null, settings, mScanCallback);
                Log.v("BLE SCAN", "LOLLIPOP");
            }
        }
    }

    public void startLEScanWithFilters(long duration, String[] serviceUUIDs) {
        int currentapiVersion = Build.VERSION.SDK_INT;
        stopScanHandler.removeCallbacks(stopScanRunnable);
        stopScanHandler.postDelayed(stopScanRunnable, duration);
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            // Do something for lollipop and above versions
            ScanSettings settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
            if (getBluetoothStatus()) {
                mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
                List<ScanFilter> mList = scanFilters(serviceUUIDs);
                mBluetoothLeScanner.startScan(mList, settings, mScanCallback);
                Log.v("BLE SCAN", "LOLLIPOP");
            }
        }

    }
    private List<ScanFilter> scanFilters(String[] serviceUUIDs) {
        List<ScanFilter> list = new ArrayList<>();
        //   ScanFilter filter = new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(GattAttributes.LINK_LOSS_SERVICE)).build();
//        ScanFilter filter2 = new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(GattAttributes.HEALTH_TEMP_SERVICE)).build();
//list.add(filter);
//list.add(filter2);
        for (int i = 0; i < serviceUUIDs.length; i++) {
            ScanFilter filter = new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(serviceUUIDs[i])).build();

            list.add(filter);
        }

        return list;
    }


    public void stopLEScan() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            // Do something for lollipop and above versions
            if (getBluetoothStatus()) {
                mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
                mBluetoothLeScanner.stopScan(mScanCallback);
                if (mBleDetector != null)
                    mBleDetector.leScanStopped();
            }
        }
    }

}

