/*
 *
 *
 *     AlertNotificationService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

import java.nio.charset.Charset;
import java.util.UUID;

public class AlertNotificationService {
    private static AlertNotificationService mTimeService;
    private String mUtc;
    private int mUtcValue = 0;


    /**
     * SingletonClass
     * Create private constructor
     */
    private AlertNotificationService() {
    }

    /**
     * Single instance creation
     */
    public static AlertNotificationService getInstance() {
        if (mTimeService == null) {

            mTimeService = new AlertNotificationService();
        }
        return mTimeService;
    }

    public BluetoothGattService initialize() {
        BluetoothGattService mHtpService;
        mHtpService = new BluetoothGattService(UUIDDatabase.UUID_ALERT_NOTIFICATION_SERVICE,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        //bleApplication.setBluetoothGattAdvService(service);
        try {
            BluetoothGattDescriptor readDescriptor = new BluetoothGattDescriptor(UUIDDatabase.UUID_CLIENT_CHARACTERISTIC_CONFIG,
                    BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);
            BluetoothGattDescriptor unreadDescriptor = new BluetoothGattDescriptor(UUIDDatabase.UUID_CLIENT_CHARACTERISTIC_CONFIG,
                    BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);
            BluetoothGattCharacteristic newAlertCategoryCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_ANS_SUPPORTED_NEW_ALERT_CATEGORY,
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_READ,
                            BluetoothGattCharacteristic.PERMISSION_READ);
            mHtpService.addCharacteristic(newAlertCategoryCharacteristic);
            BluetoothGattCharacteristic newAlertCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_ANS_NEW_ALERT,
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                            BluetoothGattCharacteristic.PERMISSION_READ);
            newAlertCharacteristic.addDescriptor(readDescriptor);
            mHtpService.addCharacteristic(newAlertCharacteristic);
            BluetoothGattCharacteristic unreadAlertCategoryCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_ANS_SUPPORTED_UNREAD_ALERT_CATEGORY,
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_READ,
                            BluetoothGattCharacteristic.PERMISSION_READ);
            mHtpService.addCharacteristic(unreadAlertCategoryCharacteristic);
            BluetoothGattCharacteristic unreadAlertStatusCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_ANS_UNREAD_ALERT_STATUS,
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                            BluetoothGattCharacteristic.PERMISSION_READ);
            unreadAlertStatusCharacteristic.addDescriptor(unreadDescriptor);
            mHtpService.addCharacteristic(unreadAlertStatusCharacteristic);
            BluetoothGattCharacteristic controlPointCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_ANS_CONTROL_POINT,
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_READ,
                            BluetoothGattCharacteristic.PERMISSION_READ);
            mHtpService.addCharacteristic(controlPointCharacteristic);
            // service.addCharacteristic(offsetCharacteristic);
            BLEConnection.addService(mHtpService);
        } catch (Exception ignored) {

        }
        //mHtpService = service;
        return mHtpService;
    }


    public void newAlertNotifyConnectedDevices(UUID characteristics, int category, int count, String text) {

        BluetoothGattCharacteristic readCharacteristic = null;

        if (characteristics.toString().equalsIgnoreCase(GattAttributes.ANS_NEW_ALERT)) {

            readCharacteristic = BLEConnection.mGattServer.getService(UUIDDatabase.UUID_ALERT_NOTIFICATION_SERVICE)
                    .getCharacteristic(characteristics);

            //String string = "unknown";

            byte[] newAlert = new byte[20];

            if (category == 0) {
                newAlert[0] = (byte) (5);
            } else {
                newAlert[0] = (byte) (4);
            }


            newAlert[1] = (byte) (count);
            if (text != null) {
                byte[] b = text.getBytes(Charset.forName("UTF-8"));

                for (int i = 2, j = 0; i < b.length; i++, j++) {
                    if (i < 20) {
                        newAlert[i] = b[j];
                    }
                }
            } else {
                for (int i = 2, j = 0; i < 20; i++, j++) {
                    newAlert[i] = 0;
                }
            }

            readCharacteristic.setValue(newAlert);

        }
        BLEConnection.notifyConnectedDevices(readCharacteristic);
    }

    public void unreadAlertNotifyConnectedDevices(UUID characteristics, int category, int count) {

        BluetoothGattCharacteristic readCharacteristic = null;

        if (characteristics.toString().equalsIgnoreCase(GattAttributes.ANS_UNREAD_ALERT_STATUS)) {

            readCharacteristic = BLEConnection.mGattServer.getService(UUIDDatabase.UUID_ALERT_NOTIFICATION_SERVICE)
                    .getCharacteristic(characteristics);

            byte[] newAlert = new byte[2];
            if (category == 0) {

                newAlert[0] = (byte) (5);
                newAlert[1] = (byte) (count);

            } else {

                newAlert[0] = (byte) (4);
                newAlert[1] = (byte) (count);
            }
            readCharacteristic.setValue(newAlert);

        }
        BLEConnection.notifyConnectedDevices(readCharacteristic);
    }


    public void writeLocalTime(int requestid, int offset, byte[] value) {

        BLEConnection.GattServerSendResponse(requestid, offset, value);
    }


}
