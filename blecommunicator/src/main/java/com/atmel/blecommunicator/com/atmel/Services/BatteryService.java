/*
 *
 *
 *     BatteryService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.BatteryLevelCharacteristics;
import com.atmel.blecommunicator.com.atmel.parsers.BatteryLevelParser;

/**
 * Created by
 */
public class BatteryService {

    private static BatteryService mBatteryService;
    private int batteryLevel = 0;
    private BatteryLevelCharacteristics mBatteryLevelCharacteristics;
    private BluetoothGattService mService;


    /**
     * Disable making Constructor
     */
    private BatteryService() {
    }

    /**
     * Making singleton
     *
     * @return
     */
    public static BatteryService getInstance() {
        return mBatteryService == null ? mBatteryService = new BatteryService() : mBatteryService;
    }

    /**
     * Setting {@link BluetoothGattService}
     *
     * @param mBatteryLevelService
     */
    public void setmBatteryLevelService(BluetoothGattService mBatteryLevelService) {
        mService = mBatteryLevelService;
    }

    /**
     * Setting all instance
     *
     * @return
     */
    public BluetoothGattCharacteristic getBatteryLevelCharacteristics() {
        BluetoothGattCharacteristic mCharacteristic;
        mBatteryLevelCharacteristics = BatteryLevelCharacteristics.getInstance();
        mCharacteristic = mService.getCharacteristic(UUIDDatabase.UUID_BATTERY_LEVEL);
        mBatteryLevelCharacteristics.setBatteryLevelCharacteristics(mCharacteristic);
//        mBatteryLevelCharacteristics.read();
        return mCharacteristic;
    }

    /**
     * Parse the characteristic values in {@link BluetoothGattCharacteristic#FORMAT_UINT8} format
     *
     * @param characteristic
     */
    public void parseValue(BluetoothGattCharacteristic characteristic) {
        BatteryLevelParser.prase(characteristic);
        batteryLevel = BatteryLevelParser.getBatteryLevel();
    }

    /**
     * Get the battery level in <code>%</code>
     *
     * @return the {@link Integer}
     */
    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setNotify(boolean isEnabled) {
        mBatteryLevelCharacteristics.notifyBattery(isEnabled);
    }
}
