/*
 *
 *
 *     BloodPressureService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.BloodPressureFeatureCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.BloodPressureMeasurementCharacteristic;

public class BloodPressureService {

    private BloodPressureMeasurementCharacteristic mBPMeasurementChar;
    private BloodPressureMeasurementCharacteristic mBPIntermediateChar;
    private BloodPressureFeatureCharacteristic mBPFeatureChar;
    private BluetoothGattService mService;

    public BloodPressureService(BluetoothGattService service) {
        mService = service;
    }

    public BluetoothGattCharacteristic getBloodPressureMeasurementCharacteristic() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mService.getCharacteristic(UUIDDatabase.UUID_BLOOD_PRESSURE_MEASUREMENT);
        mBPMeasurementChar = BloodPressureMeasurementCharacteristic.getInstance();
        mBPMeasurementChar.setBloodPressureMeasurementCharacteristic(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public void startIndicateBloodPressureMeasurementCharacteristic() {
        if (mBPMeasurementChar != null) {
            mBPMeasurementChar.indicate(true);
        }
    }

    public void stopIndicateBloodPressureMeasurementCharacteristic() {
        if (mBPMeasurementChar != null) {
            mBPMeasurementChar.indicate(false);
        }
    }

    public BluetoothGattCharacteristic getBloodPressureFeatureCharacteristic() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mService.getCharacteristic(UUIDDatabase.UUID_BLOOD_PRESSURE_FEATURE);
        mBPFeatureChar = BloodPressureFeatureCharacteristic.getInstance();
        mBPFeatureChar.setBloodPressureFeatureCharacteristic(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public void readBloodPressureFeatureCharacteristic() {
        if (mBPFeatureChar != null) {
            Log.d("Read in ", "Characteristic");
            mBPFeatureChar.read();
        }
    }

    public BluetoothGattCharacteristic getIntermediateCuffPressureCharacteristic() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mService.getCharacteristic(UUIDDatabase.UUID_BLOOD_INTERMEDIATE_CUFF_PRESSURE);
        mBPIntermediateChar = BloodPressureMeasurementCharacteristic.getInstance();
        mBPIntermediateChar.setBloodPressureMeasurementCharacteristic(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public void startNotifyIntermediateCuffPressureCharacteristic() {
        if (mBPIntermediateChar != null) {
            mBPIntermediateChar.notify(true);
        }
    }

    public void stopNotifyIntermediateCuffPressureCharacteristic() {
        if (mBPIntermediateChar != null) {
            mBPIntermediateChar.notify(false);
        }
    }
}
