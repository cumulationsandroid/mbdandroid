/*
 *
 *
 *     DeviceInformationService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

import java.util.List;

public class DeviceInformationService {
    public static BluetoothGattService mService;
    public static DeviceInformationService mDeviceInformationService;
    public BluetoothGattCharacteristic mCharacteristic;
    public String mManufacturerName;
    public String mModelNumber;
    public String mSerialNumber;
    public String mHardwareRevision;
    public String mFirmwareRevision;
    public String mSoftwareRevision;
    public String mSysIdManufacturerIdentifier;
    public String mRegulatoryDataList;
    public String mPnPIdProductId;


    private DeviceInformationService() {
    }

    /**
     * Single instance creation
     */
    public static DeviceInformationService getInstance() {

        mDeviceInformationService = new DeviceInformationService();

        return mDeviceInformationService;
    }


    /**
     * SingletonClass
     * Create private constructor
     */
    public void setGattService(BluetoothGattService service) {
        mService = service;
    }

    public List<BluetoothGattCharacteristic> getData() {


        return mService
                .getCharacteristics();
    }

    public void readData(BluetoothGattCharacteristic gattCharacteristic) {
        final int charaProp = gattCharacteristic.getProperties();
        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
            BLEConnection.readCharacteristic(gattCharacteristic);

        }
    }

    /**
     * Returns the manufacture name from the given characteristic
     *
     * @param characteristic
     * @return manfacture_name_string
     */
    public String getManufacturerNameString(
            BluetoothGattCharacteristic characteristic) {
        try {
            mManufacturerName = characteristic.getStringValue(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mManufacturerName;
    }

    /**
     * Returns the model number from the given characteristic
     *
     * @param characteristic
     * @return model_name_string
     */

    public String getModelNumberString(
            BluetoothGattCharacteristic characteristic) {
        try {
            mModelNumber = characteristic.getStringValue(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mModelNumber;
    }

    /**
     * Returns the serial number from the given characteristic
     *
     * @param characteristic
     * @return serial_number_string
     */
    public String getSerialNumberString(
            BluetoothGattCharacteristic characteristic) {
        try {
            mSerialNumber = characteristic.getStringValue(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mSerialNumber;
    }

    /**
     * Returns the hardware number from the given characteristic
     *
     * @param characteristic
     * @return hardware_revision_name_string
     */
    public String getHardwareRevisionString(
            BluetoothGattCharacteristic characteristic) {
        try {
            mHardwareRevision = characteristic.getStringValue(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mHardwareRevision;
    }


    /**
     * Returns the Firmware number from the given characteristic
     *
     * @param characteristic
     * @return hardware_revision_name_string
     */
    public String getFirmwareRevisionString(
            BluetoothGattCharacteristic characteristic) {
        try {
            mFirmwareRevision = characteristic.getStringValue(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mFirmwareRevision;
    }

    /**
     * Returns the software revision number from the given characteristic
     *
     * @param characteristic
     * @return hardware_revision_name_string
     */
    public String getSoftwareRevisionString(
            BluetoothGattCharacteristic characteristic) {
        try {
            mSoftwareRevision = characteristic.getStringValue(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mSoftwareRevision;
    }

    /**
     * Returns the PNP ID from the given characteristic
     *
     * @param characteristic
     * @return {@link String}
     */
    public String getPNPID(BluetoothGattCharacteristic characteristic) {
        if (characteristic != null) {
            final byte[] data = characteristic.getValue();
            final StringBuilder stringBuilder = new StringBuilder(data!=null? data.length:0);
            assert data != null;
            if ( data.length > 0) {
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
            }

            mPnPIdProductId = String.valueOf(stringBuilder);
        }
        return mPnPIdProductId;
    }

    /**
     * Returns the SystemID from the given characteristic
     *
     * @param characteristic
     * @return {@link String}
     */
    public String getSYSID(BluetoothGattCharacteristic characteristic) {
        if (characteristic != null) {
            final byte[] data = characteristic.getValue();
            final StringBuilder stringBuilder = new StringBuilder( data!=null? data.length:0);
            if ((data != null ? data.length : 0) > 0) {
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
            }

            mSysIdManufacturerIdentifier = String.valueOf(stringBuilder);
        }
        return mSysIdManufacturerIdentifier;
    }


    /**
     * Returns the SystemID from the given characteristic
     *
     * @param characteristic
     * @return {@link String}
     */
    public String getRegulatoryDataList(BluetoothGattCharacteristic characteristic) {
        if (characteristic != null) {
            mRegulatoryDataList = ByteArraytoHex(characteristic.getValue());
        }
        return mRegulatoryDataList;
    }


    public String ByteArraytoHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();
    }


}
