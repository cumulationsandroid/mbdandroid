/*
 *
 *
 *     EddystoneConfigService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Attributes.GattAttributes;
import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.Utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.UUID;

public class EddystoneConfigService {
    public static final int RESET_VALUE = 1;
    public static final int URI_VALUE_TYPE1 = 0;
    public static final int URI_VALUE_TYPE2 = 1;
    public static final int URI_VALUE_TYPE3 = 2;
    public static final int URI_VALUE_TYPE4 = 3;
    public static final String URI_CONTENT_TYPE1 = "http://www.";
    public static final String URI_CONTENT_TYPE2 = "https://www.";
    public static final String URI_CONTENT_TYPE3 = "http://";
    public static final String URI_CONTENT_TYPE4 = "https://";
        public static BluetoothGattService mService;
        public static EddystoneConfigService mEddystoneConfigService;
    public static String FLAGS = "0x";
        public BluetoothGattCharacteristic mCharacteristic;
        public Boolean mLockState = false;
        public String mUrlData;
        public String mFlags = null;
        public int mTxPowerLevels1 = 0;
        public int mTxPowerLevels2 = 0;
        public int mTxPowerLevels3 = 0;
        public int mTxPowerLevels4 = 0;
        public int mTxPowerMode = 0;
        public int mBeaconPeriod = 0;

        /**
         * Single instance creation
         */
        public static EddystoneConfigService getInstance() {

            if(mEddystoneConfigService == null) {
                mEddystoneConfigService = new EddystoneConfigService();
            }
            return mEddystoneConfigService;
        }


        /**
         * SingletonClass
         * Create private constructor
         */
        public void setGattService(BluetoothGattService service) {
            mService = service;
        }

        public List<BluetoothGattCharacteristic> getCharacteristics() {
            if(mService != null){
            return mService
                    .getCharacteristics();
            }else{
                return null;
            }

        }

        public void readData(BluetoothGattCharacteristic gattCharacteristic) {
            final int charaProp = gattCharacteristic.getProperties();
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                BLEConnection.readCharacteristic(gattCharacteristic);

            }
        }

        public void setLockStateCharacteristics(BluetoothGattCharacteristic lockCharacteristics){
            byte[] lockByte;
            try {
                int value = lockCharacteristics.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                byte[] val = lockCharacteristics.getValue();
                String values = Utils.ByteArraytoHex(val).toString();
                //int value = lockByte[0];
                if (value == 1){
                    mLockState = true;
                }else{
                    mLockState = false;
                }
                Log.d("value", "value " + value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void setUriCharacteristics(BluetoothGattCharacteristic uriCharacteristics){
            try {
                mUrlData = null;
           byte[] data = uriCharacteristics.getValue();
           int length = data.length;
           String url;
           byte[] value = new byte[length- 1];
           for(int i=1,j=0 ; i<length ; i++,j++){
                value[j]= data[i];
           }
           try {
                mUrlData = new String( value, "UTF8");
                int prefix =  (data[0]&0xff);
                switch(prefix){
                    case URI_VALUE_TYPE1:
                        mUrlData = URI_CONTENT_TYPE1 + mUrlData ;
                        break;
                    case URI_VALUE_TYPE2:
                        mUrlData = URI_CONTENT_TYPE2 + mUrlData ;
                        break;
                    case URI_VALUE_TYPE3:
                        mUrlData = URI_CONTENT_TYPE3 + mUrlData ;
                        break;
                    case URI_VALUE_TYPE4:
                        mUrlData = URI_CONTENT_TYPE4 + mUrlData ;
                        break;
                    default:
                        break;
                }

               Log.d("URL", "URL " + mUrlData);
           } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
           }}catch (Exception e){
                e.printStackTrace();
            }
        }

    public void setFlags(BluetoothGattCharacteristic flagCharacteristic){
       int flag;
        try {
           int flagvalue = flagCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
           mFlags = FLAGS + Integer.toHexString(flagvalue);
            //Utils.
            //Log.d("mFlags", "mFlags " + mFlags);
            //Log.d("mFlags in string", "mFlags " + Integer.toHexString(mFlags));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTxPowerLevels(BluetoothGattCharacteristic txPowerLevelCharacteristic){
        try {
            mTxPowerLevels1 = txPowerLevelCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 0);
            mTxPowerLevels2 = txPowerLevelCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 1);
            mTxPowerLevels3 = txPowerLevelCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 2);
            mTxPowerLevels4 = txPowerLevelCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 3);
            Log.d("mTxPowerLevels1", "mTxPowerLevels1 " + mTxPowerLevels1);
            Log.d("mTxPowerLevels2", "mTxPowerLevels2 " + mTxPowerLevels2);
            Log.d("mTxPowerLevels3", "mTxPowerLevels3 " + mTxPowerLevels3);
            Log.d("mTxPowerLevels4", "mTxPowerLevels4 " + mTxPowerLevels4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTxPowerMode(BluetoothGattCharacteristic txPowerModeCharacteristic){
        try {
            mTxPowerMode = txPowerModeCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            Log.d("mTxPowerMode", "mTxPowerMode " + mTxPowerMode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setBeaconPeriod(BluetoothGattCharacteristic beaconPeriodCharacteristic){
        try {
            mBeaconPeriod = beaconPeriodCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            Log.d("mBeaconPeriod", "mBeaconPeriod " + mBeaconPeriod);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeUrlData(byte urlPrefix, String data){
      /* byte urlPrefix = 0;

        switch(prefix){
            case URI_CONTENT_TYPE1:
                urlPrefix = (byte)URI_VALUE_TYPE1;
                break;
            case URI_CONTENT_TYPE2:
                urlPrefix = (byte)URI_VALUE_TYPE2;
                break;
            case URI_CONTENT_TYPE3:
                urlPrefix = (byte)URI_VALUE_TYPE3;
                break;
            case URI_CONTENT_TYPE4:
                urlPrefix = (byte)URI_VALUE_TYPE4;
                break;
            default:
                break;
        }*/
        BluetoothGattCharacteristic urlCharacteristic = mService.getCharacteristic
                (UUIDDatabase.UUID_EddyStoneURICharacteristics);
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(urlPrefix);
            try {
                outputStream.write(data.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            byte urlData[] = outputStream.toByteArray();
            BLEConnection.setCharacteristicWriteWithoutResponse(urlCharacteristic, urlData);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void writeUrlFlags(int data){
        BluetoothGattCharacteristic flagsCharacteristic = mService.getCharacteristic
                (UUID.fromString(GattAttributes.EddyStoneFlagsCharacteristics));
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.putInt(data);
        BLEConnection.setCharacteristicWriteWithoutResponse(flagsCharacteristic, byteBuffer.array());
    }

    public void writeTxPowerLevel(int level1, int level2, int level3, int level4){
        BluetoothGattCharacteristic txPowerlevelCharacteristic = mService.getCharacteristic
                (UUID.fromString(GattAttributes.EddyStoneTxPowerLevelCharacteristics));
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.put(0, (byte)level1);
        byteBuffer.put(1, (byte)level2);
        byteBuffer.put(2, (byte)level3);
        byteBuffer.put(3, (byte) level4);
        BLEConnection.setCharacteristicWriteWithoutResponse(txPowerlevelCharacteristic, byteBuffer.array());
    }

    public void writeTxPowerMOde(int data){
        BluetoothGattCharacteristic txPowerModeCharacteristic = mService.getCharacteristic
                (UUID.fromString(GattAttributes.EddyStoneTxModeCharacteristics));
        ByteBuffer byteBuffer = ByteBuffer.allocate(1);
        byteBuffer.put((byte) data);
        BLEConnection.setCharacteristicWriteWithoutResponse(txPowerModeCharacteristic, byteBuffer.array());
    }

    public void writeBeaconPeriod(int data){

        BluetoothGattCharacteristic beaconPeriodCharacteristic = mService.getCharacteristic
                (UUID.fromString(GattAttributes.EddyStoneBeaconPeriodCharacteristics));
       /* ByteBuffer byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.put((byte) data);*/
        byte[] result = new byte[2];

        result[0] = (byte) (data);
        result[1] = (byte) (data >> 8 /*>> 0*/);
       // Log.d("eddystone byte buffer",byteBuffer.array().toString());
        BLEConnection.setCharacteristicWriteWithoutResponse(beaconPeriodCharacteristic, result);
    }

    public void reset(){
        BluetoothGattCharacteristic resetCharacteristic = mService.getCharacteristic
                (UUID.fromString(GattAttributes.EddyStoneResetCharacteristics));
        ByteBuffer byteBuffer = ByteBuffer.allocate(1);
        byteBuffer.put((byte)RESET_VALUE);
        BLEConnection.setCharacteristicWriteWithoutResponse( resetCharacteristic, byteBuffer.array());
    }

    public void writeLockCode(int lockCode){
        BluetoothGattCharacteristic beaconLockCharacteristic = mService.getCharacteristic
                (UUID.fromString(GattAttributes.EddyStoneLockCodeCharacteristics));
        ByteBuffer byteBuffer = ByteBuffer.allocate(16);
        byteBuffer.put((byte) lockCode);
        if(beaconLockCharacteristic != null){
            BLEConnection.setCharacteristicWriteWithoutResponse(beaconLockCharacteristic, byteBuffer.array());
        }
    }

    public void writeUnlockCode(int unlockCode){
        BluetoothGattCharacteristic beaconUnLockCharacteristic = mService.getCharacteristic
                (UUID.fromString(GattAttributes.EddyStoneUnlockCharacteristics));
        ByteBuffer byteBuffer = ByteBuffer.allocate(16);
        byteBuffer.put((byte) unlockCode);
        if(beaconUnLockCharacteristic != null){
            BLEConnection.setCharacteristicWriteWithoutResponse(beaconUnLockCharacteristic, byteBuffer.array());
        }
    }

    }
