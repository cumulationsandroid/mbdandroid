/*
 *
 *
 *     HealthThermometerService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.TemperatureMeasurementCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.TemperatureTypeCharacteristic;

public class HealthThermometerService {

    private TemperatureMeasurementCharacteristic mHtpTempMeasurementChar;
    private BluetoothGattService mHtpService;

    public HealthThermometerService(BluetoothGattService service) {
        mHtpService = service;
    }


    public BluetoothGattCharacteristic getTemperatureMeasurementCharacteristic() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mHtpService.getCharacteristic(UUIDDatabase.UUID_HEALTH_THERMOMETER);
        mHtpTempMeasurementChar = TemperatureMeasurementCharacteristic.
                getInstance();
        mHtpTempMeasurementChar.
                setTemperatureMeasurementCharacteristic(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public void startNotifyTemperatureMeasurementCharacteristic() {
        if (mHtpTempMeasurementChar != null) {
            mHtpTempMeasurementChar.indicate(true);
        }
    }


    public void stopNotifyTemperatureMeasurementCharacteristic() {
        if (mHtpTempMeasurementChar != null) {
            mHtpTempMeasurementChar.indicate(false);
        }
    }


    public BluetoothGattCharacteristic getTemperatureTypeCharacteristic() {
        TemperatureTypeCharacteristic mHtpTempTypeChar;
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mHtpService.getCharacteristic(UUIDDatabase.UUID_HEALTH_THERMOMETER_SENSOR_LOCATION);
        mHtpTempTypeChar = TemperatureTypeCharacteristic.getInstance();
        mHtpTempTypeChar.setTemperatureTypeCharacteristic(bluetoothGattCharacteristic);
        mHtpTempTypeChar.read();
        return bluetoothGattCharacteristic;
    }

}
