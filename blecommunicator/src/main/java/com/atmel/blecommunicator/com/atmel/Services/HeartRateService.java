/*
 *
 *
 *     HeartRateService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.BodySensorLocationCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.HeartRateControlPointCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.HeartRateMeasurementCharacteristic;

public class HeartRateService {

    private HeartRateMeasurementCharacteristic mHRMeasurementChar;
    private BodySensorLocationCharacteristic mBSLocationChar;
    private BluetoothGattService mHtpService;

    public HeartRateService(BluetoothGattService service) {
        mHtpService = service;
    }

    public BluetoothGattCharacteristic getHeartRateMeasurementCharacteristic() {
        mHRMeasurementChar = HeartRateMeasurementCharacteristic.
                getInstance();
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mHtpService.getCharacteristic(UUIDDatabase.UUID_HEART_RATE_MEASUREMENT);

        mHRMeasurementChar.
                setHeartRateMeasurementCharacteristic(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public void startNotifyHeartRateMeasurementCharacteristic() {
        if (mHRMeasurementChar != null) {
            mHRMeasurementChar.notify(true);
        }
    }

    public void stopNotifyHeartRateMeasurementCharacteristic() {
        if (mHRMeasurementChar != null) {
            mHRMeasurementChar.notify(false);
        }
    }

    public BluetoothGattCharacteristic getBodySensorLocationCharacteristic() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic = mHtpService.getCharacteristic(UUIDDatabase.UUID_BODY_SENSOR_LOCATION);
        mBSLocationChar = BodySensorLocationCharacteristic.getInstance();
        mBSLocationChar.setBodySensorLocationCharacteristic(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public void readBodySensorLocationCharacteristic() {
        if (mBSLocationChar != null) {
            mBSLocationChar.read();
        }
    }

    public BluetoothGattCharacteristic getHeartRateControlPointCharacteristic() {
        HeartRateControlPointCharacteristic mHRCPChar;
        BluetoothGattCharacteristic bluetoothGattCharacteristic = mHtpService.getCharacteristic(UUIDDatabase.UUID_HEART_RATE_CONTROL_POINT);
        mHRCPChar = HeartRateControlPointCharacteristic.getInstance();
        mHRCPChar.setHeartRateControlPointCharacteristic(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }
}
