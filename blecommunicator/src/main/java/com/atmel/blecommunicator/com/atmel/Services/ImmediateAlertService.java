/*
 *
 *
 *     ImmediateAlertService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.AlertLevelCharacteristic;


public class ImmediateAlertService {
    private static ImmediateAlertService mImmediateAlertService;
    private BluetoothGattService mIMPService;

    /**
     * SingletonClass
     * Create private constructor
     */
    private ImmediateAlertService() {
    }

    /**
     * Single instance creation
     */
    public static ImmediateAlertService getInstance() {
        if (mImmediateAlertService == null) {
            mImmediateAlertService = new ImmediateAlertService();
        }
        return mImmediateAlertService;
    }

    public void setImmediateAlertService(BluetoothGattService immediateAlertService) {
        mIMPService = immediateAlertService;
    }

    public BluetoothGattCharacteristic writeAlertLevelCharacteristic(byte[] data) {
        AlertLevelCharacteristic mAlertLevelCharacteristic;
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mIMPService.getCharacteristic(UUIDDatabase.UUID_ALERT_LEVEL);
        mAlertLevelCharacteristic = AlertLevelCharacteristic.getInstance();
        mAlertLevelCharacteristic.setAlertLevelCharacteristic(bluetoothGattCharacteristic);
        mAlertLevelCharacteristic.writeLevel(data);
        return bluetoothGattCharacteristic;
    }

}
