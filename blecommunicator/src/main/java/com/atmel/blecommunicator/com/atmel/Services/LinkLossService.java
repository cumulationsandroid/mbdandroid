/*
 *
 *
 *     LinkLossService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.AlertLevelCharacteristic;


public class LinkLossService {
    private static LinkLossService mLinkLossService;
    private AlertLevelCharacteristic mAlertLevelCharacteristic;
    private BluetoothGattService mLLPService;

    /**
     * SingletonClass
     * Create private constructor
     */
    private LinkLossService() {
    }

    /**
     * Single instance creation
     */
    public static LinkLossService getInstance() {
        if (mLinkLossService == null) {
            mLinkLossService = new LinkLossService();
        }
        return mLinkLossService;
    }

    public void setLinkLossAlertService(BluetoothGattService linkLossAlertService) {
        mLLPService = linkLossAlertService;
    }

    public BluetoothGattCharacteristic writeAlertLevelCharacteristic(byte[] data) {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mLLPService.getCharacteristic(UUIDDatabase.UUID_ALERT_LEVEL);
        mAlertLevelCharacteristic = AlertLevelCharacteristic.getInstance();
        mAlertLevelCharacteristic.setAlertLevelCharacteristic(bluetoothGattCharacteristic);
        mAlertLevelCharacteristic.writeLevel(data);
        return bluetoothGattCharacteristic;
    }

    public BluetoothGattCharacteristic getAlertLevel() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mLLPService.getCharacteristic(UUIDDatabase.UUID_ALERT_LEVEL);
        Log.v("Characteristic", "" + bluetoothGattCharacteristic.getUuid());
        mAlertLevelCharacteristic = AlertLevelCharacteristic.getInstance();
        mAlertLevelCharacteristic.setAlertLevelCharacteristic(bluetoothGattCharacteristic);

        return bluetoothGattCharacteristic;
    }

    public void readAlertLevel() {
        Log.e("LevelCharacteristic", "" + mAlertLevelCharacteristic);
        if (mAlertLevelCharacteristic != null) {
            mAlertLevelCharacteristic.getLevel();
        }
    }
}
