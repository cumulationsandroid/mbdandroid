/*
 *
 *
 *     NextDstChangeService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

import java.util.Calendar;

public class NextDstChangeService {
    private static NextDstChangeService mTimeService;
    private String mUtc;
    private int mUtcValue = 0;


    /**
     * SingletonClass
     * Create private constructor
     */
    private NextDstChangeService() {
    }

    /**
     * Single instance creation
     */
    public static NextDstChangeService getInstance() {
        if (mTimeService == null) {

            mTimeService = new NextDstChangeService();
        }
        return mTimeService;
    }

    public BluetoothGattService initialize() {
        BluetoothGattService mHtpService;
        mHtpService = new BluetoothGattService(UUIDDatabase.UUID_NEXT_DST_CHANGE_SERVICE,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);

        //bleApplication.setBluetoothGattAdvService(service);
        try {
            BluetoothGattCharacteristic nextDstCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_DST_TIME,
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_READ,
                            BluetoothGattCharacteristic.PERMISSION_READ);
            boolean status = mHtpService.addCharacteristic(nextDstCharacteristic);
            if ( !status ) {
                mHtpService.addCharacteristic(nextDstCharacteristic);
            }
            // service.addCharacteristic(offsetCharacteristic);
            BLEConnection.addService(mHtpService);
        } catch (Exception ignored) {

        }
        //mHtpService = service;
        return mHtpService;
    }


    public void notifyConnectedDevices() {

        BluetoothGattCharacteristic readCharacteristic = BLEConnection.mGattServer.getService(UUIDDatabase.UUID_CURRENT_TIME_SERVICE)
                .getCharacteristic(UUIDDatabase.UUID_CURRENT_TIME);

        //Time currentTime = new Time();
        Calendar c = Calendar.getInstance();
       // int seconds = c.get(Calendar.SECOND);

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int monthday = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        int weekday = c.get(Calendar.DAY_OF_WEEK);

        byte[] timeData = new byte[10];
        timeData[0] = (byte) (year % 256);
        timeData[1] = (byte) (year / 256);
        timeData[2] = (byte) (month + 1);
        timeData[3] = (byte) (monthday);
        timeData[4] = (byte) (hour);
        timeData[5] = (byte) (minute);
        timeData[6] = (byte) (second);
        timeData[7] = (byte) (weekday - 1);
        timeData[8] = 0;
        timeData[9] = 0;


//        int value = Math.max(0,
//                (int) (System.currentTimeMillis() / 1000));
        readCharacteristic.setValue(timeData);
        BLEConnection.notifyConnectedDevices(readCharacteristic);
    }


    public void writeLocalTime(int requestid, int offset, byte[] value) {

        BLEConnection.GattServerSendResponse(requestid, offset, value);
    }

/*
    public  String getDst(int dstvalue){
        String dst = null;

        switch(dstvalue){
            case 0:
                dst = ;
                break;
            case 3600000:
                dst = "Daylight Time (+1h)";
                break;
            case 1800000:
                dst = "Half An Hour Daylight Time (+0.5h)";
                break;
            case 7200000:
                dst = "Double Daylight Time (+2h)";
                break;
            default:
                break;
        }
        return dst;
    }


    public static String timeZone()
    {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault());
        String   timeZone = new SimpleDateFormat("Z").format(calendar.getTime());
        return timeZone.substring(0, 3) + ":"+ timeZone.substring(3, 5);
    }

    public static int getTimeOffset(long time)
    {
        TimeZone tz = TimeZone.getDefault();

        return tz.getOffset(time);
    }

    private void convertUtcToValue(){

        switch(mUtc){

            case "UTC-12:00":
                mUtcValue = -48;
                break;
            case "UTC-11:00":
                mUtcValue = -44;
                break;
            case "UTC-10:00":
                mUtcValue = -40;
                break;
            case "UTC-09:30":
                mUtcValue = -38;
                break;
            case "UTC-09:00":
                mUtcValue = -36;
                break;
            case "UTC-08:00":
                mUtcValue = -32;
                break;
            case "UTC-07:00":
                mUtcValue = -28;
                break;
            case "UTC-06:00":
                mUtcValue = -24;
                break;
            case "UTC-05:00":
                mUtcValue = -20;
                break;
            case "UTC-04:30":
                mUtcValue = -18;
                break;
            case "UTC-04:00":
                mUtcValue = -16;
                break;
            case "UTC-03:30":
                mUtcValue = -14;
                break;
            case "UTC-03:00":
                mUtcValue = -12;
                break;
            case "UTC-02:00":
                mUtcValue = -8;
                break;
            case "UTC-01:00":
                mUtcValue = -4;
                break;
            case "UTC+00:00":
                mUtcValue = 0;
                break;
            case "UTC+14:00":
                mUtcValue = 56;
                break;
            case "UTC+13:00":
                mUtcValue = 52;
                break;
            case "UTC+12:45":
                mUtcValue = 51;
                break;
            case "UTC+12:00":
                mUtcValue = 48;
                break;

            case "UTC+11:30":
                mUtcValue = 46;
                break;
            case "UTC+11:00":
                mUtcValue = 44;
                break;
            case "UTC+10:30":
                mUtcValue = 42;
                break;
            case "UTC+10:00":
                mUtcValue = 40;
                break;
            case "UTC+09:30":
                mUtcValue = 38;
                break;
            case "UTC+09:00":
                mUtcValue = 36;
                break;
            case "UTC+08:45":
                mUtcValue = 35;
                break;
            case "UTC+08:00":
                mUtcValue = 32;
                break;
            case "UTC+07:00":
                mUtcValue = 28;
                break;
            case "UTC+06:30":
                mUtcValue =  26;
                break;
            case "UTC+06:00":
                mUtcValue = 24;
                break;
            case "UTC+05:45":
                mUtcValue = 23;
                break;
            case "UTC+05:30":
                mUtcValue = 22;
                break;
            case "UTC+05:00":
                mUtcValue = 20;
                break;
            case "UTC+04:30":
                mUtcValue = 18;
                break;
            case "UTC+04:00":
                mUtcValue = 16;
                break;
            case "UTC+03:30":
                mUtcValue = 14;
                break;
            case "UTC+03:00":
                mUtcValue = 12;
                break;
            case "UTC+02:00":
                mUtcValue = 8;
                break;
            case "UTC+01:00":
                mUtcValue = 4;
                break;

        }

    }
*/
}
