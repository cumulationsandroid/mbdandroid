/*
 *
 *
 *     OTAUService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic1Indicate;
import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic3Read;

public class OTAUService {

    private OTAUCharacteristic1Indicate mOTAUCharacteristic1Indicate;
    private OTAUCharacteristic2Write mOTAUCharacteristic2Write;
    private OTAUCharacteristic3Read mOTAUCharacteristic3Read;
    private BluetoothGattService mService;

    public OTAUService(BluetoothGattService service) {
        mService = service;
    }

    public BluetoothGattCharacteristic getOTAUCharacteristic1Indicate() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mService.getCharacteristic(UUIDDatabase.UUID_OTAU_CHARACTERISTIC_1_INDICATE);
        mOTAUCharacteristic1Indicate = OTAUCharacteristic1Indicate.getInstance();
        mOTAUCharacteristic1Indicate.setOTAUCharacteristic1Indicate(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public BluetoothGattCharacteristic getOTAUCharacteristic2Write() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mService.getCharacteristic(UUIDDatabase.UUID_OTAU_CHARACTERISTIC_2_WRITE);
        mOTAUCharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        mOTAUCharacteristic2Write.setOTAUCharacteristic2Write(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public BluetoothGattCharacteristic getOTAUCharacteristic3Read() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mService.getCharacteristic(UUIDDatabase.UUID_OTAU_CHARACTERISTIC_3_READ);
        mOTAUCharacteristic3Read = OTAUCharacteristic3Read.getInstance();
        mOTAUCharacteristic3Read.setOTAUCharacteristic3Read(bluetoothGattCharacteristic);
        return bluetoothGattCharacteristic;
    }

    public void startIndicateOTAUCharacteristic() {
        if (mOTAUCharacteristic1Indicate != null) {
            mOTAUCharacteristic1Indicate.indicate(true);
        }
    }
}
