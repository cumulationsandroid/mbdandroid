/*
 *
 *
 *     PhoneAlertService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

import java.text.Format;

/**
 * Created by Atmel on 20/8/15.
 */
public class PhoneAlertService {
    private static PhoneAlertService mPhoneAlertService;

    /**
     * Making SingleTone
     */
    private PhoneAlertService() {
    }

    public static PhoneAlertService getInstance() {
        return mPhoneAlertService == null ? mPhoneAlertService = new PhoneAlertService() : mPhoneAlertService;
    }


    public BluetoothGattService getBlueToothGattService() {
        BluetoothGattService mBluetoothGattService;
        mBluetoothGattService = new BluetoothGattService(UUIDDatabase.UUID_PHONE_ALERT_SERVICE,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);

        try {
            /**
             * Phone Alert Characteristic
             */
            BluetoothGattCharacteristic phoneAlertStatusCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_PHONE_ALERT_STATUS, BluetoothGattCharacteristic.FORMAT_UINT8 |
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_NOTIFY | BluetoothGattCharacteristic.PROPERTY_READ,
                            BluetoothGattCharacteristic.PERMISSION_READ);

            /**
             * Phone ringer settings Characteristic
             */

            BluetoothGattCharacteristic phoneRingerSettingsCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_PHONE_ALERT_RINGER_SETTINGS, BluetoothGattCharacteristic.FORMAT_UINT8 |
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_NOTIFY | BluetoothGattCharacteristic.PROPERTY_READ,
                            BluetoothGattCharacteristic.PERMISSION_READ);

            /**
             * Phone Controll point Characteristic
             */
            BluetoothGattCharacteristic phoneControllPointCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_PHONE_ALERT_CONTROL_POINT, BluetoothGattCharacteristic.FORMAT_UINT8 |
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE, BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);

            /**
             * Making descriptors
             */
            BluetoothGattDescriptor phoneAlertGattDescriptor = new BluetoothGattDescriptor(UUIDDatabase.UUID_CLIENT_CHARACTERISTIC_CONFIG,
                    BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);

            /**
             * Adding Descriptor phone alert status
             */
            phoneAlertStatusCharacteristic.addDescriptor(phoneAlertGattDescriptor);

            /**
             * Adding Descriptor phone ringer settings descriptor
             */
            phoneRingerSettingsCharacteristic.addDescriptor(phoneAlertGattDescriptor);


            mBluetoothGattService.addCharacteristic(phoneAlertStatusCharacteristic);
            mBluetoothGattService.addCharacteristic(phoneRingerSettingsCharacteristic);
            mBluetoothGattService.addCharacteristic(phoneControllPointCharacteristic);

            BLEConnection.addService(mBluetoothGattService);
        } catch (Exception ignored) {

        }
        return mBluetoothGattService;
    }

    public void notifyAlertStatus(byte[] bytes) {
        BluetoothGattCharacteristic readCharacteristic = BLEConnection.mGattServer.getService(UUIDDatabase.UUID_PHONE_ALERT_SERVICE)
                .getCharacteristic(UUIDDatabase.UUID_PHONE_ALERT_STATUS);
        readCharacteristic.setValue(bytes);
        BLEConnection.notifyConnectedDevices(readCharacteristic);
    }

    public void notifyRingerSettings(byte[] bytes) {
        BluetoothGattCharacteristic readCharacteristic = BLEConnection.mGattServer.getService(UUIDDatabase.UUID_PHONE_ALERT_SERVICE)
                .getCharacteristic(UUIDDatabase.UUID_PHONE_ALERT_RINGER_SETTINGS);
        for (int i = 0; i < bytes.length; i++) {
            readCharacteristic.setValue(bytes[i], BluetoothGattCharacteristic.FORMAT_UINT8, i);
        }
        BLEConnection.notifyConnectedDevices(readCharacteristic);
    }

    public void writePhoneAlert(int requestid, int offset, byte[] bytes) {

        BLEConnection.GattServerSendResponse(requestid, offset, bytes);
    }

    public void writAlertStatus(int requestid, int offset, byte[] bytes) {

        BLEConnection.GattServerSendResponse(requestid, offset, bytes);
    }


}
