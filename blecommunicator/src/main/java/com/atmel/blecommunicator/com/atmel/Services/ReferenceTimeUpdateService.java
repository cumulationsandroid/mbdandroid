/*
 *
 *
 *     ReferenceTimeUpdateService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

/**
 * Created by Atmel on 24/8/15.
 */
public class ReferenceTimeUpdateService {
    private static ReferenceTimeUpdateService mService;

    /**
     * Making single tone
     */
    private ReferenceTimeUpdateService() {
    }

    public static ReferenceTimeUpdateService getInstance() {
        return mService == null ? mService = new ReferenceTimeUpdateService() : mService;
    }

    public BluetoothGattService innitialize() {
        BluetoothGattService mBluetoothGattService;
        mBluetoothGattService = new BluetoothGattService(UUIDDatabase.UUID_REFERENCE_TIME_UPDATE_SERVICE,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        try {
            BluetoothGattCharacteristic referenceTimeBluetoothGattCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_TIME_UPDATE_CONTROL_POINT,
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE,
                            BluetoothGattCharacteristic.PERMISSION_READ);

            BluetoothGattCharacteristic timeUpdateStateBluetoothGattCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_TIME_UPDATE_STATE,
                            //Read-only characteristic, supports notifications
                            BluetoothGattCharacteristic.PROPERTY_READ,
                            BluetoothGattCharacteristic.PERMISSION_READ);

            mBluetoothGattService.addCharacteristic(referenceTimeBluetoothGattCharacteristic);
            mBluetoothGattService.addCharacteristic(timeUpdateStateBluetoothGattCharacteristic);
            BLEConnection.addService(mBluetoothGattService);
        } catch (Exception ignored) {

        }
        return mBluetoothGattService;
    }
}
