/*
 *
 *
 *     ScanParameterService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.ScanIntervalWindowCharacteristic;
import com.atmel.blecommunicator.com.atmel.Characteristics.ScanRefreshCharacteristics;

/**
 * Created by
 */
public class ScanParameterService {
    private static ScanParameterService mScanParameterService;
    private ScanRefreshCharacteristics mScanRefreshCharacteristics;
    private BluetoothGattService mService;


    /**
     * Disable Constructor for making object
     */
    private ScanParameterService() {
    }

    /**
     * Making SingleTone
     *
     * @return
     */
    public static ScanParameterService getInstance() {
        return mScanParameterService == null ? mScanParameterService = new ScanParameterService() : mScanParameterService;
    }

    public BluetoothGattCharacteristic writeScanIntervalWindowCharacteristic(byte[] data) {
        ScanIntervalWindowCharacteristic mScanIntervalWindowCharacteristic;
        mScanIntervalWindowCharacteristic = ScanIntervalWindowCharacteristic.getInstance();
        BluetoothGattCharacteristic characteristic = mService.getCharacteristic(UUIDDatabase.UUID_SCAN_INTERVAL_WINDOW_PARAM);
        mScanIntervalWindowCharacteristic.setScanIntervalWindowCharacteristic(characteristic);
        mScanIntervalWindowCharacteristic.writeWithoutResponse(data);
        return characteristic;
    }

    public void setScanParameterService(BluetoothGattService service) {
        mService = service;
    }

    public BluetoothGattCharacteristic getScanRefreshCharacteristics() {
        mScanRefreshCharacteristics = ScanRefreshCharacteristics.getInstance();
        BluetoothGattCharacteristic characteristic = mService.getCharacteristic(UUIDDatabase.UUID_SCAN_REFRESH_PARAM);
        mScanRefreshCharacteristics.setScanRefreshCharacteristics(characteristic);
        return characteristic;
    }

    public void startNotify(Boolean isNotify) {
        mScanRefreshCharacteristics.startStopNotifyScanInterval(isNotify);
    }


}
