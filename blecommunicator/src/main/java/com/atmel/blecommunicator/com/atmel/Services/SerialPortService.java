/*
 *
 *
 *     SerialPortService.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.Services;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;
import com.atmel.blecommunicator.com.atmel.Characteristics.SerialPortCharacteristic;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.parsers.SerialPortParser;


public class SerialPortService {
    private static SerialPortService mSerialPortService;
    public String mRecevicedMessage;
    private SerialPortCharacteristic mSerialPortCharacteristic;
    private BluetoothGattService mSSPService;

    /**
     * SingletonClass
     * Create private constructor
     */
    private SerialPortService() {

    }

    /**
     * Single instance creation
     */
    public static SerialPortService getInstance() {
        if (mSerialPortService == null) {
            mSerialPortService = new SerialPortService();
        }
        return mSerialPortService;
    }

    public void setmSerialPortService(BluetoothGattService serialPortService) {
        mSSPService = serialPortService;
    }

    public BluetoothGattCharacteristic writeSerialPortData(byte[] data) {
        BluetoothGattCharacteristic bluetoothGattCharacteristic
                = mSSPService.getCharacteristic(UUIDDatabase.UUID_SERIAL_PORT_END_POINT);
        mSerialPortCharacteristic = SerialPortCharacteristic.getInstance();
        mSerialPortCharacteristic.setSerialPortCharacteristic(bluetoothGattCharacteristic);
        mSerialPortCharacteristic.writeData(data);
        return bluetoothGattCharacteristic;
    }

    public BluetoothGattService initialize() {
        BluetoothGattService serialPortService = new BluetoothGattService(UUIDDatabase.UUID_SERIAL_PORT,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        try {
            /**
             * End Point Characteristic
             */
            BluetoothGattCharacteristic endPointCharacteristic =
                    new BluetoothGattCharacteristic(UUIDDatabase.UUID_SERIAL_PORT_END_POINT,
                            //supports notifications
                            BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                            BluetoothGattCharacteristic.PERMISSION_READ);


      /*  *
         * Making descriptors
         */
            BluetoothGattDescriptor GattDescriptor = new BluetoothGattDescriptor(UUIDDatabase.UUID_CLIENT_CHARACTERISTIC_CONFIG,
                    BluetoothGattCharacteristic.PERMISSION_READ);

       /* *
         * Adding Descriptor end point
         */
            endPointCharacteristic.addDescriptor(GattDescriptor);

            serialPortService.addCharacteristic(endPointCharacteristic);
            BLEConnection.addService(serialPortService);
        } catch (Exception ignored) {

        }
        return serialPortService;
    }

    public void notifyConnectedDevices(String value) {

        BluetoothGattCharacteristic characteristic = BLEConnection.mGattServer.getService(UUIDDatabase.UUID_SERIAL_PORT)
                .getCharacteristic(UUIDDatabase.UUID_SERIAL_PORT_END_POINT);
      //  byte[] values = value.getBytes();
        characteristic.setValue(value);
        BLEConnection.notifyConnectedDevices(characteristic);
    }

    public BluetoothGattCharacteristic getCharacteristic() {
        BluetoothGattCharacteristic bluetoothGattCharacteristic = null;
        try {
            bluetoothGattCharacteristic
                    = mSSPService.getCharacteristic(UUIDDatabase.UUID_SERIAL_PORT_END_POINT);
            mSerialPortCharacteristic = SerialPortCharacteristic.getInstance();
            mSerialPortCharacteristic.setSerialPortCharacteristic(bluetoothGattCharacteristic);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bluetoothGattCharacteristic;
    }

    public void startNotifyCharacteristic() {
        if (mSerialPortCharacteristic != null) {
            mSerialPortCharacteristic.notifySPP(true);
        }
    }

    public void stopNotifyCharacteristic() {
        if (mSerialPortCharacteristic != null) {
            mSerialPortCharacteristic.notifySPP(false);
        }
    }

    public void requestMtuChange() {
        BLEConnection.changeMtu(512);
    }

    /**
     * Parse the characteristic values in {@link BluetoothGattCharacteristic#FORMAT_UINT8} format
     *
     * @param characteristic
     */
    public void parseValue(BluetoothGattCharacteristic characteristic) {
        SerialPortParser.parse(characteristic);
        mRecevicedMessage = SerialPortParser.getMessage();
    }

}
