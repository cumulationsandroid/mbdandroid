/*
 *
 *
 *     ReportAttributes.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.blecommunicator.com.atmel.Utils;


import java.util.HashMap;

/**
 * Report and Report Reference class for the states of the Report
 */
public class ReportAttributes {

    public static final String INPUT_REPORT_TYPE_STRING = "Report Type: Input Report";
    public static final String OUTPUT_REPORT_TYPE_STRING = "Report Type: Output Report";
    public static final String FEATURE_REPORT_TYPE_STRING = "Report Type: Feature Report";
    //Report Reference Types
    public static String INPUT_REPORT_TYPE = "1";
    public static String OUTPUT_REPORT_TYPE = "2";
    public static String FEATURE_REPORT_TYPE = "3";
    //Report Refernce Defalut values
    public static String REPORT_REF_ID = "Report Reference ID not found";
    public static String REPORT_TYPE = "Report Type not found";


    private static HashMap<String, String> mReferenceAttributes = new HashMap<String, String>();
    private static HashMap<String, String> mReferenceAttributesType = new HashMap<String, String>();
    private static HashMap<String, Integer> mReportvalues = new HashMap<String, Integer>();

    static {

        mReferenceAttributesType.put(INPUT_REPORT_TYPE, INPUT_REPORT_TYPE_STRING);
        mReferenceAttributesType.put(OUTPUT_REPORT_TYPE, OUTPUT_REPORT_TYPE_STRING);
        mReferenceAttributesType.put(FEATURE_REPORT_TYPE, FEATURE_REPORT_TYPE_STRING);

    }

    public static String lookupReportReferenceID(String reference) {
        String name = mReferenceAttributes.get(reference);
        return name == null ? "" + reference : name;
    }

    public static String lookupReportReferenceType(String referenceType) {
        String name = mReferenceAttributesType.get(referenceType);
        return name == null ? "Reserved for future use" : name;
    }

    public static int lookupReportValues(String reportValue) {
        int returnValueDefault = 0;
        Integer value = mReportvalues.get(reportValue);
        if (value != null) {
            return value;
        } else {
            return returnValueDefault;
        }

    }
}
