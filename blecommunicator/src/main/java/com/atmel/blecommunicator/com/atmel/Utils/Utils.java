/*
 *
 *
 *     Utils.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.blecommunicator.com.atmel.Utils;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.Context;
import android.content.SharedPreferences;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Atmel on 18/8/15.
 */
public class Utils {

    public static final String COMMON_PREFE = "ATMEL_PREFERENCES";
    public static final String CURRENT_TIME_NOTIFICATION_STATUS = "CURRENT_TIME_NOTIFICATION ENABLE DISABLE INFO";
    public static final String ANS_READ_NOTIFICATION_STATUS = "ANS_READ_NOTIFICATION_STATUS";
    public static final String ANS_UNREAD_NOTIFICATION_STATUS = "ANS_UNREAD_NOTIFICATION_STATUS";
    public static final String PHONE_ALERT_STATUS = "Phone Alert Notification";
    public static final String PHONE_ALERT_VALUE = "Phone Alert VAlue";
    final private static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static byte[] bytesFromInt(int value) {
        //Convert result into raw bytes. GATT APIs expect LE order
        return ByteBuffer.allocate(4)
                .order(ByteOrder.LITTLE_ENDIAN)
                .putInt(value)
                .array();
    }

    public static byte[] getShiftedTimeValue(int timeOffset) {
        int value = Math.max(0,
                (int) (System.currentTimeMillis() / 1000) - timeOffset);
        return bytesFromInt(value);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String ByteArraytoHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();

    }

    /**
     * Store default String value to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @param value   {@link boolean}
     */
    public static void setBooleanSharedPreference(Context context, String key, boolean value) {
        SharedPreferences.Editor edit;
        edit = createSharedPreferences(context, COMMON_PREFE).edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static boolean getBooleanSharedPreference(Context context, String key) {
        SharedPreferences preferences = createSharedPreferences(context, COMMON_PREFE);
        return preferences.getBoolean(key, false);
    }

    /**
     * Initializing SharedPreferences
     *
     * @param context {@link Context}
     * @return {@link SharedPreferences}
     */
    private static SharedPreferences createSharedPreferences(Context context, String type) {
        return context.getSharedPreferences(type, Context.MODE_PRIVATE);
    }

    public static String parse(final BluetoothGattCharacteristic characteristic) {
        return parse(characteristic.getValue());
    }

    public static String parse(final BluetoothGattDescriptor descriptor) {
        return parse(descriptor.getValue());
    }

    public static String parse(final byte[] data) {
        if (data == null || data.length == 0)
            return "";

        final char[] out = new char[data.length * 3 - 1];
        for (int j = 0; j < data.length; j++) {
            int v = data[j] & 0xFF;
            out[j * 3] = HEX_ARRAY[v >>> 4];
            out[j * 3 + 1] = HEX_ARRAY[v & 0x0F];
            if (j != data.length - 1)
                out[j * 3 + 2] = '-';
        }
        return "(0x) " + new String(out);
    }

    public static String parseASCII(final byte[] data) {
        if (data == null || data.length == 0)
            return "";

        final char[] out = new char[data.length * 3 - 1];
        for (int j = 0; j < data.length; j++) {
            int v = data[j] & 0xFF;
            out[j * 3] = HEX_ARRAY[v >>> 4];
            out[j * 3 + 1] = HEX_ARRAY[v & 0x0F];
            if (j != data.length - 1)
                out[j * 3 + 2] = '-';
        }
        String temp = new String(out).replace("-", "");
        StringBuilder output = new StringBuilder("");
        try {
            for (int i = 0; i < temp.length(); i += 2) {
                String str = temp.substring(i, i + 2);
                output.append((char) Integer.parseInt(str, 16));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }


    /**
     * Method to convert hex to byteArray
     */
    public static byte[] convertingTobyteArray(String result) {
        String[] splited = result.split("\\s+");
        byte[] valueByte = new byte[splited.length];
        for (int i = 0; i < splited.length; i++) {
            if (splited[i].length() > 2) {
                String trimmedByte = splited[i].split("x")[1];
                valueByte[i] = (byte) convertstringtobyte(trimmedByte);
            }

        }
        return valueByte;
    }


    /**
     * Convert the string to byte
     *
     * @param string
     * @return
     */
    private static int convertstringtobyte(String string) {
        return Integer.parseInt(string, 16);
    }
}
