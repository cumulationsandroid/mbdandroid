/*
 *
 *
 *     OTAUCommand.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

public abstract class OTAUCommand {

    //This is the listener robject that manages the communication between
    // OTAUCommand subclasses and OTAUManager
    protected SuccessFailureListener successFailureListener;

    /**
     * This method handles the creation of packet for each command
     * and sends it to BLEConnection class for sending via BLE
     */
    public abstract void createRequestPacket();

    /**
     * This method parses the data obtained after sending any command to
     * BLE for OTA communication, based on parsed data resend it to
     * OTAUManager for further processing
     * @param characteristic data to parse
     */
    //public abstract void handleCommandResponse(BluetoothGattCharacteristic characteristic);

    /**
     * By this method each OTAUCommand subclass holds the instance of\
     * OTAUManager for further commnad flow
     * @param otauManager OTAUManager instance
     */
    public void setSuccessFailureListener(OTAUManager otauManager){
        successFailureListener = otauManager;
    }

    /**
     * Interface for communicating between
     * OTAUCommand subclasses and OTAUManager
     */
    public interface SuccessFailureListener {

        /**
         * This method is implemented in OTAUManager class to handle the successful
         * event of one command flow
         * @param command
         */
        void onSuccess(OTAUCommand command);

        /**
         * This method is implemented in OTAUManager class to handle the failure
         * event of one command flow
         * @param command
         */
        void onFailure(OTAUCommand command);

    }
}
