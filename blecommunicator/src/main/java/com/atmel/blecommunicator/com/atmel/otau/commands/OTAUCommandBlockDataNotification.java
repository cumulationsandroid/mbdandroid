/*
 *
 *
 *     OTAUCommandBlockDataNotification.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

public class OTAUCommandBlockDataNotification extends OTAUCommand implements BLEConnection.OTAResponseListener{

    private static int DATA_LENGTH = 0;
    private static final int FRAME_LENGTH = 0;
    private static final int COMMAND = 2;
    private static final int SECTION_ID = 3;
    private static final int BLOCK_NO = 4;
    private static final int BLOCK_DATA = 5;

    //Request Code
    private static final byte AT_OTAU_BLOCK_DATA_NOTIFY_REQUEST = 0x08;

    //Image Section ID table
    private static final byte PATCH_SECTION = 0x01;
    private static final byte LIBRARY_SECTIOIN = 0x02;
    private static final byte APPLICATION_SECTION = 0x03;

    private OTAUCharacteristic2Write mOTACharacteristic2Write;

    public OTAUCommandBlockDataNotification() {
        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
    }

    @Override
    public void createRequestPacket() {
        DATA_LENGTH = FRAME_LENGTH - 4;
        mOTACharacteristic2Write.writeBlockDataNotification(FRAME_LENGTH, DATA_LENGTH, AT_OTAU_BLOCK_DATA_NOTIFY_REQUEST, SECTION_ID, BLOCK_NO, BLOCK_DATA);
    }

    @Override
    public void handleCommandResponse(BluetoothGattCharacteristic characteristic) {
        OTAUCommandBlockDataNotifyResponse mResponse = new OTAUCommandBlockDataNotifyResponse();
        mResponse.parseResponseSuccess();
    }

    @Override
    public void setSuccessFailureListener(OTAUManager otauManager) {
        //otauManager.onSuccess("");
    }

    public class OTAUCommandBlockDataNotifyResponse {

        private int DATA_LENGTH;
        private int COMMAND;
        private int SECTION_ID;
        private int PAGE_NUMBER;
        private int BLOCK_NUMBER;

        //Error Code
        private static final byte AT_OTAU_IMAGE_BLOCK_INFO_ERROR = (byte) 0x92;

        //Image section ID table
        private static final byte PATCH_SECYTION = (byte) 0x01;
        private static final byte LIBRARY_SECTION = (byte) 0x02;
        private static final byte APPLICATION_SECTION = (byte) 0x03;

        private OTAUCharacteristic2Write mOTACharacteristic2Write;

        public OTAUCommandBlockDataNotifyResponse() {
            mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        }

        public void parseResponseSuccess() {
            DATA_LENGTH = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            SECTION_ID = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            PAGE_NUMBER = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 4);
            BLOCK_NUMBER = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 6);
        }

        public void parseResponseFailure() {
            DATA_LENGTH = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            SECTION_ID = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            PAGE_NUMBER = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 4);
            BLOCK_NUMBER = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 6);
        }
    }

}


