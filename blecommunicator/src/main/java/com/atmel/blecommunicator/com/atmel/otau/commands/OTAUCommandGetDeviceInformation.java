/*
 *
 *
 *     OTAUCommandGetDeviceInformation.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

public class OTAUCommandGetDeviceInformation extends OTAUCommand implements BLEConnection.OTAResponseListener {

    //Request Code
    public static final byte AT_OTAU_GET_DEVICE_INFO_REQUEST = 0x1B;
    private static final int DATA_LENGTH = 0x01;
    private static final int DATA_LENGTH_SHIFT = DATA_LENGTH >> 8;
    private static final int FRAME_LENGTH = 3;
    //Response Code
    private static final byte AT_OTAU_GET_DEVICE_INFO_RESP = (byte) 0x1C;

    //Response Error Code
    private static final byte AT_ERROR = (byte) 0x03;
    public static int PRODUCT_ID;
    public static int VENDOR_ID;
    public static int FIRMWARE_VERSION;
    public static int FIRMWARE_VERSION_MAJOR;
    public static int FIRMWARE_VERSION_MINOR;
    public static int FIRMWARE_VERSION_BUILD;
    public static int HARDWARE_VERSION;
    public static int HARDWARE_VERSION_MAJOR;
    public static int HARDWARE_VERSION_MINOR;
    public static int HARDWARE_REVISION;
    public static int PENDING_FIRMWARE_VERSION;
    public static int PENDING_FIRMWARE_VERSION_MAJOR;
    public static int PENDING_FIRMWARE_VERSION_MINOR;
    public static int PENDING_FIRMWARE_VERSION_BUILD;
    private static int COMMAND;
    private static int DATA_LENGTH_RESPONSE;
    private static int TOTAL_SECTIONS;
    private OTAUCharacteristic2Write mOTACharacteristic2Write;

    public OTAUCommandGetDeviceInformation() {
        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        BLEConnection.setOTAResponseListener(this);
    }

    public static String getFileVersion() {
        return FIRMWARE_VERSION_MAJOR + "." + FIRMWARE_VERSION_MINOR + "." + FIRMWARE_VERSION_BUILD;
    }

    @Override
    public void createRequestPacket() {
        byte[] data = new byte[3];
        data[0] = (byte) DATA_LENGTH;
        data[1] = (byte) DATA_LENGTH_SHIFT;
        data[2] = (byte) AT_OTAU_GET_DEVICE_INFO_REQUEST;
        mOTACharacteristic2Write.writeByteData(data);
    }

    @Override
    public void handleCommandResponse(BluetoothGattCharacteristic characteristic) {
        DATA_LENGTH_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        if (COMMAND == AT_OTAU_GET_DEVICE_INFO_RESP) {
            PRODUCT_ID = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 3);
            VENDOR_ID = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 5);
            TOTAL_SECTIONS = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 7);
            FIRMWARE_VERSION = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 8);
            FIRMWARE_VERSION_MAJOR = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 8);
            FIRMWARE_VERSION_MINOR = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 9);
            FIRMWARE_VERSION_BUILD = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 10);
            HARDWARE_VERSION = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 12);
            HARDWARE_VERSION_MAJOR = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 12);
            HARDWARE_VERSION_MINOR = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 13);
            HARDWARE_REVISION = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 14);
            PENDING_FIRMWARE_VERSION = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 15);
            PENDING_FIRMWARE_VERSION_MAJOR = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 15);
            PENDING_FIRMWARE_VERSION_MINOR = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 16);
            PENDING_FIRMWARE_VERSION_BUILD = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 17);

            Log.d("OTAGetDeviceInfoResp>>", " DATA_LENGTH " + DATA_LENGTH_RESPONSE + " COMMAND " + COMMAND + " PRODUCT_ID " + PRODUCT_ID
                    + " VENDOR_ID " + VENDOR_ID + " TOTAL_SECTIONS " + TOTAL_SECTIONS + " FIRMWARE_VERSION " + FIRMWARE_VERSION
                    + " FIRMWARE_VERSION_MAJOR " + FIRMWARE_VERSION_MAJOR + " FIRMWARE_VERSION_MINOR " + FIRMWARE_VERSION_MINOR + " FIRMWARE_VERSION_BUILD " + FIRMWARE_VERSION_BUILD
                    + " HARDWARE_VERSION " + HARDWARE_VERSION + " HARDWARE_VERSION_MAJOR " + HARDWARE_VERSION_MAJOR + " HARDWARE_VERSION_MINOR " + HARDWARE_VERSION_MINOR
                    + " HARDWARE_REVISION " + HARDWARE_REVISION + " PENDING_FIRMWARE_VERSION " + PENDING_FIRMWARE_VERSION
                    + " PENDING_FIRMWARE_VERSION_MAJOR " + PENDING_FIRMWARE_VERSION_MAJOR + " PENDING_FIRMWARE_VERSION_MINOR " + PENDING_FIRMWARE_VERSION_MINOR
                    + " PENDING_FIRMWARE_VERSION_BUILD " + PENDING_FIRMWARE_VERSION_BUILD);
            successFailureListener.onSuccess(this);
        } else {
            successFailureListener.onFailure(this);
        }
    }

    @Override
    public void setSuccessFailureListener(OTAUManager otauManager) {
        this.successFailureListener = otauManager;
    }


}
