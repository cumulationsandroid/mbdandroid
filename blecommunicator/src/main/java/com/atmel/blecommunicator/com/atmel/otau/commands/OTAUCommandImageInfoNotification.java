/*
 *
 *
 *     OTAUCommandImageInfoNotification.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;

public class OTAUCommandImageInfoNotification extends OTAUCommand implements BLEConnection.OTAResponseListener {

    //Request Code
    private static final byte AT_OTAU_IMAGE_INFO_NOTIFY_REQUEST = (byte) 0x03;
    //Response Code
    private static final byte AT_OTAU_IMAGE_INFO_NOTIFY_RESP = 0x04;
    private static final byte AT_OTAU_FAILURE = 0x30;
    //Image option command table
    private static final byte AT_OTAU_START_IMAGE_DOWNLOAD = (byte) 0x05;
    private static final byte AT_OTAU_RESUME_OTAU_REQUEST = (byte) 0x0D;
    //Image section ID table
    private static final byte PATCH_SECYTION = (byte) 0x01;
    private static final byte LIBRARY_SECTION = (byte) 0x02;
    private static final byte APPLICATION_SECTION = (byte) 0x03;
    //Error Code
    private static final byte AT_OTAU_INVALID_SECTION = (byte) 0x8E;
    private static final byte AT_OTAU_INVALID_IMAGE_SIZE = (byte) 0x8F;
    private static final byte AT_OTAU_UNKNOWN_ERROR = (byte) 0x90;
    public static int START_RESUME_SECTION_ID;
    public static int PAGE_NUMBER;
    public static int BLOCK_NUMBER;
    public static int PAGE_SIZE;
    public static int BLOCK_SIZE;
    private static int DATA_LENGTH = 0x2E;
    private static int DATA_LENGTH_SHIFT = DATA_LENGTH >> 8;
    private static int TOTAL_SECTIONS = 3;
    private static int TOTAL_IMAGE_SIZE = 4;
    private static int PATCH_SECTION_ID = 5;
    private static int SIZE_OF_PATCH_SECTION = 6;
    private static int PATCH_START_ADDRESS = 7;
    private static int LIBRARY_SECTION_ID = 8;
    private static int SIZE_OF_LIBRARY_SECTION = 9;
    private static int LIBRARY_START_ADDRESS = 10;
    private static int APPLICATION_SECTION_ID = 11;
    private static int SIZE_OF_APPLICATION_SECTION = 12;
    private static int APPLICATION_START_ADDRESS = 13;
    private static int TOTAL_IMAGE_CRC = 13;
    private static int PATCH_SECTION_CRC = 13;
    private static int LIBRARY_SECTION_CRC = 13;
    private static int APPLICATION_SECTION_CRC = 13;
    private static int COMMAND;
    //Response
    private int DATA_LENGTH_RESPONSE;
    private int IMAGE_OPTION_START_RESUME;
    private OTAUCharacteristic2Write mOTACharacteristic2Write;

    public OTAUCommandImageInfoNotification() {
        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        BLEConnection.setOTAResponseListener(this);
    }

    @Override
    public void createRequestPacket() {
        TOTAL_SECTIONS = (byte) BinModel.getNumberOfsections();
        TOTAL_IMAGE_SIZE = (byte) BinModel.getTotalImagesize();
        PATCH_SECTION_ID = (byte) BinModel.getPatchSectionId();
        SIZE_OF_PATCH_SECTION = (byte) BinModel.getPatchSize();
        PATCH_START_ADDRESS = (byte) BinModel.getHardwareVersion();
        LIBRARY_SECTION_ID = (byte) BinModel.getAppHeaderPatchSectionId();
        SIZE_OF_LIBRARY_SECTION = (byte) BinModel.getAppHeaderPatchSize();
        LIBRARY_START_ADDRESS = (byte) BinModel.getAppHeaderPatchStartAddress();
        APPLICATION_SECTION_ID = (byte) BinModel.getAppSectionId();
        SIZE_OF_APPLICATION_SECTION = (byte) BinModel.getAppSize();
        APPLICATION_START_ADDRESS = (byte) BinModel.getAppStartAddress();
        TOTAL_IMAGE_CRC = (byte) BinModel.getTotalImageCRC();
        PATCH_SECTION_CRC = (byte) BinModel.getPatchSectionCRC();
        LIBRARY_SECTION_CRC = (byte) BinModel.getAppHaderPatchCRC();
        APPLICATION_SECTION_CRC = (byte) BinModel.getAppSectionCRC();

        Log.e("OTAImageInfoRequest>>", " DATA_LENGTH " + DATA_LENGTH + " TOTAL_SECTIONS " + TOTAL_SECTIONS + " TOTAL_IMAGE_SIZE " + TOTAL_IMAGE_SIZE
                + " PATCH_SECTION_ID " + PATCH_SECTION_ID + " SIZE_OF_PATCH_SECTION " + SIZE_OF_PATCH_SECTION
                + " PATCH_START_ADDRESS " + PATCH_START_ADDRESS + " LIBRARY_SECTION_ID " + LIBRARY_SECTION_ID
                + " SIZE_OF_LIBRARY_SECTION " + SIZE_OF_LIBRARY_SECTION + " LIBRARY_START_ADDRESS " + LIBRARY_START_ADDRESS
                + " APPLICATION_SECTION_ID " + APPLICATION_SECTION_ID + " SIZE_OF_APPLICATION_SECTION " + SIZE_OF_APPLICATION_SECTION
                + " APPLICATION_START_ADDRESS " + APPLICATION_START_ADDRESS + " TOTAL_IMAGE_CRC " + TOTAL_IMAGE_CRC
                + " PATCH_SECTION_CRC " + PATCH_SECTION_CRC + " LIBRARY_SECTION_CRC " + LIBRARY_SECTION_CRC
                + " APPLICATION_SECTION_CRC " + APPLICATION_SECTION_CRC);

        byte[] data = new byte[48];
        data[0] = (byte) DATA_LENGTH;
        data[1] = (byte) DATA_LENGTH_SHIFT;
        data[2] = AT_OTAU_IMAGE_INFO_NOTIFY_REQUEST;
        data[3] = (byte) BinModel.getNumberOfsections();
        data[4] = (byte) BinModel.getTotalImagesize();
        data[5] = (byte) (BinModel.getTotalImagesize() >> 8);
        data[6] = (byte) (BinModel.getTotalImagesize() >> 16);
        data[7] = (byte) (BinModel.getTotalImagesize() >> 24);
        data[8] = (byte) BinModel.getPatchSectionId();
        data[9] = (byte) BinModel.getPatchSize();
        data[10] = (byte) (BinModel.getPatchSize() >> 8);
        data[11] = (byte) (BinModel.getPatchSize() >> 16);
        data[12] = (byte) BinModel.getPatchStartAddress();
        data[13] = (byte) (BinModel.getPatchStartAddress() >> 8);
        data[14] = (byte) (BinModel.getPatchStartAddress() >> 16);
        data[15] = (byte) (BinModel.getPatchStartAddress() >> 24);
        data[16] = (byte) BinModel.getAppHeaderPatchSectionId();
        data[17] = (byte) BinModel.getAppHeaderPatchSize();
        data[18] = (byte) (BinModel.getAppHeaderPatchSize() >> 8);
        data[19] = (byte) (BinModel.getAppHeaderPatchSize() >> 16);
        data[20] = (byte) BinModel.getAppHeaderPatchStartAddress();
        data[21] = (byte) (BinModel.getAppHeaderPatchStartAddress() >> 8);
        data[22] = (byte) (BinModel.getAppHeaderPatchStartAddress() >> 16);
        data[23] = (byte) (BinModel.getAppHeaderPatchStartAddress() >> 24);
        data[24] = (byte) BinModel.getAppSectionId();
        data[25] = (byte) BinModel.getAppSize();
        data[26] = (byte) (BinModel.getAppSize() >> 8);
        data[27] = (byte) (BinModel.getAppSize() >> 16);
        data[28] = (byte) BinModel.getAppStartAddress();
        data[29] = (byte) (BinModel.getAppStartAddress() >> 8);
        data[30] = (byte) (BinModel.getAppStartAddress() >> 16);
        data[31] = (byte) (BinModel.getAppStartAddress() >> 24);
        data[32] = (byte) BinModel.getTotalImageCRC();
        data[33] = (byte) (BinModel.getTotalImageCRC() >> 8);
        data[34] = (byte) (BinModel.getTotalImageCRC() >> 16);
        data[35] = (byte) (BinModel.getTotalImageCRC() >> 24);
        data[36] = (byte) BinModel.getPatchSectionCRC();
        data[37] = (byte) (BinModel.getPatchSectionCRC() >> 8);
        data[38] = (byte) (BinModel.getPatchSectionCRC() >> 16);
        data[39] = (byte) (BinModel.getPatchSectionCRC() >> 24);
        data[40] = (byte) BinModel.getAppHaderPatchCRC();
        data[41] = (byte) (BinModel.getAppHaderPatchCRC() >> 8);
        data[42] = (byte) (BinModel.getAppHaderPatchCRC() >> 16);
        data[43] = (byte) (BinModel.getAppHaderPatchCRC() >> 24);
        data[44] = (byte) BinModel.getAppSectionCRC();
        data[45] = (byte) (BinModel.getAppSectionCRC() >> 8);
        data[46] = (byte) (BinModel.getAppSectionCRC() >> 16);
        data[47] = (byte) (BinModel.getAppSectionCRC() >> 24);
        mOTACharacteristic2Write.writeByteData(data);
    }

    @Override
    public void handleCommandResponse(BluetoothGattCharacteristic characteristic) {
        DATA_LENGTH_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        if (COMMAND == AT_OTAU_IMAGE_INFO_NOTIFY_RESP) {
            IMAGE_OPTION_START_RESUME = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            START_RESUME_SECTION_ID = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 4);
            PAGE_NUMBER = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 5);
            BLOCK_NUMBER = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 7);
            PAGE_SIZE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 8);
            BLOCK_SIZE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 10);
            Log.e("OTAImageInfoResp>>", " DATA_LENGTH_RESPONSE " + DATA_LENGTH_RESPONSE + " COMMAND " + COMMAND + " IMAGE_OPTION_START_RESUME " + IMAGE_OPTION_START_RESUME
                    + " START_RESUME_SECTION_ID " + START_RESUME_SECTION_ID + " PAGE_NUMBER " + PAGE_NUMBER + " BLOCK_NUMBER "
                    + BLOCK_NUMBER + " PAGE_SIZE " + PAGE_SIZE + " BLOCK_SIZE " + BLOCK_SIZE);
            successFailureListener.onSuccess(this);
        } else {
            successFailureListener.onFailure(this);
        }
    }

    @Override
    public void setSuccessFailureListener(OTAUManager otauManager) {
        this.successFailureListener = otauManager;
    }

    public void parseResponseFailure() {
        DATA_LENGTH = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        //ERROR_CODE = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
    }

}
