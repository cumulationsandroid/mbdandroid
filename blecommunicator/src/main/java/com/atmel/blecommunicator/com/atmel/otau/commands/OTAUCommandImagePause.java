/*
 *
 *
 *     OTAUCommandImagePause.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;

public class OTAUCommandImagePause extends OTAUCommand implements BLEConnection.OTAResponseListener{

    private static final int FRAME_LENGTH = 0;

    private static final int DATA_LENGTH = 0x01;
    private static final int DATA_LENGTH_SHIFT = DATA_LENGTH >> 8;

    //Request Code
    private static final byte AT_OTAU_PAUSE_OTAU_REQUEST = 0x0B;
    private static final byte AT_OTAU_PAUSE_OTAU_RESP = 0x0C;

    private static int COMMAND;
    private static int DATA_LENGTH_RESPONSE;

    private OTAUCharacteristic2Write mOTACharacteristic2Write;

    public OTAUCommandImagePause() {
        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        BLEConnection.setOTAResponseListener(this);
    }

    @Override
    public void createRequestPacket() {
        byte[] data = new byte[3];
        data[0] = (byte) DATA_LENGTH;
        data[1] = (byte) DATA_LENGTH_SHIFT;
        data[2] = (byte) AT_OTAU_PAUSE_OTAU_REQUEST;
        mOTACharacteristic2Write.writeByteData(data);
    }

    @Override
    public void handleCommandResponse(BluetoothGattCharacteristic characteristic) {
        DATA_LENGTH_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        if (COMMAND == AT_OTAU_PAUSE_OTAU_RESP) {
            successFailureListener.onSuccess(this);
        } else {
            successFailureListener.onFailure(this);
        }
    }

    @Override
    public void setSuccessFailureListener(OTAUManager otauManager) {
        this.successFailureListener = otauManager;
    }

}
