/*
 *
 *
 *     OTAUCommandImageSectionEndNotification.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.otau.fileread.SectionModel;

public class OTAUCommandImageSectionEndNotification extends OTAUCommand implements OTAUCommand.SuccessFailureListener, BLEConnection.OTAResponseListener {

    private static final int DATA_LENGTH = 0x02;
    private static final int DATA_LENGTH_SHIFT = DATA_LENGTH >> 8;
    //Request Code
    private static final byte AT_OTAU_IMAGE_SECTION_END_NOTIFY_REQUEST = 0x19;
    //Image Section ID table
    private static final byte PATCH_SECTION = 0x01;
    private static final byte LIBRARY_SECTIOIN = 0x02;
    private static final byte APPLICATION_SECTION = 0x03;
    //Response Code
    private static final byte AT_OTAU_IMAGE_SECTION_END_NOTIFY_RESP = (byte) 0x1A;
    private static final byte AT_OTAU_IMAGE_SECTION_END_ERROR = (byte) 0x93;
    SectionModel currentSection;
    private boolean isPageMode;
    private int currentSectionID;
    private int currentPageNo;
    private int currentBlockNo;
    private int pageSize;
    private int blockSize;
    //Response
    private int DATA_LENGTH_RESPONSE;
    private int COMMAND;
    private int SECTION_ID;
    private OTAUCharacteristic2Write mOTACharacteristic2Write;

    private OTAUCommandPageDataNotification otauCommandPageDataNotification;

    public OTAUCommandImageSectionEndNotification(OTAUCommandImageInfoNotification updateInfo) {
        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        BLEConnection.setOTAResponseListener(this);
        currentSectionID = updateInfo.START_RESUME_SECTION_ID;
        currentPageNo = updateInfo.PAGE_NUMBER;
        currentBlockNo = updateInfo.BLOCK_NUMBER;
        pageSize = updateInfo.PAGE_SIZE;
        blockSize = updateInfo.BLOCK_SIZE;
        isPageMode = (pageSize == blockSize);
    }

    public void startSectionWrite() {
        currentSection = new SectionModel(currentSectionID, pageSize);
        writeSectionData();
    }

    public void writeSectionData() {
        if (isPageMode)
            writeSectionDataPageMode();
        else
            writeSectionDataBlockMode();
    }

    private void writeSectionDataPageMode() {
        otauCommandPageDataNotification = new OTAUCommandPageDataNotification(currentPageNo, pageSize, currentSection);
        otauCommandPageDataNotification.setSuccessFailureListener(this);
        otauCommandPageDataNotification.startPageWrite();
    }

    private void writeSectionDataBlockMode() {
    }

    @Override
    public void createRequestPacket() {
        byte[] data = new byte[4];
        data[0] = (byte) DATA_LENGTH;
        data[1] = (byte) DATA_LENGTH_SHIFT;
        data[2] = AT_OTAU_IMAGE_SECTION_END_NOTIFY_REQUEST;
        data[3] = (byte) currentSectionID;
        Log.e("CurrentSectionID>>> " + currentSectionID, "data>>" + BLEConnection.ByteArraytoHex(data));
        mOTACharacteristic2Write.writeByteData(data);
    }

    @Override
    public void handleCommandResponse(BluetoothGattCharacteristic characteristic) {
        DATA_LENGTH_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        if (COMMAND == AT_OTAU_IMAGE_SECTION_END_NOTIFY_RESP) {
            DATA_LENGTH_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            COMMAND = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            SECTION_ID = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            Log.e("OTAimageSectionEnd>>", " DATA_LENGTH " + DATA_LENGTH_RESPONSE + " COMMAND " + COMMAND + " SECTION_ID " + SECTION_ID);
            //Write next page in the segment
            currentSection = currentSection.nextSection();
            if (currentSection != null && currentSection.sectionData != null) {
                currentPageNo = 0;
                currentBlockNo = 0;
                currentSection.pageSize = pageSize;
                writeSectionData();
            } else {
                successFailureListener.onSuccess(this);
            }
        } else {
            successFailureListener.onFailure(this);
        }
    }

    @Override
    public void setSuccessFailureListener(OTAUManager otauManager) {
        this.successFailureListener = otauManager;
    }

    @Override
    public void onSuccess(OTAUCommand command) {
        if (OTAUManager.getInstance().mOTAUPauseFlag) {
            successFailureListener.onSuccess(command);
        } else {
            createRequestPacket();
            BLEConnection.setOTAResponseListener(this);
        }
    }

    @Override
    public void onFailure(OTAUCommand command) {

    }

    public void parseResponseFailure() {
        DATA_LENGTH_RESPONSE = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        SECTION_ID = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
    }
}