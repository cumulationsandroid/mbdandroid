/*
 *
 *
 *     OTAUCommandImageSwitchNotification.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;

public class OTAUCommandImageSwitchNotification extends OTAUCommand implements BLEConnection.OTAResponseListener{

    private static final int DATA_LENGTH = 0x06;
    private static final int DATA_LENGTH_SHIFT = DATA_LENGTH >> 8;
    private static final int FIRMWARE_VERSION = 4;

    //Request Code
    private static final byte AT_OTAU_IMAGE_SWITCH_REQUEST = (byte) 0x11;
    //Response Code
    private static final byte AT_OTAU_IMAGE_SWITCH_RESP = (byte) 0x12;
    private static final byte AT_OTAU_FAILURE = (byte) 0x30;
    //Error Code
    private static final byte AT_OTAU_IMAGE_SWITCH_ERROR = (byte) 0x82;
    //Response
    private int DATA_LENGTH_RESPONSE;
    private int COMMAND;
    private int TOTAL_SECTIONS_RESPONSE;
    private int ERROR_CODE;
    private OTAUCharacteristic2Write mOTACharacteristic2Write;

    public OTAUCommandImageSwitchNotification() {
        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        BLEConnection.setOTAResponseListener(this);
    }

    @Override
    public void createRequestPacket() {
        byte[] data = new byte[8];
        data[0] = (byte) DATA_LENGTH;
        data[1] = (byte) DATA_LENGTH_SHIFT;
        data[2] = AT_OTAU_IMAGE_SWITCH_REQUEST;
        data[3] = (byte) BinModel.getNumberOfsections();
        data[4] = (byte) BinModel.getFirmwareVersion();
        data[5] = (byte) (BinModel.getFirmwareVersion() >> 8);
        data[6] = (byte) (BinModel.getFirmwareVersion() >> 16);
        data[7] = (byte) (BinModel.getFirmwareVersion() >> 24);
        Log.e(" Image Switch Data,", "Data>>>>" + BLEConnection.ByteArraytoHex(data));
        mOTACharacteristic2Write.writeByteData(data);
    }

    @Override
    public void handleCommandResponse(BluetoothGattCharacteristic characteristic) {
        DATA_LENGTH_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);

        if (COMMAND == AT_OTAU_IMAGE_SWITCH_RESP) {
            TOTAL_SECTIONS_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            Log.e("OTAImageSwitchResp>>", " DATA_LENGTH_RESPONSE " + DATA_LENGTH_RESPONSE + " COMMAND " + COMMAND
                    + " TOTAL_SECTIONS_RESPONSE " + TOTAL_SECTIONS_RESPONSE);
            successFailureListener.onSuccess(this);
        } else {
            successFailureListener.onFailure(this);
        }
    }

    @Override
    public void setSuccessFailureListener(OTAUManager otauManager) {
        this.successFailureListener = otauManager;
    }

    public class OTAUCommandImageSwitchNotifyResponse {

        //Response Code
        private static final byte AT_OTAU_IMAGE_SWITCH_RESP = (byte) 0x12;
        private static final byte AT_OTAU_FAILURE = (byte) 0x30;
        //Error Code
        private static final byte AT_OTAU_IMAGE_SWITCH_ERROR = (byte) 0x82;
        private int DATA_LENGTH;
        private int COMMAND;
        private int TOTAL_SECTIONS;
        private int ERROR_CODE;
        private OTAUCharacteristic2Write mOTACharacteristic2Write;

        public OTAUCommandImageSwitchNotifyResponse() {
            mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        }

        public void parseResponseSuccess() {
            DATA_LENGTH = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            TOTAL_SECTIONS = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        }

        public void parseResponseFailure() {
            DATA_LENGTH = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            ERROR_CODE = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
        }
    }



}
