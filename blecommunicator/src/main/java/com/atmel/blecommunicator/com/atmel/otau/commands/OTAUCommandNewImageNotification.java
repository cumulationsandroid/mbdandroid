/*
 *
 *
 *     OTAUCommandNewImageNotification.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;

public class OTAUCommandNewImageNotification extends OTAUCommand implements BLEConnection.OTAResponseListener {

    private static final byte IMAGE_TYPE = 0x01;
    private static final byte PATCH = 0x01;
    private static final byte LIBRARY = 0x01;
    private static final byte APPLICATION = 0x01;
    private static final byte RESERVED = 0x01;
    private static final byte SECURITY_LEVEL_VALUE = 0x01;
    private static final byte AT_OTAU_IMAGE_NOTIFY_UPGRADE_REQUEST = 0x01;
    private static final byte AT_OTAU_IMAGE_NOTIFY_FORCE_UPGRADE_REQUEST = 0x0F;
    //Response Code
    private static final byte AT_OTAU_IMAGE_NOTIFY_UPGRADE_RESP = (byte) 0x02;
    private static final byte AT_OTAU_IMAGE_NOTIFY_FORCE_UPGRADE_RESP = (byte) 0x10;

    private static final byte AT_OTAU_FAILURE = 0x30;
    //Error Code
    private static final byte AT_OTAU_IMAGE_OLDER_VERSION = (byte) 0x85;
    private static final byte AT_OTAU_INVALID_VENDOR_ID = (byte) 0x88;
    private static final byte AT_OTAU_INVALID_PRODUCT_ID = (byte) 0x89;
    private static final byte AT_OTAU_INVALID_HARDWARE_VERSION = (byte) 0x8A;
    private static final byte AT_OTAU_INVALID_HARDWARE_REVISION = (byte) 0x8B;
    private static final byte AT_OTAU_SECURITY_LEVEL_NOT_SUPPORTED = (byte) 0x8d;
    public static int START_RESUME = 0;
    private static int PACKET_SIZE = 0;
    private static int DATA_LENGTH = (byte) 0x0E;
    private static int DATA_LENGTH_SHIFT = DATA_LENGTH >> 8;
    private static int PRODUCT_ID = 0;
    private static int VENDOR_ID = 0;
    private static int FIRMWARE_VERSION = 0;
    private static int HARDWARE_VERSION = 0;
    private static int HARDWARE_REVISION = 0;
    private static int SECURITY_LEVEL = 0;
    private static byte BYTE_FLAGS;
    //Response
    private static int DATA_LENGTH_RESPONSE;
    private static int COMMAND;
    private static int HARDWARE_REVISION_RESPONSE;
    private static int FIRMWARE_VERSION_RESPONSE;
    private static int HARDWARE_VERSION_RESPONSE;
    private static int ERROR_CODE;
    private OTAUCharacteristic2Write mOTACharacteristic2Write;

    public OTAUCommandNewImageNotification() {
        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        BLEConnection.setOTAResponseListener(this);
    }

    @Override
    public void createRequestPacket() {

        PRODUCT_ID = (byte) BinModel.getProductId();
        VENDOR_ID = (byte) BinModel.getVendorId();
        BYTE_FLAGS = (byte) BinModel.getFlagByte();
        FIRMWARE_VERSION = (byte) BinModel.getFirmwareVersion();
        HARDWARE_VERSION = (byte) BinModel.getHardwareVersion();
        HARDWARE_REVISION = (byte) BinModel.getHardwareRevision();
        SECURITY_LEVEL = (byte) BinModel.getSecurityLevel();

        Log.e("OTAImageNotiRequest>>", " DATA_LENGTH " + DATA_LENGTH + " PRODUCT_ID " + PRODUCT_ID + " VENDOR_ID " + VENDOR_ID
                + " BYTE_FLAGS " + BYTE_FLAGS + " FIRMWARE_VERSION " + FIRMWARE_VERSION + " HARDWARE_VERSION "
                + HARDWARE_VERSION + "HARDWARE_REVISION " + HARDWARE_REVISION + " SECURITY_LEVEL " + SECURITY_LEVEL);

        byte[] data = new byte[16];
        data[0] = (byte) DATA_LENGTH;
        data[1] = (byte) DATA_LENGTH_SHIFT;
        if (OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Upgrade")) {
            data[2] = AT_OTAU_IMAGE_NOTIFY_UPGRADE_REQUEST;
        } else if (OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Force Upgrade")) {
            data[2] = AT_OTAU_IMAGE_NOTIFY_FORCE_UPGRADE_REQUEST;
        }
        data[3] = (byte) BinModel.getProductId();
        data[4] = (byte) (BinModel.getProductId() >> 8);
        data[5] = (byte) BinModel.getVendorId();
        data[6] = (byte) (BinModel.getVendorId() >> 8);
        data[7] = (byte) BinModel.getFlagByte();
        data[8] = (byte) BinModel.getFirmwareVersion();
        data[9] = (byte) (BinModel.getFirmwareVersion() >> 8);
        data[10] = (byte) (BinModel.getFirmwareVersion() >> 16);
        data[11] = (byte) (BinModel.getFirmwareVersion() >> 24);
        data[12] = (byte) BinModel.getHardwareVersion();
        data[13] = (byte) (BinModel.getHardwareVersion() >> 8);
        data[14] = (byte) HARDWARE_REVISION;
        data[15] = (byte) SECURITY_LEVEL;
        mOTACharacteristic2Write.writeByteData(data);
    }

    @Override
    public void handleCommandResponse(BluetoothGattCharacteristic characteristic) {
        DATA_LENGTH_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);

        if (COMMAND == AT_OTAU_IMAGE_NOTIFY_UPGRADE_RESP && OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Upgrade")) {
            HARDWARE_REVISION_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            FIRMWARE_VERSION_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 4);
            HARDWARE_VERSION_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 8);
            START_RESUME = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 10);

            Log.e("OTAImageNotiUpgrResp>>", " DATA_LENGTH_RESPONSE " + DATA_LENGTH_RESPONSE + " COMMAND " + COMMAND
                    + " HARDWARE_REVISION_RESPONSE " + HARDWARE_REVISION_RESPONSE + " FIRMWARE_VERSION_RESPONSE " + FIRMWARE_VERSION_RESPONSE
                    + " HARDWARE_VERSION_RESPONSE " + HARDWARE_VERSION_RESPONSE + " START_RESUME " + START_RESUME);
            successFailureListener.onSuccess(this);
        } else if (COMMAND == AT_OTAU_IMAGE_NOTIFY_FORCE_UPGRADE_RESP && OTAUManager.getInstance().mUpgradeType.equalsIgnoreCase("Force Upgrade")) {
            HARDWARE_REVISION_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            FIRMWARE_VERSION_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 4);
            HARDWARE_VERSION_RESPONSE = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 8);
            START_RESUME = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 10);

            Log.e("OTAImageNotiForceResp>>", " DATA_LENGTH_RESPONSE " + DATA_LENGTH_RESPONSE + " COMMAND " + COMMAND
                    + " HARDWARE_REVISION_RESPONSE " + HARDWARE_REVISION_RESPONSE + " FIRMWARE_VERSION_RESPONSE " + FIRMWARE_VERSION_RESPONSE
                    + " HARDWARE_VERSION_RESPONSE " + HARDWARE_VERSION_RESPONSE + " START_RESUME " + START_RESUME);
            successFailureListener.onSuccess(this);
        } else {
            successFailureListener.onFailure(this);
        }
    }

    @Override
    public void setSuccessFailureListener(OTAUManager otauManager) {
        this.successFailureListener = otauManager;
    }

    public void parseResponseFailure() {
        DATA_LENGTH = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        ERROR_CODE = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
    }

    public int getOperation() {
        return START_RESUME;
    }
}
