/*
 *
 *
 *     OTAUCommandPageDataNotification.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.Characteristics.OTAUCharacteristic2Write;
import com.atmel.blecommunicator.com.atmel.Connection.BLEConnection;
import com.atmel.blecommunicator.com.atmel.otau.fileread.BinModel;
import com.atmel.blecommunicator.com.atmel.otau.fileread.PageModel;
import com.atmel.blecommunicator.com.atmel.otau.fileread.SectionModel;

public class OTAUCommandPageDataNotification extends OTAUCommand implements BLEConnection.OTAResponseListener {

    private static final int FRAME_LENGTH = 0;
    private static final int PAGE_NO = 4;
    private static final int PAGE_DATA = 5;
    //Request Code
    private static final byte AT_OTAU_PAGE_DATA_NOTIFY_REQUEST = 0x06;
    //Response Code
    private static final byte AT_OTAU_PAGE_DATA_NOTIFY_RESP = (byte) 0x07;
    private static final byte AT_OTAU_IMAGE_PAGE_INFO_ERROR = (byte) 0x91;
    //Image option command table
    private static final byte AT_OTAU_START_IMAGE_DOWNLOAD = (byte) 0x05;
    private static final byte AT_OTAU_RESUME_OTAU_REQUEST = (byte) 0x0D;
    //Image section ID table
    private static final byte PATCH_SECYTION = (byte) 0x01;
    private static final byte LIBRARY_SECTION = (byte) 0x02;
    private static final byte APPLICATION_SECTION = (byte) 0x03;
    private static int DATA_LENGTH = 0;
    private static int currentPageNo = 0;
    private static int mPageSize = 0;
    PageModel currentPage;
    private SectionModel currentSection;
    //Response
    private int DATA_LENGTH_RESPONSE;
    private int COMMAND;
    private int SECTION_ID;
    private int PAGE_NUMBER;
    private OTAUCharacteristic2Write mOTACharacteristic2Write;

    public OTAUCommandPageDataNotification() {
        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
    }

    public OTAUCommandPageDataNotification(int pageNo, int pageSize, SectionModel section) {

        mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        currentPageNo = pageNo;
        mPageSize = pageSize;
        currentSection = section;
    }

    public void startPageWrite() {
        BLEConnection.setOTAResponseListener(this);
        currentPage = new PageModel(currentSection, currentPageNo);
        writePageData();
    }

    public void writePageData() {
        createRequestPacket();
    }


    @Override
    public void createRequestPacket() {
        byte[] headerData = new byte[6];
        headerData[0] = (byte) (currentPage.pageData.length + 4);
        headerData[1] = (byte) ((currentPage.pageData.length + 4) >> 8);
        headerData[2] = AT_OTAU_PAGE_DATA_NOTIFY_REQUEST;
        headerData[3] = (byte) currentSection.sectionId;
        headerData[4] = (byte) currentPageNo;
        headerData[5] = (byte) (currentPageNo >> 8);
        byte[] data = new byte[headerData.length + currentPage.pageData.length];
        System.arraycopy(headerData, 0, data, 0, headerData.length);
        System.arraycopy(currentPage.pageData, 0, data, headerData.length, currentPage.pageData.length);
        Log.e("Page no>>> ", " " + currentPageNo + " Size>>> " + currentPage.pageData.length);
        Log.e("Page data>> ", " " + BLEConnection.ByteArraytoHex(currentPage.pageData));
        mOTACharacteristic2Write.writeByteData(data);
    }

    @Override
    public void handleCommandResponse(BluetoothGattCharacteristic characteristic) {
        DATA_LENGTH = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        COMMAND = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
        Log.e("cmnd>> ", " " + COMMAND);
        if (COMMAND == AT_OTAU_PAGE_DATA_NOTIFY_RESP) {
            SECTION_ID = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            PAGE_NUMBER = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 4);
            float progress = currentPage.progressFromPage();
            String progressString = "Written " + (int) ((progress * BinModel.getTotalImagesize())) + " bytes of " + (BinModel.getTotalImagesize() + " bytes");
            if (!OTAUManager.getInstance().mOTAUPauseFlag) {
                OTAUManager.getInstance().otauProgressInfoListener.OTAUProgressUpdater(progressString, progress);
            }
            //Write next page in the segment
            currentPage = currentPage.nextPage();
            if (currentPage != null && currentPage.pageData != null && !OTAUManager.getInstance().mOTAUPauseFlag) {
                currentPageNo = currentPage.pageNo;
                writePageData();
            } else {
                successFailureListener.onSuccess(this);
            }
        } else {
            successFailureListener.onFailure(this);
        }
    }

    public void setSuccessFailureListener(OTAUCommandImageSectionEndNotification sectionEndNotification) {
        this.successFailureListener = sectionEndNotification;
    }

    public class OTAUCommandPageDataNotifyResponse {

        private OTAUCharacteristic2Write mOTACharacteristic2Write;

        public OTAUCommandPageDataNotifyResponse() {
            mOTACharacteristic2Write = OTAUCharacteristic2Write.getInstance();
        }

        public void parseResponseSuccess() {
            DATA_LENGTH = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            SECTION_ID = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            PAGE_NUMBER = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 4);
        }

        public void parseResponseFailure() {
            DATA_LENGTH = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            COMMAND = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 2);
            SECTION_ID = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 3);
            PAGE_NUMBER = mOTACharacteristic2Write.mCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 4);
        }
    }

}
