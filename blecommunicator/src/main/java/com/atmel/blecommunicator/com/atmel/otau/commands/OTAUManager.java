/*
 *
 *
 *     OTAUManager.java
 *   
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.commands;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.atmel.blecommunicator.com.atmel.otau.fileread.FileModel;
import com.atmel.blecommunicator.com.atmel.otau.fileread.FileReader;

import java.io.File;
import java.util.ArrayList;

public class OTAUManager implements OTAUCommand.SuccessFailureListener {

    private static OTAUManager otauManager;
    private static File fileDirectory = new File(Environment.getExternalStorageDirectory() + File.separator + "Atmel");
    public boolean mOTAUOngoingFlag = false;
    public boolean mOTAUPauseFlag = false;
    public boolean mOTAUResumeUpdateFlag = false;
    public boolean mOTAUPendingUpdateFlag = false;
    public String mUpgradeType = "Upgrade";
    public String mResumeFilePath = null;
    public String mUpgradeFilePath = null;

    public OTAUCommandGetDeviceInformation otauGetDeviceInformation;
    public OTAUProgressInfoListener otauProgressInfoListener;
    OTAUCommandNewImageNotification otauCommandNewImageNotification;
    OTAUCommandImageInfoNotification otauCommandImageInfoNotification;
    OTAUCommandImageSectionEndNotification otauCommandSectionEndNotification;
    OTAUCommandImageEndNotification otauCommandImageEndNotification;
    OTAUCommandImageSwitchNotification otauImageSwitchNotification;
    OTAUCommandImagePause otauImagePauseNotification;
    OTAUCommandImageResume otauImageResumeNotification;
    private Context context;
    private boolean mUpdateForcedFlag = false;

    private OTAUManager() {
    }

    /**
     * This method creates object of Singleton class OTAUManager
     *
     * @return OTAUManager object
     */
    public static OTAUManager getInstance() {
        if (otauManager == null) {
            otauManager = new OTAUManager();
            return otauManager;
        } else {
            return otauManager;
        }
    }

    public void startOTAU() {
        //Sending the GetDeviceInfo request intially
        if (otauManager.otauProgressInfoListener != null) {
            otauManager.otauProgressInfoListener.OTAUProgressUpdater("initialize ota", 0);
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    getDeviceInformation();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1000);

    }

    public void startOTAUUpdate() {
        FileReader.setFileSelected(mUpgradeFilePath);
        newImageNotification();
    }

    public void startOTAUUpdateForced() {
        mResumeFilePath = null;
        mUpgradeFilePath = null;
        mUpdateForcedFlag = true;
        newImageNotification();
    }

    public void startResumeUpdate() {
        mOTAUResumeUpdateFlag = true;
        if (mResumeFilePath != null) {
            FileReader.setFileSelected(mResumeFilePath);
        } else if (mUpgradeFilePath != null) {
            FileReader.setFileSelected(mUpgradeFilePath);
        }
        newImageNotification();
    }

    public void pauseOTAUUpdate() {
        otauManager.otauProgressInfoListener.OTAUProgressUpdater("Pausing...", 0);
        mOTAUPauseFlag = true;
        //imagePauseNotification();
    }

    public void resumeOTAUUpdate() {
        mOTAUPauseFlag = false;
        otauManager.otauProgressInfoListener.OTAUProgressUpdater("Resuming...", 0);
        imageResumeNotification();
    }

    public void resumeOTAUForceUpgrade() {
        mOTAUPauseFlag = false;
        imageSectionEndNotification();
        mOTAUOngoingFlag = true;
    }

    /**
     * Handle the command AT_OTAU_GET_DEVICE_INFO_REQUEST
     */
    public void getDeviceInformation() {
        otauGetDeviceInformation = new OTAUCommandGetDeviceInformation();
        otauGetDeviceInformation.setSuccessFailureListener(this);
        otauGetDeviceInformation.createRequestPacket();
    }

    /**
     * Handle the command AT_OTAU_IMAGE_NOTIFY_REQUEST
     */
    public void newImageNotification() {
        otauCommandNewImageNotification = new OTAUCommandNewImageNotification();
        otauCommandNewImageNotification.setSuccessFailureListener(this);
        otauCommandNewImageNotification.createRequestPacket();
    }

    /**
     * Handle the command AT_OTAU_IMAGE_INFO_NOTIFY_REQUEST
     */
    public void imageInformation() {
        otauCommandImageInfoNotification = new OTAUCommandImageInfoNotification();
        otauCommandImageInfoNotification.setSuccessFailureListener(this);
        otauCommandImageInfoNotification.createRequestPacket();
    }

    /**
     * Handle the command AT_OTAU_IMAGE_PAGE_END_NOTIFY_REQUEST
     */
    public void imageSectionEndNotification() {
        otauCommandSectionEndNotification = new OTAUCommandImageSectionEndNotification(otauCommandImageInfoNotification);
        otauCommandSectionEndNotification.setSuccessFailureListener(this);
        otauCommandSectionEndNotification.startSectionWrite();
    }

    /**
     * Handle the command AT_OTAU_IMAGE_SECTION_END_NOTIFY_REQUEST
     */
    public void imageEndNotification() {
        otauCommandImageEndNotification = new OTAUCommandImageEndNotification();
        otauCommandImageEndNotification.setSuccessFailureListener(this);
        otauCommandImageEndNotification.createRequestPacket();
    }

    /**
     * Handle the command AT_OTAU_IMAGE_SWITCH_REQUEST
     */
    public void imageSwitchNotification() {
        otauImageSwitchNotification = new OTAUCommandImageSwitchNotification();
        otauImageSwitchNotification.setSuccessFailureListener(this);
        otauImageSwitchNotification.createRequestPacket();
    }

    /**
     * Handle the command AT_OTAU_IMAGE_PAUSE_REQUEST
     */
    public void imagePauseNotification() {
        otauImagePauseNotification = new OTAUCommandImagePause();
        otauImagePauseNotification.setSuccessFailureListener(this);
        otauImagePauseNotification.createRequestPacket();
    }

    /**
     * Handle the command AT_OTAU_IMAGE_RESUME_REQUEST
     */
    public void imageResumeNotification() {
        otauImageResumeNotification = new OTAUCommandImageResume();
        otauImageResumeNotification.setSuccessFailureListener(this);
        otauImageResumeNotification.createRequestPacket();
    }

    /**
     * Handle the command AT_OTAU_RESUME_OTAU_REQUEST
     */
    public void imageResumeRequest() {
        newImageNotification();
    }

    @Override
    public void onSuccess(OTAUCommand commandObj) {
        if (mOTAUPauseFlag) {
            imagePauseNotification();
            mOTAUPauseFlag = false;
        } else {
            if (commandObj == otauGetDeviceInformation) {
                if (otauManager.otauProgressInfoListener != null) {
                    ArrayList<FileModel> fileList = FileReader.getFileList(fileDirectory);
                    mUpgradeFilePath = FileReader.getFileToUpgrade(fileList);
                    if (OTAUCommandGetDeviceInformation.PENDING_FIRMWARE_VERSION != -1) {

                        mResumeFilePath = FileReader.getFileToResume(fileList);
                        if (mResumeFilePath != null && mUpgradeFilePath != null && mUpgradeFilePath != mResumeFilePath) {
                            otauManager.otauProgressInfoListener.OTAUProgressUpdater("Resume and Update Available", 0);
                        } else if (mResumeFilePath != null) {
                            otauManager.otauProgressInfoListener.OTAUProgressUpdater("Resume Available", 0);
                        } else if (mUpgradeFilePath != null) {
                            otauManager.otauProgressInfoListener.OTAUProgressUpdater("Update Available", 0);
                        }

                    } else if (mUpgradeFilePath != null) {
                        otauManager.otauProgressInfoListener.OTAUProgressUpdater("Update Available", 0);
                    } else {
                        otauManager.otauProgressInfoListener.OTAUProgressUpdater("No Update", 0);
                    }

                }
            } else if (commandObj == otauCommandNewImageNotification) {
                if (otauManager.otauProgressInfoListener != null) {
                    imageInformation();
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                otauManager.otauProgressInfoListener.OTAUProgressUpdater("Upgrading...", 0);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);
                }
            } else if (commandObj == otauCommandImageInfoNotification) {
                imageSectionEndNotification();
                mOTAUOngoingFlag = true;
            } else if (commandObj == otauCommandSectionEndNotification) {
                imageEndNotification();
            } else if (commandObj == otauCommandImageEndNotification) {
                otauManager.otauProgressInfoListener.OTAUProgressUpdater("OTAU Completed", 100);
                imageSwitchNotification();
                mOTAUOngoingFlag = false;
                mOTAUPauseFlag = false;
                mOTAUPendingUpdateFlag = false;
            } else if (commandObj == otauImagePauseNotification) {
                if (otauManager.otauProgressInfoListener != null) {
                    otauManager.otauProgressInfoListener.OTAUProgressUpdater("Paused", 0);
                }
            } else if (commandObj == otauImageResumeNotification) {
                if (otauManager.otauProgressInfoListener != null) {
                    // otauManager.otauProgressInfoListener.OTAUProgressUpdater("Resumed", 0);
                    startResumeUpdate();
                }
            }
        }
    }

    @Override
    public void onFailure(OTAUCommand command) {
        Log.e("onFailure", " " + command);
    }

    public void clear() {
        otauManager = null;
    }

    public interface OTAUProgressInfoListener {
        public void OTAUProgressUpdater(String status, float progress);
    }
}
