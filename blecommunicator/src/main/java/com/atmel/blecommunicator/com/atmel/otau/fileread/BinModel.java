/*
 *
 *
 *     BinModel.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.fileread;

import android.util.Log;

import java.math.BigInteger;

public class BinModel {

    public static byte[] binByteArray;
    public static String binHexString;

//    public int headerLength;
//    public int productId;
//
//    public int vendorId;
//    public int flags;
//    public int imageType;
//    public boolean patchFlag;
//    public boolean libraryFlag;
//    public boolean applicationFlag;
//    public boolean appHeaderPatchFlag;
//    public int firmwareVersion;
//
//    public int hardwareVersion;
//    public int hardwareRevision;
//    public int securityLevel;
//    public int numberOfSections;
//
//    public int totalImageSize;
//
//    public int patchSectionId;
//    public int patchSizeL;
//    public int patchStartAddress;
//
//    public int appHeaderPatchSectionId;
//    public int appHeaderPatchSizeM;
//    public int appHeaderPatchStartAddress;
//
//    public int appSectionId;
//    public int appSizeN;
//    public int appStartAddress;
//
//    public long totalImageCRC;
//    public long patchSectionCRC;
//    public long appHeaderPatchSectionCRC;
//    public long appSectionCRC;
//
//    public byte[] patchSectionBytes;
//    public byte[] appHeaderSectionBytes;
//    public byte[] appSectionBytes;

//    public int patchIndex;
//    public int appHeasderIndex;
//    public int appIndex;

    /**
     * Header length
     *
     * @return
     */
    public static int getHeaderLength() {
        String temp = FileReader.getMSB(binHexString.substring(0, 4));
        //Log.d("Header hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("Header String Int>>>", "" + value);
        //Log.d("Bin Length >>>", "" + binHexString.length());
        return value;
    }

    /**
     * Product id
     *
     * @return
     */
    public static int getProductId() {
        String temp = FileReader.getMSB(binHexString.substring(4, 8));
        //Log.d("Product hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("Product String Int>>>", "" + value);
        return value;
    }

    /**
     * Vendor Id
     *
     * @return
     */
    public static int getVendorId() {
        String temp = FileReader.getMSB(binHexString.substring(8, 12));
        //Log.d("Vendor hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("Vendor String Int>>>", "" + value);
        return value;
    }

    /**
     * Flags
     *
     * @return
     */
    public static int getFlagByte() {
        String temp = FileReader.getMSB(binHexString.substring(12, 14));
        //Log.d("Flag hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("Flag String Int>>>", "" + value);
        return value;
    }

    /**
     * Firmware Version
     *
     * @return
     */
    public static int getFirmwareVersion() {
        String temp = FileReader.getMSB(binHexString.substring(14, 22));
        //Log.d("FW hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("FW String Int>>>", "" + value);
        return value;
    }

    /**
     * Firmware Version Major Number
     *
     * @return
     */
    public static int getFirmwareMajorNumber() {
        String major = FileReader.getMSB(binHexString.substring(14, 16));
        int majorNo = Integer.parseInt(major, 16);
        Log.d("FW Major Int>>>", "majorNo > " + majorNo);
        return majorNo;
    }

    /**
     * Firmware Version Minor Number
     *
     * @return
     */
    public static int getFirmwareMinorNumber() {
        String minor = FileReader.getMSB(binHexString.substring(16, 18));
        int minorNo = Integer.parseInt(minor, 16);
        Log.d("FW Minor Int>>>", "minorNo > " + minorNo);
        return minorNo;
    }

    /**
     * Firmware Version Build Number
     *
     * @return
     */
    public static int getFirmwareBuildNumber() {
        String build = FileReader.getMSB(binHexString.substring(18, 22));
        //Log.d("FW hex >>>", temp);
        int buildNo = Integer.parseInt(build, 16);
        Log.d("FW String Int>>>", "buildNo > " + buildNo);
        return buildNo;
    }

    /**
     * Hardware Version
     *
     * @return
     */
    public static int getHardwareVersion() {
        String temp = FileReader.getMSB(binHexString.substring(22, 26));
        //Log.d("HW hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("HW String Int>>>", "" + value);
        return value;
    }

    /**
     * Hardware Version
     *
     * @return
     */
    public static int getHardwareRevision() {
        String temp = FileReader.getMSB(binHexString.substring(26, 28));
        //Log.d("HWR hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("HWR String Int>>>", "" + value);
        return value;
    }

    /**
     * Security Level
     *
     * @return
     */
    public static int getSecurityLevel() {
        String temp = FileReader.getMSB(binHexString.substring(28, 30));
        //Log.d("SL hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("SL String Int>>>", "" + value);
        return value;
    }

    /**
     * Number of Sections
     *
     * @return
     */
    public static int getNumberOfsections() {
        String temp = FileReader.getMSB(binHexString.substring(30, 32));
        //Log.d("NOS hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("NOS String Int>>>", "" + value);
        return value;
    }

    /**
     * Total Image Size
     *
     * @return
     */
    public static int getTotalImagesize() {
        String temp = FileReader.getMSB(binHexString.substring(32, 40));
        //Log.d("TIS hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("TIS String Int>>>", "" + value);
        return value;
    }

    /**
     * Patch Section Id
     *
     * @return
     */
    public static int getPatchSectionId() {
        String temp = FileReader.getMSB(binHexString.substring(40, 42));
        //Log.d("PSI hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("PSI String Int>>>", "" + value);
        return value;
    }

    /**
     * Patch Size(L)
     *
     * @return
     */
    public static int getPatchSize() {
        String temp = FileReader.getMSB(binHexString.substring(42, 48));
        //Log.d("PS hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("PS String Int>>>", "" + value);
        return value;
    }

    /**
     * Patch Start Address
     *
     * @return
     */
    public static int getPatchStartAddress() {
        String temp = FileReader.getMSB(binHexString.substring(48, 56));
        //Log.d("PSA hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("PSA String Int>>>", "" + value);
        return value;
    }

    /**
     * App Header Patch Section Id
     *
     * @return
     */
    public static int getAppHeaderPatchSectionId() {
        String temp = FileReader.getMSB(binHexString.substring(56, 58));
        //Log.d("AHSI hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("AHSI String Int>>>", "" + value);
        return value;
    }

    /**
     * App Header Patch Size(M)
     *
     * @return
     */
    public static int getAppHeaderPatchSize() {
        String temp = FileReader.getMSB(binHexString.substring(58, 64));
        //Log.d("AHPS hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("AHPS String Int>>>", "" + value);
        return value;
    }

    /**
     * App Header Patch Start Address
     *
     * @return
     */
    public static int getAppHeaderPatchStartAddress() {
        String temp = FileReader.getMSB(binHexString.substring(64, 72));
        //Log.d("AHPSA hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("AHPSA String Int>>>", "" + value);
        return value;
    }

    /**
     * App Section Id
     *
     * @return
     */
    public static int getAppSectionId() {
        String temp = FileReader.getMSB(binHexString.substring(72, 74));
        //Log.d("ASI hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("ASI String Int>>>", "" + value);
        return value;
    }

    /**
     * App Size(N)
     *
     * @return
     */
    public static int getAppSize() {
        String temp = FileReader.getMSB(binHexString.substring(74, 80));
        //Log.d("AS hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("AS String Int>>>", "" + value);
        return value;
    }

    /**
     * App Start Address
     *
     * @return
     */
    public static int getAppStartAddress() {
        String temp = FileReader.getMSB(binHexString.substring(80, 88));
        //Log.d("ASA hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("ASA String Int>>>", "" + value);
        return value;
    }

    /**
     * Total Image CRC
     *
     * @return
     */
    public static long getTotalImageCRC() {
        String temp = FileReader.getMSB(binHexString.substring(88, 96));
        long value = 0;
        //Log.d("TICRC hex >>>", temp);
        try {
            value = Long.parseLong(temp, 16);
            BigInteger.valueOf(value);
            //Log.d("TICRC String Int>>>", "" + value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Patch Section CRC
     *
     * @return
     */
    public static long getPatchSectionCRC() {
        String temp = FileReader.getMSB(binHexString.substring(96, 104));
        //Log.d("PSCRC hex >>>", temp);
        long value = Long.parseLong(temp, 16);
        //Log.d("PSCRC String Int>>>", "" + value);
        return value;
    }

    /**
     * App Header Patch Section CRC
     *
     * @return
     */
    public static long getAppHaderPatchCRC() {
        String temp = FileReader.getMSB(binHexString.substring(104, 112));
        //Log.d("AHPCRC hex >>>", temp);
        long value = Long.parseLong(temp, 16);
        //Log.d("AHPCRC String Int>>>", "" + value);
        return value;
    }

    /**
     * App Section CRC
     *
     * @return
     */
    public static long getAppSectionCRC() {
        String temp = FileReader.getMSB(binHexString.substring(112, 120));
        //Log.d("ASCRC hex >>>", temp);
        long value = Long.parseLong(temp, 16);
        //Log.d("ASCRC String Int>>>", "" + value);
        return value;
    }

    /**
     * Patch Section Bytes
     *
     * @return
     */
    public static byte[] getPatchSectionBytes() {
        byte[] tempByte = new byte[getPatchSize()];
        for (int i = 0; i < getPatchSize(); i++) {
            tempByte[i] = binByteArray[60 + i];
        }
        //Log.d("PSBytes String Int>>>", FileReader.bytesToHex(tempByte));
        return tempByte;
    }

    /**
     * App Header Patch Section Bytes
     *
     * @return
     */
    public static byte[] getAppHeaderPatchSectionBytes() {
        byte[] tempByte = new byte[getAppHeaderPatchSize()];
        for (int i = 0; i < getAppHeaderPatchSize(); i++) {
            tempByte[i] = binByteArray[(60 + getPatchSize()) + i];
        }
        //Log.d("AHPSBytes String Int>>>", "" + FileReader.bytesToHex(tempByte));
        return tempByte;
    }

    /**
     * App Section Bytes
     *
     * @return
     */
    public static byte[] getAppSectionBytes() {
        byte[] tempByte = new byte[getAppSize()];
        for (int i = 0; i < getAppSize(); i++) {
            tempByte[i] = binByteArray[(60 + getPatchSize() + getAppHeaderPatchSize()) + i];
        }
        //Log.d("ASBytes String Int>>>", "" + FileReader.bytesToHex(tempByte));
        return tempByte;
    }

    /**
     * Patch Index
     *
     * @return
     */
    public static int getPatchIndex() {

        return 0;
    }

    /**
     * AppHeader Index
     *
     * @return
     */
    public static int getAppHeaderIndex() {

        return getPatchIndex() + getPatchSize();
    }

    /**
     * App Index
     *
     * @return
     */
    public static int getAppIndex() {

        return getAppHeaderIndex() + getAppHeaderPatchSize();
    }


}