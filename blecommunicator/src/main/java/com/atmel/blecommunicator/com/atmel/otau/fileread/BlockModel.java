/*
 *
 *
 *     BlockModel.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.fileread;

import java.util.Arrays;

public class BlockModel {

    public int blockNo;
    public byte[] blockData;

    public PageModel pageModel;

    public BlockModel(PageModel page, int blockNumber) {
        this.pageModel = page;
        int startIndex = (blockNumber * page.blockSize);
        int lastIndex = startIndex + page.blockSize;
        this.blockData = Arrays.copyOfRange(page.pageData, startIndex, lastIndex);
        blockNo = blockNumber;
    }

    public BlockModel nextBlock() {
        if (blockNo + 1 < pageModel.numberOFBlocks) {
            return new BlockModel(pageModel, blockNo + 1);
        }
        return null;
    }

    public float progressFromPage() {
        float sectionProgress = 0;
        float imageProgress = 0;
        float pageProgress = 0;
        pageProgress = (blockNo * pageModel.blockSize);
        sectionProgress = (pageProgress + pageModel.pageNo) * pageModel.sectionModel.pageSize;
        imageProgress = (((pageModel.sectionModel.index) + sectionProgress) / BinModel.getTotalImagesize());
        return (imageProgress);
    }

}
