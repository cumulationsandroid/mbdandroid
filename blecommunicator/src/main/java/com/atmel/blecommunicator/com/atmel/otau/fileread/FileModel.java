/*
 *
 *
 *     FileModel.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.fileread;

import java.util.Comparator;

public class FileModel {

    /*Comparator for sorting the list by roll no*/
    public static Comparator<FileModel> fileComparator = new Comparator<FileModel>() {

        public int compare(FileModel f1, FileModel f2) {

            int fwbuild1 = f1.fwBuild;
            int fwbuild2 = f2.fwBuild;

	   /*For ascending order*/
            //return rollno1 - rollno2;

	   /*For descending order*/
            return fwbuild2 - fwbuild1;
        }
    };
    public String filepath;
    public int productId;
    public int vendorId;
    public int hwVersion;
    public int hwRevision;
    public int fwVersion;
    public int fwMajor;
    public int fwMinor;
    public int fwBuild;

    public String getFileVersion() {
        return fwMajor + "." + fwMinor + "." + fwBuild;
    }

}
