/*
 *
 *
 *     FileReader.java
 *   
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.fileread;

import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.atmel.blecommunicator.com.atmel.otau.commands.OTAUCommandGetDeviceInformation;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.NoSuchElementException;

public class FileReader {

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static FileModel mFile = new FileModel();
    private static String mOtaFileSelected;
    private static String TAG = "FileReader.class";

    /**
     * Pass the file selected information to ota module
     *
     * @param filename - filename selected for ota transfer
     */
    public static void setFileSelected(String filename) {
        mOtaFileSelected = filename;
        Log.d("BYTE STRING HEX>>> ", FileReader.bytesToHex(FileReader.readAlternateImpl(mOtaFileSelected)));
        BinModel.binByteArray = FileReader.readAlternateImpl(mOtaFileSelected);
        BinModel.binHexString = FileReader.bytesToHex(FileReader.readAlternateImpl(mOtaFileSelected));
    }


    /**
     * Read the given binary file, and return its contents as a byte array.
     */
    public static byte[] read(String fileName) {
        log("Reading in binary file named : " + fileName);
        File file = new File(fileName);
        log("File size: " + file.length());
        byte[] result = new byte[(int) file.length()];
        try {
            InputStream input = null;
            try {
                int totalBytesRead = 0;
                input = new BufferedInputStream(new FileInputStream(file));

                // read number of bytes available
                while (totalBytesRead < result.length) {
                    int bytesRemaining = result.length - totalBytesRead;
                    //input.read() returns -1, 0, or more :
                    int bytesRead = input.read(result, totalBytesRead, bytesRemaining);
                    if (bytesRead > 0) {
                        totalBytesRead = totalBytesRead + bytesRead;
                    }
                }
        /*
         the above style is a bit tricky: it places bytes into the 'result' array;
         'result' is an output parameter;
         the while loop usually has a single iteration only.
        */
                log("Num bytes read: " + totalBytesRead);
            } finally {
                log("Closing input stream.");
                input.close();
            }
        } catch (FileNotFoundException ex) {
            log("File not found.");
        } catch (IOException ex) {
            log(ex);
        }
        return result;
    }

    /**
     * Read the given binary file, and return its contents as a byte array.
     */
    public static byte[] readAlternateImpl(String aInputFileName) {
        log("Reading in binary file named : " + aInputFileName);
        File file = new File(aInputFileName);
        log("File size: " + file.length());
        byte[] result = null;
        try {
            InputStream input = new BufferedInputStream(new FileInputStream(file));
            result = readAndClose(input);
        } catch (FileNotFoundException ex) {
            log(ex);
        }
        return result;
    }

    /**
     * Read an input stream, and return it as a byte array.
     * Sometimes the source of bytes is an input stream instead of a file.
     * This implementation closes aInput after it's read.
     */
    public static byte[] readAndClose(InputStream aInput) {
        //carries the data from input to output :
        byte[] bucket = new byte[32 * 1024];
        ByteArrayOutputStream result = null;
        try {
            try {
                //Use buffering? No. Buffering avoids costly access to disk or network;
                //buffering to an in-memory stream makes no sense.
                result = new ByteArrayOutputStream(bucket.length);
                int bytesRead = 0;
                while (bytesRead != -1) {
                    //aInput.read() returns -1, 0, or more :
                    bytesRead = aInput.read(bucket);
                    if (bytesRead > 0) {
                        result.write(bucket, 0, bytesRead);
                    }
                }
            } finally {
                aInput.close();
                //result.close(); this is a no-operation for ByteArrayOutputStream
            }
        } catch (IOException ex) {
            log(ex);
        }
        return result.toByteArray();
    }

//    /**
//     * Read the given binary file, and return its contents as a byte array.
//     */
//    public static boolean readHeader(String aInputFileName) {
//        //log("Reading header binary file named : " + aInputFileName);
//        File file = new File(aInputFileName);
//        log("Header file size: " + file.length());
//        byte[] result = null;
//        try {
//            InputStream input = new BufferedInputStream(new FileInputStream(file));
//            result = readAndClose(input);
//            HeaderModel.headerByteArray = result;
//            HeaderModel.headerHexString = bytesToHex(result);
////            log("Current dev info > PID > " + OTAUCommandGetDeviceInformation.PRODUCT_ID + " VID > " + OTAUCommandGetDeviceInformation.VENDOR_ID +
////                    "HV > " + OTAUCommandGetDeviceInformation.HARDWARE_VERSION + " HREV > " + OTAUCommandGetDeviceInformation.HARDWARE_REVISION
////                    + "FV > " + OTAUCommandGetDeviceInformation.FIRMWARE_VERSION);
////            log("Read dev info > FWMAJ > " + HeaderModel.getFirmwareMajorNumber() + " FWMIN > " + HeaderModel.getFirmwareMinorNumber()
////                    + " FWBUILD >" + HeaderModel.getFirmwareBuildNumber());
//////            log("Read File info > PID > " + HeaderModel.getProductId() + " VID > " + HeaderModel.getVendorId() +
//////                    "HV > " + HeaderModel.getHardwareVersion() + " HREV > " + HeaderModel.getHardwareRevision() + "FV > " + HeaderModel.getFirmwareVersion());
//            if (HeaderModel.getProductId() == OTAUCommandGetDeviceInformation.PRODUCT_ID
//                    && HeaderModel.getVendorId() == OTAUCommandGetDeviceInformation.VENDOR_ID
//                    && HeaderModel.getHardwareVersion() == OTAUCommandGetDeviceInformation.HARDWARE_VERSION
//                    && HeaderModel.getHardwareRevision() == OTAUCommandGetDeviceInformation.HARDWARE_REVISION
//                    && (HeaderModel.getFirmwareMajorNumber() >= OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MAJOR
//                    || HeaderModel.getFirmwareMinorNumber() >= OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MINOR
//                    || HeaderModel.getFirmwareBuildNumber() >= OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_BUILD)) {
//                return true;
//            } else {
//                return false;
//            }
//        } catch (FileNotFoundException ex) {
//            log(ex);
//        }
//        return false;
//    }

    /**
     * Read the given binary file, and return its contents as a byte array.
     */
    public static FileModel readFirmwareVersion(String filepath) {
        File file = new File(filepath);
        byte[] result = null;
        FileModel model = new FileModel();
        try {
            InputStream input = new BufferedInputStream(new FileInputStream(file));
            result = readAndClose(input);
            HeaderModel.headerByteArray = result;
            HeaderModel.headerHexString = bytesToHex(result);
            model.filepath = filepath;
            model.productId = HeaderModel.getProductId();
            model.vendorId = HeaderModel.getVendorId();
            model.hwVersion = HeaderModel.getHardwareVersion();
            model.hwRevision = HeaderModel.getHardwareRevision();
            model.fwVersion = HeaderModel.getFirmwareVersion();
            model.fwMajor = HeaderModel.getFirmwareMajorNumber();
            model.fwMinor = HeaderModel.getFirmwareMinorNumber();
            model.fwBuild = HeaderModel.getFirmwareBuildNumber();
            return model;
        } catch (FileNotFoundException ex) {
            log(ex);
            return null;
        }
    }

    /**
     * Method to search phone/directory for the .bin files
     */
    public static boolean searchBinaryFiles(File directory) {
        if (directory.exists()) {
            ArrayList<FileModel> mFileList = new ArrayList<>();
            String filePattern = "bin";
            File[] allFilesList = directory.listFiles();
            for (int pos = 0; pos < allFilesList.length; pos++) {
                File analyseFile = allFilesList[pos];
                if (analyseFile != null) {
                    if (analyseFile.isDirectory()) {
                        searchBinaryFiles(analyseFile);
                    } else {
                        Uri selectedUri = Uri.fromFile(analyseFile);
                        String fileExtension
                                = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                        if (fileExtension.equalsIgnoreCase(filePattern)) {
                            mFileList.add(readFirmwareVersion(analyseFile.getPath()));
                        }
                    }
                }
            }

            try {
                String filepath = compareFileheader(mFileList);
                boolean headerFlag = filepath.isEmpty();
                if (!headerFlag) {
                    setFileSelected(filepath);
                    return true;
                } else {
                    return false;
                }
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                return false;
            } catch (NullPointerException e) {
                e.printStackTrace();
                return false;
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    private static String compareFileheader(ArrayList<FileModel> list) {
        ArrayList<Integer> fwbuildcache = new ArrayList<>();
        Collections.sort(list, new Comparator<FileModel>() {
            @Override
            public int compare(FileModel lhs, FileModel rhs) {
                return lhs.getFileVersion().compareTo(rhs.getFileVersion());
            }
        });
        if (!list.isEmpty()) {
            FileModel fileModel = list.get(list.size() - 1);
            if (fileModel.getFileVersion().compareTo(OTAUCommandGetDeviceInformation.getFileVersion()) > 0) {
                mFile = fileModel;
                return fileModel.filepath;
            }
        }
        return null;
/*
        for (FileModel file : list) {
            log("Current dev info > PID > " + OTAUCommandGetDeviceInformation.PRODUCT_ID + " VID > " + OTAUCommandGetDeviceInformation.VENDOR_ID +
                    " HV > " + OTAUCommandGetDeviceInformation.HARDWARE_VERSION + " HREV > " + OTAUCommandGetDeviceInformation.HARDWARE_REVISION
                    + "FV > " + OTAUCommandGetDeviceInformation.FIRMWARE_VERSION + "> FWMAJ > " + OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MAJOR
                    + " FWMIN > " + OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MINOR + " FWBUILD > " + OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_BUILD);
            log("Read File info > PID > " + file.productId + " VID > " + file.vendorId +
                    "HV > " + file.hwVersion + " HREV > " + file.hwRevision + " FV > " + file.fwVersion
                    + "> FWMAJ > " + file.fwMajor + " FWMIN > " + file.fwMinor
                    + " FWBUILD >" + file.fwBuild);
            if ((file.productId == OTAUCommandGetDeviceInformation.PRODUCT_ID
                    && file.vendorId == OTAUCommandGetDeviceInformation.VENDOR_ID
                    && file.hwVersion == OTAUCommandGetDeviceInformation.HARDWARE_VERSION
                    && file.hwRevision == OTAUCommandGetDeviceInformation.HARDWARE_REVISION)
                    && file.fwVersion > OTAUCommandGetDeviceInformation.FIRMWARE_VERSION) {
                if (file.fwMajor >= OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MAJOR
                        && file.fwMinor >= OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MINOR) {
                    fwbuildcache.add(file.fwBuild);
                    log("Sorted Files>> " + file.filepath);
                }
//                if(file.fwMajor == OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MAJOR){
//                    if(file.fwMinor == OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_MINOR){
//                        if(file.fwBuild > OTAUCommandGetDeviceInformation.FIRMWARE_VERSION_BUILD){
//                            fwbuildcache.add(file.);
//                        }
//                    }
//                }
                continue;
            }
        }
        Integer fileBuild = -1;
        if (!fwbuildcache.isEmpty()) {
            fileBuild = Collections.max(fwbuildcache);
        }
        for (FileModel file : list) {
            if (file.fwBuild == fileBuild) {
                mFile = file;
                return file.filepath;
            }
        }
        return null;*/
    }

    public static ArrayList<FileModel> getFileList(File directory) {
        ArrayList<FileModel> mFileList = new ArrayList<>();
        if (directory.exists()) {

            String filePattern = "bin";
            File[] allFilesList = directory.listFiles();
            for (int pos = 0; pos < allFilesList.length; pos++) {
                File analyseFile = allFilesList[pos];
                if (analyseFile != null) {
                    if (analyseFile.isDirectory()) {
                        mFileList.addAll(getFileList(analyseFile));
                    } else {
                        Uri selectedUri = Uri.fromFile(analyseFile);
                        String fileExtension
                                = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                        if (fileExtension.equalsIgnoreCase(filePattern)) {
                            mFileList.add(readFirmwareVersion(analyseFile.getPath()));
                        }
                    }
                }
            }
        }
        return mFileList;
    }

    /**
     * Method to search phone/directory for the .bin files
     */
    public static boolean searchPendingFile(File directory) {
        if (directory.exists()) {
            ArrayList<FileModel> mFileList = new ArrayList<>();
            String filePattern = "bin";
            File[] allFilesList = directory.listFiles();
            for (int pos = 0; pos < allFilesList.length; pos++) {
                File analyseFile = allFilesList[pos];
                if (analyseFile != null) {
                    if (analyseFile.isDirectory()) {
                        searchPendingFile(analyseFile);
                    } else {
                        Uri selectedUri = Uri.fromFile(analyseFile);
                        String fileExtension
                                = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                        if (fileExtension.equalsIgnoreCase(filePattern)) {
                            mFileList.add(readFirmwareVersion(analyseFile.getPath()));
                        }
                    }
                }
            }

            try {
                String filepath = pendingFileSearch(mFileList);
                boolean headerFlag = filepath.isEmpty();
                if (!headerFlag) {
                    setFileSelected(filepath);
                    return true;
                } else {
                    return false;
                }
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                return false;
            } catch (NullPointerException e) {
                e.printStackTrace();
                return false;
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    private static String pendingFileSearch(ArrayList<FileModel> list) {
        for (FileModel file : list) {
            if ((file.productId == OTAUCommandGetDeviceInformation.PRODUCT_ID
                    && file.vendorId == OTAUCommandGetDeviceInformation.VENDOR_ID
                    && file.hwVersion == OTAUCommandGetDeviceInformation.HARDWARE_VERSION
                    && file.hwRevision == OTAUCommandGetDeviceInformation.HARDWARE_REVISION)
                    && file.fwVersion == OTAUCommandGetDeviceInformation.PENDING_FIRMWARE_VERSION
                    && file.fwMajor == OTAUCommandGetDeviceInformation.PENDING_FIRMWARE_VERSION_MAJOR
                    && file.fwMinor == OTAUCommandGetDeviceInformation.PENDING_FIRMWARE_VERSION_MINOR
                    && file.fwBuild == OTAUCommandGetDeviceInformation.PENDING_FIRMWARE_VERSION_BUILD) {
                return file.filepath;
            }
        }
        return null;
    }

    private static void log(Object aThing) {
        Log.d(TAG, String.valueOf(aThing));
    }

    public static String asciiToHex(String asciiValue) {
        char[] chars = asciiValue.toCharArray();
        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            hex.append(Integer.toHexString((int) chars[i]));
        }
        return hex.toString();
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String byteArraytoHex(byte[] bytes) {
        if (bytes != null) {
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(String.format("%02X ", b));
            }
            return sb.toString();
        }
        return "";
    }

    public static String getMSB(String string) {
        StringBuilder msbString = new StringBuilder();

        for (int i = string.length(); i > 0; i -= 2) {
            String str = string.substring(i - 2, i);
            msbString.append(str);
        }
        return msbString.toString();
    }

    public static byte[] hexToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private static int getBit(byte[] data, int pos) {
        int posByte = pos / 8;
        int posBit = pos % 8;
        byte valByte = data[posByte];
        int valInt = valByte >> (8 - (posBit + 1)) & 0x0001;
        return valInt;
    }

    public static String getFileToResume(ArrayList<FileModel> fileDirectory) {
        return pendingFileSearch(fileDirectory);
    }

    public static String getFileToUpgrade(ArrayList<FileModel> fileDirectory) {
        return compareFileheader(fileDirectory);
    }

    /**
     * Write a byte array to the given file.
     * Writing binary data is significantly simpler than reading it.
     */
    void write(byte[] aInput, String aOutputFileName) {
        log("Writing binary file...");
        try {
            OutputStream output = null;
            try {
                output = new BufferedOutputStream(new FileOutputStream(aOutputFileName));
                output.write(aInput);
            } finally {
                output.close();
            }
        } catch (FileNotFoundException ex) {
            log("File not found.");
        } catch (IOException ex) {
            log(ex);
        }
    }
}