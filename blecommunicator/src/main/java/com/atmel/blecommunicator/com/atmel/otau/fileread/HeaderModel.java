/*
 *
 *
 *     HeaderModel.java
 *   
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.fileread;

public class HeaderModel {

    public static byte[] headerByteArray;
    public static String headerHexString;

    public int checkHeaderLength;
    public int productId;
    public int vendorId;
    public int firmwareVersion;
    public int hardwareVersion;
    public int hardwareRevision;
    public int numberOfSections;

    public static String getFileVersion() {
        return getFirmwareMajorNumber() + "." + getFirmwareMinorNumber() + "." + getFirmwareBuildNumber();
    }

    /**
     * Header length
     *
     * @return
     */
    public static int getHeaderLength() {
        String temp = FileReader.getMSB(headerHexString.substring(0, 4));
        //Log.d("Header hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("Header String Int>>>", "" + value);
        //Log.d("Bin Length >>>", "" + binHexString.length());
        return value;
    }

    /**
     * Product id
     *
     * @return
     */
    public static int getProductId() {
        String temp = FileReader.getMSB(headerHexString.substring(4, 8));
        //Log.d("Product hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("Product String Int>>>", "" + value);
        return value;
    }

    /**
     * Vendor Id
     *
     * @return
     */
    public static int getVendorId() {
        String temp = FileReader.getMSB(headerHexString.substring(8, 12));
        //Log.d("Vendor hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("Vendor String Int>>>", "" + value);
        return value;
    }


    /**
     * Firmware Version
     *
     * @return
     */
    public static int getFirmwareVersion() {
        String temp = FileReader.getMSB(headerHexString.substring(14, 22));
        //Log.d("FW hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("FW String Int>>>", "" + value);
        return value;
    }

    /**
     * Firmware Version Major Number
     *
     * @return
     */
    public static int getFirmwareMajorNumber() {
        String major = FileReader.getMSB(headerHexString.substring(14, 16));
        int majorNo = Integer.parseInt(major, 16);
        //Log.d("FW Major Int>>>", "majorNo > " + majorNo );
        return majorNo;
    }

    /**
     * Firmware Version Minor Number
     *
     * @return
     */
    public static int getFirmwareMinorNumber() {
        String minor = FileReader.getMSB(headerHexString.substring(16, 18));
        String build = FileReader.getMSB(headerHexString.substring(18, 22));
        int minorNo = Integer.parseInt(minor, 16);
        //Log.d("FW Minor Int>>>",  "minorNo > " + minorNo );
        return minorNo;
    }

    /**
     * Firmware Version Build Number
     *
     * @return
     */
    public static int getFirmwareBuildNumber() {
        String build = FileReader.getMSB(headerHexString.substring(18, 22));
        //Log.d("FW hex >>>", temp);
        int buildNo = Integer.parseInt(build, 16);
        // Log.d("FW String Int>>>",  "buildNo > " + buildNo);
        return buildNo;
    }

    /**
     * Hardware Version
     *
     * @return
     */
    public static int getHardwareVersion() {
        String temp = FileReader.getMSB(headerHexString.substring(22, 26));
        //Log.d("HW hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("HW String Int>>>", "" + value);
        return value;
    }

    /**
     * Hardware Version
     *
     * @return
     */
    public static int getHardwareRevision() {
        String temp = FileReader.getMSB(headerHexString.substring(26, 28));
        //Log.d("HWR hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("HWR String Int>>>", "" + value);
        return value;
    }

    /**
     * Number of Sections
     *
     * @return
     */
    public static int getNumberOfsections() {
        String temp = FileReader.getMSB(headerHexString.substring(30, 32));
        //Log.d("NOS hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("NOS String Int>>>", "" + value);
        return value;
    }

    /**
     * Total Image Size
     *
     * @return
     */
    public static int getTotalImagesize() {
        String temp = FileReader.getMSB(headerHexString.substring(32, 40));
        //Log.d("TIS hex >>>", temp);
        int value = Integer.parseInt(temp, 16);
        //Log.d("TIS String Int>>>", "" + value);
        return value;
    }

}