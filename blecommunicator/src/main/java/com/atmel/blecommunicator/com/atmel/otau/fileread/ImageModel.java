/*
 *
 *
 *     ImageModel.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.fileread;

public class ImageModel {

    private int headerLength;
    private int productId;

    private int vendorId;
    private int flags;
    private int imageType;
    private boolean patchFlag;
    private boolean libraryFlag;
    private boolean applicationFlag;
    private boolean appHeaderPatchFlag;
    private int firmwareVersion;

    private int hardwareVersion;
    private int hardwareRevision;
    private int securityLevel;
    private int numberOfSections;

    private int totalImageSize;

    private int patchSectionId;
    private int patchSizeL;
    private int patchStartAddress;

    private int appHeaderPatchSectionId;
    private int appHeaderPatchSizeM;
    private int appHeaderPatchStartAddress;

    private int appSectionId;
    private int appSizeN;
    private int appStartAddress;

    private long totalImageCRC;
    private long patchSectionCRC;
    private long appHeaderPatchSectionCRC;
    private long appSectionCRC;

    private byte[] patchSectionBytes;
    private byte[] appHeaderSectionBytes;
    private byte[] appSectionBytes;

    public int getHeaderLength() {
        return headerLength;
    }

    public void setHeaderLength(int hhb) {
        this.headerLength = headerLength;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public int getImageType() {
        return imageType;
    }

    public void setImageType(int imageType) {
        this.imageType = imageType;
    }

    public boolean isPatchFlag() {
        return patchFlag;
    }

    public void setPatchFlag(boolean patchFlag) {
        this.patchFlag = patchFlag;
    }

    public boolean isLibraryFlag() {
        return libraryFlag;
    }

    public void setLibraryFlag(boolean libraryFlag) {
        this.libraryFlag = libraryFlag;
    }

    public boolean isApplicationFlag() {
        return applicationFlag;
    }

    public void setApplicationFlag(boolean applicationFlag) {
        this.applicationFlag = applicationFlag;
    }

    public boolean isAppHeaderPatchFlag() {
        return appHeaderPatchFlag;
    }

    public void setAppHeaderPatchFlag(boolean appHeaderPatchFlag) {
        this.appHeaderPatchFlag = appHeaderPatchFlag;
    }

    public int getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(int firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public int getHardwareVersion() {
        return hardwareVersion;
    }

    public void setHardwareVersion(int hardwareVersion) {
        this.hardwareVersion = hardwareVersion;
    }

    public int getHardwareRevision() {
        return hardwareRevision;
    }

    public void setHardwareRevision(int hardwareRevision) {
        this.hardwareRevision = hardwareRevision;
    }

    public int getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(int securityLevel) {
        this.securityLevel = securityLevel;
    }

    public int getNumberOfSections() {
        return numberOfSections;
    }

    public void setNumberOfSections(int numberOfSections) {
        this.numberOfSections = numberOfSections;
    }

    public int getTotalImageSize() {
        return totalImageSize;
    }

    public void setTotalImageSize(int totalImageSize) {
        this.totalImageSize = totalImageSize;
    }

    public int getPatchSectionId() {
        return patchSectionId;
    }

    public void setPatchSectionId(int patchSectionId) {
        this.patchSectionId = patchSectionId;
    }

    public int getPatchSizeL() {
        return patchSizeL;
    }

    public void setPatchSizeL(int patchSizeL) {
        this.patchSizeL = patchSizeL;
    }

    public int getPatchStartAddress() {
        return patchStartAddress;
    }

    public void setPatchStartAddress(int patchStartAddress) {
        this.patchStartAddress = patchStartAddress;
    }

    public int getAppHeaderPatchSectionId() {
        return appHeaderPatchSectionId;
    }

    public void setAppHeaderPatchSectionId(int appHeaderPatchSectionId) {
        this.appHeaderPatchSectionId = appHeaderPatchSectionId;
    }

    public int getAppHeaderPatchSizeM() {
        return appHeaderPatchSizeM;
    }

    public void setAppHeaderPatchSizeM(int appHeaderPatchSizeM) {
        this.appHeaderPatchSizeM = appHeaderPatchSizeM;
    }

    public int getAppHeaderPatchStartAddress() {
        return appHeaderPatchStartAddress;
    }

    public void setAppHeaderPatchStartAddress(int appHeaderPatchStartAddress) {
        this.appHeaderPatchStartAddress = appHeaderPatchStartAddress;
    }

    public int getAppSectionId() {
        return appSectionId;
    }

    public void setAppSectionId(int appSectionId) {
        this.appSectionId = appSectionId;
    }

    public int getAppSizeN() {
        return appSizeN;
    }

    public void setAppSizeN(int appSizeN) {
        this.appSizeN = appSizeN;
    }

    public int getAppStartAddress() {
        return appStartAddress;
    }

    public void setAppStartAddress(int appStartAddress) {
        this.appStartAddress = appStartAddress;
    }

    public long getTotalImageCRC() {
        return totalImageCRC;
    }

    public void setTotalImageCRC(long totalImageCRC) {
        this.totalImageCRC = totalImageCRC;
    }

    public long getPatchSectionCRC() {
        return patchSectionCRC;
    }

    public void setPatchSectionCRC(long patchSectionCRC) {
        this.patchSectionCRC = patchSectionCRC;
    }

    public long getAppHeaderPatchSectionCRC() {
        return appHeaderPatchSectionCRC;
    }

    public void setAppHeaderPatchSectionCRC(long appHeaderPatchSectionCRC) {
        this.appHeaderPatchSectionCRC = appHeaderPatchSectionCRC;
    }

    public long getAppSectionCRC() {
        return appSectionCRC;
    }

    public void setAppSectionCRC(long appSectionCRC) {
        this.appSectionCRC = appSectionCRC;
    }

    public byte[] getPatchSectionBytes() {
        return patchSectionBytes;
    }

    public void setPatchSectionBytes(byte[] patchSectionBytes) {
        this.patchSectionBytes = patchSectionBytes;
    }

    public byte[] getAppHeaderSectionBytes() {
        return appHeaderSectionBytes;
    }

    public void setAppHeaderSectionBytes(byte[] appHeaderSectionBytes) {
        this.appHeaderSectionBytes = appHeaderSectionBytes;
    }

    public byte[] getAppSectionBytes() {
        return appSectionBytes;
    }

    public void setAppSectionBytes(byte[] appSectionBytes) {
        this.appSectionBytes = appSectionBytes;
    }


}