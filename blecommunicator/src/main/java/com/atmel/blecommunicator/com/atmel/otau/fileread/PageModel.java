/*
 *
 *
 *     PageModel.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.fileread;

import java.util.Arrays;

public class PageModel {

    public int pageNo;
    public byte[] pageData;

    public int numberOFBlocks;
    public SectionModel sectionModel;

    public int blockSize;

    public PageModel(SectionModel section, int pageNumber) {
        this.sectionModel = section;
        int startIndex = (pageNumber * section.pageSize);
        int lastIndex = startIndex + section.pageSize;
        if (lastIndex > section.sectionData.length) {
            lastIndex = (section.sectionData.length);
        }
        this.pageData = Arrays.copyOfRange(section.sectionData, startIndex, lastIndex);
        pageNo = pageNumber;
    }

    public void setBlockSize(int blockSize) {
        this.blockSize = blockSize;
        this.numberOFBlocks = pageData.length / blockSize;
    }

    public PageModel nextPage() {
        if (pageNo + 1 < sectionModel.numberOFPages) {
            return new PageModel(sectionModel, pageNo + 1);
        }
        return null;
    }

    public float progressFromPage() {
        float sectionProgress = 0;
        float imageProgress = 0;
        sectionProgress = (pageNo * sectionModel.pageSize);
        imageProgress = (((sectionModel.index) + sectionProgress) / BinModel.getTotalImagesize());
        return (imageProgress);
    }
}
