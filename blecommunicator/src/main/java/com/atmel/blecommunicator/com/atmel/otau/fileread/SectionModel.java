/*
 *
 *
 *     SectionModel.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.fileread;

import android.util.Log;

public class SectionModel {

    public byte[] sectionData;
    public int sectionId;
    public int numberOFPages;
    public int index;
    public int pageSize;

    public SectionModel(int sectionId, int pageSize) {
        switch (sectionId) {
            case 1:
                sectionData = BinModel.getPatchSectionBytes();
                index = BinModel.getPatchIndex();
                break;
            case 2:
                sectionData = BinModel.getAppHeaderPatchSectionBytes();
                index = BinModel.getAppHeaderIndex();
                break;
            case 3:
                sectionData = BinModel.getAppSectionBytes();
                index = BinModel.getAppIndex();
                break;
        }
        this.sectionId = sectionId;
        this.pageSize = pageSize;
        this.numberOFPages = (int) Math.ceil((float) sectionData.length / (float) pageSize);
        Log.e(" Section pages>> ", " " + numberOFPages);
    }

    public SectionModel nextSection() {
        switch (sectionId) {
            case 1:
                return new SectionModel(2, pageSize);
            case 2:
                return new SectionModel(3, pageSize);
            case 3:
                break;
        }
        return null;
    }

}