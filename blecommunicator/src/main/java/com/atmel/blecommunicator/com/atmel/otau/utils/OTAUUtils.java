/*
 *
 *
 *     OTAUUtils.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.otau.utils;

public class OTAUUtils {

    //Response Code
    public static final byte GET_DEVICE_INFO_RESP = (byte) 0x1C;
    public static final byte IMAGE_NOTIFY_RESP = (byte) 0x02;
    public static final byte IMAGE_INFORMATION_RESP = (byte) 0x04;
    public static final byte IMAGE_INFORMATION_RESP_PAGE = 111;
    public static final byte IMAGE_INFORMATION_RESP_BLOCK = 112;
    public static final byte PAGE_DATA_NOTIFY_RESP = (byte) 0x07;

    //OTAU Command List
    public static final byte AT_OTAU_IMAGE_NOTIFY_REQUEST = 0x01;
    public static final byte AT_OTAU_IMAGE_NOTIFY_RESP = 0x02;
    public static final byte AT_OTAU_IMAGE_INFO_NOTIFY_REQUEST = 0x03;
    public static final byte AT_OTAU_IMAGE_INFO_NOTIFY_RESP = 0x04;
    public static final byte AT_OTAU_START_IMAGE_DOWNLOAD = 0x05;
    public static final byte AT_OTAU_PAGE_DATA_NOTIFY_REQUEST = 0x06;
    public static final byte AT_OTAU_PAGE_DATA_NOTIFY_RESP = 0x07;

    public static final byte AT_OTAU_BLOCK_DATA_NOTIFY_REQUEST = 0x08;
    public static final byte AT_OTAU_BLOCK_DATA_NOTIFY_RESP = 0x09;
    public static final byte AT_OTAU_ABORT_OTAU = 0x0A;
    public static final byte AT_OTAU_PAUSE_OTAU_REQUEST = 0x0B;
    public static final byte AT_OTAU_PAUSE_OTAU_RESP = 0x0C;
    public static final byte AT_OTAU_RESUME_OTAU_REQUEST = 0x0D;
    public static final byte AT_OTAU_RESUME_OTAU_RESP = 0x0E;

    public static final byte AT_OTAU_FORCE_UDPATE_REQUEST = 0x0F;
    public static final byte AT_OTAU_FORCE_UDPATE_RESP = 0x10;
    public static final byte AT_OTAU_IMAGE_SWITCH_REQUEST = 0x11;
    public static final byte AT_OTAU_IMAGE_SWITCH_RESP = 0x12;
    public static final byte AT_OTAU_IMAGE_END_NOTIFY_REQUEST = 0x13;
    public static final byte AT_OTAU_IMAGE_END_NOTIFY_RESP = 0x14;

    public static final byte AT_OTAU_MISSED_BLOCK_REQUEST = 0x15;
    public static final byte AT_OTAU_MISSED_BLOCK_RESP = 0x16;
    public static final byte AT_OTAU_IMAGE_UPDATE_CHECK = 0x17;
    public static final byte AT_OTAU_IMAGE_PAGE_END_NOTIFY_REQUEST = 0x18;
    public static final byte AT_OTAU_IMAGE_SECTION_END_NOTIFY_REQUEST = 0x19;
    public static final byte AT_OTAU_IMAGE_SECTION_END_NOTIFY_RESP = 0x1A;
    public static final byte AT_OTAU_GET_DEVICE_INFO_REQUEST = 0x1B;
    public static final byte AT_OTAU_GET_DEVICE_INFO_RESP = 0x1C;

    //Invalid Error Codes
    public static final byte AT_OTAU_INVALID_IMAGE = (byte) 0x81;
    public static final byte AT_OTAU_IMAGE_SWITCH_ERROR = (byte) 0x82;
    public static final byte AT_OTAU_BAD_PAGE_CRC = (byte) 0x19;
    public static final byte AT_OTAU_BAD_BLOCK_CRC = (byte) 0x84;
    public static final byte AT_OTAU_IMAGE_OLDER_VERSION = (byte) 0x85;
    public static final byte AT_OTAU_IMAGE_VERIFICATION_FAILED = (byte) 0x86;
    public static final byte AT_OTAU_IMAGE_OUT_OF_MEMORY = (byte) 0x87;
    public static final byte AT_OTAU_INVALID_VENDOR_ID = (byte) 0x88;
    public static final byte AT_OTAU_INVALID_PRODCUT_ID = (byte) 0x89;
    public static final byte AT_OTAU_INVALID_HARDWARE_VERSION = (byte) 0x8A;
    public static final byte AT_OTAU_INVALID_HARDWARE_REVISION = (byte) 0x8B;
    public static final byte AT_OTAU_NO_UPDATE = (byte) 0x8C;
    public static final byte AT_OTAU_SECURITY_LEVEL_NOT_SUPPORTED = (byte) 0x8D;
    public static final byte AT_OTAU_INVALID_SECTION = (byte) 0x8E;
    public static final byte AT_OTAU_INVALID_IMAGE_SIZE = (byte) 0x8F;
    public static final byte AT_OTAU_UNKNOWN_ERROR = (byte) 0x90;
    public static final byte AT_OTAU_IMAGE_PAGE_INFO_ERROR = (byte) 0x91;
    public static final byte AT_OTAU_IMAGE_BLOCK_INFO_ERROR = (byte) 0x92;
    public static final byte AT_OTAU_IMAGE_SECTION_END_ERROR = (byte) 0x93;


}
