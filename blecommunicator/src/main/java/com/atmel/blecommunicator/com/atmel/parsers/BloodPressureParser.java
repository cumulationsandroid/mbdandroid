/*
 *
 *
 *     BloodPressureParser.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.blecommunicator.com.atmel.parsers;

import android.bluetooth.BluetoothGattCharacteristic;

import com.atmel.blecommunicator.com.atmel.Attributes.UUIDDatabase;

import java.util.Calendar;

/**
 * Class used for parsing Blood pressure related information
 */
public class BloodPressureParser {

    public static final int BLOOD_PRESSURE_UNIT_FLAG = 0x01;
    public static final int PULSE_RATE_FLAG = 0x04;
    public static final int TIME_STAMP_FLAG = 0x02;
    public static final int FIRST_BIT_MASK = 0x0001;
    public static final int SECOND_BIT_MASK = 0x0002;
    public static final int THIRD_BIT_MASK = 0x0004;
    public static final int FOURTH_BIT_MASK = 0x0008;
    public static final int FIFTH_BIT_MASK = 0x0010;
    public static final int SIXTH_BIT_MASK = 0x0020;
    public static boolean mIsIntermediateCuff = true;
    public static String mSysPressure, mDiaPressure, mPressureUnit;
    public static Float mPulseRate;
    public static Boolean mBodyMovementStatus = false, mCuffFitStatus = false, mIrregularPulseStatus = false, mPulsRateStatus = false, mMeasurementPositionStatus = false, mMultipleBondStatus = false;

    public static boolean ismIsIntermediateCuff() {
        return mIsIntermediateCuff;
    }

    public static void parse(final BluetoothGattCharacteristic characteristic, boolean IsIntermediateCuff) {
        int offset = 0;
        final int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8,
                offset++);
        mIsIntermediateCuff = IsIntermediateCuff;
        float pressureValue;
        pressureValue = characteristic.getFloatValue(
                BluetoothGattCharacteristic.FORMAT_SFLOAT, offset);
        mSysPressure = String.format("%3.1f", pressureValue);

        if (characteristic.getUuid().equals(UUIDDatabase.UUID_BLOOD_PRESSURE_MEASUREMENT)) {
            pressureValue = characteristic.getFloatValue(
                    BluetoothGattCharacteristic.FORMAT_SFLOAT, offset + 2);
            mDiaPressure = String.format("%3.1f", pressureValue);
        }

//        final float meanArterialPressure = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset + 4);
        offset += 6;

        final boolean pressureUnit = (flags & BLOOD_PRESSURE_UNIT_FLAG) > 0;

        if (pressureUnit) {
            mPressureUnit = "kPa";
        } else {
            mPressureUnit = "mmHg";
        }

        final boolean timestampPresent = (flags & TIME_STAMP_FLAG) > 0;

        if (timestampPresent) {
            final Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, offset));
            calendar.set(Calendar.MONTH, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 2) - 1); // months are 1-based
            calendar.set(Calendar.DAY_OF_MONTH, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 3));
            calendar.set(Calendar.HOUR_OF_DAY, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 4));
            calendar.set(Calendar.MINUTE, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 5));
            calendar.set(Calendar.SECOND, characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset + 6));
            offset += 7;
        }

        final boolean pulseRatePresent = (flags & PULSE_RATE_FLAG) > 0;

        if (pulseRatePresent) {
            mPulseRate = characteristic.getFloatValue(BluetoothGattCharacteristic.FORMAT_SFLOAT, offset);
        }
    }

    /**
     * Get the Blood Pressure
     *
     * @return mSysPressure
     */
    public static String getSystolicBloodPressure() {
        return mSysPressure;
    }

    /**
     * Returns the  Diastolic pressure
     *
     * @return mDiaPressure
     */
    public static String getDiastolicBloodPressure() {
        return mDiaPressure;
    }

    /**
     * Returns the pressure unit
     *
     * @return unit
     */
    public static String getBloodPressureUnit() {
        return mPressureUnit;
    }

    public static Float getPulseRate() {
        return mPulseRate;
    }

    public static void parseBPFeature(final BluetoothGattCharacteristic characteristic) {
        int offset = 0;
        final int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16,
                offset);

        if ((flags & FIRST_BIT_MASK) > 0) {
            mBodyMovementStatus = true;
        }

        if ((flags & SECOND_BIT_MASK) > 0) {
            mCuffFitStatus = true;
        }

        if ((flags & THIRD_BIT_MASK) > 0) {
            mIrregularPulseStatus = true;
        }

        if ((flags & FOURTH_BIT_MASK) > 0) {
            mPulsRateStatus = true;
        }

        if ((flags & FIFTH_BIT_MASK) > 0) {
            mMeasurementPositionStatus = true;
        }

        if ((flags & SIXTH_BIT_MASK) > 0) {
            mMultipleBondStatus = true;
        }
    }

    public static Boolean getBodyMovementStatus() {
        return mBodyMovementStatus;
    }

    public static Boolean getCuffFitStatus() {
        return mCuffFitStatus;
    }

    public static Boolean getIrregularPulseStatus() {
        return mIrregularPulseStatus;
    }

    public static Boolean getPulsRateStatus() {
        return mPulsRateStatus;
    }

    public static Boolean getMeasurementPositionStatus() {
        return mMeasurementPositionStatus;
    }

    public static Boolean getMultipleBondStatus() {
        return mMultipleBondStatus;
    }
}
