/*
 *
 *
 *     HRMParser.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */
package com.atmel.blecommunicator.com.atmel.parsers;


import android.bluetooth.BluetoothGattCharacteristic;

import java.util.ArrayList;

/**
 * Parser class for parsing the data related to HRM Profile
 */
public class HRMParser {

    private static final int FIRST_BITMASK = 0x01;
    private static final int FOURTH_BITMASK = FIRST_BITMASK << 3;
    private static final int FIFTH_BITMASK = FIRST_BITMASK << 4;

    public static int mHeartRate;
    public static Integer mEEVal = null;


    public static void parse(final BluetoothGattCharacteristic characteristic) {

        int format;
        if (isHeartRateInUINT16(characteristic.getValue()[0])) {
            format = BluetoothGattCharacteristic.FORMAT_UINT16;
        } else {
            format = BluetoothGattCharacteristic.FORMAT_UINT8;
        }
        mHeartRate = characteristic.getIntValue(format, 1);

        if (isEEpresent(characteristic.getValue()[0])) {
            if (isHeartRateInUINT16(characteristic.getValue()[0])) {
                mEEVal = characteristic.getIntValue(
                        BluetoothGattCharacteristic.FORMAT_UINT16, 3);
            } else {
                mEEVal = characteristic.getIntValue(
                        BluetoothGattCharacteristic.FORMAT_UINT16, 2);
            }
        }

    }

    public static int getHeartRate() {
        return mHeartRate;
    }

    public static Integer getEnergyExpended() {
        return mEEVal;
    }

    public static void resetEnergyExpended() {
        mEEVal = null;
    }

    /**
     * Getting the RR-Interval
     *
     * @param characteristic
     * @return ArrayList
     */

    public static ArrayList<Integer> getRRInterval(
            BluetoothGattCharacteristic characteristic) {
        ArrayList<Integer> rrinterval = new ArrayList<Integer>();
        int length = characteristic.getValue().length;
        if (isEEpresent(characteristic.getValue()[0])) {
            if (isHeartRateInUINT16(characteristic.getValue()[0])) {
                if (isRRintpresent(characteristic.getValue()[0])) {
                    int startoffset = 5;
                    for (int i = startoffset; i < length; i += 2) {
                        rrinterval.add(characteristic.getIntValue(
                                BluetoothGattCharacteristic.FORMAT_UINT16, i));
                    }
                }
            } else {
                if (isRRintpresent(characteristic.getValue()[0])) {
                    int startoffset = 4;
                    for (int i = startoffset; i < length; i += 2) {
                        rrinterval.add(characteristic.getIntValue(
                                BluetoothGattCharacteristic.FORMAT_UINT16, i));
                    }
                }
            }
        } else {
            if (isHeartRateInUINT16(characteristic.getValue()[0])) {
                if (isRRintpresent(characteristic.getValue()[0])) {
                    int startoffset = 3;
                    for (int i = startoffset; i < length; i += 2) {
                        rrinterval.add(characteristic.getIntValue(
                                BluetoothGattCharacteristic.FORMAT_UINT16, i));
                    }
                }
            } else {
                if (isRRintpresent(characteristic.getValue()[0])) {
                    int startoffset = 2;
                    for (int i = startoffset; i < length; i += 2) {
                        rrinterval.add(characteristic.getIntValue(
                                BluetoothGattCharacteristic.FORMAT_UINT16, i));
                    }
                }
            }

        }
        return rrinterval;
    }

    /**
     * Checking the RR-Interval Flag
     *
     * @param flags
     * @return boolean
     */
    private static boolean isRRintpresent(byte flags) {
        return (flags & FIFTH_BITMASK) != 0;
    }

    /**
     * Checking the Energy Expended Flag
     *
     * @param flags
     * @return boolean
     */
    private static boolean isEEpresent(byte flags) {
        return (flags & FOURTH_BITMASK) != 0;
    }

    /**
     * Checking the Heart rate value format Flag
     *
     * @param flags
     * @return boolean
     */
    private static boolean isHeartRateInUINT16(byte flags) {
        return (flags & 1) != 0;
    }


    /**
     * Getting the Body Sensor Location
     */
    public static String getBodySensorLocation(
            BluetoothGattCharacteristic characteristic) {
        final int data = characteristic.getValue()[0];
        switch (data) {
            case 6:
                return "Foot";
            case 5:
                return "Ear Lobe";
            case 4:
                return "Hand";
            case 3:
                return "Finger";
            case 2:
                return "Wrist";
            case 1:
                return "Chest";
            case 0:
            default:
                return "Other";
        }
    }
}


