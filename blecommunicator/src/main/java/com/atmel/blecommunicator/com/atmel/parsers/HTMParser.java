/*
 *
 *
 *     HTMParser.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.parsers;

import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Class used for parsing Health temperature related information
 */
public class HTMParser {

    private final static String TAG = HTMParser.class.getSimpleName();

    private static final byte TEMPERATURE_UNIT_FLAG = 0x01; // 1 bit
    private static final byte TIMESTAMP_FLAG = 0x02; // 1 bits
    private static final byte TEMPERATURE_TYPE_FLAG = 0x04; // 1 bit

    private static String mTemperatureUnit;
    private static float mTemperatureValue;
    private static String mTemperatureType;


    public static void parse(final BluetoothGattCharacteristic characteristic) {
        int offset = 0;
       // String mDateAndTime;
        final int flags = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8,
                offset++);

		/*
         * false 	Temperature is in Celsius degrees
		 * true 	Temperature is in Fahrenheit degrees
		 */
        final boolean fahrenheit = (flags & TEMPERATURE_UNIT_FLAG) > 0;

		/*
         * false 	No Timestamp in the packet
		 * true 	There is a timestamp information
		 */
        final boolean timestampIncluded = (flags & TIMESTAMP_FLAG) > 0;

		/*
		 * false 	Temperature type is not included
		 * true 	Temperature type included in the packet
		 */
        final boolean temperatureTypeIncluded = (flags & TEMPERATURE_TYPE_FLAG) > 0;

        mTemperatureValue = characteristic.getFloatValue(BluetoothGattCharacteristic.
                        FORMAT_FLOAT,
                offset);
        offset += 4;

        if (timestampIncluded) {
           // mDateAndTime = DateTimeParser.parse(characteristic, offset);
            offset += 7;
        }

        if (temperatureTypeIncluded) {
            mTemperatureType = TemperatureTypeParser.parse(characteristic, offset);
            // offset++;
        }
        if (fahrenheit)
            mTemperatureUnit = "°F";
        else
            mTemperatureUnit = "°C";
    }

    public static float getTemperature() {
//        final float temperature = data[1];
//        Log.v(TAG, "Temperature Parsed:" + temperature);
        return mTemperatureValue;
    }

    public static String getTemperatureUnit() {
//        String tempUnit = "N/A";
//        if (data != null && data.length > 0) {
//            byte flagByte = data[0];
//            if ((flagByte & 0x01) != 0) {
//                tempUnit = "°F";
//            } else {
//                tempUnit = "°C";
//            }
//        }
        return mTemperatureUnit;
    }

    /**
     * Get the thermometer sensor location
     *
     * @return
     */
    public static String getTemperatureType() {
//        String health_thermo_sensor_location = "N/A";
//        if (data != null && data.length > 0) {
//            final int temperature_type = data[0];
//            Log.v(TAG, "Temperature TYpe Parsed:" + temperature_type);
//
//            switch (temperature_type) {
//                case 1:
//                    health_thermo_sensor_location = "Armpit";
//                    break;
//                case 2:
//                    health_thermo_sensor_location = "Body (general)";
//                    break;
//                case 3:
//                    health_thermo_sensor_location = "Ear (usually ear lobe)";
//                    break;
//                case 4:
//                    health_thermo_sensor_location = "Finger";
//                    break;
//                case 5:
//                    health_thermo_sensor_location = "Gastro-intestinal Tract";
//                    break;
//                case 6:
//                    health_thermo_sensor_location = "Mouth";
//                    break;
//                case 7:
//                    health_thermo_sensor_location = "Rectum";
//                    break;
//                case 8:
//                    health_thermo_sensor_location = "Tympanum (ear drum)";
//                    break;
//                case 9:
//                    health_thermo_sensor_location = "Toe";
//                    break;
//                case 10:
//                    health_thermo_sensor_location = "Toe";
//                    break;
//                default:
//                    health_thermo_sensor_location = "Reserved for future use";
//                    break;
//            }
//
//        }
        return mTemperatureType;
    }
}
