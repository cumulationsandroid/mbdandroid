/*
 *
 *
 *     SerialPortParser.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.parsers;


import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import java.io.IOException;

/**
 * Parser class for parsing the data related to Serial Port Profile
 */
public class SerialPortParser {
    static String receivedMessage = "";
    private static String parse;

    public static String getMessage() {
        return receivedMessage;
    }

    public static void parse(BluetoothGattCharacteristic characteristic) {

        byte[] mValue = characteristic.getValue();
        receivedMessage = getParse(mValue);

    }

    public static String getParse(byte[] mValue) {
        String string = "";
       // InputStream is = new ByteArrayInputStream(mValue);
        try {
//            InputStreamReader reader = new InputStreamReader(
//                    is, "windows-1252");
//            // or what ever seems to be the correct encoding!
//
//            StringBuilder builder = new StringBuilder();
//
//            while (reader.ready()) {
//                builder.append(reader.read());
//            }
//            reader.close();
//
//             string = builder.toString();
            Log.e("mValue length", "" + mValue.length);

            string = new String(mValue, "UTF-8");
        } catch (IOException ignored) {


        }
        return string;
    }
}


