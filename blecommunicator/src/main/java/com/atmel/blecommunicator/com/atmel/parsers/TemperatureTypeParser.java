/*
 *
 *
 *     TemperatureTypeParser.java
 *
 *     Copyright (c) 2016, Atmel Corporation. All rights reserved.
 *     Released under NDA
 *     Licensed under Atmel's Limited License Agreement.
 *
 *     THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *     WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *     EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *     DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *     POSSIBILITY OF SUCH DAMAGE.
 *
 *     Atmel Corporation: http://www.atmel.com
 *
 *
 */

package com.atmel.blecommunicator.com.atmel.parsers;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;


public class TemperatureTypeParser {

    public static String parse(final BluetoothGattCharacteristic characteristic) {
        return parse(characteristic, 0);
    }

    /* package */
    static String parse(final BluetoothGattCharacteristic characteristic, final int offset) {
        final int type = characteristic.getValue()[offset];
        Log.e("type", "" + type);
        switch (type) {
            case 1:
                return "Armpit";
            case 2:
                return "Body (general)";
            case 3:
                return "Ear (usually ear lobe)";
            case 4:
                return "Finger";
            case 5:
                return "Gastro-intestinal Tract";
            case 6:
                return "Mouth";
            case 7:
                return "Rectum";
            case 8:
                return "Toe";
            case 9:
                return "Tympanum (ear drum)";
            default:
                return "Tympanum (ear drum)"; // Its for temporary. characteristic not returning 9 instead of its retruning 0 . Thats y default value change to this .
        }
    }
}
