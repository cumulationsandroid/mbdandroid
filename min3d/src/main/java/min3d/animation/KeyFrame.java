/*
© 2017 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software and any derivatives
exclusively with Microchip products. It is your responsibility to comply with third party license
terms applicable to your use of third party software (including open source software) that may
accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY,
APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND
FITNESS FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS,
DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST
EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

Licensed under the MIT License. You may obtain a copy of the MIT license at:

https://opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.
 */

package min3d.animation;

import min3d.vos.Number3d;

public class KeyFrame {
	private String name;
	private float[] vertices;
	private float[] normals;

	private int[] indices;
	
	public KeyFrame(String name, float[] vertices)
	{
		this.name = name;
		this.vertices = vertices;
	}

	public KeyFrame(String name, float[] vertices, float[] normals)
	{
		this(name, vertices);
		this.normals = normals;
	}
	
	public String getName() {
		return name;
	}

	public float[] getVertices() {
		return vertices;
	}

	public int[] getIndices() {
		return indices;
	}

	public float[] getNormals() {
		return normals;
	}
	
	public void setIndices(int[] indices) {
		this.indices = indices;
		float[] compressed = vertices;
		vertices = new float[indices.length*3];
		int len = indices.length;
		int vi = 0;
		int ii = 0;
		int normalIndex = 0;
		
		for(int i=0; i<len; i++)
		{
			ii = indices[i] * 3;
			vertices[vi++] = compressed[ii];
			vertices[vi++] = compressed[ii + 1];
			vertices[vi++] = compressed[ii + 2];
		}
		
		normals = new float[vertices.length];
		int vertLen = vertices.length;
		
		for(int i=0; i<vertLen; i+=9)
		{
			Number3d normal = calculateFaceNormal(
					new Number3d(vertices[i], vertices[i+1], vertices[i+2]),
					new Number3d(vertices[i+3], vertices[i+4], vertices[i+5]),
					new Number3d(vertices[i+6], vertices[i+7], vertices[i+8])
					);
			normals[normalIndex++] = normal.x;
			normals[normalIndex++] = normal.y;
			normals[normalIndex++] = normal.z;
			normals[normalIndex++] = normal.x;
			normals[normalIndex++] = normal.y;
			normals[normalIndex++] = normal.z;
			normals[normalIndex++] = normal.x;
			normals[normalIndex++] = normal.y;
			normals[normalIndex++] = normal.z;
		}
	}
	
	public Number3d calculateFaceNormal(Number3d v1, Number3d v2, Number3d v3)
	{
		Number3d vector1 = Number3d.subtract(v2, v1);
		Number3d vector2 = Number3d.subtract(v3, v1);
		
		Number3d normal = new Number3d();
		normal.x = (vector1.y * vector2.z) - (vector1.z * vector2.y);
		normal.y = -((vector2.z * vector1.x) - (vector2.x * vector1.z));
		normal.z = (vector1.x * vector2.y) - (vector1.y * vector2.x);
		
		double normFactor = Math.sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z));
		
		normal.x /= normFactor;
		normal.y /= normFactor;
		normal.z /= normFactor;
		
		return normal;
	}
	
	public KeyFrame clone()
	{
		KeyFrame k = new KeyFrame(name, vertices.clone(), normals.clone());
		return k;
	}
}
