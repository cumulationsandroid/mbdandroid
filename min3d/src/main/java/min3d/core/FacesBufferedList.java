/*
© 2017 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software and any derivatives
exclusively with Microchip products. It is your responsibility to comply with third party license
terms applicable to your use of third party software (including open source software) that may
accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY,
APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND
FITNESS FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS,
DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST
EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

Licensed under the MIT License. You may obtain a copy of the MIT license at:

https://opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.
 */

package min3d.core;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

import min3d.vos.Face;

public class FacesBufferedList
{
	public static final int PROPERTIES_PER_ELEMENT = 3;
	public static final int BYTES_PER_PROPERTY = 2;

	private ShortBuffer _b;
	private int _numElements;

	private int _renderSubsetStartIndex = 0;
	private int _renderSubsetLength = 1;
	private boolean _renderSubsetEnabled = false;
	
	public FacesBufferedList(ShortBuffer $b, int $size)
	{
		ByteBuffer bb = ByteBuffer.allocateDirect($b.limit() * BYTES_PER_PROPERTY); 
		bb.order(ByteOrder.nativeOrder());
		_b = bb.asShortBuffer();
		_b.put($b);
		_numElements = $size;
	}
	
	public FacesBufferedList(int $maxElements)
	{
		ByteBuffer b = ByteBuffer.allocateDirect($maxElements * PROPERTIES_PER_ELEMENT * BYTES_PER_PROPERTY); 
		b.order(ByteOrder.nativeOrder());
		_b = b.asShortBuffer();
	}
	
	/**
	 * The number of items in the list. 
	 */
	public int size()
	{
		return _numElements;
	}
	
	
	/**
	 * The _maximum_ number of items that the list can hold, as defined on instantiation.
	 * (Not to be confused with the Buffer's capacity)
	 */
	public int capacity()
	{
		return _b.capacity() / PROPERTIES_PER_ELEMENT;
	}
	
	/**
	 * Clear object in preparation for garbage collection
	 */
	public void clear()
	{
		_b.clear();
	}

	public Face get(int $index)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		return new Face( _b.get(), _b.get(), _b.get() );
	}
	
	public void putInFace(int $index, Face $face)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		$face.a = (short)_b.get();
		$face.b = (short)_b.get();
		$face.c = (short)_b.get();
	}
	
	public short getPropertyA(int $index)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		return (short)_b.get();
	}
	public short getPropertyB(int $index)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT + 1);
		return (short)_b.get();
	}
	public float getPropertyC(int $index)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT + 2);
		return (short)_b.get();
	}

	/**
	 * Enables rendering only a subset of faces (renderSubset must be set to true) 
	 * This mechanism could be expanded to render multiple 'subsets' of the list of faces...
	 */
	public void renderSubsetStartIndex(int $num)
	{
		_renderSubsetStartIndex = $num;
	}
	public int renderSubsetStartIndex()
	{
		return _renderSubsetStartIndex;
	}
	public void renderSubsetLength(int $num)
	{
		_renderSubsetLength = $num;
	}
	public int renderSubsetLength()
	{
		return _renderSubsetLength;
	}
	
	/**
	 * If true, Renderer will only draw the faces as defined by 
	 * renderSubsetStartIndex and renderSubsetLength  
	 */
	public boolean renderSubsetEnabled()
	{
		return _renderSubsetEnabled;
	}
	public void renderSubsetEnabled(boolean $b)
	{
		_renderSubsetEnabled = $b;
	}
	
	//
	
	public void add(Face $f)
	{
		set( _numElements, $f );
		_numElements++;
	}
	
	public void add(int $a, int $b, int $c) {
		add((short)$a, (short)$b, (short)$c);
	}
	
	public void add(short $a, short $b, short $c)
	{
		set(_numElements, $a, $b, $c);
		_numElements++;
	}
	
	public void set(int $index, Face $face)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		_b.put($face.a);
		_b.put($face.b);
		_b.put($face.c);
	}

	public void set(int $index, short $a, short $b, short $c)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		_b.put($a);
		_b.put($b);
		_b.put($c);
	}
	
	public void setPropertyA(int $index, short $a)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		_b.put($a);
	}
	public void setPropertyB(int $index, short $b)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT + 1);
		_b.put($b);
	}
	public void setPropertyC(int $index, short $c)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT + 2);
		_b.put($c);
	}
	
	//
	
	public ShortBuffer buffer()
	{
		return _b;
	}
	
	public FacesBufferedList clone()
	{
		_b.position(0);
		FacesBufferedList c = new FacesBufferedList(_b, size());
		return c;
	}
}
