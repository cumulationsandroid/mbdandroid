/*
© 2017 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software and any derivatives
exclusively with Microchip products. It is your responsibility to comply with third party license
terms applicable to your use of third party software (including open source software) that may
accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY,
APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND
FITNESS FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS,
DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST
EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

Licensed under the MIT License. You may obtain a copy of the MIT license at:

https://opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.
 */

package min3d.core;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import min3d.vos.Number3d;

public class Number3dBufferList
{
	public static final int PROPERTIES_PER_ELEMENT = 3;
	public static final int BYTES_PER_PROPERTY = 4;

	private FloatBuffer _b;
	private int _numElements = 0;
	
	public Number3dBufferList(FloatBuffer $b, int $size)
	{
		ByteBuffer bb = ByteBuffer.allocateDirect($b.limit() * BYTES_PER_PROPERTY); 
		bb.order(ByteOrder.nativeOrder());
		_b = bb.asFloatBuffer();
		_b.put($b);
		_numElements = $size;
	}
	
	public Number3dBufferList(int $maxElements)
	{
		int numBytes = $maxElements * PROPERTIES_PER_ELEMENT * BYTES_PER_PROPERTY;
		ByteBuffer bb = ByteBuffer.allocateDirect(numBytes); 
		bb.order(ByteOrder.nativeOrder());
		
		_b  = bb.asFloatBuffer();
	}
	
	/**
	 * The number of items in the list. 
	 */
	public int size()
	{
		return _numElements;
	}
	
	/**
	 * The _maximum_ number of items that the list can hold, as defined on instantiation.
	 * (Not to be confused with the Buffer's capacity)
	 */
	public int capacity()
	{
		return _b.capacity() / PROPERTIES_PER_ELEMENT;
	}
	
	/**
	 * Clear object in preparation for garbage collection
	 */
	public void clear()
	{
		_b.clear();
	}
	
	//
	
	public Number3d getAsNumber3d(int $index)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		return new Number3d( _b.get(), _b.get(), _b.get() );
	}
	
	public void putInNumber3d(int $index, Number3d $number3d)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		$number3d.x = _b.get();
		$number3d.y = _b.get();
		$number3d.z = _b.get();
	}
	
	public float getPropertyX(int $index)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		return _b.get();
	}
	public float getPropertyY(int $index)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT + 1);
		return _b.get();
	}
	public float getPropertyZ(int $index)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT + 2);
		return _b.get();
	}
	
	//
	
	public void add(Number3d $n)
	{
		set( _numElements, $n );
		_numElements++;
	}
	
	public void add(float $x, float $y, float $z)
	{
		set( _numElements, $x,$y,$z );
		_numElements++;
	}
	
	public void set(int $index, Number3d $n)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		_b.put($n.x);
		_b.put($n.y);
		_b.put($n.z);
	}

	public void set(int $index, float $x, float $y, float $z)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		_b.put($x);
		_b.put($y);
		_b.put($z);
	}
	
	public void setPropertyX(int $index, float $x)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT);
		_b.put($x);
	}
	public void setPropertyY(int $index, float $y)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT + 1);
		_b.put($y);
	}
	public void setPropertyZ(int $index, float $z)
	{
		_b.position($index * PROPERTIES_PER_ELEMENT + 2);
		_b.put($z);
	}
	
	//
	
	public FloatBuffer buffer()
	{
		return _b;
	}
	
	public void overwrite(float[] $newVals)
	{
		_b.position(0);
		_b.put($newVals);
	}
	
	public Number3dBufferList clone()
	{
		_b.position(0);
		Number3dBufferList c = new Number3dBufferList(_b, size());
		return c;
	}
}
