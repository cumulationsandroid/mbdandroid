/*
© 2017 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software and any derivatives
exclusively with Microchip products. It is your responsibility to comply with third party license
terms applicable to your use of third party software (including open source software) that may
accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY,
APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND
FITNESS FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS,
DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST
EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

Licensed under the MIT License. You may obtain a copy of the MIT license at:

https://opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.
 */

package min3d.core;

import java.util.ArrayList;

import min3d.Shared;
import min3d.vos.TextureVo;

/**
 * Manages a list of TextureVo's used by Object3d's.
 * This allows an Object3d to use multiple textures. 
 * 
 * If more textures are added than what's supported by the hardware  
 * running the application, the extra items are ignored by Renderer
 * 
 * Uses a subset of ArrayList's methods. 
 */
public class TextureList  
{
	private ArrayList<TextureVo> _t;
	
	
	public TextureList()
	{
		_t = new ArrayList<TextureVo>();
	}
	
	/**
	 * Adds item to the list 
	 */
	public boolean add(TextureVo $texture)
	{
		if (! Shared.textureManager().contains($texture.textureId)) return false;
		return _t.add($texture);
	}
	
	/**
	 * Adds item at the given position to the list 
	 */
	public void add(int $index, TextureVo $texture)
	{
		_t.add($index, $texture);
	}
	
	/**
	 * Adds a new TextureVo with the given textureId to the list, and returns that textureVo  
	 */
	public TextureVo addById(String $textureId)
	{
		if (! Shared.textureManager().contains($textureId)) {
			throw new Error("Could not create TextureVo using textureId \"" + $textureId + "\". TextureManager does not contain that id.");
		}
		
		TextureVo t = new TextureVo($textureId);
		_t.add(t);
		return t;
	}
	
	/**
	 * Adds texture as the sole item in the list, replacing any existing items  
	 */
	public boolean addReplace(TextureVo $texture)
	{
		_t.clear();
		return _t.add($texture);
	}
	
	/**
	 * Removes item from the list 
	 */
	public boolean remove(TextureVo $texture)
	{
		return _t.remove($texture);
	}
	
	/**
	 * Removes item with the given textureId from the list 
	 */
	public boolean removeById(String $textureId)
	{
		TextureVo t = this.getById($textureId);
		if (t == null) {
			throw new Error("No match in TextureList for id \"" + $textureId + "\"");
		}
		return _t.remove(t);
	}
	
	public void removeAll()
	{
		for (int i = 0; i < _t.size(); i++)
			_t.remove(0);
	}
	
	/**
	 * Get item from the list which is at the given index position 
	 */
	public TextureVo get(int $index)
	{
		return _t.get($index);
	}
	
	/**
	 * Gets item from the list which has the given textureId
	 */
	public TextureVo getById(String $textureId)
	{
		for (int i = 0; i < _t.size(); i++) {
			String s = _t.get(i).textureId;
			if ($textureId == s) {
				TextureVo t = _t.get(i);
				return t;
			}
		}
		return null;
	}
	
	public int size()
	{
		return _t.size();
	}
	
	public void clear()
	{
		_t.clear();
	}
	
	/**
	 * Return a TextureVo array of TextureList's items 
	 */
	public TextureVo[] toArray()
	{
		Object[] a = _t.toArray();
		TextureVo[] ret = new TextureVo[a.length];
		for (int i = 0; i < _t.size(); i++)
		{
			ret[i] = (TextureVo)_t.get(i);
		}
		return ret;
	}
	
	/**
	 * Returns a String Array of the textureIds of each of the items in the list 
	 */
	public String[] getIds()
	{
		// BTW this makes a casting error. Why?
		// (TextureVo[])_t.toArray();

		String[] a = new String[_t.size()];
		for (int i = 0; i < _t.size(); i++)
		{
			a[i] = _t.get(i).textureId;
		}
		return a;
	}
}
