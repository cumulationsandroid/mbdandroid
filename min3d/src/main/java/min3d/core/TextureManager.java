/*
© 2017 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software and any derivatives
exclusively with Microchip products. It is your responsibility to comply with third party license
terms applicable to your use of third party software (including open source software) that may
accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY,
APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND
FITNESS FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS,
DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST
EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

Licensed under the MIT License. You may obtain a copy of the MIT license at:

https://opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.
 */

package min3d.core;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Set;

import min3d.Shared;


/**
 * TextureManager is responsible for managing textures for the whole environment. 
 * It maintains a list of id's that are mapped to the GL texture names (id's).
 * 
 * You add a Bitmap to the TextureManager, which adds a textureId to its list.
 * Then, you assign one or more TextureVo's to your Object3d's using id's that 
 * exist in the TextureManager.
 * 
 * Note that the _idToTextureName and _idToHasMipMap HashMaps used below
 * don't test for exceptions. 
 */
public class TextureManager 
{
	private HashMap<String, Integer> _idToTextureName;
	private HashMap<String, Boolean> _idToHasMipMap;
	private static int _counter = 1000001;
	private static int _atlasId = 0;
	
	
	public TextureManager()
	{
		reset();
	}

	public void reset()
	{
		// Delete any extant textures

		if (_idToTextureName != null) 
		{
			Set<String> s = _idToTextureName.keySet();
			Object[] a = s.toArray(); 
			for (int i = 0; i < a.length; i++) {
				int glId = getGlTextureId((String)a[i]);
				Shared.renderer().deleteTexture(glId);
			}
			// ...pain
		}
		
		_idToTextureName = new HashMap<String, Integer>();
		_idToHasMipMap = new HashMap<String, Boolean>();
	}

	/**
	 * 'Uploads' a texture via OpenGL which is mapped to a textureId to the TextureManager, 
	 * which can subsequently be used to assign textures to Object3d's. 
	 * 
	 * @return The textureId as added to TextureManager, which is identical to $id 
	 */
	public String addTextureId(Bitmap $b, String $id, boolean $generateMipMap)
	{
		if (_idToTextureName.containsKey($id)) throw new Error("Texture id \"" + $id + "\" already exists."); 

		int glId = Shared.renderer().uploadTextureAndReturnId($b, $generateMipMap);

		String s = $id;
		_idToTextureName.put(s, glId);
		_idToHasMipMap.put(s, $generateMipMap);
	
		_counter++;
		
		// For debugging purposes (potentially adds a lot of chatter)
		// logContents();
		
		return s;
	}

	/**
	 * Alternate signature for "addTextureId", with MIP mapping set to false by default.
	 * Kept for API backward-compatibility. 
	 */
	public String addTextureId(Bitmap $b, String $id)
	{
		return this.addTextureId($b, $id, false);
	}
	
	/**
	 * 'Uploads' texture via OpenGL and returns an autoassigned textureId,
	 * which can be used to assign textures to Object3d's. 
	 */
	public String createTextureId(Bitmap $b, boolean $generateMipMap)
	{
		return addTextureId($b, (_counter+""), $generateMipMap);
	}
	
	/**
	 * Deletes a textureId from the TextureManager,  
	 * and deletes the corresponding texture from the GPU
	 */
	public void deleteTexture(String $textureId)
	{
		int glId = _idToTextureName.get($textureId);
		Shared.renderer().deleteTexture(glId);
		_idToTextureName.remove($textureId);
		_idToHasMipMap.remove($textureId);
		
		// logContents();
		
		//xxx needs error check
	}

	/**
	 * Returns a String Array of textureId's in the TextureManager 
	 */
	public String[] getTextureIds()
	{
		Set<String> set = _idToTextureName.keySet();
		String[] a = new String[set.size()];
		set.toArray(a);
		return a;
	}
	
	/**
	 * Used by Renderer
	 * 
	 */
	int getGlTextureId(String $textureId) /*package-private*/
	{
		return _idToTextureName.get($textureId);
	}
	
	/**
	 * Used by Renderer
	 */
	boolean hasMipMap(String $textureId) /*package-private*/
	{
		return _idToHasMipMap.get($textureId);
	}
	

	public boolean contains(String $textureId)
	{
		return _idToTextureName.containsKey($textureId);
	}
	
	
	private String arrayToString(String[] $a)
	{
		String s = "";
		for (int i = 0; i < $a.length; i++)
		{
			s += $a[i].toString() + " | ";
		}
		return s;
	}
	
	private void logContents()
	{
		//Log.v(Min3d.TAG, "TextureManager contents updated - " + arrayToString( getTextureIds() ) );
	}
	
	public String getNewAtlasId() {
		return "atlas".concat(Integer.toString(_atlasId++));
	}
}
