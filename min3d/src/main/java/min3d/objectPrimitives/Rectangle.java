/*
© 2017 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software and any derivatives
exclusively with Microchip products. It is your responsibility to comply with third party license
terms applicable to your use of third party software (including open source software) that may
accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY,
APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND
FITNESS FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS,
DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST
EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

Licensed under the MIT License. You may obtain a copy of the MIT license at:

https://opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.
 */

package min3d.objectPrimitives;

import min3d.Utils;
import min3d.core.Object3dContainer;
import min3d.vos.Color4;

public class Rectangle extends Object3dContainer
{
	public Rectangle(float $width, float $height, int $segsW, int $segsH)
	{
		this($width, $height, $segsW, $segsH, new Color4(255, 0, 0, 255));
	}
	
	public Rectangle(float $width, float $height, int $segsW, int $segsH, Color4 color)
	{
		super(4 * $segsW * $segsH, 2 * $segsW * $segsH);

		int row, col;

		float w = $width / $segsW;
		float h = $height / $segsH;

		float width5 = $width/2f;
		float height5 = $height/2f;
		
		// Add vertices
		
		for (row = 0; row <= $segsH; row++)
		{
			for (col = 0; col <= $segsW; col++)
			{
				this.vertices().addVertex(
					(float)col*w - width5, (float)row*h - height5,0f,	
					(float)col/(float)$segsW, 1 - (float)row/(float)$segsH,	
					0,0,1f,	
					color.r, color.g, color.b, color.a
				);
			}
		}
		
		// Add faces
		
		int colspan = $segsW + 1;
		
		for (row = 1; row <= $segsH; row++)
		{
			for (col = 1; col <= $segsW; col++)
			{
				int lr = row * colspan + col;
				int ll = lr - 1;
				int ur = lr - colspan;
				int ul = ur - 1;
				Utils.addQuad(this, ul,ur,lr,ll);
			}
		}
	}
}
