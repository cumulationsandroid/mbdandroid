/*
© 2017 Microchip Technology Inc. and its subsidiaries.

Subject to your compliance with these terms, you may use Microchip software and any derivatives
exclusively with Microchip products. It is your responsibility to comply with third party license
terms applicable to your use of third party software (including open source software) that may
accompany Microchip software.

THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY,
APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND
FITNESS FOR A PARTICULAR PURPOSE.

IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS,
DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST
EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE
WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.

Licensed under the MIT License. You may obtain a copy of the MIT license at:

https://opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.
 */

package min3d.objectPrimitives;

import android.graphics.Bitmap;
import min3d.Shared;
import min3d.Utils;
import min3d.core.Object3dContainer;
import min3d.vos.Color4;

public class SkyBox extends Object3dContainer {
	private float size;
	private float halfSize;
	private int quality;
	private Color4 color;
	private Rectangle[] faces;
	
	public enum Face {
		North,
		East,
		South,
		West,
		Up,
		Down,
		All
	}
	
	public SkyBox(float size, int quality) {
		super(0, 0);
		this.size = size;
		this.halfSize = size *.5f;
		this.quality = quality;
		build();
	}
	
	private void build() {
		color = new Color4();
		faces = new Rectangle[6];
		Rectangle north = new Rectangle(size, size, quality, quality, color);
		Rectangle east = new Rectangle(size, size, quality, quality, color);
		Rectangle south = new Rectangle(size, size, quality, quality, color);
		Rectangle west = new Rectangle(size, size, quality, quality, color);
		Rectangle up = new Rectangle(size, size, quality, quality, color);
		Rectangle down = new Rectangle(size, size, quality, quality, color);
		
		north.position().z = halfSize;
		north.lightingEnabled(false);
		
		east.rotation().y = -90;
		east.position().x = halfSize;
		east.doubleSidedEnabled(true);
		east.lightingEnabled(false);
		
		south.rotation().y = 180;
		south.position().z = -halfSize;
		south.lightingEnabled(false);
		
		west.rotation().y = 90;
		west.position().x = -halfSize;
		west.doubleSidedEnabled(true);
		west.lightingEnabled(false);
		
		up.rotation().x = 90;
		up.position().y = halfSize;
		up.doubleSidedEnabled(true);
		up.lightingEnabled(false);
		
		down.rotation().x = -90;
		down.position().y = -halfSize;
		down.doubleSidedEnabled(true);
		down.lightingEnabled(false);
		
		faces[Face.North.ordinal()] = north;
		faces[Face.East.ordinal()] = east;
		faces[Face.South.ordinal()] = south;
		faces[Face.West.ordinal()] = west;
		faces[Face.Up.ordinal()] = up;
		faces[Face.Down.ordinal()] = down;
		
		addChild(north);
		addChild(east);
		addChild(south);
		addChild(west);
		addChild(up);
		addChild(down);
	}
	
	public void addTexture(Face face, int resourceId, String id) {
		Bitmap bitmap = Utils.makeBitmapFromResourceId(resourceId);
		Shared.textureManager().addTextureId(bitmap, id, true);
		bitmap.recycle();
		addTexture(face, bitmap, id);
	}
	
	public void addTexture(Face face, Bitmap bitmap, String id) {
		if(face == Face.All)
		{
			for(int i=0; i<6; i++)
			{
				faces[i].textures().addById(id);
			}
		}
		else
		{
			faces[face.ordinal()].textures().addById(id);
		}
	}
}
