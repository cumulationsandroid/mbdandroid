/*
 *
 *   Constant class
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.adapters;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.graphics.drawable.GradientDrawable;
import android.os.ParcelUuid;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.atmel.wearable.commonutils.AppConstants;
import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.commonutils.ConnectDrawable;
import com.atmel.wearable.commonutils.DeviceDrawable;
import com.atmel.wearable.commonutils.SignalDrawable;
import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.interfaces.AdapterCallBack;
import com.atmel.wearable.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecyclerViewBleDeviceAdapter extends RecyclerView.Adapter<RecyclerViewBleDeviceAdapter.DeviceViewHolder> {

    private Activity mContext;
    private List<BluetoothDevice> mDeviceList;
    private List<Device> mCustomDeviceList = new ArrayList<>();
    private List<BluetoothDevice> mDeviceListSecond;
    private HashMap<String, Integer> mHashMap;
    private OnDeviceConnectionListener mListener;
    private AdapterCallBack mAdapterCallback;


    public RecyclerViewBleDeviceAdapter(List<BluetoothDevice> mList, Activity context) {
        this.mDeviceList = mList;
        this.mDeviceListSecond = new ArrayList<>();
        this.mDeviceListSecond.addAll(mList);
        this.mContext = context;
        this.mHashMap = new HashMap<>();
        try {
            this.mAdapterCallback = ((AdapterCallBack) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    public void removeCustomList() {
        if (mCustomDeviceList != null && mCustomDeviceList.size() > 0) {
            mCustomDeviceList.clear();
        }
    }


    @Override
    public int getItemCount() {
        return mDeviceList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void add(BluetoothDevice device, int position) {
        mDeviceList.add(position, device);
        notifyItemInserted(position);
    }

    @Override
    public void onBindViewHolder(final DeviceViewHolder contactViewHolder, final int pos) {


/**
 * Setting the name and the RSSI of the BluetoothDevice. provided it
 * is a valid one
 */
        final BluetoothDevice device = mDeviceList.get(pos);
        final String deviceName = device.getName();
        contactViewHolder.signalStrengthValue.setText(mHashMap.get(mDeviceList.get(pos).getAddress()) +
                mContext.getResources().getString(R.string.signal_strength_unit));
        int drawable = SignalDrawable.getStrength(mHashMap.get(mDeviceList.get(pos).getAddress()), pos);
        contactViewHolder.mSignalStrengthImageView.setImageDrawable(mContext.getResources().getDrawable(drawable));
        contactViewHolder.deviceAddress.setText(mDeviceList.get(pos).getAddress());
        int deviceDraw = DeviceDrawable.getImgPosition(pos + 1);
        contactViewHolder.mDeviceImageView.setImageDrawable(mContext.getResources().getDrawable(deviceDraw));
        GradientDrawable bgShape = (GradientDrawable) contactViewHolder.mConnectButton.getBackground();
        bgShape.setColor(mContext.getResources().getColor(ConnectDrawable.getImgPosition(pos + 1)));
        contactViewHolder.signalStrengthValue.setTextColor(mContext.getResources().getColor(ConnectDrawable.getImgPosition(pos + 1)));
        if (deviceName != null && deviceName.length() > 0) {
            contactViewHolder.deviceName.setText(deviceName);
            if (BLEConnection.getConnectionState(device.getAddress())) {
                contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.disconnect));
            } else {
                contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
            }
        } else {
            contactViewHolder.deviceName.setText(mContext.getResources().getString(R.string.unknown));
            contactViewHolder.deviceName.setSelected(true);
            contactViewHolder.deviceAddress.setText(mDeviceList.get(pos).getAddress());
        }
        if(BLEConnection.getDevice() == null)
            contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
        contactViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ParcelUuid> uuid = null;
                try {
                    uuid = mCustomDeviceList.get(pos).uuid;
                } catch (Exception e) {
                    e.printStackTrace();
                }

               if( BLEConnection.getDevice() != null) {
                   if (BLEConnection.getDeviceState(mContext) == 2 && BLEConnection.getDevice().getAddress()
                           .equals(device.getAddress())) {
                       mAdapterCallback.onMethodCallback();
                   } else {
                       mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                   }
               } else {
                   AppUtils.setStringSharedPreference(mContext,
                           AppConstants.PREF_DEV_ADDRESS, "");
                   AppUtils.setIntSharedPreference(mContext,
                           AppConstants.PREF_CONNECTION_STATE, 0);
                   contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
                   mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
               }

            }
        });

        contactViewHolder.mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ParcelUuid> uuid = null;
                try {
                    uuid = mCustomDeviceList.get(pos).uuid;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (BLEConnection.getDevice() != null) {

                    if (BLEConnection.getDeviceState(mContext) == 2 && BLEConnection.getDevice().getAddress()
                            .equals(device.getAddress())) {
                        BLEConnection.disconnect();
                        Toast.makeText(mContext, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
                        // new BaseActivity().clearPreferences();
                        AppUtils.setStringSharedPreference(mContext,
                                AppConstants.PREF_DEV_ADDRESS, "");
                        AppUtils.setIntSharedPreference(mContext,
                                AppConstants.PREF_CONNECTION_STATE, 0);
                        contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
                    } else {

                        mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                    }
                } else{
//                    //Log.v("checker","entered");
                    AppUtils.setStringSharedPreference(mContext,
                            AppConstants.PREF_DEV_ADDRESS, "");
                    AppUtils.setIntSharedPreference(mContext,
                            AppConstants.PREF_CONNECTION_STATE, 0);
                    contactViewHolder.mConnectButton.setText(mContext.getResources().getString(R.string.connect));
                    mListener.onDeviceConnected(device.getAddress(), deviceName, uuid);
                }
            }
        });
    }


    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.wearable_device_list_row, viewGroup, false);
        return new DeviceViewHolder(itemView);
    }


    public void addDevices(BluetoothDevice mDevice, int rssi, List<ParcelUuid> uuidList) {
        if (!mDeviceList.contains(mDevice)) {
            mDeviceList.add(mDevice);
            mDeviceListSecond.add(mDevice);
            mHashMap.put(mDevice.getAddress(), rssi);
            if (mCustomDeviceList != null) {
                Device device = new Device();
                device.device = mDevice;
                device.rssi = rssi;
                device.uuid = uuidList;
                mCustomDeviceList.add(device);
            }

            notifyDataSetChanged();
        } else {
            mHashMap.put(mDevice.getAddress(), rssi);
            notifyDataSetChanged();
        }

    }

    public void clearList() {
        mDeviceListSecond.clear();
    }

    public void setonDeviceConnectedListener(OnDeviceConnectionListener listener) {
        this.mListener = listener;
    }

    /** Setting call back for getting */
    public void setAdapterCallback(AdapterCallBack callback) {
        this.mAdapterCallback = callback;
    }


    public interface OnDeviceConnectionListener {
        void onDeviceConnected(String address, String name, List<ParcelUuid> uuidList);
    }

    public static class DeviceViewHolder extends RecyclerView.ViewHolder {
        protected TextView deviceName;
        protected TextView signalStrengthValue;
        protected TextView deviceAddress;
        protected CardView cardView;
        protected ImageView mSignalStrengthImageView;
        protected ImageView mDeviceImageView;
        protected Button mConnectButton;

        public DeviceViewHolder(View v) {
            super(v);
            deviceName = (TextView) v.findViewById(R.id.ble_device_name);
            deviceAddress = (TextView) v.findViewById(R.id.ble_device_address);
            cardView = (CardView) v.findViewById(R.id.card_view);
            mSignalStrengthImageView = (ImageView) v.findViewById(R.id.signalStrengthImageView);
            signalStrengthValue = (TextView) v.findViewById(R.id.signalStrengthValue);
            mDeviceImageView = (ImageView) v.findViewById(R.id.imageView3);
            mConnectButton = (Button) v.findViewById(R.id.button_connect);
        }
    }

    public static class Device {
        BluetoothDevice device;
        int rssi;
        List<ParcelUuid> uuid;
        int mConnectionState;
    }

}
