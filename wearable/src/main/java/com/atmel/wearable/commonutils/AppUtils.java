/*
 *   AppUtils.java
 *
 *   The util class
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.commonutils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class AppUtils {
    public static final String COMMON_PREFE = "ATMEL_PREFERENCES";
    public static final String LINK_LOSS_PREFE = "LINK_LOSS_PREFERENCES";
    public static final String DANGER_PREFE = "DANGER_PREFERENCES";
    public static final String MID_PREFE = "MID_PREFERENCES";
    public static final String SAFE_PREFE = "SAFE_PREFERENCES";
    public static final String NO_ALERT = "NO_ALERT";
    public static final String MILD_ALERT = "MILD_ALERT";
    public static final String HIEGH_ALERT = "HIEGH_ALERT";
    public static final String TEMPERATURE_DEGREE = "DEGREE CELSIUS";
    private final static String SHARED_PREFS = "settings";
    private final static String IS_CELSIUS = "CELSIUS";
    private final static String SHOW_BATTERY_ALERT = "BATTERY_ALERT";
    private final static String SHOW_DROP_DETECTION_ALERT = "DROP_ALERT";
    private final static String SHOW_OUTOFRANGE_ALERT = "OUTOFRANGE_ALERT";
    public final static String STEP_COUNT_NOTIFY = "STEP_COUNT_NOTIFY";
    private final static String ENVIRONMENT_ODR_VALUE = "ENVIRONMENT_ODR_VALUE";
    private final static String ACCELEROMETER_ODR_VALUE = "ACCELEROMETER_ODR_VALUE";
    public final static String STEP_COUNTER = "STEP_COUNTER";


    /**
     * Initializing SharedPreferences
     *
     * @param context {@link Context}
     * @return {@link SharedPreferences}
     */
    private static SharedPreferences createSharedPreferences(Context context, String type) {
        return context.getSharedPreferences(type, Context.MODE_PRIVATE);
    }

    /**
     * This method return the class name {@link Object#getClass()}
     *
     * @param o {@link Object}
     * @return {@link String}
     */
    public static String getTagName(Object o) {
        return o.getClass().getName();
    }

    /**
     * Convert Celsius to Fahrenheit
     *
     * @param mTemperature {@link Float}
     * @return {@link Float}
     */
    public static Float convertToFahrenhite(Float mTemperature) {
        DecimalFormat df = new DecimalFormat("000.00");
        return Float.valueOf((df.format((mTemperature * 9) / 5 + 35)));
    }

    /**
     * Convert Fahrenheit to Celsius
     *
     * @param mTemperature {@link Float}
     * @return {@link Float}
     */
    public static Float convertToCelsius(Float mTemperature) {
        return (mTemperature - 32) * (5 / 9);
    }

    /**
     * Store default String value to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @param value   {@link String}
     */
    public static void setStringSharedPreference(Context context, String key, String value) {
        SharedPreferences.Editor edit = createSharedPreferences(context, COMMON_PREFE).edit();
        edit.putString(key, value);
        edit.commit();
    }

    /**
     * Get String values from default {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @return {@link String}
     */
    public static String getStringSharedPreference(Context context, String key) {
        return createSharedPreferences(context, COMMON_PREFE).getString(key, "");
    }

    /**
     * Store default String value to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @param value   {@link int}
     */
    public static void setIntSharedPreference(Context context, String key, int value) {
        SharedPreferences.Editor edit = createSharedPreferences(context, COMMON_PREFE).edit();
        edit.putInt(key, value);
        edit.commit();
    }

    /**
     * Get String values from default {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @return {@link int}
     */
    public static int getIntSharedPreference(Context context, String key) {
        return createSharedPreferences(context, COMMON_PREFE).getInt(key, 0);
    }

    /**
     * Store default String value to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @param value   {@link boolean}
     */
    public static void setBooleanSharedPreference(Context context, String key, boolean value) {
        SharedPreferences.Editor edit = createSharedPreferences(context, COMMON_PREFE).edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static boolean getBooleanSharedPreference(Context context, String key) {
        SharedPreferences preferences = createSharedPreferences(context, COMMON_PREFE);
        return preferences.getBoolean(key, false);
    }

    /**
     * Get linkloss {@link Boolean} value from {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @return {@link HashMap}
     */
    public static HashMap getSharedObjectForLinkLoss(Context context) {
        SharedPreferences preferences = createSharedPreferences(context, LINK_LOSS_PREFE);
        return getValues(preferences);
    }

    /**
     * Get Danger {@link Boolean} value from {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @return {@link HashMap}
     */
    public static HashMap getSharedObjectForDanger(Context context) {
        SharedPreferences preferences = createSharedPreferences(context, DANGER_PREFE);
        return getValues(preferences);
    }

    /**
     * Get Mid {@link Boolean} value from {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @return {@link HashMap}
     */
    public static HashMap getSharedObjectForMild(Context context) {
        SharedPreferences preferences = createSharedPreferences(context, MID_PREFE);
        return getValues(preferences);
    }

    /**
     * Get Safe {@link Boolean} value from {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @return {@link HashMap}
     */
    public static HashMap getSharedObjectForLinkSafe(Context context) {
        SharedPreferences preferences = createSharedPreferences(context, SAFE_PREFE);
        return getValues(preferences);
    }

    /**
     * Common method fro getting values from {@link SharedPreferences}
     *
     * @param preferences {@link SharedPreferences}
     * @return {@link HashMap}
     */
    private static HashMap<String, Boolean> getValues(SharedPreferences preferences) {
        boolean no_alert = preferences.getBoolean("NO_ALERT", false);
        boolean mild = preferences.getBoolean("MILD_ALERT", false);
        boolean hiegh = preferences.getBoolean("HIEGH_ALERT", false);
        HashMap<String, Boolean> map = new HashMap<>();
        map.put(NO_ALERT, no_alert);
        map.put(MILD_ALERT, mild);
        map.put(HIEGH_ALERT, hiegh);
        return map;
    }

    /**
     * Common method for getting boolean value from Default Shared Preferences
     *
     * @param context
     * @param key
     * @return
     */
    public static boolean getDefaultSharedPreference(Context context, String key) {
        if (context != null) {

            SharedPreferences Pref = PreferenceManager.getDefaultSharedPreferences(context);
            return Pref.getBoolean(key, false);

        } else {
            return false;
        }
    }

    /**
     * Store linkloss {@link Boolean} values to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param map     {@link HashMap}
     */
    public static void setLinkLossPreference(Context context, HashMap<String, Boolean> map) {
        setPreference(context, LINK_LOSS_PREFE, map);
    }

    /**
     * Store Danger {@link Boolean} values to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param map     {@link HashMap}
     */
    public static void setDanferPreference(Context context, HashMap<String, Boolean> map) {
        setPreference(context, DANGER_PREFE, map);
    }

    /**
     * Store Safe {@link Boolean} values to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param map     {@link HashMap}
     */
    public static void setSafePreference(Context context, HashMap<String, Boolean> map) {
        setPreference(context, SAFE_PREFE, map);
    }

    /**
     * Store MID {@link Boolean} values to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param map     {@link HashMap}
     */
    public static void setMidPreference(Context context, HashMap<String, Boolean> map) {
        setPreference(context, MID_PREFE, map);
    }

    /**
     * Common method to store {@link Boolean} to {@link SharedPreferences}
     *
     * @param context       {@link Context}
     * @param linkLossPrefe {@link String}
     * @param map           {@link HashMap}
     */
    private static void setPreference(Context context, String linkLossPrefe, HashMap<String, Boolean> map) {
        SharedPreferences.Editor edit = createSharedPreferences(context, linkLossPrefe).edit();
        boolean noalert = map.get(NO_ALERT);
        boolean midalert = map.get(MILD_ALERT);
        boolean hieghalert = map.get(HIEGH_ALERT);
        edit.putBoolean(NO_ALERT, noalert);
        edit.putBoolean(MILD_ALERT, midalert);
        edit.putBoolean(HIEGH_ALERT, hieghalert);
        edit.commit();
    }

    public static void setDefaultSharedPreference(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Method to detect whether the device is werable_phone or tablet
     */
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * Checks if the application is being sent in the background (i.e behind
     * another application's Activity).
     *
     * @param context the context
     * @return <code>true</code> if another application will be above this one.
     */

    public static boolean isApplicationBackgroundCompat(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isApplicationBackground(Context context) {
        String[] activePackages = getActivePackages(context);
        if (activePackages.length > 0) {
            for (String activePackage : activePackages) {
                if (activePackage.equals(context.getPackageName())) {
                    return false;
                }
            }
        }
        return true;
    }

    private static String[] getActivePackages(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final Set<String> activePackages = new HashSet<String>();
        final List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                activePackages.addAll(Arrays.asList(processInfo.pkgList));
            }
        }
        return activePackages.toArray(new String[activePackages.size()]);
    }


    /**
     * used in Settings
     **/

    public static void setTemperatureUnit(Context context, String isCelsius) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(IS_CELSIUS, isCelsius);
        editor.commit();
    }

    public static String getTemperatureUnit(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(IS_CELSIUS, "");
    }


    public static void setBatteryAlert(Context context, boolean showAlert) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SHOW_BATTERY_ALERT, showAlert);
        editor.commit();
    }

    public static boolean getBatteryAlert(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SHOW_BATTERY_ALERT, true);
    }

    public static void setOutOfRangeAlert(Context context, boolean showAlert) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SHOW_OUTOFRANGE_ALERT, showAlert);
        editor.commit();
    }

    public static boolean getOutOfRangeAlert(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SHOW_OUTOFRANGE_ALERT, true);
    }


    public static void setDropAlert(Context context, boolean showAlert) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SHOW_DROP_DETECTION_ALERT, showAlert);
        editor.commit();
    }

    public static boolean getDropAlert(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SHOW_DROP_DETECTION_ALERT, true);
    }


    public static void setEnvironmentODR(Context context, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ENVIRONMENT_ODR_VALUE, value);
        editor.commit();
    }

    public static String getEnvironmentODR(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(ENVIRONMENT_ODR_VALUE, "");
    }

    public static void setAccelerometerODR(Context context, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCELEROMETER_ODR_VALUE, value);
        editor.commit();
    }

    public static String getAccelerometerODR(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(ACCELEROMETER_ODR_VALUE, "");
    }


}

