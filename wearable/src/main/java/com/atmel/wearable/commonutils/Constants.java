/*
 *   Constants.java
 *
 *   Constant class for application ble framework
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.commonutils;


public class Constants {

    public static final int DEVICE_DISCONNECTED_KEY = 5646;
    public static final int DEVICE_CONNECTED_KEY = 5647;
    public static final int SERVICE_DISCOVERED_KEY = 5648;
    public static final int CHARACTERISTICS_CHANGED_KEY = 5649;
    public static final int CHARACTERISTICS_WRITE_KEY = 5650;
    public static final int DEVICE_PAIR_IN_PROCESS_KEY = 5651;
    public static final int DEVICE_PAIR_DONE_KEY = 5652;
    public static final int DEVICE_PAIR_FAILED_KEY = 5653;
    public static final int DEVICE_BLUETOOTH_ON_KEY = 5654;
    public static final int DEVICE_BLUETOOTH_OFF_KEY = 5655;
    public static final int CHARACTERISTICS_READ_KEY = 5656;
    public static final int REMOTE_RSSI_KEY = 5657;
    public static final int DEVICE_PAIR_REQUEST = 5658;
    public static final int DEVICE_NOTIFY_REQUEST = 5659;
    public static final int DEVICE_READ_REQUEST = 5660;
    public static final int DESCRIPTOR_WRITE_KEY = 5661;
    public static final int DEVICE_WRITE_REQUEST = 5662;
    public static final int DEVICE_SERVICES_DISCOVERED = 5663;


    /**
     * Graph constants
     */
    public static  final int GRAPH_MARGIN_40 = 40;
    public static  final int GRAPH_MARGIN_90 = 90;
    public static  final int GRAPH_MARGIN_25 = 25;
    public static  final int GRAPH_MARGIN_10 = 10;


    // Immediate alert constants
    public static final String VALUE_NO_ALERT = "0x00";
    public static final String VALUE_MID_ALERT = "0x01";
    public static final String VALUE_HIGH_ALERT = "0x02";

    //Clear Alarm
    public  static final int ID_CLEAR_ALARM = 2789;


    private static final String PACKAGE_NAME = Constants.class.getPackage().getName();
    public static final String RESULT_RECEIVER_KEY = PACKAGE_NAME + "RESULT_RECEIVER_KEY";
    public static final String DEVICE_CONNECTED_NAME_KEY = PACKAGE_NAME +
            "DEVICE_CONNECTED_NAME_KEY";
    public static final String DEVICE_DISCONNECTED_NAME_KEY = PACKAGE_NAME +
            "DEVICE_DISCONNECTED_NAME_KEY";
    public static final String DEVICE_PAIRING_STATUS_IN_PROCESS_KEY = PACKAGE_NAME +
            "DEVICE_PAIRING_STATUS_IN_PROCESS_KEY";
    public static final String DEVICE_PAIRING_STATUS_DONE_KEY = PACKAGE_NAME +
            "DEVICE_PAIRING_STATUS_DONE_KEY";
    public static final String DEVICE_PAIRING_STATUS_FAILED_KEY = PACKAGE_NAME +
            "DEVICE_PAIRING_STATUS_FAILED_KEY";
    public static final String DEVICE_BLUETOOTH_STATUS_OFF_KEY = PACKAGE_NAME +
            "DEVICE_BLUETOOTH_STATUS_OFF_KEY";
    public static final String DEVICE_BLUETOOTH_STATUS_ON_KEY = PACKAGE_NAME +
            "DEVICE_BLUETOOTH_STATUS_ON_KEY";
    public static final String SERVICE_DISCOVERED_STATUS_KEY = PACKAGE_NAME +
            "SERVICE_DISCOVERED_STATUS_KEY";
    public static final String CHARACTERISTIC_CHANGE_STATUS_KEY = PACKAGE_NAME +
            "CHARACTERISTIC_CHANGE_STATUS_KEY";
    public static final String CHARACTERISTIC_WRITE_STATUS_KEY = PACKAGE_NAME +
            "CHARACTERISTIC_WRITE_STATUS_KEY";
    public static final String CHARACTERISTIC_READ_STATUS_KEY = PACKAGE_NAME +
            "CHARACTERISTIC_READ_STATUS_KEY";
    public static final String CHARACTERISTIC_READ_DATA = PACKAGE_NAME +
            "CHARACTERISTIC_READ_DATA";
    public static final String CHARACTERISTIC_READ_DATA_CODE = PACKAGE_NAME +
            "CHARACTERISTIC_READ_DATA_CODE";
    public static final String REMOTE_RSSI_VALUE = PACKAGE_NAME +
            "REMOTE_RSSI_VALUE";
    public static final String DEVICE_BLUETOOTH_KEY = PACKAGE_NAME +
            "DEVICE_BLUETOOTH_STATUS_KEY";
    public static final String GATTSERVER_READ_REQUESTID = PACKAGE_NAME +
            "GATTSERVER_READ_REQUESTID";
    public static final String GATTSERVER_READ_CHARACTERISTICS = PACKAGE_NAME +
            "GATTSERVER_READ_CHARACTERISTICS";
    public static final String GATTSERVER_READ_OFFSET = PACKAGE_NAME +
            "GATTSERVER_READ_OFFSET";
    public static final String DESCRIPTOR_WRITE_STATUS_KEY = PACKAGE_NAME +
            "DESCRIPTOR_WRITE_STATUS";
    public static final String DESCRIPTOR_WRITE_CHARACTERISTIC = PACKAGE_NAME +
            "DESCRIPTOR_WRITE_CHARACTERISTIC";
    public static final String GATTSERVER_NOTIFY_REQUEST_CHAR = PACKAGE_NAME +
            "DEVICE_NOTIFY_REQUEST";
    public static final String GATTSERVER_NOTIFY_REQUEST_STATUS = PACKAGE_NAME +
            "DEVICE_NOTIFY_REQUEST_STATUS";
    public static final String GATTSERVER_WRITE_REQUESTID = PACKAGE_NAME +
            "GATTSERVER_WRITE_REQUESTID";
    public static final String GATTSERVER_WRITE_CHARACTERISTICS = PACKAGE_NAME +
            "GATTSERVER_WRITE_CHARACTERISTICS";
    public static final String GATTSERVER_WRITE_OFFSET = PACKAGE_NAME +
            "GATTSERVER_WRITE_OFFSET";
    public static final String GATTSERVER_SERVICES_DISCOVERED_STATUS = PACKAGE_NAME +
            "GATTSERVER_SERVICES_DISCOVERED_STATUS";
    public static final String GATTSERVER_SERVICES_ADDED_SERVICE = PACKAGE_NAME +
            "GATTSERVER_SERVICES_ADDED_SERVICE";
}
