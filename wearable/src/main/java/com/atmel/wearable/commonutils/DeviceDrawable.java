/*
 *
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.commonutils;


import com.atmel.wearable.R;

/**
 * Created by sreejith on 9/11/15.
 */
public class DeviceDrawable {
public static int getImgPosition(Integer pos){
    int drawPos;
    pos = pos%6;
    if(pos==1)  {
        drawPos = R.drawable.w01;
    } else if(pos == 2){
        drawPos= R.drawable.w02;
    } else if(pos == 3){
        drawPos=R.drawable.w03;
    } else if(pos == 4){
        drawPos=R.drawable.w04;
    } else if(pos == 5){
        drawPos=R.drawable.w05;
    } else {
        drawPos=R.drawable.w06;
    }

    return drawPos;
}
//    int deviceImg;
//public DeviceDrawable(int deviceImg){
//        this.deviceImg=deviceImg;
//    }
//private List<DeviceDrawable> deviceDrawables;
//    private void InitialiseData(){
//        deviceDrawables = new ArrayList<>();
//        deviceDrawables.add(new DeviceDrawable(R.drawable.w01));
//        deviceDrawables.add(new DeviceDrawable(R.drawable.w02));
//        deviceDrawables.add(new DeviceDrawable(R.drawable.w03));
//        deviceDrawables.add(new DeviceDrawable(R.drawable.w04));
//        deviceDrawables.add(new DeviceDrawable(R.drawable.w05));
//        deviceDrawables.add(new DeviceDrawable(R.drawable.w06));
//
//    }
}
