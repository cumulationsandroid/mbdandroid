/*
 *
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.commonutils;

import java.util.UUID;

/**
 * Created by Sanu on 4/11/15.
 */
public class GattAttributes {

    /**
     * Service UUID's (128 bits)
     */
    public static final String ENVIRONMENT_SERVICE = "f05abac0-3936-11e5-87a6-0002a5d5c51b";
    public static final String BATTERY_SERVICE = "f05abac3-3936-11e5-87a6-0002a5d5c51b";
    public static final String TOUCH_GESTURE_SERVICE = "f05abac4-3936-11e5-87a6-0002a5d5c51b";
    public static final String DEVICE_ORIENTATION_SERVICE = "f05abac1-3936-11e5-87a6-0002a5d5c51b";

    /**
     * Environment Characteristics UUID's (128 bits)
     */


    public static final String ENVIRONMENT_CHARACTERISTICS = "f05abad0-3936-11e5-87a6-0002a5d5c51b";

   /* public static final String TEMPERATURE_CHARACTERISTICS = "0xF05ABAD0393611E587A6-0002A5D5C51B";
    public static final String PRESSURE_CHARACTERISTICS = "0xF05ABAD2393611E587A60002A5D5C51B";
    public static final String UV_CHARACTERISTICS = "0xF05ABAD3393611E587A60002A5D5C51B"; */


    /**
     * Battery Characteristics UUID's (128 bits)
     */

    public static final String LOW_BATTERY_INDICATION_CHARACTERISTICS = "f05abadc-3936-11e5-87a6-0002a5d5c51b";

    /**
     * Touch gesture Characteristics UUID's (128 bits)
     */

    public static final String GESTURE_CHARACTERISTICS = "f05abadd-3936-11e5-87a6-0002a5d5c51b";


    /**
     * Device orientation Characteristics UUID's (128 bits)
     */

    public static final String GYRO_POSITIONS_CHARACTERISTICS = "f05abad4-3936-11e5-87a6-0002a5d5c51b";
    public static final String ACCELERO_POSITIONS_CHARACTERISTICS = "f05abad7-3936-11e5-87a6-0002a5d5c51b";
    public static final String DROP_DETECTION_INDICATION_CHARACTERISTICS = "f05abada-3936-11e5-87a6-0002a5d5c51b";
    public static final String STEP_INC_INDICATION_CHARACTERISTICS = "f05abadb-3936-11e5-87a6-0002a5d5c51b";
    public static final String ROTATION_VECTOR_CHARACTERISTICS = "f05abad8-3936-11e5-87a6-0002a5d5c51b";

    /**
     * ODR Characteristics UUID's (128 bits)
     */
    public static final String ENVIRONMENT_ODR_CHARCTERISTICS = "f05abad1-3936-11e5-87a6-0002a5d5c51b";
    public static final String MOTION_ODR_CHARCTERISTICS = "f05abad9-3936-11e5-87a6-0002a5d5c51b";

    /**
     * used to convert string uuid to valid UUID
     *
     * @param stringUuid
     * @return UUID
     */
    public UUID getUuidFromString(String stringUuid) {
        return UUID.fromString(stringUuid);
    }


    /**
     * Descriptor UUID's
     */
    public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    public static final UUID CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");


    /**
     * Gatt services
     */
    public static final String GENERIC_ACCESS_SERVICE = "00001800-0000-1000-8000-00805f9b34fb";
    public static final String GENERIC_ATTRIBUTE_SERVICE = "00001801-0000-1000-8000-00805f9b34fb";
}
