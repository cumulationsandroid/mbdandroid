/*
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.commonutils;

import android.content.Context;
import android.content.SharedPreferences;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Atmel on 18/8/15.
 */
public class GattUtils {

    public static final String COMMON_PREFE = "ATMEL_PREFERENCES";
    public static final String CURRENT_TIME_NOTIFICATION_STATUS = "CURRENT_TIME_NOTIFICATION ENABLE DISABLE INFO";
    public static final String ANS_READ_NOTIFICATION_STATUS = "ANS_READ_NOTIFICATION_STATUS";
    public static final String ANS_UNREAD_NOTIFICATION_STATUS = "ANS_UNREAD_NOTIFICATION_STATUS";
    public static final String PHONE_ALERT_STATUS = "Phone Alert Notification";
    public static final String PHONE_ALERT_VALUE = "Phone Alert VAlue";

    public static byte[] bytesFromInt(int value) {
        //Convert result into raw bytes. GATT APIs expect LE order
        return ByteBuffer.allocate(4)
                .order(ByteOrder.LITTLE_ENDIAN)
                .putInt(value)
                .array();
    }

    public static byte[] getShiftedTimeValue(int timeOffset) {
        int value = Math.max(0,
                (int) (System.currentTimeMillis() / 1000) - timeOffset);
        return bytesFromInt(value);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String ByteArraytoHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();

    }

    /**
     * Store default String value to {@link SharedPreferences}
     *
     * @param context {@link Context}
     * @param key     {@link String}
     * @param value   {@link boolean}
     */
    public static void setBooleanSharedPreference(Context context, String key, boolean value) {
        SharedPreferences.Editor edit;
        edit = createSharedPreferences(context, COMMON_PREFE).edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static boolean getBooleanSharedPreference(Context context, String key) {
        SharedPreferences preferences = createSharedPreferences(context, COMMON_PREFE);
        return preferences.getBoolean(key, false);
    }

    /**
     * Initializing SharedPreferences
     *
     * @param context {@link Context}
     * @return {@link SharedPreferences}
     */
    private static SharedPreferences createSharedPreferences(Context context, String type) {
        return context.getSharedPreferences(type, Context.MODE_PRIVATE);
    }


}
