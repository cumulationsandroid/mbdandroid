/*
 *   SignalDrawable.java
 *
 *   This class handles the signal strength drawables for rssi values
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.commonutils;

import com.atmel.wearable.R;

public class SignalDrawable {

    private static int[] blueArray = {R.drawable.blue_01,R.drawable.blue_02,R.drawable.blue_03
            ,R.drawable.blue_04,R.drawable.blue_05};
    private static int[] greenArray = {R.drawable.green_01,R.drawable.green_02,R.drawable.green_03
            ,R.drawable.green_04,R.drawable.green_05};
    private static int[] yellowArray = {R.drawable.yellow_01,R.drawable.yellow_02,R.drawable.yellow_03
            ,R.drawable.yellow_04,R.drawable.yellow_05};
    private static int[] redArray = {R.drawable.red_01,R.drawable.red_02,R.drawable.red_03
            ,R.drawable.red_04,R.drawable.red_05};
    private static int[] violetArray = {R.drawable.violet_01,R.drawable.violet_02,R.drawable.violet_03
            ,R.drawable.violet_04,R.drawable.violet_05};
    private static int[] lightGreenArray = {R.drawable.light_green_01,R.drawable.light_green_02,R.drawable.light_green_03
            ,R.drawable.light_green_04,R.drawable.light_green_05};

    public static int getStrength(Integer strength,Integer pos) {
        int[] drawableID = new int[5];
        drawableID = getColorArray(pos);
        if (strength == 127) {     // value of 127 reserved for RSSI not available
            return R.drawable.no_signal;
        } else if (strength <= -84) {
            return drawableID[0];
        } else if (strength <= -72) {
            return drawableID[1];
        } else if (strength <= -60) {
            return drawableID[2];
        } else if (strength <= -48) {
            return drawableID[3];
        } else {
            return drawableID[4];
        }
    }



    private static int[] getColorArray(Integer pos) {
        pos = pos%6;
        if(pos == 0) {
            return greenArray;
        } else if(pos == 1) {
            return yellowArray;
        } else if(pos == 2) {
            return redArray;
        } else if(pos == 3) {
            return lightGreenArray;
        } else if(pos == 4) {
            return violetArray;
        } else {
            return blueArray;
        }


    }
}
