/*
 *   BLEDataReceiver.java
 *
 *   This class handles the communication between framework and application
 *   classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.communicator;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.commonutils.Constants;
import com.atmel.wearable.commonutils.GattUtils;
import com.atmel.wearable.wearables.BLEApplication;

public class BLEDataReceiver extends ResultReceiver {

    public static BLEConnection mConnectionListener;
    public static GATTProfiles mGattListener;
    public static GattServerProfiles mGattServerProfilesListener;
    public static LeServiceDiscoveryStatus mLeServiceDiscoveryStatus;
    public static LeDeviceDisconnected mLeDeviceDisconnected;
    public static LeCharacteristicChanged mLeCharacteristicChanged;

    private String TAG = BLEDataReceiver.class.getSimpleName();

    public BLEDataReceiver(Handler handler) {
        super(handler);
    }

    public static void setBLEConnectionListener(BLEConnection listener) {
        mConnectionListener = listener;

    }

    public static void setGattListener(GATTProfiles  gattListener) {
        mGattListener = gattListener;
    }

    public static void setGattServerListener(GattServerProfiles  gattServerListener) {
        mGattServerProfilesListener = gattServerListener;
    }


    public static void setOnServiceDiscoveryListner(LeServiceDiscoveryStatus serviceDiscoveryStatus) {
        mLeServiceDiscoveryStatus = serviceDiscoveryStatus;
    }

    public static void setOnDeviceDisconnectedListener(LeDeviceDisconnected
                                                               LeDeviceDisconnectedListener) {
        mLeDeviceDisconnected = LeDeviceDisconnectedListener;
    }

    public static void setOnCharacteristicChangeListner(LeCharacteristicChanged
                                                                characteristicChangeListner) {
        mLeCharacteristicChanged = characteristicChangeListner;
    }



    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        super.onReceiveResult(resultCode, resultData);
        switch (resultCode) {
            case Constants.DEVICE_CONNECTED_KEY:

                String connectedDevice = resultData.getString(Constants.DEVICE_CONNECTED_NAME_KEY);
                /* if (mLeDeviceConnected != null) {
                    mLeDeviceConnected.onLeDeviceConnected(connectedDevice);
                }*/
                if (mConnectionListener != null) {
                    mConnectionListener.onLeDeviceConnected(connectedDevice);
                }
                break;
            case Constants.SERVICE_DISCOVERED_KEY:
                boolean status = resultData.getBoolean(Constants.SERVICE_DISCOVERED_STATUS_KEY);
                if (mConnectionListener != null)
                    mConnectionListener.onLeServiceDiscovered(status);
                break;
            case Constants.CHARACTERISTICS_CHANGED_KEY:
                boolean changestatus = resultData.getBoolean(
                        Constants.CHARACTERISTIC_CHANGE_STATUS_KEY);
                if (mGattListener != null)
                    mGattListener.onLeCharacteristicChanged(changestatus);
                break;
            case Constants.CHARACTERISTICS_WRITE_KEY:
                boolean writestatus = resultData.getBoolean(
                        Constants.CHARACTERISTIC_WRITE_STATUS_KEY);
                if (mGattListener != null)
                    mGattListener.onLeCharacteristicWrite(writestatus);
                if(mGattServerProfilesListener != null){
                         String characteristics = resultData.getString(Constants.GATTSERVER_READ_CHARACTERISTICS, null);
                        int requestId = resultData.getInt(Constants.GATTSERVER_READ_REQUESTID, 0);
                        int offset = resultData.getInt(Constants.GATTSERVER_READ_OFFSET, 0);
                        byte[] value =resultData.getByteArray(GattUtils.PHONE_ALERT_VALUE);
                        mGattServerProfilesListener.onLeWriteRequest(characteristics, requestId, offset, value);

                }
                break;

            case Constants.CHARACTERISTICS_READ_KEY:
                boolean readinfo = resultData.getBoolean(Constants.
                        CHARACTERISTIC_READ_STATUS_KEY);
                if (mGattListener != null) {
                if (resultData.getString(Constants.CHARACTERISTIC_READ_DATA) !=null){
                    String readData = resultData.getString(Constants.
                            CHARACTERISTIC_READ_DATA);
                    int code= resultData.getInt(Constants.
                            CHARACTERISTIC_READ_DATA_CODE);
                    mGattListener.onLeCharacteristicRead(readinfo,readData,code);
                }else {
                    mGattListener.onLeCharacteristicRead(readinfo);
                }

                }
                break;
            case Constants.REMOTE_RSSI_KEY:
                int rssi = resultData.getInt(Constants.REMOTE_RSSI_VALUE);
                if (mGattListener != null) {
                    mGattListener.onLeReadRemoteRssi(rssi);
                }
                break;
            case Constants.DEVICE_DISCONNECTED_KEY:
                if (mConnectionListener != null) {
                   // mConnectionListener.onLeDeviceDisconnected();
                    /**
                     * Checking if the application running sent to background or foreground.
                     */
                    if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                        if (!AppUtils.isApplicationBackgroundCompat(BLEApplication.getInstance())) {
                            mConnectionListener.onLeDeviceDisconnected();
                        }
                    } else {
                        if (!AppUtils.isApplicationBackground(BLEApplication.getInstance())) {
                            mConnectionListener.onLeDeviceDisconnected();
                        }
                    }
                }
                break;
            case Constants.DEVICE_BLUETOOTH_ON_KEY:
                if (mConnectionListener != null) {
                    /**
                     * Checking if the application running sent to background or foreground.
                     */

                    if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                        if (!AppUtils.isApplicationBackgroundCompat(BLEApplication.getInstance())) {
                            mConnectionListener.onLeBluetoothStatusChanged();
                        }
                    } else {
                        if (!AppUtils.isApplicationBackground(BLEApplication.getInstance())) {
                            mConnectionListener.onLeBluetoothStatusChanged();
                        }
                    }

                }
                break;
            case Constants.DEVICE_BLUETOOTH_OFF_KEY:
                if (mConnectionListener != null) {

                    /**
                     * Checking if the application running sent to background or foreground.
                     */

                    if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                        if (!AppUtils.isApplicationBackgroundCompat(BLEApplication.getInstance())) {
                            mConnectionListener.onLeBluetoothStatusChanged();
                        }
                    } else {
                        if (!AppUtils.isApplicationBackground(BLEApplication.getInstance())) {
                            mConnectionListener.onLeBluetoothStatusChanged();
                        }
                    }
                }
                break;

            case Constants.DEVICE_NOTIFY_REQUEST:
                if(mGattServerProfilesListener != null){
                    mGattServerProfilesListener.onLeNotifyRequest();
                }
                break;

            case Constants.DEVICE_READ_REQUEST:
                if(mGattServerProfilesListener != null){
                    if(resultData != null) {
                        String characteristics = resultData.getString(Constants.GATTSERVER_READ_CHARACTERISTICS, null);
                        int requestId = resultData.getInt(Constants.GATTSERVER_READ_REQUESTID, 0);
                        int offset = resultData.getInt(Constants.GATTSERVER_READ_OFFSET, 0);
                        mGattServerProfilesListener.onLeReadRequest(characteristics, requestId, offset);
                    }
                }
                break;
            case Constants.DESCRIPTOR_WRITE_KEY:
                boolean notifyStatus = resultData.getBoolean(
                        Constants.DESCRIPTOR_WRITE_STATUS_KEY);
                if (mGattListener != null)
                    mGattListener.onLeDescriptorWrite(notifyStatus);
                break;

            case Constants.DEVICE_WRITE_REQUEST:
                if(mGattServerProfilesListener != null){
                    if(resultData != null) {
                        String characteristics = resultData.getString(Constants.GATTSERVER_WRITE_CHARACTERISTICS, null);
                        int requestId = resultData.getInt(Constants.GATTSERVER_WRITE_REQUESTID, 0);
                        int offset = resultData.getInt(Constants.GATTSERVER_WRITE_OFFSET, 0);
                        mGattServerProfilesListener.onLeWriteRequest(characteristics, requestId, offset);
                    }
                }
                break;

            case Constants.DEVICE_SERVICES_DISCOVERED:
                ////Log.v(TAG, "DEVICE_SERVICES_DISCOVERED " + mGattListener);
                if(mConnectionListener != null){
                    if(resultData != null) {
                        boolean serviceDiscoveredStatus = resultData.getBoolean(
                                Constants.GATTSERVER_SERVICES_DISCOVERED_STATUS);
                        String serviceUuid = resultData.getString(Constants.GATTSERVER_SERVICES_ADDED_SERVICE);
                        mConnectionListener.onLeServicesDiscovered(serviceDiscoveredStatus, serviceUuid);
                    }
                }
                break;

            default:
                break;

        }
    }

    public interface BLEConnection{
        //BLE connection Related
        void onLeDeviceConnected(String deviceName);
        void onLeServiceDiscovered(boolean status);
        void onLeDeviceDisconnected();

        //Fro Bluetooth Status
        void onLeBluetoothStatusChanged();

        void onLeServicesDiscovered(boolean status, String serviceUuid);


    }
    public interface GATTProfiles{
        void onLeCharacteristicChanged(boolean status);
        void onLeCharacteristicWrite(boolean status);
        void onLeCharacteristicRead(boolean status);
        void onLeCharacteristicRead(boolean status, String data, int code);
        void onLeReadRemoteRssi(int rssi);
        void onLeDescriptorWrite(boolean status);

    }


    public interface GattServerProfiles{
        void onLeWriteRequest(String characteristics, int requestid, int offset);
        void onLeReadRequest(String characteristics, int requestid, int offset);
        void onLeNotifyRequest();
        void onLeWriteRequest(String characteristics, int requestid, int offset, byte[] value);

    }



    public interface LeDeviceConnected {
        void onLeDeviceConnected(String deviceName);
    }

    public interface LeDeviceDisconnected {
        void onLeDeviceDisconnected();
    }

    public interface LeServiceDiscoveryStatus {
        void onLeServiceDiscovered(boolean status);
    }

    public interface LeCharacteristicChanged {
        void onLeCharacteristicChanged(boolean status);
    }

    public interface LeCharacteristicWriteStatus {
        void onLeCharacteristicWrite(boolean status);
    }

    public interface LeCharacteristicReadStatus {
        void onLeCharacteristicRead(boolean status);
    }

    public interface LePairingCallbacks {

        void onLePairingInprogressStatus(boolean status);

        void onLePairingDoneStatus(boolean status);

        void onLePairingFailedStatus(boolean status);
    }

    public interface LePairingRequest {
        void onLeLePairingRequest();
    }

}
