/*
 *   BLEConnection.java
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.connection;

import android.app.ActivityManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;


import com.atmel.wearable.commonutils.AppConstants;
import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.commonutils.Constants;
import com.atmel.wearable.commonutils.GattAttributes;
import com.atmel.wearable.models.CharacteristicChangeEventModel;
import com.atmel.wearable.models.CharacteristicReadEventModel;
import com.atmel.wearable.models.DescriptorWriteModel;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import de.greenrobot.event.EventBus;


public class BLEConnection extends Service {

    /**
     * Connection status Constants
     */
    public static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_DISCONNECTING = 4;
    private final static String TAG = BLEConnection.class.getSimpleName();
    public static Boolean mReconnectionEnabled = false;
    public static int mBondState = BluetoothDevice.BOND_NONE;
    public static BluetoothGattServer mGattServer;
    private static BluetoothAdapter mBluetoothAdapter;
    private static BluetoothGatt mBluetoothGatt;
    private static BluetoothDevice mDevice;
    private static Context mContext;
    private static int mConnectionState = STATE_DISCONNECTED;
    private static int mGattServerConnectionState = STATE_DISCONNECTED;
    private static ResultReceiver mResultReceiver;
    private static EventBus eventBus = new EventBus();
    /**
     * Device address
     */
    private static String mBluetoothDeviceAddress;
    private static String mBluetoothDeviceName;


    private final static BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {


        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            // GATT Server connected
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                    mConnectionState = STATE_CONNECTED;
                    mBondState = BluetoothDevice.BOND_NONE;
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DEVICE_CONNECTED_NAME_KEY, gatt.getDevice().getName());


                    /**
                     * Checking if the application running sent to background or foreground.
                     */

                    if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                        if (!isApplicationBackgroundCompat(mContext.getApplicationContext())) {
                            mResultReceiver.send(Constants.DEVICE_CONNECTED_KEY, bundle);
                        }
                    } else {
                        if (!isApplicationBackground(mContext.getApplicationContext())) {
                            mResultReceiver.send(Constants.DEVICE_CONNECTED_KEY, bundle);
                        }
                    }
                    BLEConnection.setDeviceAddressNState(mContext, newState);
                }
            // GATT Server disconnected
            else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    mConnectionState = STATE_DISCONNECTED;
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DEVICE_DISCONNECTED_NAME_KEY, gatt.getDevice().getName());

                    /**
                     * Checking if the application running sent to background or foreground.
                     */
                    if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                        if (!isApplicationBackgroundCompat(mContext.getApplicationContext())) {
                            mResultReceiver.send(Constants.DEVICE_DISCONNECTED_KEY, bundle);
                        }
                    } else {
                        if (!isApplicationBackground(mContext.getApplicationContext())) {
                            mResultReceiver.send(Constants.DEVICE_DISCONNECTED_KEY, bundle);
                        }
                    }
                    BLEConnection.setDiscconectDeviceAddressNState(mContext, newState);
                }
            // GATT Server disconnected
            else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                mConnectionState = STATE_DISCONNECTING;
                BLEConnection.setDiscconectDeviceAddressNState(mContext, newState);

            }
        }


        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            // GATT Services discovered
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.SERVICE_DISCOVERED_STATUS_KEY,
                        true);
                mResultReceiver.send(Constants.SERVICE_DISCOVERED_KEY, bundle);
            } else {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.SERVICE_DISCOVERED_STATUS_KEY,
                        false);
                mResultReceiver.send(Constants.SERVICE_DISCOVERED_KEY, bundle);

            }
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.REMOTE_RSSI_VALUE, rssi);
            mResultReceiver.send(Constants.REMOTE_RSSI_KEY, bundle);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            byte[] val = descriptor.getValue();
            String characteristic = descriptor.getCharacteristic().getUuid().toString();
            Bundle bundle = new Bundle();
            switch (val[0]) {
                case 0:
                    bundle.putBoolean(Constants.DESCRIPTOR_WRITE_STATUS_KEY,
                            false);
                    break;
                case 1:
                    bundle.putBoolean(Constants.DESCRIPTOR_WRITE_STATUS_KEY,
                            true);
                    break;
                case 2:
                    bundle.putBoolean(Constants.DESCRIPTOR_WRITE_STATUS_KEY,
                            true);
                    break;
            }

            bundle.putString(Constants.DESCRIPTOR_WRITE_CHARACTERISTIC, characteristic);
            mResultReceiver.send(Constants.DESCRIPTOR_WRITE_KEY, bundle);

            DescriptorWriteModel model = new DescriptorWriteModel().getDescriptorWriteModel(status, descriptor.getCharacteristic(),descriptor);
            eventBus.post(model);

            if (status == BluetoothGatt.GATT_SUCCESS) {


            } else if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) {
               // //Log.v(TAG, characteristic + " Pairing required");

            } else {
             //   //Log.v(TAG, "status code--" + status);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic
                characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            CharacteristicChangeEventModel ccEvent = new CharacteristicChangeEventModel(gatt, characteristic);
            eventBus.post(ccEvent);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.CHARACTERISTIC_WRITE_STATUS_KEY,
                    true);
            mResultReceiver.send(Constants.CHARACTERISTICS_WRITE_KEY, bundle);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
          //  //Log.d("BLE Connection ", "onCharacteristicRead " + characteristic.getUuid());

            CharacteristicReadEventModel charReadEvent = new CharacteristicReadEventModel(gatt, characteristic, status);
            eventBus.post(charReadEvent);

            String mValue;
            Bundle bundle = new Bundle();
            switch (characteristic.getUuid().toString()) {

                default:
                    break;
            }
            bundle.putBoolean(Constants.CHARACTERISTIC_READ_STATUS_KEY,
                    true);
            mResultReceiver.send(Constants.CHARACTERISTICS_READ_KEY, bundle);
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
          //  //Log.e("onMtuChanged", "onMtuChanged " + mtu);
        }
    };

    private static BluetoothManager mBluetoothManager;
    /**
     * Broadcast receiver for detecting the device bluetooth sttus change and pairing process
     */
    private final BroadcastReceiver bluetoothStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            //Received when the bond state is changed
            ////Log.e("bluetoothStatusReceiver", "" + mResultReceiver);
            ////Log.e("Context in BT Brdcast", "" + mContext);

            if (mResultReceiver != null) {

                /**
                 * Checking if the application running sent to background or foreground.
                 */

                if (Build.VERSION_CODES.KITKAT > Build.VERSION.SDK_INT) {
                    if (!isApplicationBackgroundCompat(mContext.getApplicationContext())) {
                        bluetoothStatusChecking(action, intent);
                    }
                } else {
                    if (!isApplicationBackground(mContext.getApplicationContext())) {
                      //  //Log.e("inside ", "isApplicationBackground");
                        bluetoothStatusChecking(action, intent);
                    } else {
                     //   //Log.e("else ", "isApplicationBackground");

                    }
                }
            }
        }
    };

    /**
     * indicates how to behave if the service is killed
     */
    int mStartMode;
    /**
     * interface for clients that bind
     */
    IBinder mBinder;
    /**
     * indicates whether onRebind should be used
     */
    boolean mAllowRebind;

    public static int getmConnectionState() {
        return mConnectionState;
    }

    public static int getmServerConnectionState() {
        return mGattServerConnectionState;
    }

    public static String getmBluetoothDeviceAddress() {
        return mBluetoothDeviceAddress;
    }

    /**
     * Connects to the GATT server hosted on the BlueTooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The
     * connection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public static void connect(final String address, final String devicename, Context context) {
        //mContext = context;

        if (mBluetoothAdapter == null || address == null) {
            return;
        }
        mDevice = mBluetoothAdapter.getRemoteDevice(address);
        /*if (mDevice == null) {
            return;
        }*/
        // We want to directly connect to the device, so we are setting the
        // autoConnect
        // parameter to false.
        if (mBluetoothGatt != null)
            mBluetoothGatt.close();
        mBluetoothGatt = mDevice.connectGatt(mContext, false, mGattCallback);
        refreshDeviceCache(mBluetoothGatt);
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        mReconnectionEnabled = true;
    }


    public static void connectGattServer(final String address, final String devicename, Context context) {

        mContext = context;
        if (mBluetoothAdapter == null || address == null) {
            return;
        }
        mDevice = mBluetoothAdapter
                .getRemoteDevice(address);
        /*if (mDevice == null) {
            return;
        }*/
        if (mGattServer != null) {

            mGattServer.connect(mDevice, false);
        }
        mReconnectionEnabled = true;
    }

    public static boolean getConnectionState(final String address) {
        ////Log.e("Adrress Scanned>>", "" + address);
        if (mBluetoothAdapter == null || address == "") {
            return false;
        }
//        //Log.e("Adrress Saved>>", "" + BLEConnection.getDeviceAddress(mContext));
//        //Log.e("State Saved>>", "" + BLEConnection.getDeviceState(mContext));
        if (address.equalsIgnoreCase(BLEConnection.getDeviceAddress(mContext)) && BLEConnection.getDeviceState(mContext) == 2) {
            return true;
        }
        return false;
    }


    /**
     * Saves connected devices address
     */
    public static void setDeviceAddressNState(Context context, int state) {
        String deviceAddress = BLEConnection.getmBluetoothDeviceAddress();
        AppUtils.setStringSharedPreference(context,
                AppConstants.PREF_DEV_ADDRESS, deviceAddress);
        AppUtils.setIntSharedPreference(context,
                AppConstants.PREF_CONNECTION_STATE, state);
    }

    public static void setDiscconectDeviceAddressNState(Context context, int state) {
        String deviceAddress = BLEConnection.getmBluetoothDeviceAddress();
        AppUtils.setStringSharedPreference(context,
                AppConstants.PREF_DEV_ADDRESS, deviceAddress);
        AppUtils.setIntSharedPreference(context,
                AppConstants.PREF_CONNECTION_STATE, state);
    }

    /**
     * Saves connected devices address
     */
    public static String getDeviceAddress(Context context) {
        return AppUtils.getStringSharedPreference(context,
                AppConstants.PREF_DEV_ADDRESS);
    }

    /**
     * Saves connected devices state
     */
    public static int getDeviceState(Context context) {
        return AppUtils.getIntSharedPreference(context,
                AppConstants.PREF_CONNECTION_STATE);
    }

    /**
     * Connects to the GATT server hosted on the BlueTooth LE device.
     *
     * @return Return true if the connection is initiated successfully. The
     * connection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public static void autoConnect(Context context) {
        //Log.e(TAG, "autoConnect");

        if (mDevice == null) {
            return;
        }
        // We want to directly connect to the device, so we are setting the
        // autoConnect
        // parameter to false.
        if (mBluetoothGatt != null) mBluetoothGatt.close();
        mBluetoothGatt = mDevice.connectGatt( mContext, true, mGattCallback);
       // refreshDeviceCache(mBluetoothGatt);
        mConnectionState = STATE_CONNECTING;
    }

    public static boolean refreshDeviceCache(BluetoothGatt gatt) {
        try {
            Method localMethod = gatt.getClass().getMethod("refresh");
            if (localMethod != null) {
                return (Boolean) localMethod.invoke(gatt);
            }
        } catch (Exception localException) {
           // //Log.v(TAG, "Exeception occured while clearing cache");
        }
        return false;
    }

    public static void closeGatt(){

        try{
            mBluetoothGatt.close();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The
     * disconnection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public static void disconnect() {
        try {
            if (mBluetoothAdapter == null || mBluetoothGatt == null) {
                return;
            }

            //Log.e(TAG, "disconnect bleconnection");

            mBluetoothGatt.disconnect();
            mBluetoothGatt.close();

            mBluetoothGatt = null;
            mConnectionState =  STATE_DISCONNECTING;;
           /* Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    try {
                        mBluetoothGatt.close();
                        mBluetoothGatt=null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 500);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        // mBluetoothGatt = null;
    }

    public static void discoverServices() {
        if (mBluetoothAdapter != null && mBluetoothGatt != null) {
            mBluetoothGatt.discoverServices();
        }
    }

    public static List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null)
            return null;

        return mBluetoothGatt.getServices();
    }


    public static void setCharacteristicIndication(BluetoothGattCharacteristic characteristic,
                                                   Boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            return;
        }

        if (characteristic.getDescriptor(UUID
                .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG)) != null) {
            if (enabled) {
                BluetoothGattDescriptor descriptor = characteristic
                        .getDescriptor(UUID
                                .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                descriptor
                        .setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);

                mBluetoothGatt.writeDescriptor(descriptor);
            } else {
                BluetoothGattDescriptor descriptor = characteristic
                        .getDescriptor(UUID
                                .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                descriptor
                        .setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                mBluetoothGatt.writeDescriptor(descriptor);

            }
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled        If true, enable notification. False otherwise.
     */
    public static void setCharacteristicNotification(
            BluetoothGattCharacteristic characteristic, boolean enabled) {

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            return;
        }

        if (characteristic.getDescriptor(UUID
                .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG)) != null) {
            if (enabled) {
                BluetoothGattDescriptor descriptor = characteristic
                        .getDescriptor(UUID
                                .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                descriptor
                        .setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                mBluetoothGatt.writeDescriptor(descriptor);
            } else {
                BluetoothGattDescriptor descriptor = characteristic
                        .getDescriptor(UUID
                                .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
                descriptor
                        .setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                mBluetoothGatt.writeDescriptor(descriptor);
            }
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
      //  //Log.d("Status", characteristic.getUuid().toString() + " " + enabled);
    }

    public static void setCharacteristicWriteWithoutResponse(BluetoothGattCharacteristic
                                                                     characteristic,
                                                             byte[] data) {
        if (mBluetoothAdapter != null && mBluetoothGatt != null) {
           // //Log.v(TAG, "" + ByteArraytoHex(data));
            characteristic.setValue(data);
            boolean status = mBluetoothGatt.writeCharacteristic(characteristic);
         //   //Log.d("status", characteristic.getUuid().toString() + " " + status);
        }
    }

    public static void readCharacteristic(
            BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            return;
        }
        boolean status = mBluetoothGatt.readCharacteristic(characteristic);
        ////Log.d("status", characteristic.getUuid().toString() + " " + status);
    }

    public static String ByteArraytoHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();

    }

    public static void readRemoteRssi() {
        try {
            mBluetoothGatt.readRemoteRssi();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static boolean isBonded() {
        Boolean bonded = false;
        try {
            BluetoothDevice device = mBluetoothAdapter
                    .getRemoteDevice(mDevice.getAddress());
            bonded = device.getBondState() == BluetoothDevice.BOND_BONDED;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bonded;
    }

    public static boolean isBonding() {
        Boolean bonded = false;
        try {
            BluetoothDevice device = mBluetoothAdapter
                    .getRemoteDevice(mDevice.getAddress());
            bonded = device.getBondState() == BluetoothDevice.BOND_BONDING;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bonded;
    }

    public static int getmBondstate() {
        int bondstate = 0;
        try {
            if (mBluetoothAdapter != null) {
                BluetoothDevice device = mBluetoothAdapter
                        .getRemoteDevice(mDevice.getAddress());
                bondstate = device.getBondState();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bondstate;

    }

    public static void pairDevice() {
        try {
            Method m = mBluetoothGatt.getDevice()
                    .getClass().getMethod("createBond", (Class[]) null);
            m.invoke(mBluetoothGatt.getDevice(), (Object[]) null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void unPairDevice() {
        try {
            Method m = mBluetoothGatt.getDevice()
                    .getClass().getMethod("removeBond", (Class[]) null);
            m.invoke(mBluetoothGatt.getDevice(), (Object[]) null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BluetoothDevice getDevice() {
        return mDevice;
    }

    public synchronized static void notifyConnectedDevices(BluetoothGattCharacteristic characteristic) {
        try {
            if (mGattServer != null) {
                mGattServer.notifyCharacteristicChanged(mDevice, characteristic, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if the application is being sent in the background (i.e behind
     * another application's Activity).
     *
     * @param context the context
     * @return <code>true</code> if another application will be above this one.
     */

    public static boolean isApplicationBackgroundCompat(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isApplicationBackground(Context context) {
        String[] activePackages = getActivePackages(context);
        if (activePackages.length > 0) {
            for (String activePackage : activePackages) {
                if (activePackage.equals(context.getPackageName())) {
                    return false;
                }
            }
        }
        return true;
    }

    private static String[] getActivePackages(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final Set<String> activePackages = new HashSet<String>();
        final List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        if (processInfos != null) {
            for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    activePackages.addAll(Arrays.asList(processInfo.pkgList));
                }
            }
        }
        return activePackages.toArray(new String[activePackages.size()]);
    }

    private void bluetoothStatusChecking(String action, Intent intent) {
        if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
          //  //Log.v(TAG, "state " + state);
            if (state == BluetoothDevice.BOND_BONDING) {
                mBondState = BluetoothDevice.BOND_BONDING;
                // Bonding...
              //  //Log.v(TAG, "Bonding");
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_PAIRING_STATUS_IN_PROCESS_KEY,
                        true);
                mResultReceiver.send(Constants.DEVICE_PAIR_IN_PROCESS_KEY, bundle);

            } else if (state == BluetoothDevice.BOND_BONDED) {
                mBondState = BluetoothDevice.BOND_BONDED;
                // Bonded...
               // //Log.v(TAG, "Bonded");
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_PAIRING_STATUS_DONE_KEY,
                        true);
                mResultReceiver.send(Constants.DEVICE_PAIR_DONE_KEY, bundle);

            } else if (state == BluetoothDevice.BOND_NONE) {
                mBondState = BluetoothDevice.BOND_NONE;
                // Not bonded...
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_PAIRING_STATUS_FAILED_KEY,
                        true);
                mResultReceiver.send(Constants.DEVICE_PAIR_FAILED_KEY, bundle);
            }
        }
        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) ==
                    BluetoothAdapter.STATE_OFF) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_BLUETOOTH_STATUS_OFF_KEY,
                        true);
                bundle.putBoolean(Constants.DEVICE_BLUETOOTH_KEY,
                        false);
                mResultReceiver.send(Constants.DEVICE_BLUETOOTH_OFF_KEY, bundle);
            }
            if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) ==
                    BluetoothAdapter.STATE_ON) {

                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.DEVICE_BLUETOOTH_STATUS_ON_KEY,
                        true);
                bundle.putBoolean(Constants.DEVICE_BLUETOOTH_KEY,
                        true);
                mResultReceiver.send(Constants.DEVICE_BLUETOOTH_ON_KEY, bundle);
            }

        }
        if (action.equals(BluetoothDevice.ACTION_PAIRING_REQUEST)) {
          //  //Log.e("ACTION_PAIRING_REQUEST", "DEVICE_PAIR_REQUEST");
            mResultReceiver.send(Constants.DEVICE_PAIR_REQUEST, null);
        }

    }

    /**
     * Called when the service is being created.
     */
    @Override
    public void onCreate() {
        mContext = this;
        eventBus = EventBus.getDefault();
        // Initializing the service
        if (!initialize()) {
          //  //Log.v(TAG, "Service not initialized");
        } else {
           // //Log.v(TAG, "Service  initialized");
        }
        /**
         * Broadcast receiver register
         */
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);
        registerReceiver(bluetoothStatusReceiver, filter);
    }

    /**
     * The service is starting, due to a call to startService()
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            mResultReceiver = intent.getParcelableExtra(Constants.RESULT_RECEIVER_KEY);
        } catch (Exception ignored) {

        }
        return mStartMode;
    }

    /**
     * A client is binding to the service with bindService()
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Called when all clients have unbound with unbindService()
     */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /*

    private static  void notifyConnectedDevices() {

        ByteBuffer b = ByteBuffer.allocate(4);
//b.order(ByteOrder.BIG_ENDIAN); // optional, the initial order of a byte buffer is always BIG_ENDIAN.
        b.putInt(0xAABBCCDD);

        byte[] result = b.array();

            BluetoothGattCharacteristic readCharacteristic = mGattServer.getService(UUIDDatabase.UUID_CURRENT_TIME_SERVICE)
                    .getCharacteristic(UUIDDatabase.UUID_CURRENT_TIME);
            readCharacteristic.setValue(result);
            mGattServer.notifyCharacteristicChanged(mDevice, readCharacteristic, false);
        }


    public static void startAdvertising() {
        if (mBluetoothLeAdvertiser == null) return;

        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .build();

        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .addServiceUuid(new ParcelUuid(UUIDDatabase.UUID_CURRENT_TIME_SERVICE))
                .build();

        mBluetoothLeAdvertiser.startAdvertising(settings, data, mAdvertiseCallback);
    }



    private static AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            //Log.i(TAG, "Peripheral Advertise Started.");
            //postStatusMessage("GATT Server Ready");
        }

        @Override
        public void onStartFailure(int errorCode) {
            //Log.w(TAG, "Peripheral Advertise Failed: "+errorCode);
           // postStatusMessage("GATT Server Error "+errorCode);
        }
    };
*/

    /**
     * Called when a client is binding to the service with bindService()
     */
    @Override
    public void onRebind(Intent intent) {

    }

    /**
     * Called when The service is no longer used and is being destroyed
     */
    @Override
    public void onDestroy() {
        unregisterReceiver(bluetoothStatusReceiver);
      //  //Log.v(TAG, "Service stopped");
    }

    /**
     * Initializes a reference to the local BlueTooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter
        // through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                return false;
            }
        }
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        //openGattServer();
        return mBluetoothAdapter != null;

    }


    /***
     * used to broadcasting service
     **/
    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

}
