/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.fragments;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.atmel.wearable.graphdatabase.AcceleroMeterGraphModel;
import com.atmel.wearable.interfaces.AccelerometerCallBack;
import com.atmel.wearable.models.DeviceOrientationModel;
import com.atmel.wearable.R;
import com.atmel.wearable.wearables.ServicesTabActivity;
import com.atmel.wearable.widgets.MultiLineGraph;

import java.util.Date;

import de.greenrobot.event.EventBus;


public class AccelerometerFragment extends Fragment implements AccelerometerCallBack {

    private FrameLayout layout;
    private MultiLineGraph mGraph;
    private EventBus mEventBus = EventBus.getDefault();
    private ToggleButton mStartOrStopButton;
    private View mBottomDummyView;
    private boolean mNormalReconnection = true;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_accelrometer, container, false);
        layout = (FrameLayout) rootView.findViewById(R.id.graph_env);
        mStartOrStopButton = (ToggleButton) rootView.findViewById(R.id.toggleButton);
        mBottomDummyView = (View) rootView.findViewById(R.id.dummy_bottom);
        enableClickAction();
        mGraph = new MultiLineGraph();
        try {
            mGraph.drawGraph(layout, getActivity(), false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
        setLayout();
        return rootView;
    }


    private void setLayout() {
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        mBottomDummyView.setLayoutParams(new RelativeLayout.LayoutParams(width / 9, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    /**
     * toggle button click action
     */
    private void enableClickAction() {
        mStartOrStopButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                if (isChecked) {
                    mNormalReconnection = false;
                    mGraph.setBreakTimeStart();
                } else {
                    mGraph.setBreakTimeStop();
                }
                ((ServicesTabActivity) getActivity()).setPageSwipe(isChecked);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onEventMainThread(DeviceOrientationModel envEvent) {
        if (envEvent != null) {
            Date date = new Date();
            long t = date.getTime();
            AcceleroMeterGraphModel model = new AcceleroMeterGraphModel();
            model.x = envEvent.mX;
            model.y = envEvent.mY;
            model.z = envEvent.mZ;
            model.time = t;
            mGraph.upDateGraph(model);
        }
    }


    @Override
    public void accelerometerFragmentBecameVisible() {
        mStartOrStopButton.setChecked(false);
        mGraph.resetGraph();
    }

    @Override
    public void gyroscopeFragmentBecameVisible() {
        if(mStartOrStopButton.isChecked()) {
            doSameAction();
        }
    }

    private void doSameAction() {
        mGraph.setBreakTimeStop();
        ((ServicesTabActivity) getActivity()).setPageSwipe(true);
    }

    @Override
    public void accelerometerAutoDisconnect() {
       // mStartOrStopButton.setChecked(false);
    }

    @Override
    public void gyroscopeAutoDisconnect() {

    }


}
