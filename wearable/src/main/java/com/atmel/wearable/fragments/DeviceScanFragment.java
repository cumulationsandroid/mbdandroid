/*
 *   DeviceScanFragment.java
 *
 *   This class handles the scanning related functionality
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.fragments;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.atmel.wearable.adapters.RecyclerViewBleDeviceAdapter;
import com.atmel.wearable.commonutils.AppConstants;
import com.atmel.wearable.communicator.BLEDataReceiver;
import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.connection.BLEKitKatScanner;
import com.atmel.wearable.connection.BLELollipopScanner;
import com.atmel.wearable.interfaces.AdapterCallBack;
import com.atmel.wearable.interfaces.BLEDetector;
import com.atmel.wearable.wearables.BLEApplication;
import com.atmel.wearable.wearables.BaseActivity;
import com.atmel.wearable.R;
import com.atmel.wearable.wearables.ServicesTabActivity;
import com.atmel.wearable.wearables.SplashScreenActivity;
import com.atmel.wearable.widgets.PairingAlert;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class DeviceScanFragment extends Fragment implements BLEDetector,
        BLEDataReceiver.BLEConnection, RecyclerViewBleDeviceAdapter.OnDeviceConnectionListener, AdapterCallBack {

    // Stops scanning after 5 seconds.
    private static final long SCAN_PERIOD = 5500;
    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    private static Boolean mReScan = false;
    public RecyclerView mRecyclerView;
    public SwipeRefreshLayout mSwipeRefreshLayout;
    public boolean backButtonPressed = false;
    public String mAddress = null;
    public String mName = null;
    LinearLayoutManager mLayoutManager;
    boolean isBleSupported = false;
    boolean isLollipopDevice = false;
    RecyclerViewBleDeviceAdapter adapter;
    View mCoordinatorLayoutView;
    BaseActivity baseActivity;
    PairingAlert mProgressDialog;
    boolean isAnimationUp = true;
    AppConstants.ADV_CATEGORY category = AppConstants.ADV_CATEGORY.DEFAULT;
    int count = 0;
    boolean mPairRequestInitiated = false;
    String mDeviceName = "";
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<BluetoothDevice> mDevices;


    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            mDevices.clear();
            adapter.clearList();
            adapter.removeCustomList();
            adapter.notifyDataSetChanged();
            scanStart();
            try {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (!backButtonPressed) {
                            mScanning = false;
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }

                }, SCAN_PERIOD);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    };
    private Handler mDeviceConnectionLossHandler;
    private Handler mHandler = null;
    private Runnable mHandlerTask = null;
    private boolean isFirstTime = false;
    private ProgressBar mProgressBar = null;
    private Handler mProgressBarHandler = null;
    private Timer mTimer = null;
    private TimerTask mTimerTask = null;
    private int i = 0;
    private boolean mScanning;
    private long CONNECTION_TIME_OUT = 10000;
    private String TAG = DeviceScanFragment.class.getSimpleName();
    private BLEApplication mBleApplication;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //mBleApplication = (BLEApplication) getActivity().getApplication();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.wearable_device_list,
                container, false);
        baseActivity = (BaseActivity) getActivity();
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.device_recycler_view);
        mCoordinatorLayoutView = rootView.findViewById(R.id.snackbarPosition);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
        mDevices = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        adapter = new RecyclerViewBleDeviceAdapter(mDevices,
                getActivity());
        adapter.setAdapterCallback(this);
        adapter.setonDeviceConnectedListener(this);
        mRecyclerView.setAdapter(adapter);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        mProgressDialog = new PairingAlert(getActivity());

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(getActivity());
            BLELollipopScanner.setScanListner(this);
            isLollipopDevice = true;
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(getActivity());
            BLEKitKatScanner.setScanListner(this);
            isLollipopDevice = false;
        }

        if (isLollipopDevice) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                showSnackBar();
            } else {
                if (mBleLollipopScanner.checkBLESupport()) {
                    isBleSupported = true;
                    mDevices.clear();
                    adapter.clearList();
                    adapter.removeCustomList();
                } else {
                    isBleSupported = false;

                }
            }
        } else {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                showSnackBar();
            } else {
                if (mBleKitkatScanner.checkBLESupport()) {
                    isBleSupported = true;
                    mDevices.clear();
                    adapter.clearList();
                    adapter.removeCustomList();
                } else {
                    isBleSupported = false;
                }
            }
        }

        scanStart();

        return rootView;
    }


    /**
     * This runnable for when the device is not connected and progress is showing concurrently.
     */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (mProgressDialog != null) {
                    if (mProgressDialog.isShowing()) {
                        mScanning = false;
                        Toast.makeText(getActivity(), "Unable to connect the device ..", Toast.LENGTH_SHORT).show();
                        mProgressDialog.dismissAlert();
                        BLEConnection.disconnect();
                    }
                    //scanStart();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * To start LEScan
     */
    private void scanStart() {
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            if (isBleSupported) {
                mScanning = true;
                if (!mReScan) {
                    mDevices.clear();
                    adapter.clearList();
                    adapter.removeCustomList();
                    adapter.notifyDataSetChanged();
                }
                if (isLollipopDevice) {
                    scanLollipopDevice();
                } else {
                    scanNonLollipopDevices();
                }
            } else {
                // show message that Ble not supported
            }
        } else {
            // show message to activate Bluetooth
            showSnackBar();
        }
        getActivity().invalidateOptionsMenu();
    }


    /***
     * scanning for lollipop devices
     ***/
    private void scanLollipopDevice() {

        if (null != mBleLollipopScanner) {
            if (baseActivity.mFilterSelected) {
                if (baseActivity.UUIDs != null && baseActivity.UUIDs.length > 0) {
                    mBleLollipopScanner.startLEScanWithFilters(SCAN_PERIOD, baseActivity.UUIDs);
                } else {
                    mBleLollipopScanner.startLEScan(SCAN_PERIOD);
                }
            } else {
                mBleLollipopScanner.startLEScan(SCAN_PERIOD);
            }
        } else {
            mBleLollipopScanner = BLELollipopScanner.getInstance(getActivity());
            BLELollipopScanner.setScanListner(DeviceScanFragment.this);
            if (baseActivity.mFilterSelected) {
                if (baseActivity.UUIDs != null && baseActivity.UUIDs.length > 0) {
                    mBleLollipopScanner.startLEScanWithFilters(SCAN_PERIOD, baseActivity.UUIDs);
                } else {
                    mBleLollipopScanner.startLEScan(SCAN_PERIOD);
                }
            } else {
                mBleLollipopScanner.startLEScan(SCAN_PERIOD);
            }
        }

    }


    /***
     * Scanning non lollipop devices
     */
    private void scanNonLollipopDevices() {

        if (null != mBleKitkatScanner) {
            if (baseActivity.mFilterSelected) {
                /*
                if (baseActivity.UUIDs != null && baseActivity.UUIDs.length > 0) {
                    mBleKitkatScanner.startLEScanWithFilter(SCAN_PERIOD, baseActivity.UUIDs);
                } else {*/
                mBleKitkatScanner.startLEScan(SCAN_PERIOD);
                //  }
            } else {
                mBleKitkatScanner.startLEScan(SCAN_PERIOD);
            }
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(getActivity());
            BLEKitKatScanner.setScanListner(DeviceScanFragment.this);
            if (baseActivity.mFilterSelected) {
                if (baseActivity.UUIDs != null && baseActivity.UUIDs.length > 0) {
                    mBleKitkatScanner.startLEScanWithFilter(SCAN_PERIOD, baseActivity.UUIDs);
                } else {
                    mBleKitkatScanner.startLEScan(SCAN_PERIOD);
                }
            } else {
                mBleKitkatScanner.startLEScan(SCAN_PERIOD);
            }
        }

    }


    public void stopScan() {
        mScanning = false;
        if (isBleSupported) {
            if (isLollipopDevice) {
                if (null != mBleLollipopScanner) {

                    mBleLollipopScanner.stopLEScan();
                } else {
                    mBleLollipopScanner = BLELollipopScanner.getInstance(getActivity());
                    BLELollipopScanner.setScanListner(DeviceScanFragment.this);
                    mBleLollipopScanner.stopLEScan();
                }
            } else {
                if (null != mBleKitkatScanner) {
                    mDevices.clear();
                    adapter.clearList();
                    mBleKitkatScanner.stopLEScan();
                } else {
                    mBleKitkatScanner = BLEKitKatScanner.getInstance(getActivity());
                    BLEKitKatScanner.setScanListner(DeviceScanFragment.this);
                    mBleKitkatScanner.stopLEScan();
                }
            }


        }
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        getActivity().invalidateOptionsMenu();
    }


    /**
     *
     */
    public void showSnackBar() {
        Snackbar.make(mCoordinatorLayoutView,
                getString(R.string.bluetooth_message), Snackbar.LENGTH_LONG).setAction(getString(R.string.turn_on),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mBluetoothAdapter.enable();
                    }
                }).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        permissionCheck();
    }

    private void permissionCheck() {
        //Marshmallow permission check
        if (!checkPermission()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    final Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                }
            }, 1000);
            ////Log.e("No Permission>>>>", "" + checkPermission());
        } else {
            adapter.notifyDataSetChanged();
            BLEDataReceiver.setBLEConnectionListener(this);
            if (isLollipopDevice) {
                if (!mBleLollipopScanner.getBluetoothStatus()) {
                    onLeBluetoothStatusChanged();
                }
            } else {
                if (!mBleKitkatScanner.getBluetoothStatus()) {
                    onLeBluetoothStatusChanged();
                }
            }
            backButtonPressed = false;
            //  mPairDone=false;

            if (mBleKitkatScanner != null) {
                BLEKitKatScanner.setScanListner(this);
            }
            if (mBleLollipopScanner != null) {
                BLELollipopScanner.setScanListner(this);
            }

            if (mReScan) {
                scanStart();
            }
            mReScan = false;
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            //Toast.makeText(this, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(mCoordinatorLayoutView, "Permission Granted, Now you can access location data.", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(mCoordinatorLayoutView, "Permission Denied, You cannot access location data.", Snackbar.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getActivity().finish();
                        }
                    }, 1000);
                }
                break;
        }
    }

    @Override
    public void onLeDeviceConnected(final String deviceName) {
        // //Log.e("OnLeDevice connected", "Callbacked");
        mProgressDialog.showAlert();
        mDeviceName = deviceName;
        if (mDeviceConnectionLossHandler != null && runnable != null) {
            mDeviceConnectionLossHandler.removeCallbacks(runnable);
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    if (!mPairRequestInitiated) {
                        //notifyCustomSerialChat();
                        //  startService();
                        showMessage(deviceName);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 1500);

    }


    private void showMessage(String deviceName) {

        try {
            mProgressDialog.setMessage(deviceName + " Connected successfully");
            Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                public void run() {
                    mProgressDialog.dismissAlert();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(AppConstants.GATTSERVER_PROFILES, category);
                    Intent serviceIntent = new Intent(getActivity(), ServicesTabActivity.class);
                    serviceIntent.putExtra(AppConstants.GATTSERVER_PROFILES, bundle);
                    startActivity(serviceIntent);
                    backButtonPressed = true;

                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void leDeviceDetected(final BluetoothDevice mDevice, final int rssi, final List<ParcelUuid> list) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.addDevices(mDevice, rssi, list);
            }
        });
    }


    @Override
    public void leScanStopped() {
        mScanning = false;
        getActivity().invalidateOptionsMenu();
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onDeviceConnected(String address, String name, List<ParcelUuid> uuidList) {
        if (mProgressDialog != null) {
            stopScan();
            BLEDataReceiver.setBLEConnectionListener(this);
            mProgressDialog.setMessage(getResources().getString(R.string.connecting_alert));
            mProgressDialog.showAlert();
        }
        count = 0;
        category = AppConstants.ADV_CATEGORY.DEFAULT;
        mAddress = address;
        mName = name;

        if (category == AppConstants.ADV_CATEGORY.DEFAULT) {
            BLEConnection.connect(address, name, getActivity());
            mDeviceConnectionLossHandler = new Handler();
            mDeviceConnectionLossHandler.postDelayed(runnable, CONNECTION_TIME_OUT);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mBleKitkatScanner != null) {
            BLEKitKatScanner.setScanListner(null);
        }
        if (mBleLollipopScanner != null) {
            BLELollipopScanner.setScanListner(null);
        }
        mReScan = true;
    }


    @Override
    public void onLeBluetoothStatusChanged() {


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                BlueToothOnorOFFUIChange();
            }
        }, 2000);

    }

    private void BlueToothOnorOFFUIChange() {
        if (isLollipopDevice) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showSnackBar();
                    }
                });
            } else {
                isBleSupported = mBleLollipopScanner.checkBLESupport();
            }
        } else {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                //Bluetooth is disabled
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isAnimationUp = false;
                        isFirstTime = false;
                        showSnackBar();
                    }
                });
            } else {
                isBleSupported = mBleKitkatScanner.checkBLESupport();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BLEDataReceiver.setBLEConnectionListener(null);
        if (mHandler != null) {
            mHandler.removeCallbacks(mHandlerTask);
        }
    }


    @Override
    public void onLeServiceDiscovered(boolean status) {

    }

    @Override
    public void onLeDeviceDisconnected() {

        try {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismissAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLeServicesDiscovered(boolean status, String serviceUuid) {
        BluetoothGattService service;
        if (status) {
            //starting individual service for profile

        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i1 = item.getItemId();
        if (i1 == R.id.menu_scan) {
            mDevices.clear();
            scanStart();

        } else if (i1 == R.id.menu_stop) {
            stopScan();

        } else if (i1 == android.R.id.home) {
            stopScan();
            getActivity().finish();
        }
        return true;
    }


    @Override
    public void onMethodCallback() {
        goIfAlreadyConnected();
    }

    public void goIfAlreadyConnected() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.GATTSERVER_PROFILES, category);
        Intent serviceIntent = new Intent(getActivity(), ServicesTabActivity.class);
        serviceIntent.putExtra(AppConstants.GATTSERVER_PROFILES, bundle);
        startActivity(serviceIntent);
        backButtonPressed = true;
    }
}
