/*
 *   BLEConnection.java
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.atmel.wearable.R;
import com.atmel.wearable.commonutils.AppConstants;
import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.graphdatabase.WearableDBHelper;
import com.atmel.wearable.models.EnvironmentEventModel;
import com.atmel.wearable.wearables.EnvironmentGraphActivity;

import java.util.Date;

import de.greenrobot.event.EventBus;

public class EnvironmentFragment extends Fragment implements View.OnClickListener {

    private CardView mTempGraph, mHumGraph, mPressureGraph, mUVGraph;
    private TextView mTempValue, mHumValue, mPressureValue, mUVValue;
    private EventBus mEnvBus = EventBus.getDefault();
    private String TAG = "EnvironmentFragment";
    public static final int REQUEST_CHECK    = 1;
    //Database
    private WearableDBHelper mEnvDBHelper;
    Date date = new Date();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_environment, container, false);
        mTempGraph = (CardView) rootView.findViewById(R.id.temperature_card);
        mHumGraph = (CardView) rootView.findViewById(R.id.humidity_card);
        mPressureGraph = (CardView) rootView.findViewById(R.id.pressure_card);
        mUVGraph = (CardView) rootView.findViewById(R.id.uv_card);
        mTempValue = (TextView) rootView.findViewById(R.id.temperature_value);
        mHumValue = (TextView) rootView.findViewById(R.id.humidity_value);
        mPressureValue = (TextView) rootView.findViewById(R.id.pressure_value);
        mUVValue = (TextView) rootView.findViewById(R.id.uv_value);
        mTempGraph.setOnClickListener(this);
        mHumGraph.setOnClickListener(this);
        mPressureGraph.setOnClickListener(this);
        mUVGraph.setOnClickListener(this);
        if (!mEnvBus.isRegistered(this)) {
            mEnvBus.register(this);
        }

        mEnvDBHelper = new WearableDBHelper(getActivity());
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        //mEnvBus.unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEnvBus.unregister(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Bundle bundle = new Bundle();
        if (id == R.id.temperature_card) {
            bundle.putInt(AppConstants.BUNDLE_ENV_OPTION, 1);
            bundle.putString(AppConstants.BUNDLE_GRAPH_TITLE, "Temperature");
            bundle.putString(AppConstants.BUNDLE_GRAPH_XAXIS, "Time");
            bundle.putString(AppConstants.BUNDLE_GRAPH_YAXIS, getTemperatureUnit());
            showGraph(bundle);

        } else if (id == R.id.humidity_card) {
            bundle.putInt(AppConstants.BUNDLE_ENV_OPTION, 2);
            bundle.putString(AppConstants.BUNDLE_GRAPH_TITLE, "Humidity");
            bundle.putString(AppConstants.BUNDLE_GRAPH_XAXIS, "Time");
            bundle.putString(AppConstants.BUNDLE_GRAPH_YAXIS, "%RH");
            showGraph(bundle);

        } else if (id == R.id.pressure_card) {
            bundle.putInt(AppConstants.BUNDLE_ENV_OPTION, 3);
            bundle.putString(AppConstants.BUNDLE_GRAPH_TITLE, "Pressure");
            bundle.putString(AppConstants.BUNDLE_GRAPH_XAXIS, "Time");
            bundle.putString(AppConstants.BUNDLE_GRAPH_YAXIS, "mbar");
            showGraph(bundle);

        } else if (id == R.id.uv_card) {
            bundle.putInt(AppConstants.BUNDLE_ENV_OPTION, 4);
            bundle.putString(AppConstants.BUNDLE_GRAPH_TITLE, "Light");
            bundle.putString(AppConstants.BUNDLE_GRAPH_XAXIS, "Time");
            bundle.putString(AppConstants.BUNDLE_GRAPH_YAXIS, "lx");
            showGraph(bundle);

        }
    }

    private void showGraph(Bundle bundle) {
        Intent graphIntent = new Intent(getActivity(), EnvironmentGraphActivity.class);
        graphIntent.putExtras(bundle);
        startActivityForResult(graphIntent, 1);
    }

    public void onEventMainThread(EnvironmentEventModel envEvent) {
        if (envEvent != null) {
                Date date = new Date();
                mTempValue.setText(checkTemperatureType(envEvent.getTemperature().floatValue()));
                mPressureValue.setText(envEvent.getPressure() + " mbar");
                mUVValue.setText(envEvent.getUV() + " lx");
                mHumValue.setText(envEvent.getHumidity() + " %RH");
//                mEnvDBHelper.insertEvironmentValues(BLEConnection.getmBluetoothDeviceAddress(), date.getTime(), envEvent.getTemperature(), envEvent.getPressure(),
//                        envEvent.getHumidity(), envEvent.getUV());
//                mEnvDBHelper.close();
        }
    }

    private String checkTemperatureType(float value) {
        if (AppUtils.getTemperatureUnit(getActivity()).equals(getString(R.string.degree_celsius_label))) {
            return value + " " + getString(R.string.degree_celsius);
        } else {
            return AppUtils.convertToFahrenhite(value) + " " + getString(R.string.fahrenheit);
        }
    }


    private String getTemperatureUnit() {
        if (AppUtils.getTemperatureUnit(getActivity()).equals(getString(R.string.degree_celsius_label))) {
            return getString(R.string.degree_celsius);
        } else {
            return getString(R.string.fahrenheit);
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK) {
            if (resultCode == Activity.RESULT_CANCELED) {
                getActivity().finish();
            }
        }
    }
}
