/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.fragments;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.atmel.wearable.models.Plot3dModel;
import com.atmel.wearable.R;

import de.greenrobot.event.EventBus;
import min3d.Shared;
import min3d.core.Object3dContainer;
import min3d.core.Renderer;
import min3d.core.Scene;
import min3d.interfaces.ISceneController;
import min3d.parser.IParser;
import min3d.parser.Parser;
import min3d.vos.Light;

/**
 * Created by sreejith on 23/11/15.
 */
public class Plot3DFragment extends Fragment implements ISceneController {
    public Scene scene;
    public Object3dContainer cube;
    public double x = 0, y=0, z =0, w;
    protected Handler _initSceneHander;
    protected Handler _updateSceneHander;
    public boolean _renderContinuously;
    private EventBus mEnvBus = EventBus.getDefault();
    private double dummyValue =00,oldValue =0.002;
    protected GLSurfaceView _glSurfaceView;
    final Runnable _initSceneRunnable = new Runnable() {
        public void run() {
            onInitScene();
        }
    };
public Context context;
    final Runnable _updateSceneRunnable = new Runnable() {
        public void run() {
            onUpdateScene();
        }
    };
    Handler mHandler = new Handler();

    @Override
    public void onPause() {
        super.onPause();
        _glSurfaceView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        _glSurfaceView.onResume();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _initSceneHander = new Handler();
        _updateSceneHander = new Handler();
        Shared.context(getContext());
        scene = new Scene(this);
        Renderer r = new Renderer(scene);
        Shared.renderer(r);
        _glSurfaceView = new GLSurfaceView(getContext());
//        glSurfaceViewConfig();
        _glSurfaceView.setRenderer(r);
//        renderContinuously(false);
        _glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_plot_3d, container, false);
        final LinearLayout ll1 = (LinearLayout) v.findViewById(R.id.progressview);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ll1.setVisibility(View.INVISIBLE);
            }
        }, 3100);

        ll1.setVisibility(View.VISIBLE);
        LinearLayout ll = (LinearLayout) v.findViewById(R.id.scene1Holder);
        ll.addView(_glSurfaceView);
//     _glSurfaceView.postDelayed(new Runnable() { public void run() { _glSurfaceView.setVisibility(View.GONE); } }, 3000);
//        renderContinuously(true);
        if (!mEnvBus.isRegistered(this)) {
            mEnvBus.register(this);
        }
        return v;
    }



    protected void glSurfaceViewConfig() {
//        _glSurfaceView.setPreserveEGLContextOnPause(true);
//        _glSurfaceView.setZOrderMediaOverlay(true);
//        _glSurfaceView.setZOrderOnTop(true);
//        _glSurfaceView.setEGLConfigChooser(8,8,8,8, 16, 0);
//        _glSurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);

    }

    protected GLSurfaceView glSurfaceView() {
        return _glSurfaceView;
    }

    @Override
    public void initScene() {
        //Log.v("sss","aaaaa");
//        Light light = new Light();
////        light.ambient.setAll(1, 1, 1, 1);
//        light.position.setAll(1, 1, 1);
//        scene.lights().add(light);
        Light light = new Light();
        light.position.setAllFrom(scene.camera().position);
        scene.lights().add(light);
        scene.backgroundColor().setAll(0xffffff);


        try {
            IParser parser = Parser.createParser(Parser.Type.OBJ,
                    getResources(), "com.microchip.bluetooth.data:raw/cube_obj", true);
           parser.parse();
            cube = parser.getParsedObject();
            cube.scale().x = cube.scale().y = cube.scale().z = .08f;
            cube.position().x=.3f;
            scene.addChild(cube);
            scene.camera().target = cube.position();
       }catch(OutOfMemoryError e){
            e.printStackTrace();
            //Toast.makeText(context,"Cant display the 3d plot",Toast.LENGTH_SHORT).show();
       }


    }

    @Override
    public void updateScene() {
       // //Log.v("sss","bbbbb");

    }

    public void onInitScene() {
       // //Log.v("sss","ccccc");

    }

    public void onUpdateScene() {
       // //Log.v("sss","ddddd");

    }

    public void renderContinuously(boolean $b) {
        _renderContinuously = $b;
        if (_renderContinuously)
            _glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

        else
            _glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public Handler getInitSceneHandler() {
        return _initSceneHander;
    }

    public Handler getUpdateSceneHandler() {
        return _updateSceneHander;
    }

    public Runnable getInitSceneRunnable() {
        return _initSceneRunnable;
    }

    public Runnable getUpdateSceneRunnable() {
        return _updateSceneRunnable;
    }


    public void onEventMainThread(Plot3dModel event3D) {

//        //Log.d("DDD","----------event3D.getX()==="+event3D.getX());
//        //Log.d("DDD","----------event3D.getY()==="+event3D.getY());
//        //Log.d("DDD","----------event3D.getZ()==="+event3D.getZ());


        if (event3D != null) {

//            if(event3D.getX()== dummyValue) {
//                if(oldValue<1) {
//                    oldValue = oldValue+0.01;
//                } else  {
//                    oldValue = -1;
//                }
//
//            } else {
//                oldValue = dummyValue = event3D.getX();
//            }

            cube.quaternion().rotateThis(event3D.getX(),event3D.getY(),event3D.getZ(),event3D.getW());
         }
    }


}
