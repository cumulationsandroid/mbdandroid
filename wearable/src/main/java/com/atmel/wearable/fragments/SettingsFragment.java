/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.fragments;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.commonutils.GattAttributes;
import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.graphdatabase.WearableDBHelper;
import com.atmel.wearable.models.CharacteristicReadEventModel;
import com.atmel.wearable.models.StepClearModel;
import com.atmel.wearable.R;
import com.atmel.wearable.wearables.ServicesTabActivity;
import com.atmel.wearable.widgets.CustomSpinner;

import de.greenrobot.event.EventBus;


public class SettingsFragment extends Fragment {
    private RadioGroup mRadioGroup;
    private RadioButton mCelsiusRadioButton, mFahrenheitRadioButton;
    private Switch mLowBatteryAlertSwitch, mOutOfRangeAlertSwitch, mDropAlertSwitch;
    private CustomSpinner mEnvironmentSpinner, mAccelerometerSpinner;
    private Button mStepClearButton;
    private ArrayAdapter<CharSequence> envAdapter, acceleroAdapter;
    private TextView mEnvODRtext, mMotionODRText;
    //Database
    private WearableDBHelper mStepDBHelper;
    //EventBus
    private EventBus mReadBus = EventBus.getDefault();


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings, container, false);
        mRadioGroup = (RadioGroup) v.findViewById(R.id.temperature_radio_group);
        mCelsiusRadioButton = (RadioButton) v.findViewById(R.id.degree_celsius);
        mFahrenheitRadioButton = (RadioButton) v.findViewById(R.id.fahrenheit);
        mLowBatteryAlertSwitch = (Switch) v.findViewById(R.id.range_switch);
        mOutOfRangeAlertSwitch = (Switch) v.findViewById(R.id.out_range_switch);
        mDropAlertSwitch = (Switch) v.findViewById(R.id.drop_switch);
        mStepClearButton = (Button) v.findViewById(R.id.clear_graph_text);
        mEnvironmentSpinner = (CustomSpinner) v.findViewById(R.id.env_odr_spinner);
        mAccelerometerSpinner = (CustomSpinner) v.findViewById(R.id.accelero_odr_spinner);
        mEnvODRtext = (TextView) v.findViewById(R.id.environment_odr_text);
        mMotionODRText = (TextView) v.findViewById(R.id.accelero_odr_text);

        mStepDBHelper = new WearableDBHelper(getActivity());

        if (!mReadBus.isRegistered(this)) {
            mReadBus.register(this);
        }

      //  //Log.i("Connection State", "" + BLEConnection.getmConnectionState());
        try {
            if (BLEConnection.getmConnectionState() == 2) {
                ((ServicesTabActivity) getActivity()).readEnvironmentODR();
            } else {
                mEnvODRtext.setTextColor(getActivity().getResources().getColor(R.color.gray_light));
                mMotionODRText.setTextColor(getActivity().getResources().getColor(R.color.gray_light));
                //mEnvironmentSpinner.setBackgroundColor(getActivity().getResources().getColor(R.color.gray_light));
                //mAccelerometerSpinner.setBackgroundColor(getActivity().getResources().getColor(R.color.gray_light));
                mEnvironmentSpinner.setClickable(false);
                mAccelerometerSpinner.setClickable(false);
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        envAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.env_odr_array, android.R.layout.simple_spinner_item);
        envAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mEnvironmentSpinner.setAdapter(envAdapter);
        mEnvironmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Log.i("Setting Env", "" + position);
                byte[] bytes = new byte[1];
                bytes[0] = (byte) (position + 1);
                try {
                    if (BLEConnection.getmConnectionState() == BluetoothGatt.STATE_CONNECTED) {
                        ((ServicesTabActivity) getActivity()).writeEnvironmentODR(bytes);
                    }
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        acceleroAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.accelero_odr_array, android.R.layout.simple_spinner_item);
        acceleroAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAccelerometerSpinner.setAdapter(acceleroAdapter);
        mAccelerometerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Log.i("Setting Acelero", "" + position);
                byte[] bytes = new byte[1];
                bytes[0] = (byte) (position + 1);
                try {
                    if (BLEConnection.getmConnectionState() == BluetoothGatt.STATE_CONNECTED) {
                        ((ServicesTabActivity) getActivity()).writeMotionODR(bytes);
                    }
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mRadioGroup.setOnCheckedChangeListener(radioGroupOnCheckedChangeListener);
        if (AppUtils.getTemperatureUnit(getActivity()).equals("CELSIUS")) {
            mCelsiusRadioButton.setChecked(true);
        } else {
            mFahrenheitRadioButton.setChecked(true);
        }
        // initial setup of switch
        if (AppUtils.getBatteryAlert(getActivity())) {
            mLowBatteryAlertSwitch.setChecked(true);
        } else {
            mLowBatteryAlertSwitch.setChecked(false);
        }
        doLowBatterySwitchOperation();

        // initial setup of switch
        if (AppUtils.getDropAlert(getActivity())) {
            mDropAlertSwitch.setChecked(true);
        } else {
            mDropAlertSwitch.setChecked(false);
        }
        doDropSwitchOperation();

        if (AppUtils.getOutOfRangeAlert(getActivity())) {
            mOutOfRangeAlertSwitch.setChecked(true);
        } else {
            mOutOfRangeAlertSwitch.setChecked(false);
        }
        doOutOfRangeOperation();

        mStepClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStepDBHelper.clearStepCount(BLEConnection.getmBluetoothDeviceAddress());
                AppUtils.setIntSharedPreference(getActivity(), AppUtils.STEP_COUNTER, 0);
                ServicesTabActivity.mCounter = 0;
                StepClearModel clear = new StepClearModel();
                clear.mClear = true;
                mReadBus.post(clear);
            }
        });

        return v;

    }

    public void onEventMainThread(CharacteristicReadEventModel event) {
        if (event != null) {
            String characteristicUUID = event.getBleGattCharacteristic().getUuid().toString();
            switch (characteristicUUID) {
                case GattAttributes.ENVIRONMENT_ODR_CHARCTERISTICS:
                    try {
                        if (event.getStatus() == BluetoothGatt.GATT_SUCCESS) {
                            ((ServicesTabActivity) getActivity()).readMotionODR();
                        }
                    } catch (ClassCastException e) {
                        e.printStackTrace();
                    }
                    int envODR = event.getBleGattCharacteristic().getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    //Log.i("Environment ODR READ>>", "" + envODR);
                    mEnvironmentSpinner.setSelection(envODR - 1);
                    //Log.i("Pos Env ODR>>>>>>>>>", "" + (envODR - 1));
                    break;
                case GattAttributes.MOTION_ODR_CHARCTERISTICS:
                    int acceleroODR = event.getBleGattCharacteristic().getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    //Log.i("Motion ODR READ>>", "" + acceleroODR);
                    mAccelerometerSpinner.setSelection(acceleroODR - 1);
                    //Log.i("Pos Motion ODR>>>>>>>>>", "" + (acceleroODR - 1));
                    break;
            }
        }
    }

    private void doOutOfRangeOperation() {
        mOutOfRangeAlertSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AppUtils.setOutOfRangeAlert(getActivity(), true);
                } else {
                    AppUtils.setOutOfRangeAlert(getActivity(), false);
                }
            }
        });
    }

    RadioGroup.OnCheckedChangeListener radioGroupOnCheckedChangeListener =
            new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.degree_celsius) {
                        AppUtils.setTemperatureUnit(getActivity(), "CELSIUS");

                    } else if (checkedId == R.id.fahrenheit) {
                        AppUtils.setTemperatureUnit(getActivity(), "FAHRENHEIT");

                    }
                }
            };


    /**
     * Low battery alert switch click handling
     */
    private void doLowBatterySwitchOperation() {
        mLowBatteryAlertSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
             handleCharactericts(1,isChecked);
             AppUtils.setBatteryAlert(getActivity(), isChecked);
            }
        });
    }

    /**
     * Drop alert switch click handling
     */
    private void doDropSwitchOperation() {
        mDropAlertSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                handleCharactericts(2,isChecked);
                AppUtils.setDropAlert(getActivity(), isChecked);
            }
        });
    }



    /** Enable and Disable characteristics */

    private void handleCharactericts(int pos,boolean status) {
        try {
            if (pos == 1) {
                ((ServicesTabActivity) getActivity()).setBatteryCharacteristics(status);
            } else if (pos == 2) {
                ((ServicesTabActivity) getActivity()).setDropDetectCharacteristics(status);
            } else if (pos == 3) {
                // Handle out of range
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

    }
}



