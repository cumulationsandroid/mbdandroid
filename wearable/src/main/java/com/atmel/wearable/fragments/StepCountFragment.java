/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.fragments;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;

import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.graphdatabase.StepCountGraphModel;
import com.atmel.wearable.graphdatabase.WearableDBHelper;
import com.atmel.wearable.models.StepClearModel;
import com.atmel.wearable.models.StepCountEventModel;
import com.atmel.wearable.R;
import com.atmel.wearable.wearables.ServicesTabActivity;
import com.atmel.wearable.widgets.BarStepGraph;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

public class StepCountFragment extends Fragment implements View.OnClickListener {

    //private StepCountGraph mStepGraph;
    private TextView mStepCounter;
    private SwitchCompat mStartSwitch = null;
    private Button mResetButton;
    private TextView mNotifyText;
    private BluetoothGattCharacteristic mBluetoothCharacteristics;
    private EventBus mEventBus = EventBus.getDefault();

    Date date = new Date();

    //Database
    private WearableDBHelper mStepDBHelper;
    private HashMap<Integer, ArrayList<StepCountGraphModel>> mStepGraphValues;
    private LoadStepGraphData mLoadAsynch;

    private BarStepGraph mStepBargraph;
    int[] mSteps;
    private Timer timer = null;
    private TimerTask timerTask = null;
    private View mBottomDummyView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_step_count, container, false);
        mStepCounter = (TextView) rootView.findViewById(R.id.step_count);
        mStartSwitch = (SwitchCompat) rootView.findViewById(R.id.start_switch);
        mResetButton = (Button) rootView.findViewById(R.id.step_reset);
        mNotifyText = (TextView) rootView.findViewById(R.id.start_text);
        mBottomDummyView = (View) rootView.findViewById(R.id.dummy_bottom);
        checkNotifyOn();
        mStartSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppUtils.setBooleanSharedPreference(getActivity(), AppUtils.STEP_COUNT_NOTIFY, isChecked);
                if (isChecked) {
                    mNotifyText.setText(getString(R.string.step_stop));
                    ((ServicesTabActivity) getActivity()).setStepCountCharacteristics(true);
                    try {
                        mLoadAsynch = new LoadStepGraphData();
                        mLoadAsynch.execute("");
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }
                } else {
                    mNotifyText.setText(getString(R.string.step_start));
                    ((ServicesTabActivity) getActivity()).setStepCountCharacteristics(false);
                }
            }
        });
        mResetButton.setOnClickListener(this);

        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }

        ViewGroup layout = (ViewGroup) rootView.findViewById(R.id.graph_step_count);
        mStepBargraph = new BarStepGraph();
        mStepBargraph.drawGraph(layout, mStepBargraph, getActivity());
        mStepDBHelper = new WearableDBHelper(getActivity());
        if (mStepGraphValues != null) {
            mStepGraphValues.clear();
        }
        setLayout();
        return rootView;
    }

    private void setLayout() {
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        mBottomDummyView.setLayoutParams(new RelativeLayout.LayoutParams(width / 9, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    public void onEventMainThread(StepCountEventModel step) {
        if (step != null) {
            if (mLoadAsynch.getStatus() == AsyncTask.Status.FINISHED) {
                Date date = new Date();
                int stepCount;
                mBluetoothCharacteristics = step.getStepCountGattCharacteristic();
                mStepCounter.setText(String.valueOf(ServicesTabActivity.mCounter));
                stepCount = mStepDBHelper.getStepsPerHour(getDateCurrentHour(date.getTime()), BLEConnection.getmBluetoothDeviceAddress());
                ////Log.d("Steps live>>", " " + getDateCurrentHour(date.getTime()) + " " + stepCount);
                //mStepBargraph.updateCurrentBarGraph(getDateCurrentHour(date.getTime()), stepCount);
                mStepDBHelper.close();
            }
        }
    }

    public void onEventMainThread(StepClearModel clear) {
        if (clear != null) {
            if (mLoadAsynch.getStatus() == AsyncTask.Status.FINISHED && clear.mClear == true) {
                mStepDBHelper.clearStepCount(BLEConnection.getmBluetoothDeviceAddress());
                if (mStepGraphValues != null) {
                    mStepGraphValues.clear();
                }
                mStepBargraph.clearGraph();
                mStepBargraph.updateBarGraph(new int[24]);
                mStepCounter.setText(String.valueOf(ServicesTabActivity.mCounter));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        timer = new Timer();
        timerTask = new StepUpdateTask();
        timer.scheduleAtFixedRate(timerTask, 0, 60000);
        mStepCounter.setText(String.valueOf(AppUtils.getIntSharedPreference(getActivity(), AppUtils.STEP_COUNTER)));
    }


    @Override
    public void onPause() {
        super.onPause();
        timerTask.cancel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
        timerTask.cancel();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
          if(id== R.id.step_reset) {
              StepCountEventModel model = new StepCountEventModel(0);
              mEventBus.post(model);
              ServicesTabActivity.mCounter = 0;
              AppUtils.setIntSharedPreference(getActivity(), AppUtils.STEP_COUNTER, 0);
              mStepCounter.setText(String.valueOf(AppUtils.getIntSharedPreference(getActivity(), AppUtils.STEP_COUNTER)));
          }
    }

    private class StepUpdateTask extends TimerTask {
        @Override
        public void run() {
            try {
                mLoadAsynch = new LoadStepGraphData();
                mLoadAsynch.execute("");
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
    }


    private class LoadStepGraphData extends AsyncTask<String, Void, String> {

        private int mStepHours[] = new int[24];

        @Override
        protected void onPreExecute() {
            mStepBargraph.clearGraph();
            mStepGraphValues = mStepDBHelper.getStepCountBarGraphValues(BLEConnection.getDeviceAddress(getContext()));
        }

        @Override
        protected String doInBackground(String... values) {
            StepCountGraphModel stepModel = new StepCountGraphModel();
            for (Integer key : mStepGraphValues.keySet()) {
                //Log.d("Loaded Step Key: ", "" + key);
                stepModel.stepHours[key] = mStepDBHelper.getStepsPerHour(key, BLEConnection.getDeviceAddress(getContext()));
                //Log.d("Loaded Step Size: ", "" + mStepDBHelper.getStepsPerHour(key, BLEConnection.getDeviceAddress(getContext())));
                mStepHours = stepModel.stepHours;
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            mStepBargraph.updateBarGraph(mStepHours);
            mStepDBHelper.close();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public Integer getDateCurrentHour(long timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH");
            String formattedDate = sdf.format(timestamp);
            return Integer.parseInt(formattedDate);
        } catch (Exception ignored) {
        }
        return 0;
    }


    private void checkNotifyOn() {
        if (AppUtils.getBooleanSharedPreference(getActivity(), AppUtils.STEP_COUNT_NOTIFY)) {
            mStartSwitch.setChecked(true);
            mNotifyText.setText(getString(R.string.step_stop));
        } else {
            mStartSwitch.setChecked(false);
            mNotifyText.setText(getString(R.string.step_start));
        }
    }
}
