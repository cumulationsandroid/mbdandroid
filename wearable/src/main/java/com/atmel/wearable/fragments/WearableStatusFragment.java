/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.atmel.wearable.R;
import com.atmel.wearable.models.RSSIEventModel;
import com.atmel.wearable.widgets.WearableStatusView;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class WearableStatusFragment extends Fragment {

    private EventBus mEventBus = EventBus.getDefault();
    private String TAG = "WearableStatusFragment";
    WearableStatusView mDevice;
    Bitmap mbitmap = null;
    private int mSafeMin = 30;
    private int mSafeMax = 60;
    private int mMidMin = 60;
    private int mMidMax = 80;
    private int mDangerMin = 80;
    private int mDangerMax = 100;

    int average = 0;
    ArrayList<Integer> mRssivalues;
    int position = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_wearable_status, container, false);
        mDevice = (WearableStatusView) rootView.findViewById(R.id.wearable_device);
        mDevice.setRange(mSafeMin, mSafeMax, mMidMin, mMidMax, mDangerMin, mDangerMax);
        mRssivalues = new ArrayList<Integer>(5);

        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }
        return rootView;
    }

    public void onEventMainThread(RSSIEventModel rssi) {
        if (rssi != null) {

            if (position == 10) {
                position = 0;
            } else {
                try {
                    mRssivalues.add(position, rssi.getRSSI());
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
                position++;
            }
            try {
                if (mRssivalues.get(position) != 0) {
                    average = ((mRssivalues.get(0) + mRssivalues.get(1) + mRssivalues.get(2) + mRssivalues.get(3) + mRssivalues.get(4) +
                            mRssivalues.get(5) + mRssivalues.get(6) + mRssivalues.get(7) + mRssivalues.get(8) + mRssivalues.get(9) ) / 10);
                    mDevice.setProgress(average);
                    //Log.e("RSSI>> " + rssi.getRSSI(), "Avg RSSI>>" + average);
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                try {
                    mDevice.setProgress(mRssivalues.get(0));
                }catch (Exception exception){
                    exception.printStackTrace();
                }
            }

        }
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEventBus.unregister(this);
    }
}
