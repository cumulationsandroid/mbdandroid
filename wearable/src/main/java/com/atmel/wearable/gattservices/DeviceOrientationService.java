/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.gattservices;

import android.bluetooth.BluetoothGattCharacteristic;


public class DeviceOrientationService {


    private BluetoothGattCharacteristic mGattCharacteristic;         // object of Gatt characteristic
    int offset = 0;


    /**
     * Create private constructor
     */
    public DeviceOrientationService(BluetoothGattCharacteristic characteristic) {
        this.mGattCharacteristic = characteristic;
    }


    /**
     * Getting X-pos value from Characteristic
     */
    public double getXPos() {
        return mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 0);
    }


    /**
     * Getting Y-pos value from Characteristic
     */
    public double getYPos() {
        return mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 2);
    }

    /**
     * Getting Z-pos value from Characteristic
     */
    public double getZPos() {
        return mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 4);
    }

/**
 * Getting W-pos value from Characteristic
 */
    public double getWPos() {
        return mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 6);
    }
    /**
     * Getting  step count from Characteristic
     */
    public Integer getStepCount() {
        int stepCount = mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        return stepCount;
    }

    /**
     * Getting  value from Characteristic
     */
    public Integer getDropValue() {
        int drop = mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        return drop;
    }
}
