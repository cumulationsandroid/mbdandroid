/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.gattservices;

import android.bluetooth.BluetoothGattCharacteristic;


public class EnvironmentService {

    private BluetoothGattCharacteristic mGattCharacteristic;
    public static final Double mTemperatureResolution = 0.01;
    public static final Integer mPressureResolution = 1;
    public static final Double mUVResolution = 100000.0;
    public static final Integer mHumidityResolution = 1;

    /**
     * Create private constructor
     */
    public EnvironmentService(BluetoothGattCharacteristic characteristic) {
        this.mGattCharacteristic = characteristic;
    }

    /**
     * Getting Temperature value from Characteristic
     */
    public Double getTemperature() {
        int temperature = mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
        return temperature * mTemperatureResolution;
    }


    /**
     * Getting Pressure value from Characteristic
     */
    public Integer getPressure() {
        int pressure = mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 2);
        return pressure * mPressureResolution;
    }


    /**
     * Getting UV value from Characteristic
     */
    public Double getUV() {
        int uv = mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 4);
        return uv / mUVResolution;
    }


    /**
     * Getting Humidity  from Characteristic
     */
    public Integer getHumidity() {
        int humidity = mGattCharacteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 8);
        return humidity * mHumidityResolution;
    }
}
