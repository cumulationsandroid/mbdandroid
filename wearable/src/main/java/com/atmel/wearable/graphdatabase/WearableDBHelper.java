/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.graphdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class WearableDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "AtmelWearable.db";
    public static final String ENVIRONMENT_TABLE_NAME = "environment";
    public static final String ENVIRONMENT_COLUMN_ID = "id";
    public static final String ENVIRONMENT_COLUMN_TIME = "time";
    public static final String ENVIRONMENT_COLUMN_TEMPERATURE = "temperature";
    public static final String ENVIRONMENT_COLUMN_PRESSURE = "pressure";
    public static final String ENVIRONMENT_COLUMN_HUMIDITY = "humidity";
    public static final String ENVIRONMENT_COLUMN_UV = "uv";
    private static final String ENVIRONMENT_COLUMN_DEVICE_ADDRESS = "address";

    private static final String ACCELERO_METER_TABLE_NAME = "accelerometer";
    private static final String ACCELERO_METER_COLUMN_ID = "id";
    private static final String ACCELERO_METER_COLUMN_DEVICE_ADDRESS = "address";
    private static final String ACCELERO_METER_COLUMN_TIME = "time";
    private static final String ACCELERO_METER_COLUMN_X = "x";
    private static final String ACCELERO_METER_COLUMN_Y = "y";
    private static final String ACCELERO_METER_COLUMN_Z = "z";


    private static final String GYROSCOPE_TABLE_NAME = "gyroscope";
    private static final String GYROSCOPE_COLUMN_ID = "id";
    private static final String GYROSCOPE_COLUMN_DEVICE_ADDRESS = "address";
    private static final String GYROSCOPE_COLUMN_TIME = "time";
    private static final String GYROSCOPE_COLUMN_X = "x";
    private static final String GYROSCOPE_COLUMN_Y = "y";
    private static final String GYROSCOPE_COLUMN_Z = "z";

    public static final String STEPCOUNT_TABLE_NAME = "stepcount";
    public static final String STEPCOUNT_COLUMN_ID = "id";
    public static final String STEPCOUNT_COLUMN_TIME = "time";
    public static final String STEPCOUNT_COLUMN_STEP_COUNT = "stepcount";
    private static final String STEPCOUNT_COLUMN_DEVICE_ADDRESS = "address";

    HashMap<Integer, ArrayList<StepCountGraphModel>> stepHashGraphList = new HashMap<>();
    ArrayList<StepCountGraphModel> stepGraphList = new ArrayList<>();
    //ArrayList<StepCountGraphModel> stepHourList = new ArrayList<>();
    StepCountGraphModel stepGraphModel;

    ArrayList<EnvironmentGraphModel> graphList = new ArrayList<>();
    ArrayList<AcceleroMeterGraphModel> acceleroMeterList = new ArrayList<>();
    EnvironmentGraphModel envGraphModel;
    AcceleroMeterGraphModel acceleroGraphModel;


    public WearableDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String envSql = "create table " + ENVIRONMENT_TABLE_NAME + " ( " +
                ENVIRONMENT_COLUMN_ID + " integer primary key autoincrement , " +
                ENVIRONMENT_COLUMN_DEVICE_ADDRESS + " text , " +
                ENVIRONMENT_COLUMN_TIME + " long , " +
                ENVIRONMENT_COLUMN_TEMPERATURE + " double , " +
                ENVIRONMENT_COLUMN_PRESSURE + " integer , " +
                ENVIRONMENT_COLUMN_HUMIDITY + " integer , " +
                ENVIRONMENT_COLUMN_UV + " double " +
                " ) ";

        String stepSql = "create table " + STEPCOUNT_TABLE_NAME + " ( " +
                STEPCOUNT_COLUMN_ID + " integer primary key autoincrement , " +
                STEPCOUNT_COLUMN_DEVICE_ADDRESS + " text , " +
                STEPCOUNT_COLUMN_TIME + " integer , " +
                STEPCOUNT_COLUMN_STEP_COUNT + " integer " +
                " ) ";


        /** Accelero meter table creation */
        String acceleroMeterSql = "create table " + ACCELERO_METER_TABLE_NAME + " ( " +
                ACCELERO_METER_COLUMN_ID + " integer primary key autoincrement , " +
                ACCELERO_METER_COLUMN_DEVICE_ADDRESS + " text , " +
                ACCELERO_METER_COLUMN_TIME + " long , " +
                ACCELERO_METER_COLUMN_X + " double , " +
                ACCELERO_METER_COLUMN_Y + " double , " +
                ACCELERO_METER_COLUMN_Z + " double  " +
                " ) ";

        /** Gyroscope table creation */
        String gyroscopeSql = "create table " + GYROSCOPE_TABLE_NAME + " ( " +
                GYROSCOPE_COLUMN_ID + " integer primary key autoincrement , " +
                GYROSCOPE_COLUMN_DEVICE_ADDRESS + " text , " +
                GYROSCOPE_COLUMN_TIME + " long , " +
                GYROSCOPE_COLUMN_X + " double , " +
                GYROSCOPE_COLUMN_Y + " double , " +
                GYROSCOPE_COLUMN_Z + " double  " +
                " ) ";

        db.execSQL(acceleroMeterSql);
        db.execSQL(envSql);
        db.execSQL(stepSql);
        db.execSQL(gyroscopeSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ENVIRONMENT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ACCELERO_METER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + STEPCOUNT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + GYROSCOPE_TABLE_NAME);
        onCreate(db);
    }

    public boolean insertEvironmentValues(String address, Long time, Double temperature, Integer pressure, Integer humidity, Double uv) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ENVIRONMENT_COLUMN_TIME, time);
        contentValues.put(ENVIRONMENT_COLUMN_DEVICE_ADDRESS, address);
        contentValues.put(ENVIRONMENT_COLUMN_TEMPERATURE, temperature);
        contentValues.put(ENVIRONMENT_COLUMN_PRESSURE, pressure);
        contentValues.put(ENVIRONMENT_COLUMN_HUMIDITY, humidity);
        contentValues.put(ENVIRONMENT_COLUMN_UV, uv);
        db.insert(ENVIRONMENT_TABLE_NAME, null, contentValues);
        db.close(); // Closing database connection
        return true;
    }


    /**
     * Insert values to Accelerometer Table
     */
    public synchronized boolean insertAccelerometerValuesToTable(String address, Long time, Double x, Double y, Double z) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ACCELERO_METER_COLUMN_TIME, time);
        contentValues.put(ACCELERO_METER_COLUMN_DEVICE_ADDRESS, address);
        contentValues.put(ACCELERO_METER_COLUMN_X, x);
        contentValues.put(ACCELERO_METER_COLUMN_Y, y);
        contentValues.put(ACCELERO_METER_COLUMN_Z, z);
        db.insert(ACCELERO_METER_TABLE_NAME, null, contentValues);
        db.close(); // Closing database connection
        return true;
    }


    /**
     * Insert values to Gyroscope Table
     */
    public synchronized boolean insertGyroscopeValuesToTable(String address, Long time, Double x, Double y, Double z) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GYROSCOPE_COLUMN_TIME, time);
        contentValues.put(GYROSCOPE_COLUMN_DEVICE_ADDRESS, address);
        contentValues.put(GYROSCOPE_COLUMN_X, x);
        contentValues.put(GYROSCOPE_COLUMN_Y, y);
        contentValues.put(GYROSCOPE_COLUMN_Z, z);
        db.insert(GYROSCOPE_TABLE_NAME, null, contentValues);
        db.close(); // Closing database connection
        return true;
    }


    public Cursor getGraphData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + ENVIRONMENT_TABLE_NAME + " where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, ENVIRONMENT_TABLE_NAME);
        return numRows;
    }

    public boolean updateEnvironment(Integer id, Double time, Double temperature, Double pressure, Double humidity, Double uv) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ENVIRONMENT_COLUMN_TIME, time);
        contentValues.put(ENVIRONMENT_COLUMN_TEMPERATURE, temperature);
        contentValues.put(ENVIRONMENT_COLUMN_PRESSURE, pressure);
        contentValues.put(ENVIRONMENT_COLUMN_HUMIDITY, humidity);
        contentValues.put(ENVIRONMENT_COLUMN_UV, uv);
        db.update(ENVIRONMENT_TABLE_NAME, contentValues, "id = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Integer clearStepCount(String address) {
        String[] args = {"" + address};
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(STEPCOUNT_TABLE_NAME,
                STEPCOUNT_COLUMN_DEVICE_ADDRESS + " = ? ",
                args);
    }

    public ArrayList<EnvironmentGraphModel> getEnvironmentGraphValues(String address, Long timeLimit) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] args = {"" + timeLimit, "" + address};
        Cursor cursor = db.rawQuery("select * from " + ENVIRONMENT_TABLE_NAME + " where time>? AND " +
                ENVIRONMENT_COLUMN_DEVICE_ADDRESS + " = ? ", args);
     //   //Log.e("DB>>>", "" + cursor.getColumnCount());
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                envGraphModel = new EnvironmentGraphModel();
                envGraphModel.id = (cursor.getInt(cursor.getColumnIndex(ENVIRONMENT_COLUMN_ID)));
                envGraphModel.time = (cursor.getLong(cursor.getColumnIndex(ENVIRONMENT_COLUMN_TIME)));
                envGraphModel.temperature = (cursor.getDouble(cursor.getColumnIndex(ENVIRONMENT_COLUMN_TEMPERATURE)));
                envGraphModel.pressure = (cursor.getInt(cursor.getColumnIndex(ENVIRONMENT_COLUMN_PRESSURE)));
                envGraphModel.humidity = (cursor.getInt(cursor.getColumnIndex(ENVIRONMENT_COLUMN_HUMIDITY)));
                envGraphModel.UV = (cursor.getDouble(cursor.getColumnIndex(ENVIRONMENT_COLUMN_UV)));
                graphList.add(envGraphModel);
            } while (cursor.moveToNext());
        }
        return graphList;
    }


    public synchronized ArrayList<AcceleroMeterGraphModel> getAcceleroMeterValuesFromTable(String address, Long timeLimit) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] args = {"" + timeLimit, "" + address};
        Cursor cursor = db.rawQuery("select * from " + ACCELERO_METER_TABLE_NAME + " where time>? AND " +
                ACCELERO_METER_COLUMN_DEVICE_ADDRESS + " = ? ", args);
        if (cursor.moveToFirst()) {
            do {
                acceleroGraphModel = new AcceleroMeterGraphModel();
                acceleroGraphModel.id = (cursor.getInt(cursor.getColumnIndex(ACCELERO_METER_COLUMN_ID)));
                acceleroGraphModel.time = (cursor.getLong(cursor.getColumnIndex(ACCELERO_METER_COLUMN_TIME)));
                acceleroGraphModel.x = (cursor.getDouble(cursor.getColumnIndex(ACCELERO_METER_COLUMN_X)));
                acceleroGraphModel.y = (cursor.getDouble(cursor.getColumnIndex(ACCELERO_METER_COLUMN_Y)));
                acceleroGraphModel.z = (cursor.getDouble(cursor.getColumnIndex(ACCELERO_METER_COLUMN_Z)));
                acceleroMeterList.add(acceleroGraphModel);
            } while (cursor.moveToNext());
        }
        db.close();
        return acceleroMeterList;
    }


    /***
     * Get Gyroscope values
     * we use same model class
     */

    public synchronized ArrayList<AcceleroMeterGraphModel> getGyroscopeValuesFromTable(String address, Long timeLimit) {
        SQLiteDatabase db = this.getReadableDatabase();


        String[] args = {"" + timeLimit, "" + address};
        Cursor cursor = db.rawQuery("select * from " + GYROSCOPE_TABLE_NAME + " where time>? AND " +
                GYROSCOPE_COLUMN_DEVICE_ADDRESS + " = ? ", args);
        if (cursor.moveToFirst()) {
            do {
                acceleroGraphModel = new AcceleroMeterGraphModel();
                acceleroGraphModel.id = (cursor.getInt(cursor.getColumnIndex(GYROSCOPE_COLUMN_ID)));
                acceleroGraphModel.time = (cursor.getLong(cursor.getColumnIndex(GYROSCOPE_COLUMN_TIME)));
                acceleroGraphModel.x = (cursor.getDouble(cursor.getColumnIndex(GYROSCOPE_COLUMN_X)));
                acceleroGraphModel.y = (cursor.getDouble(cursor.getColumnIndex(GYROSCOPE_COLUMN_Y)));
                acceleroGraphModel.z = (cursor.getDouble(cursor.getColumnIndex(GYROSCOPE_COLUMN_Z)));
                acceleroMeterList.add(acceleroGraphModel);
            } while (cursor.moveToNext());
        }
        //  db.close();
        return acceleroMeterList;
    }


    /**
     * delete Accelero meter values
     */


    public void deleteAcceleroMeterValues(Long timeLimit) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] args = {"" + timeLimit};
        db.delete(GYROSCOPE_TABLE_NAME,
                " time<? ", args);
        //  db.close();
    }

    public boolean insertStepCountValues(String address, Integer hour, Integer steps) {
        SQLiteDatabase db = this.getWritableDatabase();
//        String log = " Time: " + hour + " steps: " + steps;
//        //Log.d("Saved values: ", log);
        ContentValues contentValues = new ContentValues();
        contentValues.put(STEPCOUNT_COLUMN_TIME, hour);
        contentValues.put(STEPCOUNT_COLUMN_DEVICE_ADDRESS, address);
        contentValues.put(STEPCOUNT_COLUMN_STEP_COUNT, steps);
        db.insert(STEPCOUNT_TABLE_NAME, null, contentValues);
        db.close(); // Closing database connection
        return true;
    }

//    public ArrayList<StepCountGraphModel> getStepCountGraphValues(String address, long timelimit) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] args = {"" + timelimit, "" + address};
//        Cursor cursor = db.rawQuery("select * from " + STEPCOUNT_TABLE_NAME + " where time>? AND " +
//                STEPCOUNT_COLUMN_DEVICE_ADDRESS + " = ? ", args);
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                stepGraphModel = new StepCountGraphModel();
//                stepGraphModel.id = (cursor.getInt(cursor.getColumnIndex(STEPCOUNT_COLUMN_ID)));
//                stepGraphModel.hour = (cursor.getLong(cursor.getColumnIndex(STEPCOUNT_COLUMN_TIME)));
//                stepGraphModel.stepCount = (cursor.getInt(cursor.getColumnIndex(STEPCOUNT_COLUMN_STEP_COUNT)));
//                try {
//                    stepGraphList.add(stepGraphModel);
//                } catch (IndexOutOfBoundsException e) {
//                    e.printStackTrace();
//                }
//            } while (cursor.moveToNext());
//        }
//        return stepGraphList;
//    }

    public HashMap<Integer, ArrayList<StepCountGraphModel>> getStepCountBarGraphValues(String address) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + STEPCOUNT_TABLE_NAME + " WHERE " + GYROSCOPE_COLUMN_DEVICE_ADDRESS + " = ? ";
        String[] args = {"" + address};
        Cursor cursor = db.rawQuery(selectQuery, args);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                stepGraphModel = new StepCountGraphModel();
                stepGraphModel.id = (cursor.getInt(cursor.getColumnIndex(STEPCOUNT_COLUMN_ID)));
                stepGraphModel.hour = (cursor.getInt(cursor.getColumnIndex(STEPCOUNT_COLUMN_TIME)));
                stepGraphModel.stepCount = (cursor.getInt(cursor.getColumnIndex(STEPCOUNT_COLUMN_STEP_COUNT)));
                try {
                    stepGraphList.add(stepGraphModel);
                    stepHashGraphList.put(stepGraphModel.hour, stepGraphList);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        return stepHashGraphList;
    }

    public Integer getStepsPerHour(Integer hour, String address) {

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + STEPCOUNT_TABLE_NAME + " WHERE " + GYROSCOPE_COLUMN_DEVICE_ADDRESS + " = ? " + "AND " +
              STEPCOUNT_COLUMN_TIME  + " = ? ";;
        String[] args = {"" + address, "" + hour};
        Cursor cursor = db.rawQuery(selectQuery, args);
        // looping through all rows and adding to list
        return cursor.getCount();
    }

}