/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.models;


public class EnvironmentEventModel {

    private Double mTemperature;
    private Integer mPressure;
    private Double mUV;
    private Integer mHumidity;


    public EnvironmentEventModel(Double temperature, Integer pressure, Double UV, Integer humidity) {
        this.mTemperature = temperature;
        this.mPressure = pressure;
        this.mUV = UV;
        this.mHumidity = humidity;


    }

    public Double getTemperature() {
        return mTemperature;
    }

    public Integer getPressure() {
        return mPressure;
    }

    public Double getUV() {
        return mUV;
    }

    public Integer getHumidity() {
        return mHumidity;
    }

}
