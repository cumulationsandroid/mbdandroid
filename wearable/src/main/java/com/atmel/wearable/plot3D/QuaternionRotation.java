package com.atmel.wearable.plot3D;

import android.content.Context;

import com.atmel.wearable.interfaces.AdapterCallBack;

import min3d.Quaternion;
import min3d.core.Object3dContainer;
import min3d.core.RendererActivity;
import min3d.core.Scene;

public class QuaternionRotation extends RendererActivity
{
    private Object3dContainer cube;
    public float x, y, z, w;
    private Quaternion q;
    private  AdapterCallBack mCallBack;
    Scene scene;
    public QuaternionRotation(Context c) {

    }



    @Override
    public void initScene() {

    }

    @Override
    public void updateScene() {
        cube.quaternion().setRotation(x, y, z, w);
    }




}
