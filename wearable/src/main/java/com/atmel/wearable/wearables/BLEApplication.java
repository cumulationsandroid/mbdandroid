/*
 *   BLEApplication.java
 *
 *   Application class to hold the selected service information
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.wearables;


import android.app.Application;
import android.bluetooth.BluetoothGattService;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class BLEApplication {

    public static float PREV_MIN = 0.0f;
    public static float PREV_MAX = 0.0f;
    private static BLEApplication mInstance;
    private BluetoothGattService mBluetoothGattService;
    private List<BluetoothGattService> mAdvertsingList = new ArrayList<BluetoothGattService>();
    public static Context mContext;

    public static Context getInstance() {
        return mContext;
    }

    public static void setInstance(Context context) {
        if (mContext == null) {
            mContext = context;
        }
    }


    public BluetoothGattService getmBluetoothGattService() {
        return mBluetoothGattService;
    }

    public void setmBluetoothGattService(BluetoothGattService mBluetoothGattService) {
        this.mBluetoothGattService = mBluetoothGattService;
    }

    public void addAdvertisingService(BluetoothGattService service) {
        mAdvertsingList.add(service);
    }

    public List<BluetoothGattService> getAdvertsingList() {

        return mAdvertsingList;
    }

    public void removeAdvertisingList() {

        if (mAdvertsingList != null && mAdvertsingList.size() > 0) {
            mAdvertsingList.clear();
        }
    }

    /**
     * getting app context
     *
     * @return
     */
    public Context getContext() {
        return mContext;
    }
}
