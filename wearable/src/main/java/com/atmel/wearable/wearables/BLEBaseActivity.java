/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.wearables;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.atmel.wearable.R;
import com.atmel.wearable.communicator.BLEDataReceiver;
import com.atmel.wearable.fragments.AboutFragment;
import com.atmel.wearable.fragments.DeviceScanFragment;
import com.atmel.wearable.fragments.SettingsFragment;
import com.atmel.wearable.models.BatteryModel;
import com.atmel.wearable.models.DropModel;
import com.atmel.wearable.widgets.CustomDialog;
import com.atmel.wearable.widgets.RobotoMediumTextView;

import de.greenrobot.event.EventBus;


public class BLEBaseActivity extends AppCompatActivity implements BLEDataReceiver.BLEConnection, BLEDataReceiver.GATTProfiles, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private TextView mWearableText, mSettingsText, mAboutText;
    private Fragment fragment;
    private RobotoMediumTextView mToolbarTitle;
    private EventBus mMainBus = EventBus.getDefault();
    private boolean mShowLowBatteryAlert = false;


    public static void setConnectionListener(BLEDataReceiver.BLEConnection mConnectionListener) {
        BLEDataReceiver.setBLEConnectionListener(mConnectionListener);
    }

    public static void setGattListener(BLEDataReceiver.GATTProfiles mGattListener) {
        BLEDataReceiver.setGattListener(mGattListener);
    }

    /*
    public static void setGattServerListener(BLEDataReceiver.GattServerProfiles mGattServerListener) {
        BLEDataReceiver.setGattServerListener(mGattServerListener);
    }*/

    //    @Override
    protected void onCreateToolbar()
    {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        mToolbarTitle = (RobotoMediumTextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolbarTitle(getResources().getString(R.string.nav_wearables));
        if (!mMainBus.isRegistered(this)) {
            mMainBus.register(this);
        }
    }

    protected void onCreateDrawer(boolean show) {
        // super.onCreate(savedInstanceState);
        //mPairAlert=new PairingAlert(this);
        mWearableText = (TextView) findViewById(R.id.navigation_wearables);
        mSettingsText = (TextView) findViewById(R.id.navigation_settings);
        mAboutText = (TextView) findViewById(R.id.navigation_about);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        mToolbarTitle = (RobotoMediumTextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mWearableText.setOnClickListener(this);
        // Register as a subscriber
        if (!mMainBus.isRegistered(this)) {
            mMainBus.register(this);
        }
        mAboutText.setOnClickListener(this);
        navigationView = (NavigationView) findViewById(R.id.nd_view);
        navigationView.setNavigationItemSelectedListener(this);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        setToolbarTitle(getResources().getString(R.string.nav_wearables));
        if (this.getClass().getSimpleName().equals("ServicesTabActivity")) {
            mWearableText.setText(getResources().getString(R.string.nav_wearable));
            setToolbarTitle(getResources().getString(R.string.nav_wearable));
        }
        showSettingsOnlyForServiceTab(show);
    }


    /** show settings only for ServiceTab activity */
    private void showSettingsOnlyForServiceTab(boolean show) {
        if(show) {
            mSettingsText.setVisibility(View.VISIBLE);
            mSettingsText.setOnClickListener(this);
        } else {
            mSettingsText.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mShowLowBatteryAlert = true;

    }

    @Override
    protected void onPause() {
        super.onPause();
        mShowLowBatteryAlert = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainBus.unregister(this);
    }


    @Override
    public void onLeDeviceConnected(String deviceName) {
    }

    @Override
    public void onLeServiceDiscovered(boolean status) {

    }

    @Override
    public void onLeDeviceDisconnected() {
    }


    @Override
    public void onLeBluetoothStatusChanged() {

    }

    @Override
    public void onLeServicesDiscovered(boolean status, String serviceUuid) {

    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {

    }

    @Override
    public void onLeCharacteristicWrite(boolean status) {

    }

    @Override
    public void onLeCharacteristicRead(boolean status) {

    }

    @Override
    public void onLeCharacteristicRead(boolean status, String data, int code) {

    }

    @Override
    public void onLeReadRemoteRssi(int rssi) {

    }

    @Override
    public void onLeDescriptorWrite(boolean status) {
    }


    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int i = v.getId();
        if (i == R.id.navigation_wearables) {
            setTextSelection(mWearableText);
            if (ServicesTabActivity.active) {
                hidePager(false);
                drawerLayout.closeDrawers();
            } else {

                if (fragment != null && fragment.isVisible()) {
                    setToolbarTitle(getResources().getString(R.string.nav_wearables));
                    fragment = new DeviceScanFragment();
                    drawerLayout.closeDrawers();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame, fragment)
                            .commit();
                } else {
                    drawerLayout.closeDrawers();
                }
            }


        } else if (i == R.id.navigation_settings) {
            setToolbarTitle(getResources().getString(R.string.nav_settings));
            setTextSelection(mSettingsText);
            if (ServicesTabActivity.active) {
                hidePager(true);
            }
            fragment = new SettingsFragment();
            drawerLayout.closeDrawers();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame, fragment)
                    .commit();

        } else if (i == R.id.navigation_about) {
            setToolbarTitle(getResources().getString(R.string.nav_about));
            setTextSelection(mAboutText);
            if (ServicesTabActivity.active) {
                hidePager(true);
            }
            fragment = new AboutFragment();
            drawerLayout.closeDrawers();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame, fragment)
                    .commit();


        }

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        return false;
    }


    public void hidePager(Boolean show) {
    }


    private void setTextSelection(TextView text) {
        mWearableText.setSelected(false);
        mAboutText.setSelected(false);
        mSettingsText.setSelected(false);
        text.setSelected(true);
    }

    private void setToolbarTitle(String name) {
        getSupportActionBar().setTitle("");
        mToolbarTitle.setText(name);
    }

    public void onEventMainThread(BatteryModel event) {
        if (mShowLowBatteryAlert) {
            new CustomDialog().showAlert(this, getString(R.string.low_battery_msg), getString(R.string.battery));
        }
    }

    public void onEventMainThread(DropModel event) {
        if (mShowLowBatteryAlert) {
            new CustomDialog().showAlert(this, getString(R.string.drop_detection_msg), getString(R.string.drop));
        }
    }
}
