/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.wearables;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.view.KeyEvent;
import android.widget.Toast;

import com.atmel.wearable.R;
import com.atmel.wearable.commonutils.AppConstants;
import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.commonutils.Constants;
import com.atmel.wearable.commonutils.GattAttributes;
import com.atmel.wearable.communicator.BLEDataReceiver;
import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.fragments.DeviceScanFragment;
import com.atmel.wearable.interfaces.AdapterCallBack;


public class BaseActivity extends BLEBaseActivity implements AdapterCallBack {

    //Used in DeviceScanFragment
    public boolean mFilterSelected = true;
    public String[] UUIDs = new String[1];
    private String TAG = "BaseActivity";
    private boolean mIsFirstClick = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wearable_activity_base);
        UUIDs[0] = GattAttributes.ENVIRONMENT_SERVICE;
       // super.onCreateDrawer(false);
        super.onCreateToolbar();
        setFragment();
        Intent intent = new Intent(this, BLEConnection.class);
        intent.putExtra(Constants.RESULT_RECEIVER_KEY, new BLEDataReceiver(new Handler()));
        startService(intent);
   }

    private void setFragment() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        DeviceScanFragment scanFragment = new DeviceScanFragment();
        transaction.add(R.id.frame, scanFragment, AppUtils.getTagName(scanFragment)).commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setConnectionListener(BaseActivity.this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent gattServiceIntent = new Intent(this, BLEConnection.class);
        stopService(gattServiceIntent);
        clearPreferences();
    }

    public void clearPreferences() {
        //Log.e(TAG, "OnDestroy");
        AppUtils.setStringSharedPreference(this,
                AppConstants.PREF_DEV_ADDRESS, "");
        AppUtils.setIntSharedPreference(this,
                AppConstants.PREF_CONNECTION_STATE, 0);
    }


    /***
     * back action handling
     ***/
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //replaces the default 'Back' button action
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
          /*  if (mIsFirstClick) {
                mIsFirstClick = false;
                Toast.makeText(this, R.string.press_again, Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mIsFirstClick = true;
                    }
                }, 2000);
            } else {
                this.finish();
            }*/
        }
        return true;
    }


    @Override
    public void onMethodCallback() {
    }
}

