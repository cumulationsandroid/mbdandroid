/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.wearables;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.atmel.wearable.R;
import com.atmel.wearable.commonutils.AppConstants;
import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.commonutils.GattAttributes;
import com.atmel.wearable.communicator.BLEDataReceiver;
import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.connection.BLEKitKatScanner;
import com.atmel.wearable.connection.BLELollipopScanner;
import com.atmel.wearable.graphdatabase.EnvironmentGraphModel;
import com.atmel.wearable.graphdatabase.WearableDBHelper;
import com.atmel.wearable.models.BatteryModel;
import com.atmel.wearable.models.DescriptorWriteModel;
import com.atmel.wearable.models.DropModel;
import com.atmel.wearable.models.EnvironmentEventModel;
import com.atmel.wearable.widgets.CustomDialog;
import com.atmel.wearable.widgets.CustomOORDialog;
import com.atmel.wearable.widgets.LineGraph;
import com.atmel.wearable.widgets.RobotoMediumTextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

import static com.atmel.wearable.wearables.BLEBaseActivity.setConnectionListener;


public class EnvironmentGraphActivity extends AppCompatActivity implements BLEDataReceiver.BLEConnection
        , BLEDataReceiver.GATTProfiles {

    static BLELollipopScanner mBleLollipopScanner;
    static BLEKitKatScanner mBleKitkatScanner;
    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
    private Toolbar toolbar;
    private TextView mGraphValue, mLegendText;
    private ImageView mEnvFactorImage;
    String mGTitle, mGXaxis, mGYaxis;
    int mEnvOption = 0;
    private float mGraphValueInt = 0;
    private EventBus mGraphBus = EventBus.getDefault();
    private ViewGroup layout;
    private LineGraph mGraph;
    private RobotoMediumTextView mToolbarTitle;
    private ArrayList<EnvironmentGraphModel> graphValues;
    private static HashMap<String, List<BluetoothGattCharacteristic>> mBluetoothGattCharateristicsMap = new HashMap<>();
    private List<BluetoothGattService> mBluetoothGattServices = new ArrayList<>();
    private List<BluetoothGattCharacteristic> mBluetoothGattCharateristics = new ArrayList<>();
    //Database
    private WearableDBHelper mEnvDBHelper;

    private boolean dialogShown;
    CustomOORDialog outOfRangeDialog;

    private View mBottomDummyView;
    private LoadEnvironmentGraphData mLoadAsynch;
    private com.atmel.wearable.widgets.VerticalTextView mYaxisTitlt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_env_graph);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        mToolbarTitle = (RobotoMediumTextView) findViewById(R.id.toolbar_title);
        mGraphValue = (TextView) findViewById(R.id.env_value);
        mLegendText = (TextView) findViewById(R.id.text_legend);
        mEnvFactorImage = (ImageView) findViewById(R.id.env_parameter);
        mBottomDummyView = (View) findViewById(R.id.dummy_bottom);
        mYaxisTitlt = (com.atmel.wearable.widgets.VerticalTextView) findViewById(R.id.text_x_title);
        layout = (ViewGroup) findViewById(R.id.graph_env);
        setSupportActionBar(toolbar);
        Bundle extras = getIntent().getExtras();
        mEnvOption = extras.getInt(AppConstants.BUNDLE_ENV_OPTION);
        mGTitle = extras.getString(AppConstants.BUNDLE_GRAPH_TITLE);
        mGXaxis = extras.getString(AppConstants.BUNDLE_GRAPH_XAXIS);
        mGYaxis = extras.getString(AppConstants.BUNDLE_GRAPH_YAXIS);
        mGraph = new LineGraph();
        mGraph.mGraphTitle = mGTitle;
        mYaxisTitlt.setText(mGYaxis);
        mLegendText.setText(" " + mGTitle);
        if (!mGraphBus.isRegistered(this)) {
            mGraphBus.register(this);
        }
        setClassTitle(mGTitle);
        outOfRangeDialog = new CustomOORDialog(EnvironmentGraphActivity.this);
        outOfRangeDialog.setCanceledOnTouchOutside(false);
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }
        mEnvDBHelper = new WearableDBHelper(this);

        if (graphValues != null) {
            graphValues.clear();
        }
        mLoadAsynch = new LoadEnvironmentGraphData();
        mLoadAsynch.execute("");
    }

    private void setLayout() {
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        mBottomDummyView.setLayoutParams(new RelativeLayout.LayoutParams(width / 9, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public void onEventMainThread(EnvironmentEventModel event) {
        if (event != null) {
            if (mLoadAsynch.getStatus() == AsyncTask.Status.FINISHED) {
                if (mEnvOption == 1) {
                    ////Log.e("Temp graph", "" + event.getTemperature());
                    mGraphValueInt = checkTemperatureType(event.getTemperature().floatValue());
                    mGraphValue.setText("" + mGraphValueInt + " " + getTemperatureUnit());
                } else if (mEnvOption == 2) {
                    ////Log.e("Humidity graph", "" + event.getHumidity());
                    mGraphValueInt = event.getHumidity();
                    mGraphValue.setText("" + mGraphValueInt + " %RH");
                } else if (mEnvOption == 3) {
                    ////Log.e("Pressure graph", "" + event.getPressure());
                    mGraphValueInt = event.getPressure();
                    mGraphValue.setText("" + mGraphValueInt + " mbar");
                } else if (mEnvOption == 4) {
                    ////Log.e("UV graph", "" + event.getUV());
                    mGraphValueInt = Integer.valueOf(event.getUV().intValue());
                    mGraphValue.setText("" + mGraphValueInt + " lx");
                }
                Date date = new Date();
                mEnvDBHelper.insertEvironmentValues(BLEConnection.getmBluetoothDeviceAddress(), date.getTime(), event.getTemperature(), event.getPressure(),
                        event.getHumidity(), event.getUV());
                mEnvDBHelper.close();
                mGraph.upDateTempGraph(mGraphValueInt);
            }
        }
    }

    /***
     * set wearable_toolbar title
     */
    private void setClassTitle(String name) {
        setLayout();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolbarTitle(name);
        if (mEnvOption == 1) {
            mEnvFactorImage.setImageResource(R.drawable.white_temp);
        } else if (mEnvOption == 2) {
            mEnvFactorImage.setImageResource(R.drawable.humidity);
        } else if (mEnvOption == 3) {
            mEnvFactorImage.setImageResource(R.drawable.pressure);
        } else if (mEnvOption == 4) {
            mEnvFactorImage.setImageResource(R.drawable.uv);
        }
        mGraph.drawGraph(layout, mGraph, this, mEnvOption);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setConnectionListener(EnvironmentGraphActivity.this);

        /** reset x-min and x-max after one minute */
        upDateXLimits();
        bluetoothCheck();
    }

    @Override
    public void onLeCharacteristicChanged(boolean status) {

    }

    @Override
    public void onLeCharacteristicWrite(boolean status) {

    }

    @Override
    public void onLeCharacteristicRead(boolean status) {

    }

    @Override
    public void onLeCharacteristicRead(boolean status, String data, int code) {

    }

    @Override
    public void onLeReadRemoteRssi(int rssi) {

    }

    @Override
    public void onLeDescriptorWrite(boolean status) {

    }


    public class LoadEnvironmentGraphData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Date date = new Date();
            long t = date.getTime();
            Date upDatedTime = new Date(t - (60 * 60000));
            graphValues = mEnvDBHelper.getEnvironmentGraphValues(BLEConnection.getmBluetoothDeviceAddress(), upDatedTime.getTime());
        }

        @Override
        protected String doInBackground(String... values) {
            for (int i = 0; i < graphValues.size(); i++) {
                EnvironmentGraphModel model = graphValues.get(i);
                String log = "ID:" + model.id + " Time: " + model.time + " temp: " + model.temperature + " Pressure: " + model.pressure
                        + " Humidity: " + model.humidity + " UV: " + model.UV;
                //Log.d("Loaded values: ", log);
                if (mEnvOption == 1) {
                    mGraph.upDateGraphData(model.time, checkTemperatureType(model.temperature.floatValue()));
                } else if (mEnvOption == 2) {
                    mGraph.upDateGraphData(model.time, model.humidity.intValue());
                } else if (mEnvOption == 3) {
                    mGraph.upDateGraphData(model.time, model.pressure.intValue());
                } else if (mEnvOption == 4) {
                    mGraph.upDateGraphData(model.time, model.UV.intValue());
                }
            }
            return "executed";
        }

        @Override
        public void onPostExecute(String result) {
        }

        @Override
        public void onProgressUpdate(Void... values) {
        }
    }

    private void upDateXLimits() {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms

                Timer timer = new Timer();
                TimerTask hourlyTask = new TimerTask() {
                    @Override
                    public void run() {
                        mGraph.upDateXmaxAndXmin();
                    }
                };
                timer.schedule(hourlyTask, 0l, 1000);
            }
        }, 5 * 1000 * 60);
    }

    private void bluetoothCheck() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                goToHome();
            }
        }
    }

    private void goToHome() {
        BLEConnection.disconnect();
        Intent serviceIntent = new Intent(this, BLEConnection.class);
        stopService(serviceIntent);
        Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        mGraphBus.unregister(this);
        setResult(Activity.RESULT_OK);
        // do nothing. Protect from exiting the application when splash screen is shown
        //finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGraphBus.unregister(this);
        setConnectionListener(null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_disconnect, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menu_disconnect) {
            dialogShown = true;
            disconnectIntent();
            clearPreferences();
            setConnectionListener(null);
        } else if (id == android.R.id.home) {
            setResult(Activity.RESULT_OK);
            finish();

        }
        return super.onOptionsItemSelected(item);
    }


    public void clearPreferences() {
        AppUtils.setStringSharedPreference(this,
                AppConstants.PREF_DEV_ADDRESS, "");
        AppUtils.setIntSharedPreference(this,
                AppConstants.PREF_CONNECTION_STATE, 0);
    }

    private void disconnectIntent() {
        setConnectionListener(null);
        BLEConnection.disconnect();
        Intent serviceIntent = new Intent(this, BLEConnection.class);
        stopService(serviceIntent);
        Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLeDeviceDisconnected() {
        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            if (AppUtils.getOutOfRangeAlert(this)) {
                if (dialogShown) {
                    return;
                } else {
                    dialogShown = true;
                    if (!outOfRangeDialog.isShowing()) {
                        outOfRangeDialog.show();
                    }
                    BLEConnection.autoConnect(this);
                }
            } else {
            }
        }
    }


    @Override
    public void onLeDeviceConnected(String deviceName) {
        try {
            if (outOfRangeDialog.isShowing()) {
                outOfRangeDialog.dismiss();
            }
            dialogShown = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
        bluetoothCheck();
    }


    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onLeBluetoothStatusChanged() {
        disconnectIntent();
    }

    @Override
    public void onLeServicesDiscovered(boolean status, String serviceUuid) {

    }

    private float checkTemperatureType(float value) {
        if (AppUtils.getTemperatureUnit(this).equals(getString(R.string.degree_celsius_label))) {
            return value;
        } else {
            return AppUtils.convertToFahrenhite(value);
        }
    }

    private String getTemperatureUnit() {
        if (AppUtils.getTemperatureUnit(this).equals(getString(R.string.degree_celsius_label))) {
            return getString(R.string.degree_celsius);
        } else {
            return getString(R.string.fahrenheit);
        }
    }


    private void setToolbarTitle(String name) {
        getSupportActionBar().setTitle("");
        mToolbarTitle.setText(name);
    }


    public void onEventMainThread(BatteryModel event) {
        new CustomDialog().showAlert(this, getString(R.string.low_battery_msg), getString(R.string.battery));
    }

    public void onEventMainThread(DropModel event) {
        new CustomDialog().showAlert(this, getString(R.string.drop_detection_msg), getString(R.string.drop));
    }


    /**
     * Reconnection handle
     */
    private void init() {
        setConnectionListener(this);
        setGattListener(this);
        BLEConnection.discoverServices();
    }

    private static void setGattListener(BLEDataReceiver.GATTProfiles mGattListener) {
        BLEDataReceiver.setGattListener(mGattListener);
    }


    @Override
    public void onLeServiceDiscovered(boolean status) {
        {
            if (status) {
                mBluetoothGattServices.clear();
                List<BluetoothGattService> tempGattServiceList = BLEConnection.getSupportedGattServices();
                ////Log.e(TAG, "-----" + "Service discovery success with size--" + tempGattServiceList.size());
                // Loops through available GATT Services.
                for (BluetoothGattService gattService : tempGattServiceList) {
                    if (gattService.getUuid().toString()
                            .equalsIgnoreCase(GattAttributes.GENERIC_ACCESS_SERVICE)
                            || gattService.getUuid().toString()
                            .equalsIgnoreCase(GattAttributes.GENERIC_ATTRIBUTE_SERVICE)) {
                    } else {
                        mBluetoothGattServices.add(gattService);
                    }
                }
                getCharateristics(mBluetoothGattServices);
                // mEnvironmentEnable = true; // For enable environment chara in Plotting screen
                setEnvironmentCharacteristics(true);

            }
        }
    }


    private HashMap<String, List<BluetoothGattCharacteristic>> getCharateristics
            (List<BluetoothGattService> mBluetoothGattServices) {
        mBluetoothGattCharateristics.clear();
        mBluetoothGattCharateristicsMap.clear();
        List<BluetoothGattService> tempGattServiceList = mBluetoothGattServices;
        for (BluetoothGattService gattService : tempGattServiceList) {
            if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.ENVIRONMENT_SERVICE)) {
                mBluetoothGattCharateristicsMap.put(gattService.getUuid().toString(), gattService.getCharacteristics());
//                for (BluetoothGattCharacteristic gattCharacteristics : gattService.getCharacteristics()) {
//                    //Log.v(TAG, "HashMap Env Chara------" + gattCharacteristics.getUuid());
//                }
            } else if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                mBluetoothGattCharateristicsMap.put(gattService.getUuid().toString(), gattService.getCharacteristics());
//                for (BluetoothGattCharacteristic gattCharacteristics : gattService.getCharacteristics()) {
//                    //Log.v(TAG, "HashMap Device Orientation Chara------" + gattCharacteristics.getUuid());
//                }
            } else if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.BATTERY_SERVICE)) {
                mBluetoothGattCharateristicsMap.put(gattService.getUuid().toString(), gattService.getCharacteristics());
//                for (BluetoothGattCharacteristic gattCharacteristics : gattService.getCharacteristics()) {
//                    //Log.v(TAG, "HashMap Battery Chara------" + gattCharacteristics.getUuid());
//                }
            } else if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.TOUCH_GESTURE_SERVICE)) {
                mBluetoothGattCharateristicsMap.put(gattService.getUuid().toString(), gattService.getCharacteristics());
//                for (BluetoothGattCharacteristic gattCharacteristics : gattService.getCharacteristics()) {
//                    //Log.v(TAG, "HashMap Touch Chara------" + gattCharacteristics.getUuid());
//                }
            } else {
                //  //Log.v(TAG, "Service------" + gattService.getUuid());
            }
        }
        return mBluetoothGattCharateristicsMap;
    }


    /***
     * true
     * enable characteristics for environment
     **/

    private void setEnvironmentCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.ENVIRONMENT_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.ENVIRONMENT_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.ENVIRONMENT_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /***
     * enable characteristics for touch gesture
     **/
    private void setTouchCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.TOUCH_GESTURE_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.TOUCH_GESTURE_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.GESTURE_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                    //  //Log.d("DDD","---hre in gusture enable or disable status=="+status);
                }
            }
        }
    }


    /***
     * enable characteristics for low battery
     **/
    public void setBatteryCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.BATTERY_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.BATTERY_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.LOW_BATTERY_INDICATION_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /***
     * enable characteristics for step count
     **/
    public void setStepCountCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.STEP_INC_INDICATION_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /***
     * enable characteristics for drop detection
     **/
    public void setDropDetectCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.DROP_DETECTION_INDICATION_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /**
     * event bus handling characteristics enabling and disabling call back
     */
    public void onEventMainThread(DescriptorWriteModel event) {

        switch (event.mCharacteristic.getUuid().toString()) {

            case GattAttributes.LOW_BATTERY_INDICATION_CHARACTERISTICS:
                if (AppUtils.getDropAlert(this)) {
                    setDropDetectCharacteristics(true);
                } else {
                    setTouchCharacteristics(true);
                }
                break;

            case GattAttributes.DROP_DETECTION_INDICATION_CHARACTERISTICS:
                setTouchCharacteristics(true);
                break;

            case GattAttributes.GESTURE_CHARACTERISTICS:

                if (AppUtils.getBooleanSharedPreference(this, AppUtils.STEP_COUNT_NOTIFY)) {
                    setStepCountCharacteristics(true);
                }
                break;

            case GattAttributes.ENVIRONMENT_CHARACTERISTICS:
                if (AppUtils.getBatteryAlert(this)) {
                    setBatteryCharacteristics(true);
                } else if (AppUtils.getDropAlert(this)) {
                    setDropDetectCharacteristics(true);
                } else {
                    setTouchCharacteristics(true);
                }
                break;

            case GattAttributes.STEP_INC_INDICATION_CHARACTERISTICS:
                break;


            default:
                break;
        }
    }

}
