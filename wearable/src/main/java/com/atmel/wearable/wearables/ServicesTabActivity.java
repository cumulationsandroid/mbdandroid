/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.wearables;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.atmel.wearable.R;
import com.atmel.wearable.commonutils.AppConstants;
import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.commonutils.Constants;
import com.atmel.wearable.commonutils.GattAttributes;
import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.connection.BLEKitKatScanner;
import com.atmel.wearable.connection.BLELollipopScanner;
import com.atmel.wearable.fragments.AccelerometerFragment;
import com.atmel.wearable.fragments.EnvironmentFragment;
import com.atmel.wearable.fragments.GyroFragment;
import com.atmel.wearable.fragments.Plot3DFragment;
import com.atmel.wearable.fragments.StepCountFragment;
import com.atmel.wearable.fragments.WearableStatusFragment;
import com.atmel.wearable.gattservices.BatteryService;
import com.atmel.wearable.gattservices.DeviceOrientationService;
import com.atmel.wearable.gattservices.DropService;
import com.atmel.wearable.gattservices.EnvironmentService;
import com.atmel.wearable.gattservices.TouchGestureService;
import com.atmel.wearable.graphdatabase.WearableDBHelper;
import com.atmel.wearable.interfaces.AccelerometerCallBack;
import com.atmel.wearable.models.BatteryModel;
import com.atmel.wearable.models.CharacteristicChangeEventModel;
import com.atmel.wearable.models.DescriptorWriteModel;
import com.atmel.wearable.models.DeviceOrientationModel;
import com.atmel.wearable.models.DropModel;
import com.atmel.wearable.models.EnvironmentEventModel;
import com.atmel.wearable.models.GyroModel;
import com.atmel.wearable.models.Plot3dModel;
import com.atmel.wearable.models.RSSIEventModel;
import com.atmel.wearable.models.StepCountEventModel;
import com.atmel.wearable.widgets.ClearAlarm;
import com.atmel.wearable.widgets.CustomOORDialog;
import com.atmel.wearable.widgets.CustomViewPager;
import com.atmel.wearable.widgets.DepthPageTransformer;
import com.atmel.wearable.widgets.RobotoMediumTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;


import de.greenrobot.event.EventBus;

public class ServicesTabActivity extends BLEBaseActivity implements ViewPager.OnPageChangeListener {

    private CustomViewPager viewPager;
    private static BLELollipopScanner mBleLollipopScanner;
    private static BLEKitKatScanner mBleKitkatScanner;
    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
    private List<BluetoothGattService> mBluetoothGattServices = new ArrayList<>();
    private List<BluetoothGattCharacteristic> mBluetoothGattCharateristics = new ArrayList<>();
    private static HashMap<String, List<BluetoothGattCharacteristic>> mBluetoothGattCharateristicsMap = new HashMap<>();
    private AppConstants.ADV_CATEGORY mGattProfile = AppConstants.ADV_CATEGORY.DEFAULT;
    private EventBus mMainBus = EventBus.getDefault();
    private Timer timer = null;
    private TimerTask timerTask = null;
    private String TAG = "WEARABLE";
    private RobotoMediumTextView mToolbarTitle;
    public static boolean active = false;
    private BluetoothGattCharacteristic mCurrentEnablingCharacteristic;
    private int mCurrentEnabledStatus;
    private boolean mEnvironmentEnable = false, mStepCountEnable = false, mAcceleroStart = false, mAcceleroEnable = false, mGyroEnable = false, mGyroStart = false,
            mRotateEnable = false;
    private boolean dialogShown;
    private ViewPagerAdapter adapter;
    private boolean mIsRightSwipe = true;
    private int mCurrentStatePosition;
    CustomOORDialog outOfRangeDialog;

    //Step Count
    private WearableDBHelper mStepDBHelper;
    public static int mCounter = 0;
    private ClearAlarm mClearAlarm;
    private boolean mIsReconnection, isGyroStartButtonClicked = false, isInitialCharasEnabled = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_tab);
        super.onCreateDrawer(true);
        mClearAlarm = new ClearAlarm();
        outOfRangeDialog = new CustomOORDialog(ServicesTabActivity.this);
        outOfRangeDialog.setCanceledOnTouchOutside(false);
        mToolbarTitle = (RobotoMediumTextView) findViewById(R.id.toolbar_title);
        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.setOffscreenPageLimit(1);
        if (getIntent() != null) {
            Bundle bundle = getIntent().getBundleExtra(AppConstants.GATTSERVER_PROFILES);
            mGattProfile = (AppConstants.ADV_CATEGORY) bundle.getSerializable(AppConstants.GATTSERVER_PROFILES);

            switch (mGattProfile != null ? mGattProfile : AppConstants.ADV_CATEGORY.DEFAULT) {
                case DEFAULT:
                    BLEConnection.discoverServices();
                    break;
                default:
                    break;
            }
        }
        mIsReconnection = false;
        // Register as a subscriber
        if (!mMainBus.isRegistered(this)) {
            mMainBus.register(this);
        }
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            mBleLollipopScanner = BLELollipopScanner.getInstance(this);
        } else {
            mBleKitkatScanner = BLEKitKatScanner.getInstance(this);
        }

        mStepDBHelper = new WearableDBHelper(ServicesTabActivity.this);
        try {
            mCounter = AppUtils.getIntSharedPreference(ServicesTabActivity.this, AppUtils.STEP_COUNTER);
        } catch (IllegalStateException e) {
            mCounter = 0;
        } catch (NullPointerException e) {
            mCounter = 0;
        }
        setClearAlarm();
    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_CANCELED) {
                this.finish();
            }
        }
    }*/

    private void setClearAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        TimeZone defaultz = TimeZone.getDefault();
        Calendar calendar = new GregorianCalendar(defaultz);
        Calendar now = new GregorianCalendar(defaultz);
        now.setTime(new Date());
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        if (calendar.before(now)) {
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        }
        Intent intent = new Intent(this, ClearAlarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(this, Constants.ID_CLEAR_ALARM, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
        }
    }

    public void onEventMainThread(CharacteristicChangeEventModel event) {
        if (event != null) {
            String characteristicUUID = event.getBleGattCharacteristic().getUuid().toString();
            switch (characteristicUUID) {
                case GattAttributes.ENVIRONMENT_CHARACTERISTICS:
                    EnvironmentService envService = new EnvironmentService(event.getBleGattCharacteristic());
                    EnvironmentEventModel envEvent = new EnvironmentEventModel(envService.getTemperature(), envService.getPressure(),
                            envService.getUV(), envService.getHumidity());
                    mMainBus.post(envEvent);
                    break;
                case GattAttributes.GYRO_POSITIONS_CHARACTERISTICS:
                    DeviceOrientationService orientationServiceGyro = new DeviceOrientationService(event.getBleGattCharacteristic());
                    GyroModel gModel = new GyroModel().getGyroscopeModel(orientationServiceGyro.getXPos(),
                            orientationServiceGyro.getYPos(), orientationServiceGyro.getZPos());
                    mMainBus.post(gModel);
                    break;
                case GattAttributes.ACCELERO_POSITIONS_CHARACTERISTICS:
                    DeviceOrientationService orientationService = new DeviceOrientationService(event.getBleGattCharacteristic());
                    DeviceOrientationModel orientationModel = new DeviceOrientationModel().getAccelerationModel(orientationService.getXPos(),
                            orientationService.getYPos(), orientationService.getZPos());
                    mMainBus.post(orientationModel);
                    break;
                case GattAttributes.DROP_DETECTION_INDICATION_CHARACTERISTICS:
                    if (AppUtils.getDropAlert(this)) {
                        DropService dropDeviceService = new DropService(event.getBleGattCharacteristic());
                        DropModel dropModel = new DropModel(dropDeviceService.parseDropValue());
                        mMainBus.post(dropModel);
                    }
                    break;
                case GattAttributes.STEP_INC_INDICATION_CHARACTERISTICS:
                    DeviceOrientationService stepCountService = new DeviceOrientationService(event.getBleGattCharacteristic());
                    mCounter++;
                    Date date = new Date();
                    mStepDBHelper.insertStepCountValues(BLEConnection.getmBluetoothDeviceAddress(), getDateCurrentHour(date.getTime()), mCounter);
                    AppUtils.setIntSharedPreference(ServicesTabActivity.this, AppUtils.STEP_COUNTER, mCounter);
                    mStepDBHelper.close();
                    StepCountEventModel stepEvent = new StepCountEventModel(stepCountService.getStepCount());
                    stepEvent.setStepCountGattCharacteristic(event.getBleGattCharacteristic());
                    mMainBus.post(stepEvent);
                    break;
                case GattAttributes.LOW_BATTERY_INDICATION_CHARACTERISTICS:
                    if (AppUtils.getBatteryAlert(this)) {
                        BatteryService batteryService = new BatteryService(event.getBleGattCharacteristic());
                        BatteryModel batteryModel = new BatteryModel(batteryService.parseBatteryValue());
                        mMainBus.post(batteryModel);
                    }
                    break;
                case GattAttributes.GESTURE_CHARACTERISTICS:
                    TouchGestureService touchService = new TouchGestureService(event.getBleGattCharacteristic());
                    ////Log.d("Gesture value>>", "" + touchService.getGesture());
                    if (touchService.getGesture() == 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else if (touchService.getGesture() == 2) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                    }
                    break;
                case GattAttributes.ROTATION_VECTOR_CHARACTERISTICS:
                    DeviceOrientationService rotationService = new DeviceOrientationService(event.getBleGattCharacteristic());
                    Plot3dModel model = new Plot3dModel(rotationService.getXPos(),
                            rotationService.getYPos(), rotationService.getZPos(), rotationService.getWPos());
                    mMainBus.post(model);
                    break;
                default:
                    break;
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new WearableStatusFragment(), getResources().getString(R.string.tool_title_wearable_status));
        adapter.addFrag(new EnvironmentFragment(), getResources().getString(R.string.tool_title_enironment));
        adapter.addFrag(new StepCountFragment(), getResources().getString(R.string.tool_title_step_count));
        adapter.addFrag(new AccelerometerFragment(), getResources().getString(R.string.tool_title_accelerometer));
        adapter.addFrag(new GyroFragment(), getResources().getString(R.string.tool_title_gyroscope));
        adapter.addFrag(new Plot3DFragment(), getResources().getString(R.string.tool_title_3Dplot));
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

        // used to identify the swipe direction
        if (mCurrentStatePosition > position) {
            mIsRightSwipe = false;
        } else {
            mIsRightSwipe = true;
        }
        mCurrentStatePosition = position;
        DisableFlagsOnPageChange();
        switch (position) {
            case 0:

                setToolbarTitle(getString(R.string.tool_title_wearable_status));
                if (mIsRightSwipe) {

                } else {
                    if (!mEnvironmentEnable) {
                        setEnvironmentCharacteristics(false);
                    }
                }
                break;
            case 1:
                setToolbarTitle(getString(R.string.tool_title_enironment));
                break;
            case 2:
                setToolbarTitle(getString(R.string.tool_title_step_count));
                break;
            case 3:
                setToolbarTitle(getString(R.string.tool_title_accelerometer));
                break;
            case 4:
                setToolbarTitle(getString(R.string.tool_title_gyroscope));
                break;
            case 5:
                setToolbarTitle(getString(R.string.tool_title_3Dplot));
                break;
            default:
                setToolbarTitle(getString(R.string.tool_title_wearable));
                break;
        }
        enableCurrentPagerCharacteristic(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /****
     * characteristic enabling and disabling on page viewer scroll
     */
    private void enableCurrentPagerCharacteristic(int pos) {
        switch (pos) {
            case 1:
                // Environment chara enabled
                mEnvironmentEnable = true;
                if (isInitialCharasEnabled) {
                    setEnvironmentCharacteristics(true);
                } else {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setEnvironmentCharacteristics(true);
                        }
                    }, 2000);
                }
                break;
            case 2:
                if (mIsRightSwipe) {
                    if (!mEnvironmentEnable) {
                        setEnvironmentCharacteristics(false);
                    }
                } else {
                    if (!mAcceleroEnable) {
                        AccelerometerCallBack fragment = (AccelerometerCallBack) adapter.instantiateItem(viewPager, pos + 1);
                        if (fragment != null) {
                            fragment.accelerometerFragmentBecameVisible();
                        }
                    }
                }
                break;
            case 3:
                // Accelerometer chara enabled
                mAcceleroEnable = true;
                if (mIsRightSwipe) {
                    // step count disable
                } else {
                    AccelerometerCallBack fragment = (AccelerometerCallBack) adapter.instantiateItem(viewPager, pos + 1);
                    if (fragment != null) {
                        fragment.gyroscopeFragmentBecameVisible();
                    }
                }
                break;
            case 4:
                // Gyroscope chara enabled
                mGyroEnable = true;
                if (mIsRightSwipe) {
                    AccelerometerCallBack fragment = (AccelerometerCallBack) adapter.instantiateItem(viewPager, pos - 1);
                    if (fragment != null) {
                        fragment.accelerometerFragmentBecameVisible();
                    }
                } else {
                    setPlot3dCharacteristics(false);
                }

                break;
            case 5:
                // Rotation chara enabled
                mRotateEnable = true;
                if (mIsRightSwipe) {
                    AccelerometerCallBack fragment = (AccelerometerCallBack) adapter.instantiateItem(viewPager, pos - 1);
                    if (fragment != null) {
                        fragment.gyroscopeFragmentBecameVisible();
                    }
                }

                break;
            default:
                break;
        }
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onLeDescriptorWrite(boolean status) {

    }

    @Override
    public void onLeServiceDiscovered(boolean status) {
        if (status) {
            mBluetoothGattServices.clear();
            List<BluetoothGattService> tempGattServiceList = BLEConnection.getSupportedGattServices();
            ////Log.e(TAG, "-----" + "Service discovery success with size--" + tempGattServiceList.size());
            // Loops through available GATT Services.
            for (BluetoothGattService gattService : tempGattServiceList) {
                if (gattService.getUuid().toString()
                        .equalsIgnoreCase(GattAttributes.GENERIC_ACCESS_SERVICE)
                        || gattService.getUuid().toString()
                        .equalsIgnoreCase(GattAttributes.GENERIC_ATTRIBUTE_SERVICE)) {
                } else {
                    mBluetoothGattServices.add(gattService);
                }
            }
            getCharateristics(mBluetoothGattServices);
            if (mIsReconnection) {
                if (AppUtils.getBatteryAlert(this)) {
                    setBatteryCharacteristics(true);
                } else if (AppUtils.getDropAlert(this)) {
                    setDropDetectCharacteristics(true);
                } else {
                    setTouchCharacteristics(true);
                }
            } else {
                mEnvironmentEnable = true; // For enable environment chara in Plotting screen
                setEnvironmentCharacteristics(true);
            }
        }
    }

    private HashMap<String, List<BluetoothGattCharacteristic>> getCharateristics
            (List<BluetoothGattService> mBluetoothGattServices) {
        mBluetoothGattCharateristics.clear();
        mBluetoothGattCharateristicsMap.clear();
        List<BluetoothGattService> tempGattServiceList = mBluetoothGattServices;
        for (BluetoothGattService gattService : tempGattServiceList) {
            if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.ENVIRONMENT_SERVICE)) {
                mBluetoothGattCharateristicsMap.put(gattService.getUuid().toString(), gattService.getCharacteristics());
//                for (BluetoothGattCharacteristic gattCharacteristics : gattService.getCharacteristics()) {
//                    //Log.v(TAG, "HashMap Env Chara------" + gattCharacteristics.getUuid());
//                }
            } else if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                mBluetoothGattCharateristicsMap.put(gattService.getUuid().toString(), gattService.getCharacteristics());
//                for (BluetoothGattCharacteristic gattCharacteristics : gattService.getCharacteristics()) {
//                    //Log.v(TAG, "HashMap Device Orientation Chara------" + gattCharacteristics.getUuid());
//                }
            } else if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.BATTERY_SERVICE)) {
                mBluetoothGattCharateristicsMap.put(gattService.getUuid().toString(), gattService.getCharacteristics());
//                for (BluetoothGattCharacteristic gattCharacteristics : gattService.getCharacteristics()) {
//                    //Log.v(TAG, "HashMap Battery Chara------" + gattCharacteristics.getUuid());
//                }
            } else if (gattService.getUuid().toString()
                    .equalsIgnoreCase(GattAttributes.TOUCH_GESTURE_SERVICE)) {
                mBluetoothGattCharateristicsMap.put(gattService.getUuid().toString(), gattService.getCharacteristics());
//                for (BluetoothGattCharacteristic gattCharacteristics : gattService.getCharacteristics()) {
//                    //Log.v(TAG, "HashMap Touch Chara------" + gattCharacteristics.getUuid());
//                }
            } else {
                //  //Log.v(TAG, "Service------" + gattService.getUuid());
            }
        }
        return mBluetoothGattCharateristicsMap;
    }

    /***
     * true
     * enable characteristics for environment
     **/

    private void setEnvironmentCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.ENVIRONMENT_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.ENVIRONMENT_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.ENVIRONMENT_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /***
     * enable characteristics for touch gesture
     **/
    private void setTouchCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.TOUCH_GESTURE_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.TOUCH_GESTURE_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.GESTURE_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                    //  //Log.d("DDD","---hre in gusture enable or disable status=="+status);
                }
            }
        }
    }


    /***
     * enable characteristics for low battery
     **/
    public void setBatteryCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.BATTERY_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.BATTERY_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.LOW_BATTERY_INDICATION_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }


    /***
     * enable characteristics for accelerometer
     **/
    private void setAccelerometerCharacteristics(HashMap<String, List<BluetoothGattCharacteristic>> charateristicsMap, boolean status) {
        if (charateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : charateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.ACCELERO_POSITIONS_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                    ////Log.d("sss", "----charavalue reenabled");
                }
            }
        }
    }


    /***
     * enable characteristics for gyroscope
     **/
    private void setGyroscopeCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.GYRO_POSITIONS_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /***
     * enable characteristics for step count
     **/
    public void setStepCountCharacteristics(boolean status) {
        mStepCountEnable = status;
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.STEP_INC_INDICATION_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /***
     * enable characteristics for drop detection
     **/
    public void setDropDetectCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.DROP_DETECTION_INDICATION_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /**
     * enable characteristics for 3d plot
     */
    private void setPlot3dCharacteristics(boolean status) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.ROTATION_VECTOR_CHARACTERISTICS)) {
                    BLEConnection.setCharacteristicNotification(characteristics, status);
                }
            }
        }
    }

    /**
     * write characteristics for env odr
     */
    public void writeEnvironmentODR(byte[] value) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.ENVIRONMENT_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.ENVIRONMENT_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.ENVIRONMENT_ODR_CHARCTERISTICS)) {
                    BLEConnection.setCharacteristicWriteWithoutResponse(characteristics, value);
                    //  //Log.v("Environment Write", value + " " + characteristics.getUuid().toString());
                }
            }
        }
    }

    /**
     * read characteristics for env odr
     */
    public void readEnvironmentODR() {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.ENVIRONMENT_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.ENVIRONMENT_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.ENVIRONMENT_ODR_CHARCTERISTICS)) {
                    BLEConnection.readCharacteristic(characteristics);
                    //   //Log.v("Environment Read", " " + characteristics.getUuid().toString());
                }
            }
        }
    }

    /**
     * write characteristics for accelero odr
     */
    public void writeMotionODR(byte[] value) {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.MOTION_ODR_CHARCTERISTICS)) {
                    //  //Log.v("Motion Write", value + " " + characteristics.getUuid().toString());
                    BLEConnection.setCharacteristicWriteWithoutResponse(characteristics, value);
                }
            }
        }
    }


    /**
     * read characteristics for accelero odr
     */
    public void readMotionODR() {
        if (mBluetoothGattCharateristicsMap.containsKey(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
            for (BluetoothGattCharacteristic characteristics : mBluetoothGattCharateristicsMap.get(GattAttributes.DEVICE_ORIENTATION_SERVICE)) {
                if (characteristics.getUuid().toString().equalsIgnoreCase(GattAttributes.MOTION_ODR_CHARCTERISTICS)) {
                    // //Log.v("Motion Read", " " + characteristics.getUuid().toString());
                    BLEConnection.readCharacteristic(characteristics);
                }
            }
        }
    }


    private void init() {
        timer = new Timer();
        timerTask = new CustomTask();
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
        setConnectionListener(this);
        setGattListener(this);
    }

    private class CustomTask extends TimerTask {
        @Override
        public void run() {
            BLEConnection.readRemoteRssi();
        }
    }

    @Override
    public void onLeReadRemoteRssi(int rssi) {
        rssi = Math.abs(rssi);
        RSSIEventModel rssiEvent = new RSSIEventModel(rssi);
        mMainBus.post(rssiEvent);
    }

    @Override
    public void onLeDeviceDisconnected() {

        if (isBluetoothDisable()) {
            Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
            goToHome();
        } else {
            if (viewPager.getCurrentItem() == 3) {
                AccelerometerCallBack fragment = (AccelerometerCallBack) adapter.instantiateItem(viewPager, 3);
                if (fragment != null) {
                    fragment.accelerometerAutoDisconnect();
                }
            } else if (viewPager.getCurrentItem() == 4) {
                AccelerometerCallBack fragment = (AccelerometerCallBack) adapter.instantiateItem(viewPager, 4);
                if (fragment != null) {
                    fragment.gyroscopeAutoDisconnect();
                }
            }
            if (AppUtils.getOutOfRangeAlert(this)) {
                if (dialogShown) {
                    return;
                } else {
                    dialogShown = true;
                    if (!outOfRangeDialog.isShowing()) {
                        outOfRangeDialog.show();
                    }
                    BLEConnection.autoConnect(this);
                }
            } else {
                goToHome();
            }
        }
        isInitialCharasEnabled = false;
    }

    @Override
    public void onLeDeviceConnected(String deviceName) {
        try {
            if (outOfRangeDialog.isShowing()) {
                outOfRangeDialog.dismiss();
            }
            dialogShown = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        mIsReconnection = true;
        init();
        bluetoothCheck();
        BLEConnection.discoverServices();
    }


    @Override
    protected void onResume() {
        super.onResume();
        init();
        active = true;
        setGattListener(ServicesTabActivity.this);
        setConnectionListener(ServicesTabActivity.this);
        bluetoothCheck();
    }


    private void bluetoothCheck() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                goToHome();
            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {

                goToHome();
            }
        }
    }

    public boolean isBluetoothDisable() {
        if (mBleKitkatScanner != null) {
            if (!mBleKitkatScanner.getBluetoothStatus()) {
                return true;

            }
        }
        if (mBleLollipopScanner != null) {
            if (!mBleLollipopScanner.getBluetoothStatus()) {
                return true;
            }
        }
        return false;
    }

    private void goToHome() {
        BLEConnection.disconnect();
        Toast.makeText(this, R.string.bluetooth_off, Toast.LENGTH_SHORT).show();
        Intent serviceIntent = new Intent(this, BLEConnection.class);
        stopService(serviceIntent);
        Intent intent = new Intent(this, BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainBus.unregister(this);
        active = false;
        timerTask.cancel();
        setBatteryCharacteristics(false);
        setTouchCharacteristics(false);
        setAccelerometerCharacteristics(mBluetoothGattCharateristicsMap, false);
        setEnvironmentCharacteristics(false);
        setPlot3dCharacteristics(false);
        //Log.e(TAG, "setConnectionListener servicetab");
        //setConnectionListener(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public void onLeCharacteristicChanged(boolean status) {
        if (status) {
            // //Log.e(TAG, "Service Tab Chara Changed");
        }
    }

    @Override
    public void hidePager(Boolean hidePager) {
        if (hidePager) {
            viewPager.setVisibility(View.GONE);
        } else {
            viewPager.setVisibility(View.VISIBLE);
            if (viewPager.getCurrentItem() == 0) {
                setToolbarTitle(getString(R.string.wearable_status));
            } else if (viewPager.getCurrentItem() == 1) {
                setToolbarTitle(getString(R.string.environment));
            } else if (viewPager.getCurrentItem() == 2) {
                setToolbarTitle(getString(R.string.tool_title_step_count));
            } else if (viewPager.getCurrentItem() == 3) {
                setToolbarTitle(getString(R.string.tool_title_accelerometer));
            } else if (viewPager.getCurrentItem() == 4) {
                setToolbarTitle(getString(R.string.tool_title_gyroscope));
            } else {
                setToolbarTitle(getString(R.string.tool_title_3Dplot));
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_disconnect, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menu_disconnect) {
            disconnectIntent();
            clearPreferences();
            setConnectionListener(null);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLeBluetoothStatusChanged() {
        disconnectIntent();
    }

    private void disconnectIntent() {
        BLEConnection.disconnect();
        Intent serviceIntent = new Intent(this, BLEConnection.class);
        stopService(serviceIntent);
        Toast.makeText(this, R.string.disconnected_alert, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
        startActivity(intent);
        finish();
    }

    private void setToolbarTitle(String name) {
        getSupportActionBar().setTitle("");
        mToolbarTitle.setText(name);
    }

    public void clearPreferences() {
        AppUtils.setStringSharedPreference(this,
                AppConstants.PREF_DEV_ADDRESS, "");
        AppUtils.setIntSharedPreference(this,
                AppConstants.PREF_CONNECTION_STATE, 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    /**
     * used for handling disable and enable for corresponding fragments
     */
    private void DisableFlagsOnPageChange() {
        mEnvironmentEnable = false;
        mAcceleroEnable = false;
        mGyroEnable = false;
        mRotateEnable = false;
    }


    /**
     * event bus handling characteristics enabling and disabling call back
     */
    public void onEventMainThread(DescriptorWriteModel event) {
        mCurrentEnablingCharacteristic = event.mCharacteristic;
        mCurrentEnabledStatus = event.mEnableStatus;

        switch (event.mCharacteristic.getUuid().toString()) {

            case GattAttributes.LOW_BATTERY_INDICATION_CHARACTERISTICS:
                if (AppUtils.getDropAlert(this)) {
                    setDropDetectCharacteristics(true);
                } else {
                    setTouchCharacteristics(true);
                }
                break;

            case GattAttributes.DROP_DETECTION_INDICATION_CHARACTERISTICS:
                setTouchCharacteristics(true);
                break;

            case GattAttributes.GESTURE_CHARACTERISTICS:
                //  //Log.d("DDD","reached on gusture success");
                // if reconnection we need to re enable environment or rotation chara based on
                // the page which we stand
                isInitialCharasEnabled = true;
                if (AppUtils.getBooleanSharedPreference(this, AppUtils.STEP_COUNT_NOTIFY)) {
                    setStepCountCharacteristics(true);
                } else {
                    if (mIsReconnection) {
                        reEnablePageCharaOnReconnection();
                        mIsReconnection = false;
                    }
                }

                break;

            case GattAttributes.ENVIRONMENT_CHARACTERISTICS:
                if (mEnvironmentEnable == isDescriptorEnabled(event.mDescriptor)) {
                    if (!isInitialCharasEnabled) {
                        if (AppUtils.getBatteryAlert(this)) {
                            setBatteryCharacteristics(true);
                        } else if (AppUtils.getDropAlert(this)) {
                            setDropDetectCharacteristics(true);
                        } else {
                            setTouchCharacteristics(true);
                        }
                    }
                    //do nothing
                } else {
                    recallForEnableOrDisable(mCurrentEnablingCharacteristic, mEnvironmentEnable);
                }

                break;

            case GattAttributes.STEP_INC_INDICATION_CHARACTERISTICS:
                    if (mStepCountEnable == isDescriptorEnabled(event.mDescriptor)) {
                        if (mIsReconnection) {
                            reEnablePageCharaOnReconnection();
                            mIsReconnection = false;
                        }
                    } else {
                        recallForEnableOrDisable(mCurrentEnablingCharacteristic, mStepCountEnable);
                    }

                break;

            case GattAttributes.ACCELERO_POSITIONS_CHARACTERISTICS:
                if (mCurrentStatePosition == 3) {
                    if (mAcceleroEnable == isDescriptorEnabled(event.mDescriptor) ||
                            mAcceleroStart == isDescriptorEnabled(event.mDescriptor)) {//
                        // do nothing
                    } else {
                        recallForEnableOrDisable(mCurrentEnablingCharacteristic, mAcceleroEnable);
                    }
                }
                break;

            case GattAttributes.GYRO_POSITIONS_CHARACTERISTICS:

                if (mCurrentStatePosition == 5) {
                    setPlot3dCharacteristics(true);
                }
                if (mCurrentStatePosition == 4) {
                    if (mGyroEnable == isDescriptorEnabled(event.mDescriptor) ||
                            mGyroStart == isDescriptorEnabled(event.mDescriptor)) {

// do nothing
                    } else {
                        recallForEnableOrDisable(mCurrentEnablingCharacteristic, mGyroEnable);
                    }
                }
                break;
            case GattAttributes.ROTATION_VECTOR_CHARACTERISTICS:
                if (mCurrentStatePosition == 5) {
                    if (mRotateEnable == isDescriptorEnabled(event.mDescriptor)) {
                        // Do nothing
                    } else {
                        recallForEnableOrDisable(mCurrentEnablingCharacteristic, mRotateEnable);
                    }
                }
                break;

            default:
                break;
        }
    }


    /**
     * check descriptor type
     */

    private boolean isDescriptorEnabled(BluetoothGattDescriptor descriptor) {
        byte[] array = descriptor.getValue();
        if (array[0] == 0) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * recall with status
     */

    private void recallForEnableOrDisable(BluetoothGattCharacteristic chara, boolean status) {
        BLEConnection.setCharacteristicNotification(chara, status);
    }


    /***
     * enable or disable page swipe
     **/
    public void setPageSwipe(boolean status) {
        //viewPager.setPagingEnabled(status);
        mAcceleroStart = status;
        setAccelerometerCharacteristics(mBluetoothGattCharateristicsMap, status);

    }

    /***
     * enable/disable page swipe in gyro
     ***/
    public void setPageSwipeGyro(boolean stat) {
        //viewPager.setPagingEnabled(stat);
        isGyroStartButtonClicked = stat;
        mGyroStart = stat;
        setGyroscopeCharacteristics(stat);
    }


    public Integer getDateCurrentHour(long timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH");
            String formattedDate = sdf.format(timestamp);
            return Integer.parseInt(formattedDate);
        } catch (Exception ignored) {
        }
        return 0;
    }

    private void reEnablePageCharaOnReconnection() {
        if (viewPager.getCurrentItem() == 1) {
            setEnvironmentCharacteristics(true);
        } else if (viewPager.getCurrentItem() == 5) {
            setPlot3dCharacteristics(true);
        } else if (viewPager.getCurrentItem() == 3) {
            AccelerometerCallBack fragment = (AccelerometerCallBack) adapter.instantiateItem(viewPager, 3);
            if (fragment != null) {
                fragment.gyroscopeFragmentBecameVisible();
            }
        } else if (viewPager.getCurrentItem() == 4) {
            AccelerometerCallBack fragment = (AccelerometerCallBack) adapter.instantiateItem(viewPager, 4);
            if (fragment != null) {
                fragment.accelerometerFragmentBecameVisible();
            }
        } else {

        }
    }

}
