/*   \file filename.extension
        *
        *   \brief Short description about the file
        *
        *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
        *   Released under NDA
        *   Licensed under Atmel's Limited License Agreement.
        *
        *
        *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
        *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
        *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
        *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
        *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
        *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
        *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
        *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
        *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
        *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
        *   POSSIBILITY OF SUCH DAMAGE.
        *
        *   Atmel Corporation: http://www.atmel.com
        *
        */

package com.atmel.wearable.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.atmel.wearable.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

public class BarStepGraph {

    private String[] mHours = new String[]{
            "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21",
            "22", "23", "24"
    };

    int[] x = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23};
    int[] steps = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    private static BarStepGraph mInstance = null;
    private GraphicalView mChart;

    XYSeries stepSeries = new XYSeries("StepCount");
    // Creating XYSeriesRenderer to customize incomeSeries
    XYSeriesRenderer stepRenderer = new XYSeriesRenderer();

    /**
     * singleton implementation of LineGraphView class
     */
    public static synchronized BarStepGraph getBarGraphView() {
        if (mInstance == null) {
            mInstance = new BarStepGraph();
        }
        return mInstance;
    }


    /**
     * This constructor will set some properties of single chart and some properties of whole graph
     */
    public void drawGraph(ViewGroup v, BarStepGraph graph, Context c) {

        // Creating an  XYSeries for Step
        for (int i = 0; i < x.length; i++) {
            stepSeries.add(i, steps[i]);
        }

        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        // Adding Income Series to the dataset
        dataset.addSeries(stepSeries);
        // Adding Expense Series to dataset

        stepRenderer.setColor(ContextCompat.getColor(c,R.color.colorPrimary));
        stepRenderer.setFillPoints(true);
        stepRenderer.setLineWidth(10);
        stepRenderer.setDisplayChartValues(false);

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();

        //multiRenderer.setChartTitle("Income vs Expense Chart");
        //multiRenderer.setXTitle("Year 2012");
        //multiRenderer.setYTitle("Amount in Dollars");
        multiRenderer.setZoomButtonsVisible(false);
//        for (int i = 0; i < x.length; i++) {
//            multiRenderer.addXTextLabel(i, mHours[i]);
//        }
        multiRenderer.clearXTextLabels();
        for (int i = 0; i < x.length; i++) {
            if (i % 4 == 0) {
                multiRenderer.addXTextLabel(i, mHours[i]);
            }
        }

        multiRenderer.setBackgroundColor(Color.TRANSPARENT);
        multiRenderer.setMarginsColor(Color.WHITE);
        multiRenderer.setAxesColor(Color.DKGRAY);
        multiRenderer.setShowGridX(true);
        multiRenderer.setGridColor(Color.parseColor("#40E7E4E4"));
        multiRenderer.setLabelsColor(Color.DKGRAY);
        multiRenderer.setYAxisMin(0);
        multiRenderer.setXAxisMin(-1);
        multiRenderer.setXAxisMax(24);
        multiRenderer.setBarSpacing(0.2);

        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        float val = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, metrics);
        multiRenderer.setLabelsTextSize(val);
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        multiRenderer.setShowLegend(false);
        //multiRenderer.setXLabelsPadding(width / 20);
        multiRenderer.setYLabelsPadding(width / 35);
        multiRenderer.setChartTitleTextSize(val);
        multiRenderer.setMargins(new int[]{Math.round(width / 14), Math.round(width / 8), 0, 5});

        multiRenderer.setYLabelsColor(0, Color.DKGRAY);
        multiRenderer.setYLabelsAlign(Paint.Align.RIGHT);
        multiRenderer.setXLabels(0);
        multiRenderer.setXLabelsColor(Color.DKGRAY);
        //multiRenderer.setXLabelsAngle(-45);
        //renderer.setLegendTextSize(20);
        //Disable zoom
        multiRenderer.setPanEnabled(false, false);
        multiRenderer.setZoomEnabled(false, false);
        // Adding incomeRenderer and expenseRenderer to multipleRenderer
        // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
        // should be same
        multiRenderer.addSeriesRenderer(stepRenderer);
        // multiRenderer.addSeriesRenderer(expenseRenderer);

        mChart = (GraphicalView) ChartFactory.getBarChartView(c, dataset, multiRenderer, Type.DEFAULT);
        //multiRenderer.setSelectableBuffer(10);
        // Adding the Line Chart to the LinearLayout
        v.addView(mChart);
    }

    /***
     * updating graph on event call
     **/
    public void updateBarGraph(int[] steps) {
        for (int i = 0; i < x.length; i++) {
            stepSeries.add(i, steps[i]);
            ////Log.d("Hour>>>>>> " + i, "Steps>>>>" + steps[i]);
        }
        mChart.repaint();
    }

    /***
     * updating graph on event call
     **/
    public void updateCurrentBarGraph(int position, int updatedSteps) {
        //stepSeries.clear();
        //stepSeries.remove(position);
        stepSeries.add(position, updatedSteps);
        ////Log.e("Hour>>>>>> " + position, "Steps>>>>" + updatedSteps);
        mChart.repaint();
        mChart.invalidate();
    }

    /**
     * clear all previous values of chart
     */
    public void clearGraph() {
        stepSeries.clear();
    }

}
