/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.widgets;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;


import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.commonutils.Constants;
import com.atmel.wearable.graphdatabase.WearableDBHelper;
import com.atmel.wearable.models.StepClearModel;
import com.atmel.wearable.wearables.ServicesTabActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.greenrobot.event.EventBus;

public class ClearAlarm extends BroadcastReceiver {

    private WearableDBHelper mDBHelper;
    EventBus mClearBus = new EventBus();

    @Override
    public void onReceive(Context context, Intent intent) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Log.d("ClearAlarm", "wakeuptimer expired at " + sdf.format(new Date().getTime()));
        mDBHelper = new WearableDBHelper(context);
        // Put here YOUR code.
        removeAll(context);
        Date next = new Date(new Date().getTime() + ( 24 * 60 * 60 * 1000));
        PendingIntent sender = PendingIntent.getBroadcast(context, Constants.ID_CLEAR_ALARM, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //if( extrasBundle.getString("message").equals("night")){
        if (Build.VERSION.SDK_INT>=19) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, next.getTime(), sender);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, next.getTime(), sender);
        }
    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public void removeAll(Context context) {
        ////Log.e("24 hr clear", "Cleared");
        SQLiteDatabase db = mDBHelper.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(WearableDBHelper.STEPCOUNT_TABLE_NAME, null, null);
        AppUtils.setIntSharedPreference(context, AppUtils.STEP_COUNTER, 0);
        ServicesTabActivity.mCounter = 0;
        StepClearModel clear = new StepClearModel();
        clear.mClear = true;
        mClearBus.post(clear);
    }
}