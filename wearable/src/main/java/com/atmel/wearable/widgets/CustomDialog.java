/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.widgets;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.R;


public class CustomDialog {


    /**
     * Used to show the alert dialog
     *
     * @param c
     * @param Message
     */

    private Context mContext;

    public void showAlert(Context c, String Message, final String type) {

        mContext = c;

        // Create custom dialog object
        final Dialog mCustomDialog = new Dialog(c);
        // Include dialog.xml file
        mCustomDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mCustomDialog.setContentView(R.layout.custom_popup);

        // handling click action
        Button mButton = (Button) mCustomDialog.findViewById(R.id.declineButton);
        ImageView mIcon = (ImageView) mCustomDialog.findViewById(R.id.image_logo);
        RobotoMediumTextView mText = (RobotoMediumTextView) mCustomDialog.findViewById(R.id.textDialog);
        mIcon.setBackgroundResource(getImage(type));
        mText.setText(Message);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               setReenablePopupLimit(type);
                //  setPopupLimit(type);
                mCustomDialog.dismiss();
            }
        });
        mCustomDialog.show();
        mCustomDialog.setCanceledOnTouchOutside(false);
        setPopupLimit(type);
    }


    private int getImage(String type) {
        if (type.equals("BATTERY")) {
            return R.drawable.low_battery_icon;
        } else if (type.equals("RANGE")) {
            return R.drawable.low_battery_icon;
        } else {
            return R.drawable.wearable_drop_icon;
        }

    }

    /**
     * used to stop popup if already shown
     */

    private void setPopupLimit(String type) {
        if (type.equals("BATTERY")) {
            AppUtils.setBatteryAlert(mContext, false);
        } else if (type.equals("RANGE")) {

        } else {
            AppUtils.setDropAlert(mContext, false);
        }
    }


    /**
     * used to stop popup if already shown
     */

    private void setReenablePopupLimit(String type) {
        if (type.equals("BATTERY")) {
            AppUtils.setBatteryAlert(mContext, true);
        } else if (type.equals("RANGE")) {
            // do nothing
        } else {
            AppUtils.setDropAlert(mContext, true);
        }
    }


}
