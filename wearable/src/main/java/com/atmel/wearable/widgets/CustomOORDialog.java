/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.widgets;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.R;

public class CustomOORDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity mContext;
    public Dialog mDialog;
    public Button mOkButton, mSilenceButton;
    private Alarm mAlarm = new Alarm();

    public CustomOORDialog(Activity a) {
        super(a);
        this.mContext = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.outofrange_popup);
        mOkButton = (Button) findViewById(R.id.exit_button);
        mSilenceButton = (Button) findViewById(R.id.silence_button);
        mOkButton.setOnClickListener(this);
        mSilenceButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.exit_button) {
            mContext.setResult(Activity.RESULT_CANCELED);
            mAlarm.stopBeep();
            BLEConnection.closeGatt();
            dismiss();
            mContext.finish();

        } else if (i == R.id.silence_button) {
            mAlarm.stopBeep();
            //dismiss();

        } else {
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mAlarm.intializeSound(mContext, "beep.mp3");
        mAlarm.playBeep();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mAlarm.stopBeep();
    }

    @Override
    public void setOnDismissListener(OnDismissListener listener) {
        super.setOnDismissListener(listener);
        mAlarm.stopBeep();
    }
}