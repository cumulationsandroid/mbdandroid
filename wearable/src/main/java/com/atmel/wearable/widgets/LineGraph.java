/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.atmel.wearable.commonutils.AppUtils;
import com.atmel.wearable.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;


public class LineGraph {

    public String mGraphTitle, mGraphXaxisTitle, mGraphYaxisTitle;
    private GraphicalView mChart;
    private TimeSeries mVisitSeries;
    private XYMultipleSeriesRenderer multiRenderer;
    private Date oldMaxDate;
    private int mZoomCount = 0;
    private Context mContext;
    private int width;

    public void drawGraph(ViewGroup v, LineGraph graph, Context c,int typeOfGraph) {
        // Creating TimeSeries for Visits
        mVisitSeries = new TimeSeries(graph.mGraphTitle);
        mContext = c;
        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        // Adding Visits Series to the dataset
        dataset.addSeries(mVisitSeries);
        // Creating XYSeriesRenderer to customize visitsSeries
        XYSeriesRenderer visitsRenderer = new XYSeriesRenderer();
        visitsRenderer.setColor(c.getResources().getColor(R.color.colorPrimary));
       // visitsRenderer.setPointStyle(PointStyle.CIRCLE);
      //  visitsRenderer.setFillPoints(true);
        visitsRenderer.setLineWidth(2);
      //  visitsRenderer.setDisplayChartValues(true);
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(0);
        nf.setMaximumFractionDigits(2);
        visitsRenderer.setChartValuesFormat(nf);
        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setShowGrid(true);
        multiRenderer.setGridColor(Color.parseColor("#E7E4E4"));
        multiRenderer.setPanEnabled(true, false);
        multiRenderer.setZoomEnabled(true, false);
        multiRenderer.setAxesColor(Color.DKGRAY);
        multiRenderer.setMarginsColor(Color.WHITE);
        multiRenderer.setYLabels(7);
        multiRenderer.setXLabels(7);
        multiRenderer.setBackgroundColor(Color.WHITE);
        multiRenderer.setXLabelsColor(Color.DKGRAY);
        multiRenderer.setYLabelsColor(0, Color.DKGRAY);
        multiRenderer.setLabelsColor(Color.DKGRAY);
        multiRenderer.setShowLegend(false);
        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        float val = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, metrics);
        multiRenderer.setLabelsTextSize(val);
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
         width = size.x;
        multiRenderer.setChartTitleTextSize(val);
        visitsRenderer.setChartValuesTextSize(val);
        setAxisLimit(typeOfGraph);
        Date date = new Date();
        long t = date.getTime();
        Date afterAddingOneMinutes = new Date(t + (5*60000));
        multiRenderer.setXAxisMax(afterAddingOneMinutes.getTime());
        Date afterMinusingOneMinutes = new Date(t);
        multiRenderer.setXAxisMin(afterMinusingOneMinutes.getTime());
        multiRenderer.addSeriesRenderer(visitsRenderer);
        // Creating a Time Chart
        mChart = (GraphicalView) ChartFactory.getTimeChartView(c, dataset, multiRenderer, "hh:mm:ss");
        // Adding the Line Chart to the LinearLayout
        v.addView(mChart);
    }


    public void upDateTempGraph(float temp) {
        Date date = new Date();
        DecimalFormat df = new DecimalFormat("000.00");
        mVisitSeries.add(date,Float.valueOf(df.format(temp)));
        mChart.repaint();
    }

    public void upDateGraphData(long time, float value) {
        DecimalFormat df = new DecimalFormat("000.00");
        mVisitSeries.add(time, Float.valueOf(df.format(value)));
        mChart.repaint();
    }

    private void setAxisLimit(int type) {

        if(type ==1) {
            multiRenderer.setYLabelsPadding(width / 30);
            multiRenderer.setMargins(new int[]{Math.round(width / 14), Math.round(width / 14), 0, 5});
            if (AppUtils.getTemperatureUnit(mContext).equals(mContext.getResources()
                    .getString(R.string.degree_celsius_label))) {
                multiRenderer.setYAxisMax(85);
                multiRenderer.setYAxisMin(-40);
            } else {
                multiRenderer.setYAxisMax(185);
                multiRenderer.setYAxisMin(-40);
            }
        } else if(type ==2)  {
            multiRenderer.setYLabelsPadding(width / 30);
            multiRenderer.setMargins(new int[]{Math.round(width / 14), Math.round(width / 14), 0, 5});
            multiRenderer.setYAxisMax(100);
            multiRenderer.setYAxisMin(0);
        } else if(type ==3)  {
            multiRenderer.setYLabelsPadding(width / 20);
            multiRenderer.setMargins(new int[]{Math.round(width / 14), Math.round(width / 8), 0, 5});
            multiRenderer.setYAxisMax(1100);
            multiRenderer.setYAxisMin(300);
        } else if(type ==4)  {
            multiRenderer.setYLabelsPadding(width / 20);
            multiRenderer.setMargins(new int[]{Math.round(width / 14), Math.round(width / 8), 0, 5});
            multiRenderer.setYAxisMax(4560);
            multiRenderer.setYAxisMin(0);
        } else {

        }


    }

    public void upDateXmaxAndXmin() {
        oldMaxDate = new Date();
        multiRenderer.setXAxisMax(oldMaxDate.getTime());
        multiRenderer.setXAxisMin(new Date(oldMaxDate.getTime() - (5*60000)).getTime());
    }
}
