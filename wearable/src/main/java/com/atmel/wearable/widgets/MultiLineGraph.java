/*
 *
 *
 *   The service that handles the communication with BluetoothGatt and
 *   handles the callback and delegates to application classes
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */
package com.atmel.wearable.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.atmel.wearable.graphdatabase.AcceleroMeterGraphModel;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.Date;


public class MultiLineGraph {


    private GraphicalView mChart;
    private XYSeries xSeries, ySeries, zSeries;
    private XYMultipleSeriesRenderer multiRenderer;
    private Double mXmax = 5.0, mXmin = 0.0;
    private double mInitialStartTime = 0.0D;
    private boolean isFirstConnect, needToCheckTime;
    private double mBreakTimeStart = 0.0D, mBreakTimeStop = 0.0D;
    private double difference = 0;
    private  Double breakTime =0.0;
    private  Double mTotalBreakTime =0.0;

    public void drawGraph(ViewGroup v, Context c, boolean isGyro) {


        // Creating an  XYSeries for X- axis values
        xSeries = new XYSeries("X-axis");
        xSeries.add(-1,0);
        // Creating an  XYSeries for Y- axis values
        ySeries = new XYSeries("Y-axis");
        // Creating an  XYSeries for Z- axis values
        zSeries = new XYSeries("Z-axis");
        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        // Adding X Series to the dataset
        dataset.addSeries(xSeries);
        // Adding Y Series to dataset
        dataset.addSeries(ySeries);
        // Adding Z Series to dataset
        dataset.addSeries(zSeries);
        isFirstConnect = true;
        needToCheckTime = false;
        // Creating XYSeriesRenderer to customize xSeries
        XYSeriesRenderer xRenderer = new XYSeriesRenderer();
        xRenderer.setColor(Color.parseColor("#f43935"));
       // xRenderer.setPointStyle(PointStyle.CIRCLE);
       // xRenderer.setFillPoints(true);
        xRenderer.setLineWidth(2);
       // xRenderer.setDisplayChartValues(true);
        //xRenderer.setPointStrokeWidth(3);

        // Creating XYSeriesRenderer to customize ySeries
        XYSeriesRenderer yRenderer = new XYSeriesRenderer();
        yRenderer.setColor(Color.parseColor("#00b5e7"));
        //yRenderer.setPointStyle(PointStyle.CIRCLE);
        //yRenderer.setFillPoints(true);
        yRenderer.setLineWidth(2);
      //  yRenderer.setDisplayChartValues(true);
        // yRenderer.setPointStrokeWidth(3);

        // Creating XYSeriesRenderer to customize zSeries
        XYSeriesRenderer zRenderer = new XYSeriesRenderer();
        zRenderer.setColor(Color.parseColor("#7bb81e"));
        //zRenderer.setPointStyle(PointStyle.CIRCLE);
      //  zRenderer.setFillPoints(true);
        zRenderer.setLineWidth(2);
       // zRenderer.setDisplayChartValues(true);
        //  zRenderer.setPointStrokeWidth(3);

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setYLabels(5);
        multiRenderer.setXLabels(7);
        // multiRenderer.setXTitle("Time");
        // multiRenderer.setYTitle("ACCELERATION VALUE");
        multiRenderer.setShowGrid(true);
        multiRenderer.setGridColor(Color.parseColor("#E7E4E4"));
        multiRenderer.setPanEnabled(true, false);
        multiRenderer.setPanLimits(new double[]{0.0, Double.MAX_VALUE, 0.0, Double.MAX_VALUE});
        multiRenderer.setZoomEnabled(true, false);
        multiRenderer.setAxesColor(Color.DKGRAY);
        multiRenderer.setMarginsColor(Color.WHITE);
        multiRenderer.setBackgroundColor(Color.WHITE);
        multiRenderer.setXLabelsColor(Color.DKGRAY);
        multiRenderer.setYLabelsColor(0, Color.DKGRAY);
        multiRenderer.setLabelsColor(Color.DKGRAY);
        multiRenderer.setAntialiasing(false);
        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        float val = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, metrics);
        multiRenderer.setLabelsTextSize(val);
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        multiRenderer.setShowLegend(false);
        // multiRenderer.setXLabelsPadding(width / 20);
        multiRenderer.setChartTitleTextSize(val);
        multiRenderer.setXAxisMax(mXmax);
        multiRenderer.setXAxisMin(mXmin);


        if (isGyro) {
            multiRenderer.setYAxisMax(2000);
            multiRenderer.setYAxisMin(-2000);
            multiRenderer.setYLabelsPadding(width / 20);
            multiRenderer.setMargins(new int[]{Math.round(width / 14), Math.round(width / 8), 0, 5});

        } else {
            multiRenderer.setYAxisMin(-16);
            multiRenderer.setYAxisMax(16);
            multiRenderer.setYLabelsPadding(width / 30);
            multiRenderer.setMargins(new int[]{Math.round(width / 14), Math.round(width / 14), 0, 5});
        }

        // Adding xRenderer,yRenderer and zRenderer to multipleRenderer
        // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
        // should be same
        multiRenderer.addSeriesRenderer(xRenderer);
        multiRenderer.addSeriesRenderer(yRenderer);
        multiRenderer.addSeriesRenderer(zRenderer);
        multiRenderer.setZoomButtonsVisible(false);
        multiRenderer.setExternalZoomEnabled(true);
        // Creating a Time Chart
        mChart = (GraphicalView) ChartFactory.getLineChartView(c, dataset, multiRenderer);
        mChart.setBackgroundColor(Color.WHITE);
        v.addView(mChart);

    }
    /***
     * updating graph on event call for Accelero
     **/
    public synchronized void upDateGraph(AcceleroMeterGraphModel temp) {

        if (isFirstConnect) {
            mInitialStartTime = new Date().getTime();
            isFirstConnect = false;
        }
        if (needToCheckTime) {
            mBreakTimeStart = new Date().getTime();
            needToCheckTime = false;
            if (mBreakTimeStart > mBreakTimeStop && mBreakTimeStop != 0.0D) {
                breakTime = mBreakTimeStart - mBreakTimeStop;

            } else {
                breakTime = 0.0;
            }
            mTotalBreakTime = mTotalBreakTime+breakTime;
        }
        difference =0.0D;
        difference = (new Date().getTime() - mInitialStartTime )- mTotalBreakTime;
        difference = difference / 1000;
        xSeries.add(difference, temp.x);
        ySeries.add(difference, temp.y);
        zSeries.add(difference, temp.z);
        if (difference > 4.9) {
            multiRenderer.setXAxisMax(difference + .1);
            multiRenderer.setXAxisMin(difference - 4.9);
        }
        mChart.repaint();
    }


    /**
     * calculate break time
     */

    public void setBreakTimeStart() {

    }

    public void setBreakTimeStop() {
        mBreakTimeStop = new Date().getTime();
        needToCheckTime = true;
    }


    /**
     * reset graph
     */
    public void resetGraph() {
        try {
            mXmax = 5.0;
            mXmin = 0.0;
            multiRenderer.setXAxisMax(mXmax);
            multiRenderer.setXAxisMin(mXmin);
            mBreakTimeStart = 0.0D;
            mBreakTimeStop = 0.0D;
            xSeries.clear();
            ySeries.clear();
            zSeries.clear();
            mChart.invalidate();
            isFirstConnect = true;
            needToCheckTime =false;
            mTotalBreakTime =0.0D;
            mInitialStartTime =0.0D;
            breakTime = 0.0D;
        } catch (Exception e) {
            e.printStackTrace(); //if event bus will not call,timer will be null.
        }
    }


}
