/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.widgets;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.atmel.wearable.commonutils.AppConstants;
import com.atmel.wearable.connection.BLEConnection;
import com.atmel.wearable.wearables.BaseActivity;
import com.atmel.wearable.R;


public class ReconnectAlert {

    Button mExit;
    Activity mActivity;
    Dialog mDialog;

    public ReconnectAlert(Activity activity) {
        this.mActivity = activity;
        mDialog = new Dialog(mActivity);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.wearable_alertview);
        mExit = (Button) mDialog.findViewById(R.id.exit_button);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        mExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissAlert();
                Intent serviceIntent = new Intent(mActivity, BLEConnection.class);
                mActivity.stopService(serviceIntent);
                Intent intent = new Intent(mActivity, BaseActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConstants.DEVICE_DISCONNECTED_RESULT_CODE, true);
                BLEConnection.disconnect();
                mActivity.startActivity(intent);

            }
        });

    }


    public void showAlert() {
        mDialog.show();
    }

    public void dismissAlert() {
        mDialog.dismiss();
    }

    public void finishAlert() {
        mDialog.cancel();
        mDialog.dismiss();
    }

}
