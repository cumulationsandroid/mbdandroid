/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.atmel.wearable.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import com.atmel.wearable.R;

/**
 * Created by
 */
public class RobotoMediumEditText extends EditText implements TextWatcher {


    public static final int SECOND = 1;
    public static final int MILLIE_SECOND = 2;
    private boolean mValidation = false;


    private float mMaxVlaue = 0f;
    private float mMinVlaue = 0f;
    private int mUnitType;
    private int mFinalValue = 0;

    public RobotoMediumEditText(Context context) {
        super(context);
        style(context);
    }

    public RobotoMediumEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);

    }

    public RobotoMediumEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        style(context);
    }

    public int getFinalValue() {
        return mFinalValue;
    }

    private void setFinalValue(int mFinalValue) {
        this.mFinalValue = mFinalValue;
    }

    private void style(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/Roboto-Medium.ttf");
        setTypeface(tf);
        
    }

    /**
     * @param isValidation
     */
    public void enableValidation(boolean isValidation) {
        mValidation = isValidation;
        if (isValidation) {
            makeValidation();
        } else {
            this.removeTextChangedListener(this);
        }
    }


    private void makeValidation() {

        this.addTextChangedListener(this);

    }

    /**
     * @return
     */
    public boolean isValidation() {
        return mValidation;
    }

    public void setMinValue(float v) {
        mMinVlaue = v;
    }

    public void setMaxValue(float v) {
        mMaxVlaue = v;
    }

    public float getMaxVlaue() {
        return mMaxVlaue;
    }

    public float getMinVlaue() {
        return mMinVlaue;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        try {
//            long maxValue = 0;
//            long minVlaue = 0;


            if (mValidation) {
                if (text.length() > 0) {

                    if (mUnitType == SECOND) {
//                        maxValue = TimeUnit.SECONDS.toMillis((long) getMaxVlaue());
//                        minVlaue = TimeUnit.SECONDS.toMillis((long) getMinVlaue());
                      //  long currentValue = TimeUnit.SECONDS.toMillis(Long.parseLong(text.toString()));
                        setFinalValue(Integer.parseInt(text.toString()));
                       /* if (currentValue < minVlaue) {
                            setError("Minimum should be greater than " + getMinVlaue() + " Seconds");
                        } else if (currentValue > maxValue) {
                            setError("Maximum should be less than " + getMaxVlaue() + " Seconds");
                        } else {
                            setFinalValue(currentValue);
                        }*/

                    } else if (mUnitType == MILLIE_SECOND) {
//                        maxValue = TimeUnit.MILLISECONDS.toMillis((long) getMaxVlaue());
//                        minVlaue = (long) getMinVlaue();
                        int currentValue = Integer.parseInt(text.toString());
                        setFinalValue(currentValue);
                       /* if (currentValue < minVlaue) {
                            setError("Minimum should be greater than " + getMinVlaue() + " Milli Seconds");

                        } else if (currentValue > maxValue) {
                            setError("Maximum should be less than " + getMaxVlaue() + " Milli Seconds");

                        } else {
                            setFinalValue(currentValue);
                        }*/
                    }
                }
            }
        } catch (Exception ignored) {

        }
    }

    /**
     * @param unit
     */
    public void setTimeUnit(int unit) {
        mUnitType = unit;
        /**
         * Setting Drawable
         */

    }

    public int getmUnitType() {
        return mUnitType;
    }

    /**
     * @param unit
     */
    public void setUnitDrawable(int unit) {
        if (unit == SECOND) {
            setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.s, 0);
        } else if (unit == MILLIE_SECOND) {
            setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ms, 0);
        }
    }
}
