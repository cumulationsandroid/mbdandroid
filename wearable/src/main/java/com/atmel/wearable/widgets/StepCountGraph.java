/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */


package com.atmel.wearable.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.atmel.wearable.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer.FillOutsideLine;

import java.util.Date;


public class StepCountGraph {

    private TimeSeries mSeries = new TimeSeries("Step Count");
    //XYMultipleSeriesDataset will contain all the TimeSeries
    private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();

    //XYMultipleSeriesRenderer will contain all XYSeriesRenderer and it can be used to set the properties of whole Graph
    final XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
    private static StepCountGraph mInstance = null;

    private GraphicalView mChart;
    private Date oldMaxDate;

    /**
     * singleton implementation of LineGraphView class
     */
    public static synchronized StepCountGraph getLineGraphView() {
        if (mInstance == null) {
            mInstance = new StepCountGraph();
        }
        return mInstance;
    }

    /**
     * This constructor will set some properties of single chart and some properties of whole graph
     */
    public void drawGraph(ViewGroup v, StepCountGraph graph, Context c) {
        //add single line chart mSeries
        mDataset.addSeries(mSeries);

        //XYSeriesRenderer is used to set the properties like chart color, style of each point, etc. of single chart
        final XYSeriesRenderer seriesRenderer = new XYSeriesRenderer();
        FillOutsideLine fill = new FillOutsideLine(FillOutsideLine.Type.BOUNDS_ABOVE);
        fill.setColor(c.getResources().getColor(R.color.actionBarColorDark));
        seriesRenderer.addFillOutsideLine(fill);

        seriesRenderer.setColor(c.getResources().getColor(R.color.actionBarColorDark));
        //set line chart style to square points
        seriesRenderer.setFillPoints(true);
        seriesRenderer.setLineWidth(3);
        // Include low and max value
        seriesRenderer.setDisplayBoundingPoints(true);
        // we add point markers
        seriesRenderer.setPointStyle(PointStyle.CIRCLE);
        seriesRenderer.setPointStrokeWidth(5);

        renderer.setYLabels(5);
        renderer.setXLabels(5);
        //set whole graph background color to transparent color
        renderer.setBackgroundColor(Color.TRANSPARENT);
        renderer.setMarginsColor(Color.WHITE);
        renderer.setAxesColor(Color.DKGRAY);
        renderer.setShowGrid(true);
        renderer.setGridColor(Color.parseColor("#E7E4E4"));
        renderer.setLabelsColor(Color.BLACK);

        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        float val = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, metrics);
        renderer.setLabelsTextSize(val);
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        renderer.setShowLegend(false);
        // multiRenderer.setXLabelsPadding(width / 20);
        renderer.setYLabelsPadding(width / 35);
        renderer.setChartTitleTextSize(val);
        renderer.setMargins(new int[]{40, Math.round(width / 8), 40, 20});
        oldMaxDate = new Date();
        renderer.setXAxisMin(new Date(oldMaxDate.getTime() - (12 * 60 * 60000)).getTime());
        renderer.setXAxisMax(new Date(oldMaxDate.getTime() + (12 * 60 * 60000)).getTime());
        //   multiRenderer.setYLabelsAlign(Paint.Align.RIGHT);

        renderer.setYLabelsColor(0, Color.DKGRAY);
        renderer.setYLabelsAlign(Paint.Align.RIGHT);
        renderer.setXLabelsColor(Color.DKGRAY);
        //renderer.setLegendTextSize(20);
        //Disable zoom
        renderer.setPanEnabled(true, false);
        renderer.setZoomEnabled(true, false);
        //set title to x-axis and y-axis
//        renderer.setXTitle("    Time");
//        renderer.setYTitle(c.getResources().getString(R.string.step_graph_x));
        renderer.addSeriesRenderer(seriesRenderer);

        // Creating a Time Chart
        mChart = (GraphicalView) ChartFactory.getTimeChartView(c, mDataset, renderer, "hh:mm");
        renderer.setSelectableBuffer(10);
        // Adding the Line Chart to the LinearLayout
        v.addView(mChart);
    }

//    /**
//     * return graph view to activity
//     */
//    public GraphicalView getView(Context context) {
//        final GraphicalView graphView = ChartFactory.getTimeChartView(context, mDataset, mMultiRenderer, "hh:mm:ss");
//        return graphView;
//    }

    public static Integer getDeviceDpi(Context context) {
        return context.getResources().getDisplayMetrics().densityDpi;
    }

    /***
     * updating graph on event call
     **/
    public void upDateGraph(Integer step) {
        Date date = new Date();
        long time = date.getTime();
        mSeries.add(time, step);
        mChart.repaint();
    }

    /***
     * updating graph on event call
     **/
    public void upDateGraphDB(Long time, Integer temp) {
        mSeries.add(time, temp);
        mChart.repaint();
    }

    public void setAxisLimit(int value) {
        renderer.setYAxisMax(value);
        renderer.setYAxisMin(0);
    }

    /**
     * clear all previous values of chart
     */
    public void clearGraph() {
        mSeries.clear();
    }

}
