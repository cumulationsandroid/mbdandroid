/*
 *   \file filename.extension
 *
 *   \brief Short description about the file
 *
 *   Copyright (c) 2015, Atmel Corporation. All rights reserved.
 *   Released under NDA
 *   Licensed under Atmel's Limited License Agreement.
 *
 *
 *   THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 *   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 *   EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *   OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 *   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 *
 *   Atmel Corporation: http://www.atmel.com
 *
 */

package com.atmel.wearable.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.atmel.wearable.R;

public class WearableStatusView extends View {
    private Bitmap mDeviceBitmap;
    private Bitmap mTagBitmap;
    private float centerX;
    private float centerY;
    private float outerRadius;
    private int progress;
    private int mSafeMin = 0;
    private int mSafeMax = 0;
    private int decreaseRadius;
    private int mMidMin = 0;
    private int mMidMax = 0;
    private int mDangerMin = 0;
    private int mDangerMax = 0;
    float prevX;
    float prevY;
    Paint paintTwo = new Paint(Paint.ANTI_ALIAS_FLAG);
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Paint paintThree = new Paint(Paint.ANTI_ALIAS_FLAG);
    Paint paintWhite = new Paint(Paint.ANTI_ALIAS_FLAG);
    Paint paintText = new Paint(Paint.LINEAR_TEXT_FLAG);
    Paint paintTransparent =  new Paint(Paint.ANTI_ALIAS_FLAG);


    public WearableStatusView(Context context) {
        super(context);
        init();

    }

    private void init() {
        mTagBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.wearable_device);
        mDeviceBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.werable_phone);

        paintTwo.setStyle(Paint.Style.STROKE);
        paintTwo.setColor(getResources().getColor(R.color.featureTitleColor));
        paintTwo.setStrokeWidth(7);

        paintTransparent.setStyle(Paint.Style.STROKE);
        paintTransparent.setColor(getResources().getColor(R.color.transparent));
        paintTransparent.setStrokeWidth(7);

        paintWhite.setStyle(Paint.Style.STROKE);
        paintWhite.setColor(getResources().getColor(R.color.white));
        paintWhite.setStrokeWidth(7);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(getResources().getColor(R.color.colorAccent2));
        //applyFilter(paintTwo, new float[]{0f, -1f, 0.5f}, 0.9f, 15f, 1f);

        paintText.setFakeBoldText(true);
        paintText.setColor(getResources().getColor(R.color.actionBarColor));
        int scaledSize = getResources().getDimensionPixelSize(R.dimen.text_medium);
        paintText.setTextSize(scaledSize);
    }

    public WearableStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WearableStatusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawCircle(centerX, centerY, outerRadius, paint);
        canvas.drawCircle(centerX, centerY, outerRadius + 5, paintTwo);

        //Drawing Device image
        canvas.drawBitmap(mDeviceBitmap, centerX - (mDeviceBitmap.getWidth() / 2), centerY - mDeviceBitmap.getHeight(), paint);

        canvas.drawCircle(centerX, centerY, outerRadius * 2, paintTwo);
        canvas.drawCircle(centerX, centerY, (outerRadius * 2) - 7, paintWhite);
       // canvas.drawText("> -100dBm", (getWidth() / 2) - (mTagBitmap.getWidth()), ((outerRadius * 2) + 7) + (outerRadius / 2), paintText);

        canvas.drawCircle(centerX, centerY, outerRadius * 4, paintTwo);
        canvas.drawCircle(centerX, centerY, (outerRadius * 4) - 7, paintWhite);
       // canvas.drawText("> -60dBm", (getWidth() / 2) - (mTagBitmap.getWidth()), ((outerRadius * 4) + 7) + (outerRadius / 3), paintText);

        canvas.drawCircle(centerX, centerY, outerRadius * 6, paintTwo);
        canvas.drawCircle(centerX, centerY, (outerRadius * 6) - 7, paintWhite);
      //  canvas.drawText("> -30dBm", (getWidth() / 2) - (mTagBitmap.getWidth()), ((outerRadius * 6) + 7) + (outerRadius / 6), paintText);


        canvas.drawCircle(centerX, centerY, centerY - 10, paintTransparent);
        //canvas.drawCircle(centerX, centerY, (centerY - 10) - 7, paintWhite);
        //canvas.drawText("> -30dBm", (getWidth() / 2) - (mTagBitmap.getWidth()), ((outerRadius * 6) + 7) + (outerRadius / 6), paintText);

        //float prevY = (centerY - mTagBitmap.getHeight()) - decreaseRadius * 5;
        float prevY = (centerY - mTagBitmap.getHeight()) - decreaseRadius ;
        float prevX = centerX - (mTagBitmap.getWidth() / 2);
        // Drawing Tag image

        canvas.drawBitmap(mTagBitmap, prevX, prevY, paintThree);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        centerX = w / 2;
        centerY = h;
        outerRadius = (float) (centerX / 2.5);
    }

    /**
     * @param paint
     * @param direction
     * @param ambient
     * @param specular
     * @param blurRadius
     */
    private void applyFilter(Paint paint, float[] direction, float ambient,
                             float specular, float blurRadius) {
        if (Build.VERSION.SDK_INT >= 11) {
            this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        EmbossMaskFilter filter = new EmbossMaskFilter(
                direction, ambient, specular, blurRadius);
        paint.setMaskFilter(filter);
    }

    /**
     * Set Progress
     *
     * @param progress
     */
    public synchronized void setProgress(int progress) {
        //  invalidate();
        this.progress = progress;
        int region;
        int pors = 0;
        //int pors = (int) outerRadius / 4;


        if (progress <= mSafeMin) {
            pors = (int) ( outerRadius );
            region = pors / ( mSafeMin);
            int i = Math.abs(progress);
            region = region * i;
            //region = (int)outerRadius;
        } else if (progress > mSafeMin && progress <= mSafeMax) {
            pors = (int) ( outerRadius*2 - outerRadius );
            region = pors / (mSafeMax - mSafeMin);
            int i = Math.abs(mSafeMin - progress);
            region = region * i + (int)outerRadius;
        } else if (progress > mMidMin && progress <= mMidMax) {
            pors = (int) ( outerRadius*4 - outerRadius*2 );
           // region = ((pors * 2) - pors) / (mMidMax - mMidMin);
            region = pors/ (mMidMax - mMidMin);
            int i = Math.abs(mMidMin - progress);
            region = (region * i) + (int) outerRadius*2 ;
        } else if (progress > mDangerMin && progress <= mDangerMax) {
            pors = (int) ( outerRadius*6 - outerRadius*4 );
           // region = ((int) outerRadius - (pors * 2)) / (mDangerMax - mDangerMin);
            region = pors/ (mDangerMax - mDangerMin);
            int i = Math.abs(mDangerMin - progress);
            region = (region * i) + (int)outerRadius*4;
        } else if(progress > mDangerMax && progress <= 120) {
            pors = (int) ( centerY - 10 - outerRadius*6 );
            // region = ((int) outerRadius - (pors * 2)) / (mDangerMax - mDangerMin);
            region = pors/ (120 - mDangerMax);
            int i = Math.abs(mDangerMax - progress);
            region = (region * i) + (int)(outerRadius*6);
        }else{
            region = (int)centerY - 10;
        }
        decreaseRadius = region;
        invalidate();
    }

    public void setRange(int safeMin, int safeMax, int midMin, int midMax, int dangerMin, int dangerMax) {
        mSafeMin = safeMin;
        mSafeMax = safeMax;
        mMidMin = midMin;
        mMidMax = midMax;
        mDangerMin = dangerMin;
        mDangerMax = dangerMax;
    }

}
